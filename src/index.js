/* @flow */
const EventEmitter = require('events');
const Koa = require('koa')
const Pug = require('koa-pug')
const serve = require('koa-static')
const logger = require('koa-logger')
const mongo = require('./config/db')
const routes = require('./config/routes')

const app = new Koa()

app.events = new EventEmitter()

app.events.onDbConnected = (fn) => {
  return app.events.on('db-connected', fn)
}

mongo.connect(app)
  .then((db) => {
    console.log('Db connected')
    mongo.setEvents(db)
    routes.startRouter(app, db)
    app.events.emit('db-connected')
  })
  .catch(mongo.retryConnection)

const pug : Object = new Pug({
  debug: process.env === 'development',
  viewPath: `${__dirname}/views`,
  app: app
})

if (process.env.NODE_ENV !== 'test') {
  app.use(logger())
}

app.use(serve(`${__dirname}/public`))

module.exports = app
