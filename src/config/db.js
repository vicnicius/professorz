/* @flow */
'use strict'

const configuration = require('./configuration')
const Client = require('mongodb').MongoClient

const url : string = configuration.database_url

function retryConnection (error : string) {
  console.error(error)
  return setTimeout(() => {
    connect().catch(retryConnection)
  }, 1000)
}

function setEvents (db : Object) {
  return db.on('close', () => {
    retryConnection('Database connection closed')
  })
}

function connect () {
  return Client.connect(url, {wtimeout: 5000})
}

module.exports = {
  connect: connect,
  setEvents: setEvents,
  retryConnection: retryConnection
}
