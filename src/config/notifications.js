/* @flow */
'use strict'

const Mailgun = require('mailgun-js')
const configuration = require('./configuration')

const api_key : string = configuration.mg.api_key
const domain : string = configuration.mg.domain
const from_who : string = configuration.mg.from

const mailgun : Object = new Mailgun({apiKey: api_key, domain: domain})

function notifyInvite (email : string) {
  const data = {
    from: from_who,
    to: email,
    subject: 'Bem-vindo ao Professor Z',
    html: `<h3>Bem-vindo ao Professor Z!</h3>
            <p>Estamos trabalhando a todo vapor para construir um grande parceiro para o dia-a-dia do professor.</p>
            <p>Você está na lista de convidados para a versão de testes, então assim que tudo estiver pronto, você receberá um e-mail com o link para acessar a plataforma.</p>
            <p>Enquanto isso, você pode acompanhar novidades em nossa <a href="http://fb.me/professorz.co">página no facebook</a>.<p>
            <p>Um grande abraço da equipe Professor Z.</p>`
  }

  return mailgun.messages().send(data, (err, body) => {
    if (err) {
      console.error(err)
    }

    console.log(body)
  })
}

module.exports = {
  notifyInvite: notifyInvite
}
