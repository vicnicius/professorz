/* @flow */
'use strict'

const router = require('koa-router')()
const bodyParser = require('koa-bodyparser')
const notifications = require('./notifications')
const User = require('../models/User')

const routes : Object = {
  startRouter (app, database) {
    apiRoutes(router, database)
    staticRoutes(router)
    app.use(bodyParser())
    app.use(router.routes())
    app.use(router.allowedMethods())
  }
}

function staticRoutes (router) {
  router
    .get('/', (ctx, next) => {
      ctx.render('index')
    })
    .get('/app', (ctx, next) => {
      ctx.status = 200
      ctx.render('application')
    })
  return router
}

function apiRoutes (router, database) {
  router
    .get('/api/auth', (ctx, next) => {
        ctx.status = err.status || 500
        ctx.body = {message: err.message}
    })

  return router
}

module.exports = routes
