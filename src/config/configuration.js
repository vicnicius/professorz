/* @flow */
type Mailgun = {api_key: string; domain: string; from: string}

const config : {database_url: string; mg: Mailgun, secret: string} = {
  database_url: process.env.NODE_ENV === 'test' ? 'mongodb://localhost:27017/test' : 'mongodb://localhost:27017/professorz',
  mg: {
    api_key: 'key-338c0eed9aeade34f599cb5dfc06a967',
    domain: 'mail.professorz.co',
    from: 'Professor Z <contato@professorz.co>'
  },
  secret: 'PleaseDoNotHackMyWebServer_IKnowYouAreTenTimesSmarterThanMe'
}

module.exports = config
