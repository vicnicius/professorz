/* @flow */

function checkIfExists (database : Object, email : string) {
  return new Promise((resolve, reject) => {
    database.collection('users')
      .findOne({ email: email })
      .then((returned) => {
        if (returned) {
          reject({status: 409, message: 'User already exists...'})
        }
        resolve(returned)
      })
      .catch(reject)
  })
}

function createUser (database : Object, email : string) {
  return database.collection('users').insertOne({ email: email })
}

const User = (database : Object) => {
  return {
    create (email : string) {
      return new Promise((resolve, reject) => {
        if (database) {
          checkIfExists(database, email)
            .then((data) => {
              createUser(database, email)
                .then(resolve)
                .catch(reject)
            })
            .catch(reject)
        } else {
          reject({message: 'Can\'t connect to database...'})
        }
      })
    }
  }
}

module.exports = User
