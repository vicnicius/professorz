import babel from 'rollup-plugin-babel'

export default {
  entry: 'client/js/application.js',
  dest: 'src/public/js/application.js',
  sourceMap: true,
  format: 'iife',
  moduleName: 'app',
  plugins: [babel()],
  globals: {
    mithril: 'm'
  }
}

