/*******************************
            Set-up
*******************************/

var gulp = require('gulp-help')(require('gulp')),


// read user config to know what task to load
config = require('./tasks/config/user'),


// watch changes
watch = require('./tasks/watch'),


// build all files
build = require('./tasks/build'),
    buildJS = require('./tasks/build/javascript'),
    buildCSS = require('./tasks/build/css'),
    buildAssets = require('./tasks/build/assets'),


// utility
clean = require('./tasks/clean'),
    version = require('./tasks/version'),


// docs tasks
serveDocs = require('./tasks/docs/serve'),
    buildDocs = require('./tasks/docs/build'),


// rtl
buildRTL = require('./tasks/rtl/build'),
    watchRTL = require('./tasks/rtl/watch');

/*******************************
             Tasks
*******************************/

gulp.task('default', false, ['watch']);

gulp.task('watch', 'Watch for site/theme changes', watch);

gulp.task('build', 'Builds all files from source', build);
gulp.task('build-javascript', 'Builds all javascript from source', buildJS);
gulp.task('build-css', 'Builds all css from source', buildCSS);
gulp.task('build-assets', 'Copies all assets from source', buildAssets);

gulp.task('clean', 'Clean dist folder', clean);
gulp.task('version', 'Displays current version of Semantic', version);

/*--------------
      Docs
---------------*/

/*
  Lets you serve files to a local documentation instance
  https://github.com/Semantic-Org/Semantic-UI-Docs/
*/

gulp.task('serve-docs', 'Serve file changes to SUI Docs', serveDocs);
gulp.task('build-docs', 'Build all files and add to SUI Docs', buildDocs);

/*--------------
      RTL
---------------*/

if (config.rtl) {
  gulp.task('watch-rtl', 'Watch files as RTL', watchRTL);
  gulp.task('build-rtl', 'Build all files as RTL', buildRTL);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL2d1bHBmaWxlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFJQSxJQUNFLE9BQWUsUUFBUSxXQUFSLEVBQXFCLFFBQVEsTUFBUixDQUFyQixDQURqQjtBQUFBOzs7QUFJRSxTQUFlLFFBQVEscUJBQVIsQ0FKakI7QUFBQTs7O0FBT0UsUUFBZSxRQUFRLGVBQVIsQ0FQakI7QUFBQTs7O0FBVUUsUUFBZSxRQUFRLGVBQVIsQ0FWakI7QUFBQSxJQVdFLFVBQWUsUUFBUSwwQkFBUixDQVhqQjtBQUFBLElBWUUsV0FBZSxRQUFRLG1CQUFSLENBWmpCO0FBQUEsSUFhRSxjQUFlLFFBQVEsc0JBQVIsQ0FiakI7QUFBQTs7O0FBZ0JFLFFBQWUsUUFBUSxlQUFSLENBaEJqQjtBQUFBLElBaUJFLFVBQWUsUUFBUSxpQkFBUixDQWpCakI7QUFBQTs7O0FBb0JFLFlBQWUsUUFBUSxvQkFBUixDQXBCakI7QUFBQSxJQXFCRSxZQUFlLFFBQVEsb0JBQVIsQ0FyQmpCO0FBQUE7OztBQXdCRSxXQUFlLFFBQVEsbUJBQVIsQ0F4QmpCO0FBQUEsSUF5QkUsV0FBZSxRQUFRLG1CQUFSLENBekJqQjs7Ozs7O0FBaUNBLEtBQUssSUFBTCxDQUFVLFNBQVYsRUFBcUIsS0FBckIsRUFBNEIsQ0FDMUIsT0FEMEIsQ0FBNUI7O0FBSUEsS0FBSyxJQUFMLENBQVUsT0FBVixFQUFtQiw4QkFBbkIsRUFBbUQsS0FBbkQ7O0FBRUEsS0FBSyxJQUFMLENBQVUsT0FBVixFQUFtQiw4QkFBbkIsRUFBbUQsS0FBbkQ7QUFDQSxLQUFLLElBQUwsQ0FBVSxrQkFBVixFQUE4QixtQ0FBOUIsRUFBbUUsT0FBbkU7QUFDQSxLQUFLLElBQUwsQ0FBVSxXQUFWLEVBQXVCLDRCQUF2QixFQUFxRCxRQUFyRDtBQUNBLEtBQUssSUFBTCxDQUFVLGNBQVYsRUFBMEIsK0JBQTFCLEVBQTJELFdBQTNEOztBQUVBLEtBQUssSUFBTCxDQUFVLE9BQVYsRUFBbUIsbUJBQW5CLEVBQXdDLEtBQXhDO0FBQ0EsS0FBSyxJQUFMLENBQVUsU0FBVixFQUFxQixzQ0FBckIsRUFBNkQsT0FBN0Q7Ozs7Ozs7Ozs7O0FBV0EsS0FBSyxJQUFMLENBQVUsWUFBVixFQUF3QixnQ0FBeEIsRUFBMEQsU0FBMUQ7QUFDQSxLQUFLLElBQUwsQ0FBVSxZQUFWLEVBQXdCLHFDQUF4QixFQUErRCxTQUEvRDs7Ozs7O0FBT0EsSUFBRyxPQUFPLEdBQVYsRUFBZTtBQUNiLE9BQUssSUFBTCxDQUFVLFdBQVYsRUFBdUIsb0JBQXZCLEVBQTZDLFFBQTdDO0FBQ0EsT0FBSyxJQUFMLENBQVUsV0FBVixFQUF1Qix3QkFBdkIsRUFBaUQsUUFBakQ7QUFDRCIsImZpbGUiOiJndWxwZmlsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICAgICBTZXQtdXBcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbnZhclxuICBndWxwICAgICAgICAgPSByZXF1aXJlKCdndWxwLWhlbHAnKShyZXF1aXJlKCdndWxwJykpLFxuXG4gIC8vIHJlYWQgdXNlciBjb25maWcgdG8ga25vdyB3aGF0IHRhc2sgdG8gbG9hZFxuICBjb25maWcgICAgICAgPSByZXF1aXJlKCcuL3Rhc2tzL2NvbmZpZy91c2VyJyksXG5cbiAgLy8gd2F0Y2ggY2hhbmdlc1xuICB3YXRjaCAgICAgICAgPSByZXF1aXJlKCcuL3Rhc2tzL3dhdGNoJyksXG5cbiAgLy8gYnVpbGQgYWxsIGZpbGVzXG4gIGJ1aWxkICAgICAgICA9IHJlcXVpcmUoJy4vdGFza3MvYnVpbGQnKSxcbiAgYnVpbGRKUyAgICAgID0gcmVxdWlyZSgnLi90YXNrcy9idWlsZC9qYXZhc2NyaXB0JyksXG4gIGJ1aWxkQ1NTICAgICA9IHJlcXVpcmUoJy4vdGFza3MvYnVpbGQvY3NzJyksXG4gIGJ1aWxkQXNzZXRzICA9IHJlcXVpcmUoJy4vdGFza3MvYnVpbGQvYXNzZXRzJyksXG5cbiAgLy8gdXRpbGl0eVxuICBjbGVhbiAgICAgICAgPSByZXF1aXJlKCcuL3Rhc2tzL2NsZWFuJyksXG4gIHZlcnNpb24gICAgICA9IHJlcXVpcmUoJy4vdGFza3MvdmVyc2lvbicpLFxuXG4gIC8vIGRvY3MgdGFza3NcbiAgc2VydmVEb2NzICAgID0gcmVxdWlyZSgnLi90YXNrcy9kb2NzL3NlcnZlJyksXG4gIGJ1aWxkRG9jcyAgICA9IHJlcXVpcmUoJy4vdGFza3MvZG9jcy9idWlsZCcpLFxuXG4gIC8vIHJ0bFxuICBidWlsZFJUTCAgICAgPSByZXF1aXJlKCcuL3Rhc2tzL3J0bC9idWlsZCcpLFxuICB3YXRjaFJUTCAgICAgPSByZXF1aXJlKCcuL3Rhc2tzL3J0bC93YXRjaCcpXG47XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAgICAgICAgICBUYXNrc1xuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuZ3VscC50YXNrKCdkZWZhdWx0JywgZmFsc2UsIFtcbiAgJ3dhdGNoJ1xuXSk7XG5cbmd1bHAudGFzaygnd2F0Y2gnLCAnV2F0Y2ggZm9yIHNpdGUvdGhlbWUgY2hhbmdlcycsIHdhdGNoKTtcblxuZ3VscC50YXNrKCdidWlsZCcsICdCdWlsZHMgYWxsIGZpbGVzIGZyb20gc291cmNlJywgYnVpbGQpO1xuZ3VscC50YXNrKCdidWlsZC1qYXZhc2NyaXB0JywgJ0J1aWxkcyBhbGwgamF2YXNjcmlwdCBmcm9tIHNvdXJjZScsIGJ1aWxkSlMpO1xuZ3VscC50YXNrKCdidWlsZC1jc3MnLCAnQnVpbGRzIGFsbCBjc3MgZnJvbSBzb3VyY2UnLCBidWlsZENTUyk7XG5ndWxwLnRhc2soJ2J1aWxkLWFzc2V0cycsICdDb3BpZXMgYWxsIGFzc2V0cyBmcm9tIHNvdXJjZScsIGJ1aWxkQXNzZXRzKTtcblxuZ3VscC50YXNrKCdjbGVhbicsICdDbGVhbiBkaXN0IGZvbGRlcicsIGNsZWFuKTtcbmd1bHAudGFzaygndmVyc2lvbicsICdEaXNwbGF5cyBjdXJyZW50IHZlcnNpb24gb2YgU2VtYW50aWMnLCB2ZXJzaW9uKTtcblxuLyotLS0tLS0tLS0tLS0tLVxuICAgICAgRG9jc1xuLS0tLS0tLS0tLS0tLS0tKi9cblxuLypcbiAgTGV0cyB5b3Ugc2VydmUgZmlsZXMgdG8gYSBsb2NhbCBkb2N1bWVudGF0aW9uIGluc3RhbmNlXG4gIGh0dHBzOi8vZ2l0aHViLmNvbS9TZW1hbnRpYy1PcmcvU2VtYW50aWMtVUktRG9jcy9cbiovXG5cbmd1bHAudGFzaygnc2VydmUtZG9jcycsICdTZXJ2ZSBmaWxlIGNoYW5nZXMgdG8gU1VJIERvY3MnLCBzZXJ2ZURvY3MpO1xuZ3VscC50YXNrKCdidWlsZC1kb2NzJywgJ0J1aWxkIGFsbCBmaWxlcyBhbmQgYWRkIHRvIFNVSSBEb2NzJywgYnVpbGREb2NzKTtcblxuXG4vKi0tLS0tLS0tLS0tLS0tXG4gICAgICBSVExcbi0tLS0tLS0tLS0tLS0tLSovXG5cbmlmKGNvbmZpZy5ydGwpIHtcbiAgZ3VscC50YXNrKCd3YXRjaC1ydGwnLCAnV2F0Y2ggZmlsZXMgYXMgUlRMJywgd2F0Y2hSVEwpO1xuICBndWxwLnRhc2soJ2J1aWxkLXJ0bCcsICdCdWlsZCBhbGwgZmlsZXMgYXMgUlRMJywgYnVpbGRSVEwpO1xufVxuIl19