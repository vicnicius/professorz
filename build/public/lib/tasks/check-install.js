/*******************************
         Check Install
*******************************/

var
// node dependencies
gulp = require('gulp'),
    fs = require('fs'),
    console = require('better-console'),
    install = require('./config/project/install');

// export task
module.exports = function () {

  setTimeout(function () {
    if (!install.isSetup()) {
      console.log('Starting install...');
      gulp.start('install');
      return;
    } else {
      gulp.start('watch');
    }
  }, 50); // Delay to allow console.clear to remove messages from check event
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NoZWNrLWluc3RhbGwuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUlBOztBQUVFLE9BQWUsUUFBUSxNQUFSLENBRmpCO0FBQUEsSUFHRSxLQUFlLFFBQVEsSUFBUixDQUhqQjtBQUFBLElBSUUsVUFBZSxRQUFRLGdCQUFSLENBSmpCO0FBQUEsSUFLRSxVQUFlLFFBQVEsMEJBQVIsQ0FMakI7OztBQVNBLE9BQU8sT0FBUCxHQUFpQixZQUFXOztBQUUxQixhQUFXLFlBQVc7QUFDcEIsUUFBSSxDQUFDLFFBQVEsT0FBUixFQUFMLEVBQXlCO0FBQ3ZCLGNBQVEsR0FBUixDQUFZLHFCQUFaO0FBQ0EsV0FBSyxLQUFMLENBQVcsU0FBWDtBQUNBO0FBQ0QsS0FKRCxNQUtLO0FBQ0gsV0FBSyxLQUFMLENBQVcsT0FBWDtBQUNEO0FBQ0YsR0FURCxFQVNHLEVBVEgsRTtBQVlELENBZEQiLCJmaWxlIjoiY2hlY2staW5zdGFsbC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICBDaGVjayBJbnN0YWxsXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG52YXJcbiAgLy8gbm9kZSBkZXBlbmRlbmNpZXNcbiAgZ3VscCAgICAgICAgID0gcmVxdWlyZSgnZ3VscCcpLFxuICBmcyAgICAgICAgICAgPSByZXF1aXJlKCdmcycpLFxuICBjb25zb2xlICAgICAgPSByZXF1aXJlKCdiZXR0ZXItY29uc29sZScpLFxuICBpbnN0YWxsICAgICAgPSByZXF1aXJlKCcuL2NvbmZpZy9wcm9qZWN0L2luc3RhbGwnKVxuO1xuXG4vLyBleHBvcnQgdGFza1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbigpIHtcblxuICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgIGlmKCAhaW5zdGFsbC5pc1NldHVwKCkgKSB7XG4gICAgICBjb25zb2xlLmxvZygnU3RhcnRpbmcgaW5zdGFsbC4uLicpO1xuICAgICAgZ3VscC5zdGFydCgnaW5zdGFsbCcpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIGd1bHAuc3RhcnQoJ3dhdGNoJyk7XG4gICAgfVxuICB9LCA1MCk7IC8vIERlbGF5IHRvIGFsbG93IGNvbnNvbGUuY2xlYXIgdG8gcmVtb3ZlIG1lc3NhZ2VzIGZyb20gY2hlY2sgZXZlbnRcblxuXG59OyJdfQ==