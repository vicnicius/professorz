/*******************************
          Build Task
*******************************/

var
// dependencies
gulp = require('gulp-help')(require('gulp')),
    runSequence = require('run-sequence'),


// config
config = require('./config/user'),
    install = require('./config/project/install'),


// task sequence
tasks = [];

// sub-tasks
if (config.rtl) {
  require('./collections/rtl')(gulp);
}
require('./collections/build')(gulp);

module.exports = function (callback) {

  console.info('Building Semantic');

  if (!install.isSetup()) {
    console.error('Cannot find semantic.json. Run "gulp install" to set-up Semantic');
    return 1;
  }

  // check for right-to-left (RTL) language
  if (config.rtl === true || config.rtl === 'Yes') {
    gulp.start('build-rtl');
    return;
  }

  if (config.rtl == 'both') {
    tasks.push('build-rtl');
  }

  tasks.push('build-javascript');
  tasks.push('build-css');
  tasks.push('build-assets');

  runSequence(tasks, callback);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2J1aWxkLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFJQTs7QUFFRSxPQUFlLFFBQVEsV0FBUixFQUFxQixRQUFRLE1BQVIsQ0FBckIsQ0FGakI7QUFBQSxJQUdFLGNBQWUsUUFBUSxjQUFSLENBSGpCO0FBQUE7OztBQU1FLFNBQWUsUUFBUSxlQUFSLENBTmpCO0FBQUEsSUFPRSxVQUFlLFFBQVEsMEJBQVIsQ0FQakI7QUFBQTs7O0FBVUUsUUFBZSxFQVZqQjs7O0FBZUEsSUFBRyxPQUFPLEdBQVYsRUFBZTtBQUNiLFVBQVEsbUJBQVIsRUFBNkIsSUFBN0I7QUFDRDtBQUNELFFBQVEscUJBQVIsRUFBK0IsSUFBL0I7O0FBR0EsT0FBTyxPQUFQLEdBQWlCLFVBQVMsUUFBVCxFQUFtQjs7QUFFbEMsVUFBUSxJQUFSLENBQWEsbUJBQWI7O0FBRUEsTUFBSSxDQUFDLFFBQVEsT0FBUixFQUFMLEVBQXlCO0FBQ3ZCLFlBQVEsS0FBUixDQUFjLGtFQUFkO0FBQ0EsV0FBTyxDQUFQO0FBQ0Q7OztBQUdELE1BQUcsT0FBTyxHQUFQLEtBQWUsSUFBZixJQUF1QixPQUFPLEdBQVAsS0FBZSxLQUF6QyxFQUFnRDtBQUM5QyxTQUFLLEtBQUwsQ0FBVyxXQUFYO0FBQ0E7QUFDRDs7QUFFRCxNQUFHLE9BQU8sR0FBUCxJQUFjLE1BQWpCLEVBQXlCO0FBQ3ZCLFVBQU0sSUFBTixDQUFXLFdBQVg7QUFDRDs7QUFFRCxRQUFNLElBQU4sQ0FBVyxrQkFBWDtBQUNBLFFBQU0sSUFBTixDQUFXLFdBQVg7QUFDQSxRQUFNLElBQU4sQ0FBVyxjQUFYOztBQUVBLGNBQVksS0FBWixFQUFtQixRQUFuQjtBQUNELENBeEJEIiwiZmlsZSI6ImJ1aWxkLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAgICAgICBCdWlsZCBUYXNrXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG52YXJcbiAgLy8gZGVwZW5kZW5jaWVzXG4gIGd1bHAgICAgICAgICA9IHJlcXVpcmUoJ2d1bHAtaGVscCcpKHJlcXVpcmUoJ2d1bHAnKSksXG4gIHJ1blNlcXVlbmNlICA9IHJlcXVpcmUoJ3J1bi1zZXF1ZW5jZScpLFxuXG4gIC8vIGNvbmZpZ1xuICBjb25maWcgICAgICAgPSByZXF1aXJlKCcuL2NvbmZpZy91c2VyJyksXG4gIGluc3RhbGwgICAgICA9IHJlcXVpcmUoJy4vY29uZmlnL3Byb2plY3QvaW5zdGFsbCcpLFxuXG4gIC8vIHRhc2sgc2VxdWVuY2VcbiAgdGFza3MgICAgICAgID0gW11cbjtcblxuXG4vLyBzdWItdGFza3NcbmlmKGNvbmZpZy5ydGwpIHtcbiAgcmVxdWlyZSgnLi9jb2xsZWN0aW9ucy9ydGwnKShndWxwKTtcbn1cbnJlcXVpcmUoJy4vY29sbGVjdGlvbnMvYnVpbGQnKShndWxwKTtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG5cbiAgY29uc29sZS5pbmZvKCdCdWlsZGluZyBTZW1hbnRpYycpO1xuXG4gIGlmKCAhaW5zdGFsbC5pc1NldHVwKCkgKSB7XG4gICAgY29uc29sZS5lcnJvcignQ2Fubm90IGZpbmQgc2VtYW50aWMuanNvbi4gUnVuIFwiZ3VscCBpbnN0YWxsXCIgdG8gc2V0LXVwIFNlbWFudGljJyk7XG4gICAgcmV0dXJuIDE7XG4gIH1cblxuICAvLyBjaGVjayBmb3IgcmlnaHQtdG8tbGVmdCAoUlRMKSBsYW5ndWFnZVxuICBpZihjb25maWcucnRsID09PSB0cnVlIHx8IGNvbmZpZy5ydGwgPT09ICdZZXMnKSB7XG4gICAgZ3VscC5zdGFydCgnYnVpbGQtcnRsJyk7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgaWYoY29uZmlnLnJ0bCA9PSAnYm90aCcpIHtcbiAgICB0YXNrcy5wdXNoKCdidWlsZC1ydGwnKTtcbiAgfVxuXG4gIHRhc2tzLnB1c2goJ2J1aWxkLWphdmFzY3JpcHQnKTtcbiAgdGFza3MucHVzaCgnYnVpbGQtY3NzJyk7XG4gIHRhc2tzLnB1c2goJ2J1aWxkLWFzc2V0cycpO1xuXG4gIHJ1blNlcXVlbmNlKHRhc2tzLCBjYWxsYmFjayk7XG59O1xuIl19