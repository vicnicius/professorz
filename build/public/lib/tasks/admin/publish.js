/*******************************
          Release All
*******************************/

/*
 This task update all SUI individual component repos with new versions of components

  * Commits changes from create components to GitHub and Tags

*/

var runSequence = require('run-sequence');

/* Release All */
module.exports = function (callback) {

  runSequence('update distributions', // commit less/css versions to github
  'update components', // commit components to github
  callback);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2FkbWluL3B1Ymxpc2guanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFXQSxJQUNFLGNBQWMsUUFBUSxjQUFSLENBRGhCOzs7QUFLQSxPQUFPLE9BQVAsR0FBaUIsVUFBUyxRQUFULEVBQW1COztBQUVsQyxjQUNFLHNCQURGLEU7QUFFRSxxQkFGRixFO0FBR0UsVUFIRjtBQU1ELENBUkQiLCJmaWxlIjoicHVibGlzaC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICAgUmVsZWFzZSBBbGxcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi8qXG4gVGhpcyB0YXNrIHVwZGF0ZSBhbGwgU1VJIGluZGl2aWR1YWwgY29tcG9uZW50IHJlcG9zIHdpdGggbmV3IHZlcnNpb25zIG9mIGNvbXBvbmVudHNcblxuICAqIENvbW1pdHMgY2hhbmdlcyBmcm9tIGNyZWF0ZSBjb21wb25lbnRzIHRvIEdpdEh1YiBhbmQgVGFnc1xuXG4qL1xuXG52YXJcbiAgcnVuU2VxdWVuY2UgPSByZXF1aXJlKCdydW4tc2VxdWVuY2UnKVxuO1xuXG4vKiBSZWxlYXNlIEFsbCAqL1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihjYWxsYmFjaykge1xuXG4gIHJ1blNlcXVlbmNlKFxuICAgICd1cGRhdGUgZGlzdHJpYnV0aW9ucycsIC8vIGNvbW1pdCBsZXNzL2NzcyB2ZXJzaW9ucyB0byBnaXRodWJcbiAgICAndXBkYXRlIGNvbXBvbmVudHMnLCAvLyBjb21taXQgY29tcG9uZW50cyB0byBnaXRodWJcbiAgICBjYWxsYmFja1xuICApO1xuXG59OyJdfQ==