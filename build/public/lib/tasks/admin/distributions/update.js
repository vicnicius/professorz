/*******************************
          Update Repos
*******************************/

/*

 This task update all SUI individual distribution repos with new versions of distributions

  * Commits changes from create repo
  * Pushes changes to GitHub
  * Tag new releases if version changed in main repo

*/

var gulp = require('gulp'),


// node dependencies
console = require('better-console'),
    fs = require('fs'),
    path = require('path'),
    git = require('gulp-git'),
    githubAPI = require('github'),
    requireDotFile = require('require-dot-file'),


// admin files
github = require('../../config/admin/github.js'),
    release = require('../../config/admin/release'),
    project = require('../../config/project/release'),


// oAuth configuration for GitHub
oAuth = fs.existsSync(__dirname + '/../../config/admin/oauth.js') ? require('../../config/admin/oauth') : false,


// shorthand
version = project.version;

module.exports = function (callback) {

  var index = -1,
      total = release.distributions.length,
      timer,
      stream,
      stepRepo;

  if (!oAuth) {
    console.error('Must add oauth token for GitHub in tasks/config/admin/oauth.js');
    return;
  }

  // Do Git commands synchronously per distribution, to avoid issues
  stepRepo = function () {

    index = index + 1;
    if (index >= total) {
      callback();
      return;
    }

    var distribution = release.distributions[index],
        outputDirectory = path.resolve(path.join(release.outputRoot, distribution.toLowerCase())),
        repoName = release.distRepoRoot + distribution,
        commitArgs = oAuth.name !== undefined && oAuth.email !== undefined ? '--author "' + oAuth.name + ' <' + oAuth.email + '>"' : '',
        distributionPackage = fs.existsSync(outputDirectory + 'package.json') ? require(outputDirectory + 'package.json') : false,
        isNewVersion = version && distributionPackage.version != version,
        commitMessage = isNewVersion ? 'Updated distribution to version ' + version : 'Updated files from main repo',
        gitOptions = { cwd: outputDirectory },
        commitOptions = { args: commitArgs, cwd: outputDirectory },
        releaseOptions = { tag_name: version, owner: release.org, repo: repoName },
        fileModeOptions = { args: 'config core.fileMode false', cwd: outputDirectory },
        usernameOptions = { args: 'config user.name "' + oAuth.name + '"', cwd: outputDirectory },
        emailOptions = { args: 'config user.email "' + oAuth.email + '"', cwd: outputDirectory },
        versionOptions = { args: 'rev-parse --verify HEAD', cwd: outputDirectory },
        localRepoSetup = fs.existsSync(path.join(outputDirectory, '.git')),
        canProceed = true;

    console.info('Processing repository:' + outputDirectory);

    function setConfig() {
      git.exec(fileModeOptions, function () {
        git.exec(usernameOptions, function () {
          git.exec(emailOptions, function () {
            commitFiles();
          });
        });
      });
    }

    // standard path
    function commitFiles() {
      // commit files
      console.info('Committing ' + distribution + ' files', commitArgs);
      gulp.src('./', gitOptions).pipe(git.add(gitOptions)).pipe(git.commit(commitMessage, commitOptions)).on('error', function (error) {
        // canProceed = false; bug in git commit <https://github.com/stevelacy/gulp-git/issues/49>
      }).on('finish', function (callback) {
        if (canProceed) {
          pushFiles();
        } else {
          console.info('Nothing new to commit');
          nextRepo();
        }
      });
    }

    // push changes to remote
    function pushFiles() {
      console.info('Pushing files for ' + distribution);
      git.push('origin', 'master', { args: '', cwd: outputDirectory }, function (error) {
        console.info('Push completed successfully');
        getSHA();
      });
    }

    // gets SHA of last commit
    function getSHA() {
      git.exec(versionOptions, function (error, version) {
        version = version.trim();
        createRelease(version);
      });
    }

    // create release on GitHub.com
    function createRelease(version) {
      if (version) {
        releaseOptions.target_commitish = version;
      }
      github.releases.createRelease(releaseOptions, function () {
        nextRepo();
      });
    }

    // Steps to next repository
    function nextRepo() {
      console.log('Sleeping for 1 second...');
      // avoid rate throttling
      global.clearTimeout(timer);
      timer = global.setTimeout(stepRepo, 500);
    }

    if (localRepoSetup) {
      setConfig();
    } else {
      console.error('Repository must be setup before running update distributions');
    }
  };

  stepRepo();
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2FkbWluL2Rpc3RyaWJ1dGlvbnMvdXBkYXRlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBY0EsSUFDRSxPQUFpQixRQUFRLE1BQVIsQ0FEbkI7QUFBQTs7O0FBSUUsVUFBaUIsUUFBUSxnQkFBUixDQUpuQjtBQUFBLElBS0UsS0FBaUIsUUFBUSxJQUFSLENBTG5CO0FBQUEsSUFNRSxPQUFpQixRQUFRLE1BQVIsQ0FObkI7QUFBQSxJQU9FLE1BQWlCLFFBQVEsVUFBUixDQVBuQjtBQUFBLElBUUUsWUFBaUIsUUFBUSxRQUFSLENBUm5CO0FBQUEsSUFTRSxpQkFBaUIsUUFBUSxrQkFBUixDQVRuQjtBQUFBOzs7QUFZRSxTQUFpQixRQUFRLDhCQUFSLENBWm5CO0FBQUEsSUFhRSxVQUFpQixRQUFRLDRCQUFSLENBYm5CO0FBQUEsSUFjRSxVQUFpQixRQUFRLDhCQUFSLENBZG5CO0FBQUE7OztBQWtCRSxRQUFpQixHQUFHLFVBQUgsQ0FBYyxZQUFZLDhCQUExQixJQUNiLFFBQVEsMEJBQVIsQ0FEYSxHQUViLEtBcEJOO0FBQUE7OztBQXVCRSxVQUFVLFFBQVEsT0F2QnBCOztBQTBCQSxPQUFPLE9BQVAsR0FBaUIsVUFBUyxRQUFULEVBQW1COztBQUVsQyxNQUNFLFFBQVEsQ0FBQyxDQURYO0FBQUEsTUFFRSxRQUFRLFFBQVEsYUFBUixDQUFzQixNQUZoQztBQUFBLE1BR0UsS0FIRjtBQUFBLE1BSUUsTUFKRjtBQUFBLE1BS0UsUUFMRjs7QUFRQSxNQUFHLENBQUMsS0FBSixFQUFXO0FBQ1QsWUFBUSxLQUFSLENBQWMsZ0VBQWQ7QUFDQTtBQUNEOzs7QUFHRCxhQUFXLFlBQVc7O0FBRXBCLFlBQVEsUUFBUSxDQUFoQjtBQUNBLFFBQUcsU0FBUyxLQUFaLEVBQW1CO0FBQ2pCO0FBQ0E7QUFDRDs7QUFFRCxRQUNFLGVBQXVCLFFBQVEsYUFBUixDQUFzQixLQUF0QixDQUR6QjtBQUFBLFFBRUUsa0JBQXVCLEtBQUssT0FBTCxDQUFhLEtBQUssSUFBTCxDQUFVLFFBQVEsVUFBbEIsRUFBOEIsYUFBYSxXQUFiLEVBQTlCLENBQWIsQ0FGekI7QUFBQSxRQUdFLFdBQXVCLFFBQVEsWUFBUixHQUF1QixZQUhoRDtBQUFBLFFBS0UsYUFBYyxNQUFNLElBQU4sS0FBZSxTQUFmLElBQTRCLE1BQU0sS0FBTixLQUFnQixTQUE3QyxHQUNULGVBQWUsTUFBTSxJQUFyQixHQUE0QixJQUE1QixHQUFtQyxNQUFNLEtBQXpDLEdBQWlELElBRHhDLEdBRVQsRUFQTjtBQUFBLFFBU0Usc0JBQXNCLEdBQUcsVUFBSCxDQUFjLGtCQUFrQixjQUFoQyxJQUNsQixRQUFRLGtCQUFrQixjQUExQixDQURrQixHQUVsQixLQVhOO0FBQUEsUUFhRSxlQUFpQixXQUFXLG9CQUFvQixPQUFwQixJQUErQixPQWI3RDtBQUFBLFFBZUUsZ0JBQWlCLFlBQUQsR0FDWixxQ0FBcUMsT0FEekIsR0FFWiw4QkFqQk47QUFBQSxRQW1CRSxhQUFrQixFQUFFLEtBQUssZUFBUCxFQW5CcEI7QUFBQSxRQW9CRSxnQkFBa0IsRUFBRSxNQUFNLFVBQVIsRUFBb0IsS0FBSyxlQUF6QixFQXBCcEI7QUFBQSxRQXFCRSxpQkFBa0IsRUFBRSxVQUFVLE9BQVosRUFBcUIsT0FBTyxRQUFRLEdBQXBDLEVBQXlDLE1BQU0sUUFBL0MsRUFyQnBCO0FBQUEsUUF1QkUsa0JBQWtCLEVBQUUsTUFBTyw0QkFBVCxFQUF1QyxLQUFLLGVBQTVDLEVBdkJwQjtBQUFBLFFBd0JFLGtCQUFrQixFQUFFLE1BQU8sdUJBQXVCLE1BQU0sSUFBN0IsR0FBb0MsR0FBN0MsRUFBa0QsS0FBSyxlQUF2RCxFQXhCcEI7QUFBQSxRQXlCRSxlQUFrQixFQUFFLE1BQU8sd0JBQXdCLE1BQU0sS0FBOUIsR0FBc0MsR0FBL0MsRUFBb0QsS0FBSyxlQUF6RCxFQXpCcEI7QUFBQSxRQTBCRSxpQkFBa0IsRUFBRSxNQUFPLHlCQUFULEVBQW9DLEtBQUssZUFBekMsRUExQnBCO0FBQUEsUUE0QkUsaUJBQWtCLEdBQUcsVUFBSCxDQUFjLEtBQUssSUFBTCxDQUFVLGVBQVYsRUFBMkIsTUFBM0IsQ0FBZCxDQTVCcEI7QUFBQSxRQTZCRSxhQUFrQixJQTdCcEI7O0FBaUNBLFlBQVEsSUFBUixDQUFhLDJCQUEyQixlQUF4Qzs7QUFFQSxhQUFTLFNBQVQsR0FBcUI7QUFDbkIsVUFBSSxJQUFKLENBQVMsZUFBVCxFQUEwQixZQUFXO0FBQ25DLFlBQUksSUFBSixDQUFTLGVBQVQsRUFBMEIsWUFBWTtBQUNwQyxjQUFJLElBQUosQ0FBUyxZQUFULEVBQXVCLFlBQVk7QUFDakM7QUFDRCxXQUZEO0FBR0QsU0FKRDtBQUtELE9BTkQ7QUFPRDs7O0FBR0QsYUFBUyxXQUFULEdBQXVCOztBQUVyQixjQUFRLElBQVIsQ0FBYSxnQkFBZ0IsWUFBaEIsR0FBK0IsUUFBNUMsRUFBc0QsVUFBdEQ7QUFDQSxXQUFLLEdBQUwsQ0FBUyxJQUFULEVBQWUsVUFBZixFQUNHLElBREgsQ0FDUSxJQUFJLEdBQUosQ0FBUSxVQUFSLENBRFIsRUFFRyxJQUZILENBRVEsSUFBSSxNQUFKLENBQVcsYUFBWCxFQUEwQixhQUExQixDQUZSLEVBR0csRUFISCxDQUdNLE9BSE4sRUFHZSxVQUFTLEtBQVQsRUFBZ0I7O0FBRTVCLE9BTEgsRUFNRyxFQU5ILENBTU0sUUFOTixFQU1nQixVQUFTLFFBQVQsRUFBbUI7QUFDL0IsWUFBRyxVQUFILEVBQWU7QUFDYjtBQUNELFNBRkQsTUFHSztBQUNILGtCQUFRLElBQVIsQ0FBYSx1QkFBYjtBQUNBO0FBQ0Q7QUFDRixPQWRIO0FBZ0JEOzs7QUFHRCxhQUFTLFNBQVQsR0FBcUI7QUFDbkIsY0FBUSxJQUFSLENBQWEsdUJBQXVCLFlBQXBDO0FBQ0EsVUFBSSxJQUFKLENBQVMsUUFBVCxFQUFtQixRQUFuQixFQUE2QixFQUFFLE1BQU0sRUFBUixFQUFZLEtBQUssZUFBakIsRUFBN0IsRUFBaUUsVUFBUyxLQUFULEVBQWdCO0FBQy9FLGdCQUFRLElBQVIsQ0FBYSw2QkFBYjtBQUNBO0FBQ0QsT0FIRDtBQUlEOzs7QUFHRCxhQUFTLE1BQVQsR0FBa0I7QUFDaEIsVUFBSSxJQUFKLENBQVMsY0FBVCxFQUF5QixVQUFTLEtBQVQsRUFBZ0IsT0FBaEIsRUFBeUI7QUFDaEQsa0JBQVUsUUFBUSxJQUFSLEVBQVY7QUFDQSxzQkFBYyxPQUFkO0FBQ0QsT0FIRDtBQUlEOzs7QUFHRCxhQUFTLGFBQVQsQ0FBdUIsT0FBdkIsRUFBZ0M7QUFDOUIsVUFBRyxPQUFILEVBQVk7QUFDVix1QkFBZSxnQkFBZixHQUFrQyxPQUFsQztBQUNEO0FBQ0QsYUFBTyxRQUFQLENBQWdCLGFBQWhCLENBQThCLGNBQTlCLEVBQThDLFlBQVc7QUFDdkQ7QUFDRCxPQUZEO0FBR0Q7OztBQUdELGFBQVMsUUFBVCxHQUFvQjtBQUNsQixjQUFRLEdBQVIsQ0FBWSwwQkFBWjs7QUFFQSxhQUFPLFlBQVAsQ0FBb0IsS0FBcEI7QUFDQSxjQUFRLE9BQU8sVUFBUCxDQUFrQixRQUFsQixFQUE0QixHQUE1QixDQUFSO0FBQ0Q7O0FBR0QsUUFBRyxjQUFILEVBQW1CO0FBQ2pCO0FBQ0QsS0FGRCxNQUdLO0FBQ0gsY0FBUSxLQUFSLENBQWMsOERBQWQ7QUFDRDtBQUVGLEdBdEhEOztBQXdIQTtBQUVELENBMUlEIiwiZmlsZSI6InVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICAgVXBkYXRlIFJlcG9zXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4vKlxuXG4gVGhpcyB0YXNrIHVwZGF0ZSBhbGwgU1VJIGluZGl2aWR1YWwgZGlzdHJpYnV0aW9uIHJlcG9zIHdpdGggbmV3IHZlcnNpb25zIG9mIGRpc3RyaWJ1dGlvbnNcblxuICAqIENvbW1pdHMgY2hhbmdlcyBmcm9tIGNyZWF0ZSByZXBvXG4gICogUHVzaGVzIGNoYW5nZXMgdG8gR2l0SHViXG4gICogVGFnIG5ldyByZWxlYXNlcyBpZiB2ZXJzaW9uIGNoYW5nZWQgaW4gbWFpbiByZXBvXG5cbiovXG5cbnZhclxuICBndWxwICAgICAgICAgICA9IHJlcXVpcmUoJ2d1bHAnKSxcblxuICAvLyBub2RlIGRlcGVuZGVuY2llc1xuICBjb25zb2xlICAgICAgICA9IHJlcXVpcmUoJ2JldHRlci1jb25zb2xlJyksXG4gIGZzICAgICAgICAgICAgID0gcmVxdWlyZSgnZnMnKSxcbiAgcGF0aCAgICAgICAgICAgPSByZXF1aXJlKCdwYXRoJyksXG4gIGdpdCAgICAgICAgICAgID0gcmVxdWlyZSgnZ3VscC1naXQnKSxcbiAgZ2l0aHViQVBJICAgICAgPSByZXF1aXJlKCdnaXRodWInKSxcbiAgcmVxdWlyZURvdEZpbGUgPSByZXF1aXJlKCdyZXF1aXJlLWRvdC1maWxlJyksXG5cbiAgLy8gYWRtaW4gZmlsZXNcbiAgZ2l0aHViICAgICAgICAgPSByZXF1aXJlKCcuLi8uLi9jb25maWcvYWRtaW4vZ2l0aHViLmpzJyksXG4gIHJlbGVhc2UgICAgICAgID0gcmVxdWlyZSgnLi4vLi4vY29uZmlnL2FkbWluL3JlbGVhc2UnKSxcbiAgcHJvamVjdCAgICAgICAgPSByZXF1aXJlKCcuLi8uLi9jb25maWcvcHJvamVjdC9yZWxlYXNlJyksXG5cblxuICAvLyBvQXV0aCBjb25maWd1cmF0aW9uIGZvciBHaXRIdWJcbiAgb0F1dGggICAgICAgICAgPSBmcy5leGlzdHNTeW5jKF9fZGlybmFtZSArICcvLi4vLi4vY29uZmlnL2FkbWluL29hdXRoLmpzJylcbiAgICA/IHJlcXVpcmUoJy4uLy4uL2NvbmZpZy9hZG1pbi9vYXV0aCcpXG4gICAgOiBmYWxzZSxcblxuICAvLyBzaG9ydGhhbmRcbiAgdmVyc2lvbiA9IHByb2plY3QudmVyc2lvblxuO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG5cbiAgdmFyXG4gICAgaW5kZXggPSAtMSxcbiAgICB0b3RhbCA9IHJlbGVhc2UuZGlzdHJpYnV0aW9ucy5sZW5ndGgsXG4gICAgdGltZXIsXG4gICAgc3RyZWFtLFxuICAgIHN0ZXBSZXBvXG4gIDtcblxuICBpZighb0F1dGgpIHtcbiAgICBjb25zb2xlLmVycm9yKCdNdXN0IGFkZCBvYXV0aCB0b2tlbiBmb3IgR2l0SHViIGluIHRhc2tzL2NvbmZpZy9hZG1pbi9vYXV0aC5qcycpO1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8vIERvIEdpdCBjb21tYW5kcyBzeW5jaHJvbm91c2x5IHBlciBkaXN0cmlidXRpb24sIHRvIGF2b2lkIGlzc3Vlc1xuICBzdGVwUmVwbyA9IGZ1bmN0aW9uKCkge1xuXG4gICAgaW5kZXggPSBpbmRleCArIDE7XG4gICAgaWYoaW5kZXggPj0gdG90YWwpIHtcbiAgICAgIGNhbGxiYWNrKCk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyXG4gICAgICBkaXN0cmlidXRpb24gICAgICAgICA9IHJlbGVhc2UuZGlzdHJpYnV0aW9uc1tpbmRleF0sXG4gICAgICBvdXRwdXREaXJlY3RvcnkgICAgICA9IHBhdGgucmVzb2x2ZShwYXRoLmpvaW4ocmVsZWFzZS5vdXRwdXRSb290LCBkaXN0cmlidXRpb24udG9Mb3dlckNhc2UoKSApKSxcbiAgICAgIHJlcG9OYW1lICAgICAgICAgICAgID0gcmVsZWFzZS5kaXN0UmVwb1Jvb3QgKyBkaXN0cmlidXRpb24sXG5cbiAgICAgIGNvbW1pdEFyZ3MgPSAob0F1dGgubmFtZSAhPT0gdW5kZWZpbmVkICYmIG9BdXRoLmVtYWlsICE9PSB1bmRlZmluZWQpXG4gICAgICAgID8gJy0tYXV0aG9yIFwiJyArIG9BdXRoLm5hbWUgKyAnIDwnICsgb0F1dGguZW1haWwgKyAnPlwiJ1xuICAgICAgICA6ICcnLFxuXG4gICAgICBkaXN0cmlidXRpb25QYWNrYWdlID0gZnMuZXhpc3RzU3luYyhvdXRwdXREaXJlY3RvcnkgKyAncGFja2FnZS5qc29uJyApXG4gICAgICAgID8gcmVxdWlyZShvdXRwdXREaXJlY3RvcnkgKyAncGFja2FnZS5qc29uJylcbiAgICAgICAgOiBmYWxzZSxcblxuICAgICAgaXNOZXdWZXJzaW9uICA9ICh2ZXJzaW9uICYmIGRpc3RyaWJ1dGlvblBhY2thZ2UudmVyc2lvbiAhPSB2ZXJzaW9uKSxcblxuICAgICAgY29tbWl0TWVzc2FnZSA9IChpc05ld1ZlcnNpb24pXG4gICAgICAgID8gJ1VwZGF0ZWQgZGlzdHJpYnV0aW9uIHRvIHZlcnNpb24gJyArIHZlcnNpb25cbiAgICAgICAgOiAnVXBkYXRlZCBmaWxlcyBmcm9tIG1haW4gcmVwbycsXG5cbiAgICAgIGdpdE9wdGlvbnMgICAgICA9IHsgY3dkOiBvdXRwdXREaXJlY3RvcnkgfSxcbiAgICAgIGNvbW1pdE9wdGlvbnMgICA9IHsgYXJnczogY29tbWl0QXJncywgY3dkOiBvdXRwdXREaXJlY3RvcnkgfSxcbiAgICAgIHJlbGVhc2VPcHRpb25zICA9IHsgdGFnX25hbWU6IHZlcnNpb24sIG93bmVyOiByZWxlYXNlLm9yZywgcmVwbzogcmVwb05hbWUgfSxcblxuICAgICAgZmlsZU1vZGVPcHRpb25zID0geyBhcmdzIDogJ2NvbmZpZyBjb3JlLmZpbGVNb2RlIGZhbHNlJywgY3dkOiBvdXRwdXREaXJlY3RvcnkgfSxcbiAgICAgIHVzZXJuYW1lT3B0aW9ucyA9IHsgYXJncyA6ICdjb25maWcgdXNlci5uYW1lIFwiJyArIG9BdXRoLm5hbWUgKyAnXCInLCBjd2Q6IG91dHB1dERpcmVjdG9yeSB9LFxuICAgICAgZW1haWxPcHRpb25zICAgID0geyBhcmdzIDogJ2NvbmZpZyB1c2VyLmVtYWlsIFwiJyArIG9BdXRoLmVtYWlsICsgJ1wiJywgY3dkOiBvdXRwdXREaXJlY3RvcnkgfSxcbiAgICAgIHZlcnNpb25PcHRpb25zID0gIHsgYXJncyA6ICdyZXYtcGFyc2UgLS12ZXJpZnkgSEVBRCcsIGN3ZDogb3V0cHV0RGlyZWN0b3J5IH0sXG5cbiAgICAgIGxvY2FsUmVwb1NldHVwICA9IGZzLmV4aXN0c1N5bmMocGF0aC5qb2luKG91dHB1dERpcmVjdG9yeSwgJy5naXQnKSksXG4gICAgICBjYW5Qcm9jZWVkICAgICAgPSB0cnVlXG4gICAgO1xuXG5cbiAgICBjb25zb2xlLmluZm8oJ1Byb2Nlc3NpbmcgcmVwb3NpdG9yeTonICsgb3V0cHV0RGlyZWN0b3J5KTtcblxuICAgIGZ1bmN0aW9uIHNldENvbmZpZygpIHtcbiAgICAgIGdpdC5leGVjKGZpbGVNb2RlT3B0aW9ucywgZnVuY3Rpb24oKSB7XG4gICAgICAgIGdpdC5leGVjKHVzZXJuYW1lT3B0aW9ucywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgIGdpdC5leGVjKGVtYWlsT3B0aW9ucywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgY29tbWl0RmlsZXMoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICAvLyBzdGFuZGFyZCBwYXRoXG4gICAgZnVuY3Rpb24gY29tbWl0RmlsZXMoKSB7XG4gICAgICAvLyBjb21taXQgZmlsZXNcbiAgICAgIGNvbnNvbGUuaW5mbygnQ29tbWl0dGluZyAnICsgZGlzdHJpYnV0aW9uICsgJyBmaWxlcycsIGNvbW1pdEFyZ3MpO1xuICAgICAgZ3VscC5zcmMoJy4vJywgZ2l0T3B0aW9ucylcbiAgICAgICAgLnBpcGUoZ2l0LmFkZChnaXRPcHRpb25zKSlcbiAgICAgICAgLnBpcGUoZ2l0LmNvbW1pdChjb21taXRNZXNzYWdlLCBjb21taXRPcHRpb25zKSlcbiAgICAgICAgLm9uKCdlcnJvcicsIGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAgICAgLy8gY2FuUHJvY2VlZCA9IGZhbHNlOyBidWcgaW4gZ2l0IGNvbW1pdCA8aHR0cHM6Ly9naXRodWIuY29tL3N0ZXZlbGFjeS9ndWxwLWdpdC9pc3N1ZXMvNDk+XG4gICAgICAgIH0pXG4gICAgICAgIC5vbignZmluaXNoJywgZnVuY3Rpb24oY2FsbGJhY2spIHtcbiAgICAgICAgICBpZihjYW5Qcm9jZWVkKSB7XG4gICAgICAgICAgICBwdXNoRmlsZXMoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBjb25zb2xlLmluZm8oJ05vdGhpbmcgbmV3IHRvIGNvbW1pdCcpO1xuICAgICAgICAgICAgbmV4dFJlcG8oKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgICA7XG4gICAgfVxuXG4gICAgLy8gcHVzaCBjaGFuZ2VzIHRvIHJlbW90ZVxuICAgIGZ1bmN0aW9uIHB1c2hGaWxlcygpIHtcbiAgICAgIGNvbnNvbGUuaW5mbygnUHVzaGluZyBmaWxlcyBmb3IgJyArIGRpc3RyaWJ1dGlvbik7XG4gICAgICBnaXQucHVzaCgnb3JpZ2luJywgJ21hc3RlcicsIHsgYXJnczogJycsIGN3ZDogb3V0cHV0RGlyZWN0b3J5IH0sIGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAgIGNvbnNvbGUuaW5mbygnUHVzaCBjb21wbGV0ZWQgc3VjY2Vzc2Z1bGx5Jyk7XG4gICAgICAgIGdldFNIQSgpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLy8gZ2V0cyBTSEEgb2YgbGFzdCBjb21taXRcbiAgICBmdW5jdGlvbiBnZXRTSEEoKSB7XG4gICAgICBnaXQuZXhlYyh2ZXJzaW9uT3B0aW9ucywgZnVuY3Rpb24oZXJyb3IsIHZlcnNpb24pIHtcbiAgICAgICAgdmVyc2lvbiA9IHZlcnNpb24udHJpbSgpO1xuICAgICAgICBjcmVhdGVSZWxlYXNlKHZlcnNpb24pO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLy8gY3JlYXRlIHJlbGVhc2Ugb24gR2l0SHViLmNvbVxuICAgIGZ1bmN0aW9uIGNyZWF0ZVJlbGVhc2UodmVyc2lvbikge1xuICAgICAgaWYodmVyc2lvbikge1xuICAgICAgICByZWxlYXNlT3B0aW9ucy50YXJnZXRfY29tbWl0aXNoID0gdmVyc2lvbjtcbiAgICAgIH1cbiAgICAgIGdpdGh1Yi5yZWxlYXNlcy5jcmVhdGVSZWxlYXNlKHJlbGVhc2VPcHRpb25zLCBmdW5jdGlvbigpIHtcbiAgICAgICAgbmV4dFJlcG8oKTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIC8vIFN0ZXBzIHRvIG5leHQgcmVwb3NpdG9yeVxuICAgIGZ1bmN0aW9uIG5leHRSZXBvKCkge1xuICAgICAgY29uc29sZS5sb2coJ1NsZWVwaW5nIGZvciAxIHNlY29uZC4uLicpO1xuICAgICAgLy8gYXZvaWQgcmF0ZSB0aHJvdHRsaW5nXG4gICAgICBnbG9iYWwuY2xlYXJUaW1lb3V0KHRpbWVyKTtcbiAgICAgIHRpbWVyID0gZ2xvYmFsLnNldFRpbWVvdXQoc3RlcFJlcG8sIDUwMCk7XG4gICAgfVxuXG5cbiAgICBpZihsb2NhbFJlcG9TZXR1cCkge1xuICAgICAgc2V0Q29uZmlnKCk7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgY29uc29sZS5lcnJvcignUmVwb3NpdG9yeSBtdXN0IGJlIHNldHVwIGJlZm9yZSBydW5uaW5nIHVwZGF0ZSBkaXN0cmlidXRpb25zJyk7XG4gICAgfVxuXG4gIH07XG5cbiAgc3RlcFJlcG8oKTtcblxufTtcbiJdfQ==