/*******************************
        Init Dist Repos
*******************************/

/*

 This task pulls the latest version of distribution from GitHub

  * Creates new repo if doesnt exist (locally & GitHub)
  * Adds remote it doesnt exists
  * Pulls latest changes from repo

*/

var gulp = require('gulp'),


// node dependencies
console = require('better-console'),
    del = require('del'),
    fs = require('fs'),
    path = require('path'),
    git = require('gulp-git'),
    githubAPI = require('github'),
    mkdirp = require('mkdirp'),


// admin files
github = require('../../config/admin/github.js'),
    release = require('../../config/admin/release'),
    project = require('../../config/project/release'),


// oAuth configuration for GitHub
oAuth = fs.existsSync(__dirname + '/../../config/admin/oauth.js') ? require('../../config/admin/oauth') : false,


// shorthand
version = project.version;

module.exports = function (callback) {

  var index = -1,
      total = release.distributions.length,
      timer,
      stream,
      stepRepo;

  if (!oAuth) {
    console.error('Must add oauth token for GitHub in tasks/config/admin/oauth.js');
    return;
  }

  // Do Git commands synchronously per component, to avoid issues
  stepRepo = function () {

    index = index + 1;

    if (index >= total) {
      callback();
      return;
    }

    var component = release.distributions[index],
        lowerCaseComponent = component.toLowerCase(),
        outputDirectory = path.resolve(release.outputRoot + lowerCaseComponent),
        repoName = release.distRepoRoot + component,
        gitOptions = { cwd: outputDirectory },
        pullOptions = { args: '-q', cwd: outputDirectory, quiet: true },
        resetOptions = { args: '-q --hard', cwd: outputDirectory, quiet: true },
        gitURL = 'git@github.com:' + release.org + '/' + repoName + '.git',
        repoURL = 'https://github.com/' + release.org + '/' + repoName + '/',
        localRepoSetup = fs.existsSync(path.join(outputDirectory, '.git'));

    console.log('Processing repository: ' + outputDirectory);

    // create folder if doesn't exist
    if (!fs.existsSync(outputDirectory)) {
      mkdirp.sync(outputDirectory);
    }

    // clean folder
    if (release.outputRoot.search('../repos') == 0) {
      console.info('Cleaning dir', outputDirectory);
      del.sync([outputDirectory + '**/*'], { silent: true, force: true });
    }

    // set-up local repo
    function setupRepo() {
      if (localRepoSetup) {
        addRemote();
      } else {
        initRepo();
      }
    }

    function initRepo() {
      console.info('Initializing repository for ' + component);
      git.init(gitOptions, function (error) {
        if (error) {
          console.error('Error initializing repo', error);
        }
        addRemote();
      });
    }

    function createRepo() {
      console.info('Creating GitHub repo ' + repoURL);
      github.repos.createFromOrg({
        org: release.org,
        name: repoName,
        homepage: release.homepage
      }, function () {
        setupRepo();
      });
    }

    function addRemote() {
      console.info('Adding remote origin as ' + gitURL);
      git.addRemote('origin', gitURL, gitOptions, function () {
        pullFiles();
      });
    }

    function pullFiles() {
      console.info('Pulling ' + component + ' files');
      git.pull('origin', 'master', pullOptions, function (error) {
        resetFiles();
      });
    }

    function resetFiles() {
      console.info('Resetting files to head');
      git.reset('HEAD', resetOptions, function (error) {
        nextRepo();
      });
    }

    function nextRepo() {
      //console.log('Sleeping for 1 second...');
      // avoid rate throttling
      global.clearTimeout(timer);
      timer = global.setTimeout(function () {
        stepRepo();
      }, 0);
    }

    if (localRepoSetup) {
      pullFiles();
    } else {
      setupRepo();
      // createRepo() only use to create remote repo (easier to do manually)
    }
  };

  stepRepo();
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2FkbWluL2Rpc3RyaWJ1dGlvbnMvaW5pdC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQWNBLElBQ0UsT0FBWSxRQUFRLE1BQVIsQ0FEZDtBQUFBOzs7QUFJRSxVQUFZLFFBQVEsZ0JBQVIsQ0FKZDtBQUFBLElBS0UsTUFBWSxRQUFRLEtBQVIsQ0FMZDtBQUFBLElBTUUsS0FBWSxRQUFRLElBQVIsQ0FOZDtBQUFBLElBT0UsT0FBWSxRQUFRLE1BQVIsQ0FQZDtBQUFBLElBUUUsTUFBWSxRQUFRLFVBQVIsQ0FSZDtBQUFBLElBU0UsWUFBWSxRQUFRLFFBQVIsQ0FUZDtBQUFBLElBVUUsU0FBWSxRQUFRLFFBQVIsQ0FWZDtBQUFBOzs7QUFhRSxTQUFZLFFBQVEsOEJBQVIsQ0FiZDtBQUFBLElBY0UsVUFBWSxRQUFRLDRCQUFSLENBZGQ7QUFBQSxJQWVFLFVBQVksUUFBUSw4QkFBUixDQWZkO0FBQUE7OztBQW1CRSxRQUFZLEdBQUcsVUFBSCxDQUFjLFlBQVksOEJBQTFCLElBQ1IsUUFBUSwwQkFBUixDQURRLEdBRVIsS0FyQk47QUFBQTs7O0FBd0JFLFVBQVUsUUFBUSxPQXhCcEI7O0FBMkJBLE9BQU8sT0FBUCxHQUFpQixVQUFTLFFBQVQsRUFBbUI7O0FBRWxDLE1BQ0UsUUFBUSxDQUFDLENBRFg7QUFBQSxNQUVFLFFBQVEsUUFBUSxhQUFSLENBQXNCLE1BRmhDO0FBQUEsTUFHRSxLQUhGO0FBQUEsTUFJRSxNQUpGO0FBQUEsTUFLRSxRQUxGOztBQVFBLE1BQUcsQ0FBQyxLQUFKLEVBQVc7QUFDVCxZQUFRLEtBQVIsQ0FBYyxnRUFBZDtBQUNBO0FBQ0Q7OztBQUdELGFBQVcsWUFBVzs7QUFFcEIsWUFBUSxRQUFRLENBQWhCOztBQUVBLFFBQUcsU0FBUyxLQUFaLEVBQW1CO0FBQ2pCO0FBQ0E7QUFDRDs7QUFFRCxRQUNFLFlBQXFCLFFBQVEsYUFBUixDQUFzQixLQUF0QixDQUR2QjtBQUFBLFFBRUUscUJBQXFCLFVBQVUsV0FBVixFQUZ2QjtBQUFBLFFBR0Usa0JBQXFCLEtBQUssT0FBTCxDQUFhLFFBQVEsVUFBUixHQUFxQixrQkFBbEMsQ0FIdkI7QUFBQSxRQUlFLFdBQXFCLFFBQVEsWUFBUixHQUF1QixTQUo5QztBQUFBLFFBTUUsYUFBcUIsRUFBRSxLQUFLLGVBQVAsRUFOdkI7QUFBQSxRQU9FLGNBQXFCLEVBQUUsTUFBTSxJQUFSLEVBQWMsS0FBSyxlQUFuQixFQUFvQyxPQUFPLElBQTNDLEVBUHZCO0FBQUEsUUFRRSxlQUFxQixFQUFFLE1BQU0sV0FBUixFQUFxQixLQUFLLGVBQTFCLEVBQTJDLE9BQU8sSUFBbEQsRUFSdkI7QUFBQSxRQVNFLFNBQXFCLG9CQUFvQixRQUFRLEdBQTVCLEdBQWtDLEdBQWxDLEdBQXdDLFFBQXhDLEdBQW1ELE1BVDFFO0FBQUEsUUFVRSxVQUFxQix3QkFBd0IsUUFBUSxHQUFoQyxHQUFzQyxHQUF0QyxHQUE0QyxRQUE1QyxHQUF1RCxHQVY5RTtBQUFBLFFBV0UsaUJBQXFCLEdBQUcsVUFBSCxDQUFjLEtBQUssSUFBTCxDQUFVLGVBQVYsRUFBMkIsTUFBM0IsQ0FBZCxDQVh2Qjs7QUFjQSxZQUFRLEdBQVIsQ0FBWSw0QkFBNEIsZUFBeEM7OztBQUdBLFFBQUksQ0FBQyxHQUFHLFVBQUgsQ0FBYyxlQUFkLENBQUwsRUFBc0M7QUFDcEMsYUFBTyxJQUFQLENBQVksZUFBWjtBQUNEOzs7QUFHRCxRQUFHLFFBQVEsVUFBUixDQUFtQixNQUFuQixDQUEwQixVQUExQixLQUF5QyxDQUE1QyxFQUErQztBQUM3QyxjQUFRLElBQVIsQ0FBYSxjQUFiLEVBQTZCLGVBQTdCO0FBQ0EsVUFBSSxJQUFKLENBQVMsQ0FBQyxrQkFBa0IsTUFBbkIsQ0FBVCxFQUFxQyxFQUFDLFFBQVEsSUFBVCxFQUFlLE9BQU8sSUFBdEIsRUFBckM7QUFDRDs7O0FBR0QsYUFBUyxTQUFULEdBQXFCO0FBQ25CLFVBQUcsY0FBSCxFQUFtQjtBQUNqQjtBQUNELE9BRkQsTUFHSztBQUNIO0FBQ0Q7QUFDRjs7QUFFRCxhQUFTLFFBQVQsR0FBb0I7QUFDbEIsY0FBUSxJQUFSLENBQWEsaUNBQWlDLFNBQTlDO0FBQ0EsVUFBSSxJQUFKLENBQVMsVUFBVCxFQUFxQixVQUFTLEtBQVQsRUFBZ0I7QUFDbkMsWUFBRyxLQUFILEVBQVU7QUFDUixrQkFBUSxLQUFSLENBQWMseUJBQWQsRUFBeUMsS0FBekM7QUFDRDtBQUNEO0FBQ0QsT0FMRDtBQU1EOztBQUVELGFBQVMsVUFBVCxHQUFzQjtBQUNwQixjQUFRLElBQVIsQ0FBYSwwQkFBMEIsT0FBdkM7QUFDQSxhQUFPLEtBQVAsQ0FBYSxhQUFiLENBQTJCO0FBQ3pCLGFBQVcsUUFBUSxHQURNO0FBRXpCLGNBQVcsUUFGYztBQUd6QixrQkFBVyxRQUFRO0FBSE0sT0FBM0IsRUFJRyxZQUFXO0FBQ1o7QUFDRCxPQU5EO0FBT0Q7O0FBRUQsYUFBUyxTQUFULEdBQXFCO0FBQ25CLGNBQVEsSUFBUixDQUFhLDZCQUE2QixNQUExQztBQUNBLFVBQUksU0FBSixDQUFjLFFBQWQsRUFBd0IsTUFBeEIsRUFBZ0MsVUFBaEMsRUFBNEMsWUFBVTtBQUNwRDtBQUNELE9BRkQ7QUFHRDs7QUFFRCxhQUFTLFNBQVQsR0FBcUI7QUFDbkIsY0FBUSxJQUFSLENBQWEsYUFBYSxTQUFiLEdBQXlCLFFBQXRDO0FBQ0EsVUFBSSxJQUFKLENBQVMsUUFBVCxFQUFtQixRQUFuQixFQUE2QixXQUE3QixFQUEwQyxVQUFTLEtBQVQsRUFBZ0I7QUFDeEQ7QUFDRCxPQUZEO0FBR0Q7O0FBRUQsYUFBUyxVQUFULEdBQXNCO0FBQ3BCLGNBQVEsSUFBUixDQUFhLHlCQUFiO0FBQ0EsVUFBSSxLQUFKLENBQVUsTUFBVixFQUFrQixZQUFsQixFQUFnQyxVQUFTLEtBQVQsRUFBZ0I7QUFDOUM7QUFDRCxPQUZEO0FBR0Q7O0FBRUQsYUFBUyxRQUFULEdBQW9COzs7QUFHbEIsYUFBTyxZQUFQLENBQW9CLEtBQXBCO0FBQ0EsY0FBUSxPQUFPLFVBQVAsQ0FBa0IsWUFBVztBQUNuQztBQUNELE9BRk8sRUFFTCxDQUZLLENBQVI7QUFHRDs7QUFHRCxRQUFHLGNBQUgsRUFBbUI7QUFDakI7QUFDRCxLQUZELE1BR0s7QUFDSDs7QUFFRDtBQUVGLEdBMUdEOztBQTRHQTtBQUdELENBL0hEIiwiZmlsZSI6ImluaXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICBJbml0IERpc3QgUmVwb3NcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi8qXG5cbiBUaGlzIHRhc2sgcHVsbHMgdGhlIGxhdGVzdCB2ZXJzaW9uIG9mIGRpc3RyaWJ1dGlvbiBmcm9tIEdpdEh1YlxuXG4gICogQ3JlYXRlcyBuZXcgcmVwbyBpZiBkb2VzbnQgZXhpc3QgKGxvY2FsbHkgJiBHaXRIdWIpXG4gICogQWRkcyByZW1vdGUgaXQgZG9lc250IGV4aXN0c1xuICAqIFB1bGxzIGxhdGVzdCBjaGFuZ2VzIGZyb20gcmVwb1xuXG4qL1xuXG52YXJcbiAgZ3VscCAgICAgID0gcmVxdWlyZSgnZ3VscCcpLFxuXG4gIC8vIG5vZGUgZGVwZW5kZW5jaWVzXG4gIGNvbnNvbGUgICA9IHJlcXVpcmUoJ2JldHRlci1jb25zb2xlJyksXG4gIGRlbCAgICAgICA9IHJlcXVpcmUoJ2RlbCcpLFxuICBmcyAgICAgICAgPSByZXF1aXJlKCdmcycpLFxuICBwYXRoICAgICAgPSByZXF1aXJlKCdwYXRoJyksXG4gIGdpdCAgICAgICA9IHJlcXVpcmUoJ2d1bHAtZ2l0JyksXG4gIGdpdGh1YkFQSSA9IHJlcXVpcmUoJ2dpdGh1YicpLFxuICBta2RpcnAgICAgPSByZXF1aXJlKCdta2RpcnAnKSxcblxuICAvLyBhZG1pbiBmaWxlc1xuICBnaXRodWIgICAgPSByZXF1aXJlKCcuLi8uLi9jb25maWcvYWRtaW4vZ2l0aHViLmpzJyksXG4gIHJlbGVhc2UgICA9IHJlcXVpcmUoJy4uLy4uL2NvbmZpZy9hZG1pbi9yZWxlYXNlJyksXG4gIHByb2plY3QgICA9IHJlcXVpcmUoJy4uLy4uL2NvbmZpZy9wcm9qZWN0L3JlbGVhc2UnKSxcblxuXG4gIC8vIG9BdXRoIGNvbmZpZ3VyYXRpb24gZm9yIEdpdEh1YlxuICBvQXV0aCAgICAgPSBmcy5leGlzdHNTeW5jKF9fZGlybmFtZSArICcvLi4vLi4vY29uZmlnL2FkbWluL29hdXRoLmpzJylcbiAgICA/IHJlcXVpcmUoJy4uLy4uL2NvbmZpZy9hZG1pbi9vYXV0aCcpXG4gICAgOiBmYWxzZSxcblxuICAvLyBzaG9ydGhhbmRcbiAgdmVyc2lvbiA9IHByb2plY3QudmVyc2lvblxuO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG5cbiAgdmFyXG4gICAgaW5kZXggPSAtMSxcbiAgICB0b3RhbCA9IHJlbGVhc2UuZGlzdHJpYnV0aW9ucy5sZW5ndGgsXG4gICAgdGltZXIsXG4gICAgc3RyZWFtLFxuICAgIHN0ZXBSZXBvXG4gIDtcblxuICBpZighb0F1dGgpIHtcbiAgICBjb25zb2xlLmVycm9yKCdNdXN0IGFkZCBvYXV0aCB0b2tlbiBmb3IgR2l0SHViIGluIHRhc2tzL2NvbmZpZy9hZG1pbi9vYXV0aC5qcycpO1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8vIERvIEdpdCBjb21tYW5kcyBzeW5jaHJvbm91c2x5IHBlciBjb21wb25lbnQsIHRvIGF2b2lkIGlzc3Vlc1xuICBzdGVwUmVwbyA9IGZ1bmN0aW9uKCkge1xuXG4gICAgaW5kZXggPSBpbmRleCArIDE7XG5cbiAgICBpZihpbmRleCA+PSB0b3RhbCkge1xuICAgICAgY2FsbGJhY2soKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXJcbiAgICAgIGNvbXBvbmVudCAgICAgICAgICA9IHJlbGVhc2UuZGlzdHJpYnV0aW9uc1tpbmRleF0sXG4gICAgICBsb3dlckNhc2VDb21wb25lbnQgPSBjb21wb25lbnQudG9Mb3dlckNhc2UoKSxcbiAgICAgIG91dHB1dERpcmVjdG9yeSAgICA9IHBhdGgucmVzb2x2ZShyZWxlYXNlLm91dHB1dFJvb3QgKyBsb3dlckNhc2VDb21wb25lbnQpLFxuICAgICAgcmVwb05hbWUgICAgICAgICAgID0gcmVsZWFzZS5kaXN0UmVwb1Jvb3QgKyBjb21wb25lbnQsXG5cbiAgICAgIGdpdE9wdGlvbnMgICAgICAgICA9IHsgY3dkOiBvdXRwdXREaXJlY3RvcnkgfSxcbiAgICAgIHB1bGxPcHRpb25zICAgICAgICA9IHsgYXJnczogJy1xJywgY3dkOiBvdXRwdXREaXJlY3RvcnksIHF1aWV0OiB0cnVlIH0sXG4gICAgICByZXNldE9wdGlvbnMgICAgICAgPSB7IGFyZ3M6ICctcSAtLWhhcmQnLCBjd2Q6IG91dHB1dERpcmVjdG9yeSwgcXVpZXQ6IHRydWUgfSxcbiAgICAgIGdpdFVSTCAgICAgICAgICAgICA9ICdnaXRAZ2l0aHViLmNvbTonICsgcmVsZWFzZS5vcmcgKyAnLycgKyByZXBvTmFtZSArICcuZ2l0JyxcbiAgICAgIHJlcG9VUkwgICAgICAgICAgICA9ICdodHRwczovL2dpdGh1Yi5jb20vJyArIHJlbGVhc2Uub3JnICsgJy8nICsgcmVwb05hbWUgKyAnLycsXG4gICAgICBsb2NhbFJlcG9TZXR1cCAgICAgPSBmcy5leGlzdHNTeW5jKHBhdGguam9pbihvdXRwdXREaXJlY3RvcnksICcuZ2l0JykpXG4gICAgO1xuXG4gICAgY29uc29sZS5sb2coJ1Byb2Nlc3NpbmcgcmVwb3NpdG9yeTogJyArIG91dHB1dERpcmVjdG9yeSk7XG5cbiAgICAvLyBjcmVhdGUgZm9sZGVyIGlmIGRvZXNuJ3QgZXhpc3RcbiAgICBpZiggIWZzLmV4aXN0c1N5bmMob3V0cHV0RGlyZWN0b3J5KSApIHtcbiAgICAgIG1rZGlycC5zeW5jKG91dHB1dERpcmVjdG9yeSk7XG4gICAgfVxuXG4gICAgLy8gY2xlYW4gZm9sZGVyXG4gICAgaWYocmVsZWFzZS5vdXRwdXRSb290LnNlYXJjaCgnLi4vcmVwb3MnKSA9PSAwKSB7XG4gICAgICBjb25zb2xlLmluZm8oJ0NsZWFuaW5nIGRpcicsIG91dHB1dERpcmVjdG9yeSk7XG4gICAgICBkZWwuc3luYyhbb3V0cHV0RGlyZWN0b3J5ICsgJyoqLyonXSwge3NpbGVudDogdHJ1ZSwgZm9yY2U6IHRydWV9KTtcbiAgICB9XG5cbiAgICAvLyBzZXQtdXAgbG9jYWwgcmVwb1xuICAgIGZ1bmN0aW9uIHNldHVwUmVwbygpIHtcbiAgICAgIGlmKGxvY2FsUmVwb1NldHVwKSB7XG4gICAgICAgIGFkZFJlbW90ZSgpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGluaXRSZXBvKCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaW5pdFJlcG8oKSB7XG4gICAgICBjb25zb2xlLmluZm8oJ0luaXRpYWxpemluZyByZXBvc2l0b3J5IGZvciAnICsgY29tcG9uZW50KTtcbiAgICAgIGdpdC5pbml0KGdpdE9wdGlvbnMsIGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAgIGlmKGVycm9yKSB7XG4gICAgICAgICAgY29uc29sZS5lcnJvcignRXJyb3IgaW5pdGlhbGl6aW5nIHJlcG8nLCBlcnJvcik7XG4gICAgICAgIH1cbiAgICAgICAgYWRkUmVtb3RlKCk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjcmVhdGVSZXBvKCkge1xuICAgICAgY29uc29sZS5pbmZvKCdDcmVhdGluZyBHaXRIdWIgcmVwbyAnICsgcmVwb1VSTCk7XG4gICAgICBnaXRodWIucmVwb3MuY3JlYXRlRnJvbU9yZyh7XG4gICAgICAgIG9yZyAgICAgIDogcmVsZWFzZS5vcmcsXG4gICAgICAgIG5hbWUgICAgIDogcmVwb05hbWUsXG4gICAgICAgIGhvbWVwYWdlIDogcmVsZWFzZS5ob21lcGFnZVxuICAgICAgfSwgZnVuY3Rpb24oKSB7XG4gICAgICAgIHNldHVwUmVwbygpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gYWRkUmVtb3RlKCkge1xuICAgICAgY29uc29sZS5pbmZvKCdBZGRpbmcgcmVtb3RlIG9yaWdpbiBhcyAnICsgZ2l0VVJMKTtcbiAgICAgIGdpdC5hZGRSZW1vdGUoJ29yaWdpbicsIGdpdFVSTCwgZ2l0T3B0aW9ucywgZnVuY3Rpb24oKXtcbiAgICAgICAgcHVsbEZpbGVzKCk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBwdWxsRmlsZXMoKSB7XG4gICAgICBjb25zb2xlLmluZm8oJ1B1bGxpbmcgJyArIGNvbXBvbmVudCArICcgZmlsZXMnKTtcbiAgICAgIGdpdC5wdWxsKCdvcmlnaW4nLCAnbWFzdGVyJywgcHVsbE9wdGlvbnMsIGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAgIHJlc2V0RmlsZXMoKTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHJlc2V0RmlsZXMoKSB7XG4gICAgICBjb25zb2xlLmluZm8oJ1Jlc2V0dGluZyBmaWxlcyB0byBoZWFkJyk7XG4gICAgICBnaXQucmVzZXQoJ0hFQUQnLCByZXNldE9wdGlvbnMsIGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAgIG5leHRSZXBvKCk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBuZXh0UmVwbygpIHtcbiAgICAgIC8vY29uc29sZS5sb2coJ1NsZWVwaW5nIGZvciAxIHNlY29uZC4uLicpO1xuICAgICAgLy8gYXZvaWQgcmF0ZSB0aHJvdHRsaW5nXG4gICAgICBnbG9iYWwuY2xlYXJUaW1lb3V0KHRpbWVyKTtcbiAgICAgIHRpbWVyID0gZ2xvYmFsLnNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgIHN0ZXBSZXBvKClcbiAgICAgIH0sIDApO1xuICAgIH1cblxuXG4gICAgaWYobG9jYWxSZXBvU2V0dXApIHtcbiAgICAgIHB1bGxGaWxlcygpO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIHNldHVwUmVwbygpO1xuICAgICAgLy8gY3JlYXRlUmVwbygpIG9ubHkgdXNlIHRvIGNyZWF0ZSByZW1vdGUgcmVwbyAoZWFzaWVyIHRvIGRvIG1hbnVhbGx5KVxuICAgIH1cblxuICB9O1xuXG4gIHN0ZXBSZXBvKCk7XG5cblxufTtcbiJdfQ==