/*******************************
          Release
*******************************/

/*
 This task update all SUI individual component repos with new versions of components

  * Initializes repositories with current versions
  * Creates local files at ../distributions/ with each repo for release

*/

var runSequence = require('run-sequence');

/* Release All */
module.exports = function (callback) {

  runSequence(
  //'build', // build Semantic
  'init distributions', // sync with current github version
  'create distributions', // update each repo with changes from master repo
  'init components', // sync with current github version
  'create components', // update each repo
  callback);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2FkbWluL3JlbGVhc2UuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBWUEsSUFDRSxjQUFjLFFBQVEsY0FBUixDQURoQjs7O0FBS0EsT0FBTyxPQUFQLEdBQWlCLFVBQVMsUUFBVCxFQUFtQjs7QUFFbEM7O0FBRUUsc0JBRkYsRTtBQUdFLHdCQUhGLEU7QUFJRSxtQkFKRixFO0FBS0UscUJBTEYsRTtBQU1FLFVBTkY7QUFTRCxDQVhEIiwiZmlsZSI6InJlbGVhc2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgIFJlbGVhc2VcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi8qXG4gVGhpcyB0YXNrIHVwZGF0ZSBhbGwgU1VJIGluZGl2aWR1YWwgY29tcG9uZW50IHJlcG9zIHdpdGggbmV3IHZlcnNpb25zIG9mIGNvbXBvbmVudHNcblxuICAqIEluaXRpYWxpemVzIHJlcG9zaXRvcmllcyB3aXRoIGN1cnJlbnQgdmVyc2lvbnNcbiAgKiBDcmVhdGVzIGxvY2FsIGZpbGVzIGF0IC4uL2Rpc3RyaWJ1dGlvbnMvIHdpdGggZWFjaCByZXBvIGZvciByZWxlYXNlXG5cbiovXG5cbnZhclxuICBydW5TZXF1ZW5jZSA9IHJlcXVpcmUoJ3J1bi1zZXF1ZW5jZScpXG47XG5cbi8qIFJlbGVhc2UgQWxsICovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG5cbiAgcnVuU2VxdWVuY2UoXG4gICAgLy8nYnVpbGQnLCAvLyBidWlsZCBTZW1hbnRpY1xuICAgICdpbml0IGRpc3RyaWJ1dGlvbnMnLCAvLyBzeW5jIHdpdGggY3VycmVudCBnaXRodWIgdmVyc2lvblxuICAgICdjcmVhdGUgZGlzdHJpYnV0aW9ucycsIC8vIHVwZGF0ZSBlYWNoIHJlcG8gd2l0aCBjaGFuZ2VzIGZyb20gbWFzdGVyIHJlcG9cbiAgICAnaW5pdCBjb21wb25lbnRzJywgLy8gc3luYyB3aXRoIGN1cnJlbnQgZ2l0aHViIHZlcnNpb25cbiAgICAnY3JlYXRlIGNvbXBvbmVudHMnLCAvLyB1cGRhdGUgZWFjaCByZXBvXG4gICAgY2FsbGJhY2tcbiAgKTtcblxufTsiXX0=