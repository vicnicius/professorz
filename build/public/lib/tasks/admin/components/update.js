/*******************************
          Update Repos
*******************************/

/*

 This task update all SUI individual component repos with new versions of components

  * Commits changes from create repo
  * Pushes changes to GitHub
  * Tag new releases if version changed in main repo

*/

var gulp = require('gulp'),


// node dependencies
console = require('better-console'),
    fs = require('fs'),
    path = require('path'),
    git = require('gulp-git'),
    githubAPI = require('github'),
    requireDotFile = require('require-dot-file'),


// admin files
github = require('../../config/admin/github.js'),
    release = require('../../config/admin/release'),
    project = require('../../config/project/release'),


// oAuth configuration for GitHub
oAuth = fs.existsSync(__dirname + '/../../config/admin/oauth.js') ? require('../../config/admin/oauth') : false,


// shorthand
version = project.version;

module.exports = function (callback) {

  var index = -1,
      total = release.components.length,
      timer,
      stream,
      stepRepo;

  if (!oAuth) {
    console.error('Must add oauth token for GitHub in tasks/config/admin/oauth.js');
    return;
  }

  // Do Git commands synchronously per component, to avoid issues
  stepRepo = function () {

    index = index + 1;
    if (index >= total) {
      callback();
      return;
    }

    var component = release.components[index],
        outputDirectory = path.resolve(path.join(release.outputRoot, component)),
        capitalizedComponent = component.charAt(0).toUpperCase() + component.slice(1),
        repoName = release.componentRepoRoot + capitalizedComponent,
        gitURL = 'https://github.com/' + release.org + '/' + repoName + '.git',
        repoURL = 'https://github.com/' + release.org + '/' + repoName + '/',
        commitArgs = oAuth.name !== undefined && oAuth.email !== undefined ? '--author "' + oAuth.name + ' <' + oAuth.email + '>"' : '',
        componentPackage = fs.existsSync(outputDirectory + 'package.json') ? require(outputDirectory + 'package.json') : false,
        isNewVersion = version && componentPackage.version != version,
        commitMessage = isNewVersion ? 'Updated component to version ' + version : 'Updated files from main repo',
        gitOptions = { cwd: outputDirectory },
        commitOptions = { args: commitArgs, cwd: outputDirectory },
        releaseOptions = { tag_name: version, owner: release.org, repo: repoName },
        fileModeOptions = { args: 'config core.fileMode false', cwd: outputDirectory },
        usernameOptions = { args: 'config user.name "' + oAuth.name + '"', cwd: outputDirectory },
        emailOptions = { args: 'config user.email "' + oAuth.email + '"', cwd: outputDirectory },
        versionOptions = { args: 'rev-parse --verify HEAD', cwd: outputDirectory },
        localRepoSetup = fs.existsSync(path.join(outputDirectory, '.git')),
        canProceed = true;

    console.info('Processing repository:' + outputDirectory);

    function setConfig() {
      git.exec(fileModeOptions, function () {
        git.exec(usernameOptions, function () {
          git.exec(emailOptions, function () {
            commitFiles();
          });
        });
      });
    }

    // standard path
    function commitFiles() {
      // commit files
      console.info('Committing ' + component + ' files', commitArgs);
      gulp.src('./', gitOptions).pipe(git.add(gitOptions)).pipe(git.commit(commitMessage, commitOptions)).on('error', function (error) {
        // canProceed = false; bug in git commit <https://github.com/stevelacy/gulp-git/issues/49>
      }).on('finish', function (callback) {
        if (canProceed) {
          pushFiles();
        } else {
          console.info('Nothing new to commit');
          nextRepo();
        }
      });
    }

    // push changes to remote
    function pushFiles() {
      console.info('Pushing files for ' + component);
      git.push('origin', 'master', { args: '', cwd: outputDirectory }, function (error) {
        console.info('Push completed successfully');
        getSHA();
      });
    }

    // gets SHA of last commit
    function getSHA() {
      git.exec(versionOptions, function (error, version) {
        version = version.trim();
        createRelease(version);
      });
    }

    // create release on GitHub.com
    function createRelease(version) {
      if (version) {
        releaseOptions.target_commitish = version;
      }
      github.releases.createRelease(releaseOptions, function () {
        nextRepo();
      });
    }

    // Steps to next repository
    function nextRepo() {
      console.log('Sleeping for 1 second...');
      // avoid rate throttling
      global.clearTimeout(timer);
      timer = global.setTimeout(stepRepo, 1000);
    }

    if (localRepoSetup) {
      setConfig();
    } else {
      console.error('Repository must be setup before running update components');
    }
  };

  stepRepo();
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2FkbWluL2NvbXBvbmVudHMvdXBkYXRlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBY0EsSUFDRSxPQUFpQixRQUFRLE1BQVIsQ0FEbkI7QUFBQTs7O0FBSUUsVUFBaUIsUUFBUSxnQkFBUixDQUpuQjtBQUFBLElBS0UsS0FBaUIsUUFBUSxJQUFSLENBTG5CO0FBQUEsSUFNRSxPQUFpQixRQUFRLE1BQVIsQ0FObkI7QUFBQSxJQU9FLE1BQWlCLFFBQVEsVUFBUixDQVBuQjtBQUFBLElBUUUsWUFBaUIsUUFBUSxRQUFSLENBUm5CO0FBQUEsSUFTRSxpQkFBaUIsUUFBUSxrQkFBUixDQVRuQjtBQUFBOzs7QUFZRSxTQUFpQixRQUFRLDhCQUFSLENBWm5CO0FBQUEsSUFhRSxVQUFpQixRQUFRLDRCQUFSLENBYm5CO0FBQUEsSUFjRSxVQUFpQixRQUFRLDhCQUFSLENBZG5CO0FBQUE7OztBQWtCRSxRQUFpQixHQUFHLFVBQUgsQ0FBYyxZQUFZLDhCQUExQixJQUNiLFFBQVEsMEJBQVIsQ0FEYSxHQUViLEtBcEJOO0FBQUE7OztBQXVCRSxVQUFVLFFBQVEsT0F2QnBCOztBQTBCQSxPQUFPLE9BQVAsR0FBaUIsVUFBUyxRQUFULEVBQW1COztBQUVsQyxNQUNFLFFBQVEsQ0FBQyxDQURYO0FBQUEsTUFFRSxRQUFRLFFBQVEsVUFBUixDQUFtQixNQUY3QjtBQUFBLE1BR0UsS0FIRjtBQUFBLE1BSUUsTUFKRjtBQUFBLE1BS0UsUUFMRjs7QUFRQSxNQUFHLENBQUMsS0FBSixFQUFXO0FBQ1QsWUFBUSxLQUFSLENBQWMsZ0VBQWQ7QUFDQTtBQUNEOzs7QUFHRCxhQUFXLFlBQVc7O0FBRXBCLFlBQVEsUUFBUSxDQUFoQjtBQUNBLFFBQUcsU0FBUyxLQUFaLEVBQW1CO0FBQ2pCO0FBQ0E7QUFDRDs7QUFFRCxRQUNFLFlBQXVCLFFBQVEsVUFBUixDQUFtQixLQUFuQixDQUR6QjtBQUFBLFFBRUUsa0JBQXVCLEtBQUssT0FBTCxDQUFhLEtBQUssSUFBTCxDQUFVLFFBQVEsVUFBbEIsRUFBOEIsU0FBOUIsQ0FBYixDQUZ6QjtBQUFBLFFBR0UsdUJBQXVCLFVBQVUsTUFBVixDQUFpQixDQUFqQixFQUFvQixXQUFwQixLQUFvQyxVQUFVLEtBQVYsQ0FBZ0IsQ0FBaEIsQ0FIN0Q7QUFBQSxRQUlFLFdBQXVCLFFBQVEsaUJBQVIsR0FBNEIsb0JBSnJEO0FBQUEsUUFNRSxTQUF1Qix3QkFBd0IsUUFBUSxHQUFoQyxHQUFzQyxHQUF0QyxHQUE0QyxRQUE1QyxHQUF1RCxNQU5oRjtBQUFBLFFBT0UsVUFBdUIsd0JBQXdCLFFBQVEsR0FBaEMsR0FBc0MsR0FBdEMsR0FBNEMsUUFBNUMsR0FBdUQsR0FQaEY7QUFBQSxRQVNFLGFBQWMsTUFBTSxJQUFOLEtBQWUsU0FBZixJQUE0QixNQUFNLEtBQU4sS0FBZ0IsU0FBN0MsR0FDVCxlQUFlLE1BQU0sSUFBckIsR0FBNEIsSUFBNUIsR0FBbUMsTUFBTSxLQUF6QyxHQUFpRCxJQUR4QyxHQUVULEVBWE47QUFBQSxRQWFFLG1CQUFtQixHQUFHLFVBQUgsQ0FBYyxrQkFBa0IsY0FBaEMsSUFDZixRQUFRLGtCQUFrQixjQUExQixDQURlLEdBRWYsS0FmTjtBQUFBLFFBaUJFLGVBQWlCLFdBQVcsaUJBQWlCLE9BQWpCLElBQTRCLE9BakIxRDtBQUFBLFFBbUJFLGdCQUFpQixZQUFELEdBQ1osa0NBQWtDLE9BRHRCLEdBRVosOEJBckJOO0FBQUEsUUF1QkUsYUFBa0IsRUFBRSxLQUFLLGVBQVAsRUF2QnBCO0FBQUEsUUF3QkUsZ0JBQWtCLEVBQUUsTUFBTSxVQUFSLEVBQW9CLEtBQUssZUFBekIsRUF4QnBCO0FBQUEsUUF5QkUsaUJBQWtCLEVBQUUsVUFBVSxPQUFaLEVBQXFCLE9BQU8sUUFBUSxHQUFwQyxFQUF5QyxNQUFNLFFBQS9DLEVBekJwQjtBQUFBLFFBMkJFLGtCQUFrQixFQUFFLE1BQU8sNEJBQVQsRUFBdUMsS0FBSyxlQUE1QyxFQTNCcEI7QUFBQSxRQTRCRSxrQkFBa0IsRUFBRSxNQUFPLHVCQUF1QixNQUFNLElBQTdCLEdBQW9DLEdBQTdDLEVBQWtELEtBQUssZUFBdkQsRUE1QnBCO0FBQUEsUUE2QkUsZUFBa0IsRUFBRSxNQUFPLHdCQUF3QixNQUFNLEtBQTlCLEdBQXNDLEdBQS9DLEVBQW9ELEtBQUssZUFBekQsRUE3QnBCO0FBQUEsUUE4QkUsaUJBQWtCLEVBQUUsTUFBTyx5QkFBVCxFQUFvQyxLQUFLLGVBQXpDLEVBOUJwQjtBQUFBLFFBZ0NFLGlCQUFrQixHQUFHLFVBQUgsQ0FBYyxLQUFLLElBQUwsQ0FBVSxlQUFWLEVBQTJCLE1BQTNCLENBQWQsQ0FoQ3BCO0FBQUEsUUFpQ0UsYUFBa0IsSUFqQ3BCOztBQXFDQSxZQUFRLElBQVIsQ0FBYSwyQkFBMkIsZUFBeEM7O0FBRUEsYUFBUyxTQUFULEdBQXFCO0FBQ25CLFVBQUksSUFBSixDQUFTLGVBQVQsRUFBMEIsWUFBVztBQUNuQyxZQUFJLElBQUosQ0FBUyxlQUFULEVBQTBCLFlBQVk7QUFDcEMsY0FBSSxJQUFKLENBQVMsWUFBVCxFQUF1QixZQUFZO0FBQ2pDO0FBQ0QsV0FGRDtBQUdELFNBSkQ7QUFLRCxPQU5EO0FBT0Q7OztBQUlELGFBQVMsV0FBVCxHQUF1Qjs7QUFFckIsY0FBUSxJQUFSLENBQWEsZ0JBQWdCLFNBQWhCLEdBQTRCLFFBQXpDLEVBQW1ELFVBQW5EO0FBQ0EsV0FBSyxHQUFMLENBQVMsSUFBVCxFQUFlLFVBQWYsRUFDRyxJQURILENBQ1EsSUFBSSxHQUFKLENBQVEsVUFBUixDQURSLEVBRUcsSUFGSCxDQUVRLElBQUksTUFBSixDQUFXLGFBQVgsRUFBMEIsYUFBMUIsQ0FGUixFQUdHLEVBSEgsQ0FHTSxPQUhOLEVBR2UsVUFBUyxLQUFULEVBQWdCOztBQUU1QixPQUxILEVBTUcsRUFOSCxDQU1NLFFBTk4sRUFNZ0IsVUFBUyxRQUFULEVBQW1CO0FBQy9CLFlBQUcsVUFBSCxFQUFlO0FBQ2I7QUFDRCxTQUZELE1BR0s7QUFDSCxrQkFBUSxJQUFSLENBQWEsdUJBQWI7QUFDQTtBQUNEO0FBQ0YsT0FkSDtBQWdCRDs7O0FBR0QsYUFBUyxTQUFULEdBQXFCO0FBQ25CLGNBQVEsSUFBUixDQUFhLHVCQUF1QixTQUFwQztBQUNBLFVBQUksSUFBSixDQUFTLFFBQVQsRUFBbUIsUUFBbkIsRUFBNkIsRUFBRSxNQUFNLEVBQVIsRUFBWSxLQUFLLGVBQWpCLEVBQTdCLEVBQWlFLFVBQVMsS0FBVCxFQUFnQjtBQUMvRSxnQkFBUSxJQUFSLENBQWEsNkJBQWI7QUFDQTtBQUNELE9BSEQ7QUFJRDs7O0FBR0QsYUFBUyxNQUFULEdBQWtCO0FBQ2hCLFVBQUksSUFBSixDQUFTLGNBQVQsRUFBeUIsVUFBUyxLQUFULEVBQWdCLE9BQWhCLEVBQXlCO0FBQ2hELGtCQUFVLFFBQVEsSUFBUixFQUFWO0FBQ0Esc0JBQWMsT0FBZDtBQUNELE9BSEQ7QUFJRDs7O0FBR0QsYUFBUyxhQUFULENBQXVCLE9BQXZCLEVBQWdDO0FBQzlCLFVBQUcsT0FBSCxFQUFZO0FBQ1YsdUJBQWUsZ0JBQWYsR0FBa0MsT0FBbEM7QUFDRDtBQUNELGFBQU8sUUFBUCxDQUFnQixhQUFoQixDQUE4QixjQUE5QixFQUE4QyxZQUFXO0FBQ3ZEO0FBQ0QsT0FGRDtBQUdEOzs7QUFHRCxhQUFTLFFBQVQsR0FBb0I7QUFDbEIsY0FBUSxHQUFSLENBQVksMEJBQVo7O0FBRUEsYUFBTyxZQUFQLENBQW9CLEtBQXBCO0FBQ0EsY0FBUSxPQUFPLFVBQVAsQ0FBa0IsUUFBbEIsRUFBNEIsSUFBNUIsQ0FBUjtBQUNEOztBQUdELFFBQUcsY0FBSCxFQUFtQjtBQUNqQjtBQUNELEtBRkQsTUFHSztBQUNILGNBQVEsS0FBUixDQUFjLDJEQUFkO0FBQ0Q7QUFFRixHQTNIRDs7QUE2SEE7QUFFRCxDQS9JRCIsImZpbGUiOiJ1cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgIFVwZGF0ZSBSZXBvc1xuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuLypcblxuIFRoaXMgdGFzayB1cGRhdGUgYWxsIFNVSSBpbmRpdmlkdWFsIGNvbXBvbmVudCByZXBvcyB3aXRoIG5ldyB2ZXJzaW9ucyBvZiBjb21wb25lbnRzXG5cbiAgKiBDb21taXRzIGNoYW5nZXMgZnJvbSBjcmVhdGUgcmVwb1xuICAqIFB1c2hlcyBjaGFuZ2VzIHRvIEdpdEh1YlxuICAqIFRhZyBuZXcgcmVsZWFzZXMgaWYgdmVyc2lvbiBjaGFuZ2VkIGluIG1haW4gcmVwb1xuXG4qL1xuXG52YXJcbiAgZ3VscCAgICAgICAgICAgPSByZXF1aXJlKCdndWxwJyksXG5cbiAgLy8gbm9kZSBkZXBlbmRlbmNpZXNcbiAgY29uc29sZSAgICAgICAgPSByZXF1aXJlKCdiZXR0ZXItY29uc29sZScpLFxuICBmcyAgICAgICAgICAgICA9IHJlcXVpcmUoJ2ZzJyksXG4gIHBhdGggICAgICAgICAgID0gcmVxdWlyZSgncGF0aCcpLFxuICBnaXQgICAgICAgICAgICA9IHJlcXVpcmUoJ2d1bHAtZ2l0JyksXG4gIGdpdGh1YkFQSSAgICAgID0gcmVxdWlyZSgnZ2l0aHViJyksXG4gIHJlcXVpcmVEb3RGaWxlID0gcmVxdWlyZSgncmVxdWlyZS1kb3QtZmlsZScpLFxuXG4gIC8vIGFkbWluIGZpbGVzXG4gIGdpdGh1YiAgICAgICAgID0gcmVxdWlyZSgnLi4vLi4vY29uZmlnL2FkbWluL2dpdGh1Yi5qcycpLFxuICByZWxlYXNlICAgICAgICA9IHJlcXVpcmUoJy4uLy4uL2NvbmZpZy9hZG1pbi9yZWxlYXNlJyksXG4gIHByb2plY3QgICAgICAgID0gcmVxdWlyZSgnLi4vLi4vY29uZmlnL3Byb2plY3QvcmVsZWFzZScpLFxuXG5cbiAgLy8gb0F1dGggY29uZmlndXJhdGlvbiBmb3IgR2l0SHViXG4gIG9BdXRoICAgICAgICAgID0gZnMuZXhpc3RzU3luYyhfX2Rpcm5hbWUgKyAnLy4uLy4uL2NvbmZpZy9hZG1pbi9vYXV0aC5qcycpXG4gICAgPyByZXF1aXJlKCcuLi8uLi9jb25maWcvYWRtaW4vb2F1dGgnKVxuICAgIDogZmFsc2UsXG5cbiAgLy8gc2hvcnRoYW5kXG4gIHZlcnNpb24gPSBwcm9qZWN0LnZlcnNpb25cbjtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihjYWxsYmFjaykge1xuXG4gIHZhclxuICAgIGluZGV4ID0gLTEsXG4gICAgdG90YWwgPSByZWxlYXNlLmNvbXBvbmVudHMubGVuZ3RoLFxuICAgIHRpbWVyLFxuICAgIHN0cmVhbSxcbiAgICBzdGVwUmVwb1xuICA7XG5cbiAgaWYoIW9BdXRoKSB7XG4gICAgY29uc29sZS5lcnJvcignTXVzdCBhZGQgb2F1dGggdG9rZW4gZm9yIEdpdEh1YiBpbiB0YXNrcy9jb25maWcvYWRtaW4vb2F1dGguanMnKTtcbiAgICByZXR1cm47XG4gIH1cblxuICAvLyBEbyBHaXQgY29tbWFuZHMgc3luY2hyb25vdXNseSBwZXIgY29tcG9uZW50LCB0byBhdm9pZCBpc3N1ZXNcbiAgc3RlcFJlcG8gPSBmdW5jdGlvbigpIHtcblxuICAgIGluZGV4ID0gaW5kZXggKyAxO1xuICAgIGlmKGluZGV4ID49IHRvdGFsKSB7XG4gICAgICBjYWxsYmFjaygpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhclxuICAgICAgY29tcG9uZW50ICAgICAgICAgICAgPSByZWxlYXNlLmNvbXBvbmVudHNbaW5kZXhdLFxuICAgICAgb3V0cHV0RGlyZWN0b3J5ICAgICAgPSBwYXRoLnJlc29sdmUocGF0aC5qb2luKHJlbGVhc2Uub3V0cHV0Um9vdCwgY29tcG9uZW50KSksXG4gICAgICBjYXBpdGFsaXplZENvbXBvbmVudCA9IGNvbXBvbmVudC5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIGNvbXBvbmVudC5zbGljZSgxKSxcbiAgICAgIHJlcG9OYW1lICAgICAgICAgICAgID0gcmVsZWFzZS5jb21wb25lbnRSZXBvUm9vdCArIGNhcGl0YWxpemVkQ29tcG9uZW50LFxuXG4gICAgICBnaXRVUkwgICAgICAgICAgICAgICA9ICdodHRwczovL2dpdGh1Yi5jb20vJyArIHJlbGVhc2Uub3JnICsgJy8nICsgcmVwb05hbWUgKyAnLmdpdCcsXG4gICAgICByZXBvVVJMICAgICAgICAgICAgICA9ICdodHRwczovL2dpdGh1Yi5jb20vJyArIHJlbGVhc2Uub3JnICsgJy8nICsgcmVwb05hbWUgKyAnLycsXG5cbiAgICAgIGNvbW1pdEFyZ3MgPSAob0F1dGgubmFtZSAhPT0gdW5kZWZpbmVkICYmIG9BdXRoLmVtYWlsICE9PSB1bmRlZmluZWQpXG4gICAgICAgID8gJy0tYXV0aG9yIFwiJyArIG9BdXRoLm5hbWUgKyAnIDwnICsgb0F1dGguZW1haWwgKyAnPlwiJ1xuICAgICAgICA6ICcnLFxuXG4gICAgICBjb21wb25lbnRQYWNrYWdlID0gZnMuZXhpc3RzU3luYyhvdXRwdXREaXJlY3RvcnkgKyAncGFja2FnZS5qc29uJyApXG4gICAgICAgID8gcmVxdWlyZShvdXRwdXREaXJlY3RvcnkgKyAncGFja2FnZS5qc29uJylcbiAgICAgICAgOiBmYWxzZSxcblxuICAgICAgaXNOZXdWZXJzaW9uICA9ICh2ZXJzaW9uICYmIGNvbXBvbmVudFBhY2thZ2UudmVyc2lvbiAhPSB2ZXJzaW9uKSxcblxuICAgICAgY29tbWl0TWVzc2FnZSA9IChpc05ld1ZlcnNpb24pXG4gICAgICAgID8gJ1VwZGF0ZWQgY29tcG9uZW50IHRvIHZlcnNpb24gJyArIHZlcnNpb25cbiAgICAgICAgOiAnVXBkYXRlZCBmaWxlcyBmcm9tIG1haW4gcmVwbycsXG5cbiAgICAgIGdpdE9wdGlvbnMgICAgICA9IHsgY3dkOiBvdXRwdXREaXJlY3RvcnkgfSxcbiAgICAgIGNvbW1pdE9wdGlvbnMgICA9IHsgYXJnczogY29tbWl0QXJncywgY3dkOiBvdXRwdXREaXJlY3RvcnkgfSxcbiAgICAgIHJlbGVhc2VPcHRpb25zICA9IHsgdGFnX25hbWU6IHZlcnNpb24sIG93bmVyOiByZWxlYXNlLm9yZywgcmVwbzogcmVwb05hbWUgfSxcblxuICAgICAgZmlsZU1vZGVPcHRpb25zID0geyBhcmdzIDogJ2NvbmZpZyBjb3JlLmZpbGVNb2RlIGZhbHNlJywgY3dkOiBvdXRwdXREaXJlY3RvcnkgfSxcbiAgICAgIHVzZXJuYW1lT3B0aW9ucyA9IHsgYXJncyA6ICdjb25maWcgdXNlci5uYW1lIFwiJyArIG9BdXRoLm5hbWUgKyAnXCInLCBjd2Q6IG91dHB1dERpcmVjdG9yeSB9LFxuICAgICAgZW1haWxPcHRpb25zICAgID0geyBhcmdzIDogJ2NvbmZpZyB1c2VyLmVtYWlsIFwiJyArIG9BdXRoLmVtYWlsICsgJ1wiJywgY3dkOiBvdXRwdXREaXJlY3RvcnkgfSxcbiAgICAgIHZlcnNpb25PcHRpb25zID0gIHsgYXJncyA6ICdyZXYtcGFyc2UgLS12ZXJpZnkgSEVBRCcsIGN3ZDogb3V0cHV0RGlyZWN0b3J5IH0sXG5cbiAgICAgIGxvY2FsUmVwb1NldHVwICA9IGZzLmV4aXN0c1N5bmMocGF0aC5qb2luKG91dHB1dERpcmVjdG9yeSwgJy5naXQnKSksXG4gICAgICBjYW5Qcm9jZWVkICAgICAgPSB0cnVlXG4gICAgO1xuXG5cbiAgICBjb25zb2xlLmluZm8oJ1Byb2Nlc3NpbmcgcmVwb3NpdG9yeTonICsgb3V0cHV0RGlyZWN0b3J5KTtcblxuICAgIGZ1bmN0aW9uIHNldENvbmZpZygpIHtcbiAgICAgIGdpdC5leGVjKGZpbGVNb2RlT3B0aW9ucywgZnVuY3Rpb24oKSB7XG4gICAgICAgIGdpdC5leGVjKHVzZXJuYW1lT3B0aW9ucywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgIGdpdC5leGVjKGVtYWlsT3B0aW9ucywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgY29tbWl0RmlsZXMoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG5cblxuICAgIC8vIHN0YW5kYXJkIHBhdGhcbiAgICBmdW5jdGlvbiBjb21taXRGaWxlcygpIHtcbiAgICAgIC8vIGNvbW1pdCBmaWxlc1xuICAgICAgY29uc29sZS5pbmZvKCdDb21taXR0aW5nICcgKyBjb21wb25lbnQgKyAnIGZpbGVzJywgY29tbWl0QXJncyk7XG4gICAgICBndWxwLnNyYygnLi8nLCBnaXRPcHRpb25zKVxuICAgICAgICAucGlwZShnaXQuYWRkKGdpdE9wdGlvbnMpKVxuICAgICAgICAucGlwZShnaXQuY29tbWl0KGNvbW1pdE1lc3NhZ2UsIGNvbW1pdE9wdGlvbnMpKVxuICAgICAgICAub24oJ2Vycm9yJywgZnVuY3Rpb24oZXJyb3IpIHtcbiAgICAgICAgICAvLyBjYW5Qcm9jZWVkID0gZmFsc2U7IGJ1ZyBpbiBnaXQgY29tbWl0IDxodHRwczovL2dpdGh1Yi5jb20vc3RldmVsYWN5L2d1bHAtZ2l0L2lzc3Vlcy80OT5cbiAgICAgICAgfSlcbiAgICAgICAgLm9uKCdmaW5pc2gnLCBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICAgIGlmKGNhblByb2NlZWQpIHtcbiAgICAgICAgICAgIHB1c2hGaWxlcygpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGNvbnNvbGUuaW5mbygnTm90aGluZyBuZXcgdG8gY29tbWl0Jyk7XG4gICAgICAgICAgICBuZXh0UmVwbygpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICAgIDtcbiAgICB9XG5cbiAgICAvLyBwdXNoIGNoYW5nZXMgdG8gcmVtb3RlXG4gICAgZnVuY3Rpb24gcHVzaEZpbGVzKCkge1xuICAgICAgY29uc29sZS5pbmZvKCdQdXNoaW5nIGZpbGVzIGZvciAnICsgY29tcG9uZW50KTtcbiAgICAgIGdpdC5wdXNoKCdvcmlnaW4nLCAnbWFzdGVyJywgeyBhcmdzOiAnJywgY3dkOiBvdXRwdXREaXJlY3RvcnkgfSwgZnVuY3Rpb24oZXJyb3IpIHtcbiAgICAgICAgY29uc29sZS5pbmZvKCdQdXNoIGNvbXBsZXRlZCBzdWNjZXNzZnVsbHknKTtcbiAgICAgICAgZ2V0U0hBKCk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICAvLyBnZXRzIFNIQSBvZiBsYXN0IGNvbW1pdFxuICAgIGZ1bmN0aW9uIGdldFNIQSgpIHtcbiAgICAgIGdpdC5leGVjKHZlcnNpb25PcHRpb25zLCBmdW5jdGlvbihlcnJvciwgdmVyc2lvbikge1xuICAgICAgICB2ZXJzaW9uID0gdmVyc2lvbi50cmltKCk7XG4gICAgICAgIGNyZWF0ZVJlbGVhc2UodmVyc2lvbik7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICAvLyBjcmVhdGUgcmVsZWFzZSBvbiBHaXRIdWIuY29tXG4gICAgZnVuY3Rpb24gY3JlYXRlUmVsZWFzZSh2ZXJzaW9uKSB7XG4gICAgICBpZih2ZXJzaW9uKSB7XG4gICAgICAgIHJlbGVhc2VPcHRpb25zLnRhcmdldF9jb21taXRpc2ggPSB2ZXJzaW9uO1xuICAgICAgfVxuICAgICAgZ2l0aHViLnJlbGVhc2VzLmNyZWF0ZVJlbGVhc2UocmVsZWFzZU9wdGlvbnMsIGZ1bmN0aW9uKCkge1xuICAgICAgICBuZXh0UmVwbygpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLy8gU3RlcHMgdG8gbmV4dCByZXBvc2l0b3J5XG4gICAgZnVuY3Rpb24gbmV4dFJlcG8oKSB7XG4gICAgICBjb25zb2xlLmxvZygnU2xlZXBpbmcgZm9yIDEgc2Vjb25kLi4uJyk7XG4gICAgICAvLyBhdm9pZCByYXRlIHRocm90dGxpbmdcbiAgICAgIGdsb2JhbC5jbGVhclRpbWVvdXQodGltZXIpO1xuICAgICAgdGltZXIgPSBnbG9iYWwuc2V0VGltZW91dChzdGVwUmVwbywgMTAwMCk7XG4gICAgfVxuXG5cbiAgICBpZihsb2NhbFJlcG9TZXR1cCkge1xuICAgICAgc2V0Q29uZmlnKCk7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgY29uc29sZS5lcnJvcignUmVwb3NpdG9yeSBtdXN0IGJlIHNldHVwIGJlZm9yZSBydW5uaW5nIHVwZGF0ZSBjb21wb25lbnRzJyk7XG4gICAgfVxuXG4gIH07XG5cbiAgc3RlcFJlcG8oKTtcblxufTtcbiJdfQ==