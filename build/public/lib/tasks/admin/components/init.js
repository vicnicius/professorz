/*******************************
          Init Repos
*******************************/

/*

 This task pulls the latest version of each component from GitHub

  * Creates new repo if doesnt exist (locally & GitHub)
  * Adds remote it doesnt exists
  * Pulls latest changes from repo

*/

var gulp = require('gulp'),


// node dependencies
console = require('better-console'),
    del = require('del'),
    fs = require('fs'),
    path = require('path'),
    git = require('gulp-git'),
    githubAPI = require('github'),
    mkdirp = require('mkdirp'),


// admin files
github = require('../../config/admin/github.js'),
    release = require('../../config/admin/release'),
    project = require('../../config/project/release'),


// oAuth configuration for GitHub
oAuth = fs.existsSync(__dirname + '/../../config/admin/oauth.js') ? require('../../config/admin/oauth') : false,


// shorthand
version = project.version;

module.exports = function (callback) {

  var index = -1,
      total = release.components.length,
      timer,
      stream,
      stepRepo;

  if (!oAuth) {
    console.error('Must add oauth token for GitHub in tasks/config/admin/oauth.js');
    return;
  }

  // Do Git commands synchronously per component, to avoid issues
  stepRepo = function () {

    index = index + 1;

    if (index >= total) {
      callback();
      return;
    }

    var component = release.components[index];
    outputDirectory = path.resolve(release.outputRoot + component), capitalizedComponent = component.charAt(0).toUpperCase() + component.slice(1), repoName = release.componentRepoRoot + capitalizedComponent, gitOptions = { cwd: outputDirectory }, pullOptions = { args: '-q', cwd: outputDirectory, quiet: true }, resetOptions = { args: '-q --hard', cwd: outputDirectory, quiet: true }, gitURL = 'https://github.com/' + release.org + '/' + repoName + '.git', repoURL = 'https://github.com/' + release.org + '/' + repoName + '/', localRepoSetup = fs.existsSync(path.join(outputDirectory, '.git'));

    console.log('Processing repository: ' + outputDirectory);

    // create folder if doesn't exist
    if (!fs.existsSync(outputDirectory)) {
      mkdirp.sync(outputDirectory);
    }

    // clean folder
    if (release.outputRoot.search('../repos') == 0) {
      console.info('Cleaning dir', outputDirectory);
      del.sync([outputDirectory + '**/*'], { silent: true, force: true });
    }

    // set-up local repo
    function setupRepo() {
      if (localRepoSetup) {
        addRemote();
      } else {
        initRepo();
      }
    }

    function initRepo() {
      console.info('Initializing repository for ' + component);
      git.init(gitOptions, function (error) {
        if (error) {
          console.error('Error initializing repo', error);
        }
        addRemote();
      });
    }

    function createRepo() {
      console.info('Creating GitHub repo ' + repoURL);
      github.repos.createFromOrg({
        org: release.org,
        name: repoName,
        homepage: release.homepage
      }, function () {
        setupRepo();
      });
    }

    function addRemote() {
      console.info('Adding remote origin as ' + gitURL);
      git.addRemote('origin', gitURL, gitOptions, function () {
        pullFiles();
      });
    }

    function pullFiles() {
      console.info('Pulling ' + component + ' files');
      git.pull('origin', 'master', pullOptions, function (error) {
        resetFiles();
      });
    }

    function resetFiles() {
      console.info('Resetting files to head');
      git.reset('HEAD', resetOptions, function (error) {
        nextRepo();
      });
    }

    function nextRepo() {
      //console.log('Sleeping for 1 second...');
      // avoid rate throttling
      global.clearTimeout(timer);
      timer = global.setTimeout(function () {
        stepRepo();
      }, 0);
    }

    if (localRepoSetup) {
      pullFiles();
    } else {
      setupRepo();
      // createRepo() only use to create remote repo (easier to do manually)
    }
  };

  stepRepo();
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2FkbWluL2NvbXBvbmVudHMvaW5pdC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQWNBLElBQ0UsT0FBWSxRQUFRLE1BQVIsQ0FEZDtBQUFBOzs7QUFJRSxVQUFZLFFBQVEsZ0JBQVIsQ0FKZDtBQUFBLElBS0UsTUFBWSxRQUFRLEtBQVIsQ0FMZDtBQUFBLElBTUUsS0FBWSxRQUFRLElBQVIsQ0FOZDtBQUFBLElBT0UsT0FBWSxRQUFRLE1BQVIsQ0FQZDtBQUFBLElBUUUsTUFBWSxRQUFRLFVBQVIsQ0FSZDtBQUFBLElBU0UsWUFBWSxRQUFRLFFBQVIsQ0FUZDtBQUFBLElBVUUsU0FBWSxRQUFRLFFBQVIsQ0FWZDtBQUFBOzs7QUFhRSxTQUFZLFFBQVEsOEJBQVIsQ0FiZDtBQUFBLElBY0UsVUFBWSxRQUFRLDRCQUFSLENBZGQ7QUFBQSxJQWVFLFVBQVksUUFBUSw4QkFBUixDQWZkO0FBQUE7OztBQW1CRSxRQUFZLEdBQUcsVUFBSCxDQUFjLFlBQVksOEJBQTFCLElBQ1IsUUFBUSwwQkFBUixDQURRLEdBRVIsS0FyQk47QUFBQTs7O0FBd0JFLFVBQWtCLFFBQVEsT0F4QjVCOztBQTJCQSxPQUFPLE9BQVAsR0FBaUIsVUFBUyxRQUFULEVBQW1COztBQUVsQyxNQUNFLFFBQVEsQ0FBQyxDQURYO0FBQUEsTUFFRSxRQUFRLFFBQVEsVUFBUixDQUFtQixNQUY3QjtBQUFBLE1BR0UsS0FIRjtBQUFBLE1BSUUsTUFKRjtBQUFBLE1BS0UsUUFMRjs7QUFRQSxNQUFHLENBQUMsS0FBSixFQUFXO0FBQ1QsWUFBUSxLQUFSLENBQWMsZ0VBQWQ7QUFDQTtBQUNEOzs7QUFHRCxhQUFXLFlBQVc7O0FBRXBCLFlBQVEsUUFBUSxDQUFoQjs7QUFFQSxRQUFHLFNBQVMsS0FBWixFQUFtQjtBQUNqQjtBQUNBO0FBQ0Q7O0FBRUQsUUFDRSxZQUF1QixRQUFRLFVBQVIsQ0FBbUIsS0FBbkIsQ0FEekI7QUFFRSxzQkFBdUIsS0FBSyxPQUFMLENBQWEsUUFBUSxVQUFSLEdBQXFCLFNBQWxDLENBQXZCLEVBQ0EsdUJBQXVCLFVBQVUsTUFBVixDQUFpQixDQUFqQixFQUFvQixXQUFwQixLQUFvQyxVQUFVLEtBQVYsQ0FBZ0IsQ0FBaEIsQ0FEM0QsRUFFQSxXQUF1QixRQUFRLGlCQUFSLEdBQTRCLG9CQUZuRCxFQUlBLGFBQXVCLEVBQUUsS0FBSyxlQUFQLEVBSnZCLEVBS0EsY0FBdUIsRUFBRSxNQUFNLElBQVIsRUFBYyxLQUFLLGVBQW5CLEVBQW9DLE9BQU8sSUFBM0MsRUFMdkIsRUFNQSxlQUF1QixFQUFFLE1BQU0sV0FBUixFQUFxQixLQUFLLGVBQTFCLEVBQTJDLE9BQU8sSUFBbEQsRUFOdkIsRUFRQSxTQUF1Qix3QkFBd0IsUUFBUSxHQUFoQyxHQUFzQyxHQUF0QyxHQUE0QyxRQUE1QyxHQUF1RCxNQVI5RSxFQVNBLFVBQXVCLHdCQUF3QixRQUFRLEdBQWhDLEdBQXNDLEdBQXRDLEdBQTRDLFFBQTVDLEdBQXVELEdBVDlFLEVBVUEsaUJBQXVCLEdBQUcsVUFBSCxDQUFjLEtBQUssSUFBTCxDQUFVLGVBQVYsRUFBMkIsTUFBM0IsQ0FBZCxDQVZ2Qjs7QUFhRixZQUFRLEdBQVIsQ0FBWSw0QkFBNEIsZUFBeEM7OztBQUdBLFFBQUksQ0FBQyxHQUFHLFVBQUgsQ0FBYyxlQUFkLENBQUwsRUFBc0M7QUFDcEMsYUFBTyxJQUFQLENBQVksZUFBWjtBQUNEOzs7QUFHRCxRQUFHLFFBQVEsVUFBUixDQUFtQixNQUFuQixDQUEwQixVQUExQixLQUF5QyxDQUE1QyxFQUErQztBQUM3QyxjQUFRLElBQVIsQ0FBYSxjQUFiLEVBQTZCLGVBQTdCO0FBQ0EsVUFBSSxJQUFKLENBQVMsQ0FBQyxrQkFBa0IsTUFBbkIsQ0FBVCxFQUFxQyxFQUFDLFFBQVEsSUFBVCxFQUFlLE9BQU8sSUFBdEIsRUFBckM7QUFDRDs7O0FBR0QsYUFBUyxTQUFULEdBQXFCO0FBQ25CLFVBQUcsY0FBSCxFQUFtQjtBQUNqQjtBQUNELE9BRkQsTUFHSztBQUNIO0FBQ0Q7QUFDRjs7QUFFRCxhQUFTLFFBQVQsR0FBb0I7QUFDbEIsY0FBUSxJQUFSLENBQWEsaUNBQWlDLFNBQTlDO0FBQ0EsVUFBSSxJQUFKLENBQVMsVUFBVCxFQUFxQixVQUFTLEtBQVQsRUFBZ0I7QUFDbkMsWUFBRyxLQUFILEVBQVU7QUFDUixrQkFBUSxLQUFSLENBQWMseUJBQWQsRUFBeUMsS0FBekM7QUFDRDtBQUNEO0FBQ0QsT0FMRDtBQU1EOztBQUVELGFBQVMsVUFBVCxHQUFzQjtBQUNwQixjQUFRLElBQVIsQ0FBYSwwQkFBMEIsT0FBdkM7QUFDQSxhQUFPLEtBQVAsQ0FBYSxhQUFiLENBQTJCO0FBQ3pCLGFBQVcsUUFBUSxHQURNO0FBRXpCLGNBQVcsUUFGYztBQUd6QixrQkFBVyxRQUFRO0FBSE0sT0FBM0IsRUFJRyxZQUFXO0FBQ1o7QUFDRCxPQU5EO0FBT0Q7O0FBRUQsYUFBUyxTQUFULEdBQXFCO0FBQ25CLGNBQVEsSUFBUixDQUFhLDZCQUE2QixNQUExQztBQUNBLFVBQUksU0FBSixDQUFjLFFBQWQsRUFBd0IsTUFBeEIsRUFBZ0MsVUFBaEMsRUFBNEMsWUFBVTtBQUNwRDtBQUNELE9BRkQ7QUFHRDs7QUFFRCxhQUFTLFNBQVQsR0FBcUI7QUFDbkIsY0FBUSxJQUFSLENBQWEsYUFBYSxTQUFiLEdBQXlCLFFBQXRDO0FBQ0EsVUFBSSxJQUFKLENBQVMsUUFBVCxFQUFtQixRQUFuQixFQUE2QixXQUE3QixFQUEwQyxVQUFTLEtBQVQsRUFBZ0I7QUFDeEQ7QUFDRCxPQUZEO0FBR0Q7O0FBRUQsYUFBUyxVQUFULEdBQXNCO0FBQ3BCLGNBQVEsSUFBUixDQUFhLHlCQUFiO0FBQ0EsVUFBSSxLQUFKLENBQVUsTUFBVixFQUFrQixZQUFsQixFQUFnQyxVQUFTLEtBQVQsRUFBZ0I7QUFDOUM7QUFDRCxPQUZEO0FBR0Q7O0FBRUQsYUFBUyxRQUFULEdBQW9COzs7QUFHbEIsYUFBTyxZQUFQLENBQW9CLEtBQXBCO0FBQ0EsY0FBUSxPQUFPLFVBQVAsQ0FBa0IsWUFBVztBQUNuQztBQUNELE9BRk8sRUFFTCxDQUZLLENBQVI7QUFHRDs7QUFHRCxRQUFHLGNBQUgsRUFBbUI7QUFDakI7QUFDRCxLQUZELE1BR0s7QUFDSDs7QUFFRDtBQUVGLEdBM0dEOztBQTZHQTtBQUdELENBaElEIiwiZmlsZSI6ImluaXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgIEluaXQgUmVwb3NcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi8qXG5cbiBUaGlzIHRhc2sgcHVsbHMgdGhlIGxhdGVzdCB2ZXJzaW9uIG9mIGVhY2ggY29tcG9uZW50IGZyb20gR2l0SHViXG5cbiAgKiBDcmVhdGVzIG5ldyByZXBvIGlmIGRvZXNudCBleGlzdCAobG9jYWxseSAmIEdpdEh1YilcbiAgKiBBZGRzIHJlbW90ZSBpdCBkb2VzbnQgZXhpc3RzXG4gICogUHVsbHMgbGF0ZXN0IGNoYW5nZXMgZnJvbSByZXBvXG5cbiovXG5cbnZhclxuICBndWxwICAgICAgPSByZXF1aXJlKCdndWxwJyksXG5cbiAgLy8gbm9kZSBkZXBlbmRlbmNpZXNcbiAgY29uc29sZSAgID0gcmVxdWlyZSgnYmV0dGVyLWNvbnNvbGUnKSxcbiAgZGVsICAgICAgID0gcmVxdWlyZSgnZGVsJyksXG4gIGZzICAgICAgICA9IHJlcXVpcmUoJ2ZzJyksXG4gIHBhdGggICAgICA9IHJlcXVpcmUoJ3BhdGgnKSxcbiAgZ2l0ICAgICAgID0gcmVxdWlyZSgnZ3VscC1naXQnKSxcbiAgZ2l0aHViQVBJID0gcmVxdWlyZSgnZ2l0aHViJyksXG4gIG1rZGlycCAgICA9IHJlcXVpcmUoJ21rZGlycCcpLFxuXG4gIC8vIGFkbWluIGZpbGVzXG4gIGdpdGh1YiAgICA9IHJlcXVpcmUoJy4uLy4uL2NvbmZpZy9hZG1pbi9naXRodWIuanMnKSxcbiAgcmVsZWFzZSAgID0gcmVxdWlyZSgnLi4vLi4vY29uZmlnL2FkbWluL3JlbGVhc2UnKSxcbiAgcHJvamVjdCAgID0gcmVxdWlyZSgnLi4vLi4vY29uZmlnL3Byb2plY3QvcmVsZWFzZScpLFxuXG5cbiAgLy8gb0F1dGggY29uZmlndXJhdGlvbiBmb3IgR2l0SHViXG4gIG9BdXRoICAgICA9IGZzLmV4aXN0c1N5bmMoX19kaXJuYW1lICsgJy8uLi8uLi9jb25maWcvYWRtaW4vb2F1dGguanMnKVxuICAgID8gcmVxdWlyZSgnLi4vLi4vY29uZmlnL2FkbWluL29hdXRoJylcbiAgICA6IGZhbHNlLFxuXG4gIC8vIHNob3J0aGFuZFxuICB2ZXJzaW9uICAgICAgICAgPSBwcm9qZWN0LnZlcnNpb25cbjtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihjYWxsYmFjaykge1xuXG4gIHZhclxuICAgIGluZGV4ID0gLTEsXG4gICAgdG90YWwgPSByZWxlYXNlLmNvbXBvbmVudHMubGVuZ3RoLFxuICAgIHRpbWVyLFxuICAgIHN0cmVhbSxcbiAgICBzdGVwUmVwb1xuICA7XG5cbiAgaWYoIW9BdXRoKSB7XG4gICAgY29uc29sZS5lcnJvcignTXVzdCBhZGQgb2F1dGggdG9rZW4gZm9yIEdpdEh1YiBpbiB0YXNrcy9jb25maWcvYWRtaW4vb2F1dGguanMnKTtcbiAgICByZXR1cm47XG4gIH1cblxuICAvLyBEbyBHaXQgY29tbWFuZHMgc3luY2hyb25vdXNseSBwZXIgY29tcG9uZW50LCB0byBhdm9pZCBpc3N1ZXNcbiAgc3RlcFJlcG8gPSBmdW5jdGlvbigpIHtcblxuICAgIGluZGV4ID0gaW5kZXggKyAxO1xuXG4gICAgaWYoaW5kZXggPj0gdG90YWwpIHtcbiAgICAgIGNhbGxiYWNrKCk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyXG4gICAgICBjb21wb25lbnQgICAgICAgICAgICA9IHJlbGVhc2UuY29tcG9uZW50c1tpbmRleF1cbiAgICAgIG91dHB1dERpcmVjdG9yeSAgICAgID0gcGF0aC5yZXNvbHZlKHJlbGVhc2Uub3V0cHV0Um9vdCArIGNvbXBvbmVudCksXG4gICAgICBjYXBpdGFsaXplZENvbXBvbmVudCA9IGNvbXBvbmVudC5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIGNvbXBvbmVudC5zbGljZSgxKSxcbiAgICAgIHJlcG9OYW1lICAgICAgICAgICAgID0gcmVsZWFzZS5jb21wb25lbnRSZXBvUm9vdCArIGNhcGl0YWxpemVkQ29tcG9uZW50LFxuXG4gICAgICBnaXRPcHRpb25zICAgICAgICAgICA9IHsgY3dkOiBvdXRwdXREaXJlY3RvcnkgfSxcbiAgICAgIHB1bGxPcHRpb25zICAgICAgICAgID0geyBhcmdzOiAnLXEnLCBjd2Q6IG91dHB1dERpcmVjdG9yeSwgcXVpZXQ6IHRydWUgfSxcbiAgICAgIHJlc2V0T3B0aW9ucyAgICAgICAgID0geyBhcmdzOiAnLXEgLS1oYXJkJywgY3dkOiBvdXRwdXREaXJlY3RvcnksIHF1aWV0OiB0cnVlIH0sXG5cbiAgICAgIGdpdFVSTCAgICAgICAgICAgICAgID0gJ2h0dHBzOi8vZ2l0aHViLmNvbS8nICsgcmVsZWFzZS5vcmcgKyAnLycgKyByZXBvTmFtZSArICcuZ2l0JyxcbiAgICAgIHJlcG9VUkwgICAgICAgICAgICAgID0gJ2h0dHBzOi8vZ2l0aHViLmNvbS8nICsgcmVsZWFzZS5vcmcgKyAnLycgKyByZXBvTmFtZSArICcvJyxcbiAgICAgIGxvY2FsUmVwb1NldHVwICAgICAgID0gZnMuZXhpc3RzU3luYyhwYXRoLmpvaW4ob3V0cHV0RGlyZWN0b3J5LCAnLmdpdCcpKVxuICAgIDtcblxuICAgIGNvbnNvbGUubG9nKCdQcm9jZXNzaW5nIHJlcG9zaXRvcnk6ICcgKyBvdXRwdXREaXJlY3RvcnkpO1xuXG4gICAgLy8gY3JlYXRlIGZvbGRlciBpZiBkb2Vzbid0IGV4aXN0XG4gICAgaWYoICFmcy5leGlzdHNTeW5jKG91dHB1dERpcmVjdG9yeSkgKSB7XG4gICAgICBta2RpcnAuc3luYyhvdXRwdXREaXJlY3RvcnkpO1xuICAgIH1cblxuICAgIC8vIGNsZWFuIGZvbGRlclxuICAgIGlmKHJlbGVhc2Uub3V0cHV0Um9vdC5zZWFyY2goJy4uL3JlcG9zJykgPT0gMCkge1xuICAgICAgY29uc29sZS5pbmZvKCdDbGVhbmluZyBkaXInLCBvdXRwdXREaXJlY3RvcnkpO1xuICAgICAgZGVsLnN5bmMoW291dHB1dERpcmVjdG9yeSArICcqKi8qJ10sIHtzaWxlbnQ6IHRydWUsIGZvcmNlOiB0cnVlfSk7XG4gICAgfVxuXG4gICAgLy8gc2V0LXVwIGxvY2FsIHJlcG9cbiAgICBmdW5jdGlvbiBzZXR1cFJlcG8oKSB7XG4gICAgICBpZihsb2NhbFJlcG9TZXR1cCkge1xuICAgICAgICBhZGRSZW1vdGUoKTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBpbml0UmVwbygpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGluaXRSZXBvKCkge1xuICAgICAgY29uc29sZS5pbmZvKCdJbml0aWFsaXppbmcgcmVwb3NpdG9yeSBmb3IgJyArIGNvbXBvbmVudCk7XG4gICAgICBnaXQuaW5pdChnaXRPcHRpb25zLCBmdW5jdGlvbihlcnJvcikge1xuICAgICAgICBpZihlcnJvcikge1xuICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIGluaXRpYWxpemluZyByZXBvJywgZXJyb3IpO1xuICAgICAgICB9XG4gICAgICAgIGFkZFJlbW90ZSgpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY3JlYXRlUmVwbygpIHtcbiAgICAgIGNvbnNvbGUuaW5mbygnQ3JlYXRpbmcgR2l0SHViIHJlcG8gJyArIHJlcG9VUkwpO1xuICAgICAgZ2l0aHViLnJlcG9zLmNyZWF0ZUZyb21Pcmcoe1xuICAgICAgICBvcmcgICAgICA6IHJlbGVhc2Uub3JnLFxuICAgICAgICBuYW1lICAgICA6IHJlcG9OYW1lLFxuICAgICAgICBob21lcGFnZSA6IHJlbGVhc2UuaG9tZXBhZ2VcbiAgICAgIH0sIGZ1bmN0aW9uKCkge1xuICAgICAgICBzZXR1cFJlcG8oKTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGFkZFJlbW90ZSgpIHtcbiAgICAgIGNvbnNvbGUuaW5mbygnQWRkaW5nIHJlbW90ZSBvcmlnaW4gYXMgJyArIGdpdFVSTCk7XG4gICAgICBnaXQuYWRkUmVtb3RlKCdvcmlnaW4nLCBnaXRVUkwsIGdpdE9wdGlvbnMsIGZ1bmN0aW9uKCl7XG4gICAgICAgIHB1bGxGaWxlcygpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcHVsbEZpbGVzKCkge1xuICAgICAgY29uc29sZS5pbmZvKCdQdWxsaW5nICcgKyBjb21wb25lbnQgKyAnIGZpbGVzJyk7XG4gICAgICBnaXQucHVsbCgnb3JpZ2luJywgJ21hc3RlcicsIHB1bGxPcHRpb25zLCBmdW5jdGlvbihlcnJvcikge1xuICAgICAgICByZXNldEZpbGVzKCk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiByZXNldEZpbGVzKCkge1xuICAgICAgY29uc29sZS5pbmZvKCdSZXNldHRpbmcgZmlsZXMgdG8gaGVhZCcpO1xuICAgICAgZ2l0LnJlc2V0KCdIRUFEJywgcmVzZXRPcHRpb25zLCBmdW5jdGlvbihlcnJvcikge1xuICAgICAgICBuZXh0UmVwbygpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gbmV4dFJlcG8oKSB7XG4gICAgICAvL2NvbnNvbGUubG9nKCdTbGVlcGluZyBmb3IgMSBzZWNvbmQuLi4nKTtcbiAgICAgIC8vIGF2b2lkIHJhdGUgdGhyb3R0bGluZ1xuICAgICAgZ2xvYmFsLmNsZWFyVGltZW91dCh0aW1lcik7XG4gICAgICB0aW1lciA9IGdsb2JhbC5zZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICBzdGVwUmVwbygpXG4gICAgICB9LCAwKTtcbiAgICB9XG5cblxuICAgIGlmKGxvY2FsUmVwb1NldHVwKSB7XG4gICAgICBwdWxsRmlsZXMoKTtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICBzZXR1cFJlcG8oKTtcbiAgICAgIC8vIGNyZWF0ZVJlcG8oKSBvbmx5IHVzZSB0byBjcmVhdGUgcmVtb3RlIHJlcG8gKGVhc2llciB0byBkbyBtYW51YWxseSlcbiAgICB9XG5cbiAgfTtcblxuICBzdGVwUmVwbygpO1xuXG5cbn07XG4iXX0=