/*******************************
     Create Component Repos
*******************************/

/*
 This will create individual component repositories for each SUI component

  * copy component files from release
  * create commonjs files as index.js for NPM release
  * create release notes that filter only items related to component
  * custom package.json file from template
  * create bower.json from template
  * create README from template
  * create meteor.js file
*/

var gulp = require('gulp'),


// node dependencies
console = require('better-console'),
    del = require('del'),
    fs = require('fs'),
    path = require('path'),
    runSequence = require('run-sequence'),


// admin dependencies
concatFileNames = require('gulp-concat-filenames'),
    debug = require('gulp-debug'),
    flatten = require('gulp-flatten'),
    git = require('gulp-git'),
    jsonEditor = require('gulp-json-editor'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    replace = require('gulp-replace'),
    tap = require('gulp-tap'),
    util = require('gulp-util'),


// config
config = require('../../config/user'),
    release = require('../../config/admin/release'),
    project = require('../../config/project/release'),


// shorthand
version = project.version,
    output = config.paths.output;

module.exports = function (callback) {
  var stream,
      index,
      tasks = [];

  for (index in release.components) {

    var component = release.components[index];

    // streams... designed to save time and make coding fun...
    (function (component) {

      var outputDirectory = path.join(release.outputRoot, component),
          isJavascript = fs.existsSync(output.compressed + component + '.js'),
          isCSS = fs.existsSync(output.compressed + component + '.css'),
          capitalizedComponent = component.charAt(0).toUpperCase() + component.slice(1),
          packageName = release.packageRoot + component,
          repoName = release.componentRepoRoot + capitalizedComponent,
          gitURL = 'https://github.com/' + release.org + '/' + repoName + '.git',
          repoURL = 'https://github.com/' + release.org + '/' + repoName + '/',
          concatSettings = {
        newline: '',
        root: outputDirectory,
        prepend: "    '",
        append: "',"
      },
          regExp = {
        match: {
          // templated values
          name: '{component}',
          titleName: '{Component}',
          version: '{version}',
          files: '{files}',
          // release notes
          spacedVersions: /(###.*\n)\n+(?=###)/gm,
          spacedLists: /(^- .*\n)\n+(?=^-)/gm,
          trim: /^\s+|\s+$/g,
          unrelatedNotes: new RegExp('^((?!(^.*(' + component + ').*$|###.*)).)*$', 'gmi'),
          whitespace: /\n\s*\n\s*\n/gm,
          // npm
          componentExport: /(.*)\$\.fn\.\w+\s*=\s*function\(([^\)]*)\)\s*{/g,
          componentReference: '$.fn.' + component,
          settingsExport: /\$\.fn\.\w+\.settings\s*=/g,
          settingsReference: /\$\.fn\.\w+\.settings/g,
          trailingComma: /,(?=[^,]*$)/,
          jQuery: /jQuery/g
        },
        replace: {
          // readme
          name: component,
          titleName: capitalizedComponent,
          // release notes
          spacedVersions: '',
          spacedLists: '$1',
          trim: '',
          unrelatedNotes: '',
          whitespace: '\n\n',
          // npm
          componentExport: 'var _module = module;\n$1module.exports = function($2) {',
          componentReference: '_module.exports',
          settingsExport: 'module.exports.settings =',
          settingsReference: '_module.exports.settings',
          jQuery: 'require("jquery")'
        }
      },
          task = {
        all: component + ' creating',
        repo: component + ' create repo',
        bower: component + ' create bower.json',
        readme: component + ' create README',
        npm: component + ' create NPM Module',
        notes: component + ' create release notes',
        composer: component + ' create composer.json',
        package: component + ' create package.json',
        meteor: component + ' create meteor package.js'
      },

      // paths to includable assets
      manifest = {
        assets: outputDirectory + '/assets/**/' + component + '?(s).*',
        component: outputDirectory + '/' + component + '+(.js|.css)'
      };

      // copy dist files into output folder adjusting asset paths
      gulp.task(task.repo, false, function () {
        return gulp.src(release.source + component + '.*').pipe(plumber()).pipe(flatten()).pipe(replace(release.paths.source, release.paths.output)).pipe(gulp.dest(outputDirectory));
      });

      // create npm module
      gulp.task(task.npm, false, function () {
        return gulp.src(release.source + component + '!(*.min|*.map).js').pipe(plumber()).pipe(flatten()).pipe(replace(regExp.match.componentExport, regExp.replace.componentExport)).pipe(replace(regExp.match.componentReference, regExp.replace.componentReference)).pipe(replace(regExp.match.settingsExport, regExp.replace.settingsExport)).pipe(replace(regExp.match.settingsReference, regExp.replace.settingsReference)).pipe(replace(regExp.match.jQuery, regExp.replace.jQuery)).pipe(rename('index.js')).pipe(gulp.dest(outputDirectory));
      });

      // create readme
      gulp.task(task.readme, false, function () {
        return gulp.src(release.templates.readme).pipe(plumber()).pipe(flatten()).pipe(replace(regExp.match.name, regExp.replace.name)).pipe(replace(regExp.match.titleName, regExp.replace.titleName)).pipe(gulp.dest(outputDirectory));
      });

      // extend bower.json
      gulp.task(task.bower, false, function () {
        return gulp.src(release.templates.bower).pipe(plumber()).pipe(flatten()).pipe(jsonEditor(function (bower) {
          bower.name = packageName;
          bower.description = capitalizedComponent + ' - Semantic UI';
          if (isJavascript) {
            if (isCSS) {
              bower.main = [component + '.js', component + '.css'];
            } else {
              bower.main = [component + '.js'];
            }
            bower.dependencies = {
              jquery: '>=1.8'
            };
          } else {
            bower.main = [component + '.css'];
          }
          return bower;
        })).pipe(gulp.dest(outputDirectory));
      });

      // extend package.json
      gulp.task(task.package, false, function () {
        return gulp.src(release.templates.package).pipe(plumber()).pipe(flatten()).pipe(jsonEditor(function (npm) {
          if (isJavascript) {
            npm.dependencies = {
              jquery: 'x.x.x'
            };
            npm.main = 'index.js';
          }
          npm.name = packageName;
          if (version) {
            npm.version = version;
          }
          npm.title = 'Semantic UI - ' + capitalizedComponent;
          npm.description = 'Single component release of ' + component;
          npm.repository = {
            type: 'git',
            url: gitURL
          };
          return npm;
        })).pipe(gulp.dest(outputDirectory));
      });

      // extend composer.json
      gulp.task(task.composer, false, function () {
        return gulp.src(release.templates.composer).pipe(plumber()).pipe(flatten()).pipe(jsonEditor(function (composer) {
          if (isJavascript) {
            composer.dependencies = {
              jquery: 'x.x.x'
            };
            composer.main = component + '.js';
          }
          composer.name = 'semantic/' + component;
          if (version) {
            composer.version = version;
          }
          composer.description = 'Single component release of ' + component;
          return composer;
        })).pipe(gulp.dest(outputDirectory));
      });

      // create release notes
      gulp.task(task.notes, false, function () {
        return gulp.src(release.templates.notes).pipe(plumber()).pipe(flatten())
        // Remove release notes for lines not mentioning component
        .pipe(replace(regExp.match.unrelatedNotes, regExp.replace.unrelatedNotes)).pipe(replace(regExp.match.whitespace, regExp.replace.whitespace)).pipe(replace(regExp.match.spacedVersions, regExp.replace.spacedVersions)).pipe(replace(regExp.match.spacedLists, regExp.replace.spacedLists)).pipe(replace(regExp.match.trim, regExp.replace.trim)).pipe(gulp.dest(outputDirectory));
      });

      // Creates meteor package.js
      gulp.task(task.meteor, function () {
        var filenames = '';
        return gulp.src(manifest.component).pipe(concatFileNames('empty.txt', concatSettings)).pipe(tap(function (file) {
          filenames += file.contents;
        })).on('end', function () {
          gulp.src(manifest.assets).pipe(concatFileNames('empty.txt', concatSettings)).pipe(tap(function (file) {
            filenames += file.contents;
          })).on('end', function () {
            // remove trailing slash
            filenames = filenames.replace(regExp.match.trailingComma, '').trim();
            gulp.src(release.templates.meteor.component).pipe(plumber()).pipe(flatten()).pipe(replace(regExp.match.name, regExp.replace.name)).pipe(replace(regExp.match.titleName, regExp.replace.titleName)).pipe(replace(regExp.match.version, version)).pipe(replace(regExp.match.files, filenames)).pipe(rename(release.files.meteor)).pipe(gulp.dest(outputDirectory));
          });
        });
      });

      // synchronous tasks in orchestrator? I think not
      gulp.task(task.all, false, function (callback) {
        runSequence([task.repo, task.npm, task.bower, task.readme, task.package, task.composer, task.notes, task.meteor], callback);
      });

      tasks.push(task.all);
    })(component);
  }

  runSequence(tasks, callback);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2FkbWluL2NvbXBvbmVudHMvY3JlYXRlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkEsSUFDRSxPQUFrQixRQUFRLE1BQVIsQ0FEcEI7QUFBQTs7O0FBSUUsVUFBa0IsUUFBUSxnQkFBUixDQUpwQjtBQUFBLElBS0UsTUFBa0IsUUFBUSxLQUFSLENBTHBCO0FBQUEsSUFNRSxLQUFrQixRQUFRLElBQVIsQ0FOcEI7QUFBQSxJQU9FLE9BQWtCLFFBQVEsTUFBUixDQVBwQjtBQUFBLElBUUUsY0FBa0IsUUFBUSxjQUFSLENBUnBCO0FBQUE7OztBQVdFLGtCQUFrQixRQUFRLHVCQUFSLENBWHBCO0FBQUEsSUFZRSxRQUFrQixRQUFRLFlBQVIsQ0FacEI7QUFBQSxJQWFFLFVBQWtCLFFBQVEsY0FBUixDQWJwQjtBQUFBLElBY0UsTUFBa0IsUUFBUSxVQUFSLENBZHBCO0FBQUEsSUFlRSxhQUFrQixRQUFRLGtCQUFSLENBZnBCO0FBQUEsSUFnQkUsVUFBa0IsUUFBUSxjQUFSLENBaEJwQjtBQUFBLElBaUJFLFNBQWtCLFFBQVEsYUFBUixDQWpCcEI7QUFBQSxJQWtCRSxVQUFrQixRQUFRLGNBQVIsQ0FsQnBCO0FBQUEsSUFtQkUsTUFBa0IsUUFBUSxVQUFSLENBbkJwQjtBQUFBLElBb0JFLE9BQWtCLFFBQVEsV0FBUixDQXBCcEI7QUFBQTs7O0FBdUJFLFNBQWtCLFFBQVEsbUJBQVIsQ0F2QnBCO0FBQUEsSUF3QkUsVUFBa0IsUUFBUSw0QkFBUixDQXhCcEI7QUFBQSxJQXlCRSxVQUFrQixRQUFRLDhCQUFSLENBekJwQjtBQUFBOzs7QUE0QkUsVUFBa0IsUUFBUSxPQTVCNUI7QUFBQSxJQTZCRSxTQUFrQixPQUFPLEtBQVAsQ0FBYSxNQTdCakM7O0FBa0NBLE9BQU8sT0FBUCxHQUFpQixVQUFTLFFBQVQsRUFBbUI7QUFDbEMsTUFDRSxNQURGO0FBQUEsTUFFRSxLQUZGO0FBQUEsTUFHRSxRQUFRLEVBSFY7O0FBTUEsT0FBSSxLQUFKLElBQWEsUUFBUSxVQUFyQixFQUFpQzs7QUFFL0IsUUFDRSxZQUFZLFFBQVEsVUFBUixDQUFtQixLQUFuQixDQURkOzs7QUFLQSxLQUFDLFVBQVMsU0FBVCxFQUFvQjs7QUFFbkIsVUFDRSxrQkFBdUIsS0FBSyxJQUFMLENBQVUsUUFBUSxVQUFsQixFQUE4QixTQUE5QixDQUR6QjtBQUFBLFVBRUUsZUFBdUIsR0FBRyxVQUFILENBQWMsT0FBTyxVQUFQLEdBQW9CLFNBQXBCLEdBQWdDLEtBQTlDLENBRnpCO0FBQUEsVUFHRSxRQUF1QixHQUFHLFVBQUgsQ0FBYyxPQUFPLFVBQVAsR0FBb0IsU0FBcEIsR0FBZ0MsTUFBOUMsQ0FIekI7QUFBQSxVQUlFLHVCQUF1QixVQUFVLE1BQVYsQ0FBaUIsQ0FBakIsRUFBb0IsV0FBcEIsS0FBb0MsVUFBVSxLQUFWLENBQWdCLENBQWhCLENBSjdEO0FBQUEsVUFLRSxjQUF1QixRQUFRLFdBQVIsR0FBc0IsU0FML0M7QUFBQSxVQU1FLFdBQXVCLFFBQVEsaUJBQVIsR0FBNEIsb0JBTnJEO0FBQUEsVUFPRSxTQUF1Qix3QkFBd0IsUUFBUSxHQUFoQyxHQUFzQyxHQUF0QyxHQUE0QyxRQUE1QyxHQUF1RCxNQVBoRjtBQUFBLFVBUUUsVUFBdUIsd0JBQXdCLFFBQVEsR0FBaEMsR0FBc0MsR0FBdEMsR0FBNEMsUUFBNUMsR0FBdUQsR0FSaEY7QUFBQSxVQVNFLGlCQUFpQjtBQUNmLGlCQUFVLEVBREs7QUFFZixjQUFVLGVBRks7QUFHZixpQkFBVSxPQUhLO0FBSWYsZ0JBQVU7QUFKSyxPQVRuQjtBQUFBLFVBZUUsU0FBdUI7QUFDckIsZUFBbUI7O0FBRWpCLGdCQUFZLGFBRks7QUFHakIscUJBQVksYUFISztBQUlqQixtQkFBWSxXQUpLO0FBS2pCLGlCQUFZLFNBTEs7O0FBT2pCLDBCQUFvQix1QkFQSDtBQVFqQix1QkFBb0Isc0JBUkg7QUFTakIsZ0JBQW9CLFlBVEg7QUFVakIsMEJBQW9CLElBQUksTUFBSixDQUFXLGVBQWUsU0FBZixHQUEyQixrQkFBdEMsRUFBMEQsS0FBMUQsQ0FWSDtBQVdqQixzQkFBb0IsZ0JBWEg7O0FBYWpCLDJCQUFvQixpREFiSDtBQWNqQiw4QkFBb0IsVUFBVSxTQWRiO0FBZWpCLDBCQUFvQiw0QkFmSDtBQWdCakIsNkJBQW9CLHdCQWhCSDtBQWlCakIseUJBQW9CLGFBakJIO0FBa0JqQixrQkFBb0I7QUFsQkgsU0FERTtBQXFCckIsaUJBQVU7O0FBRVIsZ0JBQW9CLFNBRlo7QUFHUixxQkFBb0Isb0JBSFo7O0FBS1IsMEJBQW9CLEVBTFo7QUFNUix1QkFBb0IsSUFOWjtBQU9SLGdCQUFvQixFQVBaO0FBUVIsMEJBQW9CLEVBUlo7QUFTUixzQkFBb0IsTUFUWjs7QUFXUiwyQkFBcUIsMERBWGI7QUFZUiw4QkFBcUIsaUJBWmI7QUFhUiwwQkFBcUIsMkJBYmI7QUFjUiw2QkFBcUIsMEJBZGI7QUFlUixrQkFBcUI7QUFmYjtBQXJCVyxPQWZ6QjtBQUFBLFVBc0RFLE9BQU87QUFDTCxhQUFXLFlBQVksV0FEbEI7QUFFTCxjQUFXLFlBQVksY0FGbEI7QUFHTCxlQUFXLFlBQVksb0JBSGxCO0FBSUwsZ0JBQVcsWUFBWSxnQkFKbEI7QUFLTCxhQUFXLFlBQVksb0JBTGxCO0FBTUwsZUFBVyxZQUFZLHVCQU5sQjtBQU9MLGtCQUFXLFlBQVksdUJBUGxCO0FBUUwsaUJBQVcsWUFBWSxzQkFSbEI7QUFTTCxnQkFBVyxZQUFZO0FBVGxCLE9BdERUO0FBQUE7O0FBa0VFLGlCQUFXO0FBQ1QsZ0JBQVksa0JBQWtCLGFBQWxCLEdBQWtDLFNBQWxDLEdBQThDLFFBRGpEO0FBRVQsbUJBQVksa0JBQWtCLEdBQWxCLEdBQXdCLFNBQXhCLEdBQW9DO0FBRnZDLE9BbEViOzs7QUF5RUEsV0FBSyxJQUFMLENBQVUsS0FBSyxJQUFmLEVBQXFCLEtBQXJCLEVBQTRCLFlBQVc7QUFDckMsZUFBTyxLQUFLLEdBQUwsQ0FBUyxRQUFRLE1BQVIsR0FBaUIsU0FBakIsR0FBNkIsSUFBdEMsRUFDSixJQURJLENBQ0MsU0FERCxFQUVKLElBRkksQ0FFQyxTQUZELEVBR0osSUFISSxDQUdDLFFBQVEsUUFBUSxLQUFSLENBQWMsTUFBdEIsRUFBOEIsUUFBUSxLQUFSLENBQWMsTUFBNUMsQ0FIRCxFQUlKLElBSkksQ0FJQyxLQUFLLElBQUwsQ0FBVSxlQUFWLENBSkQsQ0FBUDtBQU1ELE9BUEQ7OztBQVVBLFdBQUssSUFBTCxDQUFVLEtBQUssR0FBZixFQUFvQixLQUFwQixFQUEyQixZQUFXO0FBQ3BDLGVBQU8sS0FBSyxHQUFMLENBQVMsUUFBUSxNQUFSLEdBQWlCLFNBQWpCLEdBQTZCLG1CQUF0QyxFQUNKLElBREksQ0FDQyxTQURELEVBRUosSUFGSSxDQUVDLFNBRkQsRUFHSixJQUhJLENBR0MsUUFBUSxPQUFPLEtBQVAsQ0FBYSxlQUFyQixFQUFzQyxPQUFPLE9BQVAsQ0FBZSxlQUFyRCxDQUhELEVBSUosSUFKSSxDQUlDLFFBQVEsT0FBTyxLQUFQLENBQWEsa0JBQXJCLEVBQXlDLE9BQU8sT0FBUCxDQUFlLGtCQUF4RCxDQUpELEVBS0osSUFMSSxDQUtDLFFBQVEsT0FBTyxLQUFQLENBQWEsY0FBckIsRUFBcUMsT0FBTyxPQUFQLENBQWUsY0FBcEQsQ0FMRCxFQU1KLElBTkksQ0FNQyxRQUFRLE9BQU8sS0FBUCxDQUFhLGlCQUFyQixFQUF3QyxPQUFPLE9BQVAsQ0FBZSxpQkFBdkQsQ0FORCxFQU9KLElBUEksQ0FPQyxRQUFRLE9BQU8sS0FBUCxDQUFhLE1BQXJCLEVBQTZCLE9BQU8sT0FBUCxDQUFlLE1BQTVDLENBUEQsRUFRSixJQVJJLENBUUMsT0FBTyxVQUFQLENBUkQsRUFTSixJQVRJLENBU0MsS0FBSyxJQUFMLENBQVUsZUFBVixDQVRELENBQVA7QUFXRCxPQVpEOzs7QUFlQSxXQUFLLElBQUwsQ0FBVSxLQUFLLE1BQWYsRUFBdUIsS0FBdkIsRUFBOEIsWUFBVztBQUN2QyxlQUFPLEtBQUssR0FBTCxDQUFTLFFBQVEsU0FBUixDQUFrQixNQUEzQixFQUNKLElBREksQ0FDQyxTQURELEVBRUosSUFGSSxDQUVDLFNBRkQsRUFHSixJQUhJLENBR0MsUUFBUSxPQUFPLEtBQVAsQ0FBYSxJQUFyQixFQUEyQixPQUFPLE9BQVAsQ0FBZSxJQUExQyxDQUhELEVBSUosSUFKSSxDQUlDLFFBQVEsT0FBTyxLQUFQLENBQWEsU0FBckIsRUFBZ0MsT0FBTyxPQUFQLENBQWUsU0FBL0MsQ0FKRCxFQUtKLElBTEksQ0FLQyxLQUFLLElBQUwsQ0FBVSxlQUFWLENBTEQsQ0FBUDtBQU9ELE9BUkQ7OztBQVdBLFdBQUssSUFBTCxDQUFVLEtBQUssS0FBZixFQUFzQixLQUF0QixFQUE2QixZQUFXO0FBQ3RDLGVBQU8sS0FBSyxHQUFMLENBQVMsUUFBUSxTQUFSLENBQWtCLEtBQTNCLEVBQ0osSUFESSxDQUNDLFNBREQsRUFFSixJQUZJLENBRUMsU0FGRCxFQUdKLElBSEksQ0FHQyxXQUFXLFVBQVMsS0FBVCxFQUFnQjtBQUMvQixnQkFBTSxJQUFOLEdBQWEsV0FBYjtBQUNBLGdCQUFNLFdBQU4sR0FBb0IsdUJBQXVCLGdCQUEzQztBQUNBLGNBQUcsWUFBSCxFQUFpQjtBQUNmLGdCQUFHLEtBQUgsRUFBVTtBQUNSLG9CQUFNLElBQU4sR0FBYSxDQUNYLFlBQVksS0FERCxFQUVYLFlBQVksTUFGRCxDQUFiO0FBSUQsYUFMRCxNQU1LO0FBQ0gsb0JBQU0sSUFBTixHQUFhLENBQ1gsWUFBWSxLQURELENBQWI7QUFHRDtBQUNELGtCQUFNLFlBQU4sR0FBcUI7QUFDbkIsc0JBQVE7QUFEVyxhQUFyQjtBQUdELFdBZkQsTUFnQks7QUFDSCxrQkFBTSxJQUFOLEdBQWEsQ0FDWCxZQUFZLE1BREQsQ0FBYjtBQUdEO0FBQ0QsaUJBQU8sS0FBUDtBQUNELFNBekJLLENBSEQsRUE2QkosSUE3QkksQ0E2QkMsS0FBSyxJQUFMLENBQVUsZUFBVixDQTdCRCxDQUFQO0FBK0JELE9BaENEOzs7QUFtQ0EsV0FBSyxJQUFMLENBQVUsS0FBSyxPQUFmLEVBQXdCLEtBQXhCLEVBQStCLFlBQVc7QUFDeEMsZUFBTyxLQUFLLEdBQUwsQ0FBUyxRQUFRLFNBQVIsQ0FBa0IsT0FBM0IsRUFDSixJQURJLENBQ0MsU0FERCxFQUVKLElBRkksQ0FFQyxTQUZELEVBR0osSUFISSxDQUdDLFdBQVcsVUFBUyxHQUFULEVBQWM7QUFDN0IsY0FBRyxZQUFILEVBQWlCO0FBQ2YsZ0JBQUksWUFBSixHQUFtQjtBQUNqQixzQkFBUTtBQURTLGFBQW5CO0FBR0EsZ0JBQUksSUFBSixHQUFXLFVBQVg7QUFDRDtBQUNELGNBQUksSUFBSixHQUFXLFdBQVg7QUFDQSxjQUFHLE9BQUgsRUFBWTtBQUNWLGdCQUFJLE9BQUosR0FBYyxPQUFkO0FBQ0Q7QUFDRCxjQUFJLEtBQUosR0FBa0IsbUJBQW1CLG9CQUFyQztBQUNBLGNBQUksV0FBSixHQUFrQixpQ0FBaUMsU0FBbkQ7QUFDQSxjQUFJLFVBQUosR0FBa0I7QUFDaEIsa0JBQU8sS0FEUztBQUVoQixpQkFBTztBQUZTLFdBQWxCO0FBSUEsaUJBQU8sR0FBUDtBQUNELFNBbEJLLENBSEQsRUFzQkosSUF0QkksQ0FzQkMsS0FBSyxJQUFMLENBQVUsZUFBVixDQXRCRCxDQUFQO0FBd0JELE9BekJEOzs7QUE0QkEsV0FBSyxJQUFMLENBQVUsS0FBSyxRQUFmLEVBQXlCLEtBQXpCLEVBQWdDLFlBQVc7QUFDekMsZUFBTyxLQUFLLEdBQUwsQ0FBUyxRQUFRLFNBQVIsQ0FBa0IsUUFBM0IsRUFDSixJQURJLENBQ0MsU0FERCxFQUVKLElBRkksQ0FFQyxTQUZELEVBR0osSUFISSxDQUdDLFdBQVcsVUFBUyxRQUFULEVBQW1CO0FBQ2xDLGNBQUcsWUFBSCxFQUFpQjtBQUNmLHFCQUFTLFlBQVQsR0FBd0I7QUFDdEIsc0JBQVE7QUFEYyxhQUF4QjtBQUdBLHFCQUFTLElBQVQsR0FBZ0IsWUFBWSxLQUE1QjtBQUNEO0FBQ0QsbUJBQVMsSUFBVCxHQUFnQixjQUFjLFNBQTlCO0FBQ0EsY0FBRyxPQUFILEVBQVk7QUFDVixxQkFBUyxPQUFULEdBQW1CLE9BQW5CO0FBQ0Q7QUFDRCxtQkFBUyxXQUFULEdBQXVCLGlDQUFpQyxTQUF4RDtBQUNBLGlCQUFPLFFBQVA7QUFDRCxTQWJLLENBSEQsRUFpQkosSUFqQkksQ0FpQkMsS0FBSyxJQUFMLENBQVUsZUFBVixDQWpCRCxDQUFQO0FBbUJELE9BcEJEOzs7QUF1QkEsV0FBSyxJQUFMLENBQVUsS0FBSyxLQUFmLEVBQXNCLEtBQXRCLEVBQTZCLFlBQVc7QUFDdEMsZUFBTyxLQUFLLEdBQUwsQ0FBUyxRQUFRLFNBQVIsQ0FBa0IsS0FBM0IsRUFDSixJQURJLENBQ0MsU0FERCxFQUVKLElBRkksQ0FFQyxTQUZEOztBQUFBLFNBSUosSUFKSSxDQUlDLFFBQVEsT0FBTyxLQUFQLENBQWEsY0FBckIsRUFBcUMsT0FBTyxPQUFQLENBQWUsY0FBcEQsQ0FKRCxFQUtKLElBTEksQ0FLQyxRQUFRLE9BQU8sS0FBUCxDQUFhLFVBQXJCLEVBQWlDLE9BQU8sT0FBUCxDQUFlLFVBQWhELENBTEQsRUFNSixJQU5JLENBTUMsUUFBUSxPQUFPLEtBQVAsQ0FBYSxjQUFyQixFQUFxQyxPQUFPLE9BQVAsQ0FBZSxjQUFwRCxDQU5ELEVBT0osSUFQSSxDQU9DLFFBQVEsT0FBTyxLQUFQLENBQWEsV0FBckIsRUFBa0MsT0FBTyxPQUFQLENBQWUsV0FBakQsQ0FQRCxFQVFKLElBUkksQ0FRQyxRQUFRLE9BQU8sS0FBUCxDQUFhLElBQXJCLEVBQTJCLE9BQU8sT0FBUCxDQUFlLElBQTFDLENBUkQsRUFTSixJQVRJLENBU0MsS0FBSyxJQUFMLENBQVUsZUFBVixDQVRELENBQVA7QUFXRCxPQVpEOzs7QUFlQSxXQUFLLElBQUwsQ0FBVSxLQUFLLE1BQWYsRUFBdUIsWUFBVztBQUNoQyxZQUNFLFlBQVksRUFEZDtBQUdBLGVBQU8sS0FBSyxHQUFMLENBQVMsU0FBUyxTQUFsQixFQUNKLElBREksQ0FDQyxnQkFBZ0IsV0FBaEIsRUFBNkIsY0FBN0IsQ0FERCxFQUVKLElBRkksQ0FFQyxJQUFJLFVBQVMsSUFBVCxFQUFlO0FBQ3ZCLHVCQUFhLEtBQUssUUFBbEI7QUFDRCxTQUZLLENBRkQsRUFLSixFQUxJLENBS0QsS0FMQyxFQUtNLFlBQVc7QUFDcEIsZUFBSyxHQUFMLENBQVMsU0FBUyxNQUFsQixFQUNHLElBREgsQ0FDUSxnQkFBZ0IsV0FBaEIsRUFBNkIsY0FBN0IsQ0FEUixFQUVHLElBRkgsQ0FFUSxJQUFJLFVBQVMsSUFBVCxFQUFlO0FBQ3ZCLHlCQUFhLEtBQUssUUFBbEI7QUFDRCxXQUZLLENBRlIsRUFLRyxFQUxILENBS00sS0FMTixFQUthLFlBQVc7O0FBRXBCLHdCQUFZLFVBQVUsT0FBVixDQUFrQixPQUFPLEtBQVAsQ0FBYSxhQUEvQixFQUE4QyxFQUE5QyxFQUFrRCxJQUFsRCxFQUFaO0FBQ0EsaUJBQUssR0FBTCxDQUFTLFFBQVEsU0FBUixDQUFrQixNQUFsQixDQUF5QixTQUFsQyxFQUNHLElBREgsQ0FDUSxTQURSLEVBRUcsSUFGSCxDQUVRLFNBRlIsRUFHRyxJQUhILENBR1EsUUFBUSxPQUFPLEtBQVAsQ0FBYSxJQUFyQixFQUEyQixPQUFPLE9BQVAsQ0FBZSxJQUExQyxDQUhSLEVBSUcsSUFKSCxDQUlRLFFBQVEsT0FBTyxLQUFQLENBQWEsU0FBckIsRUFBZ0MsT0FBTyxPQUFQLENBQWUsU0FBL0MsQ0FKUixFQUtHLElBTEgsQ0FLUSxRQUFRLE9BQU8sS0FBUCxDQUFhLE9BQXJCLEVBQThCLE9BQTlCLENBTFIsRUFNRyxJQU5ILENBTVEsUUFBUSxPQUFPLEtBQVAsQ0FBYSxLQUFyQixFQUE0QixTQUE1QixDQU5SLEVBT0csSUFQSCxDQU9RLE9BQU8sUUFBUSxLQUFSLENBQWMsTUFBckIsQ0FQUixFQVFHLElBUkgsQ0FRUSxLQUFLLElBQUwsQ0FBVSxlQUFWLENBUlI7QUFVRCxXQWxCSDtBQW9CRCxTQTFCSSxDQUFQO0FBNEJELE9BaENEOzs7QUFvQ0EsV0FBSyxJQUFMLENBQVUsS0FBSyxHQUFmLEVBQW9CLEtBQXBCLEVBQTJCLFVBQVMsUUFBVCxFQUFtQjtBQUM1QyxvQkFBWSxDQUNWLEtBQUssSUFESyxFQUVWLEtBQUssR0FGSyxFQUdWLEtBQUssS0FISyxFQUlWLEtBQUssTUFKSyxFQUtWLEtBQUssT0FMSyxFQU1WLEtBQUssUUFOSyxFQU9WLEtBQUssS0FQSyxFQVFWLEtBQUssTUFSSyxDQUFaLEVBU0csUUFUSDtBQVVELE9BWEQ7O0FBYUEsWUFBTSxJQUFOLENBQVcsS0FBSyxHQUFoQjtBQUVELEtBdlFELEVBdVFHLFNBdlFIO0FBd1FEOztBQUVELGNBQVksS0FBWixFQUFtQixRQUFuQjtBQUNELENBelJEIiwiZmlsZSI6ImNyZWF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgIENyZWF0ZSBDb21wb25lbnQgUmVwb3NcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi8qXG4gVGhpcyB3aWxsIGNyZWF0ZSBpbmRpdmlkdWFsIGNvbXBvbmVudCByZXBvc2l0b3JpZXMgZm9yIGVhY2ggU1VJIGNvbXBvbmVudFxuXG4gICogY29weSBjb21wb25lbnQgZmlsZXMgZnJvbSByZWxlYXNlXG4gICogY3JlYXRlIGNvbW1vbmpzIGZpbGVzIGFzIGluZGV4LmpzIGZvciBOUE0gcmVsZWFzZVxuICAqIGNyZWF0ZSByZWxlYXNlIG5vdGVzIHRoYXQgZmlsdGVyIG9ubHkgaXRlbXMgcmVsYXRlZCB0byBjb21wb25lbnRcbiAgKiBjdXN0b20gcGFja2FnZS5qc29uIGZpbGUgZnJvbSB0ZW1wbGF0ZVxuICAqIGNyZWF0ZSBib3dlci5qc29uIGZyb20gdGVtcGxhdGVcbiAgKiBjcmVhdGUgUkVBRE1FIGZyb20gdGVtcGxhdGVcbiAgKiBjcmVhdGUgbWV0ZW9yLmpzIGZpbGVcbiovXG5cbnZhclxuICBndWxwICAgICAgICAgICAgPSByZXF1aXJlKCdndWxwJyksXG5cbiAgLy8gbm9kZSBkZXBlbmRlbmNpZXNcbiAgY29uc29sZSAgICAgICAgID0gcmVxdWlyZSgnYmV0dGVyLWNvbnNvbGUnKSxcbiAgZGVsICAgICAgICAgICAgID0gcmVxdWlyZSgnZGVsJyksXG4gIGZzICAgICAgICAgICAgICA9IHJlcXVpcmUoJ2ZzJyksXG4gIHBhdGggICAgICAgICAgICA9IHJlcXVpcmUoJ3BhdGgnKSxcbiAgcnVuU2VxdWVuY2UgICAgID0gcmVxdWlyZSgncnVuLXNlcXVlbmNlJyksXG5cbiAgLy8gYWRtaW4gZGVwZW5kZW5jaWVzXG4gIGNvbmNhdEZpbGVOYW1lcyA9IHJlcXVpcmUoJ2d1bHAtY29uY2F0LWZpbGVuYW1lcycpLFxuICBkZWJ1ZyAgICAgICAgICAgPSByZXF1aXJlKCdndWxwLWRlYnVnJyksXG4gIGZsYXR0ZW4gICAgICAgICA9IHJlcXVpcmUoJ2d1bHAtZmxhdHRlbicpLFxuICBnaXQgICAgICAgICAgICAgPSByZXF1aXJlKCdndWxwLWdpdCcpLFxuICBqc29uRWRpdG9yICAgICAgPSByZXF1aXJlKCdndWxwLWpzb24tZWRpdG9yJyksXG4gIHBsdW1iZXIgICAgICAgICA9IHJlcXVpcmUoJ2d1bHAtcGx1bWJlcicpLFxuICByZW5hbWUgICAgICAgICAgPSByZXF1aXJlKCdndWxwLXJlbmFtZScpLFxuICByZXBsYWNlICAgICAgICAgPSByZXF1aXJlKCdndWxwLXJlcGxhY2UnKSxcbiAgdGFwICAgICAgICAgICAgID0gcmVxdWlyZSgnZ3VscC10YXAnKSxcbiAgdXRpbCAgICAgICAgICAgID0gcmVxdWlyZSgnZ3VscC11dGlsJyksXG5cbiAgLy8gY29uZmlnXG4gIGNvbmZpZyAgICAgICAgICA9IHJlcXVpcmUoJy4uLy4uL2NvbmZpZy91c2VyJyksXG4gIHJlbGVhc2UgICAgICAgICA9IHJlcXVpcmUoJy4uLy4uL2NvbmZpZy9hZG1pbi9yZWxlYXNlJyksXG4gIHByb2plY3QgICAgICAgICA9IHJlcXVpcmUoJy4uLy4uL2NvbmZpZy9wcm9qZWN0L3JlbGVhc2UnKSxcblxuICAvLyBzaG9ydGhhbmRcbiAgdmVyc2lvbiAgICAgICAgID0gcHJvamVjdC52ZXJzaW9uLFxuICBvdXRwdXQgICAgICAgICAgPSBjb25maWcucGF0aHMub3V0cHV0XG5cbjtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG4gIHZhclxuICAgIHN0cmVhbSxcbiAgICBpbmRleCxcbiAgICB0YXNrcyA9IFtdXG4gIDtcblxuICBmb3IoaW5kZXggaW4gcmVsZWFzZS5jb21wb25lbnRzKSB7XG5cbiAgICB2YXJcbiAgICAgIGNvbXBvbmVudCA9IHJlbGVhc2UuY29tcG9uZW50c1tpbmRleF1cbiAgICA7XG5cbiAgICAvLyBzdHJlYW1zLi4uIGRlc2lnbmVkIHRvIHNhdmUgdGltZSBhbmQgbWFrZSBjb2RpbmcgZnVuLi4uXG4gICAgKGZ1bmN0aW9uKGNvbXBvbmVudCkge1xuXG4gICAgICB2YXJcbiAgICAgICAgb3V0cHV0RGlyZWN0b3J5ICAgICAgPSBwYXRoLmpvaW4ocmVsZWFzZS5vdXRwdXRSb290LCBjb21wb25lbnQpLFxuICAgICAgICBpc0phdmFzY3JpcHQgICAgICAgICA9IGZzLmV4aXN0c1N5bmMob3V0cHV0LmNvbXByZXNzZWQgKyBjb21wb25lbnQgKyAnLmpzJyksXG4gICAgICAgIGlzQ1NTICAgICAgICAgICAgICAgID0gZnMuZXhpc3RzU3luYyhvdXRwdXQuY29tcHJlc3NlZCArIGNvbXBvbmVudCArICcuY3NzJyksXG4gICAgICAgIGNhcGl0YWxpemVkQ29tcG9uZW50ID0gY29tcG9uZW50LmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgY29tcG9uZW50LnNsaWNlKDEpLFxuICAgICAgICBwYWNrYWdlTmFtZSAgICAgICAgICA9IHJlbGVhc2UucGFja2FnZVJvb3QgKyBjb21wb25lbnQsXG4gICAgICAgIHJlcG9OYW1lICAgICAgICAgICAgID0gcmVsZWFzZS5jb21wb25lbnRSZXBvUm9vdCArIGNhcGl0YWxpemVkQ29tcG9uZW50LFxuICAgICAgICBnaXRVUkwgICAgICAgICAgICAgICA9ICdodHRwczovL2dpdGh1Yi5jb20vJyArIHJlbGVhc2Uub3JnICsgJy8nICsgcmVwb05hbWUgKyAnLmdpdCcsXG4gICAgICAgIHJlcG9VUkwgICAgICAgICAgICAgID0gJ2h0dHBzOi8vZ2l0aHViLmNvbS8nICsgcmVsZWFzZS5vcmcgKyAnLycgKyByZXBvTmFtZSArICcvJyxcbiAgICAgICAgY29uY2F0U2V0dGluZ3MgPSB7XG4gICAgICAgICAgbmV3bGluZSA6ICcnLFxuICAgICAgICAgIHJvb3QgICAgOiBvdXRwdXREaXJlY3RvcnksXG4gICAgICAgICAgcHJlcGVuZCA6IFwiICAgICdcIixcbiAgICAgICAgICBhcHBlbmQgIDogXCInLFwiXG4gICAgICAgIH0sXG4gICAgICAgIHJlZ0V4cCAgICAgICAgICAgICAgID0ge1xuICAgICAgICAgIG1hdGNoICAgICAgICAgICAgOiB7XG4gICAgICAgICAgICAvLyB0ZW1wbGF0ZWQgdmFsdWVzXG4gICAgICAgICAgICBuYW1lICAgICAgOiAne2NvbXBvbmVudH0nLFxuICAgICAgICAgICAgdGl0bGVOYW1lIDogJ3tDb21wb25lbnR9JyxcbiAgICAgICAgICAgIHZlcnNpb24gICA6ICd7dmVyc2lvbn0nLFxuICAgICAgICAgICAgZmlsZXMgICAgIDogJ3tmaWxlc30nLFxuICAgICAgICAgICAgLy8gcmVsZWFzZSBub3Rlc1xuICAgICAgICAgICAgc3BhY2VkVmVyc2lvbnMgICAgOiAvKCMjIy4qXFxuKVxcbisoPz0jIyMpL2dtLFxuICAgICAgICAgICAgc3BhY2VkTGlzdHMgICAgICAgOiAvKF4tIC4qXFxuKVxcbisoPz1eLSkvZ20sXG4gICAgICAgICAgICB0cmltICAgICAgICAgICAgICA6IC9eXFxzK3xcXHMrJC9nLFxuICAgICAgICAgICAgdW5yZWxhdGVkTm90ZXMgICAgOiBuZXcgUmVnRXhwKCdeKCg/ISheLiooJyArIGNvbXBvbmVudCArICcpLiokfCMjIy4qKSkuKSokJywgJ2dtaScpLFxuICAgICAgICAgICAgd2hpdGVzcGFjZSAgICAgICAgOiAvXFxuXFxzKlxcblxccypcXG4vZ20sXG4gICAgICAgICAgICAvLyBucG1cbiAgICAgICAgICAgIGNvbXBvbmVudEV4cG9ydCAgIDogLyguKilcXCRcXC5mblxcLlxcdytcXHMqPVxccypmdW5jdGlvblxcKChbXlxcKV0qKVxcKVxccyp7L2csXG4gICAgICAgICAgICBjb21wb25lbnRSZWZlcmVuY2U6ICckLmZuLicgKyBjb21wb25lbnQsXG4gICAgICAgICAgICBzZXR0aW5nc0V4cG9ydCAgICA6IC9cXCRcXC5mblxcLlxcdytcXC5zZXR0aW5nc1xccyo9L2csXG4gICAgICAgICAgICBzZXR0aW5nc1JlZmVyZW5jZSA6IC9cXCRcXC5mblxcLlxcdytcXC5zZXR0aW5ncy9nLFxuICAgICAgICAgICAgdHJhaWxpbmdDb21tYSAgICAgOiAvLCg/PVteLF0qJCkvLFxuICAgICAgICAgICAgalF1ZXJ5ICAgICAgICAgICAgOiAvalF1ZXJ5L2csXG4gICAgICAgICAgfSxcbiAgICAgICAgICByZXBsYWNlIDoge1xuICAgICAgICAgICAgLy8gcmVhZG1lXG4gICAgICAgICAgICBuYW1lICAgICAgICAgICAgICA6IGNvbXBvbmVudCxcbiAgICAgICAgICAgIHRpdGxlTmFtZSAgICAgICAgIDogY2FwaXRhbGl6ZWRDb21wb25lbnQsXG4gICAgICAgICAgICAvLyByZWxlYXNlIG5vdGVzXG4gICAgICAgICAgICBzcGFjZWRWZXJzaW9ucyAgICA6ICcnLFxuICAgICAgICAgICAgc3BhY2VkTGlzdHMgICAgICAgOiAnJDEnLFxuICAgICAgICAgICAgdHJpbSAgICAgICAgICAgICAgOiAnJyxcbiAgICAgICAgICAgIHVucmVsYXRlZE5vdGVzICAgIDogJycsXG4gICAgICAgICAgICB3aGl0ZXNwYWNlICAgICAgICA6ICdcXG5cXG4nLFxuICAgICAgICAgICAgLy8gbnBtXG4gICAgICAgICAgICBjb21wb25lbnRFeHBvcnQgICA6ICAndmFyIF9tb2R1bGUgPSBtb2R1bGU7XFxuJDFtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKCQyKSB7JyxcbiAgICAgICAgICAgIGNvbXBvbmVudFJlZmVyZW5jZTogICdfbW9kdWxlLmV4cG9ydHMnLFxuICAgICAgICAgICAgc2V0dGluZ3NFeHBvcnQgICAgOiAgJ21vZHVsZS5leHBvcnRzLnNldHRpbmdzID0nLFxuICAgICAgICAgICAgc2V0dGluZ3NSZWZlcmVuY2UgOiAgJ19tb2R1bGUuZXhwb3J0cy5zZXR0aW5ncycsXG4gICAgICAgICAgICBqUXVlcnkgICAgICAgICAgICA6ICAncmVxdWlyZShcImpxdWVyeVwiKSdcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHRhc2sgPSB7XG4gICAgICAgICAgYWxsICAgICAgOiBjb21wb25lbnQgKyAnIGNyZWF0aW5nJyxcbiAgICAgICAgICByZXBvICAgICA6IGNvbXBvbmVudCArICcgY3JlYXRlIHJlcG8nLFxuICAgICAgICAgIGJvd2VyICAgIDogY29tcG9uZW50ICsgJyBjcmVhdGUgYm93ZXIuanNvbicsXG4gICAgICAgICAgcmVhZG1lICAgOiBjb21wb25lbnQgKyAnIGNyZWF0ZSBSRUFETUUnLFxuICAgICAgICAgIG5wbSAgICAgIDogY29tcG9uZW50ICsgJyBjcmVhdGUgTlBNIE1vZHVsZScsXG4gICAgICAgICAgbm90ZXMgICAgOiBjb21wb25lbnQgKyAnIGNyZWF0ZSByZWxlYXNlIG5vdGVzJyxcbiAgICAgICAgICBjb21wb3NlciA6IGNvbXBvbmVudCArICcgY3JlYXRlIGNvbXBvc2VyLmpzb24nLFxuICAgICAgICAgIHBhY2thZ2UgIDogY29tcG9uZW50ICsgJyBjcmVhdGUgcGFja2FnZS5qc29uJyxcbiAgICAgICAgICBtZXRlb3IgICA6IGNvbXBvbmVudCArICcgY3JlYXRlIG1ldGVvciBwYWNrYWdlLmpzJyxcbiAgICAgICAgfSxcbiAgICAgICAgLy8gcGF0aHMgdG8gaW5jbHVkYWJsZSBhc3NldHNcbiAgICAgICAgbWFuaWZlc3QgPSB7XG4gICAgICAgICAgYXNzZXRzICAgIDogb3V0cHV0RGlyZWN0b3J5ICsgJy9hc3NldHMvKiovJyArIGNvbXBvbmVudCArICc/KHMpLionLFxuICAgICAgICAgIGNvbXBvbmVudCA6IG91dHB1dERpcmVjdG9yeSArICcvJyArIGNvbXBvbmVudCArICcrKC5qc3wuY3NzKSdcbiAgICAgICAgfVxuICAgICAgO1xuXG4gICAgICAvLyBjb3B5IGRpc3QgZmlsZXMgaW50byBvdXRwdXQgZm9sZGVyIGFkanVzdGluZyBhc3NldCBwYXRoc1xuICAgICAgZ3VscC50YXNrKHRhc2sucmVwbywgZmFsc2UsIGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gZ3VscC5zcmMocmVsZWFzZS5zb3VyY2UgKyBjb21wb25lbnQgKyAnLionKVxuICAgICAgICAgIC5waXBlKHBsdW1iZXIoKSlcbiAgICAgICAgICAucGlwZShmbGF0dGVuKCkpXG4gICAgICAgICAgLnBpcGUocmVwbGFjZShyZWxlYXNlLnBhdGhzLnNvdXJjZSwgcmVsZWFzZS5wYXRocy5vdXRwdXQpKVxuICAgICAgICAgIC5waXBlKGd1bHAuZGVzdChvdXRwdXREaXJlY3RvcnkpKVxuICAgICAgICA7XG4gICAgICB9KTtcblxuICAgICAgLy8gY3JlYXRlIG5wbSBtb2R1bGVcbiAgICAgIGd1bHAudGFzayh0YXNrLm5wbSwgZmFsc2UsIGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gZ3VscC5zcmMocmVsZWFzZS5zb3VyY2UgKyBjb21wb25lbnQgKyAnISgqLm1pbnwqLm1hcCkuanMnKVxuICAgICAgICAgIC5waXBlKHBsdW1iZXIoKSlcbiAgICAgICAgICAucGlwZShmbGF0dGVuKCkpXG4gICAgICAgICAgLnBpcGUocmVwbGFjZShyZWdFeHAubWF0Y2guY29tcG9uZW50RXhwb3J0LCByZWdFeHAucmVwbGFjZS5jb21wb25lbnRFeHBvcnQpKVxuICAgICAgICAgIC5waXBlKHJlcGxhY2UocmVnRXhwLm1hdGNoLmNvbXBvbmVudFJlZmVyZW5jZSwgcmVnRXhwLnJlcGxhY2UuY29tcG9uZW50UmVmZXJlbmNlKSlcbiAgICAgICAgICAucGlwZShyZXBsYWNlKHJlZ0V4cC5tYXRjaC5zZXR0aW5nc0V4cG9ydCwgcmVnRXhwLnJlcGxhY2Uuc2V0dGluZ3NFeHBvcnQpKVxuICAgICAgICAgIC5waXBlKHJlcGxhY2UocmVnRXhwLm1hdGNoLnNldHRpbmdzUmVmZXJlbmNlLCByZWdFeHAucmVwbGFjZS5zZXR0aW5nc1JlZmVyZW5jZSkpXG4gICAgICAgICAgLnBpcGUocmVwbGFjZShyZWdFeHAubWF0Y2gualF1ZXJ5LCByZWdFeHAucmVwbGFjZS5qUXVlcnkpKVxuICAgICAgICAgIC5waXBlKHJlbmFtZSgnaW5kZXguanMnKSlcbiAgICAgICAgICAucGlwZShndWxwLmRlc3Qob3V0cHV0RGlyZWN0b3J5KSlcbiAgICAgICAgO1xuICAgICAgfSk7XG5cbiAgICAgIC8vIGNyZWF0ZSByZWFkbWVcbiAgICAgIGd1bHAudGFzayh0YXNrLnJlYWRtZSwgZmFsc2UsIGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gZ3VscC5zcmMocmVsZWFzZS50ZW1wbGF0ZXMucmVhZG1lKVxuICAgICAgICAgIC5waXBlKHBsdW1iZXIoKSlcbiAgICAgICAgICAucGlwZShmbGF0dGVuKCkpXG4gICAgICAgICAgLnBpcGUocmVwbGFjZShyZWdFeHAubWF0Y2gubmFtZSwgcmVnRXhwLnJlcGxhY2UubmFtZSkpXG4gICAgICAgICAgLnBpcGUocmVwbGFjZShyZWdFeHAubWF0Y2gudGl0bGVOYW1lLCByZWdFeHAucmVwbGFjZS50aXRsZU5hbWUpKVxuICAgICAgICAgIC5waXBlKGd1bHAuZGVzdChvdXRwdXREaXJlY3RvcnkpKVxuICAgICAgICA7XG4gICAgICB9KTtcblxuICAgICAgLy8gZXh0ZW5kIGJvd2VyLmpzb25cbiAgICAgIGd1bHAudGFzayh0YXNrLmJvd2VyLCBmYWxzZSwgZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiBndWxwLnNyYyhyZWxlYXNlLnRlbXBsYXRlcy5ib3dlcilcbiAgICAgICAgICAucGlwZShwbHVtYmVyKCkpXG4gICAgICAgICAgLnBpcGUoZmxhdHRlbigpKVxuICAgICAgICAgIC5waXBlKGpzb25FZGl0b3IoZnVuY3Rpb24oYm93ZXIpIHtcbiAgICAgICAgICAgIGJvd2VyLm5hbWUgPSBwYWNrYWdlTmFtZTtcbiAgICAgICAgICAgIGJvd2VyLmRlc2NyaXB0aW9uID0gY2FwaXRhbGl6ZWRDb21wb25lbnQgKyAnIC0gU2VtYW50aWMgVUknO1xuICAgICAgICAgICAgaWYoaXNKYXZhc2NyaXB0KSB7XG4gICAgICAgICAgICAgIGlmKGlzQ1NTKSB7XG4gICAgICAgICAgICAgICAgYm93ZXIubWFpbiA9IFtcbiAgICAgICAgICAgICAgICAgIGNvbXBvbmVudCArICcuanMnLFxuICAgICAgICAgICAgICAgICAgY29tcG9uZW50ICsgJy5jc3MnXG4gICAgICAgICAgICAgICAgXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBib3dlci5tYWluID0gW1xuICAgICAgICAgICAgICAgICAgY29tcG9uZW50ICsgJy5qcydcbiAgICAgICAgICAgICAgICBdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGJvd2VyLmRlcGVuZGVuY2llcyA9IHtcbiAgICAgICAgICAgICAgICBqcXVlcnk6ICc+PTEuOCdcbiAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBib3dlci5tYWluID0gW1xuICAgICAgICAgICAgICAgIGNvbXBvbmVudCArICcuY3NzJ1xuICAgICAgICAgICAgICBdO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGJvd2VyO1xuICAgICAgICAgIH0pKVxuICAgICAgICAgIC5waXBlKGd1bHAuZGVzdChvdXRwdXREaXJlY3RvcnkpKVxuICAgICAgICA7XG4gICAgICB9KTtcblxuICAgICAgLy8gZXh0ZW5kIHBhY2thZ2UuanNvblxuICAgICAgZ3VscC50YXNrKHRhc2sucGFja2FnZSwgZmFsc2UsIGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gZ3VscC5zcmMocmVsZWFzZS50ZW1wbGF0ZXMucGFja2FnZSlcbiAgICAgICAgICAucGlwZShwbHVtYmVyKCkpXG4gICAgICAgICAgLnBpcGUoZmxhdHRlbigpKVxuICAgICAgICAgIC5waXBlKGpzb25FZGl0b3IoZnVuY3Rpb24obnBtKSB7XG4gICAgICAgICAgICBpZihpc0phdmFzY3JpcHQpIHtcbiAgICAgICAgICAgICAgbnBtLmRlcGVuZGVuY2llcyA9IHtcbiAgICAgICAgICAgICAgICBqcXVlcnk6ICd4LngueCdcbiAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgbnBtLm1haW4gPSAnaW5kZXguanMnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbnBtLm5hbWUgPSBwYWNrYWdlTmFtZTtcbiAgICAgICAgICAgIGlmKHZlcnNpb24pIHtcbiAgICAgICAgICAgICAgbnBtLnZlcnNpb24gPSB2ZXJzaW9uO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbnBtLnRpdGxlICAgICAgID0gJ1NlbWFudGljIFVJIC0gJyArIGNhcGl0YWxpemVkQ29tcG9uZW50O1xuICAgICAgICAgICAgbnBtLmRlc2NyaXB0aW9uID0gJ1NpbmdsZSBjb21wb25lbnQgcmVsZWFzZSBvZiAnICsgY29tcG9uZW50O1xuICAgICAgICAgICAgbnBtLnJlcG9zaXRvcnkgID0ge1xuICAgICAgICAgICAgICB0eXBlIDogJ2dpdCcsXG4gICAgICAgICAgICAgIHVybCAgOiBnaXRVUkxcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICByZXR1cm4gbnBtO1xuICAgICAgICAgIH0pKVxuICAgICAgICAgIC5waXBlKGd1bHAuZGVzdChvdXRwdXREaXJlY3RvcnkpKVxuICAgICAgICA7XG4gICAgICB9KTtcblxuICAgICAgLy8gZXh0ZW5kIGNvbXBvc2VyLmpzb25cbiAgICAgIGd1bHAudGFzayh0YXNrLmNvbXBvc2VyLCBmYWxzZSwgZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiBndWxwLnNyYyhyZWxlYXNlLnRlbXBsYXRlcy5jb21wb3NlcilcbiAgICAgICAgICAucGlwZShwbHVtYmVyKCkpXG4gICAgICAgICAgLnBpcGUoZmxhdHRlbigpKVxuICAgICAgICAgIC5waXBlKGpzb25FZGl0b3IoZnVuY3Rpb24oY29tcG9zZXIpIHtcbiAgICAgICAgICAgIGlmKGlzSmF2YXNjcmlwdCkge1xuICAgICAgICAgICAgICBjb21wb3Nlci5kZXBlbmRlbmNpZXMgPSB7XG4gICAgICAgICAgICAgICAganF1ZXJ5OiAneC54LngnXG4gICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgIGNvbXBvc2VyLm1haW4gPSBjb21wb25lbnQgKyAnLmpzJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbXBvc2VyLm5hbWUgPSAnc2VtYW50aWMvJyArIGNvbXBvbmVudDtcbiAgICAgICAgICAgIGlmKHZlcnNpb24pIHtcbiAgICAgICAgICAgICAgY29tcG9zZXIudmVyc2lvbiA9IHZlcnNpb247XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb21wb3Nlci5kZXNjcmlwdGlvbiA9ICdTaW5nbGUgY29tcG9uZW50IHJlbGVhc2Ugb2YgJyArIGNvbXBvbmVudDtcbiAgICAgICAgICAgIHJldHVybiBjb21wb3NlcjtcbiAgICAgICAgICB9KSlcbiAgICAgICAgICAucGlwZShndWxwLmRlc3Qob3V0cHV0RGlyZWN0b3J5KSlcbiAgICAgICAgO1xuICAgICAgfSk7XG5cbiAgICAgIC8vIGNyZWF0ZSByZWxlYXNlIG5vdGVzXG4gICAgICBndWxwLnRhc2sodGFzay5ub3RlcywgZmFsc2UsIGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gZ3VscC5zcmMocmVsZWFzZS50ZW1wbGF0ZXMubm90ZXMpXG4gICAgICAgICAgLnBpcGUocGx1bWJlcigpKVxuICAgICAgICAgIC5waXBlKGZsYXR0ZW4oKSlcbiAgICAgICAgICAvLyBSZW1vdmUgcmVsZWFzZSBub3RlcyBmb3IgbGluZXMgbm90IG1lbnRpb25pbmcgY29tcG9uZW50XG4gICAgICAgICAgLnBpcGUocmVwbGFjZShyZWdFeHAubWF0Y2gudW5yZWxhdGVkTm90ZXMsIHJlZ0V4cC5yZXBsYWNlLnVucmVsYXRlZE5vdGVzKSlcbiAgICAgICAgICAucGlwZShyZXBsYWNlKHJlZ0V4cC5tYXRjaC53aGl0ZXNwYWNlLCByZWdFeHAucmVwbGFjZS53aGl0ZXNwYWNlKSlcbiAgICAgICAgICAucGlwZShyZXBsYWNlKHJlZ0V4cC5tYXRjaC5zcGFjZWRWZXJzaW9ucywgcmVnRXhwLnJlcGxhY2Uuc3BhY2VkVmVyc2lvbnMpKVxuICAgICAgICAgIC5waXBlKHJlcGxhY2UocmVnRXhwLm1hdGNoLnNwYWNlZExpc3RzLCByZWdFeHAucmVwbGFjZS5zcGFjZWRMaXN0cykpXG4gICAgICAgICAgLnBpcGUocmVwbGFjZShyZWdFeHAubWF0Y2gudHJpbSwgcmVnRXhwLnJlcGxhY2UudHJpbSkpXG4gICAgICAgICAgLnBpcGUoZ3VscC5kZXN0KG91dHB1dERpcmVjdG9yeSkpXG4gICAgICAgIDtcbiAgICAgIH0pO1xuXG4gICAgICAvLyBDcmVhdGVzIG1ldGVvciBwYWNrYWdlLmpzXG4gICAgICBndWxwLnRhc2sodGFzay5tZXRlb3IsIGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXJcbiAgICAgICAgICBmaWxlbmFtZXMgPSAnJ1xuICAgICAgICA7XG4gICAgICAgIHJldHVybiBndWxwLnNyYyhtYW5pZmVzdC5jb21wb25lbnQpXG4gICAgICAgICAgLnBpcGUoY29uY2F0RmlsZU5hbWVzKCdlbXB0eS50eHQnLCBjb25jYXRTZXR0aW5ncykpXG4gICAgICAgICAgLnBpcGUodGFwKGZ1bmN0aW9uKGZpbGUpIHtcbiAgICAgICAgICAgIGZpbGVuYW1lcyArPSBmaWxlLmNvbnRlbnRzO1xuICAgICAgICAgIH0pKVxuICAgICAgICAgIC5vbignZW5kJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBndWxwLnNyYyhtYW5pZmVzdC5hc3NldHMpXG4gICAgICAgICAgICAgIC5waXBlKGNvbmNhdEZpbGVOYW1lcygnZW1wdHkudHh0JywgY29uY2F0U2V0dGluZ3MpKVxuICAgICAgICAgICAgICAucGlwZSh0YXAoZnVuY3Rpb24oZmlsZSkge1xuICAgICAgICAgICAgICAgIGZpbGVuYW1lcyArPSBmaWxlLmNvbnRlbnRzO1xuICAgICAgICAgICAgICB9KSlcbiAgICAgICAgICAgICAgLm9uKCdlbmQnLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAvLyByZW1vdmUgdHJhaWxpbmcgc2xhc2hcbiAgICAgICAgICAgICAgICBmaWxlbmFtZXMgPSBmaWxlbmFtZXMucmVwbGFjZShyZWdFeHAubWF0Y2gudHJhaWxpbmdDb21tYSwgJycpLnRyaW0oKTtcbiAgICAgICAgICAgICAgICBndWxwLnNyYyhyZWxlYXNlLnRlbXBsYXRlcy5tZXRlb3IuY29tcG9uZW50KVxuICAgICAgICAgICAgICAgICAgLnBpcGUocGx1bWJlcigpKVxuICAgICAgICAgICAgICAgICAgLnBpcGUoZmxhdHRlbigpKVxuICAgICAgICAgICAgICAgICAgLnBpcGUocmVwbGFjZShyZWdFeHAubWF0Y2gubmFtZSwgcmVnRXhwLnJlcGxhY2UubmFtZSkpXG4gICAgICAgICAgICAgICAgICAucGlwZShyZXBsYWNlKHJlZ0V4cC5tYXRjaC50aXRsZU5hbWUsIHJlZ0V4cC5yZXBsYWNlLnRpdGxlTmFtZSkpXG4gICAgICAgICAgICAgICAgICAucGlwZShyZXBsYWNlKHJlZ0V4cC5tYXRjaC52ZXJzaW9uLCB2ZXJzaW9uKSlcbiAgICAgICAgICAgICAgICAgIC5waXBlKHJlcGxhY2UocmVnRXhwLm1hdGNoLmZpbGVzLCBmaWxlbmFtZXMpKVxuICAgICAgICAgICAgICAgICAgLnBpcGUocmVuYW1lKHJlbGVhc2UuZmlsZXMubWV0ZW9yKSlcbiAgICAgICAgICAgICAgICAgIC5waXBlKGd1bHAuZGVzdChvdXRwdXREaXJlY3RvcnkpKVxuICAgICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9KVxuICAgICAgICA7XG4gICAgICB9KTtcblxuXG4gICAgICAvLyBzeW5jaHJvbm91cyB0YXNrcyBpbiBvcmNoZXN0cmF0b3I/IEkgdGhpbmsgbm90XG4gICAgICBndWxwLnRhc2sodGFzay5hbGwsIGZhbHNlLCBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICBydW5TZXF1ZW5jZShbXG4gICAgICAgICAgdGFzay5yZXBvLFxuICAgICAgICAgIHRhc2subnBtLFxuICAgICAgICAgIHRhc2suYm93ZXIsXG4gICAgICAgICAgdGFzay5yZWFkbWUsXG4gICAgICAgICAgdGFzay5wYWNrYWdlLFxuICAgICAgICAgIHRhc2suY29tcG9zZXIsXG4gICAgICAgICAgdGFzay5ub3RlcyxcbiAgICAgICAgICB0YXNrLm1ldGVvclxuICAgICAgICBdLCBjYWxsYmFjayk7XG4gICAgICB9KTtcblxuICAgICAgdGFza3MucHVzaCh0YXNrLmFsbCk7XG5cbiAgICB9KShjb21wb25lbnQpO1xuICB9XG5cbiAgcnVuU2VxdWVuY2UodGFza3MsIGNhbGxiYWNrKTtcbn07XG4iXX0=