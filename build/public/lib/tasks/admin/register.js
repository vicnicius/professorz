/*******************************
          Register PM
*******************************/

/*
  Task to register component repos with Package Managers
  * Registers component with bower
  * Registers component with NPM
*/

var
// node dependencies
process = require('child_process'),


// config
release = require('../config/admin/release'),


// register components and distributions
repos = release.distributions.concat(release.components),
    total = repos.length,
    index = -1,
    stream,
    stepRepo;

module.exports = function (callback) {

  console.log('Registering repos with package managers');

  // Do Git commands synchronously per component, to avoid issues
  stepRepo = function () {
    index = index + 1;
    if (index >= total) {
      callback();
      return;
    }
    var repo = repos[index].toLowerCase(),
        outputDirectory = release.outputRoot + repo + '/',
        exec = process.exec,
        execSettings = { cwd: outputDirectory },
        updateNPM = 'npm publish';

    /* Register with NPM */
    exec(updateNPM, execSettings, function (err, stdout, stderr) {
      console.log(err, stdout, stderr);
      stepRepo();
    });
  };
  stepRepo();
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2FkbWluL3JlZ2lzdGVyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFVQTs7QUFFRSxVQUFVLFFBQVEsZUFBUixDQUZaO0FBQUE7OztBQUtFLFVBQVUsUUFBUSx5QkFBUixDQUxaO0FBQUE7OztBQVFFLFFBQVUsUUFBUSxhQUFSLENBQXNCLE1BQXRCLENBQTZCLFFBQVEsVUFBckMsQ0FSWjtBQUFBLElBU0UsUUFBVSxNQUFNLE1BVGxCO0FBQUEsSUFVRSxRQUFVLENBQUMsQ0FWYjtBQUFBLElBWUUsTUFaRjtBQUFBLElBYUUsUUFiRjs7QUFnQkEsT0FBTyxPQUFQLEdBQWlCLFVBQVMsUUFBVCxFQUFtQjs7QUFFbEMsVUFBUSxHQUFSLENBQVkseUNBQVo7OztBQUdBLGFBQVcsWUFBVztBQUNwQixZQUFRLFFBQVEsQ0FBaEI7QUFDQSxRQUFHLFNBQVMsS0FBWixFQUFtQjtBQUNqQjtBQUNBO0FBQ0Q7QUFDRCxRQUNFLE9BQWtCLE1BQU0sS0FBTixFQUFhLFdBQWIsRUFEcEI7QUFBQSxRQUVFLGtCQUFrQixRQUFRLFVBQVIsR0FBcUIsSUFBckIsR0FBNEIsR0FGaEQ7QUFBQSxRQUdFLE9BQWtCLFFBQVEsSUFINUI7QUFBQSxRQUlFLGVBQWtCLEVBQUMsS0FBSyxlQUFOLEVBSnBCO0FBQUEsUUFLRSxZQUFrQixhQUxwQjs7O0FBU0EsU0FBSyxTQUFMLEVBQWdCLFlBQWhCLEVBQThCLFVBQVMsR0FBVCxFQUFjLE1BQWQsRUFBc0IsTUFBdEIsRUFBOEI7QUFDMUQsY0FBUSxHQUFSLENBQVksR0FBWixFQUFpQixNQUFqQixFQUF5QixNQUF6QjtBQUNBO0FBQ0QsS0FIRDtBQUtELEdBcEJEO0FBcUJBO0FBQ0QsQ0EzQkQiLCJmaWxlIjoicmVnaXN0ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgIFJlZ2lzdGVyIFBNXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4vKlxuICBUYXNrIHRvIHJlZ2lzdGVyIGNvbXBvbmVudCByZXBvcyB3aXRoIFBhY2thZ2UgTWFuYWdlcnNcbiAgKiBSZWdpc3RlcnMgY29tcG9uZW50IHdpdGggYm93ZXJcbiAgKiBSZWdpc3RlcnMgY29tcG9uZW50IHdpdGggTlBNXG4qL1xuXG52YXJcbiAgLy8gbm9kZSBkZXBlbmRlbmNpZXNcbiAgcHJvY2VzcyA9IHJlcXVpcmUoJ2NoaWxkX3Byb2Nlc3MnKSxcblxuICAvLyBjb25maWdcbiAgcmVsZWFzZSA9IHJlcXVpcmUoJy4uL2NvbmZpZy9hZG1pbi9yZWxlYXNlJyksXG5cbiAgLy8gcmVnaXN0ZXIgY29tcG9uZW50cyBhbmQgZGlzdHJpYnV0aW9uc1xuICByZXBvcyAgID0gcmVsZWFzZS5kaXN0cmlidXRpb25zLmNvbmNhdChyZWxlYXNlLmNvbXBvbmVudHMpLFxuICB0b3RhbCAgID0gcmVwb3MubGVuZ3RoLFxuICBpbmRleCAgID0gLTEsXG5cbiAgc3RyZWFtLFxuICBzdGVwUmVwb1xuO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG5cbiAgY29uc29sZS5sb2coJ1JlZ2lzdGVyaW5nIHJlcG9zIHdpdGggcGFja2FnZSBtYW5hZ2VycycpO1xuXG4gIC8vIERvIEdpdCBjb21tYW5kcyBzeW5jaHJvbm91c2x5IHBlciBjb21wb25lbnQsIHRvIGF2b2lkIGlzc3Vlc1xuICBzdGVwUmVwbyA9IGZ1bmN0aW9uKCkge1xuICAgIGluZGV4ID0gaW5kZXggKyAxO1xuICAgIGlmKGluZGV4ID49IHRvdGFsKSB7XG4gICAgICBjYWxsYmFjaygpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB2YXJcbiAgICAgIHJlcG8gICAgICAgICAgICA9IHJlcG9zW2luZGV4XS50b0xvd2VyQ2FzZSgpLFxuICAgICAgb3V0cHV0RGlyZWN0b3J5ID0gcmVsZWFzZS5vdXRwdXRSb290ICsgcmVwbyArICcvJyxcbiAgICAgIGV4ZWMgICAgICAgICAgICA9IHByb2Nlc3MuZXhlYyxcbiAgICAgIGV4ZWNTZXR0aW5ncyAgICA9IHtjd2Q6IG91dHB1dERpcmVjdG9yeX0sXG4gICAgICB1cGRhdGVOUE0gICAgICAgPSAnbnBtIHB1Ymxpc2gnXG4gICAgO1xuXG4gICAgLyogUmVnaXN0ZXIgd2l0aCBOUE0gKi9cbiAgICBleGVjKHVwZGF0ZU5QTSwgZXhlY1NldHRpbmdzLCBmdW5jdGlvbihlcnIsIHN0ZG91dCwgc3RkZXJyKSB7XG4gICAgICBjb25zb2xlLmxvZyhlcnIsIHN0ZG91dCwgc3RkZXJyKTtcbiAgICAgIHN0ZXBSZXBvKCk7XG4gICAgfSk7XG5cbiAgfTtcbiAgc3RlcFJlcG8oKTtcbn07XG5cbiJdfQ==