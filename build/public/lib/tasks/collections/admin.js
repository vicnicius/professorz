/*******************************
     Admin Task Collection
*******************************/

/*
  This are tasks to be run by project maintainers
  - Creating Component Repos
  - Syncing with GitHub via APIs
  - Modifying package files
*/

/*******************************
             Tasks
*******************************/

module.exports = function (gulp) {
  var
  // less/css distributions
  initComponents = require('../admin/components/init'),
      createComponents = require('../admin/components/create'),
      updateComponents = require('../admin/components/update'),


  // single component releases
  initDistributions = require('../admin/distributions/init'),
      createDistributions = require('../admin/distributions/create'),
      updateDistributions = require('../admin/distributions/update'),
      release = require('../admin/release'),
      publish = require('../admin/publish'),
      register = require('../admin/register');

  /* Release */
  gulp.task('init distributions', 'Grabs each component from GitHub', initDistributions);
  gulp.task('create distributions', 'Updates files in each repo', createDistributions);
  gulp.task('init components', 'Grabs each component from GitHub', initComponents);
  gulp.task('create components', 'Updates files in each repo', createComponents);

  /* Publish */
  gulp.task('update distributions', 'Commits component updates from create to GitHub', updateDistributions);
  gulp.task('update components', 'Commits component updates from create to GitHub', updateComponents);

  /* Tasks */
  gulp.task('release', 'Stages changes in GitHub repos for all distributions', release);
  gulp.task('publish', 'Publishes all releases (components, package)', publish);
  gulp.task('register', 'Registers all packages with NPM', register);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NvbGxlY3Rpb25zL2FkbWluLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7OztBQWdCQSxPQUFPLE9BQVAsR0FBaUIsVUFBUyxJQUFULEVBQWU7QUFDOUI7O0FBRUUsbUJBQXNCLFFBQVEsMEJBQVIsQ0FGeEI7QUFBQSxNQUdFLG1CQUFzQixRQUFRLDRCQUFSLENBSHhCO0FBQUEsTUFJRSxtQkFBc0IsUUFBUSw0QkFBUixDQUp4QjtBQUFBOzs7QUFPRSxzQkFBc0IsUUFBUSw2QkFBUixDQVB4QjtBQUFBLE1BUUUsc0JBQXNCLFFBQVEsK0JBQVIsQ0FSeEI7QUFBQSxNQVNFLHNCQUFzQixRQUFRLCtCQUFSLENBVHhCO0FBQUEsTUFXRSxVQUFzQixRQUFRLGtCQUFSLENBWHhCO0FBQUEsTUFZRSxVQUFzQixRQUFRLGtCQUFSLENBWnhCO0FBQUEsTUFhRSxXQUFzQixRQUFRLG1CQUFSLENBYnhCOzs7QUFpQkEsT0FBSyxJQUFMLENBQVUsb0JBQVYsRUFBZ0Msa0NBQWhDLEVBQW9FLGlCQUFwRTtBQUNBLE9BQUssSUFBTCxDQUFVLHNCQUFWLEVBQWtDLDRCQUFsQyxFQUFnRSxtQkFBaEU7QUFDQSxPQUFLLElBQUwsQ0FBVSxpQkFBVixFQUE2QixrQ0FBN0IsRUFBaUUsY0FBakU7QUFDQSxPQUFLLElBQUwsQ0FBVSxtQkFBVixFQUErQiw0QkFBL0IsRUFBNkQsZ0JBQTdEOzs7QUFHQSxPQUFLLElBQUwsQ0FBVSxzQkFBVixFQUFrQyxpREFBbEMsRUFBcUYsbUJBQXJGO0FBQ0EsT0FBSyxJQUFMLENBQVUsbUJBQVYsRUFBK0IsaURBQS9CLEVBQWtGLGdCQUFsRjs7O0FBR0EsT0FBSyxJQUFMLENBQVUsU0FBVixFQUFxQixzREFBckIsRUFBNkUsT0FBN0U7QUFDQSxPQUFLLElBQUwsQ0FBVSxTQUFWLEVBQXFCLDhDQUFyQixFQUFxRSxPQUFyRTtBQUNBLE9BQUssSUFBTCxDQUFVLFVBQVYsRUFBc0IsaUNBQXRCLEVBQXlELFFBQXpEO0FBRUQsQ0FoQ0QiLCJmaWxlIjoiYWRtaW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICBBZG1pbiBUYXNrIENvbGxlY3Rpb25cbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi8qXG4gIFRoaXMgYXJlIHRhc2tzIHRvIGJlIHJ1biBieSBwcm9qZWN0IG1haW50YWluZXJzXG4gIC0gQ3JlYXRpbmcgQ29tcG9uZW50IFJlcG9zXG4gIC0gU3luY2luZyB3aXRoIEdpdEh1YiB2aWEgQVBJc1xuICAtIE1vZGlmeWluZyBwYWNrYWdlIGZpbGVzXG4qL1xuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgICAgIFRhc2tzXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oZ3VscCkge1xuICB2YXJcbiAgICAvLyBsZXNzL2NzcyBkaXN0cmlidXRpb25zXG4gICAgaW5pdENvbXBvbmVudHMgICAgICA9IHJlcXVpcmUoJy4uL2FkbWluL2NvbXBvbmVudHMvaW5pdCcpLFxuICAgIGNyZWF0ZUNvbXBvbmVudHMgICAgPSByZXF1aXJlKCcuLi9hZG1pbi9jb21wb25lbnRzL2NyZWF0ZScpLFxuICAgIHVwZGF0ZUNvbXBvbmVudHMgICAgPSByZXF1aXJlKCcuLi9hZG1pbi9jb21wb25lbnRzL3VwZGF0ZScpLFxuXG4gICAgLy8gc2luZ2xlIGNvbXBvbmVudCByZWxlYXNlc1xuICAgIGluaXREaXN0cmlidXRpb25zICAgPSByZXF1aXJlKCcuLi9hZG1pbi9kaXN0cmlidXRpb25zL2luaXQnKSxcbiAgICBjcmVhdGVEaXN0cmlidXRpb25zID0gcmVxdWlyZSgnLi4vYWRtaW4vZGlzdHJpYnV0aW9ucy9jcmVhdGUnKSxcbiAgICB1cGRhdGVEaXN0cmlidXRpb25zID0gcmVxdWlyZSgnLi4vYWRtaW4vZGlzdHJpYnV0aW9ucy91cGRhdGUnKSxcblxuICAgIHJlbGVhc2UgICAgICAgICAgICAgPSByZXF1aXJlKCcuLi9hZG1pbi9yZWxlYXNlJyksXG4gICAgcHVibGlzaCAgICAgICAgICAgICA9IHJlcXVpcmUoJy4uL2FkbWluL3B1Ymxpc2gnKSxcbiAgICByZWdpc3RlciAgICAgICAgICAgID0gcmVxdWlyZSgnLi4vYWRtaW4vcmVnaXN0ZXInKVxuICA7XG5cbiAgLyogUmVsZWFzZSAqL1xuICBndWxwLnRhc2soJ2luaXQgZGlzdHJpYnV0aW9ucycsICdHcmFicyBlYWNoIGNvbXBvbmVudCBmcm9tIEdpdEh1YicsIGluaXREaXN0cmlidXRpb25zKTtcbiAgZ3VscC50YXNrKCdjcmVhdGUgZGlzdHJpYnV0aW9ucycsICdVcGRhdGVzIGZpbGVzIGluIGVhY2ggcmVwbycsIGNyZWF0ZURpc3RyaWJ1dGlvbnMpO1xuICBndWxwLnRhc2soJ2luaXQgY29tcG9uZW50cycsICdHcmFicyBlYWNoIGNvbXBvbmVudCBmcm9tIEdpdEh1YicsIGluaXRDb21wb25lbnRzKTtcbiAgZ3VscC50YXNrKCdjcmVhdGUgY29tcG9uZW50cycsICdVcGRhdGVzIGZpbGVzIGluIGVhY2ggcmVwbycsIGNyZWF0ZUNvbXBvbmVudHMpO1xuXG4gIC8qIFB1Ymxpc2ggKi9cbiAgZ3VscC50YXNrKCd1cGRhdGUgZGlzdHJpYnV0aW9ucycsICdDb21taXRzIGNvbXBvbmVudCB1cGRhdGVzIGZyb20gY3JlYXRlIHRvIEdpdEh1YicsIHVwZGF0ZURpc3RyaWJ1dGlvbnMpO1xuICBndWxwLnRhc2soJ3VwZGF0ZSBjb21wb25lbnRzJywgJ0NvbW1pdHMgY29tcG9uZW50IHVwZGF0ZXMgZnJvbSBjcmVhdGUgdG8gR2l0SHViJywgdXBkYXRlQ29tcG9uZW50cyk7XG5cbiAgLyogVGFza3MgKi9cbiAgZ3VscC50YXNrKCdyZWxlYXNlJywgJ1N0YWdlcyBjaGFuZ2VzIGluIEdpdEh1YiByZXBvcyBmb3IgYWxsIGRpc3RyaWJ1dGlvbnMnLCByZWxlYXNlKTtcbiAgZ3VscC50YXNrKCdwdWJsaXNoJywgJ1B1Ymxpc2hlcyBhbGwgcmVsZWFzZXMgKGNvbXBvbmVudHMsIHBhY2thZ2UpJywgcHVibGlzaCk7XG4gIGd1bHAudGFzaygncmVnaXN0ZXInLCAnUmVnaXN0ZXJzIGFsbCBwYWNrYWdlcyB3aXRoIE5QTScsIHJlZ2lzdGVyKTtcblxufTsiXX0=