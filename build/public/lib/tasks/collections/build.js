/*******************************
        Define Sub-Tasks
*******************************/

module.exports = function (gulp) {

  var
  // build sub-tasks
  buildJS = require('./../build/javascript'),
      buildCSS = require('./../build/css'),
      buildAssets = require('./../build/assets');

  // in case these tasks are undefined during import, less make sure these are available in scope
  gulp.task('build-javascript', 'Builds all javascript from source', buildJS);
  gulp.task('build-css', 'Builds all css from source', buildCSS);
  gulp.task('build-assets', 'Copies all assets from source', buildAssets);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NvbGxlY3Rpb25zL2J1aWxkLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFJQSxPQUFPLE9BQVAsR0FBaUIsVUFBUyxJQUFULEVBQWU7O0FBRTlCOztBQUVFLFlBQWUsUUFBUSx1QkFBUixDQUZqQjtBQUFBLE1BR0UsV0FBZSxRQUFRLGdCQUFSLENBSGpCO0FBQUEsTUFJRSxjQUFlLFFBQVEsbUJBQVIsQ0FKakI7OztBQVFBLE9BQUssSUFBTCxDQUFVLGtCQUFWLEVBQThCLG1DQUE5QixFQUFtRSxPQUFuRTtBQUNBLE9BQUssSUFBTCxDQUFVLFdBQVYsRUFBdUIsNEJBQXZCLEVBQXFELFFBQXJEO0FBQ0EsT0FBSyxJQUFMLENBQVUsY0FBVixFQUEwQiwrQkFBMUIsRUFBMkQsV0FBM0Q7QUFFRCxDQWREIiwiZmlsZSI6ImJ1aWxkLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAgICAgRGVmaW5lIFN1Yi1UYXNrc1xuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihndWxwKSB7XG5cbiAgdmFyXG4gICAgLy8gYnVpbGQgc3ViLXRhc2tzXG4gICAgYnVpbGRKUyAgICAgID0gcmVxdWlyZSgnLi8uLi9idWlsZC9qYXZhc2NyaXB0JyksXG4gICAgYnVpbGRDU1MgICAgID0gcmVxdWlyZSgnLi8uLi9idWlsZC9jc3MnKSxcbiAgICBidWlsZEFzc2V0cyAgPSByZXF1aXJlKCcuLy4uL2J1aWxkL2Fzc2V0cycpXG4gIDtcblxuICAvLyBpbiBjYXNlIHRoZXNlIHRhc2tzIGFyZSB1bmRlZmluZWQgZHVyaW5nIGltcG9ydCwgbGVzcyBtYWtlIHN1cmUgdGhlc2UgYXJlIGF2YWlsYWJsZSBpbiBzY29wZVxuICBndWxwLnRhc2soJ2J1aWxkLWphdmFzY3JpcHQnLCAnQnVpbGRzIGFsbCBqYXZhc2NyaXB0IGZyb20gc291cmNlJywgYnVpbGRKUyk7XG4gIGd1bHAudGFzaygnYnVpbGQtY3NzJywgJ0J1aWxkcyBhbGwgY3NzIGZyb20gc291cmNlJywgYnVpbGRDU1MpO1xuICBndWxwLnRhc2soJ2J1aWxkLWFzc2V0cycsICdDb3BpZXMgYWxsIGFzc2V0cyBmcm9tIHNvdXJjZScsIGJ1aWxkQXNzZXRzKTtcblxufTtcbiJdfQ==