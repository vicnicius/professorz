/*******************************
        Define Sub-Tasks
*******************************/

module.exports = function (gulp) {

  var
  // rtl
  buildRTL = require('./../rtl/build'),
      watchRTL = require('./../rtl/watch');

  gulp.task('watch-rtl', 'Build all files as RTL', watchRTL);
  gulp.task('build-rtl', 'Watch files as RTL ', buildRTL);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NvbGxlY3Rpb25zL3J0bC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBSUEsT0FBTyxPQUFQLEdBQWlCLFVBQVMsSUFBVCxFQUFlOztBQUU5Qjs7QUFFRSxhQUFlLFFBQVEsZ0JBQVIsQ0FGakI7QUFBQSxNQUdFLFdBQWUsUUFBUSxnQkFBUixDQUhqQjs7QUFNQSxPQUFLLElBQUwsQ0FBVSxXQUFWLEVBQXVCLHdCQUF2QixFQUFpRCxRQUFqRDtBQUNBLE9BQUssSUFBTCxDQUFVLFdBQVYsRUFBdUIscUJBQXZCLEVBQThDLFFBQTlDO0FBRUQsQ0FYRCIsImZpbGUiOiJydGwuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICBEZWZpbmUgU3ViLVRhc2tzXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGd1bHApIHtcblxuICB2YXJcbiAgICAvLyBydGxcbiAgICBidWlsZFJUTCAgICAgPSByZXF1aXJlKCcuLy4uL3J0bC9idWlsZCcpLFxuICAgIHdhdGNoUlRMICAgICA9IHJlcXVpcmUoJy4vLi4vcnRsL3dhdGNoJylcbiAgO1xuXG4gIGd1bHAudGFzaygnd2F0Y2gtcnRsJywgJ0J1aWxkIGFsbCBmaWxlcyBhcyBSVEwnLCB3YXRjaFJUTCk7XG4gIGd1bHAudGFzaygnYnVpbGQtcnRsJywgJ1dhdGNoIGZpbGVzIGFzIFJUTCAnLCBidWlsZFJUTCk7XG5cbn07XG4iXX0=