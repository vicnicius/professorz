/*******************************
          Build Task
*******************************/

var gulp = require('gulp'),


// gulp dependencies
chmod = require('gulp-chmod'),
    gulpif = require('gulp-if'),


// config
config = require('../config/user'),
    tasks = require('../config/tasks'),


// shorthand
globs = config.globs,
    assets = config.paths.assets,
    output = config.paths.output,
    source = config.paths.source,
    log = tasks.log;

module.exports = function (callback) {

  console.info('Building assets');

  // copy assets
  return gulp.src(source.themes + '/**/assets/**/*.*').pipe(gulpif(config.hasPermission, chmod(config.permission))).pipe(gulp.dest(output.themes));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2J1aWxkL2Fzc2V0cy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBSUEsSUFDRSxPQUFlLFFBQVEsTUFBUixDQURqQjtBQUFBOzs7QUFJRSxRQUFlLFFBQVEsWUFBUixDQUpqQjtBQUFBLElBS0UsU0FBZSxRQUFRLFNBQVIsQ0FMakI7QUFBQTs7O0FBUUUsU0FBZSxRQUFRLGdCQUFSLENBUmpCO0FBQUEsSUFTRSxRQUFlLFFBQVEsaUJBQVIsQ0FUakI7QUFBQTs7O0FBWUUsUUFBZSxPQUFPLEtBWnhCO0FBQUEsSUFhRSxTQUFlLE9BQU8sS0FBUCxDQUFhLE1BYjlCO0FBQUEsSUFjRSxTQUFlLE9BQU8sS0FBUCxDQUFhLE1BZDlCO0FBQUEsSUFlRSxTQUFlLE9BQU8sS0FBUCxDQUFhLE1BZjlCO0FBQUEsSUFpQkUsTUFBZSxNQUFNLEdBakJ2Qjs7QUFvQkEsT0FBTyxPQUFQLEdBQWlCLFVBQVMsUUFBVCxFQUFtQjs7QUFFbEMsVUFBUSxJQUFSLENBQWEsaUJBQWI7OztBQUdBLFNBQU8sS0FBSyxHQUFMLENBQVMsT0FBTyxNQUFQLEdBQWdCLG1CQUF6QixFQUNKLElBREksQ0FDQyxPQUFPLE9BQU8sYUFBZCxFQUE2QixNQUFNLE9BQU8sVUFBYixDQUE3QixDQURELEVBRUosSUFGSSxDQUVDLEtBQUssSUFBTCxDQUFVLE9BQU8sTUFBakIsQ0FGRCxDQUFQO0FBS0QsQ0FWRCIsImZpbGUiOiJhc3NldHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgIEJ1aWxkIFRhc2tcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbnZhclxuICBndWxwICAgICAgICAgPSByZXF1aXJlKCdndWxwJyksXG5cbiAgLy8gZ3VscCBkZXBlbmRlbmNpZXNcbiAgY2htb2QgICAgICAgID0gcmVxdWlyZSgnZ3VscC1jaG1vZCcpLFxuICBndWxwaWYgICAgICAgPSByZXF1aXJlKCdndWxwLWlmJyksXG5cbiAgLy8gY29uZmlnXG4gIGNvbmZpZyAgICAgICA9IHJlcXVpcmUoJy4uL2NvbmZpZy91c2VyJyksXG4gIHRhc2tzICAgICAgICA9IHJlcXVpcmUoJy4uL2NvbmZpZy90YXNrcycpLFxuXG4gIC8vIHNob3J0aGFuZFxuICBnbG9icyAgICAgICAgPSBjb25maWcuZ2xvYnMsXG4gIGFzc2V0cyAgICAgICA9IGNvbmZpZy5wYXRocy5hc3NldHMsXG4gIG91dHB1dCAgICAgICA9IGNvbmZpZy5wYXRocy5vdXRwdXQsXG4gIHNvdXJjZSAgICAgICA9IGNvbmZpZy5wYXRocy5zb3VyY2UsXG5cbiAgbG9nICAgICAgICAgID0gdGFza3MubG9nXG47XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oY2FsbGJhY2spIHtcblxuICBjb25zb2xlLmluZm8oJ0J1aWxkaW5nIGFzc2V0cycpO1xuXG4gIC8vIGNvcHkgYXNzZXRzXG4gIHJldHVybiBndWxwLnNyYyhzb3VyY2UudGhlbWVzICsgJy8qKi9hc3NldHMvKiovKi4qJylcbiAgICAucGlwZShndWxwaWYoY29uZmlnLmhhc1Blcm1pc3Npb24sIGNobW9kKGNvbmZpZy5wZXJtaXNzaW9uKSkpXG4gICAgLnBpcGUoZ3VscC5kZXN0KG91dHB1dC50aGVtZXMpKVxuICA7XG5cbn07Il19