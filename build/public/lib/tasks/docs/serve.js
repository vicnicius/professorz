/*******************************
           Serve Docs
*******************************/
var gulp = require('gulp'),


// node dependencies
console = require('better-console'),
    fs = require('fs'),


// gulp dependencies
autoprefixer = require('gulp-autoprefixer'),
    chmod = require('gulp-chmod'),
    clone = require('gulp-clone'),
    gulpif = require('gulp-if'),
    header = require('gulp-header'),
    less = require('gulp-less'),
    minifyCSS = require('gulp-minify-css'),
    plumber = require('gulp-plumber'),
    print = require('gulp-print'),
    rename = require('gulp-rename'),
    replace = require('gulp-replace'),
    uglify = require('gulp-uglify'),
    util = require('gulp-util'),
    watch = require('gulp-watch'),


// user config
config = require('../config/docs'),


// task config
tasks = require('../config/tasks'),
    configSetup = require('../config/project/config'),
    install = require('../config/project/install'),


// shorthand
banner = tasks.banner,
    comments = tasks.regExp.comments,
    log = tasks.log,
    settings = tasks.settings,
    globs,
    assets,
    output,
    source;

require('../collections/internal')(gulp);

module.exports = function () {

  // use a different config
  config = configSetup.addDerivedValues(config);

  // shorthand
  globs = config.globs;
  assets = config.paths.assets;
  output = config.paths.output;
  source = config.paths.source;

  /*--------------
     Copy Source
  ---------------*/

  gulp.watch(['src/**/*.*'], function (file) {
    console.clear();
    return gulp.src(file.path, {
      base: 'src/'
    }).pipe(gulp.dest(output.less)).pipe(print(log.created));
  });

  /*--------------
    Copy Examples
  ---------------*/

  gulp.watch(['examples/**/*.*'], function (file) {
    console.clear();
    return gulp.src(file.path, {
      base: 'examples/'
    }).pipe(gulp.dest(output.examples)).pipe(print(log.created));
  });

  /*--------------
      Watch CSS
  ---------------*/

  gulp.watch([source.config, source.definitions + '/**/*.less', source.site + '/**/*.{overrides,variables}', source.themes + '/**/*.{overrides,variables}'], function (file) {

    var lessPath, stream, compressedStream, uncompressedStream, isDefinition, isPackagedTheme, isSiteTheme, isConfig;

    // log modified file
    gulp.src(file.path).pipe(print(log.modified));

    /*--------------
       Find Source
    ---------------*/

    // recompile on *.override , *.variable change
    isConfig = file.path.indexOf('theme.config') !== -1 || file.path.indexOf('site.variables') !== -1;
    isPackagedTheme = file.path.indexOf(source.themes) !== -1;
    isSiteTheme = file.path.indexOf(source.site) !== -1;
    isDefinition = file.path.indexOf(source.definitions) !== -1;

    if (isConfig) {
      // console.info('Rebuilding all files');
      // cant rebuild paths are wrong
      // gulp.start('build-docs');
      return;
    } else if (isPackagedTheme) {
      console.log('Change detected in packaged theme');
      lessPath = util.replaceExtension(file.path, '.less');
      lessPath = lessPath.replace(tasks.regExp.theme, source.definitions);
    } else if (isSiteTheme) {
      console.log('Change detected in site theme');
      lessPath = util.replaceExtension(file.path, '.less');
      lessPath = lessPath.replace(source.site, source.definitions);
    } else {
      console.log('Change detected in definition');
      lessPath = file.path;
    }

    /*--------------
      Create CSS
    ---------------*/

    if (fs.existsSync(lessPath)) {

      // unified css stream
      stream = gulp.src(lessPath).pipe(plumber()).pipe(less(settings.less)).pipe(replace(comments.variables.in, comments.variables.out)).pipe(replace(comments.large.in, comments.large.out)).pipe(replace(comments.small.in, comments.small.out)).pipe(replace(comments.tiny.in, comments.tiny.out)).pipe(autoprefixer(settings.prefix)).pipe(gulpif(config.hasPermission, chmod(config.permission)));

      // use 2 concurrent streams from same pipe
      uncompressedStream = stream.pipe(clone());
      compressedStream = stream.pipe(clone());

      uncompressedStream.pipe(plumber()).pipe(replace(assets.source, assets.uncompressed)).pipe(header(banner, settings.header)).pipe(gulp.dest(output.uncompressed)).pipe(print(log.created)).on('end', function () {
        gulp.start('package uncompressed docs css');
      });

      compressedStream = stream.pipe(plumber()).pipe(replace(assets.source, assets.compressed)).pipe(minifyCSS(settings.minify)).pipe(rename(settings.rename.minCSS)).pipe(header(banner, settings.header)).pipe(gulp.dest(output.compressed)).pipe(print(log.created)).on('end', function () {
        gulp.start('package compressed docs css');
      });
    } else {
      console.log('Cannot find UI definition at path', lessPath);
    }
  });

  /*--------------
      Watch JS
  ---------------*/

  gulp.watch([source.definitions + '/**/*.js'], function (file) {
    gulp.src(file.path).pipe(plumber()).pipe(gulpif(config.hasPermission, chmod(config.permission))).pipe(gulp.dest(output.uncompressed)).pipe(print(log.created)).pipe(uglify(settings.uglify)).pipe(rename(settings.rename.minJS)).pipe(gulp.dest(output.compressed)).pipe(print(log.created)).on('end', function () {
      gulp.start('package compressed docs js');
      gulp.start('package uncompressed docs js');
    });
  });

  /*--------------
    Watch Assets
  ---------------*/

  // only copy assets that match component names (or their plural)
  gulp.watch([source.themes + '/**/assets/**/' + globs.components + '?(s).*'], function (file) {
    // copy assets
    gulp.src(file.path, { base: source.themes }).pipe(gulpif(config.hasPermission, chmod(config.permission))).pipe(gulp.dest(output.themes)).pipe(print(log.created));
  });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2RvY3Mvc2VydmUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBR0EsSUFDRSxPQUFlLFFBQVEsTUFBUixDQURqQjtBQUFBOzs7QUFJRSxVQUFlLFFBQVEsZ0JBQVIsQ0FKakI7QUFBQSxJQUtFLEtBQWUsUUFBUSxJQUFSLENBTGpCO0FBQUE7OztBQVFFLGVBQWUsUUFBUSxtQkFBUixDQVJqQjtBQUFBLElBU0UsUUFBZSxRQUFRLFlBQVIsQ0FUakI7QUFBQSxJQVVFLFFBQWUsUUFBUSxZQUFSLENBVmpCO0FBQUEsSUFXRSxTQUFlLFFBQVEsU0FBUixDQVhqQjtBQUFBLElBWUUsU0FBZSxRQUFRLGFBQVIsQ0FaakI7QUFBQSxJQWFFLE9BQWUsUUFBUSxXQUFSLENBYmpCO0FBQUEsSUFjRSxZQUFlLFFBQVEsaUJBQVIsQ0FkakI7QUFBQSxJQWVFLFVBQWUsUUFBUSxjQUFSLENBZmpCO0FBQUEsSUFnQkUsUUFBZSxRQUFRLFlBQVIsQ0FoQmpCO0FBQUEsSUFpQkUsU0FBZSxRQUFRLGFBQVIsQ0FqQmpCO0FBQUEsSUFrQkUsVUFBZSxRQUFRLGNBQVIsQ0FsQmpCO0FBQUEsSUFtQkUsU0FBZSxRQUFRLGFBQVIsQ0FuQmpCO0FBQUEsSUFvQkUsT0FBZSxRQUFRLFdBQVIsQ0FwQmpCO0FBQUEsSUFxQkUsUUFBZSxRQUFRLFlBQVIsQ0FyQmpCO0FBQUE7OztBQXdCRSxTQUFlLFFBQVEsZ0JBQVIsQ0F4QmpCO0FBQUE7OztBQTJCRSxRQUFlLFFBQVEsaUJBQVIsQ0EzQmpCO0FBQUEsSUE0QkUsY0FBZSxRQUFRLDBCQUFSLENBNUJqQjtBQUFBLElBNkJFLFVBQWUsUUFBUSwyQkFBUixDQTdCakI7QUFBQTs7O0FBZ0NFLFNBQWUsTUFBTSxNQWhDdkI7QUFBQSxJQWlDRSxXQUFlLE1BQU0sTUFBTixDQUFhLFFBakM5QjtBQUFBLElBa0NFLE1BQWUsTUFBTSxHQWxDdkI7QUFBQSxJQW1DRSxXQUFlLE1BQU0sUUFuQ3ZCO0FBQUEsSUFxQ0UsS0FyQ0Y7QUFBQSxJQXNDRSxNQXRDRjtBQUFBLElBdUNFLE1BdkNGO0FBQUEsSUF3Q0UsTUF4Q0Y7O0FBMkNBLFFBQVEseUJBQVIsRUFBbUMsSUFBbkM7O0FBRUEsT0FBTyxPQUFQLEdBQWlCLFlBQVk7OztBQUczQixXQUFTLFlBQVksZ0JBQVosQ0FBNkIsTUFBN0IsQ0FBVDs7O0FBR0EsVUFBUyxPQUFPLEtBQWhCO0FBQ0EsV0FBUyxPQUFPLEtBQVAsQ0FBYSxNQUF0QjtBQUNBLFdBQVMsT0FBTyxLQUFQLENBQWEsTUFBdEI7QUFDQSxXQUFTLE9BQU8sS0FBUCxDQUFhLE1BQXRCOzs7Ozs7QUFPQSxPQUNHLEtBREgsQ0FDUyxDQUNMLFlBREssQ0FEVCxFQUdLLFVBQVMsSUFBVCxFQUFlO0FBQ2hCLFlBQVEsS0FBUjtBQUNBLFdBQU8sS0FBSyxHQUFMLENBQVMsS0FBSyxJQUFkLEVBQW9CO0FBQ3ZCLFlBQU07QUFEaUIsS0FBcEIsRUFHSixJQUhJLENBR0MsS0FBSyxJQUFMLENBQVUsT0FBTyxJQUFqQixDQUhELEVBSUosSUFKSSxDQUlDLE1BQU0sSUFBSSxPQUFWLENBSkQsQ0FBUDtBQU1ELEdBWEg7Ozs7OztBQWtCQSxPQUNHLEtBREgsQ0FDUyxDQUNMLGlCQURLLENBRFQsRUFHSyxVQUFTLElBQVQsRUFBZTtBQUNoQixZQUFRLEtBQVI7QUFDQSxXQUFPLEtBQUssR0FBTCxDQUFTLEtBQUssSUFBZCxFQUFvQjtBQUN2QixZQUFNO0FBRGlCLEtBQXBCLEVBR0osSUFISSxDQUdDLEtBQUssSUFBTCxDQUFVLE9BQU8sUUFBakIsQ0FIRCxFQUlKLElBSkksQ0FJQyxNQUFNLElBQUksT0FBVixDQUpELENBQVA7QUFNRCxHQVhIOzs7Ozs7QUFrQkEsT0FDRyxLQURILENBQ1MsQ0FDTCxPQUFPLE1BREYsRUFFTCxPQUFPLFdBQVAsR0FBdUIsWUFGbEIsRUFHTCxPQUFPLElBQVAsR0FBdUIsNkJBSGxCLEVBSUwsT0FBTyxNQUFQLEdBQXVCLDZCQUpsQixDQURULEVBTUssVUFBUyxJQUFULEVBQWU7O0FBRWhCLFFBQ0UsUUFERixFQUdFLE1BSEYsRUFJRSxnQkFKRixFQUtFLGtCQUxGLEVBT0UsWUFQRixFQVFFLGVBUkYsRUFTRSxXQVRGLEVBVUUsUUFWRjs7O0FBY0EsU0FBSyxHQUFMLENBQVMsS0FBSyxJQUFkLEVBQ0csSUFESCxDQUNRLE1BQU0sSUFBSSxRQUFWLENBRFI7Ozs7Ozs7QUFTQSxlQUFtQixLQUFLLElBQUwsQ0FBVSxPQUFWLENBQWtCLGNBQWxCLE1BQXNDLENBQUMsQ0FBdkMsSUFBNEMsS0FBSyxJQUFMLENBQVUsT0FBVixDQUFrQixnQkFBbEIsTUFBd0MsQ0FBQyxDQUF4RztBQUNBLHNCQUFtQixLQUFLLElBQUwsQ0FBVSxPQUFWLENBQWtCLE9BQU8sTUFBekIsTUFBcUMsQ0FBQyxDQUF6RDtBQUNBLGtCQUFtQixLQUFLLElBQUwsQ0FBVSxPQUFWLENBQWtCLE9BQU8sSUFBekIsTUFBbUMsQ0FBQyxDQUF2RDtBQUNBLG1CQUFtQixLQUFLLElBQUwsQ0FBVSxPQUFWLENBQWtCLE9BQU8sV0FBekIsTUFBMEMsQ0FBQyxDQUE5RDs7QUFFQSxRQUFHLFFBQUgsRUFBYTs7OztBQUlYO0FBQ0QsS0FMRCxNQU1LLElBQUcsZUFBSCxFQUFvQjtBQUN2QixjQUFRLEdBQVIsQ0FBWSxtQ0FBWjtBQUNBLGlCQUFXLEtBQUssZ0JBQUwsQ0FBc0IsS0FBSyxJQUEzQixFQUFpQyxPQUFqQyxDQUFYO0FBQ0EsaUJBQVcsU0FBUyxPQUFULENBQWlCLE1BQU0sTUFBTixDQUFhLEtBQTlCLEVBQXFDLE9BQU8sV0FBNUMsQ0FBWDtBQUNELEtBSkksTUFLQSxJQUFHLFdBQUgsRUFBZ0I7QUFDbkIsY0FBUSxHQUFSLENBQVksK0JBQVo7QUFDQSxpQkFBVyxLQUFLLGdCQUFMLENBQXNCLEtBQUssSUFBM0IsRUFBaUMsT0FBakMsQ0FBWDtBQUNBLGlCQUFXLFNBQVMsT0FBVCxDQUFpQixPQUFPLElBQXhCLEVBQThCLE9BQU8sV0FBckMsQ0FBWDtBQUNELEtBSkksTUFLQTtBQUNILGNBQVEsR0FBUixDQUFZLCtCQUFaO0FBQ0EsaUJBQVcsS0FBSyxJQUFoQjtBQUNEOzs7Ozs7QUFNRCxRQUFJLEdBQUcsVUFBSCxDQUFjLFFBQWQsQ0FBSixFQUE4Qjs7O0FBRzVCLGVBQVMsS0FBSyxHQUFMLENBQVMsUUFBVCxFQUNOLElBRE0sQ0FDRCxTQURDLEVBRU4sSUFGTSxDQUVELEtBQUssU0FBUyxJQUFkLENBRkMsRUFHTixJQUhNLENBR0QsUUFBUSxTQUFTLFNBQVQsQ0FBbUIsRUFBM0IsRUFBK0IsU0FBUyxTQUFULENBQW1CLEdBQWxELENBSEMsRUFJTixJQUpNLENBSUQsUUFBUSxTQUFTLEtBQVQsQ0FBZSxFQUF2QixFQUEyQixTQUFTLEtBQVQsQ0FBZSxHQUExQyxDQUpDLEVBS04sSUFMTSxDQUtELFFBQVEsU0FBUyxLQUFULENBQWUsRUFBdkIsRUFBMkIsU0FBUyxLQUFULENBQWUsR0FBMUMsQ0FMQyxFQU1OLElBTk0sQ0FNRCxRQUFRLFNBQVMsSUFBVCxDQUFjLEVBQXRCLEVBQTBCLFNBQVMsSUFBVCxDQUFjLEdBQXhDLENBTkMsRUFPTixJQVBNLENBT0QsYUFBYSxTQUFTLE1BQXRCLENBUEMsRUFRTixJQVJNLENBUUQsT0FBTyxPQUFPLGFBQWQsRUFBNkIsTUFBTSxPQUFPLFVBQWIsQ0FBN0IsQ0FSQyxDQUFUOzs7QUFZQSwyQkFBcUIsT0FBTyxJQUFQLENBQVksT0FBWixDQUFyQjtBQUNBLHlCQUFxQixPQUFPLElBQVAsQ0FBWSxPQUFaLENBQXJCOztBQUVBLHlCQUNHLElBREgsQ0FDUSxTQURSLEVBRUcsSUFGSCxDQUVRLFFBQVEsT0FBTyxNQUFmLEVBQXVCLE9BQU8sWUFBOUIsQ0FGUixFQUdHLElBSEgsQ0FHUSxPQUFPLE1BQVAsRUFBZSxTQUFTLE1BQXhCLENBSFIsRUFJRyxJQUpILENBSVEsS0FBSyxJQUFMLENBQVUsT0FBTyxZQUFqQixDQUpSLEVBS0csSUFMSCxDQUtRLE1BQU0sSUFBSSxPQUFWLENBTFIsRUFNRyxFQU5ILENBTU0sS0FOTixFQU1hLFlBQVc7QUFDcEIsYUFBSyxLQUFMLENBQVcsK0JBQVg7QUFDRCxPQVJIOztBQVdBLHlCQUFtQixPQUNoQixJQURnQixDQUNYLFNBRFcsRUFFaEIsSUFGZ0IsQ0FFWCxRQUFRLE9BQU8sTUFBZixFQUF1QixPQUFPLFVBQTlCLENBRlcsRUFHaEIsSUFIZ0IsQ0FHWCxVQUFVLFNBQVMsTUFBbkIsQ0FIVyxFQUloQixJQUpnQixDQUlYLE9BQU8sU0FBUyxNQUFULENBQWdCLE1BQXZCLENBSlcsRUFLaEIsSUFMZ0IsQ0FLWCxPQUFPLE1BQVAsRUFBZSxTQUFTLE1BQXhCLENBTFcsRUFNaEIsSUFOZ0IsQ0FNWCxLQUFLLElBQUwsQ0FBVSxPQUFPLFVBQWpCLENBTlcsRUFPaEIsSUFQZ0IsQ0FPWCxNQUFNLElBQUksT0FBVixDQVBXLEVBUWhCLEVBUmdCLENBUWIsS0FSYSxFQVFOLFlBQVc7QUFDcEIsYUFBSyxLQUFMLENBQVcsNkJBQVg7QUFDRCxPQVZnQixDQUFuQjtBQWFELEtBMUNELE1BMkNLO0FBQ0gsY0FBUSxHQUFSLENBQVksbUNBQVosRUFBaUQsUUFBakQ7QUFDRDtBQUNGLEdBM0dIOzs7Ozs7QUFrSEEsT0FDRyxLQURILENBQ1MsQ0FDTCxPQUFPLFdBQVAsR0FBdUIsVUFEbEIsQ0FEVCxFQUdLLFVBQVMsSUFBVCxFQUFlO0FBQ2hCLFNBQUssR0FBTCxDQUFTLEtBQUssSUFBZCxFQUNHLElBREgsQ0FDUSxTQURSLEVBRUcsSUFGSCxDQUVRLE9BQU8sT0FBTyxhQUFkLEVBQTZCLE1BQU0sT0FBTyxVQUFiLENBQTdCLENBRlIsRUFHRyxJQUhILENBR1EsS0FBSyxJQUFMLENBQVUsT0FBTyxZQUFqQixDQUhSLEVBSUcsSUFKSCxDQUlRLE1BQU0sSUFBSSxPQUFWLENBSlIsRUFLRyxJQUxILENBS1EsT0FBTyxTQUFTLE1BQWhCLENBTFIsRUFNRyxJQU5ILENBTVEsT0FBTyxTQUFTLE1BQVQsQ0FBZ0IsS0FBdkIsQ0FOUixFQU9HLElBUEgsQ0FPUSxLQUFLLElBQUwsQ0FBVSxPQUFPLFVBQWpCLENBUFIsRUFRRyxJQVJILENBUVEsTUFBTSxJQUFJLE9BQVYsQ0FSUixFQVNHLEVBVEgsQ0FTTSxLQVROLEVBU2EsWUFBVztBQUNwQixXQUFLLEtBQUwsQ0FBVyw0QkFBWDtBQUNBLFdBQUssS0FBTCxDQUFXLDhCQUFYO0FBQ0QsS0FaSDtBQWNELEdBbEJIOzs7Ozs7O0FBMEJBLE9BQ0csS0FESCxDQUNTLENBQ0wsT0FBTyxNQUFQLEdBQWtCLGdCQUFsQixHQUFxQyxNQUFNLFVBQTNDLEdBQXdELFFBRG5ELENBRFQsRUFHSyxVQUFTLElBQVQsRUFBZTs7QUFFaEIsU0FBSyxHQUFMLENBQVMsS0FBSyxJQUFkLEVBQW9CLEVBQUUsTUFBTSxPQUFPLE1BQWYsRUFBcEIsRUFDRyxJQURILENBQ1EsT0FBTyxPQUFPLGFBQWQsRUFBNkIsTUFBTSxPQUFPLFVBQWIsQ0FBN0IsQ0FEUixFQUVHLElBRkgsQ0FFUSxLQUFLLElBQUwsQ0FBVSxPQUFPLE1BQWpCLENBRlIsRUFHRyxJQUhILENBR1EsTUFBTSxJQUFJLE9BQVYsQ0FIUjtBQUtELEdBVkg7QUFjRCxDQTlNRCIsImZpbGUiOiJzZXJ2ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICAgIFNlcnZlIERvY3NcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG52YXJcbiAgZ3VscCAgICAgICAgID0gcmVxdWlyZSgnZ3VscCcpLFxuXG4gIC8vIG5vZGUgZGVwZW5kZW5jaWVzXG4gIGNvbnNvbGUgICAgICA9IHJlcXVpcmUoJ2JldHRlci1jb25zb2xlJyksXG4gIGZzICAgICAgICAgICA9IHJlcXVpcmUoJ2ZzJyksXG5cbiAgLy8gZ3VscCBkZXBlbmRlbmNpZXNcbiAgYXV0b3ByZWZpeGVyID0gcmVxdWlyZSgnZ3VscC1hdXRvcHJlZml4ZXInKSxcbiAgY2htb2QgICAgICAgID0gcmVxdWlyZSgnZ3VscC1jaG1vZCcpLFxuICBjbG9uZSAgICAgICAgPSByZXF1aXJlKCdndWxwLWNsb25lJyksXG4gIGd1bHBpZiAgICAgICA9IHJlcXVpcmUoJ2d1bHAtaWYnKSxcbiAgaGVhZGVyICAgICAgID0gcmVxdWlyZSgnZ3VscC1oZWFkZXInKSxcbiAgbGVzcyAgICAgICAgID0gcmVxdWlyZSgnZ3VscC1sZXNzJyksXG4gIG1pbmlmeUNTUyAgICA9IHJlcXVpcmUoJ2d1bHAtbWluaWZ5LWNzcycpLFxuICBwbHVtYmVyICAgICAgPSByZXF1aXJlKCdndWxwLXBsdW1iZXInKSxcbiAgcHJpbnQgICAgICAgID0gcmVxdWlyZSgnZ3VscC1wcmludCcpLFxuICByZW5hbWUgICAgICAgPSByZXF1aXJlKCdndWxwLXJlbmFtZScpLFxuICByZXBsYWNlICAgICAgPSByZXF1aXJlKCdndWxwLXJlcGxhY2UnKSxcbiAgdWdsaWZ5ICAgICAgID0gcmVxdWlyZSgnZ3VscC11Z2xpZnknKSxcbiAgdXRpbCAgICAgICAgID0gcmVxdWlyZSgnZ3VscC11dGlsJyksXG4gIHdhdGNoICAgICAgICA9IHJlcXVpcmUoJ2d1bHAtd2F0Y2gnKSxcblxuICAvLyB1c2VyIGNvbmZpZ1xuICBjb25maWcgICAgICAgPSByZXF1aXJlKCcuLi9jb25maWcvZG9jcycpLFxuXG4gIC8vIHRhc2sgY29uZmlnXG4gIHRhc2tzICAgICAgICA9IHJlcXVpcmUoJy4uL2NvbmZpZy90YXNrcycpLFxuICBjb25maWdTZXR1cCAgPSByZXF1aXJlKCcuLi9jb25maWcvcHJvamVjdC9jb25maWcnKSxcbiAgaW5zdGFsbCAgICAgID0gcmVxdWlyZSgnLi4vY29uZmlnL3Byb2plY3QvaW5zdGFsbCcpLFxuXG4gIC8vIHNob3J0aGFuZFxuICBiYW5uZXIgICAgICAgPSB0YXNrcy5iYW5uZXIsXG4gIGNvbW1lbnRzICAgICA9IHRhc2tzLnJlZ0V4cC5jb21tZW50cyxcbiAgbG9nICAgICAgICAgID0gdGFza3MubG9nLFxuICBzZXR0aW5ncyAgICAgPSB0YXNrcy5zZXR0aW5ncyxcblxuICBnbG9icyxcbiAgYXNzZXRzLFxuICBvdXRwdXQsXG4gIHNvdXJjZVxuO1xuXG5yZXF1aXJlKCcuLi9jb2xsZWN0aW9ucy9pbnRlcm5hbCcpKGd1bHApO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uICgpIHtcblxuICAvLyB1c2UgYSBkaWZmZXJlbnQgY29uZmlnXG4gIGNvbmZpZyA9IGNvbmZpZ1NldHVwLmFkZERlcml2ZWRWYWx1ZXMoY29uZmlnKTtcblxuICAvLyBzaG9ydGhhbmRcbiAgZ2xvYnMgID0gY29uZmlnLmdsb2JzO1xuICBhc3NldHMgPSBjb25maWcucGF0aHMuYXNzZXRzO1xuICBvdXRwdXQgPSBjb25maWcucGF0aHMub3V0cHV0O1xuICBzb3VyY2UgPSBjb25maWcucGF0aHMuc291cmNlO1xuXG5cbiAgLyotLS0tLS0tLS0tLS0tLVxuICAgICBDb3B5IFNvdXJjZVxuICAtLS0tLS0tLS0tLS0tLS0qL1xuXG4gIGd1bHBcbiAgICAud2F0Y2goW1xuICAgICAgJ3NyYy8qKi8qLionXG4gICAgXSwgZnVuY3Rpb24oZmlsZSkge1xuICAgICAgY29uc29sZS5jbGVhcigpO1xuICAgICAgcmV0dXJuIGd1bHAuc3JjKGZpbGUucGF0aCwge1xuICAgICAgICAgIGJhc2U6ICdzcmMvJ1xuICAgICAgICB9KVxuICAgICAgICAucGlwZShndWxwLmRlc3Qob3V0cHV0Lmxlc3MpKVxuICAgICAgICAucGlwZShwcmludChsb2cuY3JlYXRlZCkpXG4gICAgICA7XG4gICAgfSlcbiAgO1xuXG4gIC8qLS0tLS0tLS0tLS0tLS1cbiAgICBDb3B5IEV4YW1wbGVzXG4gIC0tLS0tLS0tLS0tLS0tLSovXG5cbiAgZ3VscFxuICAgIC53YXRjaChbXG4gICAgICAnZXhhbXBsZXMvKiovKi4qJ1xuICAgIF0sIGZ1bmN0aW9uKGZpbGUpIHtcbiAgICAgIGNvbnNvbGUuY2xlYXIoKTtcbiAgICAgIHJldHVybiBndWxwLnNyYyhmaWxlLnBhdGgsIHtcbiAgICAgICAgICBiYXNlOiAnZXhhbXBsZXMvJ1xuICAgICAgICB9KVxuICAgICAgICAucGlwZShndWxwLmRlc3Qob3V0cHV0LmV4YW1wbGVzKSlcbiAgICAgICAgLnBpcGUocHJpbnQobG9nLmNyZWF0ZWQpKVxuICAgICAgO1xuICAgIH0pXG4gIDtcblxuICAvKi0tLS0tLS0tLS0tLS0tXG4gICAgICBXYXRjaCBDU1NcbiAgLS0tLS0tLS0tLS0tLS0tKi9cblxuICBndWxwXG4gICAgLndhdGNoKFtcbiAgICAgIHNvdXJjZS5jb25maWcsXG4gICAgICBzb3VyY2UuZGVmaW5pdGlvbnMgICArICcvKiovKi5sZXNzJyxcbiAgICAgIHNvdXJjZS5zaXRlICAgICAgICAgICsgJy8qKi8qLntvdmVycmlkZXMsdmFyaWFibGVzfScsXG4gICAgICBzb3VyY2UudGhlbWVzICAgICAgICArICcvKiovKi57b3ZlcnJpZGVzLHZhcmlhYmxlc30nXG4gICAgXSwgZnVuY3Rpb24oZmlsZSkge1xuXG4gICAgICB2YXJcbiAgICAgICAgbGVzc1BhdGgsXG5cbiAgICAgICAgc3RyZWFtLFxuICAgICAgICBjb21wcmVzc2VkU3RyZWFtLFxuICAgICAgICB1bmNvbXByZXNzZWRTdHJlYW0sXG5cbiAgICAgICAgaXNEZWZpbml0aW9uLFxuICAgICAgICBpc1BhY2thZ2VkVGhlbWUsXG4gICAgICAgIGlzU2l0ZVRoZW1lLFxuICAgICAgICBpc0NvbmZpZ1xuICAgICAgO1xuXG4gICAgICAvLyBsb2cgbW9kaWZpZWQgZmlsZVxuICAgICAgZ3VscC5zcmMoZmlsZS5wYXRoKVxuICAgICAgICAucGlwZShwcmludChsb2cubW9kaWZpZWQpKVxuICAgICAgO1xuXG4gICAgICAvKi0tLS0tLS0tLS0tLS0tXG4gICAgICAgICBGaW5kIFNvdXJjZVxuICAgICAgLS0tLS0tLS0tLS0tLS0tKi9cblxuICAgICAgLy8gcmVjb21waWxlIG9uICoub3ZlcnJpZGUgLCAqLnZhcmlhYmxlIGNoYW5nZVxuICAgICAgaXNDb25maWcgICAgICAgID0gKGZpbGUucGF0aC5pbmRleE9mKCd0aGVtZS5jb25maWcnKSAhPT0gLTEgfHwgZmlsZS5wYXRoLmluZGV4T2YoJ3NpdGUudmFyaWFibGVzJykgIT09IC0xKTtcbiAgICAgIGlzUGFja2FnZWRUaGVtZSA9IChmaWxlLnBhdGguaW5kZXhPZihzb3VyY2UudGhlbWVzKSAhPT0gLTEpO1xuICAgICAgaXNTaXRlVGhlbWUgICAgID0gKGZpbGUucGF0aC5pbmRleE9mKHNvdXJjZS5zaXRlKSAhPT0gLTEpO1xuICAgICAgaXNEZWZpbml0aW9uICAgID0gKGZpbGUucGF0aC5pbmRleE9mKHNvdXJjZS5kZWZpbml0aW9ucykgIT09IC0xKTtcblxuICAgICAgaWYoaXNDb25maWcpIHtcbiAgICAgICAgLy8gY29uc29sZS5pbmZvKCdSZWJ1aWxkaW5nIGFsbCBmaWxlcycpO1xuICAgICAgICAvLyBjYW50IHJlYnVpbGQgcGF0aHMgYXJlIHdyb25nXG4gICAgICAgIC8vIGd1bHAuc3RhcnQoJ2J1aWxkLWRvY3MnKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgZWxzZSBpZihpc1BhY2thZ2VkVGhlbWUpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0NoYW5nZSBkZXRlY3RlZCBpbiBwYWNrYWdlZCB0aGVtZScpO1xuICAgICAgICBsZXNzUGF0aCA9IHV0aWwucmVwbGFjZUV4dGVuc2lvbihmaWxlLnBhdGgsICcubGVzcycpO1xuICAgICAgICBsZXNzUGF0aCA9IGxlc3NQYXRoLnJlcGxhY2UodGFza3MucmVnRXhwLnRoZW1lLCBzb3VyY2UuZGVmaW5pdGlvbnMpO1xuICAgICAgfVxuICAgICAgZWxzZSBpZihpc1NpdGVUaGVtZSkge1xuICAgICAgICBjb25zb2xlLmxvZygnQ2hhbmdlIGRldGVjdGVkIGluIHNpdGUgdGhlbWUnKTtcbiAgICAgICAgbGVzc1BhdGggPSB1dGlsLnJlcGxhY2VFeHRlbnNpb24oZmlsZS5wYXRoLCAnLmxlc3MnKTtcbiAgICAgICAgbGVzc1BhdGggPSBsZXNzUGF0aC5yZXBsYWNlKHNvdXJjZS5zaXRlLCBzb3VyY2UuZGVmaW5pdGlvbnMpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdDaGFuZ2UgZGV0ZWN0ZWQgaW4gZGVmaW5pdGlvbicpO1xuICAgICAgICBsZXNzUGF0aCA9IGZpbGUucGF0aDtcbiAgICAgIH1cblxuICAgICAgLyotLS0tLS0tLS0tLS0tLVxuICAgICAgICBDcmVhdGUgQ1NTXG4gICAgICAtLS0tLS0tLS0tLS0tLS0qL1xuXG4gICAgICBpZiggZnMuZXhpc3RzU3luYyhsZXNzUGF0aCkgKSB7XG5cbiAgICAgICAgLy8gdW5pZmllZCBjc3Mgc3RyZWFtXG4gICAgICAgIHN0cmVhbSA9IGd1bHAuc3JjKGxlc3NQYXRoKVxuICAgICAgICAgIC5waXBlKHBsdW1iZXIoKSlcbiAgICAgICAgICAucGlwZShsZXNzKHNldHRpbmdzLmxlc3MpKVxuICAgICAgICAgIC5waXBlKHJlcGxhY2UoY29tbWVudHMudmFyaWFibGVzLmluLCBjb21tZW50cy52YXJpYWJsZXMub3V0KSlcbiAgICAgICAgICAucGlwZShyZXBsYWNlKGNvbW1lbnRzLmxhcmdlLmluLCBjb21tZW50cy5sYXJnZS5vdXQpKVxuICAgICAgICAgIC5waXBlKHJlcGxhY2UoY29tbWVudHMuc21hbGwuaW4sIGNvbW1lbnRzLnNtYWxsLm91dCkpXG4gICAgICAgICAgLnBpcGUocmVwbGFjZShjb21tZW50cy50aW55LmluLCBjb21tZW50cy50aW55Lm91dCkpXG4gICAgICAgICAgLnBpcGUoYXV0b3ByZWZpeGVyKHNldHRpbmdzLnByZWZpeCkpXG4gICAgICAgICAgLnBpcGUoZ3VscGlmKGNvbmZpZy5oYXNQZXJtaXNzaW9uLCBjaG1vZChjb25maWcucGVybWlzc2lvbikpKVxuICAgICAgICA7XG5cbiAgICAgICAgLy8gdXNlIDIgY29uY3VycmVudCBzdHJlYW1zIGZyb20gc2FtZSBwaXBlXG4gICAgICAgIHVuY29tcHJlc3NlZFN0cmVhbSA9IHN0cmVhbS5waXBlKGNsb25lKCkpO1xuICAgICAgICBjb21wcmVzc2VkU3RyZWFtICAgPSBzdHJlYW0ucGlwZShjbG9uZSgpKTtcblxuICAgICAgICB1bmNvbXByZXNzZWRTdHJlYW1cbiAgICAgICAgICAucGlwZShwbHVtYmVyKCkpXG4gICAgICAgICAgLnBpcGUocmVwbGFjZShhc3NldHMuc291cmNlLCBhc3NldHMudW5jb21wcmVzc2VkKSlcbiAgICAgICAgICAucGlwZShoZWFkZXIoYmFubmVyLCBzZXR0aW5ncy5oZWFkZXIpKVxuICAgICAgICAgIC5waXBlKGd1bHAuZGVzdChvdXRwdXQudW5jb21wcmVzc2VkKSlcbiAgICAgICAgICAucGlwZShwcmludChsb2cuY3JlYXRlZCkpXG4gICAgICAgICAgLm9uKCdlbmQnLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGd1bHAuc3RhcnQoJ3BhY2thZ2UgdW5jb21wcmVzc2VkIGRvY3MgY3NzJyk7XG4gICAgICAgICAgfSlcbiAgICAgICAgO1xuXG4gICAgICAgIGNvbXByZXNzZWRTdHJlYW0gPSBzdHJlYW1cbiAgICAgICAgICAucGlwZShwbHVtYmVyKCkpXG4gICAgICAgICAgLnBpcGUocmVwbGFjZShhc3NldHMuc291cmNlLCBhc3NldHMuY29tcHJlc3NlZCkpXG4gICAgICAgICAgLnBpcGUobWluaWZ5Q1NTKHNldHRpbmdzLm1pbmlmeSkpXG4gICAgICAgICAgLnBpcGUocmVuYW1lKHNldHRpbmdzLnJlbmFtZS5taW5DU1MpKVxuICAgICAgICAgIC5waXBlKGhlYWRlcihiYW5uZXIsIHNldHRpbmdzLmhlYWRlcikpXG4gICAgICAgICAgLnBpcGUoZ3VscC5kZXN0KG91dHB1dC5jb21wcmVzc2VkKSlcbiAgICAgICAgICAucGlwZShwcmludChsb2cuY3JlYXRlZCkpXG4gICAgICAgICAgLm9uKCdlbmQnLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGd1bHAuc3RhcnQoJ3BhY2thZ2UgY29tcHJlc3NlZCBkb2NzIGNzcycpO1xuICAgICAgICAgIH0pXG4gICAgICAgIDtcblxuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdDYW5ub3QgZmluZCBVSSBkZWZpbml0aW9uIGF0IHBhdGgnLCBsZXNzUGF0aCk7XG4gICAgICB9XG4gICAgfSlcbiAgO1xuXG4gIC8qLS0tLS0tLS0tLS0tLS1cbiAgICAgIFdhdGNoIEpTXG4gIC0tLS0tLS0tLS0tLS0tLSovXG5cbiAgZ3VscFxuICAgIC53YXRjaChbXG4gICAgICBzb3VyY2UuZGVmaW5pdGlvbnMgICArICcvKiovKi5qcydcbiAgICBdLCBmdW5jdGlvbihmaWxlKSB7XG4gICAgICBndWxwLnNyYyhmaWxlLnBhdGgpXG4gICAgICAgIC5waXBlKHBsdW1iZXIoKSlcbiAgICAgICAgLnBpcGUoZ3VscGlmKGNvbmZpZy5oYXNQZXJtaXNzaW9uLCBjaG1vZChjb25maWcucGVybWlzc2lvbikpKVxuICAgICAgICAucGlwZShndWxwLmRlc3Qob3V0cHV0LnVuY29tcHJlc3NlZCkpXG4gICAgICAgIC5waXBlKHByaW50KGxvZy5jcmVhdGVkKSlcbiAgICAgICAgLnBpcGUodWdsaWZ5KHNldHRpbmdzLnVnbGlmeSkpXG4gICAgICAgIC5waXBlKHJlbmFtZShzZXR0aW5ncy5yZW5hbWUubWluSlMpKVxuICAgICAgICAucGlwZShndWxwLmRlc3Qob3V0cHV0LmNvbXByZXNzZWQpKVxuICAgICAgICAucGlwZShwcmludChsb2cuY3JlYXRlZCkpXG4gICAgICAgIC5vbignZW5kJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgZ3VscC5zdGFydCgncGFja2FnZSBjb21wcmVzc2VkIGRvY3MganMnKTtcbiAgICAgICAgICBndWxwLnN0YXJ0KCdwYWNrYWdlIHVuY29tcHJlc3NlZCBkb2NzIGpzJyk7XG4gICAgICAgIH0pXG4gICAgICA7XG4gICAgfSlcbiAgO1xuXG4gIC8qLS0tLS0tLS0tLS0tLS1cbiAgICBXYXRjaCBBc3NldHNcbiAgLS0tLS0tLS0tLS0tLS0tKi9cblxuICAvLyBvbmx5IGNvcHkgYXNzZXRzIHRoYXQgbWF0Y2ggY29tcG9uZW50IG5hbWVzIChvciB0aGVpciBwbHVyYWwpXG4gIGd1bHBcbiAgICAud2F0Y2goW1xuICAgICAgc291cmNlLnRoZW1lcyAgICsgJy8qKi9hc3NldHMvKiovJyArIGdsb2JzLmNvbXBvbmVudHMgKyAnPyhzKS4qJ1xuICAgIF0sIGZ1bmN0aW9uKGZpbGUpIHtcbiAgICAgIC8vIGNvcHkgYXNzZXRzXG4gICAgICBndWxwLnNyYyhmaWxlLnBhdGgsIHsgYmFzZTogc291cmNlLnRoZW1lcyB9KVxuICAgICAgICAucGlwZShndWxwaWYoY29uZmlnLmhhc1Blcm1pc3Npb24sIGNobW9kKGNvbmZpZy5wZXJtaXNzaW9uKSkpXG4gICAgICAgIC5waXBlKGd1bHAuZGVzdChvdXRwdXQudGhlbWVzKSlcbiAgICAgICAgLnBpcGUocHJpbnQobG9nLmNyZWF0ZWQpKVxuICAgICAgO1xuICAgIH0pXG4gIDtcblxuXG59OyJdfQ==