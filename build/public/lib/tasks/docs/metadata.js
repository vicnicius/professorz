
/*******************************
           Summarize Docs
*******************************/

var
// node dependencies
console = require('better-console'),
    fs = require('fs'),
    YAML = require('yamljs');

var data = {};

/**
 * Test for prefix in string.
 * @param {string} str
 * @param {string} prefix
 * @return {boolean}
 */
function startsWith(str, prefix) {
  return str.indexOf(prefix) === 0;
};

function inArray(needle, haystack) {
  var length = haystack.length;
  for (var i = 0; i < length; i++) {
    if (haystack[i] == needle) return true;
  }
  return false;
}

/**
 * Parses a file for metadata and stores result in data object.
 * @param {File} file - object provided by map-stream.
 * @param {function(?,File)} - callback provided by map-stream to
 * reply when done.
 */
function parser(file, callback) {
  // file exit conditions
  if (file.isNull()) {
    return callback(null, file); // pass along
  }

  if (file.isStream()) {
    return callback(new Error('Streaming not supported'));
  }

  try {

    var
    /** @type {string} */
    text = String(file.contents.toString('utf8')),
        lines = text.split('\n'),
        filename = file.path.substring(0, file.path.length - 4),
        key = 'server/documents',
        position = filename.indexOf(key);

    // exit conditions
    if (!lines) {
      return;
    }
    if (position < 0) {
      return callback(null, file);
    }

    filename = filename.substring(position + key.length + 1, filename.length);

    var lineCount = lines.length,
        active = false,
        yaml = [],
        categories = ['UI Element', 'UI Global', 'UI Collection', 'UI View', 'UI Module', 'UI Behavior'],
        index,
        meta,
        line;

    for (index = 0; index < lineCount; index++) {

      line = lines[index];

      // Wait for metadata block to begin
      if (!active) {
        if (startsWith(line, '---')) {
          active = true;
        }
        continue;
      }
      // End of metadata block, stop parsing.
      if (startsWith(line, '---')) {
        break;
      }
      yaml.push(line);
    }

    // Parse yaml.
    meta = YAML.parse(yaml.join('\n'));
    if (meta && meta.type && meta.title && inArray(meta.type, categories)) {
      meta.category = meta.type;
      meta.filename = filename;
      meta.url = '/' + filename;
      meta.title = meta.title;
      // Primary key will by filepath
      data[meta.element] = meta;
    } else {
      // skip
      // console.log(meta);
    }
  } catch (error) {
    console.log(error, filename);
  }

  callback(null, file);
}

/**
 * Export function expected by map-stream.
 */
module.exports = {
  result: data,
  parser: parser
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2RvY3MvbWV0YWRhdGEuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFLQTs7QUFFRSxVQUFlLFFBQVEsZ0JBQVIsQ0FGakI7QUFBQSxJQUdFLEtBQWUsUUFBUSxJQUFSLENBSGpCO0FBQUEsSUFJRSxPQUFlLFFBQVEsUUFBUixDQUpqQjs7QUFPQSxJQUFJLE9BQU8sRUFBWDs7Ozs7Ozs7QUFRQSxTQUFTLFVBQVQsQ0FBb0IsR0FBcEIsRUFBeUIsTUFBekIsRUFBaUM7QUFDL0IsU0FBTyxJQUFJLE9BQUosQ0FBWSxNQUFaLE1BQXdCLENBQS9CO0FBQ0Q7O0FBRUQsU0FBUyxPQUFULENBQWlCLE1BQWpCLEVBQXlCLFFBQXpCLEVBQW1DO0FBQ2pDLE1BQUksU0FBUyxTQUFTLE1BQXRCO0FBQ0EsT0FBSSxJQUFJLElBQUksQ0FBWixFQUFlLElBQUksTUFBbkIsRUFBMkIsR0FBM0IsRUFBZ0M7QUFDNUIsUUFBRyxTQUFTLENBQVQsS0FBZSxNQUFsQixFQUEwQixPQUFPLElBQVA7QUFDN0I7QUFDRCxTQUFPLEtBQVA7QUFDRDs7Ozs7Ozs7QUFRRCxTQUFTLE1BQVQsQ0FBZ0IsSUFBaEIsRUFBc0IsUUFBdEIsRUFBZ0M7O0FBRTlCLE1BQUcsS0FBSyxNQUFMLEVBQUgsRUFBa0I7QUFDaEIsV0FBTyxTQUFTLElBQVQsRUFBZSxJQUFmLENBQVAsQztBQUNEOztBQUVELE1BQUcsS0FBSyxRQUFMLEVBQUgsRUFBb0I7QUFDbEIsV0FBTyxTQUFTLElBQUksS0FBSixDQUFVLHlCQUFWLENBQVQsQ0FBUDtBQUNEOztBQUVELE1BQUk7O0FBRUY7O0FBRUUsV0FBVyxPQUFPLEtBQUssUUFBTCxDQUFjLFFBQWQsQ0FBdUIsTUFBdkIsQ0FBUCxDQUZiO0FBQUEsUUFHRSxRQUFXLEtBQUssS0FBTCxDQUFXLElBQVgsQ0FIYjtBQUFBLFFBSUUsV0FBVyxLQUFLLElBQUwsQ0FBVSxTQUFWLENBQW9CLENBQXBCLEVBQXVCLEtBQUssSUFBTCxDQUFVLE1BQVYsR0FBbUIsQ0FBMUMsQ0FKYjtBQUFBLFFBS0UsTUFBVyxrQkFMYjtBQUFBLFFBTUUsV0FBVyxTQUFTLE9BQVQsQ0FBaUIsR0FBakIsQ0FOYjs7O0FBVUEsUUFBRyxDQUFDLEtBQUosRUFBVztBQUNUO0FBQ0Q7QUFDRCxRQUFHLFdBQVcsQ0FBZCxFQUFpQjtBQUNmLGFBQU8sU0FBUyxJQUFULEVBQWUsSUFBZixDQUFQO0FBQ0Q7O0FBRUQsZUFBVyxTQUFTLFNBQVQsQ0FBbUIsV0FBVyxJQUFJLE1BQWYsR0FBd0IsQ0FBM0MsRUFBOEMsU0FBUyxNQUF2RCxDQUFYOztBQUVBLFFBQ0UsWUFBWSxNQUFNLE1BRHBCO0FBQUEsUUFFRSxTQUFZLEtBRmQ7QUFBQSxRQUdFLE9BQVksRUFIZDtBQUFBLFFBSUUsYUFBYSxDQUNYLFlBRFcsRUFFWCxXQUZXLEVBR1gsZUFIVyxFQUlYLFNBSlcsRUFLWCxXQUxXLEVBTVgsYUFOVyxDQUpmO0FBQUEsUUFZRSxLQVpGO0FBQUEsUUFhRSxJQWJGO0FBQUEsUUFjRSxJQWRGOztBQWlCQSxTQUFJLFFBQVEsQ0FBWixFQUFlLFFBQVEsU0FBdkIsRUFBa0MsT0FBbEMsRUFBMkM7O0FBRXpDLGFBQU8sTUFBTSxLQUFOLENBQVA7OztBQUdBLFVBQUcsQ0FBQyxNQUFKLEVBQVk7QUFDVixZQUFHLFdBQVcsSUFBWCxFQUFpQixLQUFqQixDQUFILEVBQTRCO0FBQzFCLG1CQUFTLElBQVQ7QUFDRDtBQUNEO0FBQ0Q7O0FBRUQsVUFBRyxXQUFXLElBQVgsRUFBaUIsS0FBakIsQ0FBSCxFQUE0QjtBQUMxQjtBQUNEO0FBQ0QsV0FBSyxJQUFMLENBQVUsSUFBVjtBQUNEOzs7QUFJRCxXQUFPLEtBQUssS0FBTCxDQUFXLEtBQUssSUFBTCxDQUFVLElBQVYsQ0FBWCxDQUFQO0FBQ0EsUUFBRyxRQUFRLEtBQUssSUFBYixJQUFxQixLQUFLLEtBQTFCLElBQW1DLFFBQVEsS0FBSyxJQUFiLEVBQW1CLFVBQW5CLENBQXRDLEVBQXVFO0FBQ3JFLFdBQUssUUFBTCxHQUFnQixLQUFLLElBQXJCO0FBQ0EsV0FBSyxRQUFMLEdBQWdCLFFBQWhCO0FBQ0EsV0FBSyxHQUFMLEdBQWdCLE1BQU0sUUFBdEI7QUFDQSxXQUFLLEtBQUwsR0FBZ0IsS0FBSyxLQUFyQjs7QUFFQSxXQUFLLEtBQUssT0FBVixJQUFxQixJQUFyQjtBQUNELEtBUEQsTUFRSzs7O0FBR0o7QUFHRixHQXpFRCxDQTJFQSxPQUFNLEtBQU4sRUFBYTtBQUNYLFlBQVEsR0FBUixDQUFZLEtBQVosRUFBbUIsUUFBbkI7QUFDRDs7QUFFRCxXQUFTLElBQVQsRUFBZSxJQUFmO0FBRUQ7Ozs7O0FBS0QsT0FBTyxPQUFQLEdBQWlCO0FBQ2YsVUFBUyxJQURNO0FBRWYsVUFBUztBQUZNLENBQWpCIiwiZmlsZSI6Im1ldGFkYXRhLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgICBTdW1tYXJpemUgRG9jc1xuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxudmFyXG4gIC8vIG5vZGUgZGVwZW5kZW5jaWVzXG4gIGNvbnNvbGUgICAgICA9IHJlcXVpcmUoJ2JldHRlci1jb25zb2xlJyksXG4gIGZzICAgICAgICAgICA9IHJlcXVpcmUoJ2ZzJyksXG4gIFlBTUwgICAgICAgICA9IHJlcXVpcmUoJ3lhbWxqcycpXG47XG5cbnZhciBkYXRhID0ge307XG5cbi8qKlxuICogVGVzdCBmb3IgcHJlZml4IGluIHN0cmluZy5cbiAqIEBwYXJhbSB7c3RyaW5nfSBzdHJcbiAqIEBwYXJhbSB7c3RyaW5nfSBwcmVmaXhcbiAqIEByZXR1cm4ge2Jvb2xlYW59XG4gKi9cbmZ1bmN0aW9uIHN0YXJ0c1dpdGgoc3RyLCBwcmVmaXgpIHtcbiAgcmV0dXJuIHN0ci5pbmRleE9mKHByZWZpeCkgPT09IDA7XG59O1xuXG5mdW5jdGlvbiBpbkFycmF5KG5lZWRsZSwgaGF5c3RhY2spIHtcbiAgdmFyIGxlbmd0aCA9IGhheXN0YWNrLmxlbmd0aDtcbiAgZm9yKHZhciBpID0gMDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgICBpZihoYXlzdGFja1tpXSA9PSBuZWVkbGUpIHJldHVybiB0cnVlO1xuICB9XG4gIHJldHVybiBmYWxzZTtcbn1cblxuLyoqXG4gKiBQYXJzZXMgYSBmaWxlIGZvciBtZXRhZGF0YSBhbmQgc3RvcmVzIHJlc3VsdCBpbiBkYXRhIG9iamVjdC5cbiAqIEBwYXJhbSB7RmlsZX0gZmlsZSAtIG9iamVjdCBwcm92aWRlZCBieSBtYXAtc3RyZWFtLlxuICogQHBhcmFtIHtmdW5jdGlvbig/LEZpbGUpfSAtIGNhbGxiYWNrIHByb3ZpZGVkIGJ5IG1hcC1zdHJlYW0gdG9cbiAqIHJlcGx5IHdoZW4gZG9uZS5cbiAqL1xuZnVuY3Rpb24gcGFyc2VyKGZpbGUsIGNhbGxiYWNrKSB7XG4gIC8vIGZpbGUgZXhpdCBjb25kaXRpb25zXG4gIGlmKGZpbGUuaXNOdWxsKCkpIHtcbiAgICByZXR1cm4gY2FsbGJhY2sobnVsbCwgZmlsZSk7IC8vIHBhc3MgYWxvbmdcbiAgfVxuXG4gIGlmKGZpbGUuaXNTdHJlYW0oKSkge1xuICAgIHJldHVybiBjYWxsYmFjayhuZXcgRXJyb3IoJ1N0cmVhbWluZyBub3Qgc3VwcG9ydGVkJykpO1xuICB9XG5cbiAgdHJ5IHtcblxuICAgIHZhclxuICAgICAgLyoqIEB0eXBlIHtzdHJpbmd9ICovXG4gICAgICB0ZXh0ICAgICA9IFN0cmluZyhmaWxlLmNvbnRlbnRzLnRvU3RyaW5nKCd1dGY4JykpLFxuICAgICAgbGluZXMgICAgPSB0ZXh0LnNwbGl0KCdcXG4nKSxcbiAgICAgIGZpbGVuYW1lID0gZmlsZS5wYXRoLnN1YnN0cmluZygwLCBmaWxlLnBhdGgubGVuZ3RoIC0gNCksXG4gICAgICBrZXkgICAgICA9ICdzZXJ2ZXIvZG9jdW1lbnRzJyxcbiAgICAgIHBvc2l0aW9uID0gZmlsZW5hbWUuaW5kZXhPZihrZXkpXG4gICAgO1xuXG4gICAgLy8gZXhpdCBjb25kaXRpb25zXG4gICAgaWYoIWxpbmVzKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmKHBvc2l0aW9uIDwgMCkge1xuICAgICAgcmV0dXJuIGNhbGxiYWNrKG51bGwsIGZpbGUpO1xuICAgIH1cblxuICAgIGZpbGVuYW1lID0gZmlsZW5hbWUuc3Vic3RyaW5nKHBvc2l0aW9uICsga2V5Lmxlbmd0aCArIDEsIGZpbGVuYW1lLmxlbmd0aCk7XG5cbiAgICB2YXJcbiAgICAgIGxpbmVDb3VudCA9IGxpbmVzLmxlbmd0aCxcbiAgICAgIGFjdGl2ZSAgICA9IGZhbHNlLFxuICAgICAgeWFtbCAgICAgID0gW10sXG4gICAgICBjYXRlZ29yaWVzID0gW1xuICAgICAgICAnVUkgRWxlbWVudCcsXG4gICAgICAgICdVSSBHbG9iYWwnLFxuICAgICAgICAnVUkgQ29sbGVjdGlvbicsXG4gICAgICAgICdVSSBWaWV3JyxcbiAgICAgICAgJ1VJIE1vZHVsZScsXG4gICAgICAgICdVSSBCZWhhdmlvcidcbiAgICAgIF0sXG4gICAgICBpbmRleCxcbiAgICAgIG1ldGEsXG4gICAgICBsaW5lXG4gICAgO1xuXG4gICAgZm9yKGluZGV4ID0gMDsgaW5kZXggPCBsaW5lQ291bnQ7IGluZGV4KyspIHtcblxuICAgICAgbGluZSA9IGxpbmVzW2luZGV4XTtcblxuICAgICAgLy8gV2FpdCBmb3IgbWV0YWRhdGEgYmxvY2sgdG8gYmVnaW5cbiAgICAgIGlmKCFhY3RpdmUpIHtcbiAgICAgICAgaWYoc3RhcnRzV2l0aChsaW5lLCAnLS0tJykpIHtcbiAgICAgICAgICBhY3RpdmUgPSB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuICAgICAgLy8gRW5kIG9mIG1ldGFkYXRhIGJsb2NrLCBzdG9wIHBhcnNpbmcuXG4gICAgICBpZihzdGFydHNXaXRoKGxpbmUsICctLS0nKSkge1xuICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICAgIHlhbWwucHVzaChsaW5lKTtcbiAgICB9XG5cblxuICAgIC8vIFBhcnNlIHlhbWwuXG4gICAgbWV0YSA9IFlBTUwucGFyc2UoeWFtbC5qb2luKCdcXG4nKSk7XG4gICAgaWYobWV0YSAmJiBtZXRhLnR5cGUgJiYgbWV0YS50aXRsZSAmJiBpbkFycmF5KG1ldGEudHlwZSwgY2F0ZWdvcmllcykgKSB7XG4gICAgICBtZXRhLmNhdGVnb3J5ID0gbWV0YS50eXBlO1xuICAgICAgbWV0YS5maWxlbmFtZSA9IGZpbGVuYW1lO1xuICAgICAgbWV0YS51cmwgICAgICA9ICcvJyArIGZpbGVuYW1lO1xuICAgICAgbWV0YS50aXRsZSAgICA9IG1ldGEudGl0bGU7XG4gICAgICAvLyBQcmltYXJ5IGtleSB3aWxsIGJ5IGZpbGVwYXRoXG4gICAgICBkYXRhW21ldGEuZWxlbWVudF0gPSBtZXRhO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIC8vIHNraXBcbiAgICAgIC8vIGNvbnNvbGUubG9nKG1ldGEpO1xuICAgIH1cblxuXG4gIH1cblxuICBjYXRjaChlcnJvcikge1xuICAgIGNvbnNvbGUubG9nKGVycm9yLCBmaWxlbmFtZSk7XG4gIH1cblxuICBjYWxsYmFjayhudWxsLCBmaWxlKTtcblxufVxuXG4vKipcbiAqIEV4cG9ydCBmdW5jdGlvbiBleHBlY3RlZCBieSBtYXAtc3RyZWFtLlxuICovXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgcmVzdWx0IDogZGF0YSxcbiAgcGFyc2VyIDogcGFyc2VyXG59O1xuIl19