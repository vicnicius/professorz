/*******************************
          Clean Task
*******************************/

var del = require('del'),
    config = require('./config/user'),
    tasks = require('./config/tasks');

// cleans distribution files
module.exports = function (callback) {
  return del([config.paths.clean], tasks.settings.del, callback);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NsZWFuLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFJQSxJQUNFLE1BQVMsUUFBUSxLQUFSLENBRFg7QUFBQSxJQUVFLFNBQVMsUUFBUSxlQUFSLENBRlg7QUFBQSxJQUdFLFFBQVMsUUFBUSxnQkFBUixDQUhYOzs7QUFPQSxPQUFPLE9BQVAsR0FBaUIsVUFBUyxRQUFULEVBQW1CO0FBQ2xDLFNBQU8sSUFBSSxDQUFDLE9BQU8sS0FBUCxDQUFhLEtBQWQsQ0FBSixFQUEwQixNQUFNLFFBQU4sQ0FBZSxHQUF6QyxFQUE4QyxRQUE5QyxDQUFQO0FBQ0QsQ0FGRCIsImZpbGUiOiJjbGVhbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICAgQ2xlYW4gVGFza1xuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxudmFyXG4gIGRlbCAgICA9IHJlcXVpcmUoJ2RlbCcpLFxuICBjb25maWcgPSByZXF1aXJlKCcuL2NvbmZpZy91c2VyJyksXG4gIHRhc2tzICA9IHJlcXVpcmUoJy4vY29uZmlnL3Rhc2tzJylcbjtcblxuLy8gY2xlYW5zIGRpc3RyaWJ1dGlvbiBmaWxlc1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihjYWxsYmFjaykge1xuICByZXR1cm4gZGVsKFtjb25maWcucGF0aHMuY2xlYW5dLCB0YXNrcy5zZXR0aW5ncy5kZWwsIGNhbGxiYWNrKTtcbn07Il19