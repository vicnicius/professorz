/*******************************
             Docs
*******************************/

/* Paths used for "serve-docs" and "build-docs" tasks */
module.exports = {
  base: '',
  globs: {
    eco: '**/*.html.eco'
  },
  paths: {
    clean: '../docs/out/dist/',
    source: {
      config: 'src/theme.config',
      definitions: 'src/definitions/',
      site: 'src/site/',
      themes: 'src/themes/'
    },
    output: {
      examples: '../docs/out/examples/',
      less: '../docs/out/src/',
      metadata: '../docs/out/',
      packaged: '../docs/out/dist/',
      uncompressed: '../docs/out/dist/components/',
      compressed: '../docs/out/dist/components/',
      themes: '../docs/out/dist/themes/'
    },
    template: {
      eco: '../docs/server/documents/'
    }
  }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NvbmZpZy9kb2NzLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBS0EsT0FBTyxPQUFQLEdBQWlCO0FBQ2YsUUFBTSxFQURTO0FBRWYsU0FBTztBQUNMLFNBQUs7QUFEQSxHQUZRO0FBS2YsU0FBTztBQUNMLFdBQU8sbUJBREY7QUFFTCxZQUFRO0FBQ04sY0FBYyxrQkFEUjtBQUVOLG1CQUFjLGtCQUZSO0FBR04sWUFBYyxXQUhSO0FBSU4sY0FBYztBQUpSLEtBRkg7QUFRTCxZQUFRO0FBQ04sZ0JBQWUsdUJBRFQ7QUFFTixZQUFlLGtCQUZUO0FBR04sZ0JBQWUsY0FIVDtBQUlOLGdCQUFlLG1CQUpUO0FBS04sb0JBQWUsOEJBTFQ7QUFNTixrQkFBZSw4QkFOVDtBQU9OLGNBQWU7QUFQVCxLQVJIO0FBaUJMLGNBQVU7QUFDUixXQUFLO0FBREc7QUFqQkw7QUFMUSxDQUFqQiIsImZpbGUiOiJkb2NzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAgICAgICAgICBEb2NzXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4vKiBQYXRocyB1c2VkIGZvciBcInNlcnZlLWRvY3NcIiBhbmQgXCJidWlsZC1kb2NzXCIgdGFza3MgKi9cbm1vZHVsZS5leHBvcnRzID0ge1xuICBiYXNlOiAnJyxcbiAgZ2xvYnM6IHtcbiAgICBlY286ICcqKi8qLmh0bWwuZWNvJ1xuICB9LFxuICBwYXRoczoge1xuICAgIGNsZWFuOiAnLi4vZG9jcy9vdXQvZGlzdC8nLFxuICAgIHNvdXJjZToge1xuICAgICAgY29uZmlnICAgICAgOiAnc3JjL3RoZW1lLmNvbmZpZycsXG4gICAgICBkZWZpbml0aW9ucyA6ICdzcmMvZGVmaW5pdGlvbnMvJyxcbiAgICAgIHNpdGUgICAgICAgIDogJ3NyYy9zaXRlLycsXG4gICAgICB0aGVtZXMgICAgICA6ICdzcmMvdGhlbWVzLydcbiAgICB9LFxuICAgIG91dHB1dDoge1xuICAgICAgZXhhbXBsZXMgICAgIDogJy4uL2RvY3Mvb3V0L2V4YW1wbGVzLycsXG4gICAgICBsZXNzICAgICAgICAgOiAnLi4vZG9jcy9vdXQvc3JjLycsXG4gICAgICBtZXRhZGF0YSAgICAgOiAnLi4vZG9jcy9vdXQvJyxcbiAgICAgIHBhY2thZ2VkICAgICA6ICcuLi9kb2NzL291dC9kaXN0LycsXG4gICAgICB1bmNvbXByZXNzZWQgOiAnLi4vZG9jcy9vdXQvZGlzdC9jb21wb25lbnRzLycsXG4gICAgICBjb21wcmVzc2VkICAgOiAnLi4vZG9jcy9vdXQvZGlzdC9jb21wb25lbnRzLycsXG4gICAgICB0aGVtZXMgICAgICAgOiAnLi4vZG9jcy9vdXQvZGlzdC90aGVtZXMvJ1xuICAgIH0sXG4gICAgdGVtcGxhdGU6IHtcbiAgICAgIGVjbzogJy4uL2RvY3Mvc2VydmVyL2RvY3VtZW50cy8nXG4gICAgfSxcbiAgfVxufTtcbiJdfQ==