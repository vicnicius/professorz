var console = require('better-console'),
    config = require('./user'),
    release = require('./project/release');

module.exports = {

  banner: release.banner,

  log: {
    created: function (file) {
      return 'Created: ' + file;
    },
    modified: function (file) {
      return 'Modified: ' + file;
    }
  },

  filenames: {
    concatenatedCSS: 'semantic.css',
    concatenatedJS: 'semantic.js',
    concatenatedMinifiedCSS: 'semantic.min.css',
    concatenatedMinifiedJS: 'semantic.min.js',
    concatenatedRTLCSS: 'semantic.rtl.css',
    concatenatedMinifiedRTLCSS: 'semantic.rtl.min.css'
  },

  regExp: {

    comments: {

      // remove all comments from config files (.variable)
      variables: {
        in: /(\/\*[\s\S]+?\*\/+)[\s\S]+?\/\* End Config \*\//,
        out: '$1'
      },

      // add version to first comment
      license: {
        in: /(^\/\*[\s\S]+)(# Semantic UI )([\s\S]+?\*\/)/,
        out: '$1$2' + release.version + ' $3'
      },

      // adds uniform spacing around comments
      large: {
        in: /(\/\*\*\*\*[\s\S]+?\*\/)/mg,
        out: '\n\n$1\n'
      },
      small: {
        in: /(\/\*---[\s\S]+?\*\/)/mg,
        out: '\n$1\n'
      },
      tiny: {
        in: /(\/\* [\s\S]+? \*\/)/mg,
        out: '\n$1'
      }
    },

    theme: /.*(\/|\\)themes(\/|\\).*?(?=(\/|\\))/mg

  },

  settings: {

    /* Remove Files in Clean */
    del: {
      silent: true
    },

    concatCSS: {
      rebaseUrls: false
    },

    /* Comment Banners */
    header: {
      title: release.title,
      version: release.version,
      repository: release.repository,
      url: release.url
    },

    plumber: {
      less: {
        errorHandler: function (error) {
          var regExp = {
            variable: /@(\S.*?)\s/,
            theme: /themes[\/\\]+(.*?)[\/\\].*/,
            element: /[\/\\]([^\/\\*]*)\.overrides/
          },
              theme,
              element;
          if (error.filename.match(/theme.less/)) {
            if (error.line == 5) {
              element = regExp.variable.exec(error.message)[1];
              if (element) {
                console.error('Missing theme.config value for ', element);
              }
              console.error('Most likely new UI was added in an update. You will need to add missing elements from theme.config.example');
            }
            if (error.line == 46) {
              element = regExp.element.exec(error.message)[1];
              theme = regExp.theme.exec(error.message)[1];
              console.error(theme + ' is not an available theme for ' + element);
            }
          } else {
            console.log(error);
          }
          this.emit('end');
        }
      }
    },

    /* What Browsers to Prefix */
    prefix: {
      browsers: ['last 2 versions', '> 1%', 'opera 12.1', 'bb 10', 'android 4']
    },

    /* File Renames */
    rename: {
      minJS: { extname: '.min.js' },
      minCSS: { extname: '.min.css' },
      rtlCSS: { extname: '.rtl.css' },
      rtlMinCSS: { extname: '.rtl.min.css' }
    },

    /* Minified CSS Concat */
    minify: {
      processImport: false,
      restructuring: false,
      keepSpecialComments: 1
    },

    /* Minified JS Settings */
    uglify: {
      mangle: true,
      preserveComments: 'some'
    },

    /* Minified Concat CSS Settings */
    concatMinify: {
      processImport: false,
      restructuring: false,
      keepSpecialComments: false
    },

    /* Minified Concat JS */
    concatUglify: {
      mangle: true,
      preserveComments: false
    }

  }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NvbmZpZy90YXNrcy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxJQUNFLFVBQVUsUUFBUSxnQkFBUixDQURaO0FBQUEsSUFFRSxTQUFVLFFBQVEsUUFBUixDQUZaO0FBQUEsSUFHRSxVQUFVLFFBQVEsbUJBQVIsQ0FIWjs7QUFPQSxPQUFPLE9BQVAsR0FBaUI7O0FBRWYsVUFBUyxRQUFRLE1BRkY7O0FBSWYsT0FBSztBQUNILGFBQVMsVUFBUyxJQUFULEVBQWU7QUFDdEIsYUFBTyxjQUFjLElBQXJCO0FBQ0QsS0FIRTtBQUlILGNBQVUsVUFBUyxJQUFULEVBQWU7QUFDdkIsYUFBTyxlQUFlLElBQXRCO0FBQ0Q7QUFORSxHQUpVOztBQWFmLGFBQVc7QUFDVCxxQkFBNkIsY0FEcEI7QUFFVCxvQkFBNkIsYUFGcEI7QUFHVCw2QkFBNkIsa0JBSHBCO0FBSVQsNEJBQTZCLGlCQUpwQjtBQUtULHdCQUE2QixrQkFMcEI7QUFNVCxnQ0FBNkI7QUFOcEIsR0FiSTs7QUFzQmYsVUFBUTs7QUFFTixjQUFVOzs7QUFHUixpQkFBWTtBQUNWLFlBQU0saURBREk7QUFFVixhQUFNO0FBRkksT0FISjs7O0FBU1IsZUFBUztBQUNQLFlBQU0sOENBREM7QUFFUCxhQUFNLFNBQVMsUUFBUSxPQUFqQixHQUEyQjtBQUYxQixPQVREOzs7QUFlUixhQUFPO0FBQ0wsWUFBTSw0QkFERDtBQUVMLGFBQU07QUFGRCxPQWZDO0FBbUJSLGFBQU87QUFDTCxZQUFNLHlCQUREO0FBRUwsYUFBTTtBQUZELE9BbkJDO0FBdUJSLFlBQU07QUFDSixZQUFNLHdCQURGO0FBRUosYUFBTTtBQUZGO0FBdkJFLEtBRko7O0FBK0JOLFdBQU87O0FBL0JELEdBdEJPOztBQXlEZixZQUFVOzs7QUFHUixTQUFLO0FBQ0gsY0FBUztBQUROLEtBSEc7O0FBT1IsZUFBVztBQUNULGtCQUFZO0FBREgsS0FQSDs7O0FBWVIsWUFBUTtBQUNOLGFBQWEsUUFBUSxLQURmO0FBRU4sZUFBYSxRQUFRLE9BRmY7QUFHTixrQkFBYSxRQUFRLFVBSGY7QUFJTixXQUFhLFFBQVE7QUFKZixLQVpBOztBQW1CUixhQUFTO0FBQ1AsWUFBTTtBQUNKLHNCQUFjLFVBQVMsS0FBVCxFQUFnQjtBQUM1QixjQUNFLFNBQVM7QUFDUCxzQkFBVyxZQURKO0FBRVAsbUJBQVcsNEJBRko7QUFHUCxxQkFBVztBQUhKLFdBRFg7QUFBQSxjQU1FLEtBTkY7QUFBQSxjQU9FLE9BUEY7QUFTQSxjQUFHLE1BQU0sUUFBTixDQUFlLEtBQWYsQ0FBcUIsWUFBckIsQ0FBSCxFQUF1QztBQUNyQyxnQkFBRyxNQUFNLElBQU4sSUFBYyxDQUFqQixFQUFvQjtBQUNsQix3QkFBVyxPQUFPLFFBQVAsQ0FBZ0IsSUFBaEIsQ0FBcUIsTUFBTSxPQUEzQixFQUFvQyxDQUFwQyxDQUFYO0FBQ0Esa0JBQUcsT0FBSCxFQUFZO0FBQ1Ysd0JBQVEsS0FBUixDQUFjLGlDQUFkLEVBQWlELE9BQWpEO0FBQ0Q7QUFDRCxzQkFBUSxLQUFSLENBQWMsNEdBQWQ7QUFDRDtBQUNELGdCQUFHLE1BQU0sSUFBTixJQUFjLEVBQWpCLEVBQXFCO0FBQ25CLHdCQUFVLE9BQU8sT0FBUCxDQUFlLElBQWYsQ0FBb0IsTUFBTSxPQUExQixFQUFtQyxDQUFuQyxDQUFWO0FBQ0Esc0JBQVUsT0FBTyxLQUFQLENBQWEsSUFBYixDQUFrQixNQUFNLE9BQXhCLEVBQWlDLENBQWpDLENBQVY7QUFDQSxzQkFBUSxLQUFSLENBQWMsUUFBUSxpQ0FBUixHQUE0QyxPQUExRDtBQUNEO0FBQ0YsV0FiRCxNQWNLO0FBQ0gsb0JBQVEsR0FBUixDQUFZLEtBQVo7QUFDRDtBQUNELGVBQUssSUFBTCxDQUFVLEtBQVY7QUFDRDtBQTdCRztBQURDLEtBbkJEOzs7QUFzRFIsWUFBUTtBQUNOLGdCQUFVLENBQ1IsaUJBRFEsRUFFUixNQUZRLEVBR1IsWUFIUSxFQUlSLE9BSlEsRUFLUixXQUxRO0FBREosS0F0REE7OztBQWlFUixZQUFRO0FBQ04sYUFBWSxFQUFFLFNBQVUsU0FBWixFQUROO0FBRU4sY0FBWSxFQUFFLFNBQVUsVUFBWixFQUZOO0FBR04sY0FBWSxFQUFFLFNBQVUsVUFBWixFQUhOO0FBSU4saUJBQVksRUFBRSxTQUFVLGNBQVo7QUFKTixLQWpFQTs7O0FBeUVSLFlBQVE7QUFDTixxQkFBc0IsS0FEaEI7QUFFTixxQkFBc0IsS0FGaEI7QUFHTiwyQkFBc0I7QUFIaEIsS0F6RUE7OztBQWdGUixZQUFRO0FBQ04sY0FBbUIsSUFEYjtBQUVOLHdCQUFtQjtBQUZiLEtBaEZBOzs7QUFzRlIsa0JBQWM7QUFDWixxQkFBc0IsS0FEVjtBQUVaLHFCQUFzQixLQUZWO0FBR1osMkJBQXNCO0FBSFYsS0F0Rk47OztBQTZGUixrQkFBYztBQUNaLGNBQW1CLElBRFA7QUFFWix3QkFBbUI7QUFGUDs7QUE3Rk47QUF6REssQ0FBakIiLCJmaWxlIjoidGFza3MuanMiLCJzb3VyY2VzQ29udGVudCI6WyJ2YXJcbiAgY29uc29sZSA9IHJlcXVpcmUoJ2JldHRlci1jb25zb2xlJyksXG4gIGNvbmZpZyAgPSByZXF1aXJlKCcuL3VzZXInKSxcbiAgcmVsZWFzZSA9IHJlcXVpcmUoJy4vcHJvamVjdC9yZWxlYXNlJylcbjtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IHtcblxuICBiYW5uZXIgOiByZWxlYXNlLmJhbm5lcixcblxuICBsb2c6IHtcbiAgICBjcmVhdGVkOiBmdW5jdGlvbihmaWxlKSB7XG4gICAgICByZXR1cm4gJ0NyZWF0ZWQ6ICcgKyBmaWxlO1xuICAgIH0sXG4gICAgbW9kaWZpZWQ6IGZ1bmN0aW9uKGZpbGUpIHtcbiAgICAgIHJldHVybiAnTW9kaWZpZWQ6ICcgKyBmaWxlO1xuICAgIH1cbiAgfSxcblxuICBmaWxlbmFtZXM6IHtcbiAgICBjb25jYXRlbmF0ZWRDU1MgICAgICAgICAgICA6ICdzZW1hbnRpYy5jc3MnLFxuICAgIGNvbmNhdGVuYXRlZEpTICAgICAgICAgICAgIDogJ3NlbWFudGljLmpzJyxcbiAgICBjb25jYXRlbmF0ZWRNaW5pZmllZENTUyAgICA6ICdzZW1hbnRpYy5taW4uY3NzJyxcbiAgICBjb25jYXRlbmF0ZWRNaW5pZmllZEpTICAgICA6ICdzZW1hbnRpYy5taW4uanMnLFxuICAgIGNvbmNhdGVuYXRlZFJUTENTUyAgICAgICAgIDogJ3NlbWFudGljLnJ0bC5jc3MnLFxuICAgIGNvbmNhdGVuYXRlZE1pbmlmaWVkUlRMQ1NTIDogJ3NlbWFudGljLnJ0bC5taW4uY3NzJ1xuICB9LFxuXG4gIHJlZ0V4cDoge1xuXG4gICAgY29tbWVudHM6IHtcblxuICAgICAgLy8gcmVtb3ZlIGFsbCBjb21tZW50cyBmcm9tIGNvbmZpZyBmaWxlcyAoLnZhcmlhYmxlKVxuICAgICAgdmFyaWFibGVzIDoge1xuICAgICAgICBpbiAgOiAvKFxcL1xcKltcXHNcXFNdKz9cXCpcXC8rKVtcXHNcXFNdKz9cXC9cXCogRW5kIENvbmZpZyBcXCpcXC8vLFxuICAgICAgICBvdXQgOiAnJDEnLFxuICAgICAgfSxcblxuICAgICAgLy8gYWRkIHZlcnNpb24gdG8gZmlyc3QgY29tbWVudFxuICAgICAgbGljZW5zZToge1xuICAgICAgICBpbiAgOiAvKF5cXC9cXCpbXFxzXFxTXSspKCMgU2VtYW50aWMgVUkgKShbXFxzXFxTXSs/XFwqXFwvKS8sXG4gICAgICAgIG91dCA6ICckMSQyJyArIHJlbGVhc2UudmVyc2lvbiArICcgJDMnXG4gICAgICB9LFxuXG4gICAgICAvLyBhZGRzIHVuaWZvcm0gc3BhY2luZyBhcm91bmQgY29tbWVudHNcbiAgICAgIGxhcmdlOiB7XG4gICAgICAgIGluICA6IC8oXFwvXFwqXFwqXFwqXFwqW1xcc1xcU10rP1xcKlxcLykvbWcsXG4gICAgICAgIG91dCA6ICdcXG5cXG4kMVxcbidcbiAgICAgIH0sXG4gICAgICBzbWFsbDoge1xuICAgICAgICBpbiAgOiAvKFxcL1xcKi0tLVtcXHNcXFNdKz9cXCpcXC8pL21nLFxuICAgICAgICBvdXQgOiAnXFxuJDFcXG4nXG4gICAgICB9LFxuICAgICAgdGlueToge1xuICAgICAgICBpbiAgOiAvKFxcL1xcKiBbXFxzXFxTXSs/IFxcKlxcLykvbWcsXG4gICAgICAgIG91dCA6ICdcXG4kMSdcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgdGhlbWU6IC8uKihcXC98XFxcXCl0aGVtZXMoXFwvfFxcXFwpLio/KD89KFxcL3xcXFxcKSkvbWdcblxuICB9LFxuXG4gIHNldHRpbmdzOiB7XG5cbiAgICAvKiBSZW1vdmUgRmlsZXMgaW4gQ2xlYW4gKi9cbiAgICBkZWw6IHtcbiAgICAgIHNpbGVudCA6IHRydWVcbiAgICB9LFxuXG4gICAgY29uY2F0Q1NTOiB7XG4gICAgICByZWJhc2VVcmxzOiBmYWxzZVxuICAgIH0sXG5cbiAgICAvKiBDb21tZW50IEJhbm5lcnMgKi9cbiAgICBoZWFkZXI6IHtcbiAgICAgIHRpdGxlICAgICAgOiByZWxlYXNlLnRpdGxlLFxuICAgICAgdmVyc2lvbiAgICA6IHJlbGVhc2UudmVyc2lvbixcbiAgICAgIHJlcG9zaXRvcnkgOiByZWxlYXNlLnJlcG9zaXRvcnksXG4gICAgICB1cmwgICAgICAgIDogcmVsZWFzZS51cmxcbiAgICB9LFxuXG4gICAgcGx1bWJlcjoge1xuICAgICAgbGVzczoge1xuICAgICAgICBlcnJvckhhbmRsZXI6IGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICByZWdFeHAgPSB7XG4gICAgICAgICAgICAgIHZhcmlhYmxlIDogL0AoXFxTLio/KVxccy8sXG4gICAgICAgICAgICAgIHRoZW1lICAgIDogL3RoZW1lc1tcXC9cXFxcXSsoLio/KVtcXC9cXFxcXS4qLyxcbiAgICAgICAgICAgICAgZWxlbWVudCAgOiAvW1xcL1xcXFxdKFteXFwvXFxcXCpdKilcXC5vdmVycmlkZXMvXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGhlbWUsXG4gICAgICAgICAgICBlbGVtZW50XG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKGVycm9yLmZpbGVuYW1lLm1hdGNoKC90aGVtZS5sZXNzLykpIHtcbiAgICAgICAgICAgIGlmKGVycm9yLmxpbmUgPT0gNSkge1xuICAgICAgICAgICAgICBlbGVtZW50ICA9IHJlZ0V4cC52YXJpYWJsZS5leGVjKGVycm9yLm1lc3NhZ2UpWzFdO1xuICAgICAgICAgICAgICBpZihlbGVtZW50KSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignTWlzc2luZyB0aGVtZS5jb25maWcgdmFsdWUgZm9yICcsIGVsZW1lbnQpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ01vc3QgbGlrZWx5IG5ldyBVSSB3YXMgYWRkZWQgaW4gYW4gdXBkYXRlLiBZb3Ugd2lsbCBuZWVkIHRvIGFkZCBtaXNzaW5nIGVsZW1lbnRzIGZyb20gdGhlbWUuY29uZmlnLmV4YW1wbGUnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKGVycm9yLmxpbmUgPT0gNDYpIHtcbiAgICAgICAgICAgICAgZWxlbWVudCA9IHJlZ0V4cC5lbGVtZW50LmV4ZWMoZXJyb3IubWVzc2FnZSlbMV07XG4gICAgICAgICAgICAgIHRoZW1lICAgPSByZWdFeHAudGhlbWUuZXhlYyhlcnJvci5tZXNzYWdlKVsxXTtcbiAgICAgICAgICAgICAgY29uc29sZS5lcnJvcih0aGVtZSArICcgaXMgbm90IGFuIGF2YWlsYWJsZSB0aGVtZSBmb3IgJyArIGVsZW1lbnQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5lbWl0KCdlbmQnKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sXG5cbiAgICAvKiBXaGF0IEJyb3dzZXJzIHRvIFByZWZpeCAqL1xuICAgIHByZWZpeDoge1xuICAgICAgYnJvd3NlcnM6IFtcbiAgICAgICAgJ2xhc3QgMiB2ZXJzaW9ucycsXG4gICAgICAgICc+IDElJyxcbiAgICAgICAgJ29wZXJhIDEyLjEnLFxuICAgICAgICAnYmIgMTAnLFxuICAgICAgICAnYW5kcm9pZCA0J1xuICAgICAgXVxuICAgIH0sXG5cbiAgICAvKiBGaWxlIFJlbmFtZXMgKi9cbiAgICByZW5hbWU6IHtcbiAgICAgIG1pbkpTICAgICA6IHsgZXh0bmFtZSA6ICcubWluLmpzJyB9LFxuICAgICAgbWluQ1NTICAgIDogeyBleHRuYW1lIDogJy5taW4uY3NzJyB9LFxuICAgICAgcnRsQ1NTICAgIDogeyBleHRuYW1lIDogJy5ydGwuY3NzJyB9LFxuICAgICAgcnRsTWluQ1NTIDogeyBleHRuYW1lIDogJy5ydGwubWluLmNzcycgfVxuICAgIH0sXG5cbiAgICAvKiBNaW5pZmllZCBDU1MgQ29uY2F0ICovXG4gICAgbWluaWZ5OiB7XG4gICAgICBwcm9jZXNzSW1wb3J0ICAgICAgIDogZmFsc2UsXG4gICAgICByZXN0cnVjdHVyaW5nICAgICAgIDogZmFsc2UsXG4gICAgICBrZWVwU3BlY2lhbENvbW1lbnRzIDogMVxuICAgIH0sXG5cbiAgICAvKiBNaW5pZmllZCBKUyBTZXR0aW5ncyAqL1xuICAgIHVnbGlmeToge1xuICAgICAgbWFuZ2xlICAgICAgICAgICA6IHRydWUsXG4gICAgICBwcmVzZXJ2ZUNvbW1lbnRzIDogJ3NvbWUnXG4gICAgfSxcblxuICAgIC8qIE1pbmlmaWVkIENvbmNhdCBDU1MgU2V0dGluZ3MgKi9cbiAgICBjb25jYXRNaW5pZnk6IHtcbiAgICAgIHByb2Nlc3NJbXBvcnQgICAgICAgOiBmYWxzZSxcbiAgICAgIHJlc3RydWN0dXJpbmcgICAgICAgOiBmYWxzZSxcbiAgICAgIGtlZXBTcGVjaWFsQ29tbWVudHMgOiBmYWxzZVxuICAgIH0sXG5cbiAgICAvKiBNaW5pZmllZCBDb25jYXQgSlMgKi9cbiAgICBjb25jYXRVZ2xpZnk6IHtcbiAgICAgIG1hbmdsZSAgICAgICAgICAgOiB0cnVlLFxuICAgICAgcHJlc2VydmVDb21tZW50cyA6IGZhbHNlXG4gICAgfVxuXG4gIH1cbn07XG4iXX0=