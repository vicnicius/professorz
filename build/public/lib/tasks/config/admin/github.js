/*******************************
          GitHub Login
*******************************/
/*
  Logs into GitHub using OAuth
*/

var fs = require('fs'),
    path = require('path'),
    githubAPI = require('github'),


// stores oauth info for GitHub API
oAuthConfig = path.join(__dirname, 'oauth.js'),
    oAuth = fs.existsSync(oAuthConfig) ? require(oAuthConfig) : false,
    github;

if (!oAuth) {
  console.error('Must add oauth token for GitHub in tasks/config/admin/oauth.js');
}

github = new githubAPI({
  version: '3.0.0',
  debug: true,
  protocol: 'https',
  timeout: 5000
});

github.authenticate({
  type: 'oauth',
  token: oAuth.token
});

module.exports = github;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NvbmZpZy9hZG1pbi9naXRodWIuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQU9BLElBQ0UsS0FBYyxRQUFRLElBQVIsQ0FEaEI7QUFBQSxJQUVFLE9BQWMsUUFBUSxNQUFSLENBRmhCO0FBQUEsSUFHRSxZQUFjLFFBQVEsUUFBUixDQUhoQjtBQUFBOzs7QUFNRSxjQUFjLEtBQUssSUFBTCxDQUFVLFNBQVYsRUFBcUIsVUFBckIsQ0FOaEI7QUFBQSxJQU9FLFFBQWMsR0FBRyxVQUFILENBQWMsV0FBZCxJQUNWLFFBQVEsV0FBUixDQURVLEdBRVYsS0FUTjtBQUFBLElBVUUsTUFWRjs7QUFhQSxJQUFHLENBQUMsS0FBSixFQUFXO0FBQ1QsVUFBUSxLQUFSLENBQWMsZ0VBQWQ7QUFDRDs7QUFFRCxTQUFTLElBQUksU0FBSixDQUFjO0FBQ3JCLFdBQWEsT0FEUTtBQUVyQixTQUFhLElBRlE7QUFHckIsWUFBYSxPQUhRO0FBSXJCLFdBQWE7QUFKUSxDQUFkLENBQVQ7O0FBT0EsT0FBTyxZQUFQLENBQW9CO0FBQ2xCLFFBQU0sT0FEWTtBQUVsQixTQUFPLE1BQU07QUFGSyxDQUFwQjs7QUFLQSxPQUFPLE9BQVAsR0FBaUIsTUFBakIiLCJmaWxlIjoiZ2l0aHViLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAgICAgICBHaXRIdWIgTG9naW5cbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4vKlxuICBMb2dzIGludG8gR2l0SHViIHVzaW5nIE9BdXRoXG4qL1xuXG52YXJcbiAgZnMgICAgICAgICAgPSByZXF1aXJlKCdmcycpLFxuICBwYXRoICAgICAgICA9IHJlcXVpcmUoJ3BhdGgnKSxcbiAgZ2l0aHViQVBJICAgPSByZXF1aXJlKCdnaXRodWInKSxcblxuICAvLyBzdG9yZXMgb2F1dGggaW5mbyBmb3IgR2l0SHViIEFQSVxuICBvQXV0aENvbmZpZyA9IHBhdGguam9pbihfX2Rpcm5hbWUsICdvYXV0aC5qcycpLFxuICBvQXV0aCAgICAgICA9IGZzLmV4aXN0c1N5bmMob0F1dGhDb25maWcpXG4gICAgPyByZXF1aXJlKG9BdXRoQ29uZmlnKVxuICAgIDogZmFsc2UsXG4gIGdpdGh1YlxuO1xuXG5pZighb0F1dGgpIHtcbiAgY29uc29sZS5lcnJvcignTXVzdCBhZGQgb2F1dGggdG9rZW4gZm9yIEdpdEh1YiBpbiB0YXNrcy9jb25maWcvYWRtaW4vb2F1dGguanMnKTtcbn1cblxuZ2l0aHViID0gbmV3IGdpdGh1YkFQSSh7XG4gIHZlcnNpb24gICAgOiAnMy4wLjAnLFxuICBkZWJ1ZyAgICAgIDogdHJ1ZSxcbiAgcHJvdG9jb2wgICA6ICdodHRwcycsXG4gIHRpbWVvdXQgICAgOiA1MDAwXG59KTtcblxuZ2l0aHViLmF1dGhlbnRpY2F0ZSh7XG4gIHR5cGU6ICdvYXV0aCcsXG4gIHRva2VuOiBvQXV0aC50b2tlblxufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gZ2l0aHViO1xuIl19