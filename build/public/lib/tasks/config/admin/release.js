/*******************************
        Release Settings
*******************************/

// release settings
module.exports = {

  // path to components for repos
  source: './dist/components/',

  // modified asset paths for component repos
  paths: {
    source: '../themes/default/assets/',
    output: 'assets/'
  },

  templates: {
    bower: './tasks/config/admin/templates/bower.json',
    composer: './tasks/config/admin/templates/composer.json',
    package: './tasks/config/admin/templates/package.json',
    meteor: {
      css: './tasks/config/admin/templates/css-package.js',
      component: './tasks/config/admin/templates/component-package.js',
      less: './tasks/config/admin/templates/less-package.js'
    },
    readme: './tasks/config/admin/templates/README.md',
    notes: './RELEASE-NOTES.md'
  },

  org: 'Semantic-Org',
  repo: 'Semantic-UI',

  // files created for package managers
  files: {
    composer: 'composer.json',
    config: 'semantic.json',
    npm: 'package.json',
    meteor: 'package.js'
  },

  // root name for distribution repos
  distRepoRoot: 'Semantic-UI-',

  // root name for single component repos
  componentRepoRoot: 'UI-',

  // root name for package managers
  packageRoot: 'semantic-ui-',

  // root path to repos
  outputRoot: '../repos/',

  homepage: 'http://www.semantic-ui.com',

  // distributions that get separate repos
  distributions: ['LESS', 'CSS'],

  // components that get separate repositories for bower/npm
  components: ['accordion', 'ad', 'api', 'breadcrumb', 'button', 'card', 'checkbox', 'comment', 'container', 'dimmer', 'divider', 'dropdown', 'embed', 'feed', 'flag', 'form', 'grid', 'header', 'icon', 'image', 'input', 'item', 'label', 'list', 'loader', 'menu', 'message', 'modal', 'nag', 'popup', 'progress', 'rail', 'rating', 'reset', 'reveal', 'search', 'segment', 'shape', 'sidebar', 'site', 'statistic', 'step', 'sticky', 'tab', 'table', 'transition', 'visibility']
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NvbmZpZy9hZG1pbi9yZWxlYXNlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBS0EsT0FBTyxPQUFQLEdBQWlCOzs7QUFHZixVQUFhLG9CQUhFOzs7QUFNZixTQUFPO0FBQ0wsWUFBUywyQkFESjtBQUVMLFlBQVM7QUFGSixHQU5ROztBQVdmLGFBQVc7QUFDVCxXQUFXLDJDQURGO0FBRVQsY0FBVyw4Q0FGRjtBQUdULGFBQVcsNkNBSEY7QUFJVCxZQUFXO0FBQ1QsV0FBWSwrQ0FESDtBQUVULGlCQUFZLHFEQUZIO0FBR1QsWUFBWTtBQUhILEtBSkY7QUFTVCxZQUFTLDBDQVRBO0FBVVQsV0FBUztBQVZBLEdBWEk7O0FBd0JmLE9BQWMsY0F4QkM7QUF5QmYsUUFBYyxhQXpCQzs7O0FBNEJmLFNBQU87QUFDTCxjQUFXLGVBRE47QUFFTCxZQUFXLGVBRk47QUFHTCxTQUFXLGNBSE47QUFJTCxZQUFXO0FBSk4sR0E1QlE7OztBQW9DZixnQkFBb0IsY0FwQ0w7OztBQXVDZixxQkFBb0IsS0F2Q0w7OztBQTBDZixlQUF1QixjQTFDUjs7O0FBNkNmLGNBQWMsV0E3Q0M7O0FBK0NmLFlBQWMsNEJBL0NDOzs7QUFrRGYsaUJBQWUsQ0FDYixNQURhLEVBRWIsS0FGYSxDQWxEQTs7O0FBd0RmLGNBQWEsQ0FDWCxXQURXLEVBRVgsSUFGVyxFQUdYLEtBSFcsRUFJWCxZQUpXLEVBS1gsUUFMVyxFQU1YLE1BTlcsRUFPWCxVQVBXLEVBUVgsU0FSVyxFQVNYLFdBVFcsRUFVWCxRQVZXLEVBV1gsU0FYVyxFQVlYLFVBWlcsRUFhWCxPQWJXLEVBY1gsTUFkVyxFQWVYLE1BZlcsRUFnQlgsTUFoQlcsRUFpQlgsTUFqQlcsRUFrQlgsUUFsQlcsRUFtQlgsTUFuQlcsRUFvQlgsT0FwQlcsRUFxQlgsT0FyQlcsRUFzQlgsTUF0QlcsRUF1QlgsT0F2QlcsRUF3QlgsTUF4QlcsRUF5QlgsUUF6QlcsRUEwQlgsTUExQlcsRUEyQlgsU0EzQlcsRUE0QlgsT0E1QlcsRUE2QlgsS0E3QlcsRUE4QlgsT0E5QlcsRUErQlgsVUEvQlcsRUFnQ1gsTUFoQ1csRUFpQ1gsUUFqQ1csRUFrQ1gsT0FsQ1csRUFtQ1gsUUFuQ1csRUFvQ1gsUUFwQ1csRUFxQ1gsU0FyQ1csRUFzQ1gsT0F0Q1csRUF1Q1gsU0F2Q1csRUF3Q1gsTUF4Q1csRUF5Q1gsV0F6Q1csRUEwQ1gsTUExQ1csRUEyQ1gsUUEzQ1csRUE0Q1gsS0E1Q1csRUE2Q1gsT0E3Q1csRUE4Q1gsWUE5Q1csRUErQ1gsWUEvQ1c7QUF4REUsQ0FBakIiLCJmaWxlIjoicmVsZWFzZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgIFJlbGVhc2UgU2V0dGluZ3NcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi8vIHJlbGVhc2Ugc2V0dGluZ3Ncbm1vZHVsZS5leHBvcnRzID0ge1xuXG4gIC8vIHBhdGggdG8gY29tcG9uZW50cyBmb3IgcmVwb3NcbiAgc291cmNlICAgICA6ICcuL2Rpc3QvY29tcG9uZW50cy8nLFxuXG4gIC8vIG1vZGlmaWVkIGFzc2V0IHBhdGhzIGZvciBjb21wb25lbnQgcmVwb3NcbiAgcGF0aHM6IHtcbiAgICBzb3VyY2UgOiAnLi4vdGhlbWVzL2RlZmF1bHQvYXNzZXRzLycsXG4gICAgb3V0cHV0IDogJ2Fzc2V0cy8nXG4gIH0sXG5cbiAgdGVtcGxhdGVzOiB7XG4gICAgYm93ZXIgICAgOiAnLi90YXNrcy9jb25maWcvYWRtaW4vdGVtcGxhdGVzL2Jvd2VyLmpzb24nLFxuICAgIGNvbXBvc2VyIDogJy4vdGFza3MvY29uZmlnL2FkbWluL3RlbXBsYXRlcy9jb21wb3Nlci5qc29uJyxcbiAgICBwYWNrYWdlICA6ICcuL3Rhc2tzL2NvbmZpZy9hZG1pbi90ZW1wbGF0ZXMvcGFja2FnZS5qc29uJyxcbiAgICBtZXRlb3IgICA6IHtcbiAgICAgIGNzcyAgICAgICA6ICcuL3Rhc2tzL2NvbmZpZy9hZG1pbi90ZW1wbGF0ZXMvY3NzLXBhY2thZ2UuanMnLFxuICAgICAgY29tcG9uZW50IDogJy4vdGFza3MvY29uZmlnL2FkbWluL3RlbXBsYXRlcy9jb21wb25lbnQtcGFja2FnZS5qcycsXG4gICAgICBsZXNzICAgICAgOiAnLi90YXNrcy9jb25maWcvYWRtaW4vdGVtcGxhdGVzL2xlc3MtcGFja2FnZS5qcycsXG4gICAgfSxcbiAgICByZWFkbWUgOiAnLi90YXNrcy9jb25maWcvYWRtaW4vdGVtcGxhdGVzL1JFQURNRS5tZCcsXG4gICAgbm90ZXMgIDogJy4vUkVMRUFTRS1OT1RFUy5tZCdcbiAgfSxcblxuICBvcmcgICAgICAgICA6ICdTZW1hbnRpYy1PcmcnLFxuICByZXBvICAgICAgICA6ICdTZW1hbnRpYy1VSScsXG5cbiAgLy8gZmlsZXMgY3JlYXRlZCBmb3IgcGFja2FnZSBtYW5hZ2Vyc1xuICBmaWxlczoge1xuICAgIGNvbXBvc2VyIDogJ2NvbXBvc2VyLmpzb24nLFxuICAgIGNvbmZpZyAgIDogJ3NlbWFudGljLmpzb24nLFxuICAgIG5wbSAgICAgIDogJ3BhY2thZ2UuanNvbicsXG4gICAgbWV0ZW9yICAgOiAncGFja2FnZS5qcydcbiAgfSxcblxuICAvLyByb290IG5hbWUgZm9yIGRpc3RyaWJ1dGlvbiByZXBvc1xuICBkaXN0UmVwb1Jvb3QgICAgICA6ICdTZW1hbnRpYy1VSS0nLFxuXG4gIC8vIHJvb3QgbmFtZSBmb3Igc2luZ2xlIGNvbXBvbmVudCByZXBvc1xuICBjb21wb25lbnRSZXBvUm9vdCA6ICdVSS0nLFxuXG4gIC8vIHJvb3QgbmFtZSBmb3IgcGFja2FnZSBtYW5hZ2Vyc1xuICBwYWNrYWdlUm9vdCAgICAgICAgICA6ICdzZW1hbnRpYy11aS0nLFxuXG4gIC8vIHJvb3QgcGF0aCB0byByZXBvc1xuICBvdXRwdXRSb290ICA6ICcuLi9yZXBvcy8nLFxuXG4gIGhvbWVwYWdlICAgIDogJ2h0dHA6Ly93d3cuc2VtYW50aWMtdWkuY29tJyxcblxuICAvLyBkaXN0cmlidXRpb25zIHRoYXQgZ2V0IHNlcGFyYXRlIHJlcG9zXG4gIGRpc3RyaWJ1dGlvbnM6IFtcbiAgICAnTEVTUycsXG4gICAgJ0NTUydcbiAgXSxcblxuICAvLyBjb21wb25lbnRzIHRoYXQgZ2V0IHNlcGFyYXRlIHJlcG9zaXRvcmllcyBmb3IgYm93ZXIvbnBtXG4gIGNvbXBvbmVudHMgOiBbXG4gICAgJ2FjY29yZGlvbicsXG4gICAgJ2FkJyxcbiAgICAnYXBpJyxcbiAgICAnYnJlYWRjcnVtYicsXG4gICAgJ2J1dHRvbicsXG4gICAgJ2NhcmQnLFxuICAgICdjaGVja2JveCcsXG4gICAgJ2NvbW1lbnQnLFxuICAgICdjb250YWluZXInLFxuICAgICdkaW1tZXInLFxuICAgICdkaXZpZGVyJyxcbiAgICAnZHJvcGRvd24nLFxuICAgICdlbWJlZCcsXG4gICAgJ2ZlZWQnLFxuICAgICdmbGFnJyxcbiAgICAnZm9ybScsXG4gICAgJ2dyaWQnLFxuICAgICdoZWFkZXInLFxuICAgICdpY29uJyxcbiAgICAnaW1hZ2UnLFxuICAgICdpbnB1dCcsXG4gICAgJ2l0ZW0nLFxuICAgICdsYWJlbCcsXG4gICAgJ2xpc3QnLFxuICAgICdsb2FkZXInLFxuICAgICdtZW51JyxcbiAgICAnbWVzc2FnZScsXG4gICAgJ21vZGFsJyxcbiAgICAnbmFnJyxcbiAgICAncG9wdXAnLFxuICAgICdwcm9ncmVzcycsXG4gICAgJ3JhaWwnLFxuICAgICdyYXRpbmcnLFxuICAgICdyZXNldCcsXG4gICAgJ3JldmVhbCcsXG4gICAgJ3NlYXJjaCcsXG4gICAgJ3NlZ21lbnQnLFxuICAgICdzaGFwZScsXG4gICAgJ3NpZGViYXInLFxuICAgICdzaXRlJyxcbiAgICAnc3RhdGlzdGljJyxcbiAgICAnc3RlcCcsXG4gICAgJ3N0aWNreScsXG4gICAgJ3RhYicsXG4gICAgJ3RhYmxlJyxcbiAgICAndHJhbnNpdGlvbicsXG4gICAgJ3Zpc2liaWxpdHknXG4gIF1cbn07XG4iXX0=