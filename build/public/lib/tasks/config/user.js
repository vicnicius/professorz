/*******************************
             Set-up
*******************************/

var
// npm dependencies
extend = require('extend'),
    fs = require('fs'),
    path = require('path'),
    requireDotFile = require('require-dot-file'),


// semantic.json defaults
defaults = require('./defaults'),
    config = require('./project/config'),


// Final config object
gulpConfig = {},


// semantic.json settings
userConfig;

/*******************************
          User Config
*******************************/

try {
  // looks for config file across all parent directories
  userConfig = requireDotFile('semantic.json');
} catch (error) {
  if (error.code === 'MODULE_NOT_FOUND') {
    console.error('No semantic.json config found');
  }
}

// extend user config with defaults
gulpConfig = !userConfig ? extend(true, {}, defaults) : extend(false, {}, defaults, userConfig);

/*******************************
       Add Derived Values
*******************************/

// adds calculated values
config.addDerivedValues(gulpConfig);

/*******************************
             Export
*******************************/

module.exports = gulpConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NvbmZpZy91c2VyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFJQTs7QUFFRSxTQUFrQixRQUFRLFFBQVIsQ0FGcEI7QUFBQSxJQUdFLEtBQWtCLFFBQVEsSUFBUixDQUhwQjtBQUFBLElBSUUsT0FBa0IsUUFBUSxNQUFSLENBSnBCO0FBQUEsSUFLRSxpQkFBa0IsUUFBUSxrQkFBUixDQUxwQjtBQUFBOzs7QUFRRSxXQUFrQixRQUFRLFlBQVIsQ0FScEI7QUFBQSxJQVNFLFNBQWtCLFFBQVEsa0JBQVIsQ0FUcEI7QUFBQTs7O0FBWUUsYUFBYSxFQVpmO0FBQUE7OztBQWVFLFVBZkY7Ozs7OztBQXdCQSxJQUFJOztBQUVGLGVBQWEsZUFBZSxlQUFmLENBQWI7QUFDRCxDQUhELENBSUEsT0FBTSxLQUFOLEVBQWE7QUFDWCxNQUFHLE1BQU0sSUFBTixLQUFlLGtCQUFsQixFQUFzQztBQUNwQyxZQUFRLEtBQVIsQ0FBYywrQkFBZDtBQUNEO0FBQ0Y7OztBQUdELGFBQWMsQ0FBQyxVQUFGLEdBQ1QsT0FBTyxJQUFQLEVBQWEsRUFBYixFQUFpQixRQUFqQixDQURTLEdBRVQsT0FBTyxLQUFQLEVBQWMsRUFBZCxFQUFrQixRQUFsQixFQUE0QixVQUE1QixDQUZKOzs7Ozs7O0FBVUEsT0FBTyxnQkFBUCxDQUF3QixVQUF4Qjs7Ozs7O0FBT0EsT0FBTyxPQUFQLEdBQWlCLFVBQWpCIiwiZmlsZSI6InVzZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgICAgIFNldC11cFxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxudmFyXG4gIC8vIG5wbSBkZXBlbmRlbmNpZXNcbiAgZXh0ZW5kICAgICAgICAgID0gcmVxdWlyZSgnZXh0ZW5kJyksXG4gIGZzICAgICAgICAgICAgICA9IHJlcXVpcmUoJ2ZzJyksXG4gIHBhdGggICAgICAgICAgICA9IHJlcXVpcmUoJ3BhdGgnKSxcbiAgcmVxdWlyZURvdEZpbGUgID0gcmVxdWlyZSgncmVxdWlyZS1kb3QtZmlsZScpLFxuXG4gIC8vIHNlbWFudGljLmpzb24gZGVmYXVsdHNcbiAgZGVmYXVsdHMgICAgICAgID0gcmVxdWlyZSgnLi9kZWZhdWx0cycpLFxuICBjb25maWcgICAgICAgICAgPSByZXF1aXJlKCcuL3Byb2plY3QvY29uZmlnJyksXG5cbiAgLy8gRmluYWwgY29uZmlnIG9iamVjdFxuICBndWxwQ29uZmlnID0ge30sXG5cbiAgLy8gc2VtYW50aWMuanNvbiBzZXR0aW5nc1xuICB1c2VyQ29uZmlnXG5cbjtcblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgIFVzZXIgQ29uZmlnXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG50cnkge1xuICAvLyBsb29rcyBmb3IgY29uZmlnIGZpbGUgYWNyb3NzIGFsbCBwYXJlbnQgZGlyZWN0b3JpZXNcbiAgdXNlckNvbmZpZyA9IHJlcXVpcmVEb3RGaWxlKCdzZW1hbnRpYy5qc29uJyk7XG59XG5jYXRjaChlcnJvcikge1xuICBpZihlcnJvci5jb2RlID09PSAnTU9EVUxFX05PVF9GT1VORCcpIHtcbiAgICBjb25zb2xlLmVycm9yKCdObyBzZW1hbnRpYy5qc29uIGNvbmZpZyBmb3VuZCcpO1xuICB9XG59XG5cbi8vIGV4dGVuZCB1c2VyIGNvbmZpZyB3aXRoIGRlZmF1bHRzXG5ndWxwQ29uZmlnID0gKCF1c2VyQ29uZmlnKVxuICA/IGV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMpXG4gIDogZXh0ZW5kKGZhbHNlLCB7fSwgZGVmYXVsdHMsIHVzZXJDb25maWcpXG47XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgQWRkIERlcml2ZWQgVmFsdWVzXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4vLyBhZGRzIGNhbGN1bGF0ZWQgdmFsdWVzXG5jb25maWcuYWRkRGVyaXZlZFZhbHVlcyhndWxwQ29uZmlnKTtcblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgICAgIEV4cG9ydFxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxubW9kdWxlLmV4cG9ydHMgPSBndWxwQ29uZmlnO1xuXG4iXX0=