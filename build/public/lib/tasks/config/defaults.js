/*******************************
          Default Paths
*******************************/

module.exports = {

  // base path added to all other paths
  base: '',

  // base path when installed with npm
  pmRoot: 'semantic/',

  // octal permission for output files, i.e. 644 (false does not adjust)
  permission: 744,

  // whether to generate rtl files
  rtl: false,

  // file paths
  files: {
    config: 'semantic.json',
    site: 'src/site',
    theme: 'src/theme.config'
  },

  // folder paths
  paths: {
    source: {
      config: 'src/theme.config',
      definitions: 'src/definitions/',
      site: 'src/site/',
      themes: 'src/themes/'
    },
    output: {
      packaged: 'dist/',
      uncompressed: 'dist/components/',
      compressed: 'dist/components/',
      themes: 'dist/themes/'
    },
    clean: 'dist/'
  },

  // components to include in package
  components: [

  // global
  'reset', 'site',

  // elements
  'button', 'container', 'divider', 'flag', 'header', 'icon', 'image', 'input', 'label', 'list', 'loader', 'rail', 'reveal', 'segment', 'step',

  // collections
  'breadcrumb', 'form', 'grid', 'menu', 'message', 'table',

  // views
  'ad', 'card', 'comment', 'feed', 'item', 'statistic',

  // modules
  'accordion', 'checkbox', 'dimmer', 'dropdown', 'embed', 'modal', 'nag', 'popup', 'progress', 'rating', 'search', 'shape', 'sidebar', 'sticky', 'tab', 'transition',

  // behaviors
  'api', 'form', 'state', 'visibility'],

  // whether to load admin tasks
  admin: false,

  // globs used for matching file patterns
  globs: {
    ignored: '!(*.min|*.map|*.rtl)',
    ignoredRTL: '!(*.min|*.map)'
  }

};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NvbmZpZy9kZWZhdWx0cy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBSUEsT0FBTyxPQUFQLEdBQWlCOzs7QUFHZixRQUFPLEVBSFE7OztBQU1mLFVBQVEsV0FOTzs7O0FBU2YsY0FBYSxHQVRFOzs7QUFZZixPQUFhLEtBWkU7OztBQWVmLFNBQU87QUFDTCxZQUFXLGVBRE47QUFFTCxVQUFXLFVBRk47QUFHTCxXQUFXO0FBSE4sR0FmUTs7O0FBc0JmLFNBQU87QUFDTCxZQUFRO0FBQ04sY0FBYyxrQkFEUjtBQUVOLG1CQUFjLGtCQUZSO0FBR04sWUFBYyxXQUhSO0FBSU4sY0FBYztBQUpSLEtBREg7QUFPTCxZQUFRO0FBQ04sZ0JBQWUsT0FEVDtBQUVOLG9CQUFlLGtCQUZUO0FBR04sa0JBQWUsa0JBSFQ7QUFJTixjQUFlO0FBSlQsS0FQSDtBQWFMLFdBQVE7QUFiSCxHQXRCUTs7O0FBdUNmLGNBQVk7OztBQUdWLFNBSFUsRUFJVixNQUpVOzs7QUFPVixVQVBVLEVBUVYsV0FSVSxFQVNWLFNBVFUsRUFVVixNQVZVLEVBV1YsUUFYVSxFQVlWLE1BWlUsRUFhVixPQWJVLEVBY1YsT0FkVSxFQWVWLE9BZlUsRUFnQlYsTUFoQlUsRUFpQlYsUUFqQlUsRUFrQlYsTUFsQlUsRUFtQlYsUUFuQlUsRUFvQlYsU0FwQlUsRUFxQlYsTUFyQlU7OztBQXdCVixjQXhCVSxFQXlCVixNQXpCVSxFQTBCVixNQTFCVSxFQTJCVixNQTNCVSxFQTRCVixTQTVCVSxFQTZCVixPQTdCVTs7O0FBZ0NWLE1BaENVLEVBaUNWLE1BakNVLEVBa0NWLFNBbENVLEVBbUNWLE1BbkNVLEVBb0NWLE1BcENVLEVBcUNWLFdBckNVOzs7QUF3Q1YsYUF4Q1UsRUF5Q1YsVUF6Q1UsRUEwQ1YsUUExQ1UsRUEyQ1YsVUEzQ1UsRUE0Q1YsT0E1Q1UsRUE2Q1YsT0E3Q1UsRUE4Q1YsS0E5Q1UsRUErQ1YsT0EvQ1UsRUFnRFYsVUFoRFUsRUFpRFYsUUFqRFUsRUFrRFYsUUFsRFUsRUFtRFYsT0FuRFUsRUFvRFYsU0FwRFUsRUFxRFYsUUFyRFUsRUFzRFYsS0F0RFUsRUF1RFYsWUF2RFU7OztBQTBEVixPQTFEVSxFQTJEVixNQTNEVSxFQTREVixPQTVEVSxFQTZEVixZQTdEVSxDQXZDRzs7O0FBd0dmLFNBQU8sS0F4R1E7OztBQTJHZixTQUFhO0FBQ1gsYUFBYSxzQkFERjtBQUVYLGdCQUFhO0FBRkY7O0FBM0dFLENBQWpCIiwiZmlsZSI6ImRlZmF1bHRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAgICAgICBEZWZhdWx0IFBhdGhzXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IHtcblxuICAvLyBiYXNlIHBhdGggYWRkZWQgdG8gYWxsIG90aGVyIHBhdGhzXG4gIGJhc2UgOiAnJyxcblxuICAvLyBiYXNlIHBhdGggd2hlbiBpbnN0YWxsZWQgd2l0aCBucG1cbiAgcG1Sb290OiAnc2VtYW50aWMvJyxcblxuICAvLyBvY3RhbCBwZXJtaXNzaW9uIGZvciBvdXRwdXQgZmlsZXMsIGkuZS4gNjQ0IChmYWxzZSBkb2VzIG5vdCBhZGp1c3QpXG4gIHBlcm1pc3Npb24gOiA3NDQsXG5cbiAgLy8gd2hldGhlciB0byBnZW5lcmF0ZSBydGwgZmlsZXNcbiAgcnRsICAgICAgICA6IGZhbHNlLFxuXG4gIC8vIGZpbGUgcGF0aHNcbiAgZmlsZXM6IHtcbiAgICBjb25maWcgICA6ICdzZW1hbnRpYy5qc29uJyxcbiAgICBzaXRlICAgICA6ICdzcmMvc2l0ZScsXG4gICAgdGhlbWUgICAgOiAnc3JjL3RoZW1lLmNvbmZpZydcbiAgfSxcblxuICAvLyBmb2xkZXIgcGF0aHNcbiAgcGF0aHM6IHtcbiAgICBzb3VyY2U6IHtcbiAgICAgIGNvbmZpZyAgICAgIDogJ3NyYy90aGVtZS5jb25maWcnLFxuICAgICAgZGVmaW5pdGlvbnMgOiAnc3JjL2RlZmluaXRpb25zLycsXG4gICAgICBzaXRlICAgICAgICA6ICdzcmMvc2l0ZS8nLFxuICAgICAgdGhlbWVzICAgICAgOiAnc3JjL3RoZW1lcy8nXG4gICAgfSxcbiAgICBvdXRwdXQ6IHtcbiAgICAgIHBhY2thZ2VkICAgICA6ICdkaXN0LycsXG4gICAgICB1bmNvbXByZXNzZWQgOiAnZGlzdC9jb21wb25lbnRzLycsXG4gICAgICBjb21wcmVzc2VkICAgOiAnZGlzdC9jb21wb25lbnRzLycsXG4gICAgICB0aGVtZXMgICAgICAgOiAnZGlzdC90aGVtZXMvJ1xuICAgIH0sXG4gICAgY2xlYW4gOiAnZGlzdC8nXG4gIH0sXG5cbiAgLy8gY29tcG9uZW50cyB0byBpbmNsdWRlIGluIHBhY2thZ2VcbiAgY29tcG9uZW50czogW1xuXG4gICAgLy8gZ2xvYmFsXG4gICAgJ3Jlc2V0JyxcbiAgICAnc2l0ZScsXG5cbiAgICAvLyBlbGVtZW50c1xuICAgICdidXR0b24nLFxuICAgICdjb250YWluZXInLFxuICAgICdkaXZpZGVyJyxcbiAgICAnZmxhZycsXG4gICAgJ2hlYWRlcicsXG4gICAgJ2ljb24nLFxuICAgICdpbWFnZScsXG4gICAgJ2lucHV0JyxcbiAgICAnbGFiZWwnLFxuICAgICdsaXN0JyxcbiAgICAnbG9hZGVyJyxcbiAgICAncmFpbCcsXG4gICAgJ3JldmVhbCcsXG4gICAgJ3NlZ21lbnQnLFxuICAgICdzdGVwJyxcblxuICAgIC8vIGNvbGxlY3Rpb25zXG4gICAgJ2JyZWFkY3J1bWInLFxuICAgICdmb3JtJyxcbiAgICAnZ3JpZCcsXG4gICAgJ21lbnUnLFxuICAgICdtZXNzYWdlJyxcbiAgICAndGFibGUnLFxuXG4gICAgLy8gdmlld3NcbiAgICAnYWQnLFxuICAgICdjYXJkJyxcbiAgICAnY29tbWVudCcsXG4gICAgJ2ZlZWQnLFxuICAgICdpdGVtJyxcbiAgICAnc3RhdGlzdGljJyxcblxuICAgIC8vIG1vZHVsZXNcbiAgICAnYWNjb3JkaW9uJyxcbiAgICAnY2hlY2tib3gnLFxuICAgICdkaW1tZXInLFxuICAgICdkcm9wZG93bicsXG4gICAgJ2VtYmVkJyxcbiAgICAnbW9kYWwnLFxuICAgICduYWcnLFxuICAgICdwb3B1cCcsXG4gICAgJ3Byb2dyZXNzJyxcbiAgICAncmF0aW5nJyxcbiAgICAnc2VhcmNoJyxcbiAgICAnc2hhcGUnLFxuICAgICdzaWRlYmFyJyxcbiAgICAnc3RpY2t5JyxcbiAgICAndGFiJyxcbiAgICAndHJhbnNpdGlvbicsXG5cbiAgICAvLyBiZWhhdmlvcnNcbiAgICAnYXBpJyxcbiAgICAnZm9ybScsXG4gICAgJ3N0YXRlJyxcbiAgICAndmlzaWJpbGl0eSdcbiAgXSxcblxuICAvLyB3aGV0aGVyIHRvIGxvYWQgYWRtaW4gdGFza3NcbiAgYWRtaW46IGZhbHNlLFxuXG4gIC8vIGdsb2JzIHVzZWQgZm9yIG1hdGNoaW5nIGZpbGUgcGF0dGVybnNcbiAgZ2xvYnMgICAgICA6IHtcbiAgICBpZ25vcmVkICAgIDogJyEoKi5taW58Ki5tYXB8Ki5ydGwpJyxcbiAgICBpZ25vcmVkUlRMIDogJyEoKi5taW58Ki5tYXApJ1xuICB9XG5cbn07XG4iXX0=