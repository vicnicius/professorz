/*******************************
            Set-up
*******************************/

var gulp = require('gulp-help')(require('gulp')),


// read user config to know what task to load
config = require('./tasks/config/user'),


// watch changes
watch = require('./tasks/watch'),


// build all files
build = require('./tasks/build'),
    buildJS = require('./tasks/build/javascript'),
    buildCSS = require('./tasks/build/css'),
    buildAssets = require('./tasks/build/assets'),


// utility
clean = require('./tasks/clean'),
    version = require('./tasks/version'),


// docs tasks
serveDocs = require('./tasks/docs/serve'),
    buildDocs = require('./tasks/docs/build'),


// rtl
buildRTL = require('./tasks/rtl/build'),
    watchRTL = require('./tasks/rtl/watch');

/*******************************
             Tasks
*******************************/

gulp.task('default', false, ['watch']);

gulp.task('watch', 'Watch for site/theme changes', watch);

gulp.task('build', 'Builds all files from source', build);
gulp.task('build-javascript', 'Builds all javascript from source', buildJS);
gulp.task('build-css', 'Builds all css from source', buildCSS);
gulp.task('build-assets', 'Copies all assets from source', buildAssets);

gulp.task('clean', 'Clean dist folder', clean);
gulp.task('version', 'Displays current version of Semantic', version);

/*--------------
      Docs
---------------*/

/*
  Lets you serve files to a local documentation instance
  https://github.com/Semantic-Org/Semantic-UI-Docs/
*/

gulp.task('serve-docs', 'Serve file changes to SUI Docs', serveDocs);
gulp.task('build-docs', 'Build all files and add to SUI Docs', buildDocs);

/*--------------
      RTL
---------------*/

if (config.rtl) {
  gulp.task('watch-rtl', 'Watch files as RTL', watchRTL);
  gulp.task('build-rtl', 'Build all files as RTL', buildRTL);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NvbmZpZy9ucG0vZ3VscGZpbGUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUlBLElBQ0UsT0FBZSxRQUFRLFdBQVIsRUFBcUIsUUFBUSxNQUFSLENBQXJCLENBRGpCO0FBQUE7OztBQUlFLFNBQWUsUUFBUSxxQkFBUixDQUpqQjtBQUFBOzs7QUFPRSxRQUFlLFFBQVEsZUFBUixDQVBqQjtBQUFBOzs7QUFVRSxRQUFlLFFBQVEsZUFBUixDQVZqQjtBQUFBLElBV0UsVUFBZSxRQUFRLDBCQUFSLENBWGpCO0FBQUEsSUFZRSxXQUFlLFFBQVEsbUJBQVIsQ0FaakI7QUFBQSxJQWFFLGNBQWUsUUFBUSxzQkFBUixDQWJqQjtBQUFBOzs7QUFnQkUsUUFBZSxRQUFRLGVBQVIsQ0FoQmpCO0FBQUEsSUFpQkUsVUFBZSxRQUFRLGlCQUFSLENBakJqQjtBQUFBOzs7QUFvQkUsWUFBZSxRQUFRLG9CQUFSLENBcEJqQjtBQUFBLElBcUJFLFlBQWUsUUFBUSxvQkFBUixDQXJCakI7QUFBQTs7O0FBd0JFLFdBQWUsUUFBUSxtQkFBUixDQXhCakI7QUFBQSxJQXlCRSxXQUFlLFFBQVEsbUJBQVIsQ0F6QmpCOzs7Ozs7QUFpQ0EsS0FBSyxJQUFMLENBQVUsU0FBVixFQUFxQixLQUFyQixFQUE0QixDQUMxQixPQUQwQixDQUE1Qjs7QUFJQSxLQUFLLElBQUwsQ0FBVSxPQUFWLEVBQW1CLDhCQUFuQixFQUFtRCxLQUFuRDs7QUFFQSxLQUFLLElBQUwsQ0FBVSxPQUFWLEVBQW1CLDhCQUFuQixFQUFtRCxLQUFuRDtBQUNBLEtBQUssSUFBTCxDQUFVLGtCQUFWLEVBQThCLG1DQUE5QixFQUFtRSxPQUFuRTtBQUNBLEtBQUssSUFBTCxDQUFVLFdBQVYsRUFBdUIsNEJBQXZCLEVBQXFELFFBQXJEO0FBQ0EsS0FBSyxJQUFMLENBQVUsY0FBVixFQUEwQiwrQkFBMUIsRUFBMkQsV0FBM0Q7O0FBRUEsS0FBSyxJQUFMLENBQVUsT0FBVixFQUFtQixtQkFBbkIsRUFBd0MsS0FBeEM7QUFDQSxLQUFLLElBQUwsQ0FBVSxTQUFWLEVBQXFCLHNDQUFyQixFQUE2RCxPQUE3RDs7Ozs7Ozs7Ozs7QUFXQSxLQUFLLElBQUwsQ0FBVSxZQUFWLEVBQXdCLGdDQUF4QixFQUEwRCxTQUExRDtBQUNBLEtBQUssSUFBTCxDQUFVLFlBQVYsRUFBd0IscUNBQXhCLEVBQStELFNBQS9EOzs7Ozs7QUFPQSxJQUFHLE9BQU8sR0FBVixFQUFlO0FBQ2IsT0FBSyxJQUFMLENBQVUsV0FBVixFQUF1QixvQkFBdkIsRUFBNkMsUUFBN0M7QUFDQSxPQUFLLElBQUwsQ0FBVSxXQUFWLEVBQXVCLHdCQUF2QixFQUFpRCxRQUFqRDtBQUNEIiwiZmlsZSI6Imd1bHBmaWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAgICAgICAgIFNldC11cFxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxudmFyXG4gIGd1bHAgICAgICAgICA9IHJlcXVpcmUoJ2d1bHAtaGVscCcpKHJlcXVpcmUoJ2d1bHAnKSksXG5cbiAgLy8gcmVhZCB1c2VyIGNvbmZpZyB0byBrbm93IHdoYXQgdGFzayB0byBsb2FkXG4gIGNvbmZpZyAgICAgICA9IHJlcXVpcmUoJy4vdGFza3MvY29uZmlnL3VzZXInKSxcblxuICAvLyB3YXRjaCBjaGFuZ2VzXG4gIHdhdGNoICAgICAgICA9IHJlcXVpcmUoJy4vdGFza3Mvd2F0Y2gnKSxcblxuICAvLyBidWlsZCBhbGwgZmlsZXNcbiAgYnVpbGQgICAgICAgID0gcmVxdWlyZSgnLi90YXNrcy9idWlsZCcpLFxuICBidWlsZEpTICAgICAgPSByZXF1aXJlKCcuL3Rhc2tzL2J1aWxkL2phdmFzY3JpcHQnKSxcbiAgYnVpbGRDU1MgICAgID0gcmVxdWlyZSgnLi90YXNrcy9idWlsZC9jc3MnKSxcbiAgYnVpbGRBc3NldHMgID0gcmVxdWlyZSgnLi90YXNrcy9idWlsZC9hc3NldHMnKSxcblxuICAvLyB1dGlsaXR5XG4gIGNsZWFuICAgICAgICA9IHJlcXVpcmUoJy4vdGFza3MvY2xlYW4nKSxcbiAgdmVyc2lvbiAgICAgID0gcmVxdWlyZSgnLi90YXNrcy92ZXJzaW9uJyksXG5cbiAgLy8gZG9jcyB0YXNrc1xuICBzZXJ2ZURvY3MgICAgPSByZXF1aXJlKCcuL3Rhc2tzL2RvY3Mvc2VydmUnKSxcbiAgYnVpbGREb2NzICAgID0gcmVxdWlyZSgnLi90YXNrcy9kb2NzL2J1aWxkJyksXG5cbiAgLy8gcnRsXG4gIGJ1aWxkUlRMICAgICA9IHJlcXVpcmUoJy4vdGFza3MvcnRsL2J1aWxkJyksXG4gIHdhdGNoUlRMICAgICA9IHJlcXVpcmUoJy4vdGFza3MvcnRsL3dhdGNoJylcbjtcblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgICAgIFRhc2tzXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG5ndWxwLnRhc2soJ2RlZmF1bHQnLCBmYWxzZSwgW1xuICAnd2F0Y2gnXG5dKTtcblxuZ3VscC50YXNrKCd3YXRjaCcsICdXYXRjaCBmb3Igc2l0ZS90aGVtZSBjaGFuZ2VzJywgd2F0Y2gpO1xuXG5ndWxwLnRhc2soJ2J1aWxkJywgJ0J1aWxkcyBhbGwgZmlsZXMgZnJvbSBzb3VyY2UnLCBidWlsZCk7XG5ndWxwLnRhc2soJ2J1aWxkLWphdmFzY3JpcHQnLCAnQnVpbGRzIGFsbCBqYXZhc2NyaXB0IGZyb20gc291cmNlJywgYnVpbGRKUyk7XG5ndWxwLnRhc2soJ2J1aWxkLWNzcycsICdCdWlsZHMgYWxsIGNzcyBmcm9tIHNvdXJjZScsIGJ1aWxkQ1NTKTtcbmd1bHAudGFzaygnYnVpbGQtYXNzZXRzJywgJ0NvcGllcyBhbGwgYXNzZXRzIGZyb20gc291cmNlJywgYnVpbGRBc3NldHMpO1xuXG5ndWxwLnRhc2soJ2NsZWFuJywgJ0NsZWFuIGRpc3QgZm9sZGVyJywgY2xlYW4pO1xuZ3VscC50YXNrKCd2ZXJzaW9uJywgJ0Rpc3BsYXlzIGN1cnJlbnQgdmVyc2lvbiBvZiBTZW1hbnRpYycsIHZlcnNpb24pO1xuXG4vKi0tLS0tLS0tLS0tLS0tXG4gICAgICBEb2NzXG4tLS0tLS0tLS0tLS0tLS0qL1xuXG4vKlxuICBMZXRzIHlvdSBzZXJ2ZSBmaWxlcyB0byBhIGxvY2FsIGRvY3VtZW50YXRpb24gaW5zdGFuY2VcbiAgaHR0cHM6Ly9naXRodWIuY29tL1NlbWFudGljLU9yZy9TZW1hbnRpYy1VSS1Eb2NzL1xuKi9cblxuZ3VscC50YXNrKCdzZXJ2ZS1kb2NzJywgJ1NlcnZlIGZpbGUgY2hhbmdlcyB0byBTVUkgRG9jcycsIHNlcnZlRG9jcyk7XG5ndWxwLnRhc2soJ2J1aWxkLWRvY3MnLCAnQnVpbGQgYWxsIGZpbGVzIGFuZCBhZGQgdG8gU1VJIERvY3MnLCBidWlsZERvY3MpO1xuXG5cbi8qLS0tLS0tLS0tLS0tLS1cbiAgICAgIFJUTFxuLS0tLS0tLS0tLS0tLS0tKi9cblxuaWYoY29uZmlnLnJ0bCkge1xuICBndWxwLnRhc2soJ3dhdGNoLXJ0bCcsICdXYXRjaCBmaWxlcyBhcyBSVEwnLCB3YXRjaFJUTCk7XG4gIGd1bHAudGFzaygnYnVpbGQtcnRsJywgJ0J1aWxkIGFsbCBmaWxlcyBhcyBSVEwnLCBidWlsZFJUTCk7XG59XG4iXX0=