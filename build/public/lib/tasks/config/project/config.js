/*******************************
            Set-up
*******************************/

var extend = require('extend'),
    fs = require('fs'),
    path = require('path'),
    defaults = require('../defaults');

/*******************************
            Exports
*******************************/

module.exports = {

  getPath: function (file, directory) {
    var configPath,
        walk = function (directory) {
      var nextDirectory = path.resolve(path.join(directory, path.sep, '..')),
          currentPath = path.normalize(path.join(directory, file));
      if (fs.existsSync(currentPath)) {
        // found file
        configPath = path.normalize(directory);
        return;
      } else {
        // reached file system root, let's stop
        if (nextDirectory == directory) {
          return;
        }
        // otherwise recurse
        walk(nextDirectory, file);
      }
    };

    // start walk from outside require-dot-files directory
    file = file || defaults.files.config;
    directory = directory || path.join(__dirname, path.sep, '..');
    walk(directory);
    return configPath || '';
  },

  // adds additional derived values to a config object
  addDerivedValues: function (config) {

    config = config || extend(false, {}, defaults);

    /*--------------
       File Paths
    ---------------*/

    var configPath = this.getPath(),
        sourcePaths = {},
        outputPaths = {},
        folder;

    // resolve paths (config location + base + path)
    for (folder in config.paths.source) {
      if (config.paths.source.hasOwnProperty(folder)) {
        sourcePaths[folder] = path.resolve(path.join(configPath, config.base, config.paths.source[folder]));
      }
    }
    for (folder in config.paths.output) {
      if (config.paths.output.hasOwnProperty(folder)) {
        outputPaths[folder] = path.resolve(path.join(configPath, config.base, config.paths.output[folder]));
      }
    }

    // set config paths to full paths
    config.paths.source = sourcePaths;
    config.paths.output = outputPaths;

    // resolve "clean" command path
    config.paths.clean = path.resolve(path.join(configPath, config.base, config.paths.clean));

    /*--------------
        CSS URLs
    ---------------*/

    // determine asset paths in css by finding relative path between themes and output
    // force forward slashes

    config.paths.assets = {
      source: '../../themes', // source asset path is always the same
      uncompressed: '.' + path.sep + path.relative(config.paths.output.uncompressed, config.paths.output.themes).replace(/\\/g, '/'),
      compressed: '.' + path.sep + path.relative(config.paths.output.compressed, config.paths.output.themes).replace(/\\/g, '/'),
      packaged: '.' + path.sep + path.relative(config.paths.output.packaged, config.paths.output.themes).replace(/\\/g, '/')
    };

    /*--------------
       Permission
    ---------------*/

    if (config.permission) {
      config.hasPermissions = true;
    } else {
      // pass blank object to avoid causing errors
      config.permission = {};
      config.hasPermissions = false;
    }

    /*--------------
         Globs
    ---------------*/

    if (!config.globs) {
      config.globs = {};
    }

    // remove duplicates from component array
    if (config.components instanceof Array) {
      config.components = config.components.filter(function (component, index) {
        return config.components.indexOf(component) == index;
      });
    }

    // takes component object and creates file glob matching selected components
    config.globs.components = typeof config.components == 'object' ? config.components.length > 1 ? '{' + config.components.join(',') + '}' : config.components[0] : '{' + defaults.components.join(',') + '}';

    return config;
  }

};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NvbmZpZy9wcm9qZWN0L2NvbmZpZy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBSUEsSUFDRSxTQUFXLFFBQVEsUUFBUixDQURiO0FBQUEsSUFFRSxLQUFXLFFBQVEsSUFBUixDQUZiO0FBQUEsSUFHRSxPQUFXLFFBQVEsTUFBUixDQUhiO0FBQUEsSUFLRSxXQUFXLFFBQVEsYUFBUixDQUxiOzs7Ozs7QUFhQSxPQUFPLE9BQVAsR0FBaUI7O0FBRWYsV0FBUyxVQUFTLElBQVQsRUFBZSxTQUFmLEVBQTBCO0FBQ2pDLFFBQ0UsVUFERjtBQUFBLFFBRUUsT0FBTyxVQUFTLFNBQVQsRUFBb0I7QUFDekIsVUFDRSxnQkFBZ0IsS0FBSyxPQUFMLENBQWMsS0FBSyxJQUFMLENBQVUsU0FBVixFQUFxQixLQUFLLEdBQTFCLEVBQStCLElBQS9CLENBQWQsQ0FEbEI7QUFBQSxVQUVFLGNBQWdCLEtBQUssU0FBTCxDQUFnQixLQUFLLElBQUwsQ0FBVSxTQUFWLEVBQXFCLElBQXJCLENBQWhCLENBRmxCO0FBSUEsVUFBSSxHQUFHLFVBQUgsQ0FBYyxXQUFkLENBQUosRUFBaUM7O0FBRS9CLHFCQUFhLEtBQUssU0FBTCxDQUFlLFNBQWYsQ0FBYjtBQUNBO0FBQ0QsT0FKRCxNQUtLOztBQUVILFlBQUcsaUJBQWlCLFNBQXBCLEVBQStCO0FBQzdCO0FBQ0Q7O0FBRUQsYUFBSyxhQUFMLEVBQW9CLElBQXBCO0FBQ0Q7QUFDRixLQXBCSDs7O0FBd0JBLFdBQVksUUFBUSxTQUFTLEtBQVQsQ0FBZSxNQUFuQztBQUNBLGdCQUFZLGFBQWEsS0FBSyxJQUFMLENBQVUsU0FBVixFQUFxQixLQUFLLEdBQTFCLEVBQStCLElBQS9CLENBQXpCO0FBQ0EsU0FBSyxTQUFMO0FBQ0EsV0FBTyxjQUFjLEVBQXJCO0FBQ0QsR0EvQmM7OztBQWtDZixvQkFBa0IsVUFBUyxNQUFULEVBQWlCOztBQUVqQyxhQUFTLFVBQVUsT0FBTyxLQUFQLEVBQWMsRUFBZCxFQUFrQixRQUFsQixDQUFuQjs7Ozs7O0FBTUEsUUFDRSxhQUFhLEtBQUssT0FBTCxFQURmO0FBQUEsUUFFRSxjQUFjLEVBRmhCO0FBQUEsUUFHRSxjQUFjLEVBSGhCO0FBQUEsUUFJRSxNQUpGOzs7QUFRQSxTQUFJLE1BQUosSUFBYyxPQUFPLEtBQVAsQ0FBYSxNQUEzQixFQUFtQztBQUNqQyxVQUFHLE9BQU8sS0FBUCxDQUFhLE1BQWIsQ0FBb0IsY0FBcEIsQ0FBbUMsTUFBbkMsQ0FBSCxFQUErQztBQUM3QyxvQkFBWSxNQUFaLElBQXNCLEtBQUssT0FBTCxDQUFhLEtBQUssSUFBTCxDQUFVLFVBQVYsRUFBc0IsT0FBTyxJQUE3QixFQUFtQyxPQUFPLEtBQVAsQ0FBYSxNQUFiLENBQW9CLE1BQXBCLENBQW5DLENBQWIsQ0FBdEI7QUFDRDtBQUNGO0FBQ0QsU0FBSSxNQUFKLElBQWMsT0FBTyxLQUFQLENBQWEsTUFBM0IsRUFBbUM7QUFDakMsVUFBRyxPQUFPLEtBQVAsQ0FBYSxNQUFiLENBQW9CLGNBQXBCLENBQW1DLE1BQW5DLENBQUgsRUFBK0M7QUFDN0Msb0JBQVksTUFBWixJQUFzQixLQUFLLE9BQUwsQ0FBYSxLQUFLLElBQUwsQ0FBVSxVQUFWLEVBQXNCLE9BQU8sSUFBN0IsRUFBbUMsT0FBTyxLQUFQLENBQWEsTUFBYixDQUFvQixNQUFwQixDQUFuQyxDQUFiLENBQXRCO0FBQ0Q7QUFDRjs7O0FBR0QsV0FBTyxLQUFQLENBQWEsTUFBYixHQUFzQixXQUF0QjtBQUNBLFdBQU8sS0FBUCxDQUFhLE1BQWIsR0FBc0IsV0FBdEI7OztBQUdBLFdBQU8sS0FBUCxDQUFhLEtBQWIsR0FBcUIsS0FBSyxPQUFMLENBQWMsS0FBSyxJQUFMLENBQVUsVUFBVixFQUFzQixPQUFPLElBQTdCLEVBQW1DLE9BQU8sS0FBUCxDQUFhLEtBQWhELENBQWQsQ0FBckI7Ozs7Ozs7OztBQVNBLFdBQU8sS0FBUCxDQUFhLE1BQWIsR0FBc0I7QUFDcEIsY0FBZSxjQURLLEU7QUFFcEIsb0JBQWUsTUFBTSxLQUFLLEdBQVgsR0FBaUIsS0FBSyxRQUFMLENBQWMsT0FBTyxLQUFQLENBQWEsTUFBYixDQUFvQixZQUFsQyxFQUFnRCxPQUFPLEtBQVAsQ0FBYSxNQUFiLENBQW9CLE1BQXBFLEVBQTRFLE9BQTVFLENBQW9GLEtBQXBGLEVBQTJGLEdBQTNGLENBRlo7QUFHcEIsa0JBQWUsTUFBTSxLQUFLLEdBQVgsR0FBaUIsS0FBSyxRQUFMLENBQWMsT0FBTyxLQUFQLENBQWEsTUFBYixDQUFvQixVQUFsQyxFQUE4QyxPQUFPLEtBQVAsQ0FBYSxNQUFiLENBQW9CLE1BQWxFLEVBQTBFLE9BQTFFLENBQWtGLEtBQWxGLEVBQXlGLEdBQXpGLENBSFo7QUFJcEIsZ0JBQWUsTUFBTSxLQUFLLEdBQVgsR0FBaUIsS0FBSyxRQUFMLENBQWMsT0FBTyxLQUFQLENBQWEsTUFBYixDQUFvQixRQUFsQyxFQUE0QyxPQUFPLEtBQVAsQ0FBYSxNQUFiLENBQW9CLE1BQWhFLEVBQXdFLE9BQXhFLENBQWdGLEtBQWhGLEVBQXVGLEdBQXZGO0FBSlosS0FBdEI7Ozs7OztBQVdBLFFBQUcsT0FBTyxVQUFWLEVBQXNCO0FBQ3BCLGFBQU8sY0FBUCxHQUF3QixJQUF4QjtBQUNELEtBRkQsTUFHSzs7QUFFSCxhQUFPLFVBQVAsR0FBd0IsRUFBeEI7QUFDQSxhQUFPLGNBQVAsR0FBd0IsS0FBeEI7QUFDRDs7Ozs7O0FBTUQsUUFBRyxDQUFDLE9BQU8sS0FBWCxFQUFrQjtBQUNoQixhQUFPLEtBQVAsR0FBZSxFQUFmO0FBQ0Q7OztBQUdELFFBQUcsT0FBTyxVQUFQLFlBQTZCLEtBQWhDLEVBQXVDO0FBQ3JDLGFBQU8sVUFBUCxHQUFvQixPQUFPLFVBQVAsQ0FBa0IsTUFBbEIsQ0FBeUIsVUFBUyxTQUFULEVBQW9CLEtBQXBCLEVBQTJCO0FBQ3RFLGVBQU8sT0FBTyxVQUFQLENBQWtCLE9BQWxCLENBQTBCLFNBQTFCLEtBQXdDLEtBQS9DO0FBQ0QsT0FGbUIsQ0FBcEI7QUFHRDs7O0FBR0QsV0FBTyxLQUFQLENBQWEsVUFBYixHQUEyQixPQUFPLE9BQU8sVUFBZCxJQUE0QixRQUE3QixHQUNyQixPQUFPLFVBQVAsQ0FBa0IsTUFBbEIsR0FBMkIsQ0FBNUIsR0FDRSxNQUFNLE9BQU8sVUFBUCxDQUFrQixJQUFsQixDQUF1QixHQUF2QixDQUFOLEdBQW9DLEdBRHRDLEdBRUUsT0FBTyxVQUFQLENBQWtCLENBQWxCLENBSG9CLEdBSXRCLE1BQU0sU0FBUyxVQUFULENBQW9CLElBQXBCLENBQXlCLEdBQXpCLENBQU4sR0FBc0MsR0FKMUM7O0FBT0EsV0FBTyxNQUFQO0FBRUQ7O0FBeEhjLENBQWpCIiwiZmlsZSI6ImNvbmZpZy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICAgICBTZXQtdXBcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbnZhclxuICBleHRlbmQgICA9IHJlcXVpcmUoJ2V4dGVuZCcpLFxuICBmcyAgICAgICA9IHJlcXVpcmUoJ2ZzJyksXG4gIHBhdGggICAgID0gcmVxdWlyZSgncGF0aCcpLFxuXG4gIGRlZmF1bHRzID0gcmVxdWlyZSgnLi4vZGVmYXVsdHMnKVxuO1xuXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICAgICBFeHBvcnRzXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IHtcblxuICBnZXRQYXRoOiBmdW5jdGlvbihmaWxlLCBkaXJlY3RvcnkpIHtcbiAgICB2YXJcbiAgICAgIGNvbmZpZ1BhdGgsXG4gICAgICB3YWxrID0gZnVuY3Rpb24oZGlyZWN0b3J5KSB7XG4gICAgICAgIHZhclxuICAgICAgICAgIG5leHREaXJlY3RvcnkgPSBwYXRoLnJlc29sdmUoIHBhdGguam9pbihkaXJlY3RvcnksIHBhdGguc2VwLCAnLi4nKSApLFxuICAgICAgICAgIGN1cnJlbnRQYXRoICAgPSBwYXRoLm5vcm1hbGl6ZSggcGF0aC5qb2luKGRpcmVjdG9yeSwgZmlsZSkgKVxuICAgICAgICA7XG4gICAgICAgIGlmKCBmcy5leGlzdHNTeW5jKGN1cnJlbnRQYXRoKSApIHtcbiAgICAgICAgICAvLyBmb3VuZCBmaWxlXG4gICAgICAgICAgY29uZmlnUGF0aCA9IHBhdGgubm9ybWFsaXplKGRpcmVjdG9yeSk7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIC8vIHJlYWNoZWQgZmlsZSBzeXN0ZW0gcm9vdCwgbGV0J3Mgc3RvcFxuICAgICAgICAgIGlmKG5leHREaXJlY3RvcnkgPT0gZGlyZWN0b3J5KSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICAgIC8vIG90aGVyd2lzZSByZWN1cnNlXG4gICAgICAgICAgd2FsayhuZXh0RGlyZWN0b3J5LCBmaWxlKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIDtcblxuICAgIC8vIHN0YXJ0IHdhbGsgZnJvbSBvdXRzaWRlIHJlcXVpcmUtZG90LWZpbGVzIGRpcmVjdG9yeVxuICAgIGZpbGUgICAgICA9IGZpbGUgfHwgZGVmYXVsdHMuZmlsZXMuY29uZmlnO1xuICAgIGRpcmVjdG9yeSA9IGRpcmVjdG9yeSB8fCBwYXRoLmpvaW4oX19kaXJuYW1lLCBwYXRoLnNlcCwgJy4uJyk7XG4gICAgd2FsayhkaXJlY3RvcnkpO1xuICAgIHJldHVybiBjb25maWdQYXRoIHx8ICcnO1xuICB9LFxuXG4gIC8vIGFkZHMgYWRkaXRpb25hbCBkZXJpdmVkIHZhbHVlcyB0byBhIGNvbmZpZyBvYmplY3RcbiAgYWRkRGVyaXZlZFZhbHVlczogZnVuY3Rpb24oY29uZmlnKSB7XG5cbiAgICBjb25maWcgPSBjb25maWcgfHwgZXh0ZW5kKGZhbHNlLCB7fSwgZGVmYXVsdHMpO1xuXG4gICAgLyotLS0tLS0tLS0tLS0tLVxuICAgICAgIEZpbGUgUGF0aHNcbiAgICAtLS0tLS0tLS0tLS0tLS0qL1xuXG4gICAgdmFyXG4gICAgICBjb25maWdQYXRoID0gdGhpcy5nZXRQYXRoKCksXG4gICAgICBzb3VyY2VQYXRocyA9IHt9LFxuICAgICAgb3V0cHV0UGF0aHMgPSB7fSxcbiAgICAgIGZvbGRlclxuICAgIDtcblxuICAgIC8vIHJlc29sdmUgcGF0aHMgKGNvbmZpZyBsb2NhdGlvbiArIGJhc2UgKyBwYXRoKVxuICAgIGZvcihmb2xkZXIgaW4gY29uZmlnLnBhdGhzLnNvdXJjZSkge1xuICAgICAgaWYoY29uZmlnLnBhdGhzLnNvdXJjZS5oYXNPd25Qcm9wZXJ0eShmb2xkZXIpKSB7XG4gICAgICAgIHNvdXJjZVBhdGhzW2ZvbGRlcl0gPSBwYXRoLnJlc29sdmUocGF0aC5qb2luKGNvbmZpZ1BhdGgsIGNvbmZpZy5iYXNlLCBjb25maWcucGF0aHMuc291cmNlW2ZvbGRlcl0pKTtcbiAgICAgIH1cbiAgICB9XG4gICAgZm9yKGZvbGRlciBpbiBjb25maWcucGF0aHMub3V0cHV0KSB7XG4gICAgICBpZihjb25maWcucGF0aHMub3V0cHV0Lmhhc093blByb3BlcnR5KGZvbGRlcikpIHtcbiAgICAgICAgb3V0cHV0UGF0aHNbZm9sZGVyXSA9IHBhdGgucmVzb2x2ZShwYXRoLmpvaW4oY29uZmlnUGF0aCwgY29uZmlnLmJhc2UsIGNvbmZpZy5wYXRocy5vdXRwdXRbZm9sZGVyXSkpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIHNldCBjb25maWcgcGF0aHMgdG8gZnVsbCBwYXRoc1xuICAgIGNvbmZpZy5wYXRocy5zb3VyY2UgPSBzb3VyY2VQYXRocztcbiAgICBjb25maWcucGF0aHMub3V0cHV0ID0gb3V0cHV0UGF0aHM7XG5cbiAgICAvLyByZXNvbHZlIFwiY2xlYW5cIiBjb21tYW5kIHBhdGhcbiAgICBjb25maWcucGF0aHMuY2xlYW4gPSBwYXRoLnJlc29sdmUoIHBhdGguam9pbihjb25maWdQYXRoLCBjb25maWcuYmFzZSwgY29uZmlnLnBhdGhzLmNsZWFuKSApO1xuXG4gICAgLyotLS0tLS0tLS0tLS0tLVxuICAgICAgICBDU1MgVVJMc1xuICAgIC0tLS0tLS0tLS0tLS0tLSovXG5cbiAgICAvLyBkZXRlcm1pbmUgYXNzZXQgcGF0aHMgaW4gY3NzIGJ5IGZpbmRpbmcgcmVsYXRpdmUgcGF0aCBiZXR3ZWVuIHRoZW1lcyBhbmQgb3V0cHV0XG4gICAgLy8gZm9yY2UgZm9yd2FyZCBzbGFzaGVzXG5cbiAgICBjb25maWcucGF0aHMuYXNzZXRzID0ge1xuICAgICAgc291cmNlICAgICAgIDogJy4uLy4uL3RoZW1lcycsIC8vIHNvdXJjZSBhc3NldCBwYXRoIGlzIGFsd2F5cyB0aGUgc2FtZVxuICAgICAgdW5jb21wcmVzc2VkIDogJy4nICsgcGF0aC5zZXAgKyBwYXRoLnJlbGF0aXZlKGNvbmZpZy5wYXRocy5vdXRwdXQudW5jb21wcmVzc2VkLCBjb25maWcucGF0aHMub3V0cHV0LnRoZW1lcykucmVwbGFjZSgvXFxcXC9nLCAnLycpLFxuICAgICAgY29tcHJlc3NlZCAgIDogJy4nICsgcGF0aC5zZXAgKyBwYXRoLnJlbGF0aXZlKGNvbmZpZy5wYXRocy5vdXRwdXQuY29tcHJlc3NlZCwgY29uZmlnLnBhdGhzLm91dHB1dC50aGVtZXMpLnJlcGxhY2UoL1xcXFwvZywgJy8nKSxcbiAgICAgIHBhY2thZ2VkICAgICA6ICcuJyArIHBhdGguc2VwICsgcGF0aC5yZWxhdGl2ZShjb25maWcucGF0aHMub3V0cHV0LnBhY2thZ2VkLCBjb25maWcucGF0aHMub3V0cHV0LnRoZW1lcykucmVwbGFjZSgvXFxcXC9nLCAnLycpXG4gICAgfTtcblxuICAgIC8qLS0tLS0tLS0tLS0tLS1cbiAgICAgICBQZXJtaXNzaW9uXG4gICAgLS0tLS0tLS0tLS0tLS0tKi9cblxuICAgIGlmKGNvbmZpZy5wZXJtaXNzaW9uKSB7XG4gICAgICBjb25maWcuaGFzUGVybWlzc2lvbnMgPSB0cnVlO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIC8vIHBhc3MgYmxhbmsgb2JqZWN0IHRvIGF2b2lkIGNhdXNpbmcgZXJyb3JzXG4gICAgICBjb25maWcucGVybWlzc2lvbiAgICAgPSB7fTtcbiAgICAgIGNvbmZpZy5oYXNQZXJtaXNzaW9ucyA9IGZhbHNlO1xuICAgIH1cblxuICAgIC8qLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgIEdsb2JzXG4gICAgLS0tLS0tLS0tLS0tLS0tKi9cblxuICAgIGlmKCFjb25maWcuZ2xvYnMpIHtcbiAgICAgIGNvbmZpZy5nbG9icyA9IHt9O1xuICAgIH1cblxuICAgIC8vIHJlbW92ZSBkdXBsaWNhdGVzIGZyb20gY29tcG9uZW50IGFycmF5XG4gICAgaWYoY29uZmlnLmNvbXBvbmVudHMgaW5zdGFuY2VvZiBBcnJheSkge1xuICAgICAgY29uZmlnLmNvbXBvbmVudHMgPSBjb25maWcuY29tcG9uZW50cy5maWx0ZXIoZnVuY3Rpb24oY29tcG9uZW50LCBpbmRleCkge1xuICAgICAgICByZXR1cm4gY29uZmlnLmNvbXBvbmVudHMuaW5kZXhPZihjb21wb25lbnQpID09IGluZGV4O1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLy8gdGFrZXMgY29tcG9uZW50IG9iamVjdCBhbmQgY3JlYXRlcyBmaWxlIGdsb2IgbWF0Y2hpbmcgc2VsZWN0ZWQgY29tcG9uZW50c1xuICAgIGNvbmZpZy5nbG9icy5jb21wb25lbnRzID0gKHR5cGVvZiBjb25maWcuY29tcG9uZW50cyA9PSAnb2JqZWN0JylcbiAgICAgID8gKGNvbmZpZy5jb21wb25lbnRzLmxlbmd0aCA+IDEpXG4gICAgICAgID8gJ3snICsgY29uZmlnLmNvbXBvbmVudHMuam9pbignLCcpICsgJ30nXG4gICAgICAgIDogY29uZmlnLmNvbXBvbmVudHNbMF1cbiAgICAgIDogJ3snICsgZGVmYXVsdHMuY29tcG9uZW50cy5qb2luKCcsJykgKyAnfSdcbiAgICA7XG5cbiAgICByZXR1cm4gY29uZmlnO1xuXG4gIH1cblxufTtcblxuIl19