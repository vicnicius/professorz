/*******************************
            Set-up
*******************************/

var fs = require('fs'),
    path = require('path'),
    defaults = require('../defaults'),
    release = require('./release'),
    requireDotFile = require('require-dot-file');

/*******************************
          When to Ask
*******************************/

/* Preconditions for install questions */

var when = {

  // path
  changeRoot: function (questions) {
    return questions.useRoot !== undefined && questions.useRoot !== true;
  },

  // permissions
  changePermissions: function (questions) {
    return questions.changePermissions && questions.changePermissions === true;
  },

  // install
  hasConfig: function () {
    return requireDotFile('semantic.json', process.cwd());
  },

  allowOverwrite: function (questions) {
    return questions.overwrite === undefined || questions.overwrite == 'yes';
  },
  notAuto: function (questions) {
    return questions.install !== 'auto' && (questions.overwrite === undefined || questions.overwrite == 'yes');
  },
  custom: function (questions) {
    return questions.install === 'custom' && (questions.overwrite === undefined || questions.overwrite == 'yes');
  },
  express: function (questions) {
    return questions.install === 'express' && (questions.overwrite === undefined || questions.overwrite == 'yes');
  },

  // customize
  customize: function (questions) {
    return questions.customize === true;
  },
  primaryColor: function (questions) {
    return questions.primaryColor;
  },
  secondaryColor: function (questions) {
    return questions.secondaryColor;
  }
};

/*******************************
        Response Filters
*******************************/

/* Filters to user input from install questions */

var filter = {
  removeTrailingSlash: function (path) {
    return path.replace(/(\/$|\\$)+/mg, '');
  }
};

/*******************************
          Configuration
*******************************/

module.exports = {

  // check whether install is setup
  isSetup: function () {
    return when.hasConfig();
  },

  // detect whether there is a semantic.json configuration and that the auto-install option is set to true
  shouldAutoInstall: function () {
    var config = when.hasConfig();
    return config['autoInstall'];
  },

  // checks if files are in a PM directory
  getPackageManager: function (directory) {
    var
    // returns last matching result (avoid sub-module detection)
    walk = function (directory) {
      var pathArray = directory.split(path.sep),
          folder = pathArray[pathArray.length - 1],
          nextDirectory = path.join(directory, path.sep, '..');
      if (folder == 'bower_components') {
        return {
          name: 'Bower',
          root: nextDirectory
        };
      } else if (folder == 'node_modules') {
        return {
          name: 'NPM',
          root: nextDirectory
        };
      } else if (folder == 'composer') {
        return {
          name: 'Composer',
          root: nextDirectory
        };
      }
      if (path.resolve(directory) == path.resolve(nextDirectory)) {
        return false;
      }
      // recurse downward
      return walk(nextDirectory);
    };
    // start walk from current directory if none specified
    directory = directory || __dirname + path.sep;
    return walk(directory);
  },

  // checks if files is PMed submodule
  isSubModule: function (directory) {
    var moduleFolders = 0,
        walk = function (directory) {
      var pathArray = directory.split(path.sep),
          folder = pathArray[pathArray.length - 2],
          nextDirectory = path.join(directory, path.sep, '..');
      if (folder == 'bower_components') {
        moduleFolders++;
      } else if (folder == 'node_modules') {
        moduleFolders++;
      } else if (folder == 'composer') {
        moduleFolders++;
      }
      if (path.resolve(directory) == path.resolve(nextDirectory)) {
        return moduleFolders > 1;
      }
      // recurse downward
      return walk(nextDirectory);
    };
    // start walk from current directory if none specified
    directory = directory || __dirname + path.sep;
    return walk(directory);
  },

  createJSON: function (answers) {
    var json = {
      paths: {
        source: {},
        output: {}
      }
    };

    // add components
    if (answers.components) {
      json.components = answers.components;
    }

    // add rtl choice
    if (answers.rtl) {
      json.rtl = answers.rtl;
    }

    // add permissions
    if (answers.permission) {
      json.permission = answers.permission;
    }

    // add path to semantic
    if (answers.semanticRoot) {
      json.base = path.normalize(answers.semanticRoot);
    }

    // record version number to avoid re-installing on same version
    json.version = release.version;

    // add dist folder paths
    if (answers.dist) {
      answers.dist = path.normalize(answers.dist);

      json.paths.output = {
        packaged: path.normalize(answers.dist + '/'),
        uncompressed: path.normalize(answers.dist + '/components/'),
        compressed: path.normalize(answers.dist + '/components/'),
        themes: path.normalize(answers.dist + '/themes/')
      };
    }

    // add site path
    if (answers.site) {
      json.paths.source.site = path.normalize(answers.site + '/');
    }
    if (answers.packaged) {
      json.paths.output.packaged = path.normalize(answers.packaged + '/');
    }
    if (answers.compressed) {
      json.paths.output.compressed = path.normalize(answers.compressed + '/');
    }
    if (answers.uncompressed) {
      json.paths.output.uncompressed = path.normalize(answers.uncompressed + '/');
    }
    return json;
  },

  // files cleaned up after install
  setupFiles: ['./src/theme.config.example', './semantic.json.example', './src/_site'],

  regExp: {
    // used to match siteFolder variable in theme.less
    siteVariable: /@siteFolder .*\'(.*)/mg
  },

  // source paths (when installing)
  source: {
    config: './semantic.json.example',
    definitions: './src/definitions',
    gulpFile: './gulpfile.js',
    lessImport: './src/semantic.less',
    site: './src/_site',
    tasks: './tasks',
    themeConfig: './src/theme.config.example',
    themeImport: './src/theme.less',
    themes: './src/themes',
    defaultTheme: './src/themes/default',
    userGulpFile: './tasks/config/npm/gulpfile.js'
  },

  // expected final filenames
  files: {
    config: 'semantic.json',
    lessImport: 'src/semantic.less',
    site: 'src/site',
    themeConfig: 'src/theme.config',
    themeImport: 'src/theme.less'
  },

  // folder paths to files relative to root
  folders: {
    config: './',
    definitions: 'src/definitions/',
    lessImport: 'src/',
    modules: 'node_modules/',
    site: 'src/site/',
    tasks: 'tasks/',
    themeConfig: 'src/',
    themeImport: 'src/',
    themes: 'src/themes/',

    defaultTheme: 'default/' // only path that is relative to another directory and not root
  },

  // questions asked during install
  questions: {

    root: [{
      type: 'list',
      name: 'useRoot',
      message: '    \n' + '    {packageMessage} \n' + '    \n' + '    Is this your project folder?\n' + '    \x1b[92m{root}\x1b[0m \n' + '    \n ' + '\n',
      choices: [{
        name: 'Yes',
        value: true
      }, {
        name: 'No, let me specify',
        value: false
      }]
    }, {
      type: 'input',
      name: 'customRoot',
      message: 'Please enter the absolute path to your project root',
      default: '/my/project/path',
      when: when.changeRoot
    }, {
      type: 'input',
      name: 'semanticRoot',
      message: 'Where should we put Semantic UI inside your project?',
      default: 'semantic/'
    }],

    setup: [{
      type: 'list',
      name: 'overwrite',
      message: 'It looks like you have a semantic.json file already.',
      when: when.hasConfig,
      choices: [{
        name: 'Yes, extend my current settings.',
        value: 'yes'
      }, {
        name: 'Skip install',
        value: 'no'
      }]
    }, {
      type: 'list',
      name: 'install',
      message: 'Set-up Semantic UI',
      when: when.allowOverwrite,
      choices: [{
        name: 'Automatic (Use defaults locations and all components)',
        value: 'auto'
      }, {
        name: 'Express (Set components and output folder)',
        value: 'express'
      }, {
        name: 'Custom (Customize all src/dist values)',
        value: 'custom'
      }]
    }, {
      type: 'checkbox',
      name: 'components',
      message: 'What components should we include in the package?',

      // duplicated manually from tasks/defaults.js with additional property
      choices: [{ name: "reset", checked: true }, { name: "site", checked: true }, { name: "button", checked: true }, { name: "container", checked: true }, { name: "divider", checked: true }, { name: "flag", checked: true }, { name: "header", checked: true }, { name: "icon", checked: true }, { name: "image", checked: true }, { name: "input", checked: true }, { name: "label", checked: true }, { name: "list", checked: true }, { name: "loader", checked: true }, { name: "rail", checked: true }, { name: "reveal", checked: true }, { name: "segment", checked: true }, { name: "step", checked: true }, { name: "breadcrumb", checked: true }, { name: "form", checked: true }, { name: "grid", checked: true }, { name: "menu", checked: true }, { name: "message", checked: true }, { name: "table", checked: true }, { name: "ad", checked: true }, { name: "card", checked: true }, { name: "comment", checked: true }, { name: "feed", checked: true }, { name: "item", checked: true }, { name: "statistic", checked: true }, { name: "accordion", checked: true }, { name: "checkbox", checked: true }, { name: "dimmer", checked: true }, { name: "dropdown", checked: true }, { name: "embed", checked: true }, { name: "modal", checked: true }, { name: "nag", checked: true }, { name: "popup", checked: true }, { name: "progress", checked: true }, { name: "rating", checked: true }, { name: "search", checked: true }, { name: "shape", checked: true }, { name: "sidebar", checked: true }, { name: "sticky", checked: true }, { name: "tab", checked: true }, { name: "transition", checked: true }, { name: "api", checked: true }, { name: "form", checked: true }, { name: "state", checked: true }, { name: "visibility", checked: true }],
      when: when.notAuto
    }, {
      type: 'list',
      name: 'changePermissions',
      when: when.notAuto,
      message: 'Should we set permissions on outputted files?',
      choices: [{
        name: 'No',
        value: false
      }, {
        name: 'Yes',
        value: true
      }]
    }, {
      type: 'input',
      name: 'permission',
      message: 'What octal file permission should outputted files receive?',
      default: defaults.permission,
      when: when.changePermissions
    }, {
      type: 'list',
      name: 'rtl',
      message: 'Do you use a RTL (Right-To-Left) language?',
      when: when.notAuto,
      choices: [{
        name: 'No',
        value: false
      }, {
        name: 'Yes',
        value: true
      }, {
        name: 'Build Both',
        value: 'both'
      }]
    }, {
      type: 'input',
      name: 'dist',
      message: 'Where should we output Semantic UI?',
      default: defaults.paths.output.packaged,
      filter: filter.removeTrailingSlash,
      when: when.express
    }, {
      type: 'input',
      name: 'site',
      message: 'Where should we put your site folder?',
      default: defaults.paths.source.site,
      filter: filter.removeTrailingSlash,
      when: when.custom
    }, {
      type: 'input',
      name: 'packaged',
      message: 'Where should we output a packaged version?',
      default: defaults.paths.output.packaged,
      filter: filter.removeTrailingSlash,
      when: when.custom
    }, {
      type: 'input',
      name: 'compressed',
      message: 'Where should we output compressed components?',
      default: defaults.paths.output.compressed,
      filter: filter.removeTrailingSlash,
      when: when.custom
    }, {
      type: 'input',
      name: 'uncompressed',
      message: 'Where should we output uncompressed components?',
      default: defaults.paths.output.uncompressed,
      filter: filter.removeTrailingSlash,
      when: when.custom
    }],

    cleanup: [{
      type: 'list',
      name: 'cleanup',
      message: 'Should we remove set-up files?',
      choices: [{
        name: 'Yes (re-install will require redownloading semantic).',
        value: 'yes'
      }, {
        name: 'No Thanks',
        value: 'no'
      }]
    }, {
      type: 'list',
      name: 'build',
      message: 'Do you want to build Semantic now?',
      choices: [{
        name: 'Yes',
        value: 'yes'
      }, {
        name: 'No',
        value: 'no'
      }]
    }],
    site: [{
      type: 'list',
      name: 'customize',
      message: 'You have not yet customized your site, can we help you do that?',
      choices: [{
        name: 'Yes, ask me a few questions',
        value: true
      }, {
        name: 'No I\'ll do it myself',
        value: false
      }]
    }, {
      type: 'list',
      name: 'headerFont',
      message: 'Select your header font',
      choices: [{
        name: 'Helvetica Neue, Arial, sans-serif',
        value: 'Helvetica Neue, Arial, sans-serif;'
      }, {
        name: 'Lato (Google Fonts)',
        value: 'Lato'
      }, {
        name: 'Open Sans (Google Fonts)',
        value: 'Open Sans'
      }, {
        name: 'Source Sans Pro (Google Fonts)',
        value: 'Source Sans Pro'
      }, {
        name: 'Droid (Google Fonts)',
        value: 'Droid'
      }, {
        name: 'I\'ll choose on my own',
        value: false
      }],
      when: when.customize
    }, {
      type: 'list',
      name: 'pageFont',
      message: 'Select your page font',
      choices: [{
        name: 'Helvetica Neue, Arial, sans-serif',
        value: 'Helvetica Neue, Arial, sans-serif;'
      }, {
        name: 'Lato (Import from Google Fonts)',
        value: 'Lato'
      }, {
        name: 'Open Sans (Import from Google Fonts)',
        value: 'Open Sans'
      }, {
        name: 'Source Sans Pro (Import from Google Fonts)',
        value: 'Source Sans Pro'
      }, {
        name: 'Droid (Google Fonts)',
        value: 'Droid'
      }, {
        name: 'I\'ll choose on my own',
        value: false
      }],
      when: when.customize
    }, {
      type: 'list',
      name: 'fontSize',
      message: 'Select your base font size',
      default: '14px',
      choices: [{
        name: '12px'
      }, {
        name: '13px'
      }, {
        name: '14px (Recommended)',
        value: '14px'
      }, {
        name: '15px'
      }, {
        name: '16px'
      }, {
        name: 'I\'ll choose on my own',
        value: false
      }],
      when: when.customize
    }, {
      type: 'list',
      name: 'primaryColor',
      message: 'Select the closest name for your primary brand color',
      default: '14px',
      choices: [{
        name: 'Blue'
      }, {
        name: 'Green'
      }, {
        name: 'Orange'
      }, {
        name: 'Pink'
      }, {
        name: 'Purple'
      }, {
        name: 'Red'
      }, {
        name: 'Teal'
      }, {
        name: 'Yellow'
      }, {
        name: 'Black'
      }, {
        name: 'I\'ll choose on my own',
        value: false
      }],
      when: when.customize
    }, {
      type: 'input',
      name: 'PrimaryHex',
      message: 'Enter a hexcode for your primary brand color',
      when: when.primaryColor
    }, {
      type: 'list',
      name: 'secondaryColor',
      message: 'Select the closest name for your secondary brand color',
      default: '14px',
      choices: [{
        name: 'Blue'
      }, {
        name: 'Green'
      }, {
        name: 'Orange'
      }, {
        name: 'Pink'
      }, {
        name: 'Purple'
      }, {
        name: 'Red'
      }, {
        name: 'Teal'
      }, {
        name: 'Yellow'
      }, {
        name: 'Black'
      }, {
        name: 'I\'ll choose on my own',
        value: false
      }],
      when: when.customize
    }, {
      type: 'input',
      name: 'secondaryHex',
      message: 'Enter a hexcode for your secondary brand color',
      when: when.secondaryColor
    }]

  },

  settings: {

    /* Rename Files */
    rename: {
      json: { extname: '.json' }
    },

    /* Copy Install Folders */
    wrench: {

      // overwrite existing files update & install (default theme / definition)
      overwrite: {
        forceDelete: true,
        excludeHiddenUnix: true,
        preserveFiles: false
      },

      // only create files that don't exist (site theme update)
      merge: {
        forceDelete: false,
        excludeHiddenUnix: true,
        preserveFiles: true
      }

    }
  }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NvbmZpZy9wcm9qZWN0L2luc3RhbGwuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUlBLElBQ0UsS0FBaUIsUUFBUSxJQUFSLENBRG5CO0FBQUEsSUFFRSxPQUFpQixRQUFRLE1BQVIsQ0FGbkI7QUFBQSxJQUdFLFdBQWlCLFFBQVEsYUFBUixDQUhuQjtBQUFBLElBSUUsVUFBaUIsUUFBUSxXQUFSLENBSm5CO0FBQUEsSUFNRSxpQkFBaUIsUUFBUSxrQkFBUixDQU5uQjs7Ozs7Ozs7QUFlQSxJQUFJLE9BQU87OztBQUdULGNBQVksVUFBUyxTQUFULEVBQW9CO0FBQzlCLFdBQVEsVUFBVSxPQUFWLEtBQXNCLFNBQXRCLElBQW1DLFVBQVUsT0FBVixLQUFzQixJQUFqRTtBQUNELEdBTFE7OztBQVFULHFCQUFtQixVQUFTLFNBQVQsRUFBb0I7QUFDckMsV0FBUSxVQUFVLGlCQUFWLElBQStCLFVBQVUsaUJBQVYsS0FBZ0MsSUFBdkU7QUFDRCxHQVZROzs7QUFhVCxhQUFXLFlBQVc7QUFDcEIsV0FBTyxlQUFlLGVBQWYsRUFBZ0MsUUFBUSxHQUFSLEVBQWhDLENBQVA7QUFDRCxHQWZROztBQWlCVCxrQkFBZ0IsVUFBUyxTQUFULEVBQW9CO0FBQ2xDLFdBQVEsVUFBVSxTQUFWLEtBQXdCLFNBQXhCLElBQXFDLFVBQVUsU0FBVixJQUF1QixLQUFwRTtBQUNELEdBbkJRO0FBb0JULFdBQVMsVUFBUyxTQUFULEVBQW9CO0FBQzNCLFdBQVEsVUFBVSxPQUFWLEtBQXNCLE1BQXRCLEtBQWlDLFVBQVUsU0FBVixLQUF3QixTQUF4QixJQUFxQyxVQUFVLFNBQVYsSUFBdUIsS0FBN0YsQ0FBUjtBQUNELEdBdEJRO0FBdUJULFVBQVEsVUFBUyxTQUFULEVBQW9CO0FBQzFCLFdBQVEsVUFBVSxPQUFWLEtBQXNCLFFBQXRCLEtBQW1DLFVBQVUsU0FBVixLQUF3QixTQUF4QixJQUFxQyxVQUFVLFNBQVYsSUFBdUIsS0FBL0YsQ0FBUjtBQUNELEdBekJRO0FBMEJULFdBQVMsVUFBUyxTQUFULEVBQW9CO0FBQzNCLFdBQVEsVUFBVSxPQUFWLEtBQXNCLFNBQXRCLEtBQW9DLFVBQVUsU0FBVixLQUF3QixTQUF4QixJQUFxQyxVQUFVLFNBQVYsSUFBdUIsS0FBaEcsQ0FBUjtBQUNELEdBNUJROzs7QUErQlQsYUFBVyxVQUFTLFNBQVQsRUFBb0I7QUFDN0IsV0FBUSxVQUFVLFNBQVYsS0FBd0IsSUFBaEM7QUFDRCxHQWpDUTtBQWtDVCxnQkFBYyxVQUFTLFNBQVQsRUFBb0I7QUFDaEMsV0FBUSxVQUFVLFlBQWxCO0FBQ0QsR0FwQ1E7QUFxQ1Qsa0JBQWdCLFVBQVMsU0FBVCxFQUFvQjtBQUNsQyxXQUFRLFVBQVUsY0FBbEI7QUFDRDtBQXZDUSxDQUFYOzs7Ozs7OztBQWdEQSxJQUFJLFNBQVM7QUFDWCx1QkFBcUIsVUFBUyxJQUFULEVBQWU7QUFDbEMsV0FBTyxLQUFLLE9BQUwsQ0FBYSxjQUFiLEVBQTZCLEVBQTdCLENBQVA7QUFDRDtBQUhVLENBQWI7Ozs7OztBQVVBLE9BQU8sT0FBUCxHQUFpQjs7O0FBR2YsV0FBUyxZQUFXO0FBQ2xCLFdBQU8sS0FBSyxTQUFMLEVBQVA7QUFDRCxHQUxjOzs7QUFRZixxQkFBbUIsWUFBVztBQUM1QixRQUNFLFNBQVMsS0FBSyxTQUFMLEVBRFg7QUFHQSxXQUFPLE9BQU8sYUFBUCxDQUFQO0FBQ0QsR0FiYzs7O0FBZ0JmLHFCQUFtQixVQUFTLFNBQVQsRUFBb0I7QUFDckM7O0FBRUUsV0FBTyxVQUFTLFNBQVQsRUFBb0I7QUFDekIsVUFDRSxZQUFnQixVQUFVLEtBQVYsQ0FBZ0IsS0FBSyxHQUFyQixDQURsQjtBQUFBLFVBRUUsU0FBZ0IsVUFBVSxVQUFVLE1BQVYsR0FBbUIsQ0FBN0IsQ0FGbEI7QUFBQSxVQUdFLGdCQUFnQixLQUFLLElBQUwsQ0FBVSxTQUFWLEVBQXFCLEtBQUssR0FBMUIsRUFBK0IsSUFBL0IsQ0FIbEI7QUFLQSxVQUFJLFVBQVUsa0JBQWQsRUFBa0M7QUFDaEMsZUFBTztBQUNMLGdCQUFNLE9BREQ7QUFFTCxnQkFBTTtBQUZELFNBQVA7QUFJRCxPQUxELE1BTUssSUFBRyxVQUFVLGNBQWIsRUFBNkI7QUFDakMsZUFBTztBQUNKLGdCQUFNLEtBREY7QUFFSixnQkFBTTtBQUZGLFNBQVA7QUFJQSxPQUxJLE1BTUEsSUFBRyxVQUFVLFVBQWIsRUFBeUI7QUFDN0IsZUFBTztBQUNKLGdCQUFNLFVBREY7QUFFSixnQkFBTTtBQUZGLFNBQVA7QUFJQTtBQUNELFVBQUcsS0FBSyxPQUFMLENBQWEsU0FBYixLQUEyQixLQUFLLE9BQUwsQ0FBYSxhQUFiLENBQTlCLEVBQTJEO0FBQ3pELGVBQU8sS0FBUDtBQUNEOztBQUVELGFBQU8sS0FBSyxhQUFMLENBQVA7QUFDRCxLQS9CSDs7QUFrQ0EsZ0JBQVksYUFBYyxZQUFZLEtBQUssR0FBM0M7QUFDQSxXQUFPLEtBQUssU0FBTCxDQUFQO0FBQ0QsR0FyRGM7OztBQXdEZixlQUFhLFVBQVMsU0FBVCxFQUFvQjtBQUMvQixRQUNFLGdCQUFnQixDQURsQjtBQUFBLFFBRUUsT0FBTyxVQUFTLFNBQVQsRUFBb0I7QUFDekIsVUFDRSxZQUFnQixVQUFVLEtBQVYsQ0FBZ0IsS0FBSyxHQUFyQixDQURsQjtBQUFBLFVBRUUsU0FBZ0IsVUFBVSxVQUFVLE1BQVYsR0FBbUIsQ0FBN0IsQ0FGbEI7QUFBQSxVQUdFLGdCQUFnQixLQUFLLElBQUwsQ0FBVSxTQUFWLEVBQXFCLEtBQUssR0FBMUIsRUFBK0IsSUFBL0IsQ0FIbEI7QUFLQSxVQUFJLFVBQVUsa0JBQWQsRUFBa0M7QUFDaEM7QUFDRCxPQUZELE1BR0ssSUFBRyxVQUFVLGNBQWIsRUFBNkI7QUFDaEM7QUFDRCxPQUZJLE1BR0EsSUFBRyxVQUFVLFVBQWIsRUFBeUI7QUFDNUI7QUFDRDtBQUNELFVBQUcsS0FBSyxPQUFMLENBQWEsU0FBYixLQUEyQixLQUFLLE9BQUwsQ0FBYSxhQUFiLENBQTlCLEVBQTJEO0FBQ3pELGVBQVEsZ0JBQWdCLENBQXhCO0FBQ0Q7O0FBRUQsYUFBTyxLQUFLLGFBQUwsQ0FBUDtBQUNELEtBdEJIOztBQXlCQSxnQkFBWSxhQUFjLFlBQVksS0FBSyxHQUEzQztBQUNBLFdBQU8sS0FBSyxTQUFMLENBQVA7QUFDRCxHQXBGYzs7QUF1RmYsY0FBWSxVQUFTLE9BQVQsRUFBa0I7QUFDNUIsUUFDRSxPQUFPO0FBQ0wsYUFBTztBQUNMLGdCQUFRLEVBREg7QUFFTCxnQkFBUTtBQUZIO0FBREYsS0FEVDs7O0FBVUEsUUFBRyxRQUFRLFVBQVgsRUFBdUI7QUFDckIsV0FBSyxVQUFMLEdBQWtCLFFBQVEsVUFBMUI7QUFDRDs7O0FBR0QsUUFBRyxRQUFRLEdBQVgsRUFBZ0I7QUFDZCxXQUFLLEdBQUwsR0FBVyxRQUFRLEdBQW5CO0FBQ0Q7OztBQUdELFFBQUcsUUFBUSxVQUFYLEVBQXVCO0FBQ3JCLFdBQUssVUFBTCxHQUFrQixRQUFRLFVBQTFCO0FBQ0Q7OztBQUdELFFBQUcsUUFBUSxZQUFYLEVBQXlCO0FBQ3ZCLFdBQUssSUFBTCxHQUFZLEtBQUssU0FBTCxDQUFlLFFBQVEsWUFBdkIsQ0FBWjtBQUNEOzs7QUFHRCxTQUFLLE9BQUwsR0FBZSxRQUFRLE9BQXZCOzs7QUFHQSxRQUFHLFFBQVEsSUFBWCxFQUFpQjtBQUNmLGNBQVEsSUFBUixHQUFlLEtBQUssU0FBTCxDQUFlLFFBQVEsSUFBdkIsQ0FBZjs7QUFFQSxXQUFLLEtBQUwsQ0FBVyxNQUFYLEdBQW9CO0FBQ2xCLGtCQUFlLEtBQUssU0FBTCxDQUFlLFFBQVEsSUFBUixHQUFlLEdBQTlCLENBREc7QUFFbEIsc0JBQWUsS0FBSyxTQUFMLENBQWUsUUFBUSxJQUFSLEdBQWUsY0FBOUIsQ0FGRztBQUdsQixvQkFBZSxLQUFLLFNBQUwsQ0FBZSxRQUFRLElBQVIsR0FBZSxjQUE5QixDQUhHO0FBSWxCLGdCQUFlLEtBQUssU0FBTCxDQUFlLFFBQVEsSUFBUixHQUFlLFVBQTlCO0FBSkcsT0FBcEI7QUFNRDs7O0FBR0QsUUFBRyxRQUFRLElBQVgsRUFBaUI7QUFDZixXQUFLLEtBQUwsQ0FBVyxNQUFYLENBQWtCLElBQWxCLEdBQXlCLEtBQUssU0FBTCxDQUFlLFFBQVEsSUFBUixHQUFlLEdBQTlCLENBQXpCO0FBQ0Q7QUFDRCxRQUFHLFFBQVEsUUFBWCxFQUFxQjtBQUNuQixXQUFLLEtBQUwsQ0FBVyxNQUFYLENBQWtCLFFBQWxCLEdBQTZCLEtBQUssU0FBTCxDQUFlLFFBQVEsUUFBUixHQUFtQixHQUFsQyxDQUE3QjtBQUNEO0FBQ0QsUUFBRyxRQUFRLFVBQVgsRUFBdUI7QUFDckIsV0FBSyxLQUFMLENBQVcsTUFBWCxDQUFrQixVQUFsQixHQUErQixLQUFLLFNBQUwsQ0FBZSxRQUFRLFVBQVIsR0FBcUIsR0FBcEMsQ0FBL0I7QUFDRDtBQUNELFFBQUcsUUFBUSxZQUFYLEVBQXlCO0FBQ3ZCLFdBQUssS0FBTCxDQUFXLE1BQVgsQ0FBa0IsWUFBbEIsR0FBaUMsS0FBSyxTQUFMLENBQWUsUUFBUSxZQUFSLEdBQXVCLEdBQXRDLENBQWpDO0FBQ0Q7QUFDRCxXQUFPLElBQVA7QUFDRCxHQWxKYzs7O0FBcUpmLGNBQVksQ0FDViw0QkFEVSxFQUVWLHlCQUZVLEVBR1YsYUFIVSxDQXJKRzs7QUEySmYsVUFBUTs7QUFFTixrQkFBYztBQUZSLEdBM0pPOzs7QUFpS2YsVUFBUTtBQUNOLFlBQWUseUJBRFQ7QUFFTixpQkFBZSxtQkFGVDtBQUdOLGNBQWUsZUFIVDtBQUlOLGdCQUFlLHFCQUpUO0FBS04sVUFBZSxhQUxUO0FBTU4sV0FBZSxTQU5UO0FBT04saUJBQWUsNEJBUFQ7QUFRTixpQkFBZSxrQkFSVDtBQVNOLFlBQWUsY0FUVDtBQVVOLGtCQUFlLHNCQVZUO0FBV04sa0JBQWU7QUFYVCxHQWpLTzs7O0FBZ0xmLFNBQU87QUFDTCxZQUFjLGVBRFQ7QUFFTCxnQkFBYyxtQkFGVDtBQUdMLFVBQWMsVUFIVDtBQUlMLGlCQUFjLGtCQUpUO0FBS0wsaUJBQWM7QUFMVCxHQWhMUTs7O0FBeUxmLFdBQVM7QUFDUCxZQUFlLElBRFI7QUFFUCxpQkFBZSxrQkFGUjtBQUdQLGdCQUFlLE1BSFI7QUFJUCxhQUFlLGVBSlI7QUFLUCxVQUFlLFdBTFI7QUFNUCxXQUFlLFFBTlI7QUFPUCxpQkFBZSxNQVBSO0FBUVAsaUJBQWUsTUFSUjtBQVNQLFlBQWUsYUFUUjs7QUFXUCxrQkFBZSxVO0FBWFIsR0F6TE07OztBQXdNZixhQUFXOztBQUVULFVBQU0sQ0FDSjtBQUNFLFlBQVUsTUFEWjtBQUVFLFlBQVUsU0FGWjtBQUdFLGVBQ0UsV0FDQSx5QkFEQSxHQUVBLFFBRkEsR0FHQSxvQ0FIQSxHQUlBLDhCQUpBLEdBS0EsU0FMQSxHQU1BLElBVko7QUFXRSxlQUFTLENBQ1A7QUFDRSxjQUFRLEtBRFY7QUFFRSxlQUFRO0FBRlYsT0FETyxFQUtQO0FBQ0UsY0FBUSxvQkFEVjtBQUVFLGVBQVE7QUFGVixPQUxPO0FBWFgsS0FESSxFQXVCSjtBQUNFLFlBQVUsT0FEWjtBQUVFLFlBQVUsWUFGWjtBQUdFLGVBQVUscURBSFo7QUFJRSxlQUFVLGtCQUpaO0FBS0UsWUFBVSxLQUFLO0FBTGpCLEtBdkJJLEVBOEJKO0FBQ0UsWUFBVSxPQURaO0FBRUUsWUFBVSxjQUZaO0FBR0UsZUFBVSxzREFIWjtBQUlFLGVBQVU7QUFKWixLQTlCSSxDQUZHOztBQXdDVCxXQUFPLENBQ0w7QUFDRSxZQUFNLE1BRFI7QUFFRSxZQUFNLFdBRlI7QUFHRSxlQUFTLHNEQUhYO0FBSUUsWUFBTSxLQUFLLFNBSmI7QUFLRSxlQUFTLENBQ1A7QUFDRSxjQUFNLGtDQURSO0FBRUUsZUFBTztBQUZULE9BRE8sRUFLUDtBQUNFLGNBQU0sY0FEUjtBQUVFLGVBQU87QUFGVCxPQUxPO0FBTFgsS0FESyxFQWlCTDtBQUNFLFlBQU0sTUFEUjtBQUVFLFlBQU0sU0FGUjtBQUdFLGVBQVMsb0JBSFg7QUFJRSxZQUFNLEtBQUssY0FKYjtBQUtFLGVBQVMsQ0FDUDtBQUNFLGNBQU0sdURBRFI7QUFFRSxlQUFPO0FBRlQsT0FETyxFQUtQO0FBQ0UsY0FBTSw0Q0FEUjtBQUVFLGVBQU87QUFGVCxPQUxPLEVBU1A7QUFDRSxjQUFNLHdDQURSO0FBRUUsZUFBTztBQUZULE9BVE87QUFMWCxLQWpCSyxFQXFDTDtBQUNFLFlBQU0sVUFEUjtBQUVFLFlBQU0sWUFGUjtBQUdFLGVBQVMsbURBSFg7OztBQU1FLGVBQVMsQ0FDUCxFQUFFLE1BQU0sT0FBUixFQUFpQixTQUFTLElBQTFCLEVBRE8sRUFFUCxFQUFFLE1BQU0sTUFBUixFQUFnQixTQUFTLElBQXpCLEVBRk8sRUFHUCxFQUFFLE1BQU0sUUFBUixFQUFrQixTQUFTLElBQTNCLEVBSE8sRUFJUCxFQUFFLE1BQU0sV0FBUixFQUFxQixTQUFTLElBQTlCLEVBSk8sRUFLUCxFQUFFLE1BQU0sU0FBUixFQUFtQixTQUFTLElBQTVCLEVBTE8sRUFNUCxFQUFFLE1BQU0sTUFBUixFQUFnQixTQUFTLElBQXpCLEVBTk8sRUFPUCxFQUFFLE1BQU0sUUFBUixFQUFrQixTQUFTLElBQTNCLEVBUE8sRUFRUCxFQUFFLE1BQU0sTUFBUixFQUFnQixTQUFTLElBQXpCLEVBUk8sRUFTUCxFQUFFLE1BQU0sT0FBUixFQUFpQixTQUFTLElBQTFCLEVBVE8sRUFVUCxFQUFFLE1BQU0sT0FBUixFQUFpQixTQUFTLElBQTFCLEVBVk8sRUFXUCxFQUFFLE1BQU0sT0FBUixFQUFpQixTQUFTLElBQTFCLEVBWE8sRUFZUCxFQUFFLE1BQU0sTUFBUixFQUFnQixTQUFTLElBQXpCLEVBWk8sRUFhUCxFQUFFLE1BQU0sUUFBUixFQUFrQixTQUFTLElBQTNCLEVBYk8sRUFjUCxFQUFFLE1BQU0sTUFBUixFQUFnQixTQUFTLElBQXpCLEVBZE8sRUFlUCxFQUFFLE1BQU0sUUFBUixFQUFrQixTQUFTLElBQTNCLEVBZk8sRUFnQlAsRUFBRSxNQUFNLFNBQVIsRUFBbUIsU0FBUyxJQUE1QixFQWhCTyxFQWlCUCxFQUFFLE1BQU0sTUFBUixFQUFnQixTQUFTLElBQXpCLEVBakJPLEVBa0JQLEVBQUUsTUFBTSxZQUFSLEVBQXNCLFNBQVMsSUFBL0IsRUFsQk8sRUFtQlAsRUFBRSxNQUFNLE1BQVIsRUFBZ0IsU0FBUyxJQUF6QixFQW5CTyxFQW9CUCxFQUFFLE1BQU0sTUFBUixFQUFnQixTQUFTLElBQXpCLEVBcEJPLEVBcUJQLEVBQUUsTUFBTSxNQUFSLEVBQWdCLFNBQVMsSUFBekIsRUFyQk8sRUFzQlAsRUFBRSxNQUFNLFNBQVIsRUFBbUIsU0FBUyxJQUE1QixFQXRCTyxFQXVCUCxFQUFFLE1BQU0sT0FBUixFQUFpQixTQUFTLElBQTFCLEVBdkJPLEVBd0JQLEVBQUUsTUFBTSxJQUFSLEVBQWMsU0FBUyxJQUF2QixFQXhCTyxFQXlCUCxFQUFFLE1BQU0sTUFBUixFQUFnQixTQUFTLElBQXpCLEVBekJPLEVBMEJQLEVBQUUsTUFBTSxTQUFSLEVBQW1CLFNBQVMsSUFBNUIsRUExQk8sRUEyQlAsRUFBRSxNQUFNLE1BQVIsRUFBZ0IsU0FBUyxJQUF6QixFQTNCTyxFQTRCUCxFQUFFLE1BQU0sTUFBUixFQUFnQixTQUFTLElBQXpCLEVBNUJPLEVBNkJQLEVBQUUsTUFBTSxXQUFSLEVBQXFCLFNBQVMsSUFBOUIsRUE3Qk8sRUE4QlAsRUFBRSxNQUFNLFdBQVIsRUFBcUIsU0FBUyxJQUE5QixFQTlCTyxFQStCUCxFQUFFLE1BQU0sVUFBUixFQUFvQixTQUFTLElBQTdCLEVBL0JPLEVBZ0NQLEVBQUUsTUFBTSxRQUFSLEVBQWtCLFNBQVMsSUFBM0IsRUFoQ08sRUFpQ1AsRUFBRSxNQUFNLFVBQVIsRUFBb0IsU0FBUyxJQUE3QixFQWpDTyxFQWtDUCxFQUFFLE1BQU0sT0FBUixFQUFpQixTQUFTLElBQTFCLEVBbENPLEVBbUNQLEVBQUUsTUFBTSxPQUFSLEVBQWlCLFNBQVMsSUFBMUIsRUFuQ08sRUFvQ1AsRUFBRSxNQUFNLEtBQVIsRUFBZSxTQUFTLElBQXhCLEVBcENPLEVBcUNQLEVBQUUsTUFBTSxPQUFSLEVBQWlCLFNBQVMsSUFBMUIsRUFyQ08sRUFzQ1AsRUFBRSxNQUFNLFVBQVIsRUFBb0IsU0FBUyxJQUE3QixFQXRDTyxFQXVDUCxFQUFFLE1BQU0sUUFBUixFQUFrQixTQUFTLElBQTNCLEVBdkNPLEVBd0NQLEVBQUUsTUFBTSxRQUFSLEVBQWtCLFNBQVMsSUFBM0IsRUF4Q08sRUF5Q1AsRUFBRSxNQUFNLE9BQVIsRUFBaUIsU0FBUyxJQUExQixFQXpDTyxFQTBDUCxFQUFFLE1BQU0sU0FBUixFQUFtQixTQUFTLElBQTVCLEVBMUNPLEVBMkNQLEVBQUUsTUFBTSxRQUFSLEVBQWtCLFNBQVMsSUFBM0IsRUEzQ08sRUE0Q1AsRUFBRSxNQUFNLEtBQVIsRUFBZSxTQUFTLElBQXhCLEVBNUNPLEVBNkNQLEVBQUUsTUFBTSxZQUFSLEVBQXNCLFNBQVMsSUFBL0IsRUE3Q08sRUE4Q1AsRUFBRSxNQUFNLEtBQVIsRUFBZSxTQUFTLElBQXhCLEVBOUNPLEVBK0NQLEVBQUUsTUFBTSxNQUFSLEVBQWdCLFNBQVMsSUFBekIsRUEvQ08sRUFnRFAsRUFBRSxNQUFNLE9BQVIsRUFBaUIsU0FBUyxJQUExQixFQWhETyxFQWlEUCxFQUFFLE1BQU0sWUFBUixFQUFzQixTQUFTLElBQS9CLEVBakRPLENBTlg7QUF5REUsWUFBTSxLQUFLO0FBekRiLEtBckNLLEVBZ0dMO0FBQ0UsWUFBTSxNQURSO0FBRUUsWUFBTSxtQkFGUjtBQUdFLFlBQU0sS0FBSyxPQUhiO0FBSUUsZUFBUywrQ0FKWDtBQUtFLGVBQVMsQ0FDUDtBQUNFLGNBQU0sSUFEUjtBQUVFLGVBQU87QUFGVCxPQURPLEVBS1A7QUFDRSxjQUFNLEtBRFI7QUFFRSxlQUFPO0FBRlQsT0FMTztBQUxYLEtBaEdLLEVBZ0hMO0FBQ0UsWUFBTSxPQURSO0FBRUUsWUFBTSxZQUZSO0FBR0UsZUFBUyw0REFIWDtBQUlFLGVBQVMsU0FBUyxVQUpwQjtBQUtFLFlBQU0sS0FBSztBQUxiLEtBaEhLLEVBdUhMO0FBQ0UsWUFBTSxNQURSO0FBRUUsWUFBTSxLQUZSO0FBR0UsZUFBUyw0Q0FIWDtBQUlFLFlBQU0sS0FBSyxPQUpiO0FBS0UsZUFBUyxDQUNQO0FBQ0UsY0FBTSxJQURSO0FBRUUsZUFBTztBQUZULE9BRE8sRUFLUDtBQUNFLGNBQU0sS0FEUjtBQUVFLGVBQU87QUFGVCxPQUxPLEVBU1A7QUFDRSxjQUFNLFlBRFI7QUFFRSxlQUFPO0FBRlQsT0FUTztBQUxYLEtBdkhLLEVBMklMO0FBQ0UsWUFBTSxPQURSO0FBRUUsWUFBTSxNQUZSO0FBR0UsZUFBUyxxQ0FIWDtBQUlFLGVBQVMsU0FBUyxLQUFULENBQWUsTUFBZixDQUFzQixRQUpqQztBQUtFLGNBQVEsT0FBTyxtQkFMakI7QUFNRSxZQUFNLEtBQUs7QUFOYixLQTNJSyxFQW1KTDtBQUNFLFlBQU0sT0FEUjtBQUVFLFlBQU0sTUFGUjtBQUdFLGVBQVMsdUNBSFg7QUFJRSxlQUFTLFNBQVMsS0FBVCxDQUFlLE1BQWYsQ0FBc0IsSUFKakM7QUFLRSxjQUFRLE9BQU8sbUJBTGpCO0FBTUUsWUFBTSxLQUFLO0FBTmIsS0FuSkssRUEySkw7QUFDRSxZQUFNLE9BRFI7QUFFRSxZQUFNLFVBRlI7QUFHRSxlQUFTLDRDQUhYO0FBSUUsZUFBUyxTQUFTLEtBQVQsQ0FBZSxNQUFmLENBQXNCLFFBSmpDO0FBS0UsY0FBUSxPQUFPLG1CQUxqQjtBQU1FLFlBQU0sS0FBSztBQU5iLEtBM0pLLEVBbUtMO0FBQ0UsWUFBTSxPQURSO0FBRUUsWUFBTSxZQUZSO0FBR0UsZUFBUywrQ0FIWDtBQUlFLGVBQVMsU0FBUyxLQUFULENBQWUsTUFBZixDQUFzQixVQUpqQztBQUtFLGNBQVEsT0FBTyxtQkFMakI7QUFNRSxZQUFNLEtBQUs7QUFOYixLQW5LSyxFQTJLTDtBQUNFLFlBQU0sT0FEUjtBQUVFLFlBQU0sY0FGUjtBQUdFLGVBQVMsaURBSFg7QUFJRSxlQUFTLFNBQVMsS0FBVCxDQUFlLE1BQWYsQ0FBc0IsWUFKakM7QUFLRSxjQUFRLE9BQU8sbUJBTGpCO0FBTUUsWUFBTSxLQUFLO0FBTmIsS0EzS0ssQ0F4Q0U7O0FBOE5ULGFBQVMsQ0FDUDtBQUNFLFlBQU0sTUFEUjtBQUVFLFlBQU0sU0FGUjtBQUdFLGVBQVMsZ0NBSFg7QUFJRSxlQUFTLENBQ1A7QUFDRSxjQUFNLHVEQURSO0FBRUUsZUFBTztBQUZULE9BRE8sRUFLUDtBQUNFLGNBQU0sV0FEUjtBQUVFLGVBQU87QUFGVCxPQUxPO0FBSlgsS0FETyxFQWdCUDtBQUNFLFlBQU0sTUFEUjtBQUVFLFlBQU0sT0FGUjtBQUdFLGVBQVMsb0NBSFg7QUFJRSxlQUFTLENBQ1A7QUFDRSxjQUFNLEtBRFI7QUFFRSxlQUFPO0FBRlQsT0FETyxFQUtQO0FBQ0UsY0FBTSxJQURSO0FBRUUsZUFBTztBQUZULE9BTE87QUFKWCxLQWhCTyxDQTlOQTtBQThQVCxVQUFNLENBQ0o7QUFDRSxZQUFNLE1BRFI7QUFFRSxZQUFNLFdBRlI7QUFHRSxlQUFTLGlFQUhYO0FBSUUsZUFBUyxDQUNQO0FBQ0UsY0FBTSw2QkFEUjtBQUVFLGVBQU87QUFGVCxPQURPLEVBS1A7QUFDRSxjQUFNLHVCQURSO0FBRUUsZUFBTztBQUZULE9BTE87QUFKWCxLQURJLEVBZ0JKO0FBQ0UsWUFBTSxNQURSO0FBRUUsWUFBTSxZQUZSO0FBR0UsZUFBUyx5QkFIWDtBQUlFLGVBQVMsQ0FDUDtBQUNFLGNBQU0sbUNBRFI7QUFFRSxlQUFPO0FBRlQsT0FETyxFQUtQO0FBQ0UsY0FBTSxxQkFEUjtBQUVFLGVBQU87QUFGVCxPQUxPLEVBU1A7QUFDRSxjQUFNLDBCQURSO0FBRUUsZUFBTztBQUZULE9BVE8sRUFhUDtBQUNFLGNBQU0sZ0NBRFI7QUFFRSxlQUFPO0FBRlQsT0FiTyxFQWlCUDtBQUNFLGNBQU0sc0JBRFI7QUFFRSxlQUFPO0FBRlQsT0FqQk8sRUFxQlA7QUFDRSxjQUFNLHdCQURSO0FBRUUsZUFBTztBQUZULE9BckJPLENBSlg7QUE4QkUsWUFBTSxLQUFLO0FBOUJiLEtBaEJJLEVBZ0RKO0FBQ0UsWUFBTSxNQURSO0FBRUUsWUFBTSxVQUZSO0FBR0UsZUFBUyx1QkFIWDtBQUlFLGVBQVMsQ0FDUDtBQUNFLGNBQU0sbUNBRFI7QUFFRSxlQUFPO0FBRlQsT0FETyxFQUtQO0FBQ0UsY0FBTSxpQ0FEUjtBQUVFLGVBQU87QUFGVCxPQUxPLEVBU1A7QUFDRSxjQUFNLHNDQURSO0FBRUUsZUFBTztBQUZULE9BVE8sRUFhUDtBQUNFLGNBQU0sNENBRFI7QUFFRSxlQUFPO0FBRlQsT0FiTyxFQWlCUDtBQUNFLGNBQU0sc0JBRFI7QUFFRSxlQUFPO0FBRlQsT0FqQk8sRUFxQlA7QUFDRSxjQUFNLHdCQURSO0FBRUUsZUFBTztBQUZULE9BckJPLENBSlg7QUE4QkUsWUFBTSxLQUFLO0FBOUJiLEtBaERJLEVBZ0ZKO0FBQ0UsWUFBTSxNQURSO0FBRUUsWUFBTSxVQUZSO0FBR0UsZUFBUyw0QkFIWDtBQUlFLGVBQVMsTUFKWDtBQUtFLGVBQVMsQ0FDUDtBQUNFLGNBQU07QUFEUixPQURPLEVBSVA7QUFDRSxjQUFNO0FBRFIsT0FKTyxFQU9QO0FBQ0UsY0FBTSxvQkFEUjtBQUVFLGVBQU87QUFGVCxPQVBPLEVBV1A7QUFDRSxjQUFNO0FBRFIsT0FYTyxFQWNQO0FBQ0UsY0FBTTtBQURSLE9BZE8sRUFpQlA7QUFDRSxjQUFNLHdCQURSO0FBRUUsZUFBTztBQUZULE9BakJPLENBTFg7QUEyQkUsWUFBTSxLQUFLO0FBM0JiLEtBaEZJLEVBNkdKO0FBQ0UsWUFBTSxNQURSO0FBRUUsWUFBTSxjQUZSO0FBR0UsZUFBUyxzREFIWDtBQUlFLGVBQVMsTUFKWDtBQUtFLGVBQVMsQ0FDUDtBQUNFLGNBQU07QUFEUixPQURPLEVBSVA7QUFDRSxjQUFNO0FBRFIsT0FKTyxFQU9QO0FBQ0UsY0FBTTtBQURSLE9BUE8sRUFVUDtBQUNFLGNBQU07QUFEUixPQVZPLEVBYVA7QUFDRSxjQUFNO0FBRFIsT0FiTyxFQWdCUDtBQUNFLGNBQU07QUFEUixPQWhCTyxFQW1CUDtBQUNFLGNBQU07QUFEUixPQW5CTyxFQXNCUDtBQUNFLGNBQU07QUFEUixPQXRCTyxFQXlCUDtBQUNFLGNBQU07QUFEUixPQXpCTyxFQTRCUDtBQUNFLGNBQU0sd0JBRFI7QUFFRSxlQUFPO0FBRlQsT0E1Qk8sQ0FMWDtBQXNDRSxZQUFNLEtBQUs7QUF0Q2IsS0E3R0ksRUFxSko7QUFDRSxZQUFNLE9BRFI7QUFFRSxZQUFNLFlBRlI7QUFHRSxlQUFTLDhDQUhYO0FBSUUsWUFBTSxLQUFLO0FBSmIsS0FySkksRUEySko7QUFDRSxZQUFNLE1BRFI7QUFFRSxZQUFNLGdCQUZSO0FBR0UsZUFBUyx3REFIWDtBQUlFLGVBQVMsTUFKWDtBQUtFLGVBQVMsQ0FDUDtBQUNFLGNBQU07QUFEUixPQURPLEVBSVA7QUFDRSxjQUFNO0FBRFIsT0FKTyxFQU9QO0FBQ0UsY0FBTTtBQURSLE9BUE8sRUFVUDtBQUNFLGNBQU07QUFEUixPQVZPLEVBYVA7QUFDRSxjQUFNO0FBRFIsT0FiTyxFQWdCUDtBQUNFLGNBQU07QUFEUixPQWhCTyxFQW1CUDtBQUNFLGNBQU07QUFEUixPQW5CTyxFQXNCUDtBQUNFLGNBQU07QUFEUixPQXRCTyxFQXlCUDtBQUNFLGNBQU07QUFEUixPQXpCTyxFQTRCUDtBQUNFLGNBQU0sd0JBRFI7QUFFRSxlQUFPO0FBRlQsT0E1Qk8sQ0FMWDtBQXNDRSxZQUFNLEtBQUs7QUF0Q2IsS0EzSkksRUFtTUo7QUFDRSxZQUFNLE9BRFI7QUFFRSxZQUFNLGNBRlI7QUFHRSxlQUFTLGdEQUhYO0FBSUUsWUFBTSxLQUFLO0FBSmIsS0FuTUk7O0FBOVBHLEdBeE1JOztBQW1wQmYsWUFBVTs7O0FBR1IsWUFBUTtBQUNOLFlBQU8sRUFBRSxTQUFVLE9BQVo7QUFERCxLQUhBOzs7QUFRUixZQUFROzs7QUFHTixpQkFBVztBQUNULHFCQUFvQixJQURYO0FBRVQsMkJBQW9CLElBRlg7QUFHVCx1QkFBb0I7QUFIWCxPQUhMOzs7QUFVTixhQUFPO0FBQ0wscUJBQW9CLEtBRGY7QUFFTCwyQkFBb0IsSUFGZjtBQUdMLHVCQUFvQjtBQUhmOztBQVZEO0FBUkE7QUFucEJLLENBQWpCIiwiZmlsZSI6Imluc3RhbGwuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgICAgU2V0LXVwXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG52YXJcbiAgZnMgICAgICAgICAgICAgPSByZXF1aXJlKCdmcycpLFxuICBwYXRoICAgICAgICAgICA9IHJlcXVpcmUoJ3BhdGgnKSxcbiAgZGVmYXVsdHMgICAgICAgPSByZXF1aXJlKCcuLi9kZWZhdWx0cycpLFxuICByZWxlYXNlICAgICAgICA9IHJlcXVpcmUoJy4vcmVsZWFzZScpLFxuXG4gIHJlcXVpcmVEb3RGaWxlID0gcmVxdWlyZSgncmVxdWlyZS1kb3QtZmlsZScpXG47XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICAgV2hlbiB0byBBc2tcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi8qIFByZWNvbmRpdGlvbnMgZm9yIGluc3RhbGwgcXVlc3Rpb25zICovXG5cbnZhciB3aGVuID0ge1xuXG4gIC8vIHBhdGhcbiAgY2hhbmdlUm9vdDogZnVuY3Rpb24ocXVlc3Rpb25zKSB7XG4gICAgcmV0dXJuIChxdWVzdGlvbnMudXNlUm9vdCAhPT0gdW5kZWZpbmVkICYmIHF1ZXN0aW9ucy51c2VSb290ICE9PSB0cnVlKTtcbiAgfSxcblxuICAvLyBwZXJtaXNzaW9uc1xuICBjaGFuZ2VQZXJtaXNzaW9uczogZnVuY3Rpb24ocXVlc3Rpb25zKSB7XG4gICAgcmV0dXJuIChxdWVzdGlvbnMuY2hhbmdlUGVybWlzc2lvbnMgJiYgcXVlc3Rpb25zLmNoYW5nZVBlcm1pc3Npb25zID09PSB0cnVlKTtcbiAgfSxcblxuICAvLyBpbnN0YWxsXG4gIGhhc0NvbmZpZzogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHJlcXVpcmVEb3RGaWxlKCdzZW1hbnRpYy5qc29uJywgcHJvY2Vzcy5jd2QoKSk7XG4gIH0sXG5cbiAgYWxsb3dPdmVyd3JpdGU6IGZ1bmN0aW9uKHF1ZXN0aW9ucykge1xuICAgIHJldHVybiAocXVlc3Rpb25zLm92ZXJ3cml0ZSA9PT0gdW5kZWZpbmVkIHx8IHF1ZXN0aW9ucy5vdmVyd3JpdGUgPT0gJ3llcycpO1xuICB9LFxuICBub3RBdXRvOiBmdW5jdGlvbihxdWVzdGlvbnMpIHtcbiAgICByZXR1cm4gKHF1ZXN0aW9ucy5pbnN0YWxsICE9PSAnYXV0bycgJiYgKHF1ZXN0aW9ucy5vdmVyd3JpdGUgPT09IHVuZGVmaW5lZCB8fCBxdWVzdGlvbnMub3ZlcndyaXRlID09ICd5ZXMnKSk7XG4gIH0sXG4gIGN1c3RvbTogZnVuY3Rpb24ocXVlc3Rpb25zKSB7XG4gICAgcmV0dXJuIChxdWVzdGlvbnMuaW5zdGFsbCA9PT0gJ2N1c3RvbScgJiYgKHF1ZXN0aW9ucy5vdmVyd3JpdGUgPT09IHVuZGVmaW5lZCB8fCBxdWVzdGlvbnMub3ZlcndyaXRlID09ICd5ZXMnKSk7XG4gIH0sXG4gIGV4cHJlc3M6IGZ1bmN0aW9uKHF1ZXN0aW9ucykge1xuICAgIHJldHVybiAocXVlc3Rpb25zLmluc3RhbGwgPT09ICdleHByZXNzJyAmJiAocXVlc3Rpb25zLm92ZXJ3cml0ZSA9PT0gdW5kZWZpbmVkIHx8IHF1ZXN0aW9ucy5vdmVyd3JpdGUgPT0gJ3llcycpKTtcbiAgfSxcblxuICAvLyBjdXN0b21pemVcbiAgY3VzdG9taXplOiBmdW5jdGlvbihxdWVzdGlvbnMpIHtcbiAgICByZXR1cm4gKHF1ZXN0aW9ucy5jdXN0b21pemUgPT09IHRydWUpO1xuICB9LFxuICBwcmltYXJ5Q29sb3I6IGZ1bmN0aW9uKHF1ZXN0aW9ucykge1xuICAgIHJldHVybiAocXVlc3Rpb25zLnByaW1hcnlDb2xvcik7XG4gIH0sXG4gIHNlY29uZGFyeUNvbG9yOiBmdW5jdGlvbihxdWVzdGlvbnMpIHtcbiAgICByZXR1cm4gKHF1ZXN0aW9ucy5zZWNvbmRhcnlDb2xvcik7XG4gIH1cbn07XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgIFJlc3BvbnNlIEZpbHRlcnNcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi8qIEZpbHRlcnMgdG8gdXNlciBpbnB1dCBmcm9tIGluc3RhbGwgcXVlc3Rpb25zICovXG5cbnZhciBmaWx0ZXIgPSB7XG4gIHJlbW92ZVRyYWlsaW5nU2xhc2g6IGZ1bmN0aW9uKHBhdGgpIHtcbiAgICByZXR1cm4gcGF0aC5yZXBsYWNlKC8oXFwvJHxcXFxcJCkrL21nLCAnJyk7XG4gIH1cbn07XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICAgQ29uZmlndXJhdGlvblxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxubW9kdWxlLmV4cG9ydHMgPSB7XG5cbiAgLy8gY2hlY2sgd2hldGhlciBpbnN0YWxsIGlzIHNldHVwXG4gIGlzU2V0dXA6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiB3aGVuLmhhc0NvbmZpZygpO1xuICB9LFxuXG4gIC8vIGRldGVjdCB3aGV0aGVyIHRoZXJlIGlzIGEgc2VtYW50aWMuanNvbiBjb25maWd1cmF0aW9uIGFuZCB0aGF0IHRoZSBhdXRvLWluc3RhbGwgb3B0aW9uIGlzIHNldCB0byB0cnVlXG4gIHNob3VsZEF1dG9JbnN0YWxsOiBmdW5jdGlvbigpIHtcbiAgICB2YXJcbiAgICAgIGNvbmZpZyA9IHdoZW4uaGFzQ29uZmlnKClcbiAgICA7XG4gICAgcmV0dXJuIGNvbmZpZ1snYXV0b0luc3RhbGwnXTtcbiAgfSxcblxuICAvLyBjaGVja3MgaWYgZmlsZXMgYXJlIGluIGEgUE0gZGlyZWN0b3J5XG4gIGdldFBhY2thZ2VNYW5hZ2VyOiBmdW5jdGlvbihkaXJlY3RvcnkpIHtcbiAgICB2YXJcbiAgICAgIC8vIHJldHVybnMgbGFzdCBtYXRjaGluZyByZXN1bHQgKGF2b2lkIHN1Yi1tb2R1bGUgZGV0ZWN0aW9uKVxuICAgICAgd2FsayA9IGZ1bmN0aW9uKGRpcmVjdG9yeSkge1xuICAgICAgICB2YXJcbiAgICAgICAgICBwYXRoQXJyYXkgICAgID0gZGlyZWN0b3J5LnNwbGl0KHBhdGguc2VwKSxcbiAgICAgICAgICBmb2xkZXIgICAgICAgID0gcGF0aEFycmF5W3BhdGhBcnJheS5sZW5ndGggLSAxXSxcbiAgICAgICAgICBuZXh0RGlyZWN0b3J5ID0gcGF0aC5qb2luKGRpcmVjdG9yeSwgcGF0aC5zZXAsICcuLicpXG4gICAgICAgIDtcbiAgICAgICAgaWYoIGZvbGRlciA9PSAnYm93ZXJfY29tcG9uZW50cycpIHtcbiAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbmFtZTogJ0Jvd2VyJyxcbiAgICAgICAgICAgIHJvb3Q6IG5leHREaXJlY3RvcnlcbiAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYoZm9sZGVyID09ICdub2RlX21vZHVsZXMnKSB7XG4gICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbmFtZTogJ05QTScsXG4gICAgICAgICAgICByb290OiBuZXh0RGlyZWN0b3J5XG4gICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmKGZvbGRlciA9PSAnY29tcG9zZXInKSB7XG4gICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbmFtZTogJ0NvbXBvc2VyJyxcbiAgICAgICAgICAgIHJvb3Q6IG5leHREaXJlY3RvcnlcbiAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICAgIGlmKHBhdGgucmVzb2x2ZShkaXJlY3RvcnkpID09IHBhdGgucmVzb2x2ZShuZXh0RGlyZWN0b3J5KSkge1xuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICAvLyByZWN1cnNlIGRvd253YXJkXG4gICAgICAgIHJldHVybiB3YWxrKG5leHREaXJlY3RvcnkpO1xuICAgICAgfVxuICAgIDtcbiAgICAvLyBzdGFydCB3YWxrIGZyb20gY3VycmVudCBkaXJlY3RvcnkgaWYgbm9uZSBzcGVjaWZpZWRcbiAgICBkaXJlY3RvcnkgPSBkaXJlY3RvcnkgfHwgKF9fZGlybmFtZSArIHBhdGguc2VwKTtcbiAgICByZXR1cm4gd2FsayhkaXJlY3RvcnkpO1xuICB9LFxuXG4gIC8vIGNoZWNrcyBpZiBmaWxlcyBpcyBQTWVkIHN1Ym1vZHVsZVxuICBpc1N1Yk1vZHVsZTogZnVuY3Rpb24oZGlyZWN0b3J5KSB7XG4gICAgdmFyXG4gICAgICBtb2R1bGVGb2xkZXJzID0gMCxcbiAgICAgIHdhbGsgPSBmdW5jdGlvbihkaXJlY3RvcnkpIHtcbiAgICAgICAgdmFyXG4gICAgICAgICAgcGF0aEFycmF5ICAgICA9IGRpcmVjdG9yeS5zcGxpdChwYXRoLnNlcCksXG4gICAgICAgICAgZm9sZGVyICAgICAgICA9IHBhdGhBcnJheVtwYXRoQXJyYXkubGVuZ3RoIC0gMl0sXG4gICAgICAgICAgbmV4dERpcmVjdG9yeSA9IHBhdGguam9pbihkaXJlY3RvcnksIHBhdGguc2VwLCAnLi4nKVxuICAgICAgICA7XG4gICAgICAgIGlmKCBmb2xkZXIgPT0gJ2Jvd2VyX2NvbXBvbmVudHMnKSB7XG4gICAgICAgICAgbW9kdWxlRm9sZGVycysrO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYoZm9sZGVyID09ICdub2RlX21vZHVsZXMnKSB7XG4gICAgICAgICAgbW9kdWxlRm9sZGVycysrO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYoZm9sZGVyID09ICdjb21wb3NlcicpIHtcbiAgICAgICAgICBtb2R1bGVGb2xkZXJzKys7XG4gICAgICAgIH1cbiAgICAgICAgaWYocGF0aC5yZXNvbHZlKGRpcmVjdG9yeSkgPT0gcGF0aC5yZXNvbHZlKG5leHREaXJlY3RvcnkpKSB7XG4gICAgICAgICAgcmV0dXJuIChtb2R1bGVGb2xkZXJzID4gMSk7XG4gICAgICAgIH1cbiAgICAgICAgLy8gcmVjdXJzZSBkb3dud2FyZFxuICAgICAgICByZXR1cm4gd2FsayhuZXh0RGlyZWN0b3J5KTtcbiAgICAgIH1cbiAgICA7XG4gICAgLy8gc3RhcnQgd2FsayBmcm9tIGN1cnJlbnQgZGlyZWN0b3J5IGlmIG5vbmUgc3BlY2lmaWVkXG4gICAgZGlyZWN0b3J5ID0gZGlyZWN0b3J5IHx8IChfX2Rpcm5hbWUgKyBwYXRoLnNlcCk7XG4gICAgcmV0dXJuIHdhbGsoZGlyZWN0b3J5KTtcbiAgfSxcblxuXG4gIGNyZWF0ZUpTT046IGZ1bmN0aW9uKGFuc3dlcnMpIHtcbiAgICB2YXJcbiAgICAgIGpzb24gPSB7XG4gICAgICAgIHBhdGhzOiB7XG4gICAgICAgICAgc291cmNlOiB7fSxcbiAgICAgICAgICBvdXRwdXQ6IHt9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICA7XG5cbiAgICAvLyBhZGQgY29tcG9uZW50c1xuICAgIGlmKGFuc3dlcnMuY29tcG9uZW50cykge1xuICAgICAganNvbi5jb21wb25lbnRzID0gYW5zd2Vycy5jb21wb25lbnRzO1xuICAgIH1cblxuICAgIC8vIGFkZCBydGwgY2hvaWNlXG4gICAgaWYoYW5zd2Vycy5ydGwpIHtcbiAgICAgIGpzb24ucnRsID0gYW5zd2Vycy5ydGw7XG4gICAgfVxuXG4gICAgLy8gYWRkIHBlcm1pc3Npb25zXG4gICAgaWYoYW5zd2Vycy5wZXJtaXNzaW9uKSB7XG4gICAgICBqc29uLnBlcm1pc3Npb24gPSBhbnN3ZXJzLnBlcm1pc3Npb247XG4gICAgfVxuXG4gICAgLy8gYWRkIHBhdGggdG8gc2VtYW50aWNcbiAgICBpZihhbnN3ZXJzLnNlbWFudGljUm9vdCkge1xuICAgICAganNvbi5iYXNlID0gcGF0aC5ub3JtYWxpemUoYW5zd2Vycy5zZW1hbnRpY1Jvb3QpO1xuICAgIH1cblxuICAgIC8vIHJlY29yZCB2ZXJzaW9uIG51bWJlciB0byBhdm9pZCByZS1pbnN0YWxsaW5nIG9uIHNhbWUgdmVyc2lvblxuICAgIGpzb24udmVyc2lvbiA9IHJlbGVhc2UudmVyc2lvbjtcblxuICAgIC8vIGFkZCBkaXN0IGZvbGRlciBwYXRoc1xuICAgIGlmKGFuc3dlcnMuZGlzdCkge1xuICAgICAgYW5zd2Vycy5kaXN0ID0gcGF0aC5ub3JtYWxpemUoYW5zd2Vycy5kaXN0KTtcblxuICAgICAganNvbi5wYXRocy5vdXRwdXQgPSB7XG4gICAgICAgIHBhY2thZ2VkICAgICA6IHBhdGgubm9ybWFsaXplKGFuc3dlcnMuZGlzdCArICcvJyksXG4gICAgICAgIHVuY29tcHJlc3NlZCA6IHBhdGgubm9ybWFsaXplKGFuc3dlcnMuZGlzdCArICcvY29tcG9uZW50cy8nKSxcbiAgICAgICAgY29tcHJlc3NlZCAgIDogcGF0aC5ub3JtYWxpemUoYW5zd2Vycy5kaXN0ICsgJy9jb21wb25lbnRzLycpLFxuICAgICAgICB0aGVtZXMgICAgICAgOiBwYXRoLm5vcm1hbGl6ZShhbnN3ZXJzLmRpc3QgKyAnL3RoZW1lcy8nKVxuICAgICAgfTtcbiAgICB9XG5cbiAgICAvLyBhZGQgc2l0ZSBwYXRoXG4gICAgaWYoYW5zd2Vycy5zaXRlKSB7XG4gICAgICBqc29uLnBhdGhzLnNvdXJjZS5zaXRlID0gcGF0aC5ub3JtYWxpemUoYW5zd2Vycy5zaXRlICsgJy8nKTtcbiAgICB9XG4gICAgaWYoYW5zd2Vycy5wYWNrYWdlZCkge1xuICAgICAganNvbi5wYXRocy5vdXRwdXQucGFja2FnZWQgPSBwYXRoLm5vcm1hbGl6ZShhbnN3ZXJzLnBhY2thZ2VkICsgJy8nKTtcbiAgICB9XG4gICAgaWYoYW5zd2Vycy5jb21wcmVzc2VkKSB7XG4gICAgICBqc29uLnBhdGhzLm91dHB1dC5jb21wcmVzc2VkID0gcGF0aC5ub3JtYWxpemUoYW5zd2Vycy5jb21wcmVzc2VkICsgJy8nKTtcbiAgICB9XG4gICAgaWYoYW5zd2Vycy51bmNvbXByZXNzZWQpIHtcbiAgICAgIGpzb24ucGF0aHMub3V0cHV0LnVuY29tcHJlc3NlZCA9IHBhdGgubm9ybWFsaXplKGFuc3dlcnMudW5jb21wcmVzc2VkICsgJy8nKTtcbiAgICB9XG4gICAgcmV0dXJuIGpzb247XG4gIH0sXG5cbiAgLy8gZmlsZXMgY2xlYW5lZCB1cCBhZnRlciBpbnN0YWxsXG4gIHNldHVwRmlsZXM6IFtcbiAgICAnLi9zcmMvdGhlbWUuY29uZmlnLmV4YW1wbGUnLFxuICAgICcuL3NlbWFudGljLmpzb24uZXhhbXBsZScsXG4gICAgJy4vc3JjL19zaXRlJ1xuICBdLFxuXG4gIHJlZ0V4cDoge1xuICAgIC8vIHVzZWQgdG8gbWF0Y2ggc2l0ZUZvbGRlciB2YXJpYWJsZSBpbiB0aGVtZS5sZXNzXG4gICAgc2l0ZVZhcmlhYmxlOiAvQHNpdGVGb2xkZXIgLipcXCcoLiopL21nXG4gIH0sXG5cbiAgLy8gc291cmNlIHBhdGhzICh3aGVuIGluc3RhbGxpbmcpXG4gIHNvdXJjZToge1xuICAgIGNvbmZpZyAgICAgICA6ICcuL3NlbWFudGljLmpzb24uZXhhbXBsZScsXG4gICAgZGVmaW5pdGlvbnMgIDogJy4vc3JjL2RlZmluaXRpb25zJyxcbiAgICBndWxwRmlsZSAgICAgOiAnLi9ndWxwZmlsZS5qcycsXG4gICAgbGVzc0ltcG9ydCAgIDogJy4vc3JjL3NlbWFudGljLmxlc3MnLFxuICAgIHNpdGUgICAgICAgICA6ICcuL3NyYy9fc2l0ZScsXG4gICAgdGFza3MgICAgICAgIDogJy4vdGFza3MnLFxuICAgIHRoZW1lQ29uZmlnICA6ICcuL3NyYy90aGVtZS5jb25maWcuZXhhbXBsZScsXG4gICAgdGhlbWVJbXBvcnQgIDogJy4vc3JjL3RoZW1lLmxlc3MnLFxuICAgIHRoZW1lcyAgICAgICA6ICcuL3NyYy90aGVtZXMnLFxuICAgIGRlZmF1bHRUaGVtZSA6ICcuL3NyYy90aGVtZXMvZGVmYXVsdCcsXG4gICAgdXNlckd1bHBGaWxlIDogJy4vdGFza3MvY29uZmlnL25wbS9ndWxwZmlsZS5qcydcbiAgfSxcblxuICAvLyBleHBlY3RlZCBmaW5hbCBmaWxlbmFtZXNcbiAgZmlsZXM6IHtcbiAgICBjb25maWcgICAgICA6ICdzZW1hbnRpYy5qc29uJyxcbiAgICBsZXNzSW1wb3J0ICA6ICdzcmMvc2VtYW50aWMubGVzcycsXG4gICAgc2l0ZSAgICAgICAgOiAnc3JjL3NpdGUnLFxuICAgIHRoZW1lQ29uZmlnIDogJ3NyYy90aGVtZS5jb25maWcnLFxuICAgIHRoZW1lSW1wb3J0IDogJ3NyYy90aGVtZS5sZXNzJ1xuICB9LFxuXG4gIC8vIGZvbGRlciBwYXRocyB0byBmaWxlcyByZWxhdGl2ZSB0byByb290XG4gIGZvbGRlcnM6IHtcbiAgICBjb25maWcgICAgICAgOiAnLi8nLFxuICAgIGRlZmluaXRpb25zICA6ICdzcmMvZGVmaW5pdGlvbnMvJyxcbiAgICBsZXNzSW1wb3J0ICAgOiAnc3JjLycsXG4gICAgbW9kdWxlcyAgICAgIDogJ25vZGVfbW9kdWxlcy8nLFxuICAgIHNpdGUgICAgICAgICA6ICdzcmMvc2l0ZS8nLFxuICAgIHRhc2tzICAgICAgICA6ICd0YXNrcy8nLFxuICAgIHRoZW1lQ29uZmlnICA6ICdzcmMvJyxcbiAgICB0aGVtZUltcG9ydCAgOiAnc3JjLycsXG4gICAgdGhlbWVzICAgICAgIDogJ3NyYy90aGVtZXMvJyxcblxuICAgIGRlZmF1bHRUaGVtZSA6ICdkZWZhdWx0LycgLy8gb25seSBwYXRoIHRoYXQgaXMgcmVsYXRpdmUgdG8gYW5vdGhlciBkaXJlY3RvcnkgYW5kIG5vdCByb290XG4gIH0sXG5cbiAgLy8gcXVlc3Rpb25zIGFza2VkIGR1cmluZyBpbnN0YWxsXG4gIHF1ZXN0aW9uczoge1xuXG4gICAgcm9vdDogW1xuICAgICAge1xuICAgICAgICB0eXBlICAgIDogJ2xpc3QnLFxuICAgICAgICBuYW1lICAgIDogJ3VzZVJvb3QnLFxuICAgICAgICBtZXNzYWdlIDpcbiAgICAgICAgICAnICAgIFxcbicgK1xuICAgICAgICAgICcgICAge3BhY2thZ2VNZXNzYWdlfSBcXG4nICtcbiAgICAgICAgICAnICAgIFxcbicgK1xuICAgICAgICAgICcgICAgSXMgdGhpcyB5b3VyIHByb2plY3QgZm9sZGVyP1xcbicgK1xuICAgICAgICAgICcgICAgXFx4MWJbOTJte3Jvb3R9XFx4MWJbMG0gXFxuJyArXG4gICAgICAgICAgJyAgICBcXG4gJyArXG4gICAgICAgICAgJ1xcbicsXG4gICAgICAgIGNob2ljZXM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lICA6ICdZZXMnLFxuICAgICAgICAgICAgdmFsdWUgOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lICA6ICdObywgbGV0IG1lIHNwZWNpZnknLFxuICAgICAgICAgICAgdmFsdWUgOiBmYWxzZVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdHlwZSAgICA6ICdpbnB1dCcsXG4gICAgICAgIG5hbWUgICAgOiAnY3VzdG9tUm9vdCcsXG4gICAgICAgIG1lc3NhZ2UgOiAnUGxlYXNlIGVudGVyIHRoZSBhYnNvbHV0ZSBwYXRoIHRvIHlvdXIgcHJvamVjdCByb290JyxcbiAgICAgICAgZGVmYXVsdCA6ICcvbXkvcHJvamVjdC9wYXRoJyxcbiAgICAgICAgd2hlbiAgICA6IHdoZW4uY2hhbmdlUm9vdFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdHlwZSAgICA6ICdpbnB1dCcsXG4gICAgICAgIG5hbWUgICAgOiAnc2VtYW50aWNSb290JyxcbiAgICAgICAgbWVzc2FnZSA6ICdXaGVyZSBzaG91bGQgd2UgcHV0IFNlbWFudGljIFVJIGluc2lkZSB5b3VyIHByb2plY3Q/JyxcbiAgICAgICAgZGVmYXVsdCA6ICdzZW1hbnRpYy8nXG4gICAgICB9XG4gICAgXSxcblxuICAgIHNldHVwOiBbXG4gICAgICB7XG4gICAgICAgIHR5cGU6ICdsaXN0JyxcbiAgICAgICAgbmFtZTogJ292ZXJ3cml0ZScsXG4gICAgICAgIG1lc3NhZ2U6ICdJdCBsb29rcyBsaWtlIHlvdSBoYXZlIGEgc2VtYW50aWMuanNvbiBmaWxlIGFscmVhZHkuJyxcbiAgICAgICAgd2hlbjogd2hlbi5oYXNDb25maWcsXG4gICAgICAgIGNob2ljZXM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnWWVzLCBleHRlbmQgbXkgY3VycmVudCBzZXR0aW5ncy4nLFxuICAgICAgICAgICAgdmFsdWU6ICd5ZXMnXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnU2tpcCBpbnN0YWxsJyxcbiAgICAgICAgICAgIHZhbHVlOiAnbm8nXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0eXBlOiAnbGlzdCcsXG4gICAgICAgIG5hbWU6ICdpbnN0YWxsJyxcbiAgICAgICAgbWVzc2FnZTogJ1NldC11cCBTZW1hbnRpYyBVSScsXG4gICAgICAgIHdoZW46IHdoZW4uYWxsb3dPdmVyd3JpdGUsXG4gICAgICAgIGNob2ljZXM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnQXV0b21hdGljIChVc2UgZGVmYXVsdHMgbG9jYXRpb25zIGFuZCBhbGwgY29tcG9uZW50cyknLFxuICAgICAgICAgICAgdmFsdWU6ICdhdXRvJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ0V4cHJlc3MgKFNldCBjb21wb25lbnRzIGFuZCBvdXRwdXQgZm9sZGVyKScsXG4gICAgICAgICAgICB2YWx1ZTogJ2V4cHJlc3MnXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnQ3VzdG9tIChDdXN0b21pemUgYWxsIHNyYy9kaXN0IHZhbHVlcyknLFxuICAgICAgICAgICAgdmFsdWU6ICdjdXN0b20nXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0eXBlOiAnY2hlY2tib3gnLFxuICAgICAgICBuYW1lOiAnY29tcG9uZW50cycsXG4gICAgICAgIG1lc3NhZ2U6ICdXaGF0IGNvbXBvbmVudHMgc2hvdWxkIHdlIGluY2x1ZGUgaW4gdGhlIHBhY2thZ2U/JyxcblxuICAgICAgICAvLyBkdXBsaWNhdGVkIG1hbnVhbGx5IGZyb20gdGFza3MvZGVmYXVsdHMuanMgd2l0aCBhZGRpdGlvbmFsIHByb3BlcnR5XG4gICAgICAgIGNob2ljZXM6IFtcbiAgICAgICAgICB7IG5hbWU6IFwicmVzZXRcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJzaXRlXCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwiYnV0dG9uXCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwiY29udGFpbmVyXCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwiZGl2aWRlclwiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcImZsYWdcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJoZWFkZXJcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJpY29uXCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwiaW1hZ2VcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJpbnB1dFwiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcImxhYmVsXCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwibGlzdFwiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcImxvYWRlclwiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcInJhaWxcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJyZXZlYWxcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJzZWdtZW50XCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwic3RlcFwiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcImJyZWFkY3J1bWJcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJmb3JtXCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwiZ3JpZFwiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcIm1lbnVcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJtZXNzYWdlXCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwidGFibGVcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJhZFwiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcImNhcmRcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJjb21tZW50XCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwiZmVlZFwiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcIml0ZW1cIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJzdGF0aXN0aWNcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJhY2NvcmRpb25cIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJjaGVja2JveFwiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcImRpbW1lclwiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcImRyb3Bkb3duXCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwiZW1iZWRcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJtb2RhbFwiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcIm5hZ1wiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcInBvcHVwXCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwicHJvZ3Jlc3NcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJyYXRpbmdcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJzZWFyY2hcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJzaGFwZVwiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcInNpZGViYXJcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJzdGlja3lcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJ0YWJcIiwgY2hlY2tlZDogdHJ1ZSB9LFxuICAgICAgICAgIHsgbmFtZTogXCJ0cmFuc2l0aW9uXCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwiYXBpXCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwiZm9ybVwiLCBjaGVja2VkOiB0cnVlIH0sXG4gICAgICAgICAgeyBuYW1lOiBcInN0YXRlXCIsIGNoZWNrZWQ6IHRydWUgfSxcbiAgICAgICAgICB7IG5hbWU6IFwidmlzaWJpbGl0eVwiLCBjaGVja2VkOiB0cnVlIH1cbiAgICAgICAgXSxcbiAgICAgICAgd2hlbjogd2hlbi5ub3RBdXRvXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0eXBlOiAnbGlzdCcsXG4gICAgICAgIG5hbWU6ICdjaGFuZ2VQZXJtaXNzaW9ucycsXG4gICAgICAgIHdoZW46IHdoZW4ubm90QXV0byxcbiAgICAgICAgbWVzc2FnZTogJ1Nob3VsZCB3ZSBzZXQgcGVybWlzc2lvbnMgb24gb3V0cHV0dGVkIGZpbGVzPycsXG4gICAgICAgIGNob2ljZXM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnTm8nLFxuICAgICAgICAgICAgdmFsdWU6IGZhbHNlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnWWVzJyxcbiAgICAgICAgICAgIHZhbHVlOiB0cnVlXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0eXBlOiAnaW5wdXQnLFxuICAgICAgICBuYW1lOiAncGVybWlzc2lvbicsXG4gICAgICAgIG1lc3NhZ2U6ICdXaGF0IG9jdGFsIGZpbGUgcGVybWlzc2lvbiBzaG91bGQgb3V0cHV0dGVkIGZpbGVzIHJlY2VpdmU/JyxcbiAgICAgICAgZGVmYXVsdDogZGVmYXVsdHMucGVybWlzc2lvbixcbiAgICAgICAgd2hlbjogd2hlbi5jaGFuZ2VQZXJtaXNzaW9uc1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdHlwZTogJ2xpc3QnLFxuICAgICAgICBuYW1lOiAncnRsJyxcbiAgICAgICAgbWVzc2FnZTogJ0RvIHlvdSB1c2UgYSBSVEwgKFJpZ2h0LVRvLUxlZnQpIGxhbmd1YWdlPycsXG4gICAgICAgIHdoZW46IHdoZW4ubm90QXV0byxcbiAgICAgICAgY2hvaWNlczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdObycsXG4gICAgICAgICAgICB2YWx1ZTogZmFsc2VcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdZZXMnLFxuICAgICAgICAgICAgdmFsdWU6IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdCdWlsZCBCb3RoJyxcbiAgICAgICAgICAgIHZhbHVlOiAnYm90aCdcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHR5cGU6ICdpbnB1dCcsXG4gICAgICAgIG5hbWU6ICdkaXN0JyxcbiAgICAgICAgbWVzc2FnZTogJ1doZXJlIHNob3VsZCB3ZSBvdXRwdXQgU2VtYW50aWMgVUk/JyxcbiAgICAgICAgZGVmYXVsdDogZGVmYXVsdHMucGF0aHMub3V0cHV0LnBhY2thZ2VkLFxuICAgICAgICBmaWx0ZXI6IGZpbHRlci5yZW1vdmVUcmFpbGluZ1NsYXNoLFxuICAgICAgICB3aGVuOiB3aGVuLmV4cHJlc3NcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHR5cGU6ICdpbnB1dCcsXG4gICAgICAgIG5hbWU6ICdzaXRlJyxcbiAgICAgICAgbWVzc2FnZTogJ1doZXJlIHNob3VsZCB3ZSBwdXQgeW91ciBzaXRlIGZvbGRlcj8nLFxuICAgICAgICBkZWZhdWx0OiBkZWZhdWx0cy5wYXRocy5zb3VyY2Uuc2l0ZSxcbiAgICAgICAgZmlsdGVyOiBmaWx0ZXIucmVtb3ZlVHJhaWxpbmdTbGFzaCxcbiAgICAgICAgd2hlbjogd2hlbi5jdXN0b21cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHR5cGU6ICdpbnB1dCcsXG4gICAgICAgIG5hbWU6ICdwYWNrYWdlZCcsXG4gICAgICAgIG1lc3NhZ2U6ICdXaGVyZSBzaG91bGQgd2Ugb3V0cHV0IGEgcGFja2FnZWQgdmVyc2lvbj8nLFxuICAgICAgICBkZWZhdWx0OiBkZWZhdWx0cy5wYXRocy5vdXRwdXQucGFja2FnZWQsXG4gICAgICAgIGZpbHRlcjogZmlsdGVyLnJlbW92ZVRyYWlsaW5nU2xhc2gsXG4gICAgICAgIHdoZW46IHdoZW4uY3VzdG9tXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0eXBlOiAnaW5wdXQnLFxuICAgICAgICBuYW1lOiAnY29tcHJlc3NlZCcsXG4gICAgICAgIG1lc3NhZ2U6ICdXaGVyZSBzaG91bGQgd2Ugb3V0cHV0IGNvbXByZXNzZWQgY29tcG9uZW50cz8nLFxuICAgICAgICBkZWZhdWx0OiBkZWZhdWx0cy5wYXRocy5vdXRwdXQuY29tcHJlc3NlZCxcbiAgICAgICAgZmlsdGVyOiBmaWx0ZXIucmVtb3ZlVHJhaWxpbmdTbGFzaCxcbiAgICAgICAgd2hlbjogd2hlbi5jdXN0b21cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHR5cGU6ICdpbnB1dCcsXG4gICAgICAgIG5hbWU6ICd1bmNvbXByZXNzZWQnLFxuICAgICAgICBtZXNzYWdlOiAnV2hlcmUgc2hvdWxkIHdlIG91dHB1dCB1bmNvbXByZXNzZWQgY29tcG9uZW50cz8nLFxuICAgICAgICBkZWZhdWx0OiBkZWZhdWx0cy5wYXRocy5vdXRwdXQudW5jb21wcmVzc2VkLFxuICAgICAgICBmaWx0ZXI6IGZpbHRlci5yZW1vdmVUcmFpbGluZ1NsYXNoLFxuICAgICAgICB3aGVuOiB3aGVuLmN1c3RvbVxuICAgICAgfVxuICAgIF0sXG5cblxuICAgIGNsZWFudXA6IFtcbiAgICAgIHtcbiAgICAgICAgdHlwZTogJ2xpc3QnLFxuICAgICAgICBuYW1lOiAnY2xlYW51cCcsXG4gICAgICAgIG1lc3NhZ2U6ICdTaG91bGQgd2UgcmVtb3ZlIHNldC11cCBmaWxlcz8nLFxuICAgICAgICBjaG9pY2VzOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ1llcyAocmUtaW5zdGFsbCB3aWxsIHJlcXVpcmUgcmVkb3dubG9hZGluZyBzZW1hbnRpYykuJyxcbiAgICAgICAgICAgIHZhbHVlOiAneWVzJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ05vIFRoYW5rcycsXG4gICAgICAgICAgICB2YWx1ZTogJ25vJ1xuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdHlwZTogJ2xpc3QnLFxuICAgICAgICBuYW1lOiAnYnVpbGQnLFxuICAgICAgICBtZXNzYWdlOiAnRG8geW91IHdhbnQgdG8gYnVpbGQgU2VtYW50aWMgbm93PycsXG4gICAgICAgIGNob2ljZXM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnWWVzJyxcbiAgICAgICAgICAgIHZhbHVlOiAneWVzJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ05vJyxcbiAgICAgICAgICAgIHZhbHVlOiAnbm8nXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgIF0sXG4gICAgc2l0ZTogW1xuICAgICAge1xuICAgICAgICB0eXBlOiAnbGlzdCcsXG4gICAgICAgIG5hbWU6ICdjdXN0b21pemUnLFxuICAgICAgICBtZXNzYWdlOiAnWW91IGhhdmUgbm90IHlldCBjdXN0b21pemVkIHlvdXIgc2l0ZSwgY2FuIHdlIGhlbHAgeW91IGRvIHRoYXQ/JyxcbiAgICAgICAgY2hvaWNlczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdZZXMsIGFzayBtZSBhIGZldyBxdWVzdGlvbnMnLFxuICAgICAgICAgICAgdmFsdWU6IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdObyBJXFwnbGwgZG8gaXQgbXlzZWxmJyxcbiAgICAgICAgICAgIHZhbHVlOiBmYWxzZVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdHlwZTogJ2xpc3QnLFxuICAgICAgICBuYW1lOiAnaGVhZGVyRm9udCcsXG4gICAgICAgIG1lc3NhZ2U6ICdTZWxlY3QgeW91ciBoZWFkZXIgZm9udCcsXG4gICAgICAgIGNob2ljZXM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnSGVsdmV0aWNhIE5ldWUsIEFyaWFsLCBzYW5zLXNlcmlmJyxcbiAgICAgICAgICAgIHZhbHVlOiAnSGVsdmV0aWNhIE5ldWUsIEFyaWFsLCBzYW5zLXNlcmlmOydcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdMYXRvIChHb29nbGUgRm9udHMpJyxcbiAgICAgICAgICAgIHZhbHVlOiAnTGF0bydcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdPcGVuIFNhbnMgKEdvb2dsZSBGb250cyknLFxuICAgICAgICAgICAgdmFsdWU6ICdPcGVuIFNhbnMnXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnU291cmNlIFNhbnMgUHJvIChHb29nbGUgRm9udHMpJyxcbiAgICAgICAgICAgIHZhbHVlOiAnU291cmNlIFNhbnMgUHJvJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ0Ryb2lkIChHb29nbGUgRm9udHMpJyxcbiAgICAgICAgICAgIHZhbHVlOiAnRHJvaWQnXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnSVxcJ2xsIGNob29zZSBvbiBteSBvd24nLFxuICAgICAgICAgICAgdmFsdWU6IGZhbHNlXG4gICAgICAgICAgfVxuICAgICAgICBdLFxuICAgICAgICB3aGVuOiB3aGVuLmN1c3RvbWl6ZVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdHlwZTogJ2xpc3QnLFxuICAgICAgICBuYW1lOiAncGFnZUZvbnQnLFxuICAgICAgICBtZXNzYWdlOiAnU2VsZWN0IHlvdXIgcGFnZSBmb250JyxcbiAgICAgICAgY2hvaWNlczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdIZWx2ZXRpY2EgTmV1ZSwgQXJpYWwsIHNhbnMtc2VyaWYnLFxuICAgICAgICAgICAgdmFsdWU6ICdIZWx2ZXRpY2EgTmV1ZSwgQXJpYWwsIHNhbnMtc2VyaWY7J1xuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ0xhdG8gKEltcG9ydCBmcm9tIEdvb2dsZSBGb250cyknLFxuICAgICAgICAgICAgdmFsdWU6ICdMYXRvJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ09wZW4gU2FucyAoSW1wb3J0IGZyb20gR29vZ2xlIEZvbnRzKScsXG4gICAgICAgICAgICB2YWx1ZTogJ09wZW4gU2FucydcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdTb3VyY2UgU2FucyBQcm8gKEltcG9ydCBmcm9tIEdvb2dsZSBGb250cyknLFxuICAgICAgICAgICAgdmFsdWU6ICdTb3VyY2UgU2FucyBQcm8nXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnRHJvaWQgKEdvb2dsZSBGb250cyknLFxuICAgICAgICAgICAgdmFsdWU6ICdEcm9pZCdcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdJXFwnbGwgY2hvb3NlIG9uIG15IG93bicsXG4gICAgICAgICAgICB2YWx1ZTogZmFsc2VcbiAgICAgICAgICB9XG4gICAgICAgIF0sXG4gICAgICAgIHdoZW46IHdoZW4uY3VzdG9taXplXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0eXBlOiAnbGlzdCcsXG4gICAgICAgIG5hbWU6ICdmb250U2l6ZScsXG4gICAgICAgIG1lc3NhZ2U6ICdTZWxlY3QgeW91ciBiYXNlIGZvbnQgc2l6ZScsXG4gICAgICAgIGRlZmF1bHQ6ICcxNHB4JyxcbiAgICAgICAgY2hvaWNlczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICcxMnB4JyxcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICcxM3B4JyxcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICcxNHB4IChSZWNvbW1lbmRlZCknLFxuICAgICAgICAgICAgdmFsdWU6ICcxNHB4J1xuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJzE1cHgnLFxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJzE2cHgnLFxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ0lcXCdsbCBjaG9vc2Ugb24gbXkgb3duJyxcbiAgICAgICAgICAgIHZhbHVlOiBmYWxzZVxuICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgd2hlbjogd2hlbi5jdXN0b21pemVcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHR5cGU6ICdsaXN0JyxcbiAgICAgICAgbmFtZTogJ3ByaW1hcnlDb2xvcicsXG4gICAgICAgIG1lc3NhZ2U6ICdTZWxlY3QgdGhlIGNsb3Nlc3QgbmFtZSBmb3IgeW91ciBwcmltYXJ5IGJyYW5kIGNvbG9yJyxcbiAgICAgICAgZGVmYXVsdDogJzE0cHgnLFxuICAgICAgICBjaG9pY2VzOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ0JsdWUnXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnR3JlZW4nXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnT3JhbmdlJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ1BpbmsnXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnUHVycGxlJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ1JlZCdcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdUZWFsJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ1llbGxvdydcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdCbGFjaydcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdJXFwnbGwgY2hvb3NlIG9uIG15IG93bicsXG4gICAgICAgICAgICB2YWx1ZTogZmFsc2VcbiAgICAgICAgICB9XG4gICAgICAgIF0sXG4gICAgICAgIHdoZW46IHdoZW4uY3VzdG9taXplXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0eXBlOiAnaW5wdXQnLFxuICAgICAgICBuYW1lOiAnUHJpbWFyeUhleCcsXG4gICAgICAgIG1lc3NhZ2U6ICdFbnRlciBhIGhleGNvZGUgZm9yIHlvdXIgcHJpbWFyeSBicmFuZCBjb2xvcicsXG4gICAgICAgIHdoZW46IHdoZW4ucHJpbWFyeUNvbG9yXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0eXBlOiAnbGlzdCcsXG4gICAgICAgIG5hbWU6ICdzZWNvbmRhcnlDb2xvcicsXG4gICAgICAgIG1lc3NhZ2U6ICdTZWxlY3QgdGhlIGNsb3Nlc3QgbmFtZSBmb3IgeW91ciBzZWNvbmRhcnkgYnJhbmQgY29sb3InLFxuICAgICAgICBkZWZhdWx0OiAnMTRweCcsXG4gICAgICAgIGNob2ljZXM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnQmx1ZSdcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdHcmVlbidcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdPcmFuZ2UnXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnUGluaydcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdQdXJwbGUnXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnUmVkJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ1RlYWwnXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnWWVsbG93J1xuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ0JsYWNrJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbmFtZTogJ0lcXCdsbCBjaG9vc2Ugb24gbXkgb3duJyxcbiAgICAgICAgICAgIHZhbHVlOiBmYWxzZVxuICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgd2hlbjogd2hlbi5jdXN0b21pemVcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHR5cGU6ICdpbnB1dCcsXG4gICAgICAgIG5hbWU6ICdzZWNvbmRhcnlIZXgnLFxuICAgICAgICBtZXNzYWdlOiAnRW50ZXIgYSBoZXhjb2RlIGZvciB5b3VyIHNlY29uZGFyeSBicmFuZCBjb2xvcicsXG4gICAgICAgIHdoZW46IHdoZW4uc2Vjb25kYXJ5Q29sb3JcbiAgICAgIH1cbiAgICBdXG5cbiAgfSxcblxuICBzZXR0aW5nczoge1xuXG4gICAgLyogUmVuYW1lIEZpbGVzICovXG4gICAgcmVuYW1lOiB7XG4gICAgICBqc29uIDogeyBleHRuYW1lIDogJy5qc29uJyB9XG4gICAgfSxcblxuICAgIC8qIENvcHkgSW5zdGFsbCBGb2xkZXJzICovXG4gICAgd3JlbmNoOiB7XG5cbiAgICAgIC8vIG92ZXJ3cml0ZSBleGlzdGluZyBmaWxlcyB1cGRhdGUgJiBpbnN0YWxsIChkZWZhdWx0IHRoZW1lIC8gZGVmaW5pdGlvbilcbiAgICAgIG92ZXJ3cml0ZToge1xuICAgICAgICBmb3JjZURlbGV0ZSAgICAgICA6IHRydWUsXG4gICAgICAgIGV4Y2x1ZGVIaWRkZW5Vbml4IDogdHJ1ZSxcbiAgICAgICAgcHJlc2VydmVGaWxlcyAgICAgOiBmYWxzZVxuICAgICAgfSxcblxuICAgICAgLy8gb25seSBjcmVhdGUgZmlsZXMgdGhhdCBkb24ndCBleGlzdCAoc2l0ZSB0aGVtZSB1cGRhdGUpXG4gICAgICBtZXJnZToge1xuICAgICAgICBmb3JjZURlbGV0ZSAgICAgICA6IGZhbHNlLFxuICAgICAgICBleGNsdWRlSGlkZGVuVW5peCA6IHRydWUsXG4gICAgICAgIHByZXNlcnZlRmlsZXMgICAgIDogdHJ1ZVxuICAgICAgfVxuXG4gICAgfVxuICB9XG59O1xuIl19