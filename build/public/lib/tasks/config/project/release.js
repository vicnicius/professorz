/*******************************
         Release Config
*******************************/

var requireDotFile = require('require-dot-file'),
    config,
    npmPackage,
    version;

/*******************************
         Derived Values
*******************************/

try {
  config = requireDotFile('semantic.json');
} catch (error) {}

try {
  npmPackage = require('../../../package.json');
} catch (error) {
  // generate fake package
  npmPackage = {
    name: 'Unknown',
    version: 'x.x'
  };
}

// looks for version in config or package.json (whichever is available)
version = npmPackage && npmPackage.version !== undefined && npmPackage.name == 'semantic-ui' ? npmPackage.version : config.version;

/*******************************
             Export
*******************************/

module.exports = {

  title: 'Semantic UI',
  repository: 'https://github.com/Semantic-Org/Semantic-UI',
  url: 'http://www.semantic-ui.com/',

  banner: '' + ' /*' + '\n' + ' * # <%= title %> - <%= version %>' + '\n' + ' * <%= repository %>' + '\n' + ' * <%= url %>' + '\n' + ' *' + '\n' + ' * Copyright 2014 Contributors' + '\n' + ' * Released under the MIT license' + '\n' + ' * http://opensource.org/licenses/MIT' + '\n' + ' *' + '\n' + ' */' + '\n',

  version: version

};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2NvbmZpZy9wcm9qZWN0L3JlbGVhc2UuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUlBLElBQ0UsaUJBQWlCLFFBQVEsa0JBQVIsQ0FEbkI7QUFBQSxJQUVFLE1BRkY7QUFBQSxJQUdFLFVBSEY7QUFBQSxJQUlFLE9BSkY7Ozs7OztBQVlBLElBQUk7QUFDRixXQUFTLGVBQWUsZUFBZixDQUFUO0FBQ0QsQ0FGRCxDQUdBLE9BQU0sS0FBTixFQUFhLENBQUU7O0FBR2YsSUFBSTtBQUNGLGVBQWEsUUFBUSx1QkFBUixDQUFiO0FBQ0QsQ0FGRCxDQUdBLE9BQU0sS0FBTixFQUFhOztBQUVYLGVBQWE7QUFDWCxVQUFNLFNBREs7QUFFWCxhQUFTO0FBRkUsR0FBYjtBQUlEOzs7QUFHRCxVQUFXLGNBQWMsV0FBVyxPQUFYLEtBQXVCLFNBQXJDLElBQWtELFdBQVcsSUFBWCxJQUFtQixhQUF0RSxHQUNOLFdBQVcsT0FETCxHQUVOLE9BQU8sT0FGWDs7Ozs7O0FBVUEsT0FBTyxPQUFQLEdBQWlCOztBQUVmLFNBQWEsYUFGRTtBQUdmLGNBQWEsNkNBSEU7QUFJZixPQUFhLDZCQUpFOztBQU1mLFVBQVEsS0FDSixLQURJLEdBQ0ksSUFESixHQUVKLG9DQUZJLEdBRW1DLElBRm5DLEdBR0osc0JBSEksR0FHcUIsSUFIckIsR0FJSixlQUpJLEdBSWMsSUFKZCxHQUtKLElBTEksR0FLRyxJQUxILEdBTUosZ0NBTkksR0FNK0IsSUFOL0IsR0FPSixtQ0FQSSxHQU9rQyxJQVBsQyxHQVFKLHVDQVJJLEdBUXNDLElBUnRDLEdBU0osSUFUSSxHQVNHLElBVEgsR0FVSixLQVZJLEdBVUksSUFoQkc7O0FBa0JmLFdBQWE7O0FBbEJFLENBQWpCIiwiZmlsZSI6InJlbGVhc2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgUmVsZWFzZSBDb25maWdcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbnZhclxuICByZXF1aXJlRG90RmlsZSA9IHJlcXVpcmUoJ3JlcXVpcmUtZG90LWZpbGUnKSxcbiAgY29uZmlnLFxuICBucG1QYWNrYWdlLFxuICB2ZXJzaW9uXG47XG5cblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAgICAgIERlcml2ZWQgVmFsdWVzXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG50cnkge1xuICBjb25maWcgPSByZXF1aXJlRG90RmlsZSgnc2VtYW50aWMuanNvbicpO1xufVxuY2F0Y2goZXJyb3IpIHt9XG5cblxudHJ5IHtcbiAgbnBtUGFja2FnZSA9IHJlcXVpcmUoJy4uLy4uLy4uL3BhY2thZ2UuanNvbicpO1xufVxuY2F0Y2goZXJyb3IpIHtcbiAgLy8gZ2VuZXJhdGUgZmFrZSBwYWNrYWdlXG4gIG5wbVBhY2thZ2UgPSB7XG4gICAgbmFtZTogJ1Vua25vd24nLFxuICAgIHZlcnNpb246ICd4LngnXG4gIH07XG59XG5cbi8vIGxvb2tzIGZvciB2ZXJzaW9uIGluIGNvbmZpZyBvciBwYWNrYWdlLmpzb24gKHdoaWNoZXZlciBpcyBhdmFpbGFibGUpXG52ZXJzaW9uID0gKG5wbVBhY2thZ2UgJiYgbnBtUGFja2FnZS52ZXJzaW9uICE9PSB1bmRlZmluZWQgJiYgbnBtUGFja2FnZS5uYW1lID09ICdzZW1hbnRpYy11aScpXG4gID8gbnBtUGFja2FnZS52ZXJzaW9uXG4gIDogY29uZmlnLnZlcnNpb25cbjtcblxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgICAgIEV4cG9ydFxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxubW9kdWxlLmV4cG9ydHMgPSB7XG5cbiAgdGl0bGUgICAgICA6ICdTZW1hbnRpYyBVSScsXG4gIHJlcG9zaXRvcnkgOiAnaHR0cHM6Ly9naXRodWIuY29tL1NlbWFudGljLU9yZy9TZW1hbnRpYy1VSScsXG4gIHVybCAgICAgICAgOiAnaHR0cDovL3d3dy5zZW1hbnRpYy11aS5jb20vJyxcblxuICBiYW5uZXI6ICcnXG4gICAgKyAnIC8qJyArICdcXG4nXG4gICAgKyAnICogIyA8JT0gdGl0bGUgJT4gLSA8JT0gdmVyc2lvbiAlPicgKyAnXFxuJ1xuICAgICsgJyAqIDwlPSByZXBvc2l0b3J5ICU+JyArICdcXG4nXG4gICAgKyAnICogPCU9IHVybCAlPicgKyAnXFxuJ1xuICAgICsgJyAqJyArICdcXG4nXG4gICAgKyAnICogQ29weXJpZ2h0IDIwMTQgQ29udHJpYnV0b3JzJyArICdcXG4nXG4gICAgKyAnICogUmVsZWFzZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlJyArICdcXG4nXG4gICAgKyAnICogaHR0cDovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVCcgKyAnXFxuJ1xuICAgICsgJyAqJyArICdcXG4nXG4gICAgKyAnICovJyArICdcXG4nLFxuXG4gIHZlcnNpb24gICAgOiB2ZXJzaW9uXG5cbn07XG4iXX0=