/*******************************
         Install Task
*******************************/

/*
   Install tasks

   For more notes

   * Runs automatically after npm update (hooks)
   * (NPM) Install - Will ask for where to put semantic (outside pm folder)
   * (NPM) Upgrade - Will look for semantic install, copy over files and update if new version
   * Standard installer runs asking for paths to site files etc

*/

var gulp = require('gulp'),


// node dependencies
console = require('better-console'),
    extend = require('extend'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    runSequence = require('run-sequence'),


// gulp dependencies
chmod = require('gulp-chmod'),
    del = require('del'),
    jsonEditor = require('gulp-json-editor'),
    plumber = require('gulp-plumber'),
    prompt = require('gulp-prompt'),
    rename = require('gulp-rename'),
    replace = require('gulp-replace'),
    requireDotFile = require('require-dot-file'),
    wrench = require('wrench'),


// install config
install = require('./config/project/install'),


// user config
config = require('./config/user'),


// release config (name/title/etc)
release = require('./config/project/release'),


// shorthand
questions = install.questions,
    files = install.files,
    folders = install.folders,
    regExp = install.regExp,
    settings = install.settings,
    source = install.source;

// Export install task
module.exports = function (callback) {

  var currentConfig = requireDotFile('semantic.json'),
      manager = install.getPackageManager(),
      rootQuestions = questions.root,
      installFolder = false,
      answers;

  console.clear();

  /* Test NPM install
  manager = {
    name : 'NPM',
    root : path.normalize(__dirname + '/../')
  };
  */

  /* Don't do end user config if SUI is a sub-module */
  if (install.isSubModule()) {
    console.info('SUI is a sub-module, skipping end-user install');
    return;
  }

  /*-----------------
      Update SUI
  -----------------*/

  // run update scripts if semantic.json exists
  if (currentConfig && manager.name === 'NPM') {

    var updateFolder = path.join(manager.root, currentConfig.base),
        updatePaths = {
      config: path.join(manager.root, files.config),
      tasks: path.join(updateFolder, folders.tasks),
      themeImport: path.join(updateFolder, folders.themeImport),
      definition: path.join(currentConfig.paths.source.definitions),
      site: path.join(currentConfig.paths.source.site),
      theme: path.join(currentConfig.paths.source.themes),
      defaultTheme: path.join(currentConfig.paths.source.themes, folders.defaultTheme)
    };

    // duck-type if there is a project installed
    if (fs.existsSync(updatePaths.definition)) {

      // perform update if new version
      if (currentConfig.version !== release.version) {
        console.log('Updating Semantic UI from ' + currentConfig.version + ' to ' + release.version);

        console.info('Updating ui definitions...');
        wrench.copyDirSyncRecursive(source.definitions, updatePaths.definition, settings.wrench.overwrite);

        console.info('Updating default theme...');
        wrench.copyDirSyncRecursive(source.themes, updatePaths.theme, settings.wrench.merge);
        wrench.copyDirSyncRecursive(source.defaultTheme, updatePaths.defaultTheme, settings.wrench.overwrite);

        console.info('Updating tasks...');
        wrench.copyDirSyncRecursive(source.tasks, updatePaths.tasks, settings.wrench.overwrite);

        console.info('Updating gulpfile.js');
        gulp.src(source.userGulpFile).pipe(plumber()).pipe(gulp.dest(updateFolder));

        // copy theme import
        console.info('Updating theme import file');
        gulp.src(source.themeImport).pipe(plumber()).pipe(gulp.dest(updatePaths.themeImport));

        console.info('Adding new site theme files...');
        wrench.copyDirSyncRecursive(source.site, updatePaths.site, settings.wrench.merge);

        console.info('Updating version...');

        // update version number in semantic.json
        gulp.src(updatePaths.config).pipe(plumber()).pipe(rename(settings.rename.json)) // preserve file extension
        .pipe(jsonEditor({
          version: release.version
        })).pipe(gulp.dest(manager.root));

        console.info('Update complete! Run "\x1b[92mgulp build\x1b[0m" to rebuild dist/ files.');

        return;
      } else {
        console.log('Current version of Semantic UI already installed');
        return;
      }
    } else {
      console.error('Cannot locate files to update at path: ', updatePaths.definition);
      console.log('Running installer');
    }
  }

  /*--------------
   Determine Root
  ---------------*/

  // PM that supports Build Tools (NPM Only Now)
  if (manager.name == 'NPM') {
    rootQuestions[0].message = rootQuestions[0].message.replace('{packageMessage}', 'We detected you are using \x1b[92m' + manager.name + '\x1b[0m. Nice! ').replace('{root}', manager.root);
    // set default path to detected PM root
    rootQuestions[0].default = manager.root;
    rootQuestions[1].default = manager.root;

    // insert PM questions after "Install Type" question
    Array.prototype.splice.apply(questions.setup, [2, 0].concat(rootQuestions));

    // omit cleanup questions for managed install
    questions.cleanup = [];
  }

  /*--------------
     Create SUI
  ---------------*/

  gulp.task('run setup', function () {

    // If auto-install is switched on, we skip the configuration section and simply reuse the configuration from semantic.json
    if (install.shouldAutoInstall()) {
      answers = {
        overwrite: 'yes',
        install: 'auto'
      };
    } else {
      return gulp.src('gulpfile.js').pipe(prompt.prompt(questions.setup, function (setupAnswers) {
        // hoist
        answers = setupAnswers;
      }));
    }
  });

  gulp.task('create install files', function (callback) {

    /*--------------
     Exit Conditions
    ---------------*/

    // if config exists and user specifies not to proceed
    if (answers.overwrite !== undefined && answers.overwrite == 'no') {
      return;
    }
    console.clear();
    if (install.shouldAutoInstall()) {
      console.log('Auto-Installing (Without User Interaction)');
    } else {
      console.log('Installing');
    }
    console.log('------------------------------');

    /*--------------
          Paths
    ---------------*/

    var installPaths = {
      config: files.config,
      configFolder: folders.config,
      site: answers.site || folders.site,
      themeConfig: files.themeConfig,
      themeConfigFolder: folders.themeConfig
    };

    /*--------------
      NPM Install
    ---------------*/

    // Check if PM install
    if (answers.useRoot || answers.customRoot) {

      // Set root to custom root path if set
      if (answers.customRoot) {
        if (answers.customRoot === '') {
          console.log('Unable to proceed, invalid project root');
          return;
        }
        manager.root = answers.customRoot;
      }

      // special install paths only for PM install
      installPaths = extend(false, {}, installPaths, {
        definition: folders.definitions,
        lessImport: folders.lessImport,
        tasks: folders.tasks,
        theme: folders.themes,
        defaultTheme: path.join(folders.themes, folders.defaultTheme),
        themeImport: folders.themeImport
      });

      // add project root to semantic root
      installFolder = path.join(manager.root, answers.semanticRoot);

      // add install folder to all output paths
      for (var destination in installPaths) {
        if (installPaths.hasOwnProperty(destination)) {
          // config goes in project root, rest in install folder
          installPaths[destination] = destination == 'config' || destination == 'configFolder' ? path.normalize(path.join(manager.root, installPaths[destination])) : path.normalize(path.join(installFolder, installPaths[destination]));
        }
      }

      // create project folders
      try {
        mkdirp.sync(installFolder);
        mkdirp.sync(installPaths.definition);
        mkdirp.sync(installPaths.theme);
        mkdirp.sync(installPaths.tasks);
      } catch (error) {
        console.error('NPM does not have permissions to create folders at your specified path. Adjust your folders permissions and run "npm install" again');
      }

      console.log('Installing to \x1b[92m' + answers.semanticRoot + '\x1b[0m');

      console.info('Copying UI definitions');
      wrench.copyDirSyncRecursive(source.definitions, installPaths.definition, settings.wrench.overwrite);

      console.info('Copying UI themes');
      wrench.copyDirSyncRecursive(source.themes, installPaths.theme, settings.wrench.merge);
      wrench.copyDirSyncRecursive(source.defaultTheme, installPaths.defaultTheme, settings.wrench.overwrite);

      console.info('Copying gulp tasks');
      wrench.copyDirSyncRecursive(source.tasks, installPaths.tasks, settings.wrench.overwrite);

      // copy theme import
      console.info('Adding theme files');
      gulp.src(source.themeImport).pipe(plumber()).pipe(gulp.dest(installPaths.themeImport));
      gulp.src(source.lessImport).pipe(plumber()).pipe(gulp.dest(installPaths.lessImport));

      // create gulp file
      console.info('Creating gulpfile.js');
      gulp.src(source.userGulpFile).pipe(plumber()).pipe(gulp.dest(installFolder));
    }

    /*--------------
       Site Theme
    ---------------*/

    // Copy _site templates folder to destination
    if (fs.existsSync(installPaths.site)) {
      console.info('Site folder exists, merging files (no overwrite)', installPaths.site);
    } else {
      console.info('Creating site theme folder', installPaths.site);
    }
    wrench.copyDirSyncRecursive(source.site, installPaths.site, settings.wrench.merge);

    /*--------------
      Theme Config
    ---------------*/

    gulp.task('create theme.config', function () {
      var
      // determine path to site theme folder from theme config
      // force CSS path variable to use forward slashes for paths
      pathToSite = path.relative(path.resolve(installPaths.themeConfigFolder), path.resolve(installPaths.site)).replace(/\\/g, '/'),
          siteVariable = "@siteFolder   : '" + pathToSite + "/';";

      // rewrite site variable in theme.less
      console.info('Adjusting @siteFolder to: ', pathToSite + '/');

      if (fs.existsSync(installPaths.themeConfig)) {
        console.info('Modifying src/theme.config (LESS config)', installPaths.themeConfig);
        return gulp.src(installPaths.themeConfig).pipe(plumber()).pipe(replace(regExp.siteVariable, siteVariable)).pipe(gulp.dest(installPaths.themeConfigFolder));
      } else {
        console.info('Creating src/theme.config (LESS config)', installPaths.themeConfig);
        return gulp.src(source.themeConfig).pipe(plumber()).pipe(rename({ extname: '' })).pipe(replace(regExp.siteVariable, siteVariable)).pipe(gulp.dest(installPaths.themeConfigFolder));
      }
    });

    /*--------------
      Semantic.json
    ---------------*/

    gulp.task('create semantic.json', function () {

      var jsonConfig = install.createJSON(answers);

      // adjust variables in theme.less
      if (fs.existsSync(files.config)) {
        console.info('Extending config file (semantic.json)', installPaths.config);
        return gulp.src(installPaths.config).pipe(plumber()).pipe(rename(settings.rename.json)) // preserve file extension
        .pipe(jsonEditor(jsonConfig)).pipe(gulp.dest(installPaths.configFolder));
      } else {
        console.info('Creating config file (semantic.json)', installPaths.config);
        return gulp.src(source.config).pipe(plumber()).pipe(rename({ extname: '' })) // remove .template from ext
        .pipe(jsonEditor(jsonConfig)).pipe(gulp.dest(installPaths.configFolder));
      }
    });

    runSequence('create theme.config', 'create semantic.json', callback);
  });

  gulp.task('clean up install', function () {

    // Completion Message
    if (installFolder) {
      console.log('\n Setup Complete! \n Installing Peer Dependencies. \x1b[0;31mPlease refrain from ctrl + c\x1b[0m... \n After completion navigate to \x1b[92m' + answers.semanticRoot + '\x1b[0m and run "\x1b[92mgulp build\x1b[0m" to build');
      process.exit(0);
    } else {
      console.log('');
      console.log('');
    }

    // If auto-install is switched on, we skip the configuration section and simply build the dependencies
    if (install.shouldAutoInstall()) {
      return gulp.start('build');
    } else {
      return gulp.src('gulpfile.js').pipe(prompt.prompt(questions.cleanup, function (answers) {
        if (answers.cleanup == 'yes') {
          del(install.setupFiles);
        }
        if (answers.build == 'yes') {
          gulp.start('build');
        }
      }));
    }
  });

  runSequence('run setup', 'create install files', 'clean up install', callback);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3Rhc2tzL2luc3RhbGwuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQSxJQUNFLE9BQWlCLFFBQVEsTUFBUixDQURuQjtBQUFBOzs7QUFJRSxVQUFpQixRQUFRLGdCQUFSLENBSm5CO0FBQUEsSUFLRSxTQUFpQixRQUFRLFFBQVIsQ0FMbkI7QUFBQSxJQU1FLEtBQWlCLFFBQVEsSUFBUixDQU5uQjtBQUFBLElBT0UsU0FBaUIsUUFBUSxRQUFSLENBUG5CO0FBQUEsSUFRRSxPQUFpQixRQUFRLE1BQVIsQ0FSbkI7QUFBQSxJQVNFLGNBQWlCLFFBQVEsY0FBUixDQVRuQjtBQUFBOzs7QUFZRSxRQUFpQixRQUFRLFlBQVIsQ0FabkI7QUFBQSxJQWFFLE1BQWlCLFFBQVEsS0FBUixDQWJuQjtBQUFBLElBY0UsYUFBaUIsUUFBUSxrQkFBUixDQWRuQjtBQUFBLElBZUUsVUFBaUIsUUFBUSxjQUFSLENBZm5CO0FBQUEsSUFnQkUsU0FBaUIsUUFBUSxhQUFSLENBaEJuQjtBQUFBLElBaUJFLFNBQWlCLFFBQVEsYUFBUixDQWpCbkI7QUFBQSxJQWtCRSxVQUFpQixRQUFRLGNBQVIsQ0FsQm5CO0FBQUEsSUFtQkUsaUJBQWlCLFFBQVEsa0JBQVIsQ0FuQm5CO0FBQUEsSUFvQkUsU0FBaUIsUUFBUSxRQUFSLENBcEJuQjtBQUFBOzs7QUF1QkUsVUFBaUIsUUFBUSwwQkFBUixDQXZCbkI7QUFBQTs7O0FBMEJFLFNBQWlCLFFBQVEsZUFBUixDQTFCbkI7QUFBQTs7O0FBNkJFLFVBQWlCLFFBQVEsMEJBQVIsQ0E3Qm5CO0FBQUE7OztBQWdDRSxZQUFpQixRQUFRLFNBaEMzQjtBQUFBLElBaUNFLFFBQWlCLFFBQVEsS0FqQzNCO0FBQUEsSUFrQ0UsVUFBaUIsUUFBUSxPQWxDM0I7QUFBQSxJQW1DRSxTQUFpQixRQUFRLE1BbkMzQjtBQUFBLElBb0NFLFdBQWlCLFFBQVEsUUFwQzNCO0FBQUEsSUFxQ0UsU0FBaUIsUUFBUSxNQXJDM0I7OztBQXlDQSxPQUFPLE9BQVAsR0FBaUIsVUFBVSxRQUFWLEVBQW9COztBQUVyQyxNQUNFLGdCQUFnQixlQUFlLGVBQWYsQ0FEbEI7QUFBQSxNQUVFLFVBQWdCLFFBQVEsaUJBQVIsRUFGbEI7QUFBQSxNQUdFLGdCQUFnQixVQUFVLElBSDVCO0FBQUEsTUFJRSxnQkFBZ0IsS0FKbEI7QUFBQSxNQUtFLE9BTEY7O0FBUUEsVUFBUSxLQUFSOzs7Ozs7Ozs7O0FBV0EsTUFBSSxRQUFRLFdBQVIsRUFBSixFQUE0QjtBQUMxQixZQUFRLElBQVIsQ0FBYSxnREFBYjtBQUNBO0FBQ0Q7Ozs7Ozs7QUFPRCxNQUFHLGlCQUFpQixRQUFRLElBQVIsS0FBaUIsS0FBckMsRUFBNEM7O0FBRTFDLFFBQ0UsZUFBZSxLQUFLLElBQUwsQ0FBVSxRQUFRLElBQWxCLEVBQXdCLGNBQWMsSUFBdEMsQ0FEakI7QUFBQSxRQUVFLGNBQWU7QUFDYixjQUFlLEtBQUssSUFBTCxDQUFVLFFBQVEsSUFBbEIsRUFBd0IsTUFBTSxNQUE5QixDQURGO0FBRWIsYUFBZSxLQUFLLElBQUwsQ0FBVSxZQUFWLEVBQXdCLFFBQVEsS0FBaEMsQ0FGRjtBQUdiLG1CQUFlLEtBQUssSUFBTCxDQUFVLFlBQVYsRUFBd0IsUUFBUSxXQUFoQyxDQUhGO0FBSWIsa0JBQWUsS0FBSyxJQUFMLENBQVUsY0FBYyxLQUFkLENBQW9CLE1BQXBCLENBQTJCLFdBQXJDLENBSkY7QUFLYixZQUFlLEtBQUssSUFBTCxDQUFVLGNBQWMsS0FBZCxDQUFvQixNQUFwQixDQUEyQixJQUFyQyxDQUxGO0FBTWIsYUFBZSxLQUFLLElBQUwsQ0FBVSxjQUFjLEtBQWQsQ0FBb0IsTUFBcEIsQ0FBMkIsTUFBckMsQ0FORjtBQU9iLG9CQUFlLEtBQUssSUFBTCxDQUFVLGNBQWMsS0FBZCxDQUFvQixNQUFwQixDQUEyQixNQUFyQyxFQUE2QyxRQUFRLFlBQXJEO0FBUEYsS0FGakI7OztBQWNBLFFBQUksR0FBRyxVQUFILENBQWMsWUFBWSxVQUExQixDQUFKLEVBQTRDOzs7QUFHMUMsVUFBRyxjQUFjLE9BQWQsS0FBMEIsUUFBUSxPQUFyQyxFQUE4QztBQUM1QyxnQkFBUSxHQUFSLENBQVksK0JBQStCLGNBQWMsT0FBN0MsR0FBdUQsTUFBdkQsR0FBZ0UsUUFBUSxPQUFwRjs7QUFFQSxnQkFBUSxJQUFSLENBQWEsNEJBQWI7QUFDQSxlQUFPLG9CQUFQLENBQTRCLE9BQU8sV0FBbkMsRUFBZ0QsWUFBWSxVQUE1RCxFQUF3RSxTQUFTLE1BQVQsQ0FBZ0IsU0FBeEY7O0FBRUEsZ0JBQVEsSUFBUixDQUFhLDJCQUFiO0FBQ0EsZUFBTyxvQkFBUCxDQUE0QixPQUFPLE1BQW5DLEVBQTJDLFlBQVksS0FBdkQsRUFBOEQsU0FBUyxNQUFULENBQWdCLEtBQTlFO0FBQ0EsZUFBTyxvQkFBUCxDQUE0QixPQUFPLFlBQW5DLEVBQWlELFlBQVksWUFBN0QsRUFBMkUsU0FBUyxNQUFULENBQWdCLFNBQTNGOztBQUVBLGdCQUFRLElBQVIsQ0FBYSxtQkFBYjtBQUNBLGVBQU8sb0JBQVAsQ0FBNEIsT0FBTyxLQUFuQyxFQUEwQyxZQUFZLEtBQXRELEVBQTZELFNBQVMsTUFBVCxDQUFnQixTQUE3RTs7QUFFQSxnQkFBUSxJQUFSLENBQWEsc0JBQWI7QUFDQSxhQUFLLEdBQUwsQ0FBUyxPQUFPLFlBQWhCLEVBQ0csSUFESCxDQUNRLFNBRFIsRUFFRyxJQUZILENBRVEsS0FBSyxJQUFMLENBQVUsWUFBVixDQUZSOzs7QUFNQSxnQkFBUSxJQUFSLENBQWEsNEJBQWI7QUFDQSxhQUFLLEdBQUwsQ0FBUyxPQUFPLFdBQWhCLEVBQ0csSUFESCxDQUNRLFNBRFIsRUFFRyxJQUZILENBRVEsS0FBSyxJQUFMLENBQVUsWUFBWSxXQUF0QixDQUZSOztBQUtBLGdCQUFRLElBQVIsQ0FBYSxnQ0FBYjtBQUNBLGVBQU8sb0JBQVAsQ0FBNEIsT0FBTyxJQUFuQyxFQUF5QyxZQUFZLElBQXJELEVBQTJELFNBQVMsTUFBVCxDQUFnQixLQUEzRTs7QUFFQSxnQkFBUSxJQUFSLENBQWEscUJBQWI7OztBQUdBLGFBQUssR0FBTCxDQUFTLFlBQVksTUFBckIsRUFDRyxJQURILENBQ1EsU0FEUixFQUVHLElBRkgsQ0FFUSxPQUFPLFNBQVMsTUFBVCxDQUFnQixJQUF2QixDQUZSLEM7QUFBQSxTQUdHLElBSEgsQ0FHUSxXQUFXO0FBQ2YsbUJBQVMsUUFBUTtBQURGLFNBQVgsQ0FIUixFQU1HLElBTkgsQ0FNUSxLQUFLLElBQUwsQ0FBVSxRQUFRLElBQWxCLENBTlI7O0FBU0EsZ0JBQVEsSUFBUixDQUFhLDBFQUFiOztBQUVBO0FBQ0QsT0E1Q0QsTUE2Q0s7QUFDSCxnQkFBUSxHQUFSLENBQVksa0RBQVo7QUFDQTtBQUNEO0FBRUYsS0FyREQsTUFzREs7QUFDSCxjQUFRLEtBQVIsQ0FBYyx5Q0FBZCxFQUF5RCxZQUFZLFVBQXJFO0FBQ0EsY0FBUSxHQUFSLENBQVksbUJBQVo7QUFDRDtBQUVGOzs7Ozs7O0FBT0QsTUFBRyxRQUFRLElBQVIsSUFBZ0IsS0FBbkIsRUFBMEI7QUFDeEIsa0JBQWMsQ0FBZCxFQUFpQixPQUFqQixHQUEyQixjQUFjLENBQWQsRUFBaUIsT0FBakIsQ0FDeEIsT0FEd0IsQ0FDaEIsa0JBRGdCLEVBQ0ksdUNBQXVDLFFBQVEsSUFBL0MsR0FBc0QsaUJBRDFELEVBRXhCLE9BRndCLENBRWhCLFFBRmdCLEVBRU4sUUFBUSxJQUZGLENBQTNCOztBQUtBLGtCQUFjLENBQWQsRUFBaUIsT0FBakIsR0FBMkIsUUFBUSxJQUFuQztBQUNBLGtCQUFjLENBQWQsRUFBaUIsT0FBakIsR0FBMkIsUUFBUSxJQUFuQzs7O0FBR0EsVUFBTSxTQUFOLENBQWdCLE1BQWhCLENBQXVCLEtBQXZCLENBQTZCLFVBQVUsS0FBdkMsRUFBOEMsQ0FBQyxDQUFELEVBQUksQ0FBSixFQUFPLE1BQVAsQ0FBYyxhQUFkLENBQTlDOzs7QUFHQSxjQUFVLE9BQVYsR0FBb0IsRUFBcEI7QUFDRDs7Ozs7O0FBT0QsT0FBSyxJQUFMLENBQVUsV0FBVixFQUF1QixZQUFXOzs7QUFHaEMsUUFBRyxRQUFRLGlCQUFSLEVBQUgsRUFBZ0M7QUFDOUIsZ0JBQVU7QUFDUixtQkFBWSxLQURKO0FBRVIsaUJBQVk7QUFGSixPQUFWO0FBSUQsS0FMRCxNQU1LO0FBQ0gsYUFBTyxLQUNKLEdBREksQ0FDQSxhQURBLEVBRUosSUFGSSxDQUVDLE9BQU8sTUFBUCxDQUFjLFVBQVUsS0FBeEIsRUFBK0IsVUFBUyxZQUFULEVBQXVCOztBQUUxRCxrQkFBVSxZQUFWO0FBQ0QsT0FISyxDQUZELENBQVA7QUFPRDtBQUNGLEdBbEJEOztBQW9CQSxPQUFLLElBQUwsQ0FBVSxzQkFBVixFQUFrQyxVQUFTLFFBQVQsRUFBbUI7Ozs7Ozs7QUFPbkQsUUFBRyxRQUFRLFNBQVIsS0FBc0IsU0FBdEIsSUFBbUMsUUFBUSxTQUFSLElBQXFCLElBQTNELEVBQWlFO0FBQy9EO0FBQ0Q7QUFDRCxZQUFRLEtBQVI7QUFDQSxRQUFHLFFBQVEsaUJBQVIsRUFBSCxFQUFnQztBQUM5QixjQUFRLEdBQVIsQ0FBWSw0Q0FBWjtBQUNELEtBRkQsTUFHSztBQUNILGNBQVEsR0FBUixDQUFZLFlBQVo7QUFDRDtBQUNELFlBQVEsR0FBUixDQUFZLGdDQUFaOzs7Ozs7QUFPQSxRQUNFLGVBQWU7QUFDYixjQUFvQixNQUFNLE1BRGI7QUFFYixvQkFBb0IsUUFBUSxNQUZmO0FBR2IsWUFBb0IsUUFBUSxJQUFSLElBQWdCLFFBQVEsSUFIL0I7QUFJYixtQkFBb0IsTUFBTSxXQUpiO0FBS2IseUJBQW9CLFFBQVE7QUFMZixLQURqQjs7Ozs7OztBQWVBLFFBQUcsUUFBUSxPQUFSLElBQW1CLFFBQVEsVUFBOUIsRUFBMEM7OztBQUd4QyxVQUFHLFFBQVEsVUFBWCxFQUF1QjtBQUNyQixZQUFHLFFBQVEsVUFBUixLQUF1QixFQUExQixFQUE4QjtBQUM1QixrQkFBUSxHQUFSLENBQVkseUNBQVo7QUFDQTtBQUNEO0FBQ0QsZ0JBQVEsSUFBUixHQUFlLFFBQVEsVUFBdkI7QUFDRDs7O0FBR0QscUJBQWUsT0FBTyxLQUFQLEVBQWMsRUFBZCxFQUFrQixZQUFsQixFQUFnQztBQUM3QyxvQkFBZSxRQUFRLFdBRHNCO0FBRTdDLG9CQUFlLFFBQVEsVUFGc0I7QUFHN0MsZUFBZSxRQUFRLEtBSHNCO0FBSTdDLGVBQWUsUUFBUSxNQUpzQjtBQUs3QyxzQkFBZSxLQUFLLElBQUwsQ0FBVSxRQUFRLE1BQWxCLEVBQTBCLFFBQVEsWUFBbEMsQ0FMOEI7QUFNN0MscUJBQWUsUUFBUTtBQU5zQixPQUFoQyxDQUFmOzs7QUFVQSxzQkFBZ0IsS0FBSyxJQUFMLENBQVUsUUFBUSxJQUFsQixFQUF3QixRQUFRLFlBQWhDLENBQWhCOzs7QUFHQSxXQUFJLElBQUksV0FBUixJQUF1QixZQUF2QixFQUFxQztBQUNuQyxZQUFJLGFBQWEsY0FBYixDQUE0QixXQUE1QixDQUFKLEVBQStDOztBQUU3Qyx1QkFBYSxXQUFiLElBQTZCLGVBQWUsUUFBZixJQUEyQixlQUFlLGNBQTNDLEdBQ3hCLEtBQUssU0FBTCxDQUFnQixLQUFLLElBQUwsQ0FBVSxRQUFRLElBQWxCLEVBQXdCLGFBQWEsV0FBYixDQUF4QixDQUFoQixDQUR3QixHQUV4QixLQUFLLFNBQUwsQ0FBZ0IsS0FBSyxJQUFMLENBQVUsYUFBVixFQUF5QixhQUFhLFdBQWIsQ0FBekIsQ0FBaEIsQ0FGSjtBQUlEO0FBQ0Y7OztBQUdELFVBQUk7QUFDRixlQUFPLElBQVAsQ0FBWSxhQUFaO0FBQ0EsZUFBTyxJQUFQLENBQVksYUFBYSxVQUF6QjtBQUNBLGVBQU8sSUFBUCxDQUFZLGFBQWEsS0FBekI7QUFDQSxlQUFPLElBQVAsQ0FBWSxhQUFhLEtBQXpCO0FBQ0QsT0FMRCxDQU1BLE9BQU0sS0FBTixFQUFhO0FBQ1gsZ0JBQVEsS0FBUixDQUFjLHFJQUFkO0FBQ0Q7O0FBRUQsY0FBUSxHQUFSLENBQVksMkJBQTJCLFFBQVEsWUFBbkMsR0FBa0QsU0FBOUQ7O0FBRUEsY0FBUSxJQUFSLENBQWEsd0JBQWI7QUFDQSxhQUFPLG9CQUFQLENBQTRCLE9BQU8sV0FBbkMsRUFBZ0QsYUFBYSxVQUE3RCxFQUF5RSxTQUFTLE1BQVQsQ0FBZ0IsU0FBekY7O0FBRUEsY0FBUSxJQUFSLENBQWEsbUJBQWI7QUFDQSxhQUFPLG9CQUFQLENBQTRCLE9BQU8sTUFBbkMsRUFBMkMsYUFBYSxLQUF4RCxFQUErRCxTQUFTLE1BQVQsQ0FBZ0IsS0FBL0U7QUFDQSxhQUFPLG9CQUFQLENBQTRCLE9BQU8sWUFBbkMsRUFBaUQsYUFBYSxZQUE5RCxFQUE0RSxTQUFTLE1BQVQsQ0FBZ0IsU0FBNUY7O0FBRUEsY0FBUSxJQUFSLENBQWEsb0JBQWI7QUFDQSxhQUFPLG9CQUFQLENBQTRCLE9BQU8sS0FBbkMsRUFBMEMsYUFBYSxLQUF2RCxFQUE4RCxTQUFTLE1BQVQsQ0FBZ0IsU0FBOUU7OztBQUdBLGNBQVEsSUFBUixDQUFhLG9CQUFiO0FBQ0EsV0FBSyxHQUFMLENBQVMsT0FBTyxXQUFoQixFQUNHLElBREgsQ0FDUSxTQURSLEVBRUcsSUFGSCxDQUVRLEtBQUssSUFBTCxDQUFVLGFBQWEsV0FBdkIsQ0FGUjtBQUlBLFdBQUssR0FBTCxDQUFTLE9BQU8sVUFBaEIsRUFDRyxJQURILENBQ1EsU0FEUixFQUVHLElBRkgsQ0FFUSxLQUFLLElBQUwsQ0FBVSxhQUFhLFVBQXZCLENBRlI7OztBQU1BLGNBQVEsSUFBUixDQUFhLHNCQUFiO0FBQ0EsV0FBSyxHQUFMLENBQVMsT0FBTyxZQUFoQixFQUNHLElBREgsQ0FDUSxTQURSLEVBRUcsSUFGSCxDQUVRLEtBQUssSUFBTCxDQUFVLGFBQVYsQ0FGUjtBQUtEOzs7Ozs7O0FBUUQsUUFBSSxHQUFHLFVBQUgsQ0FBYyxhQUFhLElBQTNCLENBQUosRUFBdUM7QUFDckMsY0FBUSxJQUFSLENBQWEsa0RBQWIsRUFBaUUsYUFBYSxJQUE5RTtBQUNELEtBRkQsTUFHSztBQUNILGNBQVEsSUFBUixDQUFhLDRCQUFiLEVBQTJDLGFBQWEsSUFBeEQ7QUFDRDtBQUNELFdBQU8sb0JBQVAsQ0FBNEIsT0FBTyxJQUFuQyxFQUF5QyxhQUFhLElBQXRELEVBQTRELFNBQVMsTUFBVCxDQUFnQixLQUE1RTs7Ozs7O0FBTUEsU0FBSyxJQUFMLENBQVUscUJBQVYsRUFBaUMsWUFBVztBQUMxQzs7O0FBR0UsbUJBQWUsS0FBSyxRQUFMLENBQWMsS0FBSyxPQUFMLENBQWEsYUFBYSxpQkFBMUIsQ0FBZCxFQUE0RCxLQUFLLE9BQUwsQ0FBYSxhQUFhLElBQTFCLENBQTVELEVBQTZGLE9BQTdGLENBQXFHLEtBQXJHLEVBQTJHLEdBQTNHLENBSGpCO0FBQUEsVUFJRSxlQUFlLHNCQUFzQixVQUF0QixHQUFtQyxLQUpwRDs7O0FBUUEsY0FBUSxJQUFSLENBQWEsNEJBQWIsRUFBMkMsYUFBYSxHQUF4RDs7QUFFQSxVQUFHLEdBQUcsVUFBSCxDQUFjLGFBQWEsV0FBM0IsQ0FBSCxFQUE0QztBQUMxQyxnQkFBUSxJQUFSLENBQWEsMENBQWIsRUFBeUQsYUFBYSxXQUF0RTtBQUNBLGVBQU8sS0FBSyxHQUFMLENBQVMsYUFBYSxXQUF0QixFQUNKLElBREksQ0FDQyxTQURELEVBRUosSUFGSSxDQUVDLFFBQVEsT0FBTyxZQUFmLEVBQTZCLFlBQTdCLENBRkQsRUFHSixJQUhJLENBR0MsS0FBSyxJQUFMLENBQVUsYUFBYSxpQkFBdkIsQ0FIRCxDQUFQO0FBS0QsT0FQRCxNQVFLO0FBQ0gsZ0JBQVEsSUFBUixDQUFhLHlDQUFiLEVBQXdELGFBQWEsV0FBckU7QUFDQSxlQUFPLEtBQUssR0FBTCxDQUFTLE9BQU8sV0FBaEIsRUFDSixJQURJLENBQ0MsU0FERCxFQUVKLElBRkksQ0FFQyxPQUFPLEVBQUUsU0FBVSxFQUFaLEVBQVAsQ0FGRCxFQUdKLElBSEksQ0FHQyxRQUFRLE9BQU8sWUFBZixFQUE2QixZQUE3QixDQUhELEVBSUosSUFKSSxDQUlDLEtBQUssSUFBTCxDQUFVLGFBQWEsaUJBQXZCLENBSkQsQ0FBUDtBQU1EO0FBQ0YsS0E1QkQ7Ozs7OztBQWtDQSxTQUFLLElBQUwsQ0FBVSxzQkFBVixFQUFrQyxZQUFXOztBQUUzQyxVQUNFLGFBQWEsUUFBUSxVQUFSLENBQW1CLE9BQW5CLENBRGY7OztBQUtBLFVBQUksR0FBRyxVQUFILENBQWMsTUFBTSxNQUFwQixDQUFKLEVBQWtDO0FBQ2hDLGdCQUFRLElBQVIsQ0FBYSx1Q0FBYixFQUFzRCxhQUFhLE1BQW5FO0FBQ0EsZUFBTyxLQUFLLEdBQUwsQ0FBUyxhQUFhLE1BQXRCLEVBQ0osSUFESSxDQUNDLFNBREQsRUFFSixJQUZJLENBRUMsT0FBTyxTQUFTLE1BQVQsQ0FBZ0IsSUFBdkIsQ0FGRCxDO0FBQUEsU0FHSixJQUhJLENBR0MsV0FBVyxVQUFYLENBSEQsRUFJSixJQUpJLENBSUMsS0FBSyxJQUFMLENBQVUsYUFBYSxZQUF2QixDQUpELENBQVA7QUFNRCxPQVJELE1BU0s7QUFDSCxnQkFBUSxJQUFSLENBQWEsc0NBQWIsRUFBcUQsYUFBYSxNQUFsRTtBQUNBLGVBQU8sS0FBSyxHQUFMLENBQVMsT0FBTyxNQUFoQixFQUNKLElBREksQ0FDQyxTQURELEVBRUosSUFGSSxDQUVDLE9BQU8sRUFBRSxTQUFVLEVBQVosRUFBUCxDQUZELEM7QUFBQSxTQUdKLElBSEksQ0FHQyxXQUFXLFVBQVgsQ0FIRCxFQUlKLElBSkksQ0FJQyxLQUFLLElBQUwsQ0FBVSxhQUFhLFlBQXZCLENBSkQsQ0FBUDtBQU1EO0FBRUYsS0ExQkQ7O0FBNEJBLGdCQUNFLHFCQURGLEVBRUUsc0JBRkYsRUFHRSxRQUhGO0FBTUQsR0EzTUQ7O0FBNk1BLE9BQUssSUFBTCxDQUFVLGtCQUFWLEVBQThCLFlBQVc7OztBQUd2QyxRQUFHLGFBQUgsRUFBa0I7QUFDaEIsY0FBUSxHQUFSLENBQVksa0pBQWtKLFFBQVEsWUFBMUosR0FBeUssc0RBQXJMO0FBQ0EsY0FBUSxJQUFSLENBQWEsQ0FBYjtBQUNELEtBSEQsTUFJSztBQUNILGNBQVEsR0FBUixDQUFZLEVBQVo7QUFDQSxjQUFRLEdBQVIsQ0FBWSxFQUFaO0FBQ0Q7OztBQUdELFFBQUcsUUFBUSxpQkFBUixFQUFILEVBQWdDO0FBQzlCLGFBQU8sS0FBSyxLQUFMLENBQVcsT0FBWCxDQUFQO0FBQ0QsS0FGRCxNQUdLO0FBQ0gsYUFBTyxLQUNKLEdBREksQ0FDQSxhQURBLEVBRUosSUFGSSxDQUVDLE9BQU8sTUFBUCxDQUFjLFVBQVUsT0FBeEIsRUFBaUMsVUFBUyxPQUFULEVBQWtCO0FBQ3ZELFlBQUcsUUFBUSxPQUFSLElBQW1CLEtBQXRCLEVBQTZCO0FBQzNCLGNBQUksUUFBUSxVQUFaO0FBQ0Q7QUFDRCxZQUFHLFFBQVEsS0FBUixJQUFpQixLQUFwQixFQUEyQjtBQUN6QixlQUFLLEtBQUwsQ0FBVyxPQUFYO0FBQ0Q7QUFDRixPQVBLLENBRkQsQ0FBUDtBQVdEO0FBR0YsR0EvQkQ7O0FBaUNBLGNBQ0UsV0FERixFQUVFLHNCQUZGLEVBR0Usa0JBSEYsRUFJRSxRQUpGO0FBT0MsQ0EvWUQiLCJmaWxlIjoiaW5zdGFsbC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICBJbnN0YWxsIFRhc2tcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi8qXG4gICBJbnN0YWxsIHRhc2tzXG5cbiAgIEZvciBtb3JlIG5vdGVzXG5cbiAgICogUnVucyBhdXRvbWF0aWNhbGx5IGFmdGVyIG5wbSB1cGRhdGUgKGhvb2tzKVxuICAgKiAoTlBNKSBJbnN0YWxsIC0gV2lsbCBhc2sgZm9yIHdoZXJlIHRvIHB1dCBzZW1hbnRpYyAob3V0c2lkZSBwbSBmb2xkZXIpXG4gICAqIChOUE0pIFVwZ3JhZGUgLSBXaWxsIGxvb2sgZm9yIHNlbWFudGljIGluc3RhbGwsIGNvcHkgb3ZlciBmaWxlcyBhbmQgdXBkYXRlIGlmIG5ldyB2ZXJzaW9uXG4gICAqIFN0YW5kYXJkIGluc3RhbGxlciBydW5zIGFza2luZyBmb3IgcGF0aHMgdG8gc2l0ZSBmaWxlcyBldGNcblxuKi9cblxudmFyXG4gIGd1bHAgICAgICAgICAgID0gcmVxdWlyZSgnZ3VscCcpLFxuXG4gIC8vIG5vZGUgZGVwZW5kZW5jaWVzXG4gIGNvbnNvbGUgICAgICAgID0gcmVxdWlyZSgnYmV0dGVyLWNvbnNvbGUnKSxcbiAgZXh0ZW5kICAgICAgICAgPSByZXF1aXJlKCdleHRlbmQnKSxcbiAgZnMgICAgICAgICAgICAgPSByZXF1aXJlKCdmcycpLFxuICBta2RpcnAgICAgICAgICA9IHJlcXVpcmUoJ21rZGlycCcpLFxuICBwYXRoICAgICAgICAgICA9IHJlcXVpcmUoJ3BhdGgnKSxcbiAgcnVuU2VxdWVuY2UgICAgPSByZXF1aXJlKCdydW4tc2VxdWVuY2UnKSxcblxuICAvLyBndWxwIGRlcGVuZGVuY2llc1xuICBjaG1vZCAgICAgICAgICA9IHJlcXVpcmUoJ2d1bHAtY2htb2QnKSxcbiAgZGVsICAgICAgICAgICAgPSByZXF1aXJlKCdkZWwnKSxcbiAganNvbkVkaXRvciAgICAgPSByZXF1aXJlKCdndWxwLWpzb24tZWRpdG9yJyksXG4gIHBsdW1iZXIgICAgICAgID0gcmVxdWlyZSgnZ3VscC1wbHVtYmVyJyksXG4gIHByb21wdCAgICAgICAgID0gcmVxdWlyZSgnZ3VscC1wcm9tcHQnKSxcbiAgcmVuYW1lICAgICAgICAgPSByZXF1aXJlKCdndWxwLXJlbmFtZScpLFxuICByZXBsYWNlICAgICAgICA9IHJlcXVpcmUoJ2d1bHAtcmVwbGFjZScpLFxuICByZXF1aXJlRG90RmlsZSA9IHJlcXVpcmUoJ3JlcXVpcmUtZG90LWZpbGUnKSxcbiAgd3JlbmNoICAgICAgICAgPSByZXF1aXJlKCd3cmVuY2gnKSxcblxuICAvLyBpbnN0YWxsIGNvbmZpZ1xuICBpbnN0YWxsICAgICAgICA9IHJlcXVpcmUoJy4vY29uZmlnL3Byb2plY3QvaW5zdGFsbCcpLFxuXG4gIC8vIHVzZXIgY29uZmlnXG4gIGNvbmZpZyAgICAgICAgID0gcmVxdWlyZSgnLi9jb25maWcvdXNlcicpLFxuXG4gIC8vIHJlbGVhc2UgY29uZmlnIChuYW1lL3RpdGxlL2V0YylcbiAgcmVsZWFzZSAgICAgICAgPSByZXF1aXJlKCcuL2NvbmZpZy9wcm9qZWN0L3JlbGVhc2UnKSxcblxuICAvLyBzaG9ydGhhbmRcbiAgcXVlc3Rpb25zICAgICAgPSBpbnN0YWxsLnF1ZXN0aW9ucyxcbiAgZmlsZXMgICAgICAgICAgPSBpbnN0YWxsLmZpbGVzLFxuICBmb2xkZXJzICAgICAgICA9IGluc3RhbGwuZm9sZGVycyxcbiAgcmVnRXhwICAgICAgICAgPSBpbnN0YWxsLnJlZ0V4cCxcbiAgc2V0dGluZ3MgICAgICAgPSBpbnN0YWxsLnNldHRpbmdzLFxuICBzb3VyY2UgICAgICAgICA9IGluc3RhbGwuc291cmNlXG47XG5cbi8vIEV4cG9ydCBpbnN0YWxsIHRhc2tcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG5cbnZhclxuICBjdXJyZW50Q29uZmlnID0gcmVxdWlyZURvdEZpbGUoJ3NlbWFudGljLmpzb24nKSxcbiAgbWFuYWdlciAgICAgICA9IGluc3RhbGwuZ2V0UGFja2FnZU1hbmFnZXIoKSxcbiAgcm9vdFF1ZXN0aW9ucyA9IHF1ZXN0aW9ucy5yb290LFxuICBpbnN0YWxsRm9sZGVyID0gZmFsc2UsXG4gIGFuc3dlcnNcbjtcblxuY29uc29sZS5jbGVhcigpO1xuXG4vKiBUZXN0IE5QTSBpbnN0YWxsXG5tYW5hZ2VyID0ge1xuICBuYW1lIDogJ05QTScsXG4gIHJvb3QgOiBwYXRoLm5vcm1hbGl6ZShfX2Rpcm5hbWUgKyAnLy4uLycpXG59O1xuKi9cblxuXG4vKiBEb24ndCBkbyBlbmQgdXNlciBjb25maWcgaWYgU1VJIGlzIGEgc3ViLW1vZHVsZSAqL1xuaWYoIGluc3RhbGwuaXNTdWJNb2R1bGUoKSApIHtcbiAgY29uc29sZS5pbmZvKCdTVUkgaXMgYSBzdWItbW9kdWxlLCBza2lwcGluZyBlbmQtdXNlciBpbnN0YWxsJyk7XG4gIHJldHVybjtcbn1cblxuLyotLS0tLS0tLS0tLS0tLS0tLVxuICAgIFVwZGF0ZSBTVUlcbi0tLS0tLS0tLS0tLS0tLS0tKi9cblxuLy8gcnVuIHVwZGF0ZSBzY3JpcHRzIGlmIHNlbWFudGljLmpzb24gZXhpc3RzXG5pZihjdXJyZW50Q29uZmlnICYmIG1hbmFnZXIubmFtZSA9PT0gJ05QTScpIHtcblxuICB2YXJcbiAgICB1cGRhdGVGb2xkZXIgPSBwYXRoLmpvaW4obWFuYWdlci5yb290LCBjdXJyZW50Q29uZmlnLmJhc2UpLFxuICAgIHVwZGF0ZVBhdGhzICA9IHtcbiAgICAgIGNvbmZpZyAgICAgICA6IHBhdGguam9pbihtYW5hZ2VyLnJvb3QsIGZpbGVzLmNvbmZpZyksXG4gICAgICB0YXNrcyAgICAgICAgOiBwYXRoLmpvaW4odXBkYXRlRm9sZGVyLCBmb2xkZXJzLnRhc2tzKSxcbiAgICAgIHRoZW1lSW1wb3J0ICA6IHBhdGguam9pbih1cGRhdGVGb2xkZXIsIGZvbGRlcnMudGhlbWVJbXBvcnQpLFxuICAgICAgZGVmaW5pdGlvbiAgIDogcGF0aC5qb2luKGN1cnJlbnRDb25maWcucGF0aHMuc291cmNlLmRlZmluaXRpb25zKSxcbiAgICAgIHNpdGUgICAgICAgICA6IHBhdGguam9pbihjdXJyZW50Q29uZmlnLnBhdGhzLnNvdXJjZS5zaXRlKSxcbiAgICAgIHRoZW1lICAgICAgICA6IHBhdGguam9pbihjdXJyZW50Q29uZmlnLnBhdGhzLnNvdXJjZS50aGVtZXMpLFxuICAgICAgZGVmYXVsdFRoZW1lIDogcGF0aC5qb2luKGN1cnJlbnRDb25maWcucGF0aHMuc291cmNlLnRoZW1lcywgZm9sZGVycy5kZWZhdWx0VGhlbWUpXG4gICAgfVxuICA7XG5cbiAgLy8gZHVjay10eXBlIGlmIHRoZXJlIGlzIGEgcHJvamVjdCBpbnN0YWxsZWRcbiAgaWYoIGZzLmV4aXN0c1N5bmModXBkYXRlUGF0aHMuZGVmaW5pdGlvbikgKSB7XG5cbiAgICAvLyBwZXJmb3JtIHVwZGF0ZSBpZiBuZXcgdmVyc2lvblxuICAgIGlmKGN1cnJlbnRDb25maWcudmVyc2lvbiAhPT0gcmVsZWFzZS52ZXJzaW9uKSB7XG4gICAgICBjb25zb2xlLmxvZygnVXBkYXRpbmcgU2VtYW50aWMgVUkgZnJvbSAnICsgY3VycmVudENvbmZpZy52ZXJzaW9uICsgJyB0byAnICsgcmVsZWFzZS52ZXJzaW9uKTtcblxuICAgICAgY29uc29sZS5pbmZvKCdVcGRhdGluZyB1aSBkZWZpbml0aW9ucy4uLicpO1xuICAgICAgd3JlbmNoLmNvcHlEaXJTeW5jUmVjdXJzaXZlKHNvdXJjZS5kZWZpbml0aW9ucywgdXBkYXRlUGF0aHMuZGVmaW5pdGlvbiwgc2V0dGluZ3Mud3JlbmNoLm92ZXJ3cml0ZSk7XG5cbiAgICAgIGNvbnNvbGUuaW5mbygnVXBkYXRpbmcgZGVmYXVsdCB0aGVtZS4uLicpO1xuICAgICAgd3JlbmNoLmNvcHlEaXJTeW5jUmVjdXJzaXZlKHNvdXJjZS50aGVtZXMsIHVwZGF0ZVBhdGhzLnRoZW1lLCBzZXR0aW5ncy53cmVuY2gubWVyZ2UpO1xuICAgICAgd3JlbmNoLmNvcHlEaXJTeW5jUmVjdXJzaXZlKHNvdXJjZS5kZWZhdWx0VGhlbWUsIHVwZGF0ZVBhdGhzLmRlZmF1bHRUaGVtZSwgc2V0dGluZ3Mud3JlbmNoLm92ZXJ3cml0ZSk7XG5cbiAgICAgIGNvbnNvbGUuaW5mbygnVXBkYXRpbmcgdGFza3MuLi4nKTtcbiAgICAgIHdyZW5jaC5jb3B5RGlyU3luY1JlY3Vyc2l2ZShzb3VyY2UudGFza3MsIHVwZGF0ZVBhdGhzLnRhc2tzLCBzZXR0aW5ncy53cmVuY2gub3ZlcndyaXRlKTtcblxuICAgICAgY29uc29sZS5pbmZvKCdVcGRhdGluZyBndWxwZmlsZS5qcycpO1xuICAgICAgZ3VscC5zcmMoc291cmNlLnVzZXJHdWxwRmlsZSlcbiAgICAgICAgLnBpcGUocGx1bWJlcigpKVxuICAgICAgICAucGlwZShndWxwLmRlc3QodXBkYXRlRm9sZGVyKSlcbiAgICAgIDtcblxuICAgICAgLy8gY29weSB0aGVtZSBpbXBvcnRcbiAgICAgIGNvbnNvbGUuaW5mbygnVXBkYXRpbmcgdGhlbWUgaW1wb3J0IGZpbGUnKTtcbiAgICAgIGd1bHAuc3JjKHNvdXJjZS50aGVtZUltcG9ydClcbiAgICAgICAgLnBpcGUocGx1bWJlcigpKVxuICAgICAgICAucGlwZShndWxwLmRlc3QodXBkYXRlUGF0aHMudGhlbWVJbXBvcnQpKVxuICAgICAgO1xuXG4gICAgICBjb25zb2xlLmluZm8oJ0FkZGluZyBuZXcgc2l0ZSB0aGVtZSBmaWxlcy4uLicpO1xuICAgICAgd3JlbmNoLmNvcHlEaXJTeW5jUmVjdXJzaXZlKHNvdXJjZS5zaXRlLCB1cGRhdGVQYXRocy5zaXRlLCBzZXR0aW5ncy53cmVuY2gubWVyZ2UpO1xuXG4gICAgICBjb25zb2xlLmluZm8oJ1VwZGF0aW5nIHZlcnNpb24uLi4nKTtcblxuICAgICAgLy8gdXBkYXRlIHZlcnNpb24gbnVtYmVyIGluIHNlbWFudGljLmpzb25cbiAgICAgIGd1bHAuc3JjKHVwZGF0ZVBhdGhzLmNvbmZpZylcbiAgICAgICAgLnBpcGUocGx1bWJlcigpKVxuICAgICAgICAucGlwZShyZW5hbWUoc2V0dGluZ3MucmVuYW1lLmpzb24pKSAvLyBwcmVzZXJ2ZSBmaWxlIGV4dGVuc2lvblxuICAgICAgICAucGlwZShqc29uRWRpdG9yKHtcbiAgICAgICAgICB2ZXJzaW9uOiByZWxlYXNlLnZlcnNpb25cbiAgICAgICAgfSkpXG4gICAgICAgIC5waXBlKGd1bHAuZGVzdChtYW5hZ2VyLnJvb3QpKVxuICAgICAgO1xuXG4gICAgICBjb25zb2xlLmluZm8oJ1VwZGF0ZSBjb21wbGV0ZSEgUnVuIFwiXFx4MWJbOTJtZ3VscCBidWlsZFxceDFiWzBtXCIgdG8gcmVidWlsZCBkaXN0LyBmaWxlcy4nKTtcblxuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIGNvbnNvbGUubG9nKCdDdXJyZW50IHZlcnNpb24gb2YgU2VtYW50aWMgVUkgYWxyZWFkeSBpbnN0YWxsZWQnKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgfVxuICBlbHNlIHtcbiAgICBjb25zb2xlLmVycm9yKCdDYW5ub3QgbG9jYXRlIGZpbGVzIHRvIHVwZGF0ZSBhdCBwYXRoOiAnLCB1cGRhdGVQYXRocy5kZWZpbml0aW9uKTtcbiAgICBjb25zb2xlLmxvZygnUnVubmluZyBpbnN0YWxsZXInKTtcbiAgfVxuXG59XG5cbi8qLS0tLS0tLS0tLS0tLS1cbiBEZXRlcm1pbmUgUm9vdFxuLS0tLS0tLS0tLS0tLS0tKi9cblxuLy8gUE0gdGhhdCBzdXBwb3J0cyBCdWlsZCBUb29scyAoTlBNIE9ubHkgTm93KVxuaWYobWFuYWdlci5uYW1lID09ICdOUE0nKSB7XG4gIHJvb3RRdWVzdGlvbnNbMF0ubWVzc2FnZSA9IHJvb3RRdWVzdGlvbnNbMF0ubWVzc2FnZVxuICAgIC5yZXBsYWNlKCd7cGFja2FnZU1lc3NhZ2V9JywgJ1dlIGRldGVjdGVkIHlvdSBhcmUgdXNpbmcgXFx4MWJbOTJtJyArIG1hbmFnZXIubmFtZSArICdcXHgxYlswbS4gTmljZSEgJylcbiAgICAucmVwbGFjZSgne3Jvb3R9JywgbWFuYWdlci5yb290KVxuICA7XG4gIC8vIHNldCBkZWZhdWx0IHBhdGggdG8gZGV0ZWN0ZWQgUE0gcm9vdFxuICByb290UXVlc3Rpb25zWzBdLmRlZmF1bHQgPSBtYW5hZ2VyLnJvb3Q7XG4gIHJvb3RRdWVzdGlvbnNbMV0uZGVmYXVsdCA9IG1hbmFnZXIucm9vdDtcblxuICAvLyBpbnNlcnQgUE0gcXVlc3Rpb25zIGFmdGVyIFwiSW5zdGFsbCBUeXBlXCIgcXVlc3Rpb25cbiAgQXJyYXkucHJvdG90eXBlLnNwbGljZS5hcHBseShxdWVzdGlvbnMuc2V0dXAsIFsyLCAwXS5jb25jYXQocm9vdFF1ZXN0aW9ucykpO1xuXG4gIC8vIG9taXQgY2xlYW51cCBxdWVzdGlvbnMgZm9yIG1hbmFnZWQgaW5zdGFsbFxuICBxdWVzdGlvbnMuY2xlYW51cCA9IFtdO1xufVxuXG5cbi8qLS0tLS0tLS0tLS0tLS1cbiAgIENyZWF0ZSBTVUlcbi0tLS0tLS0tLS0tLS0tLSovXG5cbmd1bHAudGFzaygncnVuIHNldHVwJywgZnVuY3Rpb24oKSB7XG5cbiAgLy8gSWYgYXV0by1pbnN0YWxsIGlzIHN3aXRjaGVkIG9uLCB3ZSBza2lwIHRoZSBjb25maWd1cmF0aW9uIHNlY3Rpb24gYW5kIHNpbXBseSByZXVzZSB0aGUgY29uZmlndXJhdGlvbiBmcm9tIHNlbWFudGljLmpzb25cbiAgaWYoaW5zdGFsbC5zaG91bGRBdXRvSW5zdGFsbCgpKSB7XG4gICAgYW5zd2VycyA9IHtcbiAgICAgIG92ZXJ3cml0ZSA6ICd5ZXMnLFxuICAgICAgaW5zdGFsbCAgIDogJ2F1dG8nLFxuICAgIH07XG4gIH1cbiAgZWxzZSB7XG4gICAgcmV0dXJuIGd1bHBcbiAgICAgIC5zcmMoJ2d1bHBmaWxlLmpzJylcbiAgICAgIC5waXBlKHByb21wdC5wcm9tcHQocXVlc3Rpb25zLnNldHVwLCBmdW5jdGlvbihzZXR1cEFuc3dlcnMpIHtcbiAgICAgICAgLy8gaG9pc3RcbiAgICAgICAgYW5zd2VycyA9IHNldHVwQW5zd2VycztcbiAgICAgIH0pKVxuICAgIDtcbiAgfVxufSk7XG5cbmd1bHAudGFzaygnY3JlYXRlIGluc3RhbGwgZmlsZXMnLCBmdW5jdGlvbihjYWxsYmFjaykge1xuXG4gIC8qLS0tLS0tLS0tLS0tLS1cbiAgIEV4aXQgQ29uZGl0aW9uc1xuICAtLS0tLS0tLS0tLS0tLS0qL1xuXG4gIC8vIGlmIGNvbmZpZyBleGlzdHMgYW5kIHVzZXIgc3BlY2lmaWVzIG5vdCB0byBwcm9jZWVkXG4gIGlmKGFuc3dlcnMub3ZlcndyaXRlICE9PSB1bmRlZmluZWQgJiYgYW5zd2Vycy5vdmVyd3JpdGUgPT0gJ25vJykge1xuICAgIHJldHVybjtcbiAgfVxuICBjb25zb2xlLmNsZWFyKCk7XG4gIGlmKGluc3RhbGwuc2hvdWxkQXV0b0luc3RhbGwoKSkge1xuICAgIGNvbnNvbGUubG9nKCdBdXRvLUluc3RhbGxpbmcgKFdpdGhvdXQgVXNlciBJbnRlcmFjdGlvbiknKTtcbiAgfVxuICBlbHNlIHtcbiAgICBjb25zb2xlLmxvZygnSW5zdGFsbGluZycpO1xuICB9XG4gIGNvbnNvbGUubG9nKCctLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0nKTtcblxuXG4gIC8qLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgUGF0aHNcbiAgLS0tLS0tLS0tLS0tLS0tKi9cblxuICB2YXJcbiAgICBpbnN0YWxsUGF0aHMgPSB7XG4gICAgICBjb25maWcgICAgICAgICAgICA6IGZpbGVzLmNvbmZpZyxcbiAgICAgIGNvbmZpZ0ZvbGRlciAgICAgIDogZm9sZGVycy5jb25maWcsXG4gICAgICBzaXRlICAgICAgICAgICAgICA6IGFuc3dlcnMuc2l0ZSB8fCBmb2xkZXJzLnNpdGUsXG4gICAgICB0aGVtZUNvbmZpZyAgICAgICA6IGZpbGVzLnRoZW1lQ29uZmlnLFxuICAgICAgdGhlbWVDb25maWdGb2xkZXIgOiBmb2xkZXJzLnRoZW1lQ29uZmlnXG4gICAgfVxuICA7XG5cbiAgLyotLS0tLS0tLS0tLS0tLVxuICAgIE5QTSBJbnN0YWxsXG4gIC0tLS0tLS0tLS0tLS0tLSovXG5cbiAgLy8gQ2hlY2sgaWYgUE0gaW5zdGFsbFxuICBpZihhbnN3ZXJzLnVzZVJvb3QgfHwgYW5zd2Vycy5jdXN0b21Sb290KSB7XG5cbiAgICAvLyBTZXQgcm9vdCB0byBjdXN0b20gcm9vdCBwYXRoIGlmIHNldFxuICAgIGlmKGFuc3dlcnMuY3VzdG9tUm9vdCkge1xuICAgICAgaWYoYW5zd2Vycy5jdXN0b21Sb290ID09PSAnJykge1xuICAgICAgICBjb25zb2xlLmxvZygnVW5hYmxlIHRvIHByb2NlZWQsIGludmFsaWQgcHJvamVjdCByb290Jyk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIG1hbmFnZXIucm9vdCA9IGFuc3dlcnMuY3VzdG9tUm9vdDtcbiAgICB9XG5cbiAgICAvLyBzcGVjaWFsIGluc3RhbGwgcGF0aHMgb25seSBmb3IgUE0gaW5zdGFsbFxuICAgIGluc3RhbGxQYXRocyA9IGV4dGVuZChmYWxzZSwge30sIGluc3RhbGxQYXRocywge1xuICAgICAgZGVmaW5pdGlvbiAgIDogZm9sZGVycy5kZWZpbml0aW9ucyxcbiAgICAgIGxlc3NJbXBvcnQgICA6IGZvbGRlcnMubGVzc0ltcG9ydCxcbiAgICAgIHRhc2tzICAgICAgICA6IGZvbGRlcnMudGFza3MsXG4gICAgICB0aGVtZSAgICAgICAgOiBmb2xkZXJzLnRoZW1lcyxcbiAgICAgIGRlZmF1bHRUaGVtZSA6IHBhdGguam9pbihmb2xkZXJzLnRoZW1lcywgZm9sZGVycy5kZWZhdWx0VGhlbWUpLFxuICAgICAgdGhlbWVJbXBvcnQgIDogZm9sZGVycy50aGVtZUltcG9ydFxuICAgIH0pO1xuXG4gICAgLy8gYWRkIHByb2plY3Qgcm9vdCB0byBzZW1hbnRpYyByb290XG4gICAgaW5zdGFsbEZvbGRlciA9IHBhdGguam9pbihtYW5hZ2VyLnJvb3QsIGFuc3dlcnMuc2VtYW50aWNSb290KTtcblxuICAgIC8vIGFkZCBpbnN0YWxsIGZvbGRlciB0byBhbGwgb3V0cHV0IHBhdGhzXG4gICAgZm9yKHZhciBkZXN0aW5hdGlvbiBpbiBpbnN0YWxsUGF0aHMpIHtcbiAgICAgIGlmKCBpbnN0YWxsUGF0aHMuaGFzT3duUHJvcGVydHkoZGVzdGluYXRpb24pICkge1xuICAgICAgICAvLyBjb25maWcgZ29lcyBpbiBwcm9qZWN0IHJvb3QsIHJlc3QgaW4gaW5zdGFsbCBmb2xkZXJcbiAgICAgICAgaW5zdGFsbFBhdGhzW2Rlc3RpbmF0aW9uXSA9IChkZXN0aW5hdGlvbiA9PSAnY29uZmlnJyB8fCBkZXN0aW5hdGlvbiA9PSAnY29uZmlnRm9sZGVyJylcbiAgICAgICAgICA/IHBhdGgubm9ybWFsaXplKCBwYXRoLmpvaW4obWFuYWdlci5yb290LCBpbnN0YWxsUGF0aHNbZGVzdGluYXRpb25dKSApXG4gICAgICAgICAgOiBwYXRoLm5vcm1hbGl6ZSggcGF0aC5qb2luKGluc3RhbGxGb2xkZXIsIGluc3RhbGxQYXRoc1tkZXN0aW5hdGlvbl0pIClcbiAgICAgICAgO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIGNyZWF0ZSBwcm9qZWN0IGZvbGRlcnNcbiAgICB0cnkge1xuICAgICAgbWtkaXJwLnN5bmMoaW5zdGFsbEZvbGRlcik7XG4gICAgICBta2RpcnAuc3luYyhpbnN0YWxsUGF0aHMuZGVmaW5pdGlvbik7XG4gICAgICBta2RpcnAuc3luYyhpbnN0YWxsUGF0aHMudGhlbWUpO1xuICAgICAgbWtkaXJwLnN5bmMoaW5zdGFsbFBhdGhzLnRhc2tzKTtcbiAgICB9XG4gICAgY2F0Y2goZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ05QTSBkb2VzIG5vdCBoYXZlIHBlcm1pc3Npb25zIHRvIGNyZWF0ZSBmb2xkZXJzIGF0IHlvdXIgc3BlY2lmaWVkIHBhdGguIEFkanVzdCB5b3VyIGZvbGRlcnMgcGVybWlzc2lvbnMgYW5kIHJ1biBcIm5wbSBpbnN0YWxsXCIgYWdhaW4nKTtcbiAgICB9XG5cbiAgICBjb25zb2xlLmxvZygnSW5zdGFsbGluZyB0byBcXHgxYls5Mm0nICsgYW5zd2Vycy5zZW1hbnRpY1Jvb3QgKyAnXFx4MWJbMG0nKTtcblxuICAgIGNvbnNvbGUuaW5mbygnQ29weWluZyBVSSBkZWZpbml0aW9ucycpO1xuICAgIHdyZW5jaC5jb3B5RGlyU3luY1JlY3Vyc2l2ZShzb3VyY2UuZGVmaW5pdGlvbnMsIGluc3RhbGxQYXRocy5kZWZpbml0aW9uLCBzZXR0aW5ncy53cmVuY2gub3ZlcndyaXRlKTtcblxuICAgIGNvbnNvbGUuaW5mbygnQ29weWluZyBVSSB0aGVtZXMnKTtcbiAgICB3cmVuY2guY29weURpclN5bmNSZWN1cnNpdmUoc291cmNlLnRoZW1lcywgaW5zdGFsbFBhdGhzLnRoZW1lLCBzZXR0aW5ncy53cmVuY2gubWVyZ2UpO1xuICAgIHdyZW5jaC5jb3B5RGlyU3luY1JlY3Vyc2l2ZShzb3VyY2UuZGVmYXVsdFRoZW1lLCBpbnN0YWxsUGF0aHMuZGVmYXVsdFRoZW1lLCBzZXR0aW5ncy53cmVuY2gub3ZlcndyaXRlKTtcblxuICAgIGNvbnNvbGUuaW5mbygnQ29weWluZyBndWxwIHRhc2tzJyk7XG4gICAgd3JlbmNoLmNvcHlEaXJTeW5jUmVjdXJzaXZlKHNvdXJjZS50YXNrcywgaW5zdGFsbFBhdGhzLnRhc2tzLCBzZXR0aW5ncy53cmVuY2gub3ZlcndyaXRlKTtcblxuICAgIC8vIGNvcHkgdGhlbWUgaW1wb3J0XG4gICAgY29uc29sZS5pbmZvKCdBZGRpbmcgdGhlbWUgZmlsZXMnKTtcbiAgICBndWxwLnNyYyhzb3VyY2UudGhlbWVJbXBvcnQpXG4gICAgICAucGlwZShwbHVtYmVyKCkpXG4gICAgICAucGlwZShndWxwLmRlc3QoaW5zdGFsbFBhdGhzLnRoZW1lSW1wb3J0KSlcbiAgICA7XG4gICAgZ3VscC5zcmMoc291cmNlLmxlc3NJbXBvcnQpXG4gICAgICAucGlwZShwbHVtYmVyKCkpXG4gICAgICAucGlwZShndWxwLmRlc3QoaW5zdGFsbFBhdGhzLmxlc3NJbXBvcnQpKVxuICAgIDtcblxuICAgIC8vIGNyZWF0ZSBndWxwIGZpbGVcbiAgICBjb25zb2xlLmluZm8oJ0NyZWF0aW5nIGd1bHBmaWxlLmpzJyk7XG4gICAgZ3VscC5zcmMoc291cmNlLnVzZXJHdWxwRmlsZSlcbiAgICAgIC5waXBlKHBsdW1iZXIoKSlcbiAgICAgIC5waXBlKGd1bHAuZGVzdChpbnN0YWxsRm9sZGVyKSlcbiAgICA7XG5cbiAgfVxuXG5cbiAgLyotLS0tLS0tLS0tLS0tLVxuICAgICBTaXRlIFRoZW1lXG4gIC0tLS0tLS0tLS0tLS0tLSovXG5cbiAgLy8gQ29weSBfc2l0ZSB0ZW1wbGF0ZXMgZm9sZGVyIHRvIGRlc3RpbmF0aW9uXG4gIGlmKCBmcy5leGlzdHNTeW5jKGluc3RhbGxQYXRocy5zaXRlKSApIHtcbiAgICBjb25zb2xlLmluZm8oJ1NpdGUgZm9sZGVyIGV4aXN0cywgbWVyZ2luZyBmaWxlcyAobm8gb3ZlcndyaXRlKScsIGluc3RhbGxQYXRocy5zaXRlKTtcbiAgfVxuICBlbHNlIHtcbiAgICBjb25zb2xlLmluZm8oJ0NyZWF0aW5nIHNpdGUgdGhlbWUgZm9sZGVyJywgaW5zdGFsbFBhdGhzLnNpdGUpO1xuICB9XG4gIHdyZW5jaC5jb3B5RGlyU3luY1JlY3Vyc2l2ZShzb3VyY2Uuc2l0ZSwgaW5zdGFsbFBhdGhzLnNpdGUsIHNldHRpbmdzLndyZW5jaC5tZXJnZSk7XG5cbiAgLyotLS0tLS0tLS0tLS0tLVxuICAgIFRoZW1lIENvbmZpZ1xuICAtLS0tLS0tLS0tLS0tLS0qL1xuXG4gIGd1bHAudGFzaygnY3JlYXRlIHRoZW1lLmNvbmZpZycsIGZ1bmN0aW9uKCkge1xuICAgIHZhclxuICAgICAgLy8gZGV0ZXJtaW5lIHBhdGggdG8gc2l0ZSB0aGVtZSBmb2xkZXIgZnJvbSB0aGVtZSBjb25maWdcbiAgICAgIC8vIGZvcmNlIENTUyBwYXRoIHZhcmlhYmxlIHRvIHVzZSBmb3J3YXJkIHNsYXNoZXMgZm9yIHBhdGhzXG4gICAgICBwYXRoVG9TaXRlICAgPSBwYXRoLnJlbGF0aXZlKHBhdGgucmVzb2x2ZShpbnN0YWxsUGF0aHMudGhlbWVDb25maWdGb2xkZXIpLCBwYXRoLnJlc29sdmUoaW5zdGFsbFBhdGhzLnNpdGUpKS5yZXBsYWNlKC9cXFxcL2csJy8nKSxcbiAgICAgIHNpdGVWYXJpYWJsZSA9IFwiQHNpdGVGb2xkZXIgICA6ICdcIiArIHBhdGhUb1NpdGUgKyBcIi8nO1wiXG4gICAgO1xuXG4gICAgLy8gcmV3cml0ZSBzaXRlIHZhcmlhYmxlIGluIHRoZW1lLmxlc3NcbiAgICBjb25zb2xlLmluZm8oJ0FkanVzdGluZyBAc2l0ZUZvbGRlciB0bzogJywgcGF0aFRvU2l0ZSArICcvJyk7XG5cbiAgICBpZihmcy5leGlzdHNTeW5jKGluc3RhbGxQYXRocy50aGVtZUNvbmZpZykpIHtcbiAgICAgIGNvbnNvbGUuaW5mbygnTW9kaWZ5aW5nIHNyYy90aGVtZS5jb25maWcgKExFU1MgY29uZmlnKScsIGluc3RhbGxQYXRocy50aGVtZUNvbmZpZyk7XG4gICAgICByZXR1cm4gZ3VscC5zcmMoaW5zdGFsbFBhdGhzLnRoZW1lQ29uZmlnKVxuICAgICAgICAucGlwZShwbHVtYmVyKCkpXG4gICAgICAgIC5waXBlKHJlcGxhY2UocmVnRXhwLnNpdGVWYXJpYWJsZSwgc2l0ZVZhcmlhYmxlKSlcbiAgICAgICAgLnBpcGUoZ3VscC5kZXN0KGluc3RhbGxQYXRocy50aGVtZUNvbmZpZ0ZvbGRlcikpXG4gICAgICA7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgY29uc29sZS5pbmZvKCdDcmVhdGluZyBzcmMvdGhlbWUuY29uZmlnIChMRVNTIGNvbmZpZyknLCBpbnN0YWxsUGF0aHMudGhlbWVDb25maWcpO1xuICAgICAgcmV0dXJuIGd1bHAuc3JjKHNvdXJjZS50aGVtZUNvbmZpZylcbiAgICAgICAgLnBpcGUocGx1bWJlcigpKVxuICAgICAgICAucGlwZShyZW5hbWUoeyBleHRuYW1lIDogJycgfSkpXG4gICAgICAgIC5waXBlKHJlcGxhY2UocmVnRXhwLnNpdGVWYXJpYWJsZSwgc2l0ZVZhcmlhYmxlKSlcbiAgICAgICAgLnBpcGUoZ3VscC5kZXN0KGluc3RhbGxQYXRocy50aGVtZUNvbmZpZ0ZvbGRlcikpXG4gICAgICA7XG4gICAgfVxuICB9KTtcblxuICAvKi0tLS0tLS0tLS0tLS0tXG4gICAgU2VtYW50aWMuanNvblxuICAtLS0tLS0tLS0tLS0tLS0qL1xuXG4gIGd1bHAudGFzaygnY3JlYXRlIHNlbWFudGljLmpzb24nLCBmdW5jdGlvbigpIHtcblxuICAgIHZhclxuICAgICAganNvbkNvbmZpZyA9IGluc3RhbGwuY3JlYXRlSlNPTihhbnN3ZXJzKVxuICAgIDtcblxuICAgIC8vIGFkanVzdCB2YXJpYWJsZXMgaW4gdGhlbWUubGVzc1xuICAgIGlmKCBmcy5leGlzdHNTeW5jKGZpbGVzLmNvbmZpZykgKSB7XG4gICAgICBjb25zb2xlLmluZm8oJ0V4dGVuZGluZyBjb25maWcgZmlsZSAoc2VtYW50aWMuanNvbiknLCBpbnN0YWxsUGF0aHMuY29uZmlnKTtcbiAgICAgIHJldHVybiBndWxwLnNyYyhpbnN0YWxsUGF0aHMuY29uZmlnKVxuICAgICAgICAucGlwZShwbHVtYmVyKCkpXG4gICAgICAgIC5waXBlKHJlbmFtZShzZXR0aW5ncy5yZW5hbWUuanNvbikpIC8vIHByZXNlcnZlIGZpbGUgZXh0ZW5zaW9uXG4gICAgICAgIC5waXBlKGpzb25FZGl0b3IoanNvbkNvbmZpZykpXG4gICAgICAgIC5waXBlKGd1bHAuZGVzdChpbnN0YWxsUGF0aHMuY29uZmlnRm9sZGVyKSlcbiAgICAgIDtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICBjb25zb2xlLmluZm8oJ0NyZWF0aW5nIGNvbmZpZyBmaWxlIChzZW1hbnRpYy5qc29uKScsIGluc3RhbGxQYXRocy5jb25maWcpO1xuICAgICAgcmV0dXJuIGd1bHAuc3JjKHNvdXJjZS5jb25maWcpXG4gICAgICAgIC5waXBlKHBsdW1iZXIoKSlcbiAgICAgICAgLnBpcGUocmVuYW1lKHsgZXh0bmFtZSA6ICcnIH0pKSAvLyByZW1vdmUgLnRlbXBsYXRlIGZyb20gZXh0XG4gICAgICAgIC5waXBlKGpzb25FZGl0b3IoanNvbkNvbmZpZykpXG4gICAgICAgIC5waXBlKGd1bHAuZGVzdChpbnN0YWxsUGF0aHMuY29uZmlnRm9sZGVyKSlcbiAgICAgIDtcbiAgICB9XG5cbiAgfSk7XG5cbiAgcnVuU2VxdWVuY2UoXG4gICAgJ2NyZWF0ZSB0aGVtZS5jb25maWcnLFxuICAgICdjcmVhdGUgc2VtYW50aWMuanNvbicsXG4gICAgY2FsbGJhY2tcbiAgKTtcblxufSk7XG5cbmd1bHAudGFzaygnY2xlYW4gdXAgaW5zdGFsbCcsIGZ1bmN0aW9uKCkge1xuXG4gIC8vIENvbXBsZXRpb24gTWVzc2FnZVxuICBpZihpbnN0YWxsRm9sZGVyKSB7XG4gICAgY29uc29sZS5sb2coJ1xcbiBTZXR1cCBDb21wbGV0ZSEgXFxuIEluc3RhbGxpbmcgUGVlciBEZXBlbmRlbmNpZXMuIFxceDFiWzA7MzFtUGxlYXNlIHJlZnJhaW4gZnJvbSBjdHJsICsgY1xceDFiWzBtLi4uIFxcbiBBZnRlciBjb21wbGV0aW9uIG5hdmlnYXRlIHRvIFxceDFiWzkybScgKyBhbnN3ZXJzLnNlbWFudGljUm9vdCArICdcXHgxYlswbSBhbmQgcnVuIFwiXFx4MWJbOTJtZ3VscCBidWlsZFxceDFiWzBtXCIgdG8gYnVpbGQnKTtcbiAgICBwcm9jZXNzLmV4aXQoMCk7XG4gIH1cbiAgZWxzZSB7XG4gICAgY29uc29sZS5sb2coJycpO1xuICAgIGNvbnNvbGUubG9nKCcnKTtcbiAgfVxuXG4gIC8vIElmIGF1dG8taW5zdGFsbCBpcyBzd2l0Y2hlZCBvbiwgd2Ugc2tpcCB0aGUgY29uZmlndXJhdGlvbiBzZWN0aW9uIGFuZCBzaW1wbHkgYnVpbGQgdGhlIGRlcGVuZGVuY2llc1xuICBpZihpbnN0YWxsLnNob3VsZEF1dG9JbnN0YWxsKCkpIHtcbiAgICByZXR1cm4gZ3VscC5zdGFydCgnYnVpbGQnKTtcbiAgfVxuICBlbHNlIHtcbiAgICByZXR1cm4gZ3VscFxuICAgICAgLnNyYygnZ3VscGZpbGUuanMnKVxuICAgICAgLnBpcGUocHJvbXB0LnByb21wdChxdWVzdGlvbnMuY2xlYW51cCwgZnVuY3Rpb24oYW5zd2Vycykge1xuICAgICAgICBpZihhbnN3ZXJzLmNsZWFudXAgPT0gJ3llcycpIHtcbiAgICAgICAgICBkZWwoaW5zdGFsbC5zZXR1cEZpbGVzKTtcbiAgICAgICAgfVxuICAgICAgICBpZihhbnN3ZXJzLmJ1aWxkID09ICd5ZXMnKSB7XG4gICAgICAgICAgZ3VscC5zdGFydCgnYnVpbGQnKTtcbiAgICAgICAgfVxuICAgICAgfSkpXG4gICAgO1xuICB9XG5cblxufSk7XG5cbnJ1blNlcXVlbmNlKFxuICAncnVuIHNldHVwJyxcbiAgJ2NyZWF0ZSBpbnN0YWxsIGZpbGVzJyxcbiAgJ2NsZWFuIHVwIGluc3RhbGwnLFxuICBjYWxsYmFja1xuKTtcblxufTtcbiJdfQ==