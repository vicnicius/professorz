/*!
 * # Semantic UI - Site
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  $.site = $.fn.site = function (parameters) {
    var time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.site.settings, parameters) : $.extend({}, $.site.settings),
        namespace = settings.namespace,
        error = settings.error,
        eventNamespace = '.' + namespace,
        moduleNamespace = 'module-' + namespace,
        $document = $(document),
        $module = $document,
        element = this,
        instance = $module.data(moduleNamespace),
        module,
        returnedValue;
    module = {

      initialize: function () {
        module.instantiate();
      },

      instantiate: function () {
        module.verbose('Storing instance of site', module);
        instance = module;
        $module.data(moduleNamespace, module);
      },

      normalize: function () {
        module.fix.console();
        module.fix.requestAnimationFrame();
      },

      fix: {
        console: function () {
          module.debug('Normalizing window.console');
          if (console === undefined || console.log === undefined) {
            module.verbose('Console not available, normalizing events');
            module.disable.console();
          }
          if (typeof console.group == 'undefined' || typeof console.groupEnd == 'undefined' || typeof console.groupCollapsed == 'undefined') {
            module.verbose('Console group not available, normalizing events');
            window.console.group = function () {};
            window.console.groupEnd = function () {};
            window.console.groupCollapsed = function () {};
          }
          if (typeof console.markTimeline == 'undefined') {
            module.verbose('Mark timeline not available, normalizing events');
            window.console.markTimeline = function () {};
          }
        },
        consoleClear: function () {
          module.debug('Disabling programmatic console clearing');
          window.console.clear = function () {};
        },
        requestAnimationFrame: function () {
          module.debug('Normalizing requestAnimationFrame');
          if (window.requestAnimationFrame === undefined) {
            module.debug('RequestAnimationFrame not available, normalizing event');
            window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
              setTimeout(callback, 0);
            };
          }
        }
      },

      moduleExists: function (name) {
        return $.fn[name] !== undefined && $.fn[name].settings !== undefined;
      },

      enabled: {
        modules: function (modules) {
          var enabledModules = [];
          modules = modules || settings.modules;
          $.each(modules, function (index, name) {
            if (module.moduleExists(name)) {
              enabledModules.push(name);
            }
          });
          return enabledModules;
        }
      },

      disabled: {
        modules: function (modules) {
          var disabledModules = [];
          modules = modules || settings.modules;
          $.each(modules, function (index, name) {
            if (!module.moduleExists(name)) {
              disabledModules.push(name);
            }
          });
          return disabledModules;
        }
      },

      change: {
        setting: function (setting, value, modules, modifyExisting) {
          modules = typeof modules === 'string' ? modules === 'all' ? settings.modules : [modules] : modules || settings.modules;
          modifyExisting = modifyExisting !== undefined ? modifyExisting : true;
          $.each(modules, function (index, name) {
            var namespace = module.moduleExists(name) ? $.fn[name].settings.namespace || false : true,
                $existingModules;
            if (module.moduleExists(name)) {
              module.verbose('Changing default setting', setting, value, name);
              $.fn[name].settings[setting] = value;
              if (modifyExisting && namespace) {
                $existingModules = $(':data(module-' + namespace + ')');
                if ($existingModules.length > 0) {
                  module.verbose('Modifying existing settings', $existingModules);
                  $existingModules[name]('setting', setting, value);
                }
              }
            }
          });
        },
        settings: function (newSettings, modules, modifyExisting) {
          modules = typeof modules === 'string' ? [modules] : modules || settings.modules;
          modifyExisting = modifyExisting !== undefined ? modifyExisting : true;
          $.each(modules, function (index, name) {
            var $existingModules;
            if (module.moduleExists(name)) {
              module.verbose('Changing default setting', newSettings, name);
              $.extend(true, $.fn[name].settings, newSettings);
              if (modifyExisting && namespace) {
                $existingModules = $(':data(module-' + namespace + ')');
                if ($existingModules.length > 0) {
                  module.verbose('Modifying existing settings', $existingModules);
                  $existingModules[name]('setting', newSettings);
                }
              }
            }
          });
        }
      },

      enable: {
        console: function () {
          module.console(true);
        },
        debug: function (modules, modifyExisting) {
          modules = modules || settings.modules;
          module.debug('Enabling debug for modules', modules);
          module.change.setting('debug', true, modules, modifyExisting);
        },
        verbose: function (modules, modifyExisting) {
          modules = modules || settings.modules;
          module.debug('Enabling verbose debug for modules', modules);
          module.change.setting('verbose', true, modules, modifyExisting);
        }
      },
      disable: {
        console: function () {
          module.console(false);
        },
        debug: function (modules, modifyExisting) {
          modules = modules || settings.modules;
          module.debug('Disabling debug for modules', modules);
          module.change.setting('debug', false, modules, modifyExisting);
        },
        verbose: function (modules, modifyExisting) {
          modules = modules || settings.modules;
          module.debug('Disabling verbose debug for modules', modules);
          module.change.setting('verbose', false, modules, modifyExisting);
        }
      },

      console: function (enable) {
        if (enable) {
          if (instance.cache.console === undefined) {
            module.error(error.console);
            return;
          }
          module.debug('Restoring console function');
          window.console = instance.cache.console;
        } else {
          module.debug('Disabling console function');
          instance.cache.console = window.console;
          window.console = {
            clear: function () {},
            error: function () {},
            group: function () {},
            groupCollapsed: function () {},
            groupEnd: function () {},
            info: function () {},
            log: function () {},
            markTimeline: function () {},
            warn: function () {}
          };
        }
      },

      destroy: function () {
        module.verbose('Destroying previous site for', $module);
        $module.removeData(moduleNamespace);
      },

      cache: {},

      setting: function (name, value) {
        if ($.isPlainObject(name)) {
          $.extend(true, settings, name);
        } else if (value !== undefined) {
          settings[name] = value;
        } else {
          return settings[name];
        }
      },
      internal: function (name, value) {
        if ($.isPlainObject(name)) {
          $.extend(true, module, name);
        } else if (value !== undefined) {
          module[name] = value;
        } else {
          return module[name];
        }
      },
      debug: function () {
        if (settings.debug) {
          if (settings.performance) {
            module.performance.log(arguments);
          } else {
            module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
            module.debug.apply(console, arguments);
          }
        }
      },
      verbose: function () {
        if (settings.verbose && settings.debug) {
          if (settings.performance) {
            module.performance.log(arguments);
          } else {
            module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
            module.verbose.apply(console, arguments);
          }
        }
      },
      error: function () {
        module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
        module.error.apply(console, arguments);
      },
      performance: {
        log: function (message) {
          var currentTime, executionTime, previousTime;
          if (settings.performance) {
            currentTime = new Date().getTime();
            previousTime = time || currentTime;
            executionTime = currentTime - previousTime;
            time = currentTime;
            performance.push({
              'Element': element,
              'Name': message[0],
              'Arguments': [].slice.call(message, 1) || '',
              'Execution Time': executionTime
            });
          }
          clearTimeout(module.performance.timer);
          module.performance.timer = setTimeout(module.performance.display, 500);
        },
        display: function () {
          var title = settings.name + ':',
              totalTime = 0;
          time = false;
          clearTimeout(module.performance.timer);
          $.each(performance, function (index, data) {
            totalTime += data['Execution Time'];
          });
          title += ' ' + totalTime + 'ms';
          if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
            console.groupCollapsed(title);
            if (console.table) {
              console.table(performance);
            } else {
              $.each(performance, function (index, data) {
                console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
              });
            }
            console.groupEnd();
          }
          performance = [];
        }
      },
      invoke: function (query, passedArguments, context) {
        var object = instance,
            maxDepth,
            found,
            response;
        passedArguments = passedArguments || queryArguments;
        context = element || context;
        if (typeof query == 'string' && object !== undefined) {
          query = query.split(/[\. ]/);
          maxDepth = query.length - 1;
          $.each(query, function (depth, value) {
            var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
            if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
              object = object[camelCaseValue];
            } else if (object[camelCaseValue] !== undefined) {
              found = object[camelCaseValue];
              return false;
            } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
              object = object[value];
            } else if (object[value] !== undefined) {
              found = object[value];
              return false;
            } else {
              module.error(error.method, query);
              return false;
            }
          });
        }
        if ($.isFunction(found)) {
          response = found.apply(context, passedArguments);
        } else if (found !== undefined) {
          response = found;
        }
        if ($.isArray(returnedValue)) {
          returnedValue.push(response);
        } else if (returnedValue !== undefined) {
          returnedValue = [returnedValue, response];
        } else if (response !== undefined) {
          returnedValue = response;
        }
        return found;
      }
    };

    if (methodInvoked) {
      if (instance === undefined) {
        module.initialize();
      }
      module.invoke(query);
    } else {
      if (instance !== undefined) {
        module.destroy();
      }
      module.initialize();
    }
    return returnedValue !== undefined ? returnedValue : this;
  };

  $.site.settings = {

    name: 'Site',
    namespace: 'site',

    error: {
      console: 'Console cannot be restored, most likely it was overwritten outside of module',
      method: 'The method you called is not defined.'
    },

    debug: false,
    verbose: false,
    performance: true,

    modules: ['accordion', 'api', 'checkbox', 'dimmer', 'dropdown', 'embed', 'form', 'modal', 'nag', 'popup', 'rating', 'shape', 'sidebar', 'state', 'sticky', 'tab', 'transition', 'visit', 'visibility'],

    siteNamespace: 'site',
    namespaceStub: {
      cache: {},
      config: {},
      sections: {},
      section: {},
      utilities: {}
    }

  };

  // allows for selection of elements with data attributes
  $.extend($.expr[":"], {
    data: $.expr.createPseudo ? $.expr.createPseudo(function (dataName) {
      return function (elem) {
        return !!$.data(elem, dataName);
      };
    }) : function (elem, i, match) {
      // support: jQuery < 1.8
      return !!$.data(elem, match[3]);
    }
  });
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9nbG9iYWxzL3NpdGUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQVVBLENBQUMsQ0FBQyxVQUFVLENBQVYsRUFBYSxNQUFiLEVBQXFCLFFBQXJCLEVBQStCLFNBQS9CLEVBQTBDOztBQUU1QyxJQUFFLElBQUYsR0FBUyxFQUFFLEVBQUYsQ0FBSyxJQUFMLEdBQVksVUFBUyxVQUFULEVBQXFCO0FBQ3hDLFFBQ0UsT0FBaUIsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQURuQjtBQUFBLFFBRUUsY0FBaUIsRUFGbkI7QUFBQSxRQUlFLFFBQWlCLFVBQVUsQ0FBVixDQUpuQjtBQUFBLFFBS0UsZ0JBQWtCLE9BQU8sS0FBUCxJQUFnQixRQUxwQztBQUFBLFFBTUUsaUJBQWlCLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLENBQXpCLENBTm5CO0FBQUEsUUFRRSxXQUFvQixFQUFFLGFBQUYsQ0FBZ0IsVUFBaEIsQ0FBRixHQUNkLEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLEVBQUUsSUFBRixDQUFPLFFBQTFCLEVBQW9DLFVBQXBDLENBRGMsR0FFZCxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsRUFBRSxJQUFGLENBQU8sUUFBcEIsQ0FWTjtBQUFBLFFBWUUsWUFBa0IsU0FBUyxTQVo3QjtBQUFBLFFBYUUsUUFBa0IsU0FBUyxLQWI3QjtBQUFBLFFBZUUsaUJBQWtCLE1BQU0sU0FmMUI7QUFBQSxRQWdCRSxrQkFBa0IsWUFBWSxTQWhCaEM7QUFBQSxRQWtCRSxZQUFrQixFQUFFLFFBQUYsQ0FsQnBCO0FBQUEsUUFtQkUsVUFBa0IsU0FuQnBCO0FBQUEsUUFvQkUsVUFBa0IsSUFwQnBCO0FBQUEsUUFxQkUsV0FBa0IsUUFBUSxJQUFSLENBQWEsZUFBYixDQXJCcEI7QUFBQSxRQXVCRSxNQXZCRjtBQUFBLFFBd0JFLGFBeEJGO0FBMEJBLGFBQVM7O0FBRVAsa0JBQVksWUFBVztBQUNyQixlQUFPLFdBQVA7QUFDRCxPQUpNOztBQU1QLG1CQUFhLFlBQVc7QUFDdEIsZUFBTyxPQUFQLENBQWUsMEJBQWYsRUFBMkMsTUFBM0M7QUFDQSxtQkFBVyxNQUFYO0FBQ0EsZ0JBQ0csSUFESCxDQUNRLGVBRFIsRUFDeUIsTUFEekI7QUFHRCxPQVpNOztBQWNQLGlCQUFXLFlBQVc7QUFDcEIsZUFBTyxHQUFQLENBQVcsT0FBWDtBQUNBLGVBQU8sR0FBUCxDQUFXLHFCQUFYO0FBQ0QsT0FqQk07O0FBbUJQLFdBQUs7QUFDSCxpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLEtBQVAsQ0FBYSw0QkFBYjtBQUNBLGNBQUksWUFBWSxTQUFaLElBQXlCLFFBQVEsR0FBUixLQUFnQixTQUE3QyxFQUF3RDtBQUN0RCxtQkFBTyxPQUFQLENBQWUsMkNBQWY7QUFDQSxtQkFBTyxPQUFQLENBQWUsT0FBZjtBQUNEO0FBQ0QsY0FBSSxPQUFPLFFBQVEsS0FBZixJQUF3QixXQUF4QixJQUF1QyxPQUFPLFFBQVEsUUFBZixJQUEyQixXQUFsRSxJQUFpRixPQUFPLFFBQVEsY0FBZixJQUFpQyxXQUF0SCxFQUFtSTtBQUNqSSxtQkFBTyxPQUFQLENBQWUsaURBQWY7QUFDQSxtQkFBTyxPQUFQLENBQWUsS0FBZixHQUF1QixZQUFXLENBQUUsQ0FBcEM7QUFDQSxtQkFBTyxPQUFQLENBQWUsUUFBZixHQUEwQixZQUFXLENBQUUsQ0FBdkM7QUFDQSxtQkFBTyxPQUFQLENBQWUsY0FBZixHQUFnQyxZQUFXLENBQUUsQ0FBN0M7QUFDRDtBQUNELGNBQUksT0FBTyxRQUFRLFlBQWYsSUFBK0IsV0FBbkMsRUFBZ0Q7QUFDOUMsbUJBQU8sT0FBUCxDQUFlLGlEQUFmO0FBQ0EsbUJBQU8sT0FBUCxDQUFlLFlBQWYsR0FBOEIsWUFBVyxDQUFFLENBQTNDO0FBQ0Q7QUFDRixTQWpCRTtBQWtCSCxzQkFBYyxZQUFXO0FBQ3ZCLGlCQUFPLEtBQVAsQ0FBYSx5Q0FBYjtBQUNBLGlCQUFPLE9BQVAsQ0FBZSxLQUFmLEdBQXVCLFlBQVcsQ0FBRSxDQUFwQztBQUNELFNBckJFO0FBc0JILCtCQUF1QixZQUFXO0FBQ2hDLGlCQUFPLEtBQVAsQ0FBYSxtQ0FBYjtBQUNBLGNBQUcsT0FBTyxxQkFBUCxLQUFpQyxTQUFwQyxFQUErQztBQUM3QyxtQkFBTyxLQUFQLENBQWEsd0RBQWI7QUFDQSxtQkFBTyxxQkFBUCxHQUErQixPQUFPLHFCQUFQLElBQzFCLE9BQU8sd0JBRG1CLElBRTFCLE9BQU8sMkJBRm1CLElBRzFCLE9BQU8sdUJBSG1CLElBSTFCLFVBQVMsUUFBVCxFQUFtQjtBQUFFLHlCQUFXLFFBQVgsRUFBcUIsQ0FBckI7QUFBMEIsYUFKcEQ7QUFNRDtBQUNGO0FBakNFLE9BbkJFOztBQXVEUCxvQkFBYyxVQUFTLElBQVQsRUFBZTtBQUMzQixlQUFRLEVBQUUsRUFBRixDQUFLLElBQUwsTUFBZSxTQUFmLElBQTRCLEVBQUUsRUFBRixDQUFLLElBQUwsRUFBVyxRQUFYLEtBQXdCLFNBQTVEO0FBQ0QsT0F6RE07O0FBMkRQLGVBQVM7QUFDUCxpQkFBUyxVQUFTLE9BQVQsRUFBa0I7QUFDekIsY0FDRSxpQkFBaUIsRUFEbkI7QUFHQSxvQkFBVSxXQUFXLFNBQVMsT0FBOUI7QUFDQSxZQUFFLElBQUYsQ0FBTyxPQUFQLEVBQWdCLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjtBQUNwQyxnQkFBRyxPQUFPLFlBQVAsQ0FBb0IsSUFBcEIsQ0FBSCxFQUE4QjtBQUM1Qiw2QkFBZSxJQUFmLENBQW9CLElBQXBCO0FBQ0Q7QUFDRixXQUpEO0FBS0EsaUJBQU8sY0FBUDtBQUNEO0FBWk0sT0EzREY7O0FBMEVQLGdCQUFVO0FBQ1IsaUJBQVMsVUFBUyxPQUFULEVBQWtCO0FBQ3pCLGNBQ0Usa0JBQWtCLEVBRHBCO0FBR0Esb0JBQVUsV0FBVyxTQUFTLE9BQTlCO0FBQ0EsWUFBRSxJQUFGLENBQU8sT0FBUCxFQUFnQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDcEMsZ0JBQUcsQ0FBQyxPQUFPLFlBQVAsQ0FBb0IsSUFBcEIsQ0FBSixFQUErQjtBQUM3Qiw4QkFBZ0IsSUFBaEIsQ0FBcUIsSUFBckI7QUFDRDtBQUNGLFdBSkQ7QUFLQSxpQkFBTyxlQUFQO0FBQ0Q7QUFaTyxPQTFFSDs7QUF5RlAsY0FBUTtBQUNOLGlCQUFTLFVBQVMsT0FBVCxFQUFrQixLQUFsQixFQUF5QixPQUF6QixFQUFrQyxjQUFsQyxFQUFrRDtBQUN6RCxvQkFBVyxPQUFPLE9BQVAsS0FBbUIsUUFBcEIsR0FDTCxZQUFZLEtBQWIsR0FDRSxTQUFTLE9BRFgsR0FFRSxDQUFDLE9BQUQsQ0FISSxHQUlOLFdBQVcsU0FBUyxPQUp4QjtBQU1BLDJCQUFrQixtQkFBbUIsU0FBcEIsR0FDYixjQURhLEdBRWIsSUFGSjtBQUlBLFlBQUUsSUFBRixDQUFPLE9BQVAsRUFBZ0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3BDLGdCQUNFLFlBQWEsT0FBTyxZQUFQLENBQW9CLElBQXBCLENBQUQsR0FDUixFQUFFLEVBQUYsQ0FBSyxJQUFMLEVBQVcsUUFBWCxDQUFvQixTQUFwQixJQUFpQyxLQUR6QixHQUVSLElBSE47QUFBQSxnQkFJRSxnQkFKRjtBQU1BLGdCQUFHLE9BQU8sWUFBUCxDQUFvQixJQUFwQixDQUFILEVBQThCO0FBQzVCLHFCQUFPLE9BQVAsQ0FBZSwwQkFBZixFQUEyQyxPQUEzQyxFQUFvRCxLQUFwRCxFQUEyRCxJQUEzRDtBQUNBLGdCQUFFLEVBQUYsQ0FBSyxJQUFMLEVBQVcsUUFBWCxDQUFvQixPQUFwQixJQUErQixLQUEvQjtBQUNBLGtCQUFHLGtCQUFrQixTQUFyQixFQUFnQztBQUM5QixtQ0FBbUIsRUFBRSxrQkFBa0IsU0FBbEIsR0FBOEIsR0FBaEMsQ0FBbkI7QUFDQSxvQkFBRyxpQkFBaUIsTUFBakIsR0FBMEIsQ0FBN0IsRUFBZ0M7QUFDOUIseUJBQU8sT0FBUCxDQUFlLDZCQUFmLEVBQThDLGdCQUE5QztBQUNBLG1DQUFpQixJQUFqQixFQUF1QixTQUF2QixFQUFrQyxPQUFsQyxFQUEyQyxLQUEzQztBQUNEO0FBQ0Y7QUFDRjtBQUNGLFdBbEJEO0FBbUJELFNBL0JLO0FBZ0NOLGtCQUFVLFVBQVMsV0FBVCxFQUFzQixPQUF0QixFQUErQixjQUEvQixFQUErQztBQUN2RCxvQkFBVyxPQUFPLE9BQVAsS0FBbUIsUUFBcEIsR0FDTixDQUFDLE9BQUQsQ0FETSxHQUVOLFdBQVcsU0FBUyxPQUZ4QjtBQUlBLDJCQUFrQixtQkFBbUIsU0FBcEIsR0FDYixjQURhLEdBRWIsSUFGSjtBQUlBLFlBQUUsSUFBRixDQUFPLE9BQVAsRUFBZ0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3BDLGdCQUNFLGdCQURGO0FBR0EsZ0JBQUcsT0FBTyxZQUFQLENBQW9CLElBQXBCLENBQUgsRUFBOEI7QUFDNUIscUJBQU8sT0FBUCxDQUFlLDBCQUFmLEVBQTJDLFdBQTNDLEVBQXdELElBQXhEO0FBQ0EsZ0JBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFFLEVBQUYsQ0FBSyxJQUFMLEVBQVcsUUFBMUIsRUFBb0MsV0FBcEM7QUFDQSxrQkFBRyxrQkFBa0IsU0FBckIsRUFBZ0M7QUFDOUIsbUNBQW1CLEVBQUUsa0JBQWtCLFNBQWxCLEdBQThCLEdBQWhDLENBQW5CO0FBQ0Esb0JBQUcsaUJBQWlCLE1BQWpCLEdBQTBCLENBQTdCLEVBQWdDO0FBQzlCLHlCQUFPLE9BQVAsQ0FBZSw2QkFBZixFQUE4QyxnQkFBOUM7QUFDQSxtQ0FBaUIsSUFBakIsRUFBdUIsU0FBdkIsRUFBa0MsV0FBbEM7QUFDRDtBQUNGO0FBQ0Y7QUFDRixXQWZEO0FBZ0JEO0FBekRLLE9BekZEOztBQXFKUCxjQUFRO0FBQ04saUJBQVMsWUFBVztBQUNsQixpQkFBTyxPQUFQLENBQWUsSUFBZjtBQUNELFNBSEs7QUFJTixlQUFPLFVBQVMsT0FBVCxFQUFrQixjQUFsQixFQUFrQztBQUN2QyxvQkFBVSxXQUFXLFNBQVMsT0FBOUI7QUFDQSxpQkFBTyxLQUFQLENBQWEsNEJBQWIsRUFBMkMsT0FBM0M7QUFDQSxpQkFBTyxNQUFQLENBQWMsT0FBZCxDQUFzQixPQUF0QixFQUErQixJQUEvQixFQUFxQyxPQUFyQyxFQUE4QyxjQUE5QztBQUNELFNBUks7QUFTTixpQkFBUyxVQUFTLE9BQVQsRUFBa0IsY0FBbEIsRUFBa0M7QUFDekMsb0JBQVUsV0FBVyxTQUFTLE9BQTlCO0FBQ0EsaUJBQU8sS0FBUCxDQUFhLG9DQUFiLEVBQW1ELE9BQW5EO0FBQ0EsaUJBQU8sTUFBUCxDQUFjLE9BQWQsQ0FBc0IsU0FBdEIsRUFBaUMsSUFBakMsRUFBdUMsT0FBdkMsRUFBZ0QsY0FBaEQ7QUFDRDtBQWJLLE9BckpEO0FBb0tQLGVBQVM7QUFDUCxpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLE9BQVAsQ0FBZSxLQUFmO0FBQ0QsU0FITTtBQUlQLGVBQU8sVUFBUyxPQUFULEVBQWtCLGNBQWxCLEVBQWtDO0FBQ3ZDLG9CQUFVLFdBQVcsU0FBUyxPQUE5QjtBQUNBLGlCQUFPLEtBQVAsQ0FBYSw2QkFBYixFQUE0QyxPQUE1QztBQUNBLGlCQUFPLE1BQVAsQ0FBYyxPQUFkLENBQXNCLE9BQXRCLEVBQStCLEtBQS9CLEVBQXNDLE9BQXRDLEVBQStDLGNBQS9DO0FBQ0QsU0FSTTtBQVNQLGlCQUFTLFVBQVMsT0FBVCxFQUFrQixjQUFsQixFQUFrQztBQUN6QyxvQkFBVSxXQUFXLFNBQVMsT0FBOUI7QUFDQSxpQkFBTyxLQUFQLENBQWEscUNBQWIsRUFBb0QsT0FBcEQ7QUFDQSxpQkFBTyxNQUFQLENBQWMsT0FBZCxDQUFzQixTQUF0QixFQUFpQyxLQUFqQyxFQUF3QyxPQUF4QyxFQUFpRCxjQUFqRDtBQUNEO0FBYk0sT0FwS0Y7O0FBb0xQLGVBQVMsVUFBUyxNQUFULEVBQWlCO0FBQ3hCLFlBQUcsTUFBSCxFQUFXO0FBQ1QsY0FBRyxTQUFTLEtBQVQsQ0FBZSxPQUFmLEtBQTJCLFNBQTlCLEVBQXlDO0FBQ3ZDLG1CQUFPLEtBQVAsQ0FBYSxNQUFNLE9BQW5CO0FBQ0E7QUFDRDtBQUNELGlCQUFPLEtBQVAsQ0FBYSw0QkFBYjtBQUNBLGlCQUFPLE9BQVAsR0FBaUIsU0FBUyxLQUFULENBQWUsT0FBaEM7QUFDRCxTQVBELE1BUUs7QUFDSCxpQkFBTyxLQUFQLENBQWEsNEJBQWI7QUFDQSxtQkFBUyxLQUFULENBQWUsT0FBZixHQUF5QixPQUFPLE9BQWhDO0FBQ0EsaUJBQU8sT0FBUCxHQUFpQjtBQUNmLG1CQUFpQixZQUFVLENBQUUsQ0FEZDtBQUVmLG1CQUFpQixZQUFVLENBQUUsQ0FGZDtBQUdmLG1CQUFpQixZQUFVLENBQUUsQ0FIZDtBQUlmLDRCQUFpQixZQUFVLENBQUUsQ0FKZDtBQUtmLHNCQUFpQixZQUFVLENBQUUsQ0FMZDtBQU1mLGtCQUFpQixZQUFVLENBQUUsQ0FOZDtBQU9mLGlCQUFpQixZQUFVLENBQUUsQ0FQZDtBQVFmLDBCQUFpQixZQUFVLENBQUUsQ0FSZDtBQVNmLGtCQUFpQixZQUFVLENBQUU7QUFUZCxXQUFqQjtBQVdEO0FBQ0YsT0E1TU07O0FBOE1QLGVBQVMsWUFBVztBQUNsQixlQUFPLE9BQVAsQ0FBZSw4QkFBZixFQUErQyxPQUEvQztBQUNBLGdCQUNHLFVBREgsQ0FDYyxlQURkO0FBR0QsT0FuTk07O0FBcU5QLGFBQU8sRUFyTkE7O0FBdU5QLGVBQVMsVUFBUyxJQUFULEVBQWUsS0FBZixFQUFzQjtBQUM3QixZQUFJLEVBQUUsYUFBRixDQUFnQixJQUFoQixDQUFKLEVBQTRCO0FBQzFCLFlBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxRQUFmLEVBQXlCLElBQXpCO0FBQ0QsU0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLG1CQUFTLElBQVQsSUFBaUIsS0FBakI7QUFDRCxTQUZJLE1BR0E7QUFDSCxpQkFBTyxTQUFTLElBQVQsQ0FBUDtBQUNEO0FBQ0YsT0FqT007QUFrT1AsZ0JBQVUsVUFBUyxJQUFULEVBQWUsS0FBZixFQUFzQjtBQUM5QixZQUFJLEVBQUUsYUFBRixDQUFnQixJQUFoQixDQUFKLEVBQTRCO0FBQzFCLFlBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxNQUFmLEVBQXVCLElBQXZCO0FBQ0QsU0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLGlCQUFPLElBQVAsSUFBZSxLQUFmO0FBQ0QsU0FGSSxNQUdBO0FBQ0gsaUJBQU8sT0FBTyxJQUFQLENBQVA7QUFDRDtBQUNGLE9BNU9NO0FBNk9QLGFBQU8sWUFBVztBQUNoQixZQUFHLFNBQVMsS0FBWixFQUFtQjtBQUNqQixjQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2QixtQkFBTyxXQUFQLENBQW1CLEdBQW5CLENBQXVCLFNBQXZCO0FBQ0QsV0FGRCxNQUdLO0FBQ0gsbUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFmO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGO0FBQ0YsT0F2UE07QUF3UFAsZUFBUyxZQUFXO0FBQ2xCLFlBQUcsU0FBUyxPQUFULElBQW9CLFNBQVMsS0FBaEMsRUFBdUM7QUFDckMsY0FBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIsbUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELFdBRkQsTUFHSztBQUNILG1CQUFPLE9BQVAsR0FBaUIsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWpCO0FBQ0EsbUJBQU8sT0FBUCxDQUFlLEtBQWYsQ0FBcUIsT0FBckIsRUFBOEIsU0FBOUI7QUFDRDtBQUNGO0FBQ0YsT0FsUU07QUFtUVAsYUFBTyxZQUFXO0FBQ2hCLGVBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLEtBQXJDLEVBQTRDLE9BQTVDLEVBQXFELFNBQVMsSUFBVCxHQUFnQixHQUFyRSxDQUFmO0FBQ0EsZUFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNELE9BdFFNO0FBdVFQLG1CQUFhO0FBQ1gsYUFBSyxVQUFTLE9BQVQsRUFBa0I7QUFDckIsY0FDRSxXQURGLEVBRUUsYUFGRixFQUdFLFlBSEY7QUFLQSxjQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2QiwwQkFBZ0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUFoQjtBQUNBLDJCQUFnQixRQUFRLFdBQXhCO0FBQ0EsNEJBQWdCLGNBQWMsWUFBOUI7QUFDQSxtQkFBZ0IsV0FBaEI7QUFDQSx3QkFBWSxJQUFaLENBQWlCO0FBQ2YseUJBQW1CLE9BREo7QUFFZixzQkFBbUIsUUFBUSxDQUFSLENBRko7QUFHZiwyQkFBbUIsR0FBRyxLQUFILENBQVMsSUFBVCxDQUFjLE9BQWQsRUFBdUIsQ0FBdkIsS0FBNkIsRUFIakM7QUFJZixnQ0FBbUI7QUFKSixhQUFqQjtBQU1EO0FBQ0QsdUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsaUJBQU8sV0FBUCxDQUFtQixLQUFuQixHQUEyQixXQUFXLE9BQU8sV0FBUCxDQUFtQixPQUE5QixFQUF1QyxHQUF2QyxDQUEzQjtBQUNELFNBckJVO0FBc0JYLGlCQUFTLFlBQVc7QUFDbEIsY0FDRSxRQUFRLFNBQVMsSUFBVCxHQUFnQixHQUQxQjtBQUFBLGNBRUUsWUFBWSxDQUZkO0FBSUEsaUJBQU8sS0FBUDtBQUNBLHVCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLFlBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLHlCQUFhLEtBQUssZ0JBQUwsQ0FBYjtBQUNELFdBRkQ7QUFHQSxtQkFBUyxNQUFNLFNBQU4sR0FBa0IsSUFBM0I7QUFDQSxjQUFJLENBQUMsUUFBUSxLQUFSLEtBQWtCLFNBQWxCLElBQStCLFFBQVEsS0FBUixLQUFrQixTQUFsRCxLQUFnRSxZQUFZLE1BQVosR0FBcUIsQ0FBekYsRUFBNEY7QUFDMUYsb0JBQVEsY0FBUixDQUF1QixLQUF2QjtBQUNBLGdCQUFHLFFBQVEsS0FBWCxFQUFrQjtBQUNoQixzQkFBUSxLQUFSLENBQWMsV0FBZDtBQUNELGFBRkQsTUFHSztBQUNILGdCQUFFLElBQUYsQ0FBTyxXQUFQLEVBQW9CLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjtBQUN4Qyx3QkFBUSxHQUFSLENBQVksS0FBSyxNQUFMLElBQWUsSUFBZixHQUFzQixLQUFLLGdCQUFMLENBQXRCLEdBQTZDLElBQXpEO0FBQ0QsZUFGRDtBQUdEO0FBQ0Qsb0JBQVEsUUFBUjtBQUNEO0FBQ0Qsd0JBQWMsRUFBZDtBQUNEO0FBOUNVLE9BdlFOO0FBdVRQLGNBQVEsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDLE9BQWpDLEVBQTBDO0FBQ2hELFlBQ0UsU0FBUyxRQURYO0FBQUEsWUFFRSxRQUZGO0FBQUEsWUFHRSxLQUhGO0FBQUEsWUFJRSxRQUpGO0FBTUEsMEJBQWtCLG1CQUFtQixjQUFyQztBQUNBLGtCQUFrQixXQUFtQixPQUFyQztBQUNBLFlBQUcsT0FBTyxLQUFQLElBQWdCLFFBQWhCLElBQTRCLFdBQVcsU0FBMUMsRUFBcUQ7QUFDbkQsa0JBQVcsTUFBTSxLQUFOLENBQVksT0FBWixDQUFYO0FBQ0EscUJBQVcsTUFBTSxNQUFOLEdBQWUsQ0FBMUI7QUFDQSxZQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ25DLGdCQUFJLGlCQUFrQixTQUFTLFFBQVYsR0FDakIsUUFBUSxNQUFNLFFBQVEsQ0FBZCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixFQUEyQixXQUEzQixFQUFSLEdBQW1ELE1BQU0sUUFBUSxDQUFkLEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBRGxDLEdBRWpCLEtBRko7QUFJQSxnQkFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxjQUFQLENBQWpCLEtBQThDLFNBQVMsUUFBM0QsRUFBdUU7QUFDckUsdUJBQVMsT0FBTyxjQUFQLENBQVQ7QUFDRCxhQUZELE1BR0ssSUFBSSxPQUFPLGNBQVAsTUFBMkIsU0FBL0IsRUFBMkM7QUFDOUMsc0JBQVEsT0FBTyxjQUFQLENBQVI7QUFDQSxxQkFBTyxLQUFQO0FBQ0QsYUFISSxNQUlBLElBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sS0FBUCxDQUFqQixLQUFxQyxTQUFTLFFBQWxELEVBQThEO0FBQ2pFLHVCQUFTLE9BQU8sS0FBUCxDQUFUO0FBQ0QsYUFGSSxNQUdBLElBQUksT0FBTyxLQUFQLE1BQWtCLFNBQXRCLEVBQWtDO0FBQ3JDLHNCQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EscUJBQU8sS0FBUDtBQUNELGFBSEksTUFJQTtBQUNILHFCQUFPLEtBQVAsQ0FBYSxNQUFNLE1BQW5CLEVBQTJCLEtBQTNCO0FBQ0EscUJBQU8sS0FBUDtBQUNEO0FBQ0YsV0F2QkQ7QUF3QkQ7QUFDRCxZQUFLLEVBQUUsVUFBRixDQUFjLEtBQWQsQ0FBTCxFQUE2QjtBQUMzQixxQkFBVyxNQUFNLEtBQU4sQ0FBWSxPQUFaLEVBQXFCLGVBQXJCLENBQVg7QUFDRCxTQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IscUJBQVcsS0FBWDtBQUNEO0FBQ0QsWUFBRyxFQUFFLE9BQUYsQ0FBVSxhQUFWLENBQUgsRUFBNkI7QUFDM0Isd0JBQWMsSUFBZCxDQUFtQixRQUFuQjtBQUNELFNBRkQsTUFHSyxJQUFHLGtCQUFrQixTQUFyQixFQUFnQztBQUNuQywwQkFBZ0IsQ0FBQyxhQUFELEVBQWdCLFFBQWhCLENBQWhCO0FBQ0QsU0FGSSxNQUdBLElBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUM5QiwwQkFBZ0IsUUFBaEI7QUFDRDtBQUNELGVBQU8sS0FBUDtBQUNEO0FBNVdNLEtBQVQ7O0FBK1dBLFFBQUcsYUFBSCxFQUFrQjtBQUNoQixVQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsZUFBTyxVQUFQO0FBQ0Q7QUFDRCxhQUFPLE1BQVAsQ0FBYyxLQUFkO0FBQ0QsS0FMRCxNQU1LO0FBQ0gsVUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLGVBQU8sT0FBUDtBQUNEO0FBQ0QsYUFBTyxVQUFQO0FBQ0Q7QUFDRCxXQUFRLGtCQUFrQixTQUFuQixHQUNILGFBREcsR0FFSCxJQUZKO0FBSUQsR0ExWkQ7O0FBNFpBLElBQUUsSUFBRixDQUFPLFFBQVAsR0FBa0I7O0FBRWhCLFVBQWMsTUFGRTtBQUdoQixlQUFjLE1BSEU7O0FBS2hCLFdBQVE7QUFDTixlQUFVLDhFQURKO0FBRU4sY0FBUztBQUZILEtBTFE7O0FBVWhCLFdBQWMsS0FWRTtBQVdoQixhQUFjLEtBWEU7QUFZaEIsaUJBQWMsSUFaRTs7QUFjaEIsYUFBUyxDQUNQLFdBRE8sRUFFUCxLQUZPLEVBR1AsVUFITyxFQUlQLFFBSk8sRUFLUCxVQUxPLEVBTVAsT0FOTyxFQU9QLE1BUE8sRUFRUCxPQVJPLEVBU1AsS0FUTyxFQVVQLE9BVk8sRUFXUCxRQVhPLEVBWVAsT0FaTyxFQWFQLFNBYk8sRUFjUCxPQWRPLEVBZVAsUUFmTyxFQWdCUCxLQWhCTyxFQWlCUCxZQWpCTyxFQWtCUCxPQWxCTyxFQW1CUCxZQW5CTyxDQWRPOztBQW9DaEIsbUJBQWtCLE1BcENGO0FBcUNoQixtQkFBa0I7QUFDaEIsYUFBWSxFQURJO0FBRWhCLGNBQVksRUFGSTtBQUdoQixnQkFBWSxFQUhJO0FBSWhCLGVBQVksRUFKSTtBQUtoQixpQkFBWTtBQUxJOztBQXJDRixHQUFsQjs7O0FBZ0RBLElBQUUsTUFBRixDQUFTLEVBQUUsSUFBRixDQUFRLEdBQVIsQ0FBVCxFQUF3QjtBQUN0QixVQUFPLEVBQUUsSUFBRixDQUFPLFlBQVIsR0FDRixFQUFFLElBQUYsQ0FBTyxZQUFQLENBQW9CLFVBQVMsUUFBVCxFQUFtQjtBQUNyQyxhQUFPLFVBQVMsSUFBVCxFQUFlO0FBQ3BCLGVBQU8sQ0FBQyxDQUFDLEVBQUUsSUFBRixDQUFPLElBQVAsRUFBYSxRQUFiLENBQVQ7QUFDRCxPQUZEO0FBR0QsS0FKRCxDQURFLEdBTUYsVUFBUyxJQUFULEVBQWUsQ0FBZixFQUFrQixLQUFsQixFQUF5Qjs7QUFFekIsYUFBTyxDQUFDLENBQUMsRUFBRSxJQUFGLENBQU8sSUFBUCxFQUFhLE1BQU8sQ0FBUCxDQUFiLENBQVQ7QUFDRDtBQVZtQixHQUF4QjtBQWNDLENBNWRBLEVBNGRHLE1BNWRILEVBNGRXLE1BNWRYLEVBNGRtQixRQTVkbkIiLCJmaWxlIjoic2l0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxuICogIyBTZW1hbnRpYyBVSSAtIFNpdGVcbiAqIGh0dHA6Ly9naXRodWIuY29tL3NlbWFudGljLW9yZy9zZW1hbnRpYy11aS9cbiAqXG4gKlxuICogUmVsZWFzZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlXG4gKiBodHRwOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvTUlUXG4gKlxuICovXG5cbjsoZnVuY3Rpb24gKCQsIHdpbmRvdywgZG9jdW1lbnQsIHVuZGVmaW5lZCkge1xuXG4kLnNpdGUgPSAkLmZuLnNpdGUgPSBmdW5jdGlvbihwYXJhbWV0ZXJzKSB7XG4gIHZhclxuICAgIHRpbWUgICAgICAgICAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCksXG4gICAgcGVyZm9ybWFuY2UgICAgPSBbXSxcblxuICAgIHF1ZXJ5ICAgICAgICAgID0gYXJndW1lbnRzWzBdLFxuICAgIG1ldGhvZEludm9rZWQgID0gKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyksXG4gICAgcXVlcnlBcmd1bWVudHMgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSksXG5cbiAgICBzZXR0aW5ncyAgICAgICAgPSAoICQuaXNQbGFpbk9iamVjdChwYXJhbWV0ZXJzKSApXG4gICAgICA/ICQuZXh0ZW5kKHRydWUsIHt9LCAkLnNpdGUuc2V0dGluZ3MsIHBhcmFtZXRlcnMpXG4gICAgICA6ICQuZXh0ZW5kKHt9LCAkLnNpdGUuc2V0dGluZ3MpLFxuXG4gICAgbmFtZXNwYWNlICAgICAgID0gc2V0dGluZ3MubmFtZXNwYWNlLFxuICAgIGVycm9yICAgICAgICAgICA9IHNldHRpbmdzLmVycm9yLFxuXG4gICAgZXZlbnROYW1lc3BhY2UgID0gJy4nICsgbmFtZXNwYWNlLFxuICAgIG1vZHVsZU5hbWVzcGFjZSA9ICdtb2R1bGUtJyArIG5hbWVzcGFjZSxcblxuICAgICRkb2N1bWVudCAgICAgICA9ICQoZG9jdW1lbnQpLFxuICAgICRtb2R1bGUgICAgICAgICA9ICRkb2N1bWVudCxcbiAgICBlbGVtZW50ICAgICAgICAgPSB0aGlzLFxuICAgIGluc3RhbmNlICAgICAgICA9ICRtb2R1bGUuZGF0YShtb2R1bGVOYW1lc3BhY2UpLFxuXG4gICAgbW9kdWxlLFxuICAgIHJldHVybmVkVmFsdWVcbiAgO1xuICBtb2R1bGUgPSB7XG5cbiAgICBpbml0aWFsaXplOiBmdW5jdGlvbigpIHtcbiAgICAgIG1vZHVsZS5pbnN0YW50aWF0ZSgpO1xuICAgIH0sXG5cbiAgICBpbnN0YW50aWF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICBtb2R1bGUudmVyYm9zZSgnU3RvcmluZyBpbnN0YW5jZSBvZiBzaXRlJywgbW9kdWxlKTtcbiAgICAgIGluc3RhbmNlID0gbW9kdWxlO1xuICAgICAgJG1vZHVsZVxuICAgICAgICAuZGF0YShtb2R1bGVOYW1lc3BhY2UsIG1vZHVsZSlcbiAgICAgIDtcbiAgICB9LFxuXG4gICAgbm9ybWFsaXplOiBmdW5jdGlvbigpIHtcbiAgICAgIG1vZHVsZS5maXguY29uc29sZSgpO1xuICAgICAgbW9kdWxlLmZpeC5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUoKTtcbiAgICB9LFxuXG4gICAgZml4OiB7XG4gICAgICBjb25zb2xlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgbW9kdWxlLmRlYnVnKCdOb3JtYWxpemluZyB3aW5kb3cuY29uc29sZScpO1xuICAgICAgICBpZiAoY29uc29sZSA9PT0gdW5kZWZpbmVkIHx8IGNvbnNvbGUubG9nID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQ29uc29sZSBub3QgYXZhaWxhYmxlLCBub3JtYWxpemluZyBldmVudHMnKTtcbiAgICAgICAgICBtb2R1bGUuZGlzYWJsZS5jb25zb2xlKCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHR5cGVvZiBjb25zb2xlLmdyb3VwID09ICd1bmRlZmluZWQnIHx8IHR5cGVvZiBjb25zb2xlLmdyb3VwRW5kID09ICd1bmRlZmluZWQnIHx8IHR5cGVvZiBjb25zb2xlLmdyb3VwQ29sbGFwc2VkID09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0NvbnNvbGUgZ3JvdXAgbm90IGF2YWlsYWJsZSwgbm9ybWFsaXppbmcgZXZlbnRzJyk7XG4gICAgICAgICAgd2luZG93LmNvbnNvbGUuZ3JvdXAgPSBmdW5jdGlvbigpIHt9O1xuICAgICAgICAgIHdpbmRvdy5jb25zb2xlLmdyb3VwRW5kID0gZnVuY3Rpb24oKSB7fTtcbiAgICAgICAgICB3aW5kb3cuY29uc29sZS5ncm91cENvbGxhcHNlZCA9IGZ1bmN0aW9uKCkge307XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHR5cGVvZiBjb25zb2xlLm1hcmtUaW1lbGluZSA9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdNYXJrIHRpbWVsaW5lIG5vdCBhdmFpbGFibGUsIG5vcm1hbGl6aW5nIGV2ZW50cycpO1xuICAgICAgICAgIHdpbmRvdy5jb25zb2xlLm1hcmtUaW1lbGluZSA9IGZ1bmN0aW9uKCkge307XG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICBjb25zb2xlQ2xlYXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICBtb2R1bGUuZGVidWcoJ0Rpc2FibGluZyBwcm9ncmFtbWF0aWMgY29uc29sZSBjbGVhcmluZycpO1xuICAgICAgICB3aW5kb3cuY29uc29sZS5jbGVhciA9IGZ1bmN0aW9uKCkge307XG4gICAgICB9LFxuICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lOiBmdW5jdGlvbigpIHtcbiAgICAgICAgbW9kdWxlLmRlYnVnKCdOb3JtYWxpemluZyByZXF1ZXN0QW5pbWF0aW9uRnJhbWUnKTtcbiAgICAgICAgaWYod2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZXF1ZXN0QW5pbWF0aW9uRnJhbWUgbm90IGF2YWlsYWJsZSwgbm9ybWFsaXppbmcgZXZlbnQnKTtcbiAgICAgICAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lID0gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgICAgICAgfHwgd2luZG93Lm1velJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgICAgICAgfHwgd2luZG93LndlYmtpdFJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgICAgICAgfHwgd2luZG93Lm1zUmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gICAgICAgICAgICB8fCBmdW5jdGlvbihjYWxsYmFjaykgeyBzZXRUaW1lb3V0KGNhbGxiYWNrLCAwKTsgfVxuICAgICAgICAgIDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sXG5cbiAgICBtb2R1bGVFeGlzdHM6IGZ1bmN0aW9uKG5hbWUpIHtcbiAgICAgIHJldHVybiAoJC5mbltuYW1lXSAhPT0gdW5kZWZpbmVkICYmICQuZm5bbmFtZV0uc2V0dGluZ3MgIT09IHVuZGVmaW5lZCk7XG4gICAgfSxcblxuICAgIGVuYWJsZWQ6IHtcbiAgICAgIG1vZHVsZXM6IGZ1bmN0aW9uKG1vZHVsZXMpIHtcbiAgICAgICAgdmFyXG4gICAgICAgICAgZW5hYmxlZE1vZHVsZXMgPSBbXVxuICAgICAgICA7XG4gICAgICAgIG1vZHVsZXMgPSBtb2R1bGVzIHx8IHNldHRpbmdzLm1vZHVsZXM7XG4gICAgICAgICQuZWFjaChtb2R1bGVzLCBmdW5jdGlvbihpbmRleCwgbmFtZSkge1xuICAgICAgICAgIGlmKG1vZHVsZS5tb2R1bGVFeGlzdHMobmFtZSkpIHtcbiAgICAgICAgICAgIGVuYWJsZWRNb2R1bGVzLnB1c2gobmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIGVuYWJsZWRNb2R1bGVzO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICBkaXNhYmxlZDoge1xuICAgICAgbW9kdWxlczogZnVuY3Rpb24obW9kdWxlcykge1xuICAgICAgICB2YXJcbiAgICAgICAgICBkaXNhYmxlZE1vZHVsZXMgPSBbXVxuICAgICAgICA7XG4gICAgICAgIG1vZHVsZXMgPSBtb2R1bGVzIHx8IHNldHRpbmdzLm1vZHVsZXM7XG4gICAgICAgICQuZWFjaChtb2R1bGVzLCBmdW5jdGlvbihpbmRleCwgbmFtZSkge1xuICAgICAgICAgIGlmKCFtb2R1bGUubW9kdWxlRXhpc3RzKG5hbWUpKSB7XG4gICAgICAgICAgICBkaXNhYmxlZE1vZHVsZXMucHVzaChuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gZGlzYWJsZWRNb2R1bGVzO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICBjaGFuZ2U6IHtcbiAgICAgIHNldHRpbmc6IGZ1bmN0aW9uKHNldHRpbmcsIHZhbHVlLCBtb2R1bGVzLCBtb2RpZnlFeGlzdGluZykge1xuICAgICAgICBtb2R1bGVzID0gKHR5cGVvZiBtb2R1bGVzID09PSAnc3RyaW5nJylcbiAgICAgICAgICA/IChtb2R1bGVzID09PSAnYWxsJylcbiAgICAgICAgICAgID8gc2V0dGluZ3MubW9kdWxlc1xuICAgICAgICAgICAgOiBbbW9kdWxlc11cbiAgICAgICAgICA6IG1vZHVsZXMgfHwgc2V0dGluZ3MubW9kdWxlc1xuICAgICAgICA7XG4gICAgICAgIG1vZGlmeUV4aXN0aW5nID0gKG1vZGlmeUV4aXN0aW5nICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgPyBtb2RpZnlFeGlzdGluZ1xuICAgICAgICAgIDogdHJ1ZVxuICAgICAgICA7XG4gICAgICAgICQuZWFjaChtb2R1bGVzLCBmdW5jdGlvbihpbmRleCwgbmFtZSkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgbmFtZXNwYWNlID0gKG1vZHVsZS5tb2R1bGVFeGlzdHMobmFtZSkpXG4gICAgICAgICAgICAgID8gJC5mbltuYW1lXS5zZXR0aW5ncy5uYW1lc3BhY2UgfHwgZmFsc2VcbiAgICAgICAgICAgICAgOiB0cnVlLFxuICAgICAgICAgICAgJGV4aXN0aW5nTW9kdWxlc1xuICAgICAgICAgIDtcbiAgICAgICAgICBpZihtb2R1bGUubW9kdWxlRXhpc3RzKG5hbWUpKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQ2hhbmdpbmcgZGVmYXVsdCBzZXR0aW5nJywgc2V0dGluZywgdmFsdWUsIG5hbWUpO1xuICAgICAgICAgICAgJC5mbltuYW1lXS5zZXR0aW5nc1tzZXR0aW5nXSA9IHZhbHVlO1xuICAgICAgICAgICAgaWYobW9kaWZ5RXhpc3RpbmcgJiYgbmFtZXNwYWNlKSB7XG4gICAgICAgICAgICAgICRleGlzdGluZ01vZHVsZXMgPSAkKCc6ZGF0YShtb2R1bGUtJyArIG5hbWVzcGFjZSArICcpJyk7XG4gICAgICAgICAgICAgIGlmKCRleGlzdGluZ01vZHVsZXMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdNb2RpZnlpbmcgZXhpc3Rpbmcgc2V0dGluZ3MnLCAkZXhpc3RpbmdNb2R1bGVzKTtcbiAgICAgICAgICAgICAgICAkZXhpc3RpbmdNb2R1bGVzW25hbWVdKCdzZXR0aW5nJywgc2V0dGluZywgdmFsdWUpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0sXG4gICAgICBzZXR0aW5nczogZnVuY3Rpb24obmV3U2V0dGluZ3MsIG1vZHVsZXMsIG1vZGlmeUV4aXN0aW5nKSB7XG4gICAgICAgIG1vZHVsZXMgPSAodHlwZW9mIG1vZHVsZXMgPT09ICdzdHJpbmcnKVxuICAgICAgICAgID8gW21vZHVsZXNdXG4gICAgICAgICAgOiBtb2R1bGVzIHx8IHNldHRpbmdzLm1vZHVsZXNcbiAgICAgICAgO1xuICAgICAgICBtb2RpZnlFeGlzdGluZyA9IChtb2RpZnlFeGlzdGluZyAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgID8gbW9kaWZ5RXhpc3RpbmdcbiAgICAgICAgICA6IHRydWVcbiAgICAgICAgO1xuICAgICAgICAkLmVhY2gobW9kdWxlcywgZnVuY3Rpb24oaW5kZXgsIG5hbWUpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgICRleGlzdGluZ01vZHVsZXNcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYobW9kdWxlLm1vZHVsZUV4aXN0cyhuYW1lKSkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0NoYW5naW5nIGRlZmF1bHQgc2V0dGluZycsIG5ld1NldHRpbmdzLCBuYW1lKTtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsICQuZm5bbmFtZV0uc2V0dGluZ3MsIG5ld1NldHRpbmdzKTtcbiAgICAgICAgICAgIGlmKG1vZGlmeUV4aXN0aW5nICYmIG5hbWVzcGFjZSkge1xuICAgICAgICAgICAgICAkZXhpc3RpbmdNb2R1bGVzID0gJCgnOmRhdGEobW9kdWxlLScgKyBuYW1lc3BhY2UgKyAnKScpO1xuICAgICAgICAgICAgICBpZigkZXhpc3RpbmdNb2R1bGVzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnTW9kaWZ5aW5nIGV4aXN0aW5nIHNldHRpbmdzJywgJGV4aXN0aW5nTW9kdWxlcyk7XG4gICAgICAgICAgICAgICAgJGV4aXN0aW5nTW9kdWxlc1tuYW1lXSgnc2V0dGluZycsIG5ld1NldHRpbmdzKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfSxcblxuICAgIGVuYWJsZToge1xuICAgICAgY29uc29sZTogZnVuY3Rpb24oKSB7XG4gICAgICAgIG1vZHVsZS5jb25zb2xlKHRydWUpO1xuICAgICAgfSxcbiAgICAgIGRlYnVnOiBmdW5jdGlvbihtb2R1bGVzLCBtb2RpZnlFeGlzdGluZykge1xuICAgICAgICBtb2R1bGVzID0gbW9kdWxlcyB8fCBzZXR0aW5ncy5tb2R1bGVzO1xuICAgICAgICBtb2R1bGUuZGVidWcoJ0VuYWJsaW5nIGRlYnVnIGZvciBtb2R1bGVzJywgbW9kdWxlcyk7XG4gICAgICAgIG1vZHVsZS5jaGFuZ2Uuc2V0dGluZygnZGVidWcnLCB0cnVlLCBtb2R1bGVzLCBtb2RpZnlFeGlzdGluZyk7XG4gICAgICB9LFxuICAgICAgdmVyYm9zZTogZnVuY3Rpb24obW9kdWxlcywgbW9kaWZ5RXhpc3RpbmcpIHtcbiAgICAgICAgbW9kdWxlcyA9IG1vZHVsZXMgfHwgc2V0dGluZ3MubW9kdWxlcztcbiAgICAgICAgbW9kdWxlLmRlYnVnKCdFbmFibGluZyB2ZXJib3NlIGRlYnVnIGZvciBtb2R1bGVzJywgbW9kdWxlcyk7XG4gICAgICAgIG1vZHVsZS5jaGFuZ2Uuc2V0dGluZygndmVyYm9zZScsIHRydWUsIG1vZHVsZXMsIG1vZGlmeUV4aXN0aW5nKTtcbiAgICAgIH1cbiAgICB9LFxuICAgIGRpc2FibGU6IHtcbiAgICAgIGNvbnNvbGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICBtb2R1bGUuY29uc29sZShmYWxzZSk7XG4gICAgICB9LFxuICAgICAgZGVidWc6IGZ1bmN0aW9uKG1vZHVsZXMsIG1vZGlmeUV4aXN0aW5nKSB7XG4gICAgICAgIG1vZHVsZXMgPSBtb2R1bGVzIHx8IHNldHRpbmdzLm1vZHVsZXM7XG4gICAgICAgIG1vZHVsZS5kZWJ1ZygnRGlzYWJsaW5nIGRlYnVnIGZvciBtb2R1bGVzJywgbW9kdWxlcyk7XG4gICAgICAgIG1vZHVsZS5jaGFuZ2Uuc2V0dGluZygnZGVidWcnLCBmYWxzZSwgbW9kdWxlcywgbW9kaWZ5RXhpc3RpbmcpO1xuICAgICAgfSxcbiAgICAgIHZlcmJvc2U6IGZ1bmN0aW9uKG1vZHVsZXMsIG1vZGlmeUV4aXN0aW5nKSB7XG4gICAgICAgIG1vZHVsZXMgPSBtb2R1bGVzIHx8IHNldHRpbmdzLm1vZHVsZXM7XG4gICAgICAgIG1vZHVsZS5kZWJ1ZygnRGlzYWJsaW5nIHZlcmJvc2UgZGVidWcgZm9yIG1vZHVsZXMnLCBtb2R1bGVzKTtcbiAgICAgICAgbW9kdWxlLmNoYW5nZS5zZXR0aW5nKCd2ZXJib3NlJywgZmFsc2UsIG1vZHVsZXMsIG1vZGlmeUV4aXN0aW5nKTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgY29uc29sZTogZnVuY3Rpb24oZW5hYmxlKSB7XG4gICAgICBpZihlbmFibGUpIHtcbiAgICAgICAgaWYoaW5zdGFuY2UuY2FjaGUuY29uc29sZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLmNvbnNvbGUpO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBtb2R1bGUuZGVidWcoJ1Jlc3RvcmluZyBjb25zb2xlIGZ1bmN0aW9uJyk7XG4gICAgICAgIHdpbmRvdy5jb25zb2xlID0gaW5zdGFuY2UuY2FjaGUuY29uc29sZTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBtb2R1bGUuZGVidWcoJ0Rpc2FibGluZyBjb25zb2xlIGZ1bmN0aW9uJyk7XG4gICAgICAgIGluc3RhbmNlLmNhY2hlLmNvbnNvbGUgPSB3aW5kb3cuY29uc29sZTtcbiAgICAgICAgd2luZG93LmNvbnNvbGUgPSB7XG4gICAgICAgICAgY2xlYXIgICAgICAgICAgOiBmdW5jdGlvbigpe30sXG4gICAgICAgICAgZXJyb3IgICAgICAgICAgOiBmdW5jdGlvbigpe30sXG4gICAgICAgICAgZ3JvdXAgICAgICAgICAgOiBmdW5jdGlvbigpe30sXG4gICAgICAgICAgZ3JvdXBDb2xsYXBzZWQgOiBmdW5jdGlvbigpe30sXG4gICAgICAgICAgZ3JvdXBFbmQgICAgICAgOiBmdW5jdGlvbigpe30sXG4gICAgICAgICAgaW5mbyAgICAgICAgICAgOiBmdW5jdGlvbigpe30sXG4gICAgICAgICAgbG9nICAgICAgICAgICAgOiBmdW5jdGlvbigpe30sXG4gICAgICAgICAgbWFya1RpbWVsaW5lICAgOiBmdW5jdGlvbigpe30sXG4gICAgICAgICAgd2FybiAgICAgICAgICAgOiBmdW5jdGlvbigpe31cbiAgICAgICAgfTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgZGVzdHJveTogZnVuY3Rpb24oKSB7XG4gICAgICBtb2R1bGUudmVyYm9zZSgnRGVzdHJveWluZyBwcmV2aW91cyBzaXRlIGZvcicsICRtb2R1bGUpO1xuICAgICAgJG1vZHVsZVxuICAgICAgICAucmVtb3ZlRGF0YShtb2R1bGVOYW1lc3BhY2UpXG4gICAgICA7XG4gICAgfSxcblxuICAgIGNhY2hlOiB7fSxcblxuICAgIHNldHRpbmc6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAkLmV4dGVuZCh0cnVlLCBzZXR0aW5ncywgbmFtZSk7XG4gICAgICB9XG4gICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgc2V0dGluZ3NbbmFtZV0gPSB2YWx1ZTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICByZXR1cm4gc2V0dGluZ3NbbmFtZV07XG4gICAgICB9XG4gICAgfSxcbiAgICBpbnRlcm5hbDogZnVuY3Rpb24obmFtZSwgdmFsdWUpIHtcbiAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICQuZXh0ZW5kKHRydWUsIG1vZHVsZSwgbmFtZSk7XG4gICAgICB9XG4gICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgbW9kdWxlW25hbWVdID0gdmFsdWU7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgcmV0dXJuIG1vZHVsZVtuYW1lXTtcbiAgICAgIH1cbiAgICB9LFxuICAgIGRlYnVnOiBmdW5jdGlvbigpIHtcbiAgICAgIGlmKHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZyA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICBtb2R1bGUuZGVidWcuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sXG4gICAgdmVyYm9zZTogZnVuY3Rpb24oKSB7XG4gICAgICBpZihzZXR0aW5ncy52ZXJib3NlICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmluZm8sIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuICAgIGVycm9yOiBmdW5jdGlvbigpIHtcbiAgICAgIG1vZHVsZS5lcnJvciA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5lcnJvciwgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICBtb2R1bGUuZXJyb3IuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICB9LFxuICAgIHBlcmZvcm1hbmNlOiB7XG4gICAgICBsb2c6IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcbiAgICAgICAgdmFyXG4gICAgICAgICAgY3VycmVudFRpbWUsXG4gICAgICAgICAgZXhlY3V0aW9uVGltZSxcbiAgICAgICAgICBwcmV2aW91c1RpbWVcbiAgICAgICAgO1xuICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgIGN1cnJlbnRUaW1lICAgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgICAgICBwcmV2aW91c1RpbWUgID0gdGltZSB8fCBjdXJyZW50VGltZTtcbiAgICAgICAgICBleGVjdXRpb25UaW1lID0gY3VycmVudFRpbWUgLSBwcmV2aW91c1RpbWU7XG4gICAgICAgICAgdGltZSAgICAgICAgICA9IGN1cnJlbnRUaW1lO1xuICAgICAgICAgIHBlcmZvcm1hbmNlLnB1c2goe1xuICAgICAgICAgICAgJ0VsZW1lbnQnICAgICAgICA6IGVsZW1lbnQsXG4gICAgICAgICAgICAnTmFtZScgICAgICAgICAgIDogbWVzc2FnZVswXSxcbiAgICAgICAgICAgICdBcmd1bWVudHMnICAgICAgOiBbXS5zbGljZS5jYWxsKG1lc3NhZ2UsIDEpIHx8ICcnLFxuICAgICAgICAgICAgJ0V4ZWN1dGlvbiBUaW1lJyA6IGV4ZWN1dGlvblRpbWVcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyID0gc2V0VGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UuZGlzcGxheSwgNTAwKTtcbiAgICAgIH0sXG4gICAgICBkaXNwbGF5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyXG4gICAgICAgICAgdGl0bGUgPSBzZXR0aW5ncy5uYW1lICsgJzonLFxuICAgICAgICAgIHRvdGFsVGltZSA9IDBcbiAgICAgICAgO1xuICAgICAgICB0aW1lID0gZmFsc2U7XG4gICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAkLmVhY2gocGVyZm9ybWFuY2UsIGZ1bmN0aW9uKGluZGV4LCBkYXRhKSB7XG4gICAgICAgICAgdG90YWxUaW1lICs9IGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ107XG4gICAgICAgIH0pO1xuICAgICAgICB0aXRsZSArPSAnICcgKyB0b3RhbFRpbWUgKyAnbXMnO1xuICAgICAgICBpZiggKGNvbnNvbGUuZ3JvdXAgIT09IHVuZGVmaW5lZCB8fCBjb25zb2xlLnRhYmxlICE9PSB1bmRlZmluZWQpICYmIHBlcmZvcm1hbmNlLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKHRpdGxlKTtcbiAgICAgICAgICBpZihjb25zb2xlLnRhYmxlKSB7XG4gICAgICAgICAgICBjb25zb2xlLnRhYmxlKHBlcmZvcm1hbmNlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAkLmVhY2gocGVyZm9ybWFuY2UsIGZ1bmN0aW9uKGluZGV4LCBkYXRhKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGFbJ05hbWUnXSArICc6ICcgKyBkYXRhWydFeGVjdXRpb24gVGltZSddKydtcycpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcbiAgICAgICAgfVxuICAgICAgICBwZXJmb3JtYW5jZSA9IFtdO1xuICAgICAgfVxuICAgIH0sXG4gICAgaW52b2tlOiBmdW5jdGlvbihxdWVyeSwgcGFzc2VkQXJndW1lbnRzLCBjb250ZXh0KSB7XG4gICAgICB2YXJcbiAgICAgICAgb2JqZWN0ID0gaW5zdGFuY2UsXG4gICAgICAgIG1heERlcHRoLFxuICAgICAgICBmb3VuZCxcbiAgICAgICAgcmVzcG9uc2VcbiAgICAgIDtcbiAgICAgIHBhc3NlZEFyZ3VtZW50cyA9IHBhc3NlZEFyZ3VtZW50cyB8fCBxdWVyeUFyZ3VtZW50cztcbiAgICAgIGNvbnRleHQgICAgICAgICA9IGVsZW1lbnQgICAgICAgICB8fCBjb250ZXh0O1xuICAgICAgaWYodHlwZW9mIHF1ZXJ5ID09ICdzdHJpbmcnICYmIG9iamVjdCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHF1ZXJ5ICAgID0gcXVlcnkuc3BsaXQoL1tcXC4gXS8pO1xuICAgICAgICBtYXhEZXB0aCA9IHF1ZXJ5Lmxlbmd0aCAtIDE7XG4gICAgICAgICQuZWFjaChxdWVyeSwgZnVuY3Rpb24oZGVwdGgsIHZhbHVlKSB7XG4gICAgICAgICAgdmFyIGNhbWVsQ2FzZVZhbHVlID0gKGRlcHRoICE9IG1heERlcHRoKVxuICAgICAgICAgICAgPyB2YWx1ZSArIHF1ZXJ5W2RlcHRoICsgMV0uY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBxdWVyeVtkZXB0aCArIDFdLnNsaWNlKDEpXG4gICAgICAgICAgICA6IHF1ZXJ5XG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gKSAmJiAoZGVwdGggIT0gbWF4RGVwdGgpICkge1xuICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZiggb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKCAkLmlzUGxhaW5PYmplY3QoIG9iamVjdFt2YWx1ZV0gKSAmJiAoZGVwdGggIT0gbWF4RGVwdGgpICkge1xuICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZiggb2JqZWN0W3ZhbHVlXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5tZXRob2QsIHF1ZXJ5KTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgICAgaWYgKCAkLmlzRnVuY3Rpb24oIGZvdW5kICkgKSB7XG4gICAgICAgIHJlc3BvbnNlID0gZm91bmQuYXBwbHkoY29udGV4dCwgcGFzc2VkQXJndW1lbnRzKTtcbiAgICAgIH1cbiAgICAgIGVsc2UgaWYoZm91bmQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICByZXNwb25zZSA9IGZvdW5kO1xuICAgICAgfVxuICAgICAgaWYoJC5pc0FycmF5KHJldHVybmVkVmFsdWUpKSB7XG4gICAgICAgIHJldHVybmVkVmFsdWUucHVzaChyZXNwb25zZSk7XG4gICAgICB9XG4gICAgICBlbHNlIGlmKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICByZXR1cm5lZFZhbHVlID0gW3JldHVybmVkVmFsdWUsIHJlc3BvbnNlXTtcbiAgICAgIH1cbiAgICAgIGVsc2UgaWYocmVzcG9uc2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICByZXR1cm5lZFZhbHVlID0gcmVzcG9uc2U7XG4gICAgICB9XG4gICAgICByZXR1cm4gZm91bmQ7XG4gICAgfVxuICB9O1xuXG4gIGlmKG1ldGhvZEludm9rZWQpIHtcbiAgICBpZihpbnN0YW5jZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgIH1cbiAgICBtb2R1bGUuaW52b2tlKHF1ZXJ5KTtcbiAgfVxuICBlbHNlIHtcbiAgICBpZihpbnN0YW5jZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICBtb2R1bGUuZGVzdHJveSgpO1xuICAgIH1cbiAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICB9XG4gIHJldHVybiAocmV0dXJuZWRWYWx1ZSAhPT0gdW5kZWZpbmVkKVxuICAgID8gcmV0dXJuZWRWYWx1ZVxuICAgIDogdGhpc1xuICA7XG59O1xuXG4kLnNpdGUuc2V0dGluZ3MgPSB7XG5cbiAgbmFtZSAgICAgICAgOiAnU2l0ZScsXG4gIG5hbWVzcGFjZSAgIDogJ3NpdGUnLFxuXG4gIGVycm9yIDoge1xuICAgIGNvbnNvbGUgOiAnQ29uc29sZSBjYW5ub3QgYmUgcmVzdG9yZWQsIG1vc3QgbGlrZWx5IGl0IHdhcyBvdmVyd3JpdHRlbiBvdXRzaWRlIG9mIG1vZHVsZScsXG4gICAgbWV0aG9kIDogJ1RoZSBtZXRob2QgeW91IGNhbGxlZCBpcyBub3QgZGVmaW5lZC4nXG4gIH0sXG5cbiAgZGVidWcgICAgICAgOiBmYWxzZSxcbiAgdmVyYm9zZSAgICAgOiBmYWxzZSxcbiAgcGVyZm9ybWFuY2UgOiB0cnVlLFxuXG4gIG1vZHVsZXM6IFtcbiAgICAnYWNjb3JkaW9uJyxcbiAgICAnYXBpJyxcbiAgICAnY2hlY2tib3gnLFxuICAgICdkaW1tZXInLFxuICAgICdkcm9wZG93bicsXG4gICAgJ2VtYmVkJyxcbiAgICAnZm9ybScsXG4gICAgJ21vZGFsJyxcbiAgICAnbmFnJyxcbiAgICAncG9wdXAnLFxuICAgICdyYXRpbmcnLFxuICAgICdzaGFwZScsXG4gICAgJ3NpZGViYXInLFxuICAgICdzdGF0ZScsXG4gICAgJ3N0aWNreScsXG4gICAgJ3RhYicsXG4gICAgJ3RyYW5zaXRpb24nLFxuICAgICd2aXNpdCcsXG4gICAgJ3Zpc2liaWxpdHknXG4gIF0sXG5cbiAgc2l0ZU5hbWVzcGFjZSAgIDogJ3NpdGUnLFxuICBuYW1lc3BhY2VTdHViICAgOiB7XG4gICAgY2FjaGUgICAgIDoge30sXG4gICAgY29uZmlnICAgIDoge30sXG4gICAgc2VjdGlvbnMgIDoge30sXG4gICAgc2VjdGlvbiAgIDoge30sXG4gICAgdXRpbGl0aWVzIDoge31cbiAgfVxuXG59O1xuXG4vLyBhbGxvd3MgZm9yIHNlbGVjdGlvbiBvZiBlbGVtZW50cyB3aXRoIGRhdGEgYXR0cmlidXRlc1xuJC5leHRlbmQoJC5leHByWyBcIjpcIiBdLCB7XG4gIGRhdGE6ICgkLmV4cHIuY3JlYXRlUHNldWRvKVxuICAgID8gJC5leHByLmNyZWF0ZVBzZXVkbyhmdW5jdGlvbihkYXRhTmFtZSkge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24oZWxlbSkge1xuICAgICAgICAgIHJldHVybiAhISQuZGF0YShlbGVtLCBkYXRhTmFtZSk7XG4gICAgICAgIH07XG4gICAgICB9KVxuICAgIDogZnVuY3Rpb24oZWxlbSwgaSwgbWF0Y2gpIHtcbiAgICAgIC8vIHN1cHBvcnQ6IGpRdWVyeSA8IDEuOFxuICAgICAgcmV0dXJuICEhJC5kYXRhKGVsZW0sIG1hdGNoWyAzIF0pO1xuICAgIH1cbn0pO1xuXG5cbn0pKCBqUXVlcnksIHdpbmRvdywgZG9jdW1lbnQgKTtcbiJdfQ==