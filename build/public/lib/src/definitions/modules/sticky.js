/*!
 * # Semantic UI - Sticky
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.sticky = function (parameters) {
    var $allModules = $(this),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue;

    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.sticky.settings, parameters) : $.extend({}, $.fn.sticky.settings),
          className = settings.className,
          namespace = settings.namespace,
          error = settings.error,
          eventNamespace = '.' + namespace,
          moduleNamespace = 'module-' + namespace,
          $module = $(this),
          $window = $(window),
          $scroll = $(settings.scrollContext),
          $container,
          $context,
          selector = $module.selector || '',
          instance = $module.data(moduleNamespace),
          requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
        setTimeout(callback, 0);
      },
          element = this,
          documentObserver,
          observer,
          module;

      module = {

        initialize: function () {

          module.determineContainer();
          module.determineContext();
          module.verbose('Initializing sticky', settings, $container);

          module.save.positions();
          module.checkErrors();
          module.bind.events();

          if (settings.observeChanges) {
            module.observeChanges();
          }
          module.instantiate();
        },

        instantiate: function () {
          module.verbose('Storing instance of module', module);
          instance = module;
          $module.data(moduleNamespace, module);
        },

        destroy: function () {
          module.verbose('Destroying previous instance');
          module.reset();
          if (documentObserver) {
            documentObserver.disconnect();
          }
          if (observer) {
            observer.disconnect();
          }
          $window.off('load' + eventNamespace, module.event.load).off('resize' + eventNamespace, module.event.resize);
          $scroll.off('scrollchange' + eventNamespace, module.event.scrollchange);
          $module.removeData(moduleNamespace);
        },

        observeChanges: function () {
          if ('MutationObserver' in window) {
            documentObserver = new MutationObserver(module.event.documentChanged);
            observer = new MutationObserver(module.event.changed);
            documentObserver.observe(document, {
              childList: true,
              subtree: true
            });
            observer.observe(element, {
              childList: true,
              subtree: true
            });
            observer.observe($context[0], {
              childList: true,
              subtree: true
            });
            module.debug('Setting up mutation observer', observer);
          }
        },

        determineContainer: function () {
          $container = $module.offsetParent();
        },

        determineContext: function () {
          if (settings.context) {
            $context = $(settings.context);
          } else {
            $context = $container;
          }
          if ($context.length === 0) {
            module.error(error.invalidContext, settings.context, $module);
            return;
          }
        },

        checkErrors: function () {
          if (module.is.hidden()) {
            module.error(error.visible, $module);
          }
          if (module.cache.element.height > module.cache.context.height) {
            module.reset();
            module.error(error.elementSize, $module);
            return;
          }
        },

        bind: {
          events: function () {
            $window.on('load' + eventNamespace, module.event.load).on('resize' + eventNamespace, module.event.resize);
            // pub/sub pattern
            $scroll.off('scroll' + eventNamespace).on('scroll' + eventNamespace, module.event.scroll).on('scrollchange' + eventNamespace, module.event.scrollchange);
          }
        },

        event: {
          changed: function (mutations) {
            clearTimeout(module.timer);
            module.timer = setTimeout(function () {
              module.verbose('DOM tree modified, updating sticky menu', mutations);
              module.refresh();
            }, 100);
          },
          documentChanged: function (mutations) {
            [].forEach.call(mutations, function (mutation) {
              if (mutation.removedNodes) {
                [].forEach.call(mutation.removedNodes, function (node) {
                  if (node == element || $(node).find(element).length > 0) {
                    module.debug('Element removed from DOM, tearing down events');
                    module.destroy();
                  }
                });
              }
            });
          },
          load: function () {
            module.verbose('Page contents finished loading');
            requestAnimationFrame(module.refresh);
          },
          resize: function () {
            module.verbose('Window resized');
            requestAnimationFrame(module.refresh);
          },
          scroll: function () {
            requestAnimationFrame(function () {
              $scroll.triggerHandler('scrollchange' + eventNamespace, $scroll.scrollTop());
            });
          },
          scrollchange: function (event, scrollPosition) {
            module.stick(scrollPosition);
            settings.onScroll.call(element);
          }
        },

        refresh: function (hardRefresh) {
          module.reset();
          if (!settings.context) {
            module.determineContext();
          }
          if (hardRefresh) {
            module.determineContainer();
          }
          module.save.positions();
          module.stick();
          settings.onReposition.call(element);
        },

        supports: {
          sticky: function () {
            var $element = $('<div/>'),
                element = $element[0];
            $element.addClass(className.supported);
            return $element.css('position').match('sticky');
          }
        },

        save: {
          lastScroll: function (scroll) {
            module.lastScroll = scroll;
          },
          elementScroll: function (scroll) {
            module.elementScroll = scroll;
          },
          positions: function () {
            var scrollContext = {
              height: $scroll.height()
            },
                element = {
              margin: {
                top: parseInt($module.css('margin-top'), 10),
                bottom: parseInt($module.css('margin-bottom'), 10)
              },
              offset: $module.offset(),
              width: $module.outerWidth(),
              height: $module.outerHeight()
            },
                context = {
              offset: $context.offset(),
              height: $context.outerHeight()
            },
                container = {
              height: $container.outerHeight()
            };
            if (!module.is.standardScroll()) {
              module.debug('Non-standard scroll. Removing scroll offset from element offset');

              scrollContext.top = $scroll.scrollTop();
              scrollContext.left = $scroll.scrollLeft();

              element.offset.top += scrollContext.top;
              context.offset.top += scrollContext.top;
              element.offset.left += scrollContext.left;
              context.offset.left += scrollContext.left;
            }
            module.cache = {
              fits: element.height < scrollContext.height,
              scrollContext: {
                height: scrollContext.height
              },
              element: {
                margin: element.margin,
                top: element.offset.top - element.margin.top,
                left: element.offset.left,
                width: element.width,
                height: element.height,
                bottom: element.offset.top + element.height
              },
              context: {
                top: context.offset.top,
                height: context.height,
                bottom: context.offset.top + context.height
              }
            };
            module.set.containerSize();
            module.set.size();
            module.stick();
            module.debug('Caching element positions', module.cache);
          }
        },

        get: {
          direction: function (scroll) {
            var direction = 'down';
            scroll = scroll || $scroll.scrollTop();
            if (module.lastScroll !== undefined) {
              if (module.lastScroll < scroll) {
                direction = 'down';
              } else if (module.lastScroll > scroll) {
                direction = 'up';
              }
            }
            return direction;
          },
          scrollChange: function (scroll) {
            scroll = scroll || $scroll.scrollTop();
            return module.lastScroll ? scroll - module.lastScroll : 0;
          },
          currentElementScroll: function () {
            if (module.elementScroll) {
              return module.elementScroll;
            }
            return module.is.top() ? Math.abs(parseInt($module.css('top'), 10)) || 0 : Math.abs(parseInt($module.css('bottom'), 10)) || 0;
          },

          elementScroll: function (scroll) {
            scroll = scroll || $scroll.scrollTop();
            var element = module.cache.element,
                scrollContext = module.cache.scrollContext,
                delta = module.get.scrollChange(scroll),
                maxScroll = element.height - scrollContext.height + settings.offset,
                elementScroll = module.get.currentElementScroll(),
                possibleScroll = elementScroll + delta;
            if (module.cache.fits || possibleScroll < 0) {
              elementScroll = 0;
            } else if (possibleScroll > maxScroll) {
              elementScroll = maxScroll;
            } else {
              elementScroll = possibleScroll;
            }
            return elementScroll;
          }
        },

        remove: {
          lastScroll: function () {
            delete module.lastScroll;
          },
          elementScroll: function (scroll) {
            delete module.elementScroll;
          },
          offset: function () {
            $module.css('margin-top', '');
          }
        },

        set: {
          offset: function () {
            module.verbose('Setting offset on element', settings.offset);
            $module.css('margin-top', settings.offset);
          },
          containerSize: function () {
            var tagName = $container.get(0).tagName;
            if (tagName === 'HTML' || tagName == 'body') {
              // this can trigger for too many reasons
              //module.error(error.container, tagName, $module);
              module.determineContainer();
            } else {
              if (Math.abs($container.outerHeight() - module.cache.context.height) > settings.jitter) {
                module.debug('Context has padding, specifying exact height for container', module.cache.context.height);
                $container.css({
                  height: module.cache.context.height
                });
              }
            }
          },
          minimumSize: function () {
            var element = module.cache.element;
            $container.css('min-height', element.height);
          },
          scroll: function (scroll) {
            module.debug('Setting scroll on element', scroll);
            if (module.elementScroll == scroll) {
              return;
            }
            if (module.is.top()) {
              $module.css('bottom', '').css('top', -scroll);
            }
            if (module.is.bottom()) {
              $module.css('top', '').css('bottom', scroll);
            }
          },
          size: function () {
            if (module.cache.element.height !== 0 && module.cache.element.width !== 0) {
              element.style.setProperty('width', module.cache.element.width + 'px', 'important');
              element.style.setProperty('height', module.cache.element.height + 'px', 'important');
            }
          }
        },

        is: {
          standardScroll: function () {
            return $scroll[0] == window;
          },
          top: function () {
            return $module.hasClass(className.top);
          },
          bottom: function () {
            return $module.hasClass(className.bottom);
          },
          initialPosition: function () {
            return !module.is.fixed() && !module.is.bound();
          },
          hidden: function () {
            return !$module.is(':visible');
          },
          bound: function () {
            return $module.hasClass(className.bound);
          },
          fixed: function () {
            return $module.hasClass(className.fixed);
          }
        },

        stick: function (scroll) {
          var cachedPosition = scroll || $scroll.scrollTop(),
              cache = module.cache,
              fits = cache.fits,
              element = cache.element,
              scrollContext = cache.scrollContext,
              context = cache.context,
              offset = module.is.bottom() && settings.pushing ? settings.bottomOffset : settings.offset,
              scroll = {
            top: cachedPosition + offset,
            bottom: cachedPosition + offset + scrollContext.height
          },
              direction = module.get.direction(scroll.top),
              elementScroll = fits ? 0 : module.get.elementScroll(scroll.top),


          // shorthand
          doesntFit = !fits,
              elementVisible = element.height !== 0;

          if (elementVisible) {

            if (module.is.initialPosition()) {
              if (scroll.top >= context.bottom) {
                module.debug('Initial element position is bottom of container');
                module.bindBottom();
              } else if (scroll.top > element.top) {
                if (element.height + scroll.top - elementScroll >= context.bottom) {
                  module.debug('Initial element position is bottom of container');
                  module.bindBottom();
                } else {
                  module.debug('Initial element position is fixed');
                  module.fixTop();
                }
              }
            } else if (module.is.fixed()) {

              // currently fixed top
              if (module.is.top()) {
                if (scroll.top <= element.top) {
                  module.debug('Fixed element reached top of container');
                  module.setInitialPosition();
                } else if (element.height + scroll.top - elementScroll >= context.bottom) {
                  module.debug('Fixed element reached bottom of container');
                  module.bindBottom();
                }
                // scroll element if larger than screen
                else if (doesntFit) {
                    module.set.scroll(elementScroll);
                    module.save.lastScroll(scroll.top);
                    module.save.elementScroll(elementScroll);
                  }
              }

              // currently fixed bottom
              else if (module.is.bottom()) {

                  // top edge
                  if (scroll.bottom - element.height <= element.top) {
                    module.debug('Bottom fixed rail has reached top of container');
                    module.setInitialPosition();
                  }
                  // bottom edge
                  else if (scroll.bottom >= context.bottom) {
                      module.debug('Bottom fixed rail has reached bottom of container');
                      module.bindBottom();
                    }
                    // scroll element if larger than screen
                    else if (doesntFit) {
                        module.set.scroll(elementScroll);
                        module.save.lastScroll(scroll.top);
                        module.save.elementScroll(elementScroll);
                      }
                }
            } else if (module.is.bottom()) {
              if (scroll.top <= element.top) {
                module.debug('Jumped from bottom fixed to top fixed, most likely used home/end button');
                module.setInitialPosition();
              } else {
                if (settings.pushing) {
                  if (module.is.bound() && scroll.bottom <= context.bottom) {
                    module.debug('Fixing bottom attached element to bottom of browser.');
                    module.fixBottom();
                  }
                } else {
                  if (module.is.bound() && scroll.top <= context.bottom - element.height) {
                    module.debug('Fixing bottom attached element to top of browser.');
                    module.fixTop();
                  }
                }
              }
            }
          }
        },

        bindTop: function () {
          module.debug('Binding element to top of parent container');
          module.remove.offset();
          $module.css({
            left: '',
            top: '',
            marginBottom: ''
          }).removeClass(className.fixed).removeClass(className.bottom).addClass(className.bound).addClass(className.top);
          settings.onTop.call(element);
          settings.onUnstick.call(element);
        },
        bindBottom: function () {
          module.debug('Binding element to bottom of parent container');
          module.remove.offset();
          $module.css({
            left: '',
            top: ''
          }).removeClass(className.fixed).removeClass(className.top).addClass(className.bound).addClass(className.bottom);
          settings.onBottom.call(element);
          settings.onUnstick.call(element);
        },

        setInitialPosition: function () {
          module.debug('Returning to initial position');
          module.unfix();
          module.unbind();
        },

        fixTop: function () {
          module.debug('Fixing element to top of page');
          module.set.minimumSize();
          module.set.offset();
          $module.css({
            left: module.cache.element.left,
            bottom: '',
            marginBottom: ''
          }).removeClass(className.bound).removeClass(className.bottom).addClass(className.fixed).addClass(className.top);
          settings.onStick.call(element);
        },

        fixBottom: function () {
          module.debug('Sticking element to bottom of page');
          module.set.minimumSize();
          module.set.offset();
          $module.css({
            left: module.cache.element.left,
            bottom: '',
            marginBottom: ''
          }).removeClass(className.bound).removeClass(className.top).addClass(className.fixed).addClass(className.bottom);
          settings.onStick.call(element);
        },

        unbind: function () {
          if (module.is.bound()) {
            module.debug('Removing container bound position on element');
            module.remove.offset();
            $module.removeClass(className.bound).removeClass(className.top).removeClass(className.bottom);
          }
        },

        unfix: function () {
          if (module.is.fixed()) {
            module.debug('Removing fixed position on element');
            module.remove.offset();
            $module.removeClass(className.fixed).removeClass(className.top).removeClass(className.bottom);
            settings.onUnstick.call(element);
          }
        },

        reset: function () {
          module.debug('Resetting elements position');
          module.unbind();
          module.unfix();
          module.resetCSS();
          module.remove.offset();
          module.remove.lastScroll();
        },

        resetCSS: function () {
          $module.css({
            width: '',
            height: ''
          });
          $container.css({
            height: ''
          });
        },

        setting: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            settings[name] = value;
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 0);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };

      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.sticky.settings = {

    name: 'Sticky',
    namespace: 'sticky',

    silent: false,
    debug: false,
    verbose: true,
    performance: true,

    // whether to stick in the opposite direction on scroll up
    pushing: false,

    context: false,

    // Context to watch scroll events
    scrollContext: window,

    // Offset to adjust scroll
    offset: 0,

    // Offset to adjust scroll when attached to bottom of screen
    bottomOffset: 0,

    jitter: 5, // will only set container height if difference between context and container is larger than this number

    // Whether to automatically observe changes with Mutation Observers
    observeChanges: false,

    // Called when position is recalculated
    onReposition: function () {},

    // Called on each scroll
    onScroll: function () {},

    // Called when element is stuck to viewport
    onStick: function () {},

    // Called when element is unstuck from viewport
    onUnstick: function () {},

    // Called when element reaches top of context
    onTop: function () {},

    // Called when element reaches bottom of context
    onBottom: function () {},

    error: {
      container: 'Sticky element must be inside a relative container',
      visible: 'Element is hidden, you must call refresh after element becomes visible. Use silent setting to surpress this warning in production.',
      method: 'The method you called is not defined.',
      invalidContext: 'Context specified does not exist',
      elementSize: 'Sticky element is larger than its container, cannot create sticky.'
    },

    className: {
      bound: 'bound',
      fixed: 'fixed',
      supported: 'native',
      top: 'top',
      bottom: 'bottom'
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL3N0aWNreS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBVUEsQ0FBQyxDQUFDLFVBQVUsQ0FBVixFQUFhLE1BQWIsRUFBcUIsUUFBckIsRUFBK0IsU0FBL0IsRUFBMEM7O0FBRTVDOztBQUVBLFdBQVUsT0FBTyxNQUFQLElBQWlCLFdBQWpCLElBQWdDLE9BQU8sSUFBUCxJQUFlLElBQWhELEdBQ0wsTUFESyxHQUVKLE9BQU8sSUFBUCxJQUFlLFdBQWYsSUFBOEIsS0FBSyxJQUFMLElBQWEsSUFBNUMsR0FDRSxJQURGLEdBRUUsU0FBUyxhQUFULEdBSk47O0FBT0EsSUFBRSxFQUFGLENBQUssTUFBTCxHQUFjLFVBQVMsVUFBVCxFQUFxQjtBQUNqQyxRQUNFLGNBQWlCLEVBQUUsSUFBRixDQURuQjtBQUFBLFFBRUUsaUJBQWlCLFlBQVksUUFBWixJQUF3QixFQUYzQztBQUFBLFFBSUUsT0FBaUIsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUpuQjtBQUFBLFFBS0UsY0FBaUIsRUFMbkI7QUFBQSxRQU9FLFFBQWlCLFVBQVUsQ0FBVixDQVBuQjtBQUFBLFFBUUUsZ0JBQWtCLE9BQU8sS0FBUCxJQUFnQixRQVJwQztBQUFBLFFBU0UsaUJBQWlCLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLENBQXpCLENBVG5CO0FBQUEsUUFVRSxhQVZGOztBQWFBLGdCQUNHLElBREgsQ0FDUSxZQUFXO0FBQ2YsVUFDRSxXQUEwQixFQUFFLGFBQUYsQ0FBZ0IsVUFBaEIsQ0FBRixHQUNwQixFQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQixFQUFFLEVBQUYsQ0FBSyxNQUFMLENBQVksUUFBL0IsRUFBeUMsVUFBekMsQ0FEb0IsR0FFcEIsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLEVBQUUsRUFBRixDQUFLLE1BQUwsQ0FBWSxRQUF6QixDQUhOO0FBQUEsVUFLRSxZQUF3QixTQUFTLFNBTG5DO0FBQUEsVUFNRSxZQUF3QixTQUFTLFNBTm5DO0FBQUEsVUFPRSxRQUF3QixTQUFTLEtBUG5DO0FBQUEsVUFTRSxpQkFBd0IsTUFBTSxTQVRoQztBQUFBLFVBVUUsa0JBQXdCLFlBQVksU0FWdEM7QUFBQSxVQVlFLFVBQXdCLEVBQUUsSUFBRixDQVoxQjtBQUFBLFVBYUUsVUFBd0IsRUFBRSxNQUFGLENBYjFCO0FBQUEsVUFjRSxVQUF3QixFQUFFLFNBQVMsYUFBWCxDQWQxQjtBQUFBLFVBZUUsVUFmRjtBQUFBLFVBZ0JFLFFBaEJGO0FBQUEsVUFrQkUsV0FBd0IsUUFBUSxRQUFSLElBQW9CLEVBbEI5QztBQUFBLFVBbUJFLFdBQXdCLFFBQVEsSUFBUixDQUFhLGVBQWIsQ0FuQjFCO0FBQUEsVUFxQkUsd0JBQXdCLE9BQU8scUJBQVAsSUFDbkIsT0FBTyx3QkFEWSxJQUVuQixPQUFPLDJCQUZZLElBR25CLE9BQU8sdUJBSFksSUFJbkIsVUFBUyxRQUFULEVBQW1CO0FBQUUsbUJBQVcsUUFBWCxFQUFxQixDQUFyQjtBQUEwQixPQXpCdEQ7QUFBQSxVQTJCRSxVQUFrQixJQTNCcEI7QUFBQSxVQTZCRSxnQkE3QkY7QUFBQSxVQThCRSxRQTlCRjtBQUFBLFVBK0JFLE1BL0JGOztBQWtDQSxlQUFjOztBQUVaLG9CQUFZLFlBQVc7O0FBRXJCLGlCQUFPLGtCQUFQO0FBQ0EsaUJBQU8sZ0JBQVA7QUFDQSxpQkFBTyxPQUFQLENBQWUscUJBQWYsRUFBc0MsUUFBdEMsRUFBZ0QsVUFBaEQ7O0FBRUEsaUJBQU8sSUFBUCxDQUFZLFNBQVo7QUFDQSxpQkFBTyxXQUFQO0FBQ0EsaUJBQU8sSUFBUCxDQUFZLE1BQVo7O0FBRUEsY0FBRyxTQUFTLGNBQVosRUFBNEI7QUFDMUIsbUJBQU8sY0FBUDtBQUNEO0FBQ0QsaUJBQU8sV0FBUDtBQUNELFNBaEJXOztBQWtCWixxQkFBYSxZQUFXO0FBQ3RCLGlCQUFPLE9BQVAsQ0FBZSw0QkFBZixFQUE2QyxNQUE3QztBQUNBLHFCQUFXLE1BQVg7QUFDQSxrQkFDRyxJQURILENBQ1EsZUFEUixFQUN5QixNQUR6QjtBQUdELFNBeEJXOztBQTBCWixpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLE9BQVAsQ0FBZSw4QkFBZjtBQUNBLGlCQUFPLEtBQVA7QUFDQSxjQUFHLGdCQUFILEVBQXFCO0FBQ25CLDZCQUFpQixVQUFqQjtBQUNEO0FBQ0QsY0FBRyxRQUFILEVBQWE7QUFDWCxxQkFBUyxVQUFUO0FBQ0Q7QUFDRCxrQkFDRyxHQURILENBQ08sU0FBUyxjQURoQixFQUNnQyxPQUFPLEtBQVAsQ0FBYSxJQUQ3QyxFQUVHLEdBRkgsQ0FFTyxXQUFXLGNBRmxCLEVBRWtDLE9BQU8sS0FBUCxDQUFhLE1BRi9DO0FBSUEsa0JBQ0csR0FESCxDQUNPLGlCQUFpQixjQUR4QixFQUN3QyxPQUFPLEtBQVAsQ0FBYSxZQURyRDtBQUdBLGtCQUFRLFVBQVIsQ0FBbUIsZUFBbkI7QUFDRCxTQTNDVzs7QUE2Q1osd0JBQWdCLFlBQVc7QUFDekIsY0FBRyxzQkFBc0IsTUFBekIsRUFBaUM7QUFDL0IsK0JBQW1CLElBQUksZ0JBQUosQ0FBcUIsT0FBTyxLQUFQLENBQWEsZUFBbEMsQ0FBbkI7QUFDQSx1QkFBbUIsSUFBSSxnQkFBSixDQUFxQixPQUFPLEtBQVAsQ0FBYSxPQUFsQyxDQUFuQjtBQUNBLDZCQUFpQixPQUFqQixDQUF5QixRQUF6QixFQUFtQztBQUNqQyx5QkFBWSxJQURxQjtBQUVqQyx1QkFBWTtBQUZxQixhQUFuQztBQUlBLHFCQUFTLE9BQVQsQ0FBaUIsT0FBakIsRUFBMEI7QUFDeEIseUJBQVksSUFEWTtBQUV4Qix1QkFBWTtBQUZZLGFBQTFCO0FBSUEscUJBQVMsT0FBVCxDQUFpQixTQUFTLENBQVQsQ0FBakIsRUFBOEI7QUFDNUIseUJBQVksSUFEZ0I7QUFFNUIsdUJBQVk7QUFGZ0IsYUFBOUI7QUFJQSxtQkFBTyxLQUFQLENBQWEsOEJBQWIsRUFBNkMsUUFBN0M7QUFDRDtBQUNGLFNBL0RXOztBQWlFWiw0QkFBb0IsWUFBVztBQUM3Qix1QkFBYSxRQUFRLFlBQVIsRUFBYjtBQUNELFNBbkVXOztBQXFFWiwwQkFBa0IsWUFBVztBQUMzQixjQUFHLFNBQVMsT0FBWixFQUFxQjtBQUNuQix1QkFBVyxFQUFFLFNBQVMsT0FBWCxDQUFYO0FBQ0QsV0FGRCxNQUdLO0FBQ0gsdUJBQVcsVUFBWDtBQUNEO0FBQ0QsY0FBRyxTQUFTLE1BQVQsS0FBb0IsQ0FBdkIsRUFBMEI7QUFDeEIsbUJBQU8sS0FBUCxDQUFhLE1BQU0sY0FBbkIsRUFBbUMsU0FBUyxPQUE1QyxFQUFxRCxPQUFyRDtBQUNBO0FBQ0Q7QUFDRixTQWhGVzs7QUFrRloscUJBQWEsWUFBVztBQUN0QixjQUFJLE9BQU8sRUFBUCxDQUFVLE1BQVYsRUFBSixFQUF5QjtBQUN2QixtQkFBTyxLQUFQLENBQWEsTUFBTSxPQUFuQixFQUE0QixPQUE1QjtBQUNEO0FBQ0QsY0FBRyxPQUFPLEtBQVAsQ0FBYSxPQUFiLENBQXFCLE1BQXJCLEdBQThCLE9BQU8sS0FBUCxDQUFhLE9BQWIsQ0FBcUIsTUFBdEQsRUFBOEQ7QUFDNUQsbUJBQU8sS0FBUDtBQUNBLG1CQUFPLEtBQVAsQ0FBYSxNQUFNLFdBQW5CLEVBQWdDLE9BQWhDO0FBQ0E7QUFDRDtBQUNGLFNBM0ZXOztBQTZGWixjQUFNO0FBQ0osa0JBQVEsWUFBVztBQUNqQixvQkFDRyxFQURILENBQ00sU0FBUyxjQURmLEVBQytCLE9BQU8sS0FBUCxDQUFhLElBRDVDLEVBRUcsRUFGSCxDQUVNLFdBQVcsY0FGakIsRUFFaUMsT0FBTyxLQUFQLENBQWEsTUFGOUM7O0FBS0Esb0JBQ0csR0FESCxDQUNPLFdBQVcsY0FEbEIsRUFFRyxFQUZILENBRU0sV0FBVyxjQUZqQixFQUVpQyxPQUFPLEtBQVAsQ0FBYSxNQUY5QyxFQUdHLEVBSEgsQ0FHTSxpQkFBaUIsY0FIdkIsRUFHdUMsT0FBTyxLQUFQLENBQWEsWUFIcEQ7QUFLRDtBQVpHLFNBN0ZNOztBQTRHWixlQUFPO0FBQ0wsbUJBQVMsVUFBUyxTQUFULEVBQW9CO0FBQzNCLHlCQUFhLE9BQU8sS0FBcEI7QUFDQSxtQkFBTyxLQUFQLEdBQWUsV0FBVyxZQUFXO0FBQ25DLHFCQUFPLE9BQVAsQ0FBZSx5Q0FBZixFQUEwRCxTQUExRDtBQUNBLHFCQUFPLE9BQVA7QUFDRCxhQUhjLEVBR1osR0FIWSxDQUFmO0FBSUQsV0FQSTtBQVFMLDJCQUFpQixVQUFTLFNBQVQsRUFBb0I7QUFDbkMsZUFBRyxPQUFILENBQVcsSUFBWCxDQUFnQixTQUFoQixFQUEyQixVQUFTLFFBQVQsRUFBbUI7QUFDNUMsa0JBQUcsU0FBUyxZQUFaLEVBQTBCO0FBQ3hCLG1CQUFHLE9BQUgsQ0FBVyxJQUFYLENBQWdCLFNBQVMsWUFBekIsRUFBdUMsVUFBUyxJQUFULEVBQWU7QUFDcEQsc0JBQUcsUUFBUSxPQUFSLElBQW1CLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxPQUFiLEVBQXNCLE1BQXRCLEdBQStCLENBQXJELEVBQXdEO0FBQ3RELDJCQUFPLEtBQVAsQ0FBYSwrQ0FBYjtBQUNBLDJCQUFPLE9BQVA7QUFDRDtBQUNGLGlCQUxEO0FBTUQ7QUFDRixhQVREO0FBVUQsV0FuQkk7QUFvQkwsZ0JBQU0sWUFBVztBQUNmLG1CQUFPLE9BQVAsQ0FBZSxnQ0FBZjtBQUNBLGtDQUFzQixPQUFPLE9BQTdCO0FBQ0QsV0F2Qkk7QUF3Qkwsa0JBQVEsWUFBVztBQUNqQixtQkFBTyxPQUFQLENBQWUsZ0JBQWY7QUFDQSxrQ0FBc0IsT0FBTyxPQUE3QjtBQUNELFdBM0JJO0FBNEJMLGtCQUFRLFlBQVc7QUFDakIsa0NBQXNCLFlBQVc7QUFDL0Isc0JBQVEsY0FBUixDQUF1QixpQkFBaUIsY0FBeEMsRUFBd0QsUUFBUSxTQUFSLEVBQXhEO0FBQ0QsYUFGRDtBQUdELFdBaENJO0FBaUNMLHdCQUFjLFVBQVMsS0FBVCxFQUFnQixjQUFoQixFQUFnQztBQUM1QyxtQkFBTyxLQUFQLENBQWEsY0FBYjtBQUNBLHFCQUFTLFFBQVQsQ0FBa0IsSUFBbEIsQ0FBdUIsT0FBdkI7QUFDRDtBQXBDSSxTQTVHSzs7QUFtSlosaUJBQVMsVUFBUyxXQUFULEVBQXNCO0FBQzdCLGlCQUFPLEtBQVA7QUFDQSxjQUFHLENBQUMsU0FBUyxPQUFiLEVBQXNCO0FBQ3BCLG1CQUFPLGdCQUFQO0FBQ0Q7QUFDRCxjQUFHLFdBQUgsRUFBZ0I7QUFDZCxtQkFBTyxrQkFBUDtBQUNEO0FBQ0QsaUJBQU8sSUFBUCxDQUFZLFNBQVo7QUFDQSxpQkFBTyxLQUFQO0FBQ0EsbUJBQVMsWUFBVCxDQUFzQixJQUF0QixDQUEyQixPQUEzQjtBQUNELFNBOUpXOztBQWdLWixrQkFBVTtBQUNSLGtCQUFRLFlBQVc7QUFDakIsZ0JBQ0UsV0FBVyxFQUFFLFFBQUYsQ0FEYjtBQUFBLGdCQUVFLFVBQVUsU0FBUyxDQUFULENBRlo7QUFJQSxxQkFBUyxRQUFULENBQWtCLFVBQVUsU0FBNUI7QUFDQSxtQkFBTyxTQUFTLEdBQVQsQ0FBYSxVQUFiLEVBQXlCLEtBQXpCLENBQStCLFFBQS9CLENBQVA7QUFDRDtBQVJPLFNBaEtFOztBQTJLWixjQUFNO0FBQ0osc0JBQVksVUFBUyxNQUFULEVBQWlCO0FBQzNCLG1CQUFPLFVBQVAsR0FBb0IsTUFBcEI7QUFDRCxXQUhHO0FBSUoseUJBQWUsVUFBUyxNQUFULEVBQWlCO0FBQzlCLG1CQUFPLGFBQVAsR0FBdUIsTUFBdkI7QUFDRCxXQU5HO0FBT0oscUJBQVcsWUFBVztBQUNwQixnQkFDRSxnQkFBZ0I7QUFDZCxzQkFBUyxRQUFRLE1BQVI7QUFESyxhQURsQjtBQUFBLGdCQUlFLFVBQVU7QUFDUixzQkFBUTtBQUNOLHFCQUFTLFNBQVMsUUFBUSxHQUFSLENBQVksWUFBWixDQUFULEVBQW9DLEVBQXBDLENBREg7QUFFTix3QkFBUyxTQUFTLFFBQVEsR0FBUixDQUFZLGVBQVosQ0FBVCxFQUF1QyxFQUF2QztBQUZILGVBREE7QUFLUixzQkFBUyxRQUFRLE1BQVIsRUFMRDtBQU1SLHFCQUFTLFFBQVEsVUFBUixFQU5EO0FBT1Isc0JBQVMsUUFBUSxXQUFSO0FBUEQsYUFKWjtBQUFBLGdCQWFFLFVBQVU7QUFDUixzQkFBUyxTQUFTLE1BQVQsRUFERDtBQUVSLHNCQUFTLFNBQVMsV0FBVDtBQUZELGFBYlo7QUFBQSxnQkFpQkUsWUFBWTtBQUNWLHNCQUFRLFdBQVcsV0FBWDtBQURFLGFBakJkO0FBcUJBLGdCQUFJLENBQUMsT0FBTyxFQUFQLENBQVUsY0FBVixFQUFMLEVBQWtDO0FBQ2hDLHFCQUFPLEtBQVAsQ0FBYSxpRUFBYjs7QUFFQSw0QkFBYyxHQUFkLEdBQXFCLFFBQVEsU0FBUixFQUFyQjtBQUNBLDRCQUFjLElBQWQsR0FBcUIsUUFBUSxVQUFSLEVBQXJCOztBQUVBLHNCQUFRLE1BQVIsQ0FBZSxHQUFmLElBQXVCLGNBQWMsR0FBckM7QUFDQSxzQkFBUSxNQUFSLENBQWUsR0FBZixJQUF1QixjQUFjLEdBQXJDO0FBQ0Esc0JBQVEsTUFBUixDQUFlLElBQWYsSUFBdUIsY0FBYyxJQUFyQztBQUNBLHNCQUFRLE1BQVIsQ0FBZSxJQUFmLElBQXVCLGNBQWMsSUFBckM7QUFDRDtBQUNELG1CQUFPLEtBQVAsR0FBZTtBQUNiLG9CQUFTLFFBQVEsTUFBUixHQUFpQixjQUFjLE1BRDNCO0FBRWIsNkJBQWdCO0FBQ2Qsd0JBQVMsY0FBYztBQURULGVBRkg7QUFLYix1QkFBUztBQUNQLHdCQUFTLFFBQVEsTUFEVjtBQUVQLHFCQUFTLFFBQVEsTUFBUixDQUFlLEdBQWYsR0FBcUIsUUFBUSxNQUFSLENBQWUsR0FGdEM7QUFHUCxzQkFBUyxRQUFRLE1BQVIsQ0FBZSxJQUhqQjtBQUlQLHVCQUFTLFFBQVEsS0FKVjtBQUtQLHdCQUFTLFFBQVEsTUFMVjtBQU1QLHdCQUFTLFFBQVEsTUFBUixDQUFlLEdBQWYsR0FBcUIsUUFBUTtBQU4vQixlQUxJO0FBYWIsdUJBQVM7QUFDUCxxQkFBZ0IsUUFBUSxNQUFSLENBQWUsR0FEeEI7QUFFUCx3QkFBZ0IsUUFBUSxNQUZqQjtBQUdQLHdCQUFnQixRQUFRLE1BQVIsQ0FBZSxHQUFmLEdBQXFCLFFBQVE7QUFIdEM7QUFiSSxhQUFmO0FBbUJBLG1CQUFPLEdBQVAsQ0FBVyxhQUFYO0FBQ0EsbUJBQU8sR0FBUCxDQUFXLElBQVg7QUFDQSxtQkFBTyxLQUFQO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLDJCQUFiLEVBQTBDLE9BQU8sS0FBakQ7QUFDRDtBQS9ERyxTQTNLTTs7QUE2T1osYUFBSztBQUNILHFCQUFXLFVBQVMsTUFBVCxFQUFpQjtBQUMxQixnQkFDRSxZQUFZLE1BRGQ7QUFHQSxxQkFBUyxVQUFVLFFBQVEsU0FBUixFQUFuQjtBQUNBLGdCQUFHLE9BQU8sVUFBUCxLQUFzQixTQUF6QixFQUFvQztBQUNsQyxrQkFBRyxPQUFPLFVBQVAsR0FBb0IsTUFBdkIsRUFBK0I7QUFDN0IsNEJBQVksTUFBWjtBQUNELGVBRkQsTUFHSyxJQUFHLE9BQU8sVUFBUCxHQUFvQixNQUF2QixFQUErQjtBQUNsQyw0QkFBWSxJQUFaO0FBQ0Q7QUFDRjtBQUNELG1CQUFPLFNBQVA7QUFDRCxXQWZFO0FBZ0JILHdCQUFjLFVBQVMsTUFBVCxFQUFpQjtBQUM3QixxQkFBUyxVQUFVLFFBQVEsU0FBUixFQUFuQjtBQUNBLG1CQUFRLE9BQU8sVUFBUixHQUNGLFNBQVMsT0FBTyxVQURkLEdBRUgsQ0FGSjtBQUlELFdBdEJFO0FBdUJILGdDQUFzQixZQUFXO0FBQy9CLGdCQUFHLE9BQU8sYUFBVixFQUF5QjtBQUN2QixxQkFBTyxPQUFPLGFBQWQ7QUFDRDtBQUNELG1CQUFTLE9BQU8sRUFBUCxDQUFVLEdBQVYsRUFBRixHQUNILEtBQUssR0FBTCxDQUFTLFNBQVMsUUFBUSxHQUFSLENBQVksS0FBWixDQUFULEVBQTZCLEVBQTdCLENBQVQsS0FBaUQsQ0FEOUMsR0FFSCxLQUFLLEdBQUwsQ0FBUyxTQUFTLFFBQVEsR0FBUixDQUFZLFFBQVosQ0FBVCxFQUFnQyxFQUFoQyxDQUFULEtBQWlELENBRnJEO0FBSUQsV0EvQkU7O0FBaUNILHlCQUFlLFVBQVMsTUFBVCxFQUFpQjtBQUM5QixxQkFBUyxVQUFVLFFBQVEsU0FBUixFQUFuQjtBQUNBLGdCQUNFLFVBQWlCLE9BQU8sS0FBUCxDQUFhLE9BRGhDO0FBQUEsZ0JBRUUsZ0JBQWlCLE9BQU8sS0FBUCxDQUFhLGFBRmhDO0FBQUEsZ0JBR0UsUUFBaUIsT0FBTyxHQUFQLENBQVcsWUFBWCxDQUF3QixNQUF4QixDQUhuQjtBQUFBLGdCQUlFLFlBQWtCLFFBQVEsTUFBUixHQUFpQixjQUFjLE1BQS9CLEdBQXdDLFNBQVMsTUFKckU7QUFBQSxnQkFLRSxnQkFBaUIsT0FBTyxHQUFQLENBQVcsb0JBQVgsRUFMbkI7QUFBQSxnQkFNRSxpQkFBa0IsZ0JBQWdCLEtBTnBDO0FBUUEsZ0JBQUcsT0FBTyxLQUFQLENBQWEsSUFBYixJQUFxQixpQkFBaUIsQ0FBekMsRUFBNEM7QUFDMUMsOEJBQWdCLENBQWhCO0FBQ0QsYUFGRCxNQUdLLElBQUcsaUJBQWlCLFNBQXBCLEVBQWdDO0FBQ25DLDhCQUFnQixTQUFoQjtBQUNELGFBRkksTUFHQTtBQUNILDhCQUFnQixjQUFoQjtBQUNEO0FBQ0QsbUJBQU8sYUFBUDtBQUNEO0FBckRFLFNBN09POztBQXFTWixnQkFBUTtBQUNOLHNCQUFZLFlBQVc7QUFDckIsbUJBQU8sT0FBTyxVQUFkO0FBQ0QsV0FISztBQUlOLHlCQUFlLFVBQVMsTUFBVCxFQUFpQjtBQUM5QixtQkFBTyxPQUFPLGFBQWQ7QUFDRCxXQU5LO0FBT04sa0JBQVEsWUFBVztBQUNqQixvQkFBUSxHQUFSLENBQVksWUFBWixFQUEwQixFQUExQjtBQUNEO0FBVEssU0FyU0k7O0FBaVRaLGFBQUs7QUFDSCxrQkFBUSxZQUFXO0FBQ2pCLG1CQUFPLE9BQVAsQ0FBZSwyQkFBZixFQUE0QyxTQUFTLE1BQXJEO0FBQ0Esb0JBQ0csR0FESCxDQUNPLFlBRFAsRUFDcUIsU0FBUyxNQUQ5QjtBQUdELFdBTkU7QUFPSCx5QkFBZSxZQUFXO0FBQ3hCLGdCQUNFLFVBQVUsV0FBVyxHQUFYLENBQWUsQ0FBZixFQUFrQixPQUQ5QjtBQUdBLGdCQUFHLFlBQVksTUFBWixJQUFzQixXQUFXLE1BQXBDLEVBQTRDOzs7QUFHMUMscUJBQU8sa0JBQVA7QUFDRCxhQUpELE1BS0s7QUFDSCxrQkFBSSxLQUFLLEdBQUwsQ0FBUyxXQUFXLFdBQVgsS0FBMkIsT0FBTyxLQUFQLENBQWEsT0FBYixDQUFxQixNQUF6RCxJQUFtRSxTQUFTLE1BQWhGLEVBQXdGO0FBQ3RGLHVCQUFPLEtBQVAsQ0FBYSw0REFBYixFQUEyRSxPQUFPLEtBQVAsQ0FBYSxPQUFiLENBQXFCLE1BQWhHO0FBQ0EsMkJBQVcsR0FBWCxDQUFlO0FBQ2IsMEJBQVEsT0FBTyxLQUFQLENBQWEsT0FBYixDQUFxQjtBQURoQixpQkFBZjtBQUdEO0FBQ0Y7QUFDRixXQXhCRTtBQXlCSCx1QkFBYSxZQUFXO0FBQ3RCLGdCQUNFLFVBQVksT0FBTyxLQUFQLENBQWEsT0FEM0I7QUFHQSx1QkFDRyxHQURILENBQ08sWUFEUCxFQUNxQixRQUFRLE1BRDdCO0FBR0QsV0FoQ0U7QUFpQ0gsa0JBQVEsVUFBUyxNQUFULEVBQWlCO0FBQ3ZCLG1CQUFPLEtBQVAsQ0FBYSwyQkFBYixFQUEwQyxNQUExQztBQUNBLGdCQUFHLE9BQU8sYUFBUCxJQUF3QixNQUEzQixFQUFtQztBQUNqQztBQUNEO0FBQ0QsZ0JBQUksT0FBTyxFQUFQLENBQVUsR0FBVixFQUFKLEVBQXNCO0FBQ3BCLHNCQUNHLEdBREgsQ0FDTyxRQURQLEVBQ2lCLEVBRGpCLEVBRUcsR0FGSCxDQUVPLEtBRlAsRUFFYyxDQUFDLE1BRmY7QUFJRDtBQUNELGdCQUFJLE9BQU8sRUFBUCxDQUFVLE1BQVYsRUFBSixFQUF5QjtBQUN2QixzQkFDRyxHQURILENBQ08sS0FEUCxFQUNjLEVBRGQsRUFFRyxHQUZILENBRU8sUUFGUCxFQUVpQixNQUZqQjtBQUlEO0FBQ0YsV0FsREU7QUFtREgsZ0JBQU0sWUFBVztBQUNmLGdCQUFHLE9BQU8sS0FBUCxDQUFhLE9BQWIsQ0FBcUIsTUFBckIsS0FBZ0MsQ0FBaEMsSUFBcUMsT0FBTyxLQUFQLENBQWEsT0FBYixDQUFxQixLQUFyQixLQUErQixDQUF2RSxFQUEwRTtBQUN4RSxzQkFBUSxLQUFSLENBQWMsV0FBZCxDQUEwQixPQUExQixFQUFvQyxPQUFPLEtBQVAsQ0FBYSxPQUFiLENBQXFCLEtBQXJCLEdBQThCLElBQWxFLEVBQXdFLFdBQXhFO0FBQ0Esc0JBQVEsS0FBUixDQUFjLFdBQWQsQ0FBMEIsUUFBMUIsRUFBb0MsT0FBTyxLQUFQLENBQWEsT0FBYixDQUFxQixNQUFyQixHQUE4QixJQUFsRSxFQUF3RSxXQUF4RTtBQUNEO0FBQ0Y7QUF4REUsU0FqVE87O0FBNFdaLFlBQUk7QUFDRiwwQkFBZ0IsWUFBVztBQUN6QixtQkFBUSxRQUFRLENBQVIsS0FBYyxNQUF0QjtBQUNELFdBSEM7QUFJRixlQUFLLFlBQVc7QUFDZCxtQkFBTyxRQUFRLFFBQVIsQ0FBaUIsVUFBVSxHQUEzQixDQUFQO0FBQ0QsV0FOQztBQU9GLGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sUUFBUSxRQUFSLENBQWlCLFVBQVUsTUFBM0IsQ0FBUDtBQUNELFdBVEM7QUFVRiwyQkFBaUIsWUFBVztBQUMxQixtQkFBUSxDQUFDLE9BQU8sRUFBUCxDQUFVLEtBQVYsRUFBRCxJQUFzQixDQUFDLE9BQU8sRUFBUCxDQUFVLEtBQVYsRUFBL0I7QUFDRCxXQVpDO0FBYUYsa0JBQVEsWUFBVztBQUNqQixtQkFBUSxDQUFDLFFBQVEsRUFBUixDQUFXLFVBQVgsQ0FBVDtBQUNELFdBZkM7QUFnQkYsaUJBQU8sWUFBVztBQUNoQixtQkFBTyxRQUFRLFFBQVIsQ0FBaUIsVUFBVSxLQUEzQixDQUFQO0FBQ0QsV0FsQkM7QUFtQkYsaUJBQU8sWUFBVztBQUNoQixtQkFBTyxRQUFRLFFBQVIsQ0FBaUIsVUFBVSxLQUEzQixDQUFQO0FBQ0Q7QUFyQkMsU0E1V1E7O0FBb1laLGVBQU8sVUFBUyxNQUFULEVBQWlCO0FBQ3RCLGNBQ0UsaUJBQWlCLFVBQVUsUUFBUSxTQUFSLEVBRDdCO0FBQUEsY0FFRSxRQUFpQixPQUFPLEtBRjFCO0FBQUEsY0FHRSxPQUFpQixNQUFNLElBSHpCO0FBQUEsY0FJRSxVQUFpQixNQUFNLE9BSnpCO0FBQUEsY0FLRSxnQkFBaUIsTUFBTSxhQUx6QjtBQUFBLGNBTUUsVUFBaUIsTUFBTSxPQU56QjtBQUFBLGNBT0UsU0FBa0IsT0FBTyxFQUFQLENBQVUsTUFBVixNQUFzQixTQUFTLE9BQWhDLEdBQ2IsU0FBUyxZQURJLEdBRWIsU0FBUyxNQVRmO0FBQUEsY0FVRSxTQUFpQjtBQUNmLGlCQUFTLGlCQUFpQixNQURYO0FBRWYsb0JBQVMsaUJBQWlCLE1BQWpCLEdBQTBCLGNBQWM7QUFGbEMsV0FWbkI7QUFBQSxjQWNFLFlBQWlCLE9BQU8sR0FBUCxDQUFXLFNBQVgsQ0FBcUIsT0FBTyxHQUE1QixDQWRuQjtBQUFBLGNBZUUsZ0JBQWtCLElBQUQsR0FDYixDQURhLEdBRWIsT0FBTyxHQUFQLENBQVcsYUFBWCxDQUF5QixPQUFPLEdBQWhDLENBakJOO0FBQUE7OztBQW9CRSxzQkFBaUIsQ0FBQyxJQXBCcEI7QUFBQSxjQXFCRSxpQkFBa0IsUUFBUSxNQUFSLEtBQW1CLENBckJ2Qzs7QUF3QkEsY0FBRyxjQUFILEVBQW1COztBQUVqQixnQkFBSSxPQUFPLEVBQVAsQ0FBVSxlQUFWLEVBQUosRUFBa0M7QUFDaEMsa0JBQUcsT0FBTyxHQUFQLElBQWMsUUFBUSxNQUF6QixFQUFpQztBQUMvQix1QkFBTyxLQUFQLENBQWEsaURBQWI7QUFDQSx1QkFBTyxVQUFQO0FBQ0QsZUFIRCxNQUlLLElBQUcsT0FBTyxHQUFQLEdBQWEsUUFBUSxHQUF4QixFQUE2QjtBQUNoQyxvQkFBSyxRQUFRLE1BQVIsR0FBaUIsT0FBTyxHQUF4QixHQUE4QixhQUEvQixJQUFpRCxRQUFRLE1BQTdELEVBQXNFO0FBQ3BFLHlCQUFPLEtBQVAsQ0FBYSxpREFBYjtBQUNBLHlCQUFPLFVBQVA7QUFDRCxpQkFIRCxNQUlLO0FBQ0gseUJBQU8sS0FBUCxDQUFhLG1DQUFiO0FBQ0EseUJBQU8sTUFBUDtBQUNEO0FBQ0Y7QUFFRixhQWhCRCxNQWlCSyxJQUFJLE9BQU8sRUFBUCxDQUFVLEtBQVYsRUFBSixFQUF3Qjs7O0FBRzNCLGtCQUFJLE9BQU8sRUFBUCxDQUFVLEdBQVYsRUFBSixFQUFzQjtBQUNwQixvQkFBSSxPQUFPLEdBQVAsSUFBYyxRQUFRLEdBQTFCLEVBQWdDO0FBQzlCLHlCQUFPLEtBQVAsQ0FBYSx3Q0FBYjtBQUNBLHlCQUFPLGtCQUFQO0FBQ0QsaUJBSEQsTUFJSyxJQUFLLFFBQVEsTUFBUixHQUFpQixPQUFPLEdBQXhCLEdBQThCLGFBQS9CLElBQWlELFFBQVEsTUFBN0QsRUFBc0U7QUFDekUseUJBQU8sS0FBUCxDQUFhLDJDQUFiO0FBQ0EseUJBQU8sVUFBUDtBQUNEOztBQUhJLHFCQUtBLElBQUcsU0FBSCxFQUFjO0FBQ2pCLDJCQUFPLEdBQVAsQ0FBVyxNQUFYLENBQWtCLGFBQWxCO0FBQ0EsMkJBQU8sSUFBUCxDQUFZLFVBQVosQ0FBdUIsT0FBTyxHQUE5QjtBQUNBLDJCQUFPLElBQVAsQ0FBWSxhQUFaLENBQTBCLGFBQTFCO0FBQ0Q7QUFDRjs7O0FBZkQsbUJBa0JLLElBQUcsT0FBTyxFQUFQLENBQVUsTUFBVixFQUFILEVBQXdCOzs7QUFHM0Isc0JBQUssT0FBTyxNQUFQLEdBQWdCLFFBQVEsTUFBekIsSUFBb0MsUUFBUSxHQUFoRCxFQUFxRDtBQUNuRCwyQkFBTyxLQUFQLENBQWEsZ0RBQWI7QUFDQSwyQkFBTyxrQkFBUDtBQUNEOztBQUhELHVCQUtLLElBQUcsT0FBTyxNQUFQLElBQWlCLFFBQVEsTUFBNUIsRUFBb0M7QUFDdkMsNkJBQU8sS0FBUCxDQUFhLG1EQUFiO0FBQ0EsNkJBQU8sVUFBUDtBQUNEOztBQUhJLHlCQUtBLElBQUcsU0FBSCxFQUFjO0FBQ2pCLCtCQUFPLEdBQVAsQ0FBVyxNQUFYLENBQWtCLGFBQWxCO0FBQ0EsK0JBQU8sSUFBUCxDQUFZLFVBQVosQ0FBdUIsT0FBTyxHQUE5QjtBQUNBLCtCQUFPLElBQVAsQ0FBWSxhQUFaLENBQTBCLGFBQTFCO0FBQ0Q7QUFFRjtBQUNGLGFBekNJLE1BMENBLElBQUksT0FBTyxFQUFQLENBQVUsTUFBVixFQUFKLEVBQXlCO0FBQzVCLGtCQUFJLE9BQU8sR0FBUCxJQUFjLFFBQVEsR0FBMUIsRUFBZ0M7QUFDOUIsdUJBQU8sS0FBUCxDQUFhLHlFQUFiO0FBQ0EsdUJBQU8sa0JBQVA7QUFDRCxlQUhELE1BSUs7QUFDSCxvQkFBRyxTQUFTLE9BQVosRUFBcUI7QUFDbkIsc0JBQUcsT0FBTyxFQUFQLENBQVUsS0FBVixNQUFxQixPQUFPLE1BQVAsSUFBaUIsUUFBUSxNQUFqRCxFQUEwRDtBQUN4RCwyQkFBTyxLQUFQLENBQWEsc0RBQWI7QUFDQSwyQkFBTyxTQUFQO0FBQ0Q7QUFDRixpQkFMRCxNQU1LO0FBQ0gsc0JBQUcsT0FBTyxFQUFQLENBQVUsS0FBVixNQUFzQixPQUFPLEdBQVAsSUFBYyxRQUFRLE1BQVIsR0FBaUIsUUFBUSxNQUFoRSxFQUEwRTtBQUN4RSwyQkFBTyxLQUFQLENBQWEsbURBQWI7QUFDQSwyQkFBTyxNQUFQO0FBQ0Q7QUFDRjtBQUNGO0FBQ0Y7QUFDRjtBQUNGLFNBL2VXOztBQWlmWixpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLEtBQVAsQ0FBYSw0Q0FBYjtBQUNBLGlCQUFPLE1BQVAsQ0FBYyxNQUFkO0FBQ0Esa0JBQ0csR0FESCxDQUNPO0FBQ0gsa0JBQWUsRUFEWjtBQUVILGlCQUFlLEVBRlo7QUFHSCwwQkFBZTtBQUhaLFdBRFAsRUFNRyxXQU5ILENBTWUsVUFBVSxLQU56QixFQU9HLFdBUEgsQ0FPZSxVQUFVLE1BUHpCLEVBUUcsUUFSSCxDQVFZLFVBQVUsS0FSdEIsRUFTRyxRQVRILENBU1ksVUFBVSxHQVR0QjtBQVdBLG1CQUFTLEtBQVQsQ0FBZSxJQUFmLENBQW9CLE9BQXBCO0FBQ0EsbUJBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixPQUF4QjtBQUNELFNBamdCVztBQWtnQlosb0JBQVksWUFBVztBQUNyQixpQkFBTyxLQUFQLENBQWEsK0NBQWI7QUFDQSxpQkFBTyxNQUFQLENBQWMsTUFBZDtBQUNBLGtCQUNHLEdBREgsQ0FDTztBQUNILGtCQUFlLEVBRFo7QUFFSCxpQkFBZTtBQUZaLFdBRFAsRUFLRyxXQUxILENBS2UsVUFBVSxLQUx6QixFQU1HLFdBTkgsQ0FNZSxVQUFVLEdBTnpCLEVBT0csUUFQSCxDQU9ZLFVBQVUsS0FQdEIsRUFRRyxRQVJILENBUVksVUFBVSxNQVJ0QjtBQVVBLG1CQUFTLFFBQVQsQ0FBa0IsSUFBbEIsQ0FBdUIsT0FBdkI7QUFDQSxtQkFBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLE9BQXhCO0FBQ0QsU0FqaEJXOztBQW1oQlosNEJBQW9CLFlBQVc7QUFDN0IsaUJBQU8sS0FBUCxDQUFhLCtCQUFiO0FBQ0EsaUJBQU8sS0FBUDtBQUNBLGlCQUFPLE1BQVA7QUFDRCxTQXZoQlc7O0FBMGhCWixnQkFBUSxZQUFXO0FBQ2pCLGlCQUFPLEtBQVAsQ0FBYSwrQkFBYjtBQUNBLGlCQUFPLEdBQVAsQ0FBVyxXQUFYO0FBQ0EsaUJBQU8sR0FBUCxDQUFXLE1BQVg7QUFDQSxrQkFDRyxHQURILENBQ087QUFDSCxrQkFBZSxPQUFPLEtBQVAsQ0FBYSxPQUFiLENBQXFCLElBRGpDO0FBRUgsb0JBQWUsRUFGWjtBQUdILDBCQUFlO0FBSFosV0FEUCxFQU1HLFdBTkgsQ0FNZSxVQUFVLEtBTnpCLEVBT0csV0FQSCxDQU9lLFVBQVUsTUFQekIsRUFRRyxRQVJILENBUVksVUFBVSxLQVJ0QixFQVNHLFFBVEgsQ0FTWSxVQUFVLEdBVHRCO0FBV0EsbUJBQVMsT0FBVCxDQUFpQixJQUFqQixDQUFzQixPQUF0QjtBQUNELFNBMWlCVzs7QUE0aUJaLG1CQUFXLFlBQVc7QUFDcEIsaUJBQU8sS0FBUCxDQUFhLG9DQUFiO0FBQ0EsaUJBQU8sR0FBUCxDQUFXLFdBQVg7QUFDQSxpQkFBTyxHQUFQLENBQVcsTUFBWDtBQUNBLGtCQUNHLEdBREgsQ0FDTztBQUNILGtCQUFlLE9BQU8sS0FBUCxDQUFhLE9BQWIsQ0FBcUIsSUFEakM7QUFFSCxvQkFBZSxFQUZaO0FBR0gsMEJBQWU7QUFIWixXQURQLEVBTUcsV0FOSCxDQU1lLFVBQVUsS0FOekIsRUFPRyxXQVBILENBT2UsVUFBVSxHQVB6QixFQVFHLFFBUkgsQ0FRWSxVQUFVLEtBUnRCLEVBU0csUUFUSCxDQVNZLFVBQVUsTUFUdEI7QUFXQSxtQkFBUyxPQUFULENBQWlCLElBQWpCLENBQXNCLE9BQXRCO0FBQ0QsU0E1akJXOztBQThqQlosZ0JBQVEsWUFBVztBQUNqQixjQUFJLE9BQU8sRUFBUCxDQUFVLEtBQVYsRUFBSixFQUF3QjtBQUN0QixtQkFBTyxLQUFQLENBQWEsOENBQWI7QUFDQSxtQkFBTyxNQUFQLENBQWMsTUFBZDtBQUNBLG9CQUNHLFdBREgsQ0FDZSxVQUFVLEtBRHpCLEVBRUcsV0FGSCxDQUVlLFVBQVUsR0FGekIsRUFHRyxXQUhILENBR2UsVUFBVSxNQUh6QjtBQUtEO0FBQ0YsU0F4a0JXOztBQTBrQlosZUFBTyxZQUFXO0FBQ2hCLGNBQUksT0FBTyxFQUFQLENBQVUsS0FBVixFQUFKLEVBQXdCO0FBQ3RCLG1CQUFPLEtBQVAsQ0FBYSxvQ0FBYjtBQUNBLG1CQUFPLE1BQVAsQ0FBYyxNQUFkO0FBQ0Esb0JBQ0csV0FESCxDQUNlLFVBQVUsS0FEekIsRUFFRyxXQUZILENBRWUsVUFBVSxHQUZ6QixFQUdHLFdBSEgsQ0FHZSxVQUFVLE1BSHpCO0FBS0EscUJBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixPQUF4QjtBQUNEO0FBQ0YsU0FybEJXOztBQXVsQlosZUFBTyxZQUFXO0FBQ2hCLGlCQUFPLEtBQVAsQ0FBYSw2QkFBYjtBQUNBLGlCQUFPLE1BQVA7QUFDQSxpQkFBTyxLQUFQO0FBQ0EsaUJBQU8sUUFBUDtBQUNBLGlCQUFPLE1BQVAsQ0FBYyxNQUFkO0FBQ0EsaUJBQU8sTUFBUCxDQUFjLFVBQWQ7QUFDRCxTQTlsQlc7O0FBZ21CWixrQkFBVSxZQUFXO0FBQ25CLGtCQUNHLEdBREgsQ0FDTztBQUNILG1CQUFTLEVBRE47QUFFSCxvQkFBUztBQUZOLFdBRFA7QUFNQSxxQkFDRyxHQURILENBQ087QUFDSCxvQkFBUTtBQURMLFdBRFA7QUFLRCxTQTVtQlc7O0FBOG1CWixpQkFBUyxVQUFTLElBQVQsRUFBZSxLQUFmLEVBQXNCO0FBQzdCLGNBQUksRUFBRSxhQUFGLENBQWdCLElBQWhCLENBQUosRUFBNEI7QUFDMUIsY0FBRSxNQUFGLENBQVMsSUFBVCxFQUFlLFFBQWYsRUFBeUIsSUFBekI7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IscUJBQVMsSUFBVCxJQUFpQixLQUFqQjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLFNBQVMsSUFBVCxDQUFQO0FBQ0Q7QUFDRixTQXhuQlc7QUF5bkJaLGtCQUFVLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDOUIsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsTUFBZixFQUF1QixJQUF2QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixtQkFBTyxJQUFQLElBQWUsS0FBZjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLE9BQU8sSUFBUCxDQUFQO0FBQ0Q7QUFDRixTQW5vQlc7QUFvb0JaLGVBQU8sWUFBVztBQUNoQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsS0FBaEMsRUFBdUM7QUFDckMsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxLQUFQLEdBQWUsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWY7QUFDQSxxQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNEO0FBQ0Y7QUFDRixTQTlvQlc7QUErb0JaLGlCQUFTLFlBQVc7QUFDbEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLE9BQTdCLElBQXdDLFNBQVMsS0FBcEQsRUFBMkQ7QUFDekQsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxPQUFQLEdBQWlCLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFqQjtBQUNBLHFCQUFPLE9BQVAsQ0FBZSxLQUFmLENBQXFCLE9BQXJCLEVBQThCLFNBQTlCO0FBQ0Q7QUFDRjtBQUNGLFNBenBCVztBQTBwQlosZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQWIsRUFBcUI7QUFDbkIsbUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLEtBQXJDLEVBQTRDLE9BQTVDLEVBQXFELFNBQVMsSUFBVCxHQUFnQixHQUFyRSxDQUFmO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGLFNBL3BCVztBQWdxQloscUJBQWE7QUFDWCxlQUFLLFVBQVMsT0FBVCxFQUFrQjtBQUNyQixnQkFDRSxXQURGLEVBRUUsYUFGRixFQUdFLFlBSEY7QUFLQSxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIsNEJBQWdCLElBQUksSUFBSixHQUFXLE9BQVgsRUFBaEI7QUFDQSw2QkFBZ0IsUUFBUSxXQUF4QjtBQUNBLDhCQUFnQixjQUFjLFlBQTlCO0FBQ0EscUJBQWdCLFdBQWhCO0FBQ0EsMEJBQVksSUFBWixDQUFpQjtBQUNmLHdCQUFtQixRQUFRLENBQVIsQ0FESjtBQUVmLDZCQUFtQixHQUFHLEtBQUgsQ0FBUyxJQUFULENBQWMsT0FBZCxFQUF1QixDQUF2QixLQUE2QixFQUZqQztBQUdmLDJCQUFtQixPQUhKO0FBSWYsa0NBQW1CO0FBSkosZUFBakI7QUFNRDtBQUNELHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLG1CQUFPLFdBQVAsQ0FBbUIsS0FBbkIsR0FBMkIsV0FBVyxPQUFPLFdBQVAsQ0FBbUIsT0FBOUIsRUFBdUMsQ0FBdkMsQ0FBM0I7QUFDRCxXQXJCVTtBQXNCWCxtQkFBUyxZQUFXO0FBQ2xCLGdCQUNFLFFBQVEsU0FBUyxJQUFULEdBQWdCLEdBRDFCO0FBQUEsZ0JBRUUsWUFBWSxDQUZkO0FBSUEsbUJBQU8sS0FBUDtBQUNBLHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLGNBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDJCQUFhLEtBQUssZ0JBQUwsQ0FBYjtBQUNELGFBRkQ7QUFHQSxxQkFBUyxNQUFNLFNBQU4sR0FBa0IsSUFBM0I7QUFDQSxnQkFBRyxjQUFILEVBQW1CO0FBQ2pCLHVCQUFTLFFBQVEsY0FBUixHQUF5QixJQUFsQztBQUNEO0FBQ0QsZ0JBQUksQ0FBQyxRQUFRLEtBQVIsS0FBa0IsU0FBbEIsSUFBK0IsUUFBUSxLQUFSLEtBQWtCLFNBQWxELEtBQWdFLFlBQVksTUFBWixHQUFxQixDQUF6RixFQUE0RjtBQUMxRixzQkFBUSxjQUFSLENBQXVCLEtBQXZCO0FBQ0Esa0JBQUcsUUFBUSxLQUFYLEVBQWtCO0FBQ2hCLHdCQUFRLEtBQVIsQ0FBYyxXQUFkO0FBQ0QsZUFGRCxNQUdLO0FBQ0gsa0JBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDBCQUFRLEdBQVIsQ0FBWSxLQUFLLE1BQUwsSUFBZSxJQUFmLEdBQXNCLEtBQUssZ0JBQUwsQ0FBdEIsR0FBNkMsSUFBekQ7QUFDRCxpQkFGRDtBQUdEO0FBQ0Qsc0JBQVEsUUFBUjtBQUNEO0FBQ0QsMEJBQWMsRUFBZDtBQUNEO0FBakRVLFNBaHFCRDtBQW10QlosZ0JBQVEsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDLE9BQWpDLEVBQTBDO0FBQ2hELGNBQ0UsU0FBUyxRQURYO0FBQUEsY0FFRSxRQUZGO0FBQUEsY0FHRSxLQUhGO0FBQUEsY0FJRSxRQUpGO0FBTUEsNEJBQWtCLG1CQUFtQixjQUFyQztBQUNBLG9CQUFrQixXQUFtQixPQUFyQztBQUNBLGNBQUcsT0FBTyxLQUFQLElBQWdCLFFBQWhCLElBQTRCLFdBQVcsU0FBMUMsRUFBcUQ7QUFDbkQsb0JBQVcsTUFBTSxLQUFOLENBQVksT0FBWixDQUFYO0FBQ0EsdUJBQVcsTUFBTSxNQUFOLEdBQWUsQ0FBMUI7QUFDQSxjQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ25DLGtCQUFJLGlCQUFrQixTQUFTLFFBQVYsR0FDakIsUUFBUSxNQUFNLFFBQVEsQ0FBZCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixFQUEyQixXQUEzQixFQUFSLEdBQW1ELE1BQU0sUUFBUSxDQUFkLEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBRGxDLEdBRWpCLEtBRko7QUFJQSxrQkFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxjQUFQLENBQWpCLEtBQThDLFNBQVMsUUFBM0QsRUFBdUU7QUFDckUseUJBQVMsT0FBTyxjQUFQLENBQVQ7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLGNBQVAsTUFBMkIsU0FBL0IsRUFBMkM7QUFDOUMsd0JBQVEsT0FBTyxjQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBLElBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sS0FBUCxDQUFqQixLQUFxQyxTQUFTLFFBQWxELEVBQThEO0FBQ2pFLHlCQUFTLE9BQU8sS0FBUCxDQUFUO0FBQ0QsZUFGSSxNQUdBLElBQUksT0FBTyxLQUFQLE1BQWtCLFNBQXRCLEVBQWtDO0FBQ3JDLHdCQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQTtBQUNILHVCQUFPLEtBQVA7QUFDRDtBQUNGLGFBdEJEO0FBdUJEO0FBQ0QsY0FBSyxFQUFFLFVBQUYsQ0FBYyxLQUFkLENBQUwsRUFBNkI7QUFDM0IsdUJBQVcsTUFBTSxLQUFOLENBQVksT0FBWixFQUFxQixlQUFyQixDQUFYO0FBQ0QsV0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLHVCQUFXLEtBQVg7QUFDRDtBQUNELGNBQUcsRUFBRSxPQUFGLENBQVUsYUFBVixDQUFILEVBQTZCO0FBQzNCLDBCQUFjLElBQWQsQ0FBbUIsUUFBbkI7QUFDRCxXQUZELE1BR0ssSUFBRyxrQkFBa0IsU0FBckIsRUFBZ0M7QUFDbkMsNEJBQWdCLENBQUMsYUFBRCxFQUFnQixRQUFoQixDQUFoQjtBQUNELFdBRkksTUFHQSxJQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDOUIsNEJBQWdCLFFBQWhCO0FBQ0Q7QUFDRCxpQkFBTyxLQUFQO0FBQ0Q7QUF2d0JXLE9BQWQ7O0FBMHdCQSxVQUFHLGFBQUgsRUFBa0I7QUFDaEIsWUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLGlCQUFPLFVBQVA7QUFDRDtBQUNELGVBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDRCxPQUxELE1BTUs7QUFDSCxZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsbUJBQVMsTUFBVCxDQUFnQixTQUFoQjtBQUNEO0FBQ0QsZUFBTyxVQUFQO0FBQ0Q7QUFDRixLQTF6Qkg7O0FBNnpCQSxXQUFRLGtCQUFrQixTQUFuQixHQUNILGFBREcsR0FFSCxJQUZKO0FBSUQsR0EvMEJEOztBQWkxQkEsSUFBRSxFQUFGLENBQUssTUFBTCxDQUFZLFFBQVosR0FBdUI7O0FBRXJCLFVBQWlCLFFBRkk7QUFHckIsZUFBaUIsUUFISTs7QUFLckIsWUFBaUIsS0FMSTtBQU1yQixXQUFpQixLQU5JO0FBT3JCLGFBQWlCLElBUEk7QUFRckIsaUJBQWlCLElBUkk7OztBQVdyQixhQUFpQixLQVhJOztBQWFyQixhQUFpQixLQWJJOzs7QUFnQnJCLG1CQUFpQixNQWhCSTs7O0FBbUJyQixZQUFpQixDQW5CSTs7O0FBc0JyQixrQkFBaUIsQ0F0Qkk7O0FBd0JyQixZQUFpQixDQXhCSSxFOzs7QUEyQnJCLG9CQUFpQixLQTNCSTs7O0FBOEJyQixrQkFBaUIsWUFBVSxDQUFFLENBOUJSOzs7QUFpQ3JCLGNBQWlCLFlBQVUsQ0FBRSxDQWpDUjs7O0FBb0NyQixhQUFpQixZQUFVLENBQUUsQ0FwQ1I7OztBQXVDckIsZUFBaUIsWUFBVSxDQUFFLENBdkNSOzs7QUEwQ3JCLFdBQWlCLFlBQVUsQ0FBRSxDQTFDUjs7O0FBNkNyQixjQUFpQixZQUFVLENBQUUsQ0E3Q1I7O0FBK0NyQixXQUFnQjtBQUNkLGlCQUFpQixvREFESDtBQUVkLGVBQWlCLG9JQUZIO0FBR2QsY0FBaUIsdUNBSEg7QUFJZCxzQkFBaUIsa0NBSkg7QUFLZCxtQkFBaUI7QUFMSCxLQS9DSzs7QUF1RHJCLGVBQVk7QUFDVixhQUFZLE9BREY7QUFFVixhQUFZLE9BRkY7QUFHVixpQkFBWSxRQUhGO0FBSVYsV0FBWSxLQUpGO0FBS1YsY0FBWTtBQUxGOztBQXZEUyxHQUF2QjtBQWlFQyxDQTc1QkEsRUE2NUJHLE1BNzVCSCxFQTY1QlcsTUE3NUJYLEVBNjVCbUIsUUE3NUJuQiIsImZpbGUiOiJzdGlja3kuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcbiAqICMgU2VtYW50aWMgVUkgLSBTdGlja3lcbiAqIGh0dHA6Ly9naXRodWIuY29tL3NlbWFudGljLW9yZy9zZW1hbnRpYy11aS9cbiAqXG4gKlxuICogUmVsZWFzZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlXG4gKiBodHRwOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvTUlUXG4gKlxuICovXG5cbjsoZnVuY3Rpb24gKCQsIHdpbmRvdywgZG9jdW1lbnQsIHVuZGVmaW5lZCkge1xuXG5cInVzZSBzdHJpY3RcIjtcblxud2luZG93ID0gKHR5cGVvZiB3aW5kb3cgIT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93Lk1hdGggPT0gTWF0aClcbiAgPyB3aW5kb3dcbiAgOiAodHlwZW9mIHNlbGYgIT0gJ3VuZGVmaW5lZCcgJiYgc2VsZi5NYXRoID09IE1hdGgpXG4gICAgPyBzZWxmXG4gICAgOiBGdW5jdGlvbigncmV0dXJuIHRoaXMnKSgpXG47XG5cbiQuZm4uc3RpY2t5ID0gZnVuY3Rpb24ocGFyYW1ldGVycykge1xuICB2YXJcbiAgICAkYWxsTW9kdWxlcyAgICA9ICQodGhpcyksXG4gICAgbW9kdWxlU2VsZWN0b3IgPSAkYWxsTW9kdWxlcy5zZWxlY3RvciB8fCAnJyxcblxuICAgIHRpbWUgICAgICAgICAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCksXG4gICAgcGVyZm9ybWFuY2UgICAgPSBbXSxcblxuICAgIHF1ZXJ5ICAgICAgICAgID0gYXJndW1lbnRzWzBdLFxuICAgIG1ldGhvZEludm9rZWQgID0gKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyksXG4gICAgcXVlcnlBcmd1bWVudHMgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSksXG4gICAgcmV0dXJuZWRWYWx1ZVxuICA7XG5cbiAgJGFsbE1vZHVsZXNcbiAgICAuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgIHZhclxuICAgICAgICBzZXR0aW5ncyAgICAgICAgICAgICAgPSAoICQuaXNQbGFpbk9iamVjdChwYXJhbWV0ZXJzKSApXG4gICAgICAgICAgPyAkLmV4dGVuZCh0cnVlLCB7fSwgJC5mbi5zdGlja3kuc2V0dGluZ3MsIHBhcmFtZXRlcnMpXG4gICAgICAgICAgOiAkLmV4dGVuZCh7fSwgJC5mbi5zdGlja3kuc2V0dGluZ3MpLFxuXG4gICAgICAgIGNsYXNzTmFtZSAgICAgICAgICAgICA9IHNldHRpbmdzLmNsYXNzTmFtZSxcbiAgICAgICAgbmFtZXNwYWNlICAgICAgICAgICAgID0gc2V0dGluZ3MubmFtZXNwYWNlLFxuICAgICAgICBlcnJvciAgICAgICAgICAgICAgICAgPSBzZXR0aW5ncy5lcnJvcixcblxuICAgICAgICBldmVudE5hbWVzcGFjZSAgICAgICAgPSAnLicgKyBuYW1lc3BhY2UsXG4gICAgICAgIG1vZHVsZU5hbWVzcGFjZSAgICAgICA9ICdtb2R1bGUtJyArIG5hbWVzcGFjZSxcblxuICAgICAgICAkbW9kdWxlICAgICAgICAgICAgICAgPSAkKHRoaXMpLFxuICAgICAgICAkd2luZG93ICAgICAgICAgICAgICAgPSAkKHdpbmRvdyksXG4gICAgICAgICRzY3JvbGwgICAgICAgICAgICAgICA9ICQoc2V0dGluZ3Muc2Nyb2xsQ29udGV4dCksXG4gICAgICAgICRjb250YWluZXIsXG4gICAgICAgICRjb250ZXh0LFxuXG4gICAgICAgIHNlbGVjdG9yICAgICAgICAgICAgICA9ICRtb2R1bGUuc2VsZWN0b3IgfHwgJycsXG4gICAgICAgIGluc3RhbmNlICAgICAgICAgICAgICA9ICRtb2R1bGUuZGF0YShtb2R1bGVOYW1lc3BhY2UpLFxuXG4gICAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZSA9IHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWVcbiAgICAgICAgICB8fCB3aW5kb3cubW96UmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gICAgICAgICAgfHwgd2luZG93LndlYmtpdFJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgICAgIHx8IHdpbmRvdy5tc1JlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgICAgIHx8IGZ1bmN0aW9uKGNhbGxiYWNrKSB7IHNldFRpbWVvdXQoY2FsbGJhY2ssIDApOyB9LFxuXG4gICAgICAgIGVsZW1lbnQgICAgICAgICA9IHRoaXMsXG5cbiAgICAgICAgZG9jdW1lbnRPYnNlcnZlcixcbiAgICAgICAgb2JzZXJ2ZXIsXG4gICAgICAgIG1vZHVsZVxuICAgICAgO1xuXG4gICAgICBtb2R1bGUgICAgICA9IHtcblxuICAgICAgICBpbml0aWFsaXplOiBmdW5jdGlvbigpIHtcblxuICAgICAgICAgIG1vZHVsZS5kZXRlcm1pbmVDb250YWluZXIoKTtcbiAgICAgICAgICBtb2R1bGUuZGV0ZXJtaW5lQ29udGV4dCgpO1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdJbml0aWFsaXppbmcgc3RpY2t5Jywgc2V0dGluZ3MsICRjb250YWluZXIpO1xuXG4gICAgICAgICAgbW9kdWxlLnNhdmUucG9zaXRpb25zKCk7XG4gICAgICAgICAgbW9kdWxlLmNoZWNrRXJyb3JzKCk7XG4gICAgICAgICAgbW9kdWxlLmJpbmQuZXZlbnRzKCk7XG5cbiAgICAgICAgICBpZihzZXR0aW5ncy5vYnNlcnZlQ2hhbmdlcykge1xuICAgICAgICAgICAgbW9kdWxlLm9ic2VydmVDaGFuZ2VzKCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIG1vZHVsZS5pbnN0YW50aWF0ZSgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGluc3RhbnRpYXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU3RvcmluZyBpbnN0YW5jZSBvZiBtb2R1bGUnLCBtb2R1bGUpO1xuICAgICAgICAgIGluc3RhbmNlID0gbW9kdWxlO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5kYXRhKG1vZHVsZU5hbWVzcGFjZSwgbW9kdWxlKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBkZXN0cm95OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnRGVzdHJveWluZyBwcmV2aW91cyBpbnN0YW5jZScpO1xuICAgICAgICAgIG1vZHVsZS5yZXNldCgpO1xuICAgICAgICAgIGlmKGRvY3VtZW50T2JzZXJ2ZXIpIHtcbiAgICAgICAgICAgIGRvY3VtZW50T2JzZXJ2ZXIuZGlzY29ubmVjdCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZihvYnNlcnZlcikge1xuICAgICAgICAgICAgb2JzZXJ2ZXIuZGlzY29ubmVjdCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICAkd2luZG93XG4gICAgICAgICAgICAub2ZmKCdsb2FkJyArIGV2ZW50TmFtZXNwYWNlLCBtb2R1bGUuZXZlbnQubG9hZClcbiAgICAgICAgICAgIC5vZmYoJ3Jlc2l6ZScgKyBldmVudE5hbWVzcGFjZSwgbW9kdWxlLmV2ZW50LnJlc2l6ZSlcbiAgICAgICAgICA7XG4gICAgICAgICAgJHNjcm9sbFxuICAgICAgICAgICAgLm9mZignc2Nyb2xsY2hhbmdlJyArIGV2ZW50TmFtZXNwYWNlLCBtb2R1bGUuZXZlbnQuc2Nyb2xsY2hhbmdlKVxuICAgICAgICAgIDtcbiAgICAgICAgICAkbW9kdWxlLnJlbW92ZURhdGEobW9kdWxlTmFtZXNwYWNlKTtcbiAgICAgICAgfSxcblxuICAgICAgICBvYnNlcnZlQ2hhbmdlczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoJ011dGF0aW9uT2JzZXJ2ZXInIGluIHdpbmRvdykge1xuICAgICAgICAgICAgZG9jdW1lbnRPYnNlcnZlciA9IG5ldyBNdXRhdGlvbk9ic2VydmVyKG1vZHVsZS5ldmVudC5kb2N1bWVudENoYW5nZWQpO1xuICAgICAgICAgICAgb2JzZXJ2ZXIgICAgICAgICA9IG5ldyBNdXRhdGlvbk9ic2VydmVyKG1vZHVsZS5ldmVudC5jaGFuZ2VkKTtcbiAgICAgICAgICAgIGRvY3VtZW50T2JzZXJ2ZXIub2JzZXJ2ZShkb2N1bWVudCwge1xuICAgICAgICAgICAgICBjaGlsZExpc3QgOiB0cnVlLFxuICAgICAgICAgICAgICBzdWJ0cmVlICAgOiB0cnVlXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIG9ic2VydmVyLm9ic2VydmUoZWxlbWVudCwge1xuICAgICAgICAgICAgICBjaGlsZExpc3QgOiB0cnVlLFxuICAgICAgICAgICAgICBzdWJ0cmVlICAgOiB0cnVlXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIG9ic2VydmVyLm9ic2VydmUoJGNvbnRleHRbMF0sIHtcbiAgICAgICAgICAgICAgY2hpbGRMaXN0IDogdHJ1ZSxcbiAgICAgICAgICAgICAgc3VidHJlZSAgIDogdHJ1ZVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NldHRpbmcgdXAgbXV0YXRpb24gb2JzZXJ2ZXInLCBvYnNlcnZlcik7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGRldGVybWluZUNvbnRhaW5lcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgJGNvbnRhaW5lciA9ICRtb2R1bGUub2Zmc2V0UGFyZW50KCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGV0ZXJtaW5lQ29udGV4dDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoc2V0dGluZ3MuY29udGV4dCkge1xuICAgICAgICAgICAgJGNvbnRleHQgPSAkKHNldHRpbmdzLmNvbnRleHQpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICRjb250ZXh0ID0gJGNvbnRhaW5lcjtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoJGNvbnRleHQubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3IuaW52YWxpZENvbnRleHQsIHNldHRpbmdzLmNvbnRleHQsICRtb2R1bGUpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBjaGVja0Vycm9yczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIG1vZHVsZS5pcy5oaWRkZW4oKSApIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci52aXNpYmxlLCAkbW9kdWxlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYobW9kdWxlLmNhY2hlLmVsZW1lbnQuaGVpZ2h0ID4gbW9kdWxlLmNhY2hlLmNvbnRleHQuaGVpZ2h0KSB7XG4gICAgICAgICAgICBtb2R1bGUucmVzZXQoKTtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5lbGVtZW50U2l6ZSwgJG1vZHVsZSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGJpbmQ6IHtcbiAgICAgICAgICBldmVudHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJHdpbmRvd1xuICAgICAgICAgICAgICAub24oJ2xvYWQnICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS5ldmVudC5sb2FkKVxuICAgICAgICAgICAgICAub24oJ3Jlc2l6ZScgKyBldmVudE5hbWVzcGFjZSwgbW9kdWxlLmV2ZW50LnJlc2l6ZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIC8vIHB1Yi9zdWIgcGF0dGVyblxuICAgICAgICAgICAgJHNjcm9sbFxuICAgICAgICAgICAgICAub2ZmKCdzY3JvbGwnICsgZXZlbnROYW1lc3BhY2UpXG4gICAgICAgICAgICAgIC5vbignc2Nyb2xsJyArIGV2ZW50TmFtZXNwYWNlLCBtb2R1bGUuZXZlbnQuc2Nyb2xsKVxuICAgICAgICAgICAgICAub24oJ3Njcm9sbGNoYW5nZScgKyBldmVudE5hbWVzcGFjZSwgbW9kdWxlLmV2ZW50LnNjcm9sbGNoYW5nZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZXZlbnQ6IHtcbiAgICAgICAgICBjaGFuZ2VkOiBmdW5jdGlvbihtdXRhdGlvbnMpIHtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUudGltZXIpO1xuICAgICAgICAgICAgbW9kdWxlLnRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0RPTSB0cmVlIG1vZGlmaWVkLCB1cGRhdGluZyBzdGlja3kgbWVudScsIG11dGF0aW9ucyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5yZWZyZXNoKCk7XG4gICAgICAgICAgICB9LCAxMDApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZG9jdW1lbnRDaGFuZ2VkOiBmdW5jdGlvbihtdXRhdGlvbnMpIHtcbiAgICAgICAgICAgIFtdLmZvckVhY2guY2FsbChtdXRhdGlvbnMsIGZ1bmN0aW9uKG11dGF0aW9uKSB7XG4gICAgICAgICAgICAgIGlmKG11dGF0aW9uLnJlbW92ZWROb2Rlcykge1xuICAgICAgICAgICAgICAgIFtdLmZvckVhY2guY2FsbChtdXRhdGlvbi5yZW1vdmVkTm9kZXMsIGZ1bmN0aW9uKG5vZGUpIHtcbiAgICAgICAgICAgICAgICAgIGlmKG5vZGUgPT0gZWxlbWVudCB8fCAkKG5vZGUpLmZpbmQoZWxlbWVudCkubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0VsZW1lbnQgcmVtb3ZlZCBmcm9tIERPTSwgdGVhcmluZyBkb3duIGV2ZW50cycpO1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUuZGVzdHJveSgpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGxvYWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1BhZ2UgY29udGVudHMgZmluaXNoZWQgbG9hZGluZycpO1xuICAgICAgICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKG1vZHVsZS5yZWZyZXNoKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHJlc2l6ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnV2luZG93IHJlc2l6ZWQnKTtcbiAgICAgICAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZShtb2R1bGUucmVmcmVzaCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzY3JvbGw6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAkc2Nyb2xsLnRyaWdnZXJIYW5kbGVyKCdzY3JvbGxjaGFuZ2UnICsgZXZlbnROYW1lc3BhY2UsICRzY3JvbGwuc2Nyb2xsVG9wKCkgKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc2Nyb2xsY2hhbmdlOiBmdW5jdGlvbihldmVudCwgc2Nyb2xsUG9zaXRpb24pIHtcbiAgICAgICAgICAgIG1vZHVsZS5zdGljayhzY3JvbGxQb3NpdGlvbik7XG4gICAgICAgICAgICBzZXR0aW5ncy5vblNjcm9sbC5jYWxsKGVsZW1lbnQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICByZWZyZXNoOiBmdW5jdGlvbihoYXJkUmVmcmVzaCkge1xuICAgICAgICAgIG1vZHVsZS5yZXNldCgpO1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5jb250ZXh0KSB7XG4gICAgICAgICAgICBtb2R1bGUuZGV0ZXJtaW5lQ29udGV4dCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZihoYXJkUmVmcmVzaCkge1xuICAgICAgICAgICAgbW9kdWxlLmRldGVybWluZUNvbnRhaW5lcigpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBtb2R1bGUuc2F2ZS5wb3NpdGlvbnMoKTtcbiAgICAgICAgICBtb2R1bGUuc3RpY2soKTtcbiAgICAgICAgICBzZXR0aW5ncy5vblJlcG9zaXRpb24uY2FsbChlbGVtZW50KTtcbiAgICAgICAgfSxcblxuICAgICAgICBzdXBwb3J0czoge1xuICAgICAgICAgIHN0aWNreTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgJGVsZW1lbnQgPSAkKCc8ZGl2Lz4nKSxcbiAgICAgICAgICAgICAgZWxlbWVudCA9ICRlbGVtZW50WzBdXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICAkZWxlbWVudC5hZGRDbGFzcyhjbGFzc05hbWUuc3VwcG9ydGVkKTtcbiAgICAgICAgICAgIHJldHVybigkZWxlbWVudC5jc3MoJ3Bvc2l0aW9uJykubWF0Y2goJ3N0aWNreScpKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2F2ZToge1xuICAgICAgICAgIGxhc3RTY3JvbGw6IGZ1bmN0aW9uKHNjcm9sbCkge1xuICAgICAgICAgICAgbW9kdWxlLmxhc3RTY3JvbGwgPSBzY3JvbGw7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBlbGVtZW50U2Nyb2xsOiBmdW5jdGlvbihzY3JvbGwpIHtcbiAgICAgICAgICAgIG1vZHVsZS5lbGVtZW50U2Nyb2xsID0gc2Nyb2xsO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcG9zaXRpb25zOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBzY3JvbGxDb250ZXh0ID0ge1xuICAgICAgICAgICAgICAgIGhlaWdodCA6ICRzY3JvbGwuaGVpZ2h0KClcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgZWxlbWVudCA9IHtcbiAgICAgICAgICAgICAgICBtYXJnaW46IHtcbiAgICAgICAgICAgICAgICAgIHRvcCAgICA6IHBhcnNlSW50KCRtb2R1bGUuY3NzKCdtYXJnaW4tdG9wJyksIDEwKSxcbiAgICAgICAgICAgICAgICAgIGJvdHRvbSA6IHBhcnNlSW50KCRtb2R1bGUuY3NzKCdtYXJnaW4tYm90dG9tJyksIDEwKSxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9mZnNldCA6ICRtb2R1bGUub2Zmc2V0KCksXG4gICAgICAgICAgICAgICAgd2lkdGggIDogJG1vZHVsZS5vdXRlcldpZHRoKCksXG4gICAgICAgICAgICAgICAgaGVpZ2h0IDogJG1vZHVsZS5vdXRlckhlaWdodCgpXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGNvbnRleHQgPSB7XG4gICAgICAgICAgICAgICAgb2Zmc2V0IDogJGNvbnRleHQub2Zmc2V0KCksXG4gICAgICAgICAgICAgICAgaGVpZ2h0IDogJGNvbnRleHQub3V0ZXJIZWlnaHQoKVxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBjb250YWluZXIgPSB7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAkY29udGFpbmVyLm91dGVySGVpZ2h0KClcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoICFtb2R1bGUuaXMuc3RhbmRhcmRTY3JvbGwoKSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdOb24tc3RhbmRhcmQgc2Nyb2xsLiBSZW1vdmluZyBzY3JvbGwgb2Zmc2V0IGZyb20gZWxlbWVudCBvZmZzZXQnKTtcblxuICAgICAgICAgICAgICBzY3JvbGxDb250ZXh0LnRvcCAgPSAkc2Nyb2xsLnNjcm9sbFRvcCgpO1xuICAgICAgICAgICAgICBzY3JvbGxDb250ZXh0LmxlZnQgPSAkc2Nyb2xsLnNjcm9sbExlZnQoKTtcblxuICAgICAgICAgICAgICBlbGVtZW50Lm9mZnNldC50b3AgICs9IHNjcm9sbENvbnRleHQudG9wO1xuICAgICAgICAgICAgICBjb250ZXh0Lm9mZnNldC50b3AgICs9IHNjcm9sbENvbnRleHQudG9wO1xuICAgICAgICAgICAgICBlbGVtZW50Lm9mZnNldC5sZWZ0ICs9IHNjcm9sbENvbnRleHQubGVmdDtcbiAgICAgICAgICAgICAgY29udGV4dC5vZmZzZXQubGVmdCArPSBzY3JvbGxDb250ZXh0LmxlZnQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUuY2FjaGUgPSB7XG4gICAgICAgICAgICAgIGZpdHMgOiAoIGVsZW1lbnQuaGVpZ2h0IDwgc2Nyb2xsQ29udGV4dC5oZWlnaHQgKSxcbiAgICAgICAgICAgICAgc2Nyb2xsQ29udGV4dCA6IHtcbiAgICAgICAgICAgICAgICBoZWlnaHQgOiBzY3JvbGxDb250ZXh0LmhlaWdodFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBlbGVtZW50OiB7XG4gICAgICAgICAgICAgICAgbWFyZ2luIDogZWxlbWVudC5tYXJnaW4sXG4gICAgICAgICAgICAgICAgdG9wICAgIDogZWxlbWVudC5vZmZzZXQudG9wIC0gZWxlbWVudC5tYXJnaW4udG9wLFxuICAgICAgICAgICAgICAgIGxlZnQgICA6IGVsZW1lbnQub2Zmc2V0LmxlZnQsXG4gICAgICAgICAgICAgICAgd2lkdGggIDogZWxlbWVudC53aWR0aCxcbiAgICAgICAgICAgICAgICBoZWlnaHQgOiBlbGVtZW50LmhlaWdodCxcbiAgICAgICAgICAgICAgICBib3R0b20gOiBlbGVtZW50Lm9mZnNldC50b3AgKyBlbGVtZW50LmhlaWdodFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBjb250ZXh0OiB7XG4gICAgICAgICAgICAgICAgdG9wICAgICAgICAgICA6IGNvbnRleHQub2Zmc2V0LnRvcCxcbiAgICAgICAgICAgICAgICBoZWlnaHQgICAgICAgIDogY29udGV4dC5oZWlnaHQsXG4gICAgICAgICAgICAgICAgYm90dG9tICAgICAgICA6IGNvbnRleHQub2Zmc2V0LnRvcCArIGNvbnRleHQuaGVpZ2h0XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBtb2R1bGUuc2V0LmNvbnRhaW5lclNpemUoKTtcbiAgICAgICAgICAgIG1vZHVsZS5zZXQuc2l6ZSgpO1xuICAgICAgICAgICAgbW9kdWxlLnN0aWNrKCk7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NhY2hpbmcgZWxlbWVudCBwb3NpdGlvbnMnLCBtb2R1bGUuY2FjaGUpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBnZXQ6IHtcbiAgICAgICAgICBkaXJlY3Rpb246IGZ1bmN0aW9uKHNjcm9sbCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGRpcmVjdGlvbiA9ICdkb3duJ1xuICAgICAgICAgICAgO1xuICAgICAgICAgICAgc2Nyb2xsID0gc2Nyb2xsIHx8ICRzY3JvbGwuc2Nyb2xsVG9wKCk7XG4gICAgICAgICAgICBpZihtb2R1bGUubGFzdFNjcm9sbCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIGlmKG1vZHVsZS5sYXN0U2Nyb2xsIDwgc2Nyb2xsKSB7XG4gICAgICAgICAgICAgICAgZGlyZWN0aW9uID0gJ2Rvd24nO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYobW9kdWxlLmxhc3RTY3JvbGwgPiBzY3JvbGwpIHtcbiAgICAgICAgICAgICAgICBkaXJlY3Rpb24gPSAndXAnO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZGlyZWN0aW9uO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc2Nyb2xsQ2hhbmdlOiBmdW5jdGlvbihzY3JvbGwpIHtcbiAgICAgICAgICAgIHNjcm9sbCA9IHNjcm9sbCB8fCAkc2Nyb2xsLnNjcm9sbFRvcCgpO1xuICAgICAgICAgICAgcmV0dXJuIChtb2R1bGUubGFzdFNjcm9sbClcbiAgICAgICAgICAgICAgPyAoc2Nyb2xsIC0gbW9kdWxlLmxhc3RTY3JvbGwpXG4gICAgICAgICAgICAgIDogMFxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgY3VycmVudEVsZW1lbnRTY3JvbGw6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmVsZW1lbnRTY3JvbGwpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIG1vZHVsZS5lbGVtZW50U2Nyb2xsO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuICggbW9kdWxlLmlzLnRvcCgpIClcbiAgICAgICAgICAgICAgPyBNYXRoLmFicyhwYXJzZUludCgkbW9kdWxlLmNzcygndG9wJyksIDEwKSkgICAgfHwgMFxuICAgICAgICAgICAgICA6IE1hdGguYWJzKHBhcnNlSW50KCRtb2R1bGUuY3NzKCdib3R0b20nKSwgMTApKSB8fCAwXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIGVsZW1lbnRTY3JvbGw6IGZ1bmN0aW9uKHNjcm9sbCkge1xuICAgICAgICAgICAgc2Nyb2xsID0gc2Nyb2xsIHx8ICRzY3JvbGwuc2Nyb2xsVG9wKCk7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgZWxlbWVudCAgICAgICAgPSBtb2R1bGUuY2FjaGUuZWxlbWVudCxcbiAgICAgICAgICAgICAgc2Nyb2xsQ29udGV4dCAgPSBtb2R1bGUuY2FjaGUuc2Nyb2xsQ29udGV4dCxcbiAgICAgICAgICAgICAgZGVsdGEgICAgICAgICAgPSBtb2R1bGUuZ2V0LnNjcm9sbENoYW5nZShzY3JvbGwpLFxuICAgICAgICAgICAgICBtYXhTY3JvbGwgICAgICA9IChlbGVtZW50LmhlaWdodCAtIHNjcm9sbENvbnRleHQuaGVpZ2h0ICsgc2V0dGluZ3Mub2Zmc2V0KSxcbiAgICAgICAgICAgICAgZWxlbWVudFNjcm9sbCAgPSBtb2R1bGUuZ2V0LmN1cnJlbnRFbGVtZW50U2Nyb2xsKCksXG4gICAgICAgICAgICAgIHBvc3NpYmxlU2Nyb2xsID0gKGVsZW1lbnRTY3JvbGwgKyBkZWx0YSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKG1vZHVsZS5jYWNoZS5maXRzIHx8IHBvc3NpYmxlU2Nyb2xsIDwgMCkge1xuICAgICAgICAgICAgICBlbGVtZW50U2Nyb2xsID0gMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYocG9zc2libGVTY3JvbGwgPiBtYXhTY3JvbGwgKSB7XG4gICAgICAgICAgICAgIGVsZW1lbnRTY3JvbGwgPSBtYXhTY3JvbGw7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgZWxlbWVudFNjcm9sbCA9IHBvc3NpYmxlU2Nyb2xsO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGVsZW1lbnRTY3JvbGw7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlbW92ZToge1xuICAgICAgICAgIGxhc3RTY3JvbGw6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgZGVsZXRlIG1vZHVsZS5sYXN0U2Nyb2xsO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZWxlbWVudFNjcm9sbDogZnVuY3Rpb24oc2Nyb2xsKSB7XG4gICAgICAgICAgICBkZWxldGUgbW9kdWxlLmVsZW1lbnRTY3JvbGw7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBvZmZzZXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZS5jc3MoJ21hcmdpbi10b3AnLCAnJyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldDoge1xuICAgICAgICAgIG9mZnNldDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU2V0dGluZyBvZmZzZXQgb24gZWxlbWVudCcsIHNldHRpbmdzLm9mZnNldCk7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5jc3MoJ21hcmdpbi10b3AnLCBzZXR0aW5ncy5vZmZzZXQpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBjb250YWluZXJTaXplOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICB0YWdOYW1lID0gJGNvbnRhaW5lci5nZXQoMCkudGFnTmFtZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYodGFnTmFtZSA9PT0gJ0hUTUwnIHx8IHRhZ05hbWUgPT0gJ2JvZHknKSB7XG4gICAgICAgICAgICAgIC8vIHRoaXMgY2FuIHRyaWdnZXIgZm9yIHRvbyBtYW55IHJlYXNvbnNcbiAgICAgICAgICAgICAgLy9tb2R1bGUuZXJyb3IoZXJyb3IuY29udGFpbmVyLCB0YWdOYW1lLCAkbW9kdWxlKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmRldGVybWluZUNvbnRhaW5lcigpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIGlmKCBNYXRoLmFicygkY29udGFpbmVyLm91dGVySGVpZ2h0KCkgLSBtb2R1bGUuY2FjaGUuY29udGV4dC5oZWlnaHQpID4gc2V0dGluZ3Muaml0dGVyKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDb250ZXh0IGhhcyBwYWRkaW5nLCBzcGVjaWZ5aW5nIGV4YWN0IGhlaWdodCBmb3IgY29udGFpbmVyJywgbW9kdWxlLmNhY2hlLmNvbnRleHQuaGVpZ2h0KTtcbiAgICAgICAgICAgICAgICAkY29udGFpbmVyLmNzcyh7XG4gICAgICAgICAgICAgICAgICBoZWlnaHQ6IG1vZHVsZS5jYWNoZS5jb250ZXh0LmhlaWdodFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBtaW5pbXVtU2l6ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgZWxlbWVudCAgID0gbW9kdWxlLmNhY2hlLmVsZW1lbnRcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgICRjb250YWluZXJcbiAgICAgICAgICAgICAgLmNzcygnbWluLWhlaWdodCcsIGVsZW1lbnQuaGVpZ2h0KVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc2Nyb2xsOiBmdW5jdGlvbihzY3JvbGwpIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2V0dGluZyBzY3JvbGwgb24gZWxlbWVudCcsIHNjcm9sbCk7XG4gICAgICAgICAgICBpZihtb2R1bGUuZWxlbWVudFNjcm9sbCA9PSBzY3JvbGwpIHtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoIG1vZHVsZS5pcy50b3AoKSApIHtcbiAgICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAgIC5jc3MoJ2JvdHRvbScsICcnKVxuICAgICAgICAgICAgICAgIC5jc3MoJ3RvcCcsIC1zY3JvbGwpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCBtb2R1bGUuaXMuYm90dG9tKCkgKSB7XG4gICAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgICAuY3NzKCd0b3AnLCAnJylcbiAgICAgICAgICAgICAgICAuY3NzKCdib3R0b20nLCBzY3JvbGwpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHNpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmNhY2hlLmVsZW1lbnQuaGVpZ2h0ICE9PSAwICYmIG1vZHVsZS5jYWNoZS5lbGVtZW50LndpZHRoICE9PSAwKSB7XG4gICAgICAgICAgICAgIGVsZW1lbnQuc3R5bGUuc2V0UHJvcGVydHkoJ3dpZHRoJywgIG1vZHVsZS5jYWNoZS5lbGVtZW50LndpZHRoICArICdweCcsICdpbXBvcnRhbnQnKTtcbiAgICAgICAgICAgICAgZWxlbWVudC5zdHlsZS5zZXRQcm9wZXJ0eSgnaGVpZ2h0JywgbW9kdWxlLmNhY2hlLmVsZW1lbnQuaGVpZ2h0ICsgJ3B4JywgJ2ltcG9ydGFudCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBpczoge1xuICAgICAgICAgIHN0YW5kYXJkU2Nyb2xsOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAoJHNjcm9sbFswXSA9PSB3aW5kb3cpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgdG9wOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkbW9kdWxlLmhhc0NsYXNzKGNsYXNzTmFtZS50b3ApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYm90dG9tOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkbW9kdWxlLmhhc0NsYXNzKGNsYXNzTmFtZS5ib3R0b20pO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgaW5pdGlhbFBvc2l0aW9uOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAoIW1vZHVsZS5pcy5maXhlZCgpICYmICFtb2R1bGUuaXMuYm91bmQoKSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBoaWRkZW46IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICghJG1vZHVsZS5pcygnOnZpc2libGUnKSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBib3VuZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5oYXNDbGFzcyhjbGFzc05hbWUuYm91bmQpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZml4ZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRtb2R1bGUuaGFzQ2xhc3MoY2xhc3NOYW1lLmZpeGVkKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc3RpY2s6IGZ1bmN0aW9uKHNjcm9sbCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgY2FjaGVkUG9zaXRpb24gPSBzY3JvbGwgfHwgJHNjcm9sbC5zY3JvbGxUb3AoKSxcbiAgICAgICAgICAgIGNhY2hlICAgICAgICAgID0gbW9kdWxlLmNhY2hlLFxuICAgICAgICAgICAgZml0cyAgICAgICAgICAgPSBjYWNoZS5maXRzLFxuICAgICAgICAgICAgZWxlbWVudCAgICAgICAgPSBjYWNoZS5lbGVtZW50LFxuICAgICAgICAgICAgc2Nyb2xsQ29udGV4dCAgPSBjYWNoZS5zY3JvbGxDb250ZXh0LFxuICAgICAgICAgICAgY29udGV4dCAgICAgICAgPSBjYWNoZS5jb250ZXh0LFxuICAgICAgICAgICAgb2Zmc2V0ICAgICAgICAgPSAobW9kdWxlLmlzLmJvdHRvbSgpICYmIHNldHRpbmdzLnB1c2hpbmcpXG4gICAgICAgICAgICAgID8gc2V0dGluZ3MuYm90dG9tT2Zmc2V0XG4gICAgICAgICAgICAgIDogc2V0dGluZ3Mub2Zmc2V0LFxuICAgICAgICAgICAgc2Nyb2xsICAgICAgICAgPSB7XG4gICAgICAgICAgICAgIHRvcCAgICA6IGNhY2hlZFBvc2l0aW9uICsgb2Zmc2V0LFxuICAgICAgICAgICAgICBib3R0b20gOiBjYWNoZWRQb3NpdGlvbiArIG9mZnNldCArIHNjcm9sbENvbnRleHQuaGVpZ2h0XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZGlyZWN0aW9uICAgICAgPSBtb2R1bGUuZ2V0LmRpcmVjdGlvbihzY3JvbGwudG9wKSxcbiAgICAgICAgICAgIGVsZW1lbnRTY3JvbGwgID0gKGZpdHMpXG4gICAgICAgICAgICAgID8gMFxuICAgICAgICAgICAgICA6IG1vZHVsZS5nZXQuZWxlbWVudFNjcm9sbChzY3JvbGwudG9wKSxcblxuICAgICAgICAgICAgLy8gc2hvcnRoYW5kXG4gICAgICAgICAgICBkb2VzbnRGaXQgICAgICA9ICFmaXRzLFxuICAgICAgICAgICAgZWxlbWVudFZpc2libGUgPSAoZWxlbWVudC5oZWlnaHQgIT09IDApXG4gICAgICAgICAgO1xuXG4gICAgICAgICAgaWYoZWxlbWVudFZpc2libGUpIHtcblxuICAgICAgICAgICAgaWYoIG1vZHVsZS5pcy5pbml0aWFsUG9zaXRpb24oKSApIHtcbiAgICAgICAgICAgICAgaWYoc2Nyb2xsLnRvcCA+PSBjb250ZXh0LmJvdHRvbSkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW5pdGlhbCBlbGVtZW50IHBvc2l0aW9uIGlzIGJvdHRvbSBvZiBjb250YWluZXInKTtcbiAgICAgICAgICAgICAgICBtb2R1bGUuYmluZEJvdHRvbSgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoc2Nyb2xsLnRvcCA+IGVsZW1lbnQudG9wKSB7XG4gICAgICAgICAgICAgICAgaWYoIChlbGVtZW50LmhlaWdodCArIHNjcm9sbC50b3AgLSBlbGVtZW50U2Nyb2xsKSA+PSBjb250ZXh0LmJvdHRvbSApIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW5pdGlhbCBlbGVtZW50IHBvc2l0aW9uIGlzIGJvdHRvbSBvZiBjb250YWluZXInKTtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5iaW5kQm90dG9tKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdJbml0aWFsIGVsZW1lbnQgcG9zaXRpb24gaXMgZml4ZWQnKTtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5maXhUb3AoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiggbW9kdWxlLmlzLmZpeGVkKCkgKSB7XG5cbiAgICAgICAgICAgICAgLy8gY3VycmVudGx5IGZpeGVkIHRvcFxuICAgICAgICAgICAgICBpZiggbW9kdWxlLmlzLnRvcCgpICkge1xuICAgICAgICAgICAgICAgIGlmKCBzY3JvbGwudG9wIDw9IGVsZW1lbnQudG9wICkge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdGaXhlZCBlbGVtZW50IHJlYWNoZWQgdG9wIG9mIGNvbnRhaW5lcicpO1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnNldEluaXRpYWxQb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmKCAoZWxlbWVudC5oZWlnaHQgKyBzY3JvbGwudG9wIC0gZWxlbWVudFNjcm9sbCkgPj0gY29udGV4dC5ib3R0b20gKSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0ZpeGVkIGVsZW1lbnQgcmVhY2hlZCBib3R0b20gb2YgY29udGFpbmVyJyk7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuYmluZEJvdHRvbSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvLyBzY3JvbGwgZWxlbWVudCBpZiBsYXJnZXIgdGhhbiBzY3JlZW5cbiAgICAgICAgICAgICAgICBlbHNlIGlmKGRvZXNudEZpdCkge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnNldC5zY3JvbGwoZWxlbWVudFNjcm9sbCk7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuc2F2ZS5sYXN0U2Nyb2xsKHNjcm9sbC50b3ApO1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnNhdmUuZWxlbWVudFNjcm9sbChlbGVtZW50U2Nyb2xsKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAvLyBjdXJyZW50bHkgZml4ZWQgYm90dG9tXG4gICAgICAgICAgICAgIGVsc2UgaWYobW9kdWxlLmlzLmJvdHRvbSgpICkge1xuXG4gICAgICAgICAgICAgICAgLy8gdG9wIGVkZ2VcbiAgICAgICAgICAgICAgICBpZiggKHNjcm9sbC5ib3R0b20gLSBlbGVtZW50LmhlaWdodCkgPD0gZWxlbWVudC50b3ApIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQm90dG9tIGZpeGVkIHJhaWwgaGFzIHJlYWNoZWQgdG9wIG9mIGNvbnRhaW5lcicpO1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnNldEluaXRpYWxQb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvLyBib3R0b20gZWRnZVxuICAgICAgICAgICAgICAgIGVsc2UgaWYoc2Nyb2xsLmJvdHRvbSA+PSBjb250ZXh0LmJvdHRvbSkge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdCb3R0b20gZml4ZWQgcmFpbCBoYXMgcmVhY2hlZCBib3R0b20gb2YgY29udGFpbmVyJyk7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuYmluZEJvdHRvbSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvLyBzY3JvbGwgZWxlbWVudCBpZiBsYXJnZXIgdGhhbiBzY3JlZW5cbiAgICAgICAgICAgICAgICBlbHNlIGlmKGRvZXNudEZpdCkge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnNldC5zY3JvbGwoZWxlbWVudFNjcm9sbCk7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuc2F2ZS5sYXN0U2Nyb2xsKHNjcm9sbC50b3ApO1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnNhdmUuZWxlbWVudFNjcm9sbChlbGVtZW50U2Nyb2xsKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiggbW9kdWxlLmlzLmJvdHRvbSgpICkge1xuICAgICAgICAgICAgICBpZiggc2Nyb2xsLnRvcCA8PSBlbGVtZW50LnRvcCApIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0p1bXBlZCBmcm9tIGJvdHRvbSBmaXhlZCB0byB0b3AgZml4ZWQsIG1vc3QgbGlrZWx5IHVzZWQgaG9tZS9lbmQgYnV0dG9uJyk7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnNldEluaXRpYWxQb3NpdGlvbigpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGlmKHNldHRpbmdzLnB1c2hpbmcpIHtcbiAgICAgICAgICAgICAgICAgIGlmKG1vZHVsZS5pcy5ib3VuZCgpICYmIHNjcm9sbC5ib3R0b20gPD0gY29udGV4dC5ib3R0b20gKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRml4aW5nIGJvdHRvbSBhdHRhY2hlZCBlbGVtZW50IHRvIGJvdHRvbSBvZiBicm93c2VyLicpO1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUuZml4Qm90dG9tKCk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgaWYobW9kdWxlLmlzLmJvdW5kKCkgJiYgKHNjcm9sbC50b3AgPD0gY29udGV4dC5ib3R0b20gLSBlbGVtZW50LmhlaWdodCkgKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRml4aW5nIGJvdHRvbSBhdHRhY2hlZCBlbGVtZW50IHRvIHRvcCBvZiBicm93c2VyLicpO1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUuZml4VG9wKCk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGJpbmRUb3A6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQmluZGluZyBlbGVtZW50IHRvIHRvcCBvZiBwYXJlbnQgY29udGFpbmVyJyk7XG4gICAgICAgICAgbW9kdWxlLnJlbW92ZS5vZmZzZXQoKTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAuY3NzKHtcbiAgICAgICAgICAgICAgbGVmdCAgICAgICAgIDogJycsXG4gICAgICAgICAgICAgIHRvcCAgICAgICAgICA6ICcnLFxuICAgICAgICAgICAgICBtYXJnaW5Cb3R0b20gOiAnJ1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuZml4ZWQpXG4gICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmJvdHRvbSlcbiAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUuYm91bmQpXG4gICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLnRvcClcbiAgICAgICAgICA7XG4gICAgICAgICAgc2V0dGluZ3Mub25Ub3AuY2FsbChlbGVtZW50KTtcbiAgICAgICAgICBzZXR0aW5ncy5vblVuc3RpY2suY2FsbChlbGVtZW50KTtcbiAgICAgICAgfSxcbiAgICAgICAgYmluZEJvdHRvbTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdCaW5kaW5nIGVsZW1lbnQgdG8gYm90dG9tIG9mIHBhcmVudCBjb250YWluZXInKTtcbiAgICAgICAgICBtb2R1bGUucmVtb3ZlLm9mZnNldCgpO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICBsZWZ0ICAgICAgICAgOiAnJyxcbiAgICAgICAgICAgICAgdG9wICAgICAgICAgIDogJydcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmZpeGVkKVxuICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS50b3ApXG4gICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLmJvdW5kKVxuICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5ib3R0b20pXG4gICAgICAgICAgO1xuICAgICAgICAgIHNldHRpbmdzLm9uQm90dG9tLmNhbGwoZWxlbWVudCk7XG4gICAgICAgICAgc2V0dGluZ3Mub25VbnN0aWNrLmNhbGwoZWxlbWVudCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0SW5pdGlhbFBvc2l0aW9uOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ1JldHVybmluZyB0byBpbml0aWFsIHBvc2l0aW9uJyk7XG4gICAgICAgICAgbW9kdWxlLnVuZml4KCk7XG4gICAgICAgICAgbW9kdWxlLnVuYmluZCgpO1xuICAgICAgICB9LFxuXG5cbiAgICAgICAgZml4VG9wOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0ZpeGluZyBlbGVtZW50IHRvIHRvcCBvZiBwYWdlJyk7XG4gICAgICAgICAgbW9kdWxlLnNldC5taW5pbXVtU2l6ZSgpO1xuICAgICAgICAgIG1vZHVsZS5zZXQub2Zmc2V0KCk7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLmNzcyh7XG4gICAgICAgICAgICAgIGxlZnQgICAgICAgICA6IG1vZHVsZS5jYWNoZS5lbGVtZW50LmxlZnQsXG4gICAgICAgICAgICAgIGJvdHRvbSAgICAgICA6ICcnLFxuICAgICAgICAgICAgICBtYXJnaW5Cb3R0b20gOiAnJ1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuYm91bmQpXG4gICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmJvdHRvbSlcbiAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUuZml4ZWQpXG4gICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLnRvcClcbiAgICAgICAgICA7XG4gICAgICAgICAgc2V0dGluZ3Mub25TdGljay5jYWxsKGVsZW1lbnQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGZpeEJvdHRvbTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdTdGlja2luZyBlbGVtZW50IHRvIGJvdHRvbSBvZiBwYWdlJyk7XG4gICAgICAgICAgbW9kdWxlLnNldC5taW5pbXVtU2l6ZSgpO1xuICAgICAgICAgIG1vZHVsZS5zZXQub2Zmc2V0KCk7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLmNzcyh7XG4gICAgICAgICAgICAgIGxlZnQgICAgICAgICA6IG1vZHVsZS5jYWNoZS5lbGVtZW50LmxlZnQsXG4gICAgICAgICAgICAgIGJvdHRvbSAgICAgICA6ICcnLFxuICAgICAgICAgICAgICBtYXJnaW5Cb3R0b20gOiAnJ1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuYm91bmQpXG4gICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLnRvcClcbiAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUuZml4ZWQpXG4gICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLmJvdHRvbSlcbiAgICAgICAgICA7XG4gICAgICAgICAgc2V0dGluZ3Mub25TdGljay5jYWxsKGVsZW1lbnQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHVuYmluZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIG1vZHVsZS5pcy5ib3VuZCgpICkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZW1vdmluZyBjb250YWluZXIgYm91bmQgcG9zaXRpb24gb24gZWxlbWVudCcpO1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5vZmZzZXQoKTtcbiAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5ib3VuZClcbiAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS50b3ApXG4gICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuYm90dG9tKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICB1bmZpeDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIG1vZHVsZS5pcy5maXhlZCgpICkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZW1vdmluZyBmaXhlZCBwb3NpdGlvbiBvbiBlbGVtZW50Jyk7XG4gICAgICAgICAgICBtb2R1bGUucmVtb3ZlLm9mZnNldCgpO1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmZpeGVkKVxuICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLnRvcClcbiAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5ib3R0b20pXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBzZXR0aW5ncy5vblVuc3RpY2suY2FsbChlbGVtZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVzZXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnUmVzZXR0aW5nIGVsZW1lbnRzIHBvc2l0aW9uJyk7XG4gICAgICAgICAgbW9kdWxlLnVuYmluZCgpO1xuICAgICAgICAgIG1vZHVsZS51bmZpeCgpO1xuICAgICAgICAgIG1vZHVsZS5yZXNldENTUygpO1xuICAgICAgICAgIG1vZHVsZS5yZW1vdmUub2Zmc2V0KCk7XG4gICAgICAgICAgbW9kdWxlLnJlbW92ZS5sYXN0U2Nyb2xsKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVzZXRDU1M6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICB3aWR0aCAgOiAnJyxcbiAgICAgICAgICAgICAgaGVpZ2h0IDogJydcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgO1xuICAgICAgICAgICRjb250YWluZXJcbiAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICBoZWlnaHQ6ICcnXG4gICAgICAgICAgICB9KVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBzZXR0aW5nOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBzZXR0aW5ncywgbmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgc2V0dGluZ3NbbmFtZV0gPSB2YWx1ZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gc2V0dGluZ3NbbmFtZV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnRlcm5hbDogZnVuY3Rpb24obmFtZSwgdmFsdWUpIHtcbiAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgbW9kdWxlLCBuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBtb2R1bGVbbmFtZV0gPSB2YWx1ZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZGVidWc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1Zy5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdmVyYm9zZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy52ZXJib3NlICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UubG9nKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBlcnJvcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCkge1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmVycm9yLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvci5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgcGVyZm9ybWFuY2U6IHtcbiAgICAgICAgICBsb2c6IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBjdXJyZW50VGltZSxcbiAgICAgICAgICAgICAgZXhlY3V0aW9uVGltZSxcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBjdXJyZW50VGltZSAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG4gICAgICAgICAgICAgIHByZXZpb3VzVGltZSAgPSB0aW1lIHx8IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lID0gY3VycmVudFRpbWUgLSBwcmV2aW91c1RpbWU7XG4gICAgICAgICAgICAgIHRpbWUgICAgICAgICAgPSBjdXJyZW50VGltZTtcbiAgICAgICAgICAgICAgcGVyZm9ybWFuY2UucHVzaCh7XG4gICAgICAgICAgICAgICAgJ05hbWUnICAgICAgICAgICA6IG1lc3NhZ2VbMF0sXG4gICAgICAgICAgICAgICAgJ0FyZ3VtZW50cycgICAgICA6IFtdLnNsaWNlLmNhbGwobWVzc2FnZSwgMSkgfHwgJycsXG4gICAgICAgICAgICAgICAgJ0VsZW1lbnQnICAgICAgICA6IGVsZW1lbnQsXG4gICAgICAgICAgICAgICAgJ0V4ZWN1dGlvbiBUaW1lJyA6IGV4ZWN1dGlvblRpbWVcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lciA9IHNldFRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLmRpc3BsYXksIDApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZGlzcGxheTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgdGl0bGUgPSBzZXR0aW5ncy5uYW1lICsgJzonLFxuICAgICAgICAgICAgICB0b3RhbFRpbWUgPSAwXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICB0aW1lID0gZmFsc2U7XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgdG90YWxUaW1lICs9IGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ107XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHRpdGxlICs9ICcgJyArIHRvdGFsVGltZSArICdtcyc7XG4gICAgICAgICAgICBpZihtb2R1bGVTZWxlY3Rvcikge1xuICAgICAgICAgICAgICB0aXRsZSArPSAnIFxcJycgKyBtb2R1bGVTZWxlY3RvciArICdcXCcnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoIChjb25zb2xlLmdyb3VwICE9PSB1bmRlZmluZWQgfHwgY29uc29sZS50YWJsZSAhPT0gdW5kZWZpbmVkKSAmJiBwZXJmb3JtYW5jZS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBDb2xsYXBzZWQodGl0bGUpO1xuICAgICAgICAgICAgICBpZihjb25zb2xlLnRhYmxlKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS50YWJsZShwZXJmb3JtYW5jZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YVsnTmFtZSddICsgJzogJyArIGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ10rJ21zJyk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgY29uc29sZS5ncm91cEVuZCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcGVyZm9ybWFuY2UgPSBbXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludm9rZTogZnVuY3Rpb24ocXVlcnksIHBhc3NlZEFyZ3VtZW50cywgY29udGV4dCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgb2JqZWN0ID0gaW5zdGFuY2UsXG4gICAgICAgICAgICBtYXhEZXB0aCxcbiAgICAgICAgICAgIGZvdW5kLFxuICAgICAgICAgICAgcmVzcG9uc2VcbiAgICAgICAgICA7XG4gICAgICAgICAgcGFzc2VkQXJndW1lbnRzID0gcGFzc2VkQXJndW1lbnRzIHx8IHF1ZXJ5QXJndW1lbnRzO1xuICAgICAgICAgIGNvbnRleHQgICAgICAgICA9IGVsZW1lbnQgICAgICAgICB8fCBjb250ZXh0O1xuICAgICAgICAgIGlmKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyAmJiBvYmplY3QgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnkgICAgPSBxdWVyeS5zcGxpdCgvW1xcLiBdLyk7XG4gICAgICAgICAgICBtYXhEZXB0aCA9IHF1ZXJ5Lmxlbmd0aCAtIDE7XG4gICAgICAgICAgICAkLmVhY2gocXVlcnksIGZ1bmN0aW9uKGRlcHRoLCB2YWx1ZSkge1xuICAgICAgICAgICAgICB2YXIgY2FtZWxDYXNlVmFsdWUgPSAoZGVwdGggIT0gbWF4RGVwdGgpXG4gICAgICAgICAgICAgICAgPyB2YWx1ZSArIHF1ZXJ5W2RlcHRoICsgMV0uY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBxdWVyeVtkZXB0aCArIDFdLnNsaWNlKDEpXG4gICAgICAgICAgICAgICAgOiBxdWVyeVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gKSAmJiAoZGVwdGggIT0gbWF4RGVwdGgpICkge1xuICAgICAgICAgICAgICAgIG9iamVjdCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgICAgIGZvdW5kID0gb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbdmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFt2YWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFt2YWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmICggJC5pc0Z1bmN0aW9uKCBmb3VuZCApICkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZC5hcHBseShjb250ZXh0LCBwYXNzZWRBcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKGZvdW5kICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gZm91bmQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKCQuaXNBcnJheShyZXR1cm5lZFZhbHVlKSkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZS5wdXNoKHJlc3BvbnNlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSBbcmV0dXJuZWRWYWx1ZSwgcmVzcG9uc2VdO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJlc3BvbnNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSByZXNwb25zZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGZvdW5kO1xuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICBpZihtZXRob2RJbnZva2VkKSB7XG4gICAgICAgIGlmKGluc3RhbmNlID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbnZva2UocXVlcnkpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGlmKGluc3RhbmNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBpbnN0YW5jZS5pbnZva2UoJ2Rlc3Ryb3knKTtcbiAgICAgICAgfVxuICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgfVxuICAgIH0pXG4gIDtcblxuICByZXR1cm4gKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZClcbiAgICA/IHJldHVybmVkVmFsdWVcbiAgICA6IHRoaXNcbiAgO1xufTtcblxuJC5mbi5zdGlja3kuc2V0dGluZ3MgPSB7XG5cbiAgbmFtZSAgICAgICAgICAgOiAnU3RpY2t5JyxcbiAgbmFtZXNwYWNlICAgICAgOiAnc3RpY2t5JyxcblxuICBzaWxlbnQgICAgICAgICA6IGZhbHNlLFxuICBkZWJ1ZyAgICAgICAgICA6IGZhbHNlLFxuICB2ZXJib3NlICAgICAgICA6IHRydWUsXG4gIHBlcmZvcm1hbmNlICAgIDogdHJ1ZSxcblxuICAvLyB3aGV0aGVyIHRvIHN0aWNrIGluIHRoZSBvcHBvc2l0ZSBkaXJlY3Rpb24gb24gc2Nyb2xsIHVwXG4gIHB1c2hpbmcgICAgICAgIDogZmFsc2UsXG5cbiAgY29udGV4dCAgICAgICAgOiBmYWxzZSxcblxuICAvLyBDb250ZXh0IHRvIHdhdGNoIHNjcm9sbCBldmVudHNcbiAgc2Nyb2xsQ29udGV4dCAgOiB3aW5kb3csXG5cbiAgLy8gT2Zmc2V0IHRvIGFkanVzdCBzY3JvbGxcbiAgb2Zmc2V0ICAgICAgICAgOiAwLFxuXG4gIC8vIE9mZnNldCB0byBhZGp1c3Qgc2Nyb2xsIHdoZW4gYXR0YWNoZWQgdG8gYm90dG9tIG9mIHNjcmVlblxuICBib3R0b21PZmZzZXQgICA6IDAsXG5cbiAgaml0dGVyICAgICAgICAgOiA1LCAvLyB3aWxsIG9ubHkgc2V0IGNvbnRhaW5lciBoZWlnaHQgaWYgZGlmZmVyZW5jZSBiZXR3ZWVuIGNvbnRleHQgYW5kIGNvbnRhaW5lciBpcyBsYXJnZXIgdGhhbiB0aGlzIG51bWJlclxuXG4gIC8vIFdoZXRoZXIgdG8gYXV0b21hdGljYWxseSBvYnNlcnZlIGNoYW5nZXMgd2l0aCBNdXRhdGlvbiBPYnNlcnZlcnNcbiAgb2JzZXJ2ZUNoYW5nZXMgOiBmYWxzZSxcblxuICAvLyBDYWxsZWQgd2hlbiBwb3NpdGlvbiBpcyByZWNhbGN1bGF0ZWRcbiAgb25SZXBvc2l0aW9uICAgOiBmdW5jdGlvbigpe30sXG5cbiAgLy8gQ2FsbGVkIG9uIGVhY2ggc2Nyb2xsXG4gIG9uU2Nyb2xsICAgICAgIDogZnVuY3Rpb24oKXt9LFxuXG4gIC8vIENhbGxlZCB3aGVuIGVsZW1lbnQgaXMgc3R1Y2sgdG8gdmlld3BvcnRcbiAgb25TdGljayAgICAgICAgOiBmdW5jdGlvbigpe30sXG5cbiAgLy8gQ2FsbGVkIHdoZW4gZWxlbWVudCBpcyB1bnN0dWNrIGZyb20gdmlld3BvcnRcbiAgb25VbnN0aWNrICAgICAgOiBmdW5jdGlvbigpe30sXG5cbiAgLy8gQ2FsbGVkIHdoZW4gZWxlbWVudCByZWFjaGVzIHRvcCBvZiBjb250ZXh0XG4gIG9uVG9wICAgICAgICAgIDogZnVuY3Rpb24oKXt9LFxuXG4gIC8vIENhbGxlZCB3aGVuIGVsZW1lbnQgcmVhY2hlcyBib3R0b20gb2YgY29udGV4dFxuICBvbkJvdHRvbSAgICAgICA6IGZ1bmN0aW9uKCl7fSxcblxuICBlcnJvciAgICAgICAgIDoge1xuICAgIGNvbnRhaW5lciAgICAgIDogJ1N0aWNreSBlbGVtZW50IG11c3QgYmUgaW5zaWRlIGEgcmVsYXRpdmUgY29udGFpbmVyJyxcbiAgICB2aXNpYmxlICAgICAgICA6ICdFbGVtZW50IGlzIGhpZGRlbiwgeW91IG11c3QgY2FsbCByZWZyZXNoIGFmdGVyIGVsZW1lbnQgYmVjb21lcyB2aXNpYmxlLiBVc2Ugc2lsZW50IHNldHRpbmcgdG8gc3VycHJlc3MgdGhpcyB3YXJuaW5nIGluIHByb2R1Y3Rpb24uJyxcbiAgICBtZXRob2QgICAgICAgICA6ICdUaGUgbWV0aG9kIHlvdSBjYWxsZWQgaXMgbm90IGRlZmluZWQuJyxcbiAgICBpbnZhbGlkQ29udGV4dCA6ICdDb250ZXh0IHNwZWNpZmllZCBkb2VzIG5vdCBleGlzdCcsXG4gICAgZWxlbWVudFNpemUgICAgOiAnU3RpY2t5IGVsZW1lbnQgaXMgbGFyZ2VyIHRoYW4gaXRzIGNvbnRhaW5lciwgY2Fubm90IGNyZWF0ZSBzdGlja3kuJ1xuICB9LFxuXG4gIGNsYXNzTmFtZSA6IHtcbiAgICBib3VuZCAgICAgOiAnYm91bmQnLFxuICAgIGZpeGVkICAgICA6ICdmaXhlZCcsXG4gICAgc3VwcG9ydGVkIDogJ25hdGl2ZScsXG4gICAgdG9wICAgICAgIDogJ3RvcCcsXG4gICAgYm90dG9tICAgIDogJ2JvdHRvbSdcbiAgfVxuXG59O1xuXG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7XG4iXX0=