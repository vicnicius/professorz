/*!
 * # Semantic UI - Nag
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.nag = function (parameters) {
    var $allModules = $(this),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue;
    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.nag.settings, parameters) : $.extend({}, $.fn.nag.settings),
          className = settings.className,
          selector = settings.selector,
          error = settings.error,
          namespace = settings.namespace,
          eventNamespace = '.' + namespace,
          moduleNamespace = namespace + '-module',
          $module = $(this),
          $close = $module.find(selector.close),
          $context = settings.context ? $(settings.context) : $('body'),
          element = this,
          instance = $module.data(moduleNamespace),
          moduleOffset,
          moduleHeight,
          contextWidth,
          contextHeight,
          contextOffset,
          yOffset,
          yPosition,
          timer,
          module,
          requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
        setTimeout(callback, 0);
      };
      module = {

        initialize: function () {
          module.verbose('Initializing element');

          $module.on('click' + eventNamespace, selector.close, module.dismiss).data(moduleNamespace, module);

          if (settings.detachable && $module.parent()[0] !== $context[0]) {
            $module.detach().prependTo($context);
          }

          if (settings.displayTime > 0) {
            setTimeout(module.hide, settings.displayTime);
          }
          module.show();
        },

        destroy: function () {
          module.verbose('Destroying instance');
          $module.removeData(moduleNamespace).off(eventNamespace);
        },

        show: function () {
          if (module.should.show() && !$module.is(':visible')) {
            module.debug('Showing nag', settings.animation.show);
            if (settings.animation.show == 'fade') {
              $module.fadeIn(settings.duration, settings.easing);
            } else {
              $module.slideDown(settings.duration, settings.easing);
            }
          }
        },

        hide: function () {
          module.debug('Showing nag', settings.animation.hide);
          if (settings.animation.show == 'fade') {
            $module.fadeIn(settings.duration, settings.easing);
          } else {
            $module.slideUp(settings.duration, settings.easing);
          }
        },

        onHide: function () {
          module.debug('Removing nag', settings.animation.hide);
          $module.remove();
          if (settings.onHide) {
            settings.onHide();
          }
        },

        dismiss: function (event) {
          if (settings.storageMethod) {
            module.storage.set(settings.key, settings.value);
          }
          module.hide();
          event.stopImmediatePropagation();
          event.preventDefault();
        },

        should: {
          show: function () {
            if (settings.persist) {
              module.debug('Persistent nag is set, can show nag');
              return true;
            }
            if (module.storage.get(settings.key) != settings.value.toString()) {
              module.debug('Stored value is not set, can show nag', module.storage.get(settings.key));
              return true;
            }
            module.debug('Stored value is set, cannot show nag', module.storage.get(settings.key));
            return false;
          }
        },

        get: {
          storageOptions: function () {
            var options = {};
            if (settings.expires) {
              options.expires = settings.expires;
            }
            if (settings.domain) {
              options.domain = settings.domain;
            }
            if (settings.path) {
              options.path = settings.path;
            }
            return options;
          }
        },

        clear: function () {
          module.storage.remove(settings.key);
        },

        storage: {
          set: function (key, value) {
            var options = module.get.storageOptions();
            if (settings.storageMethod == 'localstorage' && window.localStorage !== undefined) {
              window.localStorage.setItem(key, value);
              module.debug('Value stored using local storage', key, value);
            } else if (settings.storageMethod == 'sessionstorage' && window.sessionStorage !== undefined) {
              window.sessionStorage.setItem(key, value);
              module.debug('Value stored using session storage', key, value);
            } else if ($.cookie !== undefined) {
              $.cookie(key, value, options);
              module.debug('Value stored using cookie', key, value, options);
            } else {
              module.error(error.noCookieStorage);
              return;
            }
          },
          get: function (key, value) {
            var storedValue;
            if (settings.storageMethod == 'localstorage' && window.localStorage !== undefined) {
              storedValue = window.localStorage.getItem(key);
            } else if (settings.storageMethod == 'sessionstorage' && window.sessionStorage !== undefined) {
              storedValue = window.sessionStorage.getItem(key);
            }
            // get by cookie
            else if ($.cookie !== undefined) {
                storedValue = $.cookie(key);
              } else {
                module.error(error.noCookieStorage);
              }
            if (storedValue == 'undefined' || storedValue == 'null' || storedValue === undefined || storedValue === null) {
              storedValue = undefined;
            }
            return storedValue;
          },
          remove: function (key) {
            var options = module.get.storageOptions();
            if (settings.storageMethod == 'localstorage' && window.localStorage !== undefined) {
              window.localStorage.removeItem(key);
            } else if (settings.storageMethod == 'sessionstorage' && window.sessionStorage !== undefined) {
              window.sessionStorage.removeItem(key);
            }
            // store by cookie
            else if ($.cookie !== undefined) {
                $.removeCookie(key, options);
              } else {
                module.error(error.noStorage);
              }
          }
        },

        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                module.error(error.method, query);
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };

      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.nag.settings = {

    name: 'Nag',

    silent: false,
    debug: false,
    verbose: false,
    performance: true,

    namespace: 'Nag',

    // allows cookie to be overridden
    persist: false,

    // set to zero to require manually dismissal, otherwise hides on its own
    displayTime: 0,

    animation: {
      show: 'slide',
      hide: 'slide'
    },

    context: false,
    detachable: false,

    expires: 30,
    domain: false,
    path: '/',

    // type of storage to use
    storageMethod: 'cookie',

    // value to store in dismissed localstorage/cookie
    key: 'nag',
    value: 'dismiss',

    error: {
      noCookieStorage: '$.cookie is not included. A storage solution is required.',
      noStorage: 'Neither $.cookie or store is defined. A storage solution is required for storing state',
      method: 'The method you called is not defined.'
    },

    className: {
      bottom: 'bottom',
      fixed: 'fixed'
    },

    selector: {
      close: '.close.icon'
    },

    speed: 500,
    easing: 'easeOutQuad',

    onHide: function () {}

  };

  // Adds easing
  $.extend($.easing, {
    easeOutQuad: function (x, t, b, c, d) {
      return -c * (t /= d) * (t - 2) + b;
    }
  });
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL25hZy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBVUEsQ0FBQyxDQUFDLFVBQVUsQ0FBVixFQUFhLE1BQWIsRUFBcUIsUUFBckIsRUFBK0IsU0FBL0IsRUFBMEM7O0FBRTVDOztBQUVBLFdBQVUsT0FBTyxNQUFQLElBQWlCLFdBQWpCLElBQWdDLE9BQU8sSUFBUCxJQUFlLElBQWhELEdBQ0wsTUFESyxHQUVKLE9BQU8sSUFBUCxJQUFlLFdBQWYsSUFBOEIsS0FBSyxJQUFMLElBQWEsSUFBNUMsR0FDRSxJQURGLEdBRUUsU0FBUyxhQUFULEdBSk47O0FBT0EsSUFBRSxFQUFGLENBQUssR0FBTCxHQUFXLFVBQVMsVUFBVCxFQUFxQjtBQUM5QixRQUNFLGNBQWlCLEVBQUUsSUFBRixDQURuQjtBQUFBLFFBRUUsaUJBQWlCLFlBQVksUUFBWixJQUF3QixFQUYzQztBQUFBLFFBSUUsT0FBaUIsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUpuQjtBQUFBLFFBS0UsY0FBaUIsRUFMbkI7QUFBQSxRQU9FLFFBQWlCLFVBQVUsQ0FBVixDQVBuQjtBQUFBLFFBUUUsZ0JBQWtCLE9BQU8sS0FBUCxJQUFnQixRQVJwQztBQUFBLFFBU0UsaUJBQWlCLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLENBQXpCLENBVG5CO0FBQUEsUUFVRSxhQVZGO0FBWUEsZ0JBQ0csSUFESCxDQUNRLFlBQVc7QUFDZixVQUNFLFdBQXNCLEVBQUUsYUFBRixDQUFnQixVQUFoQixDQUFGLEdBQ2hCLEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLEVBQUUsRUFBRixDQUFLLEdBQUwsQ0FBUyxRQUE1QixFQUFzQyxVQUF0QyxDQURnQixHQUVoQixFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsRUFBRSxFQUFGLENBQUssR0FBTCxDQUFTLFFBQXRCLENBSE47QUFBQSxVQUtFLFlBQWtCLFNBQVMsU0FMN0I7QUFBQSxVQU1FLFdBQWtCLFNBQVMsUUFON0I7QUFBQSxVQU9FLFFBQWtCLFNBQVMsS0FQN0I7QUFBQSxVQVFFLFlBQWtCLFNBQVMsU0FSN0I7QUFBQSxVQVVFLGlCQUFrQixNQUFNLFNBVjFCO0FBQUEsVUFXRSxrQkFBa0IsWUFBWSxTQVhoQztBQUFBLFVBYUUsVUFBa0IsRUFBRSxJQUFGLENBYnBCO0FBQUEsVUFlRSxTQUFrQixRQUFRLElBQVIsQ0FBYSxTQUFTLEtBQXRCLENBZnBCO0FBQUEsVUFnQkUsV0FBbUIsU0FBUyxPQUFWLEdBQ2QsRUFBRSxTQUFTLE9BQVgsQ0FEYyxHQUVkLEVBQUUsTUFBRixDQWxCTjtBQUFBLFVBb0JFLFVBQWtCLElBcEJwQjtBQUFBLFVBcUJFLFdBQWtCLFFBQVEsSUFBUixDQUFhLGVBQWIsQ0FyQnBCO0FBQUEsVUF1QkUsWUF2QkY7QUFBQSxVQXdCRSxZQXhCRjtBQUFBLFVBMEJFLFlBMUJGO0FBQUEsVUEyQkUsYUEzQkY7QUFBQSxVQTRCRSxhQTVCRjtBQUFBLFVBOEJFLE9BOUJGO0FBQUEsVUErQkUsU0EvQkY7QUFBQSxVQWlDRSxLQWpDRjtBQUFBLFVBa0NFLE1BbENGO0FBQUEsVUFvQ0Usd0JBQXdCLE9BQU8scUJBQVAsSUFDbkIsT0FBTyx3QkFEWSxJQUVuQixPQUFPLDJCQUZZLElBR25CLE9BQU8sdUJBSFksSUFJbkIsVUFBUyxRQUFULEVBQW1CO0FBQUUsbUJBQVcsUUFBWCxFQUFxQixDQUFyQjtBQUEwQixPQXhDdEQ7QUEwQ0EsZUFBUzs7QUFFUCxvQkFBWSxZQUFXO0FBQ3JCLGlCQUFPLE9BQVAsQ0FBZSxzQkFBZjs7QUFFQSxrQkFDRyxFQURILENBQ00sVUFBVSxjQURoQixFQUNnQyxTQUFTLEtBRHpDLEVBQ2dELE9BQU8sT0FEdkQsRUFFRyxJQUZILENBRVEsZUFGUixFQUV5QixNQUZ6Qjs7QUFLQSxjQUFHLFNBQVMsVUFBVCxJQUF1QixRQUFRLE1BQVIsR0FBaUIsQ0FBakIsTUFBd0IsU0FBUyxDQUFULENBQWxELEVBQStEO0FBQzdELG9CQUNHLE1BREgsR0FFRyxTQUZILENBRWEsUUFGYjtBQUlEOztBQUVELGNBQUcsU0FBUyxXQUFULEdBQXVCLENBQTFCLEVBQTZCO0FBQzNCLHVCQUFXLE9BQU8sSUFBbEIsRUFBd0IsU0FBUyxXQUFqQztBQUNEO0FBQ0QsaUJBQU8sSUFBUDtBQUNELFNBckJNOztBQXVCUCxpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLE9BQVAsQ0FBZSxxQkFBZjtBQUNBLGtCQUNHLFVBREgsQ0FDYyxlQURkLEVBRUcsR0FGSCxDQUVPLGNBRlA7QUFJRCxTQTdCTTs7QUErQlAsY0FBTSxZQUFXO0FBQ2YsY0FBSSxPQUFPLE1BQVAsQ0FBYyxJQUFkLE1BQXdCLENBQUMsUUFBUSxFQUFSLENBQVcsVUFBWCxDQUE3QixFQUFzRDtBQUNwRCxtQkFBTyxLQUFQLENBQWEsYUFBYixFQUE0QixTQUFTLFNBQVQsQ0FBbUIsSUFBL0M7QUFDQSxnQkFBRyxTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsSUFBMkIsTUFBOUIsRUFBc0M7QUFDcEMsc0JBQ0csTUFESCxDQUNVLFNBQVMsUUFEbkIsRUFDNkIsU0FBUyxNQUR0QztBQUdELGFBSkQsTUFLSztBQUNILHNCQUNHLFNBREgsQ0FDYSxTQUFTLFFBRHRCLEVBQ2dDLFNBQVMsTUFEekM7QUFHRDtBQUNGO0FBQ0YsU0E3Q007O0FBK0NQLGNBQU0sWUFBVztBQUNmLGlCQUFPLEtBQVAsQ0FBYSxhQUFiLEVBQTRCLFNBQVMsU0FBVCxDQUFtQixJQUEvQztBQUNBLGNBQUcsU0FBUyxTQUFULENBQW1CLElBQW5CLElBQTJCLE1BQTlCLEVBQXNDO0FBQ3BDLG9CQUNHLE1BREgsQ0FDVSxTQUFTLFFBRG5CLEVBQzZCLFNBQVMsTUFEdEM7QUFHRCxXQUpELE1BS0s7QUFDSCxvQkFDRyxPQURILENBQ1csU0FBUyxRQURwQixFQUM4QixTQUFTLE1BRHZDO0FBR0Q7QUFDRixTQTNETTs7QUE2RFAsZ0JBQVEsWUFBVztBQUNqQixpQkFBTyxLQUFQLENBQWEsY0FBYixFQUE2QixTQUFTLFNBQVQsQ0FBbUIsSUFBaEQ7QUFDQSxrQkFBUSxNQUFSO0FBQ0EsY0FBSSxTQUFTLE1BQWIsRUFBcUI7QUFDbkIscUJBQVMsTUFBVDtBQUNEO0FBQ0YsU0FuRU07O0FBcUVQLGlCQUFTLFVBQVMsS0FBVCxFQUFnQjtBQUN2QixjQUFHLFNBQVMsYUFBWixFQUEyQjtBQUN6QixtQkFBTyxPQUFQLENBQWUsR0FBZixDQUFtQixTQUFTLEdBQTVCLEVBQWlDLFNBQVMsS0FBMUM7QUFDRDtBQUNELGlCQUFPLElBQVA7QUFDQSxnQkFBTSx3QkFBTjtBQUNBLGdCQUFNLGNBQU47QUFDRCxTQTVFTTs7QUE4RVAsZ0JBQVE7QUFDTixnQkFBTSxZQUFXO0FBQ2YsZ0JBQUcsU0FBUyxPQUFaLEVBQXFCO0FBQ25CLHFCQUFPLEtBQVAsQ0FBYSxxQ0FBYjtBQUNBLHFCQUFPLElBQVA7QUFDRDtBQUNELGdCQUFJLE9BQU8sT0FBUCxDQUFlLEdBQWYsQ0FBbUIsU0FBUyxHQUE1QixLQUFvQyxTQUFTLEtBQVQsQ0FBZSxRQUFmLEVBQXhDLEVBQW9FO0FBQ2xFLHFCQUFPLEtBQVAsQ0FBYSx1Q0FBYixFQUFzRCxPQUFPLE9BQVAsQ0FBZSxHQUFmLENBQW1CLFNBQVMsR0FBNUIsQ0FBdEQ7QUFDQSxxQkFBTyxJQUFQO0FBQ0Q7QUFDRCxtQkFBTyxLQUFQLENBQWEsc0NBQWIsRUFBcUQsT0FBTyxPQUFQLENBQWUsR0FBZixDQUFtQixTQUFTLEdBQTVCLENBQXJEO0FBQ0EsbUJBQU8sS0FBUDtBQUNEO0FBWkssU0E5RUQ7O0FBNkZQLGFBQUs7QUFDSCwwQkFBZ0IsWUFBVztBQUN6QixnQkFDRSxVQUFVLEVBRFo7QUFHQSxnQkFBRyxTQUFTLE9BQVosRUFBcUI7QUFDbkIsc0JBQVEsT0FBUixHQUFrQixTQUFTLE9BQTNCO0FBQ0Q7QUFDRCxnQkFBRyxTQUFTLE1BQVosRUFBb0I7QUFDbEIsc0JBQVEsTUFBUixHQUFpQixTQUFTLE1BQTFCO0FBQ0Q7QUFDRCxnQkFBRyxTQUFTLElBQVosRUFBa0I7QUFDaEIsc0JBQVEsSUFBUixHQUFlLFNBQVMsSUFBeEI7QUFDRDtBQUNELG1CQUFPLE9BQVA7QUFDRDtBQWZFLFNBN0ZFOztBQStHUCxlQUFPLFlBQVc7QUFDaEIsaUJBQU8sT0FBUCxDQUFlLE1BQWYsQ0FBc0IsU0FBUyxHQUEvQjtBQUNELFNBakhNOztBQW1IUCxpQkFBUztBQUNQLGVBQUssVUFBUyxHQUFULEVBQWMsS0FBZCxFQUFxQjtBQUN4QixnQkFDRSxVQUFVLE9BQU8sR0FBUCxDQUFXLGNBQVgsRUFEWjtBQUdBLGdCQUFHLFNBQVMsYUFBVCxJQUEwQixjQUExQixJQUE0QyxPQUFPLFlBQVAsS0FBd0IsU0FBdkUsRUFBa0Y7QUFDaEYscUJBQU8sWUFBUCxDQUFvQixPQUFwQixDQUE0QixHQUE1QixFQUFpQyxLQUFqQztBQUNBLHFCQUFPLEtBQVAsQ0FBYSxrQ0FBYixFQUFpRCxHQUFqRCxFQUFzRCxLQUF0RDtBQUNELGFBSEQsTUFJSyxJQUFHLFNBQVMsYUFBVCxJQUEwQixnQkFBMUIsSUFBOEMsT0FBTyxjQUFQLEtBQTBCLFNBQTNFLEVBQXNGO0FBQ3pGLHFCQUFPLGNBQVAsQ0FBc0IsT0FBdEIsQ0FBOEIsR0FBOUIsRUFBbUMsS0FBbkM7QUFDQSxxQkFBTyxLQUFQLENBQWEsb0NBQWIsRUFBbUQsR0FBbkQsRUFBd0QsS0FBeEQ7QUFDRCxhQUhJLE1BSUEsSUFBRyxFQUFFLE1BQUYsS0FBYSxTQUFoQixFQUEyQjtBQUM5QixnQkFBRSxNQUFGLENBQVMsR0FBVCxFQUFjLEtBQWQsRUFBcUIsT0FBckI7QUFDQSxxQkFBTyxLQUFQLENBQWEsMkJBQWIsRUFBMEMsR0FBMUMsRUFBK0MsS0FBL0MsRUFBc0QsT0FBdEQ7QUFDRCxhQUhJLE1BSUE7QUFDSCxxQkFBTyxLQUFQLENBQWEsTUFBTSxlQUFuQjtBQUNBO0FBQ0Q7QUFDRixXQXJCTTtBQXNCUCxlQUFLLFVBQVMsR0FBVCxFQUFjLEtBQWQsRUFBcUI7QUFDeEIsZ0JBQ0UsV0FERjtBQUdBLGdCQUFHLFNBQVMsYUFBVCxJQUEwQixjQUExQixJQUE0QyxPQUFPLFlBQVAsS0FBd0IsU0FBdkUsRUFBa0Y7QUFDaEYsNEJBQWMsT0FBTyxZQUFQLENBQW9CLE9BQXBCLENBQTRCLEdBQTVCLENBQWQ7QUFDRCxhQUZELE1BR0ssSUFBRyxTQUFTLGFBQVQsSUFBMEIsZ0JBQTFCLElBQThDLE9BQU8sY0FBUCxLQUEwQixTQUEzRSxFQUFzRjtBQUN6Riw0QkFBYyxPQUFPLGNBQVAsQ0FBc0IsT0FBdEIsQ0FBOEIsR0FBOUIsQ0FBZDtBQUNEOztBQUZJLGlCQUlBLElBQUcsRUFBRSxNQUFGLEtBQWEsU0FBaEIsRUFBMkI7QUFDOUIsOEJBQWMsRUFBRSxNQUFGLENBQVMsR0FBVCxDQUFkO0FBQ0QsZUFGSSxNQUdBO0FBQ0gsdUJBQU8sS0FBUCxDQUFhLE1BQU0sZUFBbkI7QUFDRDtBQUNELGdCQUFHLGVBQWUsV0FBZixJQUE4QixlQUFlLE1BQTdDLElBQXVELGdCQUFnQixTQUF2RSxJQUFvRixnQkFBZ0IsSUFBdkcsRUFBNkc7QUFDM0csNEJBQWMsU0FBZDtBQUNEO0FBQ0QsbUJBQU8sV0FBUDtBQUNELFdBM0NNO0FBNENQLGtCQUFRLFVBQVMsR0FBVCxFQUFjO0FBQ3BCLGdCQUNFLFVBQVUsT0FBTyxHQUFQLENBQVcsY0FBWCxFQURaO0FBR0EsZ0JBQUcsU0FBUyxhQUFULElBQTBCLGNBQTFCLElBQTRDLE9BQU8sWUFBUCxLQUF3QixTQUF2RSxFQUFrRjtBQUNoRixxQkFBTyxZQUFQLENBQW9CLFVBQXBCLENBQStCLEdBQS9CO0FBQ0QsYUFGRCxNQUdLLElBQUcsU0FBUyxhQUFULElBQTBCLGdCQUExQixJQUE4QyxPQUFPLGNBQVAsS0FBMEIsU0FBM0UsRUFBc0Y7QUFDekYscUJBQU8sY0FBUCxDQUFzQixVQUF0QixDQUFpQyxHQUFqQztBQUNEOztBQUZJLGlCQUlBLElBQUcsRUFBRSxNQUFGLEtBQWEsU0FBaEIsRUFBMkI7QUFDOUIsa0JBQUUsWUFBRixDQUFlLEdBQWYsRUFBb0IsT0FBcEI7QUFDRCxlQUZJLE1BR0E7QUFDSCx1QkFBTyxLQUFQLENBQWEsTUFBTSxTQUFuQjtBQUNEO0FBQ0Y7QUE3RE0sU0FuSEY7O0FBbUxQLGlCQUFTLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDN0IsaUJBQU8sS0FBUCxDQUFhLGtCQUFiLEVBQWlDLElBQWpDLEVBQXVDLEtBQXZDO0FBQ0EsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsUUFBZixFQUF5QixJQUF6QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixnQkFBRyxFQUFFLGFBQUYsQ0FBZ0IsU0FBUyxJQUFULENBQWhCLENBQUgsRUFBb0M7QUFDbEMsZ0JBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxTQUFTLElBQVQsQ0FBZixFQUErQixLQUEvQjtBQUNELGFBRkQsTUFHSztBQUNILHVCQUFTLElBQVQsSUFBaUIsS0FBakI7QUFDRDtBQUNGLFdBUEksTUFRQTtBQUNILG1CQUFPLFNBQVMsSUFBVCxDQUFQO0FBQ0Q7QUFDRixTQW5NTTtBQW9NUCxrQkFBVSxVQUFTLElBQVQsRUFBZSxLQUFmLEVBQXNCO0FBQzlCLGNBQUksRUFBRSxhQUFGLENBQWdCLElBQWhCLENBQUosRUFBNEI7QUFDMUIsY0FBRSxNQUFGLENBQVMsSUFBVCxFQUFlLE1BQWYsRUFBdUIsSUFBdkI7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsbUJBQU8sSUFBUCxJQUFlLEtBQWY7QUFDRCxXQUZJLE1BR0E7QUFDSCxtQkFBTyxPQUFPLElBQVAsQ0FBUDtBQUNEO0FBQ0YsU0E5TU07QUErTVAsZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQVYsSUFBb0IsU0FBUyxLQUFoQyxFQUF1QztBQUNyQyxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLEtBQVAsR0FBZSxTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxJQUFyQyxFQUEyQyxPQUEzQyxFQUFvRCxTQUFTLElBQVQsR0FBZ0IsR0FBcEUsQ0FBZjtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLE9BQW5CLEVBQTRCLFNBQTVCO0FBQ0Q7QUFDRjtBQUNGLFNBek5NO0FBME5QLGlCQUFTLFlBQVc7QUFDbEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLE9BQTdCLElBQXdDLFNBQVMsS0FBcEQsRUFBMkQ7QUFDekQsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxPQUFQLEdBQWlCLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFqQjtBQUNBLHFCQUFPLE9BQVAsQ0FBZSxLQUFmLENBQXFCLE9BQXJCLEVBQThCLFNBQTlCO0FBQ0Q7QUFDRjtBQUNGLFNBcE9NO0FBcU9QLGVBQU8sWUFBVztBQUNoQixjQUFHLENBQUMsU0FBUyxNQUFiLEVBQXFCO0FBQ25CLG1CQUFPLEtBQVAsR0FBZSxTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxLQUFyQyxFQUE0QyxPQUE1QyxFQUFxRCxTQUFTLElBQVQsR0FBZ0IsR0FBckUsQ0FBZjtBQUNBLG1CQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLE9BQW5CLEVBQTRCLFNBQTVCO0FBQ0Q7QUFDRixTQTFPTTtBQTJPUCxxQkFBYTtBQUNYLGVBQUssVUFBUyxPQUFULEVBQWtCO0FBQ3JCLGdCQUNFLFdBREYsRUFFRSxhQUZGLEVBR0UsWUFIRjtBQUtBLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2Qiw0QkFBZ0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUFoQjtBQUNBLDZCQUFnQixRQUFRLFdBQXhCO0FBQ0EsOEJBQWdCLGNBQWMsWUFBOUI7QUFDQSxxQkFBZ0IsV0FBaEI7QUFDQSwwQkFBWSxJQUFaLENBQWlCO0FBQ2Ysd0JBQW1CLFFBQVEsQ0FBUixDQURKO0FBRWYsNkJBQW1CLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxPQUFkLEVBQXVCLENBQXZCLEtBQTZCLEVBRmpDO0FBR2YsMkJBQW1CLE9BSEo7QUFJZixrQ0FBbUI7QUFKSixlQUFqQjtBQU1EO0FBQ0QseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsbUJBQU8sV0FBUCxDQUFtQixLQUFuQixHQUEyQixXQUFXLE9BQU8sV0FBUCxDQUFtQixPQUE5QixFQUF1QyxHQUF2QyxDQUEzQjtBQUNELFdBckJVO0FBc0JYLG1CQUFTLFlBQVc7QUFDbEIsZ0JBQ0UsUUFBUSxTQUFTLElBQVQsR0FBZ0IsR0FEMUI7QUFBQSxnQkFFRSxZQUFZLENBRmQ7QUFJQSxtQkFBTyxLQUFQO0FBQ0EseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsY0FBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMkJBQWEsS0FBSyxnQkFBTCxDQUFiO0FBQ0QsYUFGRDtBQUdBLHFCQUFTLE1BQU0sU0FBTixHQUFrQixJQUEzQjtBQUNBLGdCQUFHLGNBQUgsRUFBbUI7QUFDakIsdUJBQVMsUUFBUSxjQUFSLEdBQXlCLElBQWxDO0FBQ0Q7QUFDRCxnQkFBSSxDQUFDLFFBQVEsS0FBUixLQUFrQixTQUFsQixJQUErQixRQUFRLEtBQVIsS0FBa0IsU0FBbEQsS0FBZ0UsWUFBWSxNQUFaLEdBQXFCLENBQXpGLEVBQTRGO0FBQzFGLHNCQUFRLGNBQVIsQ0FBdUIsS0FBdkI7QUFDQSxrQkFBRyxRQUFRLEtBQVgsRUFBa0I7QUFDaEIsd0JBQVEsS0FBUixDQUFjLFdBQWQ7QUFDRCxlQUZELE1BR0s7QUFDSCxrQkFBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMEJBQVEsR0FBUixDQUFZLEtBQUssTUFBTCxJQUFlLElBQWYsR0FBc0IsS0FBSyxnQkFBTCxDQUF0QixHQUE2QyxJQUF6RDtBQUNELGlCQUZEO0FBR0Q7QUFDRCxzQkFBUSxRQUFSO0FBQ0Q7QUFDRCwwQkFBYyxFQUFkO0FBQ0Q7QUFqRFUsU0EzT047QUE4UlAsZ0JBQVEsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDLE9BQWpDLEVBQTBDO0FBQ2hELGNBQ0UsU0FBUyxRQURYO0FBQUEsY0FFRSxRQUZGO0FBQUEsY0FHRSxLQUhGO0FBQUEsY0FJRSxRQUpGO0FBTUEsNEJBQWtCLG1CQUFtQixjQUFyQztBQUNBLG9CQUFrQixXQUFtQixPQUFyQztBQUNBLGNBQUcsT0FBTyxLQUFQLElBQWdCLFFBQWhCLElBQTRCLFdBQVcsU0FBMUMsRUFBcUQ7QUFDbkQsb0JBQVcsTUFBTSxLQUFOLENBQVksT0FBWixDQUFYO0FBQ0EsdUJBQVcsTUFBTSxNQUFOLEdBQWUsQ0FBMUI7QUFDQSxjQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ25DLGtCQUFJLGlCQUFrQixTQUFTLFFBQVYsR0FDakIsUUFBUSxNQUFNLFFBQVEsQ0FBZCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixFQUEyQixXQUEzQixFQUFSLEdBQW1ELE1BQU0sUUFBUSxDQUFkLEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBRGxDLEdBRWpCLEtBRko7QUFJQSxrQkFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxjQUFQLENBQWpCLEtBQThDLFNBQVMsUUFBM0QsRUFBdUU7QUFDckUseUJBQVMsT0FBTyxjQUFQLENBQVQ7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLGNBQVAsTUFBMkIsU0FBL0IsRUFBMkM7QUFDOUMsd0JBQVEsT0FBTyxjQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBLElBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sS0FBUCxDQUFqQixLQUFxQyxTQUFTLFFBQWxELEVBQThEO0FBQ2pFLHlCQUFTLE9BQU8sS0FBUCxDQUFUO0FBQ0QsZUFGSSxNQUdBLElBQUksT0FBTyxLQUFQLE1BQWtCLFNBQXRCLEVBQWtDO0FBQ3JDLHdCQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQTtBQUNILHVCQUFPLEtBQVAsQ0FBYSxNQUFNLE1BQW5CLEVBQTJCLEtBQTNCO0FBQ0EsdUJBQU8sS0FBUDtBQUNEO0FBQ0YsYUF2QkQ7QUF3QkQ7QUFDRCxjQUFLLEVBQUUsVUFBRixDQUFjLEtBQWQsQ0FBTCxFQUE2QjtBQUMzQix1QkFBVyxNQUFNLEtBQU4sQ0FBWSxPQUFaLEVBQXFCLGVBQXJCLENBQVg7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsdUJBQVcsS0FBWDtBQUNEO0FBQ0QsY0FBRyxFQUFFLE9BQUYsQ0FBVSxhQUFWLENBQUgsRUFBNkI7QUFDM0IsMEJBQWMsSUFBZCxDQUFtQixRQUFuQjtBQUNELFdBRkQsTUFHSyxJQUFHLGtCQUFrQixTQUFyQixFQUFnQztBQUNuQyw0QkFBZ0IsQ0FBQyxhQUFELEVBQWdCLFFBQWhCLENBQWhCO0FBQ0QsV0FGSSxNQUdBLElBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUM5Qiw0QkFBZ0IsUUFBaEI7QUFDRDtBQUNELGlCQUFPLEtBQVA7QUFDRDtBQW5WTSxPQUFUOztBQXNWQSxVQUFHLGFBQUgsRUFBa0I7QUFDaEIsWUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLGlCQUFPLFVBQVA7QUFDRDtBQUNELGVBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDRCxPQUxELE1BTUs7QUFDSCxZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsbUJBQVMsTUFBVCxDQUFnQixTQUFoQjtBQUNEO0FBQ0QsZUFBTyxVQUFQO0FBQ0Q7QUFDRixLQTlZSDs7QUFpWkEsV0FBUSxrQkFBa0IsU0FBbkIsR0FDSCxhQURHLEdBRUgsSUFGSjtBQUlELEdBbGFEOztBQW9hQSxJQUFFLEVBQUYsQ0FBSyxHQUFMLENBQVMsUUFBVCxHQUFvQjs7QUFFbEIsVUFBYyxLQUZJOztBQUlsQixZQUFjLEtBSkk7QUFLbEIsV0FBYyxLQUxJO0FBTWxCLGFBQWMsS0FOSTtBQU9sQixpQkFBYyxJQVBJOztBQVNsQixlQUFjLEtBVEk7OztBQVlsQixhQUFjLEtBWkk7OztBQWVsQixpQkFBYyxDQWZJOztBQWlCbEIsZUFBYztBQUNaLFlBQU8sT0FESztBQUVaLFlBQU87QUFGSyxLQWpCSTs7QUFzQmxCLGFBQWdCLEtBdEJFO0FBdUJsQixnQkFBZ0IsS0F2QkU7O0FBeUJsQixhQUFnQixFQXpCRTtBQTBCbEIsWUFBZ0IsS0ExQkU7QUEyQmxCLFVBQWdCLEdBM0JFOzs7QUE4QmxCLG1CQUFnQixRQTlCRTs7O0FBaUNsQixTQUFnQixLQWpDRTtBQWtDbEIsV0FBZ0IsU0FsQ0U7O0FBb0NsQixXQUFPO0FBQ0wsdUJBQWtCLDJEQURiO0FBRUwsaUJBQWtCLHdGQUZiO0FBR0wsY0FBa0I7QUFIYixLQXBDVzs7QUEwQ2xCLGVBQWdCO0FBQ2QsY0FBUyxRQURLO0FBRWQsYUFBUztBQUZLLEtBMUNFOztBQStDbEIsY0FBZ0I7QUFDZCxhQUFRO0FBRE0sS0EvQ0U7O0FBbURsQixXQUFnQixHQW5ERTtBQW9EbEIsWUFBZ0IsYUFwREU7O0FBc0RsQixZQUFRLFlBQVcsQ0FBRTs7QUF0REgsR0FBcEI7OztBQTJEQSxJQUFFLE1BQUYsQ0FBVSxFQUFFLE1BQVosRUFBb0I7QUFDbEIsaUJBQWEsVUFBVSxDQUFWLEVBQWEsQ0FBYixFQUFnQixDQUFoQixFQUFtQixDQUFuQixFQUFzQixDQUF0QixFQUF5QjtBQUNwQyxhQUFPLENBQUMsQ0FBRCxJQUFLLEtBQUcsQ0FBUixLQUFZLElBQUUsQ0FBZCxJQUFtQixDQUExQjtBQUNEO0FBSGlCLEdBQXBCO0FBTUMsQ0FoZkEsRUFnZkcsTUFoZkgsRUFnZlcsTUFoZlgsRUFnZm1CLFFBaGZuQiIsImZpbGUiOiJuYWcuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcbiAqICMgU2VtYW50aWMgVUkgLSBOYWdcbiAqIGh0dHA6Ly9naXRodWIuY29tL3NlbWFudGljLW9yZy9zZW1hbnRpYy11aS9cbiAqXG4gKlxuICogUmVsZWFzZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlXG4gKiBodHRwOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvTUlUXG4gKlxuICovXG5cbjsoZnVuY3Rpb24gKCQsIHdpbmRvdywgZG9jdW1lbnQsIHVuZGVmaW5lZCkge1xuXG5cInVzZSBzdHJpY3RcIjtcblxud2luZG93ID0gKHR5cGVvZiB3aW5kb3cgIT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93Lk1hdGggPT0gTWF0aClcbiAgPyB3aW5kb3dcbiAgOiAodHlwZW9mIHNlbGYgIT0gJ3VuZGVmaW5lZCcgJiYgc2VsZi5NYXRoID09IE1hdGgpXG4gICAgPyBzZWxmXG4gICAgOiBGdW5jdGlvbigncmV0dXJuIHRoaXMnKSgpXG47XG5cbiQuZm4ubmFnID0gZnVuY3Rpb24ocGFyYW1ldGVycykge1xuICB2YXJcbiAgICAkYWxsTW9kdWxlcyAgICA9ICQodGhpcyksXG4gICAgbW9kdWxlU2VsZWN0b3IgPSAkYWxsTW9kdWxlcy5zZWxlY3RvciB8fCAnJyxcblxuICAgIHRpbWUgICAgICAgICAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCksXG4gICAgcGVyZm9ybWFuY2UgICAgPSBbXSxcblxuICAgIHF1ZXJ5ICAgICAgICAgID0gYXJndW1lbnRzWzBdLFxuICAgIG1ldGhvZEludm9rZWQgID0gKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyksXG4gICAgcXVlcnlBcmd1bWVudHMgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSksXG4gICAgcmV0dXJuZWRWYWx1ZVxuICA7XG4gICRhbGxNb2R1bGVzXG4gICAgLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICB2YXJcbiAgICAgICAgc2V0dGluZ3MgICAgICAgICAgPSAoICQuaXNQbGFpbk9iamVjdChwYXJhbWV0ZXJzKSApXG4gICAgICAgICAgPyAkLmV4dGVuZCh0cnVlLCB7fSwgJC5mbi5uYWcuc2V0dGluZ3MsIHBhcmFtZXRlcnMpXG4gICAgICAgICAgOiAkLmV4dGVuZCh7fSwgJC5mbi5uYWcuc2V0dGluZ3MpLFxuXG4gICAgICAgIGNsYXNzTmFtZSAgICAgICA9IHNldHRpbmdzLmNsYXNzTmFtZSxcbiAgICAgICAgc2VsZWN0b3IgICAgICAgID0gc2V0dGluZ3Muc2VsZWN0b3IsXG4gICAgICAgIGVycm9yICAgICAgICAgICA9IHNldHRpbmdzLmVycm9yLFxuICAgICAgICBuYW1lc3BhY2UgICAgICAgPSBzZXR0aW5ncy5uYW1lc3BhY2UsXG5cbiAgICAgICAgZXZlbnROYW1lc3BhY2UgID0gJy4nICsgbmFtZXNwYWNlLFxuICAgICAgICBtb2R1bGVOYW1lc3BhY2UgPSBuYW1lc3BhY2UgKyAnLW1vZHVsZScsXG5cbiAgICAgICAgJG1vZHVsZSAgICAgICAgID0gJCh0aGlzKSxcblxuICAgICAgICAkY2xvc2UgICAgICAgICAgPSAkbW9kdWxlLmZpbmQoc2VsZWN0b3IuY2xvc2UpLFxuICAgICAgICAkY29udGV4dCAgICAgICAgPSAoc2V0dGluZ3MuY29udGV4dClcbiAgICAgICAgICA/ICQoc2V0dGluZ3MuY29udGV4dClcbiAgICAgICAgICA6ICQoJ2JvZHknKSxcblxuICAgICAgICBlbGVtZW50ICAgICAgICAgPSB0aGlzLFxuICAgICAgICBpbnN0YW5jZSAgICAgICAgPSAkbW9kdWxlLmRhdGEobW9kdWxlTmFtZXNwYWNlKSxcblxuICAgICAgICBtb2R1bGVPZmZzZXQsXG4gICAgICAgIG1vZHVsZUhlaWdodCxcblxuICAgICAgICBjb250ZXh0V2lkdGgsXG4gICAgICAgIGNvbnRleHRIZWlnaHQsXG4gICAgICAgIGNvbnRleHRPZmZzZXQsXG5cbiAgICAgICAgeU9mZnNldCxcbiAgICAgICAgeVBvc2l0aW9uLFxuXG4gICAgICAgIHRpbWVyLFxuICAgICAgICBtb2R1bGUsXG5cbiAgICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lID0gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgICAgIHx8IHdpbmRvdy5tb3pSZXF1ZXN0QW5pbWF0aW9uRnJhbWVcbiAgICAgICAgICB8fCB3aW5kb3cud2Via2l0UmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gICAgICAgICAgfHwgd2luZG93Lm1zUmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gICAgICAgICAgfHwgZnVuY3Rpb24oY2FsbGJhY2spIHsgc2V0VGltZW91dChjYWxsYmFjaywgMCk7IH1cbiAgICAgIDtcbiAgICAgIG1vZHVsZSA9IHtcblxuICAgICAgICBpbml0aWFsaXplOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnSW5pdGlhbGl6aW5nIGVsZW1lbnQnKTtcblxuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5vbignY2xpY2snICsgZXZlbnROYW1lc3BhY2UsIHNlbGVjdG9yLmNsb3NlLCBtb2R1bGUuZGlzbWlzcylcbiAgICAgICAgICAgIC5kYXRhKG1vZHVsZU5hbWVzcGFjZSwgbW9kdWxlKVxuICAgICAgICAgIDtcblxuICAgICAgICAgIGlmKHNldHRpbmdzLmRldGFjaGFibGUgJiYgJG1vZHVsZS5wYXJlbnQoKVswXSAhPT0gJGNvbnRleHRbMF0pIHtcbiAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgLmRldGFjaCgpXG4gICAgICAgICAgICAgIC5wcmVwZW5kVG8oJGNvbnRleHQpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYoc2V0dGluZ3MuZGlzcGxheVRpbWUgPiAwKSB7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KG1vZHVsZS5oaWRlLCBzZXR0aW5ncy5kaXNwbGF5VGltZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIG1vZHVsZS5zaG93KCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVzdHJveTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0Rlc3Ryb3lpbmcgaW5zdGFuY2UnKTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAucmVtb3ZlRGF0YShtb2R1bGVOYW1lc3BhY2UpXG4gICAgICAgICAgICAub2ZmKGV2ZW50TmFtZXNwYWNlKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBzaG93OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZiggbW9kdWxlLnNob3VsZC5zaG93KCkgJiYgISRtb2R1bGUuaXMoJzp2aXNpYmxlJykgKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Nob3dpbmcgbmFnJywgc2V0dGluZ3MuYW5pbWF0aW9uLnNob3cpO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MuYW5pbWF0aW9uLnNob3cgPT0gJ2ZhZGUnKSB7XG4gICAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgICAuZmFkZUluKHNldHRpbmdzLmR1cmF0aW9uLCBzZXR0aW5ncy5lYXNpbmcpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgICAgLnNsaWRlRG93bihzZXR0aW5ncy5kdXJhdGlvbiwgc2V0dGluZ3MuZWFzaW5nKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGhpZGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2hvd2luZyBuYWcnLCBzZXR0aW5ncy5hbmltYXRpb24uaGlkZSk7XG4gICAgICAgICAgaWYoc2V0dGluZ3MuYW5pbWF0aW9uLnNob3cgPT0gJ2ZhZGUnKSB7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5mYWRlSW4oc2V0dGluZ3MuZHVyYXRpb24sIHNldHRpbmdzLmVhc2luZylcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5zbGlkZVVwKHNldHRpbmdzLmR1cmF0aW9uLCBzZXR0aW5ncy5lYXNpbmcpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIG9uSGlkZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZW1vdmluZyBuYWcnLCBzZXR0aW5ncy5hbmltYXRpb24uaGlkZSk7XG4gICAgICAgICAgJG1vZHVsZS5yZW1vdmUoKTtcbiAgICAgICAgICBpZiAoc2V0dGluZ3Mub25IaWRlKSB7XG4gICAgICAgICAgICBzZXR0aW5ncy5vbkhpZGUoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGlzbWlzczogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICBpZihzZXR0aW5ncy5zdG9yYWdlTWV0aG9kKSB7XG4gICAgICAgICAgICBtb2R1bGUuc3RvcmFnZS5zZXQoc2V0dGluZ3Mua2V5LCBzZXR0aW5ncy52YWx1ZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIG1vZHVsZS5oaWRlKCk7XG4gICAgICAgICAgZXZlbnQuc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgfSxcblxuICAgICAgICBzaG91bGQ6IHtcbiAgICAgICAgICBzaG93OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcnNpc3QpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdQZXJzaXN0ZW50IG5hZyBpcyBzZXQsIGNhbiBzaG93IG5hZycpO1xuICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCBtb2R1bGUuc3RvcmFnZS5nZXQoc2V0dGluZ3Mua2V5KSAhPSBzZXR0aW5ncy52YWx1ZS50b1N0cmluZygpICkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1N0b3JlZCB2YWx1ZSBpcyBub3Qgc2V0LCBjYW4gc2hvdyBuYWcnLCBtb2R1bGUuc3RvcmFnZS5nZXQoc2V0dGluZ3Mua2V5KSk7XG4gICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTdG9yZWQgdmFsdWUgaXMgc2V0LCBjYW5ub3Qgc2hvdyBuYWcnLCBtb2R1bGUuc3RvcmFnZS5nZXQoc2V0dGluZ3Mua2V5KSk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGdldDoge1xuICAgICAgICAgIHN0b3JhZ2VPcHRpb25zOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBvcHRpb25zID0ge31cbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmV4cGlyZXMpIHtcbiAgICAgICAgICAgICAgb3B0aW9ucy5leHBpcmVzID0gc2V0dGluZ3MuZXhwaXJlcztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmRvbWFpbikge1xuICAgICAgICAgICAgICBvcHRpb25zLmRvbWFpbiA9IHNldHRpbmdzLmRvbWFpbjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBhdGgpIHtcbiAgICAgICAgICAgICAgb3B0aW9ucy5wYXRoID0gc2V0dGluZ3MucGF0aDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBvcHRpb25zO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBjbGVhcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnN0b3JhZ2UucmVtb3ZlKHNldHRpbmdzLmtleSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc3RvcmFnZToge1xuICAgICAgICAgIHNldDogZnVuY3Rpb24oa2V5LCB2YWx1ZSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIG9wdGlvbnMgPSBtb2R1bGUuZ2V0LnN0b3JhZ2VPcHRpb25zKClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnN0b3JhZ2VNZXRob2QgPT0gJ2xvY2Fsc3RvcmFnZScgJiYgd2luZG93LmxvY2FsU3RvcmFnZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIHdpbmRvdy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbShrZXksIHZhbHVlKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdWYWx1ZSBzdG9yZWQgdXNpbmcgbG9jYWwgc3RvcmFnZScsIGtleSwgdmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZihzZXR0aW5ncy5zdG9yYWdlTWV0aG9kID09ICdzZXNzaW9uc3RvcmFnZScgJiYgd2luZG93LnNlc3Npb25TdG9yYWdlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgd2luZG93LnNlc3Npb25TdG9yYWdlLnNldEl0ZW0oa2V5LCB2YWx1ZSk7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVmFsdWUgc3RvcmVkIHVzaW5nIHNlc3Npb24gc3RvcmFnZScsIGtleSwgdmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZigkLmNvb2tpZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICQuY29va2llKGtleSwgdmFsdWUsIG9wdGlvbnMpO1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1ZhbHVlIHN0b3JlZCB1c2luZyBjb29raWUnLCBrZXksIHZhbHVlLCBvcHRpb25zKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3Iubm9Db29raWVTdG9yYWdlKTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgZ2V0OiBmdW5jdGlvbihrZXksIHZhbHVlKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgc3RvcmVkVmFsdWVcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnN0b3JhZ2VNZXRob2QgPT0gJ2xvY2Fsc3RvcmFnZScgJiYgd2luZG93LmxvY2FsU3RvcmFnZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIHN0b3JlZFZhbHVlID0gd2luZG93LmxvY2FsU3RvcmFnZS5nZXRJdGVtKGtleSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKHNldHRpbmdzLnN0b3JhZ2VNZXRob2QgPT0gJ3Nlc3Npb25zdG9yYWdlJyAmJiB3aW5kb3cuc2Vzc2lvblN0b3JhZ2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICBzdG9yZWRWYWx1ZSA9IHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGtleSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBnZXQgYnkgY29va2llXG4gICAgICAgICAgICBlbHNlIGlmKCQuY29va2llICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgc3RvcmVkVmFsdWUgPSAkLmNvb2tpZShrZXkpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5ub0Nvb2tpZVN0b3JhZ2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoc3RvcmVkVmFsdWUgPT0gJ3VuZGVmaW5lZCcgfHwgc3RvcmVkVmFsdWUgPT0gJ251bGwnIHx8IHN0b3JlZFZhbHVlID09PSB1bmRlZmluZWQgfHwgc3RvcmVkVmFsdWUgPT09IG51bGwpIHtcbiAgICAgICAgICAgICAgc3RvcmVkVmFsdWUgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gc3RvcmVkVmFsdWU7XG4gICAgICAgICAgfSxcbiAgICAgICAgICByZW1vdmU6IGZ1bmN0aW9uKGtleSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIG9wdGlvbnMgPSBtb2R1bGUuZ2V0LnN0b3JhZ2VPcHRpb25zKClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnN0b3JhZ2VNZXRob2QgPT0gJ2xvY2Fsc3RvcmFnZScgJiYgd2luZG93LmxvY2FsU3RvcmFnZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIHdpbmRvdy5sb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShrZXkpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZihzZXR0aW5ncy5zdG9yYWdlTWV0aG9kID09ICdzZXNzaW9uc3RvcmFnZScgJiYgd2luZG93LnNlc3Npb25TdG9yYWdlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgd2luZG93LnNlc3Npb25TdG9yYWdlLnJlbW92ZUl0ZW0oa2V5KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vIHN0b3JlIGJ5IGNvb2tpZVxuICAgICAgICAgICAgZWxzZSBpZigkLmNvb2tpZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICQucmVtb3ZlQ29va2llKGtleSwgb3B0aW9ucyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm5vU3RvcmFnZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldHRpbmc6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdDaGFuZ2luZyBzZXR0aW5nJywgbmFtZSwgdmFsdWUpO1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBzZXR0aW5ncywgbmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgaWYoJC5pc1BsYWluT2JqZWN0KHNldHRpbmdzW25hbWVdKSkge1xuICAgICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBzZXR0aW5nc1tuYW1lXSwgdmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHNldHRpbmdzW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHNldHRpbmdzW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgaW50ZXJuYWw6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChuYW1lKSApIHtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIG1vZHVsZSwgbmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgbW9kdWxlW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZVtuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGRlYnVnOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50ICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UubG9nKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmluZm8sIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHZlcmJvc2U6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQgJiYgc2V0dGluZ3MudmVyYm9zZSAmJiBzZXR0aW5ncy5kZWJ1Zykge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmluZm8sIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZS5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQpIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvciA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5lcnJvciwgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHBlcmZvcm1hbmNlOiB7XG4gICAgICAgICAgbG9nOiBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgY3VycmVudFRpbWUsXG4gICAgICAgICAgICAgIGV4ZWN1dGlvblRpbWUsXG4gICAgICAgICAgICAgIHByZXZpb3VzVGltZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgY3VycmVudFRpbWUgICA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgICAgICAgICAgICBwcmV2aW91c1RpbWUgID0gdGltZSB8fCBjdXJyZW50VGltZTtcbiAgICAgICAgICAgICAgZXhlY3V0aW9uVGltZSA9IGN1cnJlbnRUaW1lIC0gcHJldmlvdXNUaW1lO1xuICAgICAgICAgICAgICB0aW1lICAgICAgICAgID0gY3VycmVudFRpbWU7XG4gICAgICAgICAgICAgIHBlcmZvcm1hbmNlLnB1c2goe1xuICAgICAgICAgICAgICAgICdOYW1lJyAgICAgICAgICAgOiBtZXNzYWdlWzBdLFxuICAgICAgICAgICAgICAgICdBcmd1bWVudHMnICAgICAgOiBbXS5zbGljZS5jYWxsKG1lc3NhZ2UsIDEpIHx8ICcnLFxuICAgICAgICAgICAgICAgICdFbGVtZW50JyAgICAgICAgOiBlbGVtZW50LFxuICAgICAgICAgICAgICAgICdFeGVjdXRpb24gVGltZScgOiBleGVjdXRpb25UaW1lXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lcik7XG4gICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UudGltZXIgPSBzZXRUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS5kaXNwbGF5LCA1MDApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZGlzcGxheTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgdGl0bGUgPSBzZXR0aW5ncy5uYW1lICsgJzonLFxuICAgICAgICAgICAgICB0b3RhbFRpbWUgPSAwXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICB0aW1lID0gZmFsc2U7XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgdG90YWxUaW1lICs9IGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ107XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHRpdGxlICs9ICcgJyArIHRvdGFsVGltZSArICdtcyc7XG4gICAgICAgICAgICBpZihtb2R1bGVTZWxlY3Rvcikge1xuICAgICAgICAgICAgICB0aXRsZSArPSAnIFxcJycgKyBtb2R1bGVTZWxlY3RvciArICdcXCcnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoIChjb25zb2xlLmdyb3VwICE9PSB1bmRlZmluZWQgfHwgY29uc29sZS50YWJsZSAhPT0gdW5kZWZpbmVkKSAmJiBwZXJmb3JtYW5jZS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBDb2xsYXBzZWQodGl0bGUpO1xuICAgICAgICAgICAgICBpZihjb25zb2xlLnRhYmxlKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS50YWJsZShwZXJmb3JtYW5jZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YVsnTmFtZSddICsgJzogJyArIGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ10rJ21zJyk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgY29uc29sZS5ncm91cEVuZCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcGVyZm9ybWFuY2UgPSBbXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludm9rZTogZnVuY3Rpb24ocXVlcnksIHBhc3NlZEFyZ3VtZW50cywgY29udGV4dCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgb2JqZWN0ID0gaW5zdGFuY2UsXG4gICAgICAgICAgICBtYXhEZXB0aCxcbiAgICAgICAgICAgIGZvdW5kLFxuICAgICAgICAgICAgcmVzcG9uc2VcbiAgICAgICAgICA7XG4gICAgICAgICAgcGFzc2VkQXJndW1lbnRzID0gcGFzc2VkQXJndW1lbnRzIHx8IHF1ZXJ5QXJndW1lbnRzO1xuICAgICAgICAgIGNvbnRleHQgICAgICAgICA9IGVsZW1lbnQgICAgICAgICB8fCBjb250ZXh0O1xuICAgICAgICAgIGlmKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyAmJiBvYmplY3QgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnkgICAgPSBxdWVyeS5zcGxpdCgvW1xcLiBdLyk7XG4gICAgICAgICAgICBtYXhEZXB0aCA9IHF1ZXJ5Lmxlbmd0aCAtIDE7XG4gICAgICAgICAgICAkLmVhY2gocXVlcnksIGZ1bmN0aW9uKGRlcHRoLCB2YWx1ZSkge1xuICAgICAgICAgICAgICB2YXIgY2FtZWxDYXNlVmFsdWUgPSAoZGVwdGggIT0gbWF4RGVwdGgpXG4gICAgICAgICAgICAgICAgPyB2YWx1ZSArIHF1ZXJ5W2RlcHRoICsgMV0uY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBxdWVyeVtkZXB0aCArIDFdLnNsaWNlKDEpXG4gICAgICAgICAgICAgICAgOiBxdWVyeVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gKSAmJiAoZGVwdGggIT0gbWF4RGVwdGgpICkge1xuICAgICAgICAgICAgICAgIG9iamVjdCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgICAgIGZvdW5kID0gb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbdmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFt2YWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFt2YWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5tZXRob2QsIHF1ZXJ5KTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoICQuaXNGdW5jdGlvbiggZm91bmQgKSApIHtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gZm91bmQuYXBwbHkoY29udGV4dCwgcGFzc2VkQXJndW1lbnRzKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihmb3VuZCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXNwb25zZSA9IGZvdW5kO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZigkLmlzQXJyYXkocmV0dXJuZWRWYWx1ZSkpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUucHVzaChyZXNwb25zZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYocmV0dXJuZWRWYWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlID0gW3JldHVybmVkVmFsdWUsIHJlc3BvbnNlXTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihyZXNwb25zZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlID0gcmVzcG9uc2U7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBmb3VuZDtcbiAgICAgICAgfVxuICAgICAgfTtcblxuICAgICAgaWYobWV0aG9kSW52b2tlZCkge1xuICAgICAgICBpZihpbnN0YW5jZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgbW9kdWxlLmluaXRpYWxpemUoKTtcbiAgICAgICAgfVxuICAgICAgICBtb2R1bGUuaW52b2tlKHF1ZXJ5KTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBpZihpbnN0YW5jZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgaW5zdGFuY2UuaW52b2tlKCdkZXN0cm95Jyk7XG4gICAgICAgIH1cbiAgICAgICAgbW9kdWxlLmluaXRpYWxpemUoKTtcbiAgICAgIH1cbiAgICB9KVxuICA7XG5cbiAgcmV0dXJuIChyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpXG4gICAgPyByZXR1cm5lZFZhbHVlXG4gICAgOiB0aGlzXG4gIDtcbn07XG5cbiQuZm4ubmFnLnNldHRpbmdzID0ge1xuXG4gIG5hbWUgICAgICAgIDogJ05hZycsXG5cbiAgc2lsZW50ICAgICAgOiBmYWxzZSxcbiAgZGVidWcgICAgICAgOiBmYWxzZSxcbiAgdmVyYm9zZSAgICAgOiBmYWxzZSxcbiAgcGVyZm9ybWFuY2UgOiB0cnVlLFxuXG4gIG5hbWVzcGFjZSAgIDogJ05hZycsXG5cbiAgLy8gYWxsb3dzIGNvb2tpZSB0byBiZSBvdmVycmlkZGVuXG4gIHBlcnNpc3QgICAgIDogZmFsc2UsXG5cbiAgLy8gc2V0IHRvIHplcm8gdG8gcmVxdWlyZSBtYW51YWxseSBkaXNtaXNzYWwsIG90aGVyd2lzZSBoaWRlcyBvbiBpdHMgb3duXG4gIGRpc3BsYXlUaW1lIDogMCxcblxuICBhbmltYXRpb24gICA6IHtcbiAgICBzaG93IDogJ3NsaWRlJyxcbiAgICBoaWRlIDogJ3NsaWRlJ1xuICB9LFxuXG4gIGNvbnRleHQgICAgICAgOiBmYWxzZSxcbiAgZGV0YWNoYWJsZSAgICA6IGZhbHNlLFxuXG4gIGV4cGlyZXMgICAgICAgOiAzMCxcbiAgZG9tYWluICAgICAgICA6IGZhbHNlLFxuICBwYXRoICAgICAgICAgIDogJy8nLFxuXG4gIC8vIHR5cGUgb2Ygc3RvcmFnZSB0byB1c2VcbiAgc3RvcmFnZU1ldGhvZCA6ICdjb29raWUnLFxuXG4gIC8vIHZhbHVlIHRvIHN0b3JlIGluIGRpc21pc3NlZCBsb2NhbHN0b3JhZ2UvY29va2llXG4gIGtleSAgICAgICAgICAgOiAnbmFnJyxcbiAgdmFsdWUgICAgICAgICA6ICdkaXNtaXNzJyxcblxuICBlcnJvcjoge1xuICAgIG5vQ29va2llU3RvcmFnZSA6ICckLmNvb2tpZSBpcyBub3QgaW5jbHVkZWQuIEEgc3RvcmFnZSBzb2x1dGlvbiBpcyByZXF1aXJlZC4nLFxuICAgIG5vU3RvcmFnZSAgICAgICA6ICdOZWl0aGVyICQuY29va2llIG9yIHN0b3JlIGlzIGRlZmluZWQuIEEgc3RvcmFnZSBzb2x1dGlvbiBpcyByZXF1aXJlZCBmb3Igc3RvcmluZyBzdGF0ZScsXG4gICAgbWV0aG9kICAgICAgICAgIDogJ1RoZSBtZXRob2QgeW91IGNhbGxlZCBpcyBub3QgZGVmaW5lZC4nXG4gIH0sXG5cbiAgY2xhc3NOYW1lICAgICA6IHtcbiAgICBib3R0b20gOiAnYm90dG9tJyxcbiAgICBmaXhlZCAgOiAnZml4ZWQnXG4gIH0sXG5cbiAgc2VsZWN0b3IgICAgICA6IHtcbiAgICBjbG9zZSA6ICcuY2xvc2UuaWNvbidcbiAgfSxcblxuICBzcGVlZCAgICAgICAgIDogNTAwLFxuICBlYXNpbmcgICAgICAgIDogJ2Vhc2VPdXRRdWFkJyxcblxuICBvbkhpZGU6IGZ1bmN0aW9uKCkge31cblxufTtcblxuLy8gQWRkcyBlYXNpbmdcbiQuZXh0ZW5kKCAkLmVhc2luZywge1xuICBlYXNlT3V0UXVhZDogZnVuY3Rpb24gKHgsIHQsIGIsIGMsIGQpIHtcbiAgICByZXR1cm4gLWMgKih0Lz1kKSoodC0yKSArIGI7XG4gIH1cbn0pO1xuXG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7XG4iXX0=