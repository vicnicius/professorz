/*!
 * # Semantic UI - Shape
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.shape = function (parameters) {
    var $allModules = $(this),
        $body = $('body'),
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
      setTimeout(callback, 0);
    },
        returnedValue;

    $allModules.each(function () {
      var moduleSelector = $allModules.selector || '',
          settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.shape.settings, parameters) : $.extend({}, $.fn.shape.settings),


      // internal aliases
      namespace = settings.namespace,
          selector = settings.selector,
          error = settings.error,
          className = settings.className,


      // define namespaces for modules
      eventNamespace = '.' + namespace,
          moduleNamespace = 'module-' + namespace,


      // selector cache
      $module = $(this),
          $sides = $module.find(selector.sides),
          $side = $module.find(selector.side),


      // private variables
      nextIndex = false,
          $activeSide,
          $nextSide,


      // standard module
      element = this,
          instance = $module.data(moduleNamespace),
          module;

      module = {

        initialize: function () {
          module.verbose('Initializing module for', element);
          module.set.defaultSide();
          module.instantiate();
        },

        instantiate: function () {
          module.verbose('Storing instance of module', module);
          instance = module;
          $module.data(moduleNamespace, instance);
        },

        destroy: function () {
          module.verbose('Destroying previous module for', element);
          $module.removeData(moduleNamespace).off(eventNamespace);
        },

        refresh: function () {
          module.verbose('Refreshing selector cache for', element);
          $module = $(element);
          $sides = $(this).find(selector.shape);
          $side = $(this).find(selector.side);
        },

        repaint: function () {
          module.verbose('Forcing repaint event');
          var shape = $sides[0] || document.createElement('div'),
              fakeAssignment = shape.offsetWidth;
        },

        animate: function (propertyObject, callback) {
          module.verbose('Animating box with properties', propertyObject);
          callback = callback || function (event) {
            module.verbose('Executing animation callback');
            if (event !== undefined) {
              event.stopPropagation();
            }
            module.reset();
            module.set.active();
          };
          settings.beforeChange.call($nextSide[0]);
          if (module.get.transitionEvent()) {
            module.verbose('Starting CSS animation');
            $module.addClass(className.animating);
            $sides.css(propertyObject).one(module.get.transitionEvent(), callback);
            module.set.duration(settings.duration);
            requestAnimationFrame(function () {
              $module.addClass(className.animating);
              $activeSide.addClass(className.hidden);
            });
          } else {
            callback();
          }
        },

        queue: function (method) {
          module.debug('Queueing animation of', method);
          $sides.one(module.get.transitionEvent(), function () {
            module.debug('Executing queued animation');
            setTimeout(function () {
              $module.shape(method);
            }, 0);
          });
        },

        reset: function () {
          module.verbose('Animating states reset');
          $module.removeClass(className.animating).attr('style', '').removeAttr('style');
          // removeAttr style does not consistently work in safari
          $sides.attr('style', '').removeAttr('style');
          $side.attr('style', '').removeAttr('style').removeClass(className.hidden);
          $nextSide.removeClass(className.animating).attr('style', '').removeAttr('style');
        },

        is: {
          complete: function () {
            return $side.filter('.' + className.active)[0] == $nextSide[0];
          },
          animating: function () {
            return $module.hasClass(className.animating);
          }
        },

        set: {

          defaultSide: function () {
            $activeSide = $module.find('.' + settings.className.active);
            $nextSide = $activeSide.next(selector.side).length > 0 ? $activeSide.next(selector.side) : $module.find(selector.side).first();
            nextIndex = false;
            module.verbose('Active side set to', $activeSide);
            module.verbose('Next side set to', $nextSide);
          },

          duration: function (duration) {
            duration = duration || settings.duration;
            duration = typeof duration == 'number' ? duration + 'ms' : duration;
            module.verbose('Setting animation duration', duration);
            if (settings.duration || settings.duration === 0) {
              $sides.add($side).css({
                '-webkit-transition-duration': duration,
                '-moz-transition-duration': duration,
                '-ms-transition-duration': duration,
                '-o-transition-duration': duration,
                'transition-duration': duration
              });
            }
          },

          currentStageSize: function () {
            var $activeSide = $module.find('.' + settings.className.active),
                width = $activeSide.outerWidth(true),
                height = $activeSide.outerHeight(true);
            $module.css({
              width: width,
              height: height
            });
          },

          stageSize: function () {
            var $clone = $module.clone().addClass(className.loading),
                $activeSide = $clone.find('.' + settings.className.active),
                $nextSide = nextIndex ? $clone.find(selector.side).eq(nextIndex) : $activeSide.next(selector.side).length > 0 ? $activeSide.next(selector.side) : $clone.find(selector.side).first(),
                newWidth = settings.width == 'next' ? $nextSide.outerWidth(true) : settings.width == 'initial' ? $module.width() : settings.width,
                newHeight = settings.height == 'next' ? $nextSide.outerHeight(true) : settings.height == 'initial' ? $module.height() : settings.height;
            $activeSide.removeClass(className.active);
            $nextSide.addClass(className.active);
            $clone.insertAfter($module);
            $clone.remove();
            if (settings.width != 'auto') {
              $module.css('width', newWidth + settings.jitter);
              module.verbose('Specifying width during animation', newWidth);
            }
            if (settings.height != 'auto') {
              $module.css('height', newHeight + settings.jitter);
              module.verbose('Specifying height during animation', newHeight);
            }
          },

          nextSide: function (selector) {
            nextIndex = selector;
            $nextSide = $side.filter(selector);
            nextIndex = $side.index($nextSide);
            if ($nextSide.length === 0) {
              module.set.defaultSide();
              module.error(error.side);
            }
            module.verbose('Next side manually set to', $nextSide);
          },

          active: function () {
            module.verbose('Setting new side to active', $nextSide);
            $side.removeClass(className.active);
            $nextSide.addClass(className.active);
            settings.onChange.call($nextSide[0]);
            module.set.defaultSide();
          }
        },

        flip: {

          up: function () {
            if (module.is.complete() && !module.is.animating() && !settings.allowRepeats) {
              module.debug('Side already visible', $nextSide);
              return;
            }
            if (!module.is.animating()) {
              module.debug('Flipping up', $nextSide);
              var transform = module.get.transform.up();
              module.set.stageSize();
              module.stage.above();
              module.animate(transform);
            } else {
              module.queue('flip up');
            }
          },

          down: function () {
            if (module.is.complete() && !module.is.animating() && !settings.allowRepeats) {
              module.debug('Side already visible', $nextSide);
              return;
            }
            if (!module.is.animating()) {
              module.debug('Flipping down', $nextSide);
              var transform = module.get.transform.down();
              module.set.stageSize();
              module.stage.below();
              module.animate(transform);
            } else {
              module.queue('flip down');
            }
          },

          left: function () {
            if (module.is.complete() && !module.is.animating() && !settings.allowRepeats) {
              module.debug('Side already visible', $nextSide);
              return;
            }
            if (!module.is.animating()) {
              module.debug('Flipping left', $nextSide);
              var transform = module.get.transform.left();
              module.set.stageSize();
              module.stage.left();
              module.animate(transform);
            } else {
              module.queue('flip left');
            }
          },

          right: function () {
            if (module.is.complete() && !module.is.animating() && !settings.allowRepeats) {
              module.debug('Side already visible', $nextSide);
              return;
            }
            if (!module.is.animating()) {
              module.debug('Flipping right', $nextSide);
              var transform = module.get.transform.right();
              module.set.stageSize();
              module.stage.right();
              module.animate(transform);
            } else {
              module.queue('flip right');
            }
          },

          over: function () {
            if (module.is.complete() && !module.is.animating() && !settings.allowRepeats) {
              module.debug('Side already visible', $nextSide);
              return;
            }
            if (!module.is.animating()) {
              module.debug('Flipping over', $nextSide);
              module.set.stageSize();
              module.stage.behind();
              module.animate(module.get.transform.over());
            } else {
              module.queue('flip over');
            }
          },

          back: function () {
            if (module.is.complete() && !module.is.animating() && !settings.allowRepeats) {
              module.debug('Side already visible', $nextSide);
              return;
            }
            if (!module.is.animating()) {
              module.debug('Flipping back', $nextSide);
              module.set.stageSize();
              module.stage.behind();
              module.animate(module.get.transform.back());
            } else {
              module.queue('flip back');
            }
          }

        },

        get: {

          transform: {
            up: function () {
              var translate = {
                y: -(($activeSide.outerHeight(true) - $nextSide.outerHeight(true)) / 2),
                z: -($activeSide.outerHeight(true) / 2)
              };
              return {
                transform: 'translateY(' + translate.y + 'px) translateZ(' + translate.z + 'px) rotateX(-90deg)'
              };
            },

            down: function () {
              var translate = {
                y: -(($activeSide.outerHeight(true) - $nextSide.outerHeight(true)) / 2),
                z: -($activeSide.outerHeight(true) / 2)
              };
              return {
                transform: 'translateY(' + translate.y + 'px) translateZ(' + translate.z + 'px) rotateX(90deg)'
              };
            },

            left: function () {
              var translate = {
                x: -(($activeSide.outerWidth(true) - $nextSide.outerWidth(true)) / 2),
                z: -($activeSide.outerWidth(true) / 2)
              };
              return {
                transform: 'translateX(' + translate.x + 'px) translateZ(' + translate.z + 'px) rotateY(90deg)'
              };
            },

            right: function () {
              var translate = {
                x: -(($activeSide.outerWidth(true) - $nextSide.outerWidth(true)) / 2),
                z: -($activeSide.outerWidth(true) / 2)
              };
              return {
                transform: 'translateX(' + translate.x + 'px) translateZ(' + translate.z + 'px) rotateY(-90deg)'
              };
            },

            over: function () {
              var translate = {
                x: -(($activeSide.outerWidth(true) - $nextSide.outerWidth(true)) / 2)
              };
              return {
                transform: 'translateX(' + translate.x + 'px) rotateY(180deg)'
              };
            },

            back: function () {
              var translate = {
                x: -(($activeSide.outerWidth(true) - $nextSide.outerWidth(true)) / 2)
              };
              return {
                transform: 'translateX(' + translate.x + 'px) rotateY(-180deg)'
              };
            }
          },

          transitionEvent: function () {
            var element = document.createElement('element'),
                transitions = {
              'transition': 'transitionend',
              'OTransition': 'oTransitionEnd',
              'MozTransition': 'transitionend',
              'WebkitTransition': 'webkitTransitionEnd'
            },
                transition;
            for (transition in transitions) {
              if (element.style[transition] !== undefined) {
                return transitions[transition];
              }
            }
          },

          nextSide: function () {
            return $activeSide.next(selector.side).length > 0 ? $activeSide.next(selector.side) : $module.find(selector.side).first();
          }

        },

        stage: {

          above: function () {
            var box = {
              origin: ($activeSide.outerHeight(true) - $nextSide.outerHeight(true)) / 2,
              depth: {
                active: $nextSide.outerHeight(true) / 2,
                next: $activeSide.outerHeight(true) / 2
              }
            };
            module.verbose('Setting the initial animation position as above', $nextSide, box);
            $sides.css({
              'transform': 'translateZ(-' + box.depth.active + 'px)'
            });
            $activeSide.css({
              'transform': 'rotateY(0deg) translateZ(' + box.depth.active + 'px)'
            });
            $nextSide.addClass(className.animating).css({
              'top': box.origin + 'px',
              'transform': 'rotateX(90deg) translateZ(' + box.depth.next + 'px)'
            });
          },

          below: function () {
            var box = {
              origin: ($activeSide.outerHeight(true) - $nextSide.outerHeight(true)) / 2,
              depth: {
                active: $nextSide.outerHeight(true) / 2,
                next: $activeSide.outerHeight(true) / 2
              }
            };
            module.verbose('Setting the initial animation position as below', $nextSide, box);
            $sides.css({
              'transform': 'translateZ(-' + box.depth.active + 'px)'
            });
            $activeSide.css({
              'transform': 'rotateY(0deg) translateZ(' + box.depth.active + 'px)'
            });
            $nextSide.addClass(className.animating).css({
              'top': box.origin + 'px',
              'transform': 'rotateX(-90deg) translateZ(' + box.depth.next + 'px)'
            });
          },

          left: function () {
            var height = {
              active: $activeSide.outerWidth(true),
              next: $nextSide.outerWidth(true)
            },
                box = {
              origin: (height.active - height.next) / 2,
              depth: {
                active: height.next / 2,
                next: height.active / 2
              }
            };
            module.verbose('Setting the initial animation position as left', $nextSide, box);
            $sides.css({
              'transform': 'translateZ(-' + box.depth.active + 'px)'
            });
            $activeSide.css({
              'transform': 'rotateY(0deg) translateZ(' + box.depth.active + 'px)'
            });
            $nextSide.addClass(className.animating).css({
              'left': box.origin + 'px',
              'transform': 'rotateY(-90deg) translateZ(' + box.depth.next + 'px)'
            });
          },

          right: function () {
            var height = {
              active: $activeSide.outerWidth(true),
              next: $nextSide.outerWidth(true)
            },
                box = {
              origin: (height.active - height.next) / 2,
              depth: {
                active: height.next / 2,
                next: height.active / 2
              }
            };
            module.verbose('Setting the initial animation position as left', $nextSide, box);
            $sides.css({
              'transform': 'translateZ(-' + box.depth.active + 'px)'
            });
            $activeSide.css({
              'transform': 'rotateY(0deg) translateZ(' + box.depth.active + 'px)'
            });
            $nextSide.addClass(className.animating).css({
              'left': box.origin + 'px',
              'transform': 'rotateY(90deg) translateZ(' + box.depth.next + 'px)'
            });
          },

          behind: function () {
            var height = {
              active: $activeSide.outerWidth(true),
              next: $nextSide.outerWidth(true)
            },
                box = {
              origin: (height.active - height.next) / 2,
              depth: {
                active: height.next / 2,
                next: height.active / 2
              }
            };
            module.verbose('Setting the initial animation position as behind', $nextSide, box);
            $activeSide.css({
              'transform': 'rotateY(0deg)'
            });
            $nextSide.addClass(className.animating).css({
              'left': box.origin + 'px',
              'transform': 'rotateY(-180deg)'
            });
          }
        },
        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ($allModules.length > 1) {
              title += ' ' + '(' + $allModules.length + ')';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };

      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.shape.settings = {

    // module info
    name: 'Shape',

    // hide all debug content
    silent: false,

    // debug content outputted to console
    debug: false,

    // verbose debug output
    verbose: false,

    // fudge factor in pixels when swapping from 2d to 3d (can be useful to correct rounding errors)
    jitter: 0,

    // performance data output
    performance: true,

    // event namespace
    namespace: 'shape',

    // width during animation, can be set to 'auto', initial', 'next' or pixel amount
    width: 'initial',

    // height during animation, can be set to 'auto', 'initial', 'next' or pixel amount
    height: 'initial',

    // callback occurs on side change
    beforeChange: function () {},
    onChange: function () {},

    // allow animation to same side
    allowRepeats: false,

    // animation duration
    duration: false,

    // possible errors
    error: {
      side: 'You tried to switch to a side that does not exist.',
      method: 'The method you called is not defined'
    },

    // classnames used
    className: {
      animating: 'animating',
      hidden: 'hidden',
      loading: 'loading',
      active: 'active'
    },

    // selectors used
    selector: {
      sides: '.sides',
      side: '.side'
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL3NoYXBlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFVQSxDQUFDLENBQUMsVUFBVSxDQUFWLEVBQWEsTUFBYixFQUFxQixRQUFyQixFQUErQixTQUEvQixFQUEwQzs7QUFFNUM7O0FBRUEsV0FBVSxPQUFPLE1BQVAsSUFBaUIsV0FBakIsSUFBZ0MsT0FBTyxJQUFQLElBQWUsSUFBaEQsR0FDTCxNQURLLEdBRUosT0FBTyxJQUFQLElBQWUsV0FBZixJQUE4QixLQUFLLElBQUwsSUFBYSxJQUE1QyxHQUNFLElBREYsR0FFRSxTQUFTLGFBQVQsR0FKTjs7QUFPQSxJQUFFLEVBQUYsQ0FBSyxLQUFMLEdBQWEsVUFBUyxVQUFULEVBQXFCO0FBQ2hDLFFBQ0UsY0FBa0IsRUFBRSxJQUFGLENBRHBCO0FBQUEsUUFFRSxRQUFrQixFQUFFLE1BQUYsQ0FGcEI7QUFBQSxRQUlFLE9BQWtCLElBQUksSUFBSixHQUFXLE9BQVgsRUFKcEI7QUFBQSxRQUtFLGNBQWtCLEVBTHBCO0FBQUEsUUFPRSxRQUFrQixVQUFVLENBQVYsQ0FQcEI7QUFBQSxRQVFFLGdCQUFtQixPQUFPLEtBQVAsSUFBZ0IsUUFSckM7QUFBQSxRQVNFLGlCQUFrQixHQUFHLEtBQUgsQ0FBUyxJQUFULENBQWMsU0FBZCxFQUF5QixDQUF6QixDQVRwQjtBQUFBLFFBV0Usd0JBQXdCLE9BQU8scUJBQVAsSUFDbkIsT0FBTyx3QkFEWSxJQUVuQixPQUFPLDJCQUZZLElBR25CLE9BQU8sdUJBSFksSUFJbkIsVUFBUyxRQUFULEVBQW1CO0FBQUUsaUJBQVcsUUFBWCxFQUFxQixDQUFyQjtBQUEwQixLQWZ0RDtBQUFBLFFBaUJFLGFBakJGOztBQW9CQSxnQkFDRyxJQURILENBQ1EsWUFBVztBQUNmLFVBQ0UsaUJBQWlCLFlBQVksUUFBWixJQUF3QixFQUQzQztBQUFBLFVBRUUsV0FBbUIsRUFBRSxhQUFGLENBQWdCLFVBQWhCLENBQUYsR0FDYixFQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQixFQUFFLEVBQUYsQ0FBSyxLQUFMLENBQVcsUUFBOUIsRUFBd0MsVUFBeEMsQ0FEYSxHQUViLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxFQUFFLEVBQUYsQ0FBSyxLQUFMLENBQVcsUUFBeEIsQ0FKTjtBQUFBOzs7QUFPRSxrQkFBZ0IsU0FBUyxTQVAzQjtBQUFBLFVBUUUsV0FBZ0IsU0FBUyxRQVIzQjtBQUFBLFVBU0UsUUFBZ0IsU0FBUyxLQVQzQjtBQUFBLFVBVUUsWUFBZ0IsU0FBUyxTQVYzQjtBQUFBOzs7QUFhRSx1QkFBa0IsTUFBTSxTQWIxQjtBQUFBLFVBY0Usa0JBQWtCLFlBQVksU0FkaEM7QUFBQTs7O0FBaUJFLGdCQUFnQixFQUFFLElBQUYsQ0FqQmxCO0FBQUEsVUFrQkUsU0FBZ0IsUUFBUSxJQUFSLENBQWEsU0FBUyxLQUF0QixDQWxCbEI7QUFBQSxVQW1CRSxRQUFnQixRQUFRLElBQVIsQ0FBYSxTQUFTLElBQXRCLENBbkJsQjtBQUFBOzs7QUFzQkUsa0JBQVksS0F0QmQ7QUFBQSxVQXVCRSxXQXZCRjtBQUFBLFVBd0JFLFNBeEJGO0FBQUE7OztBQTJCRSxnQkFBZ0IsSUEzQmxCO0FBQUEsVUE0QkUsV0FBZ0IsUUFBUSxJQUFSLENBQWEsZUFBYixDQTVCbEI7QUFBQSxVQTZCRSxNQTdCRjs7QUFnQ0EsZUFBUzs7QUFFUCxvQkFBWSxZQUFXO0FBQ3JCLGlCQUFPLE9BQVAsQ0FBZSx5QkFBZixFQUEwQyxPQUExQztBQUNBLGlCQUFPLEdBQVAsQ0FBVyxXQUFYO0FBQ0EsaUJBQU8sV0FBUDtBQUNELFNBTk07O0FBUVAscUJBQWEsWUFBVztBQUN0QixpQkFBTyxPQUFQLENBQWUsNEJBQWYsRUFBNkMsTUFBN0M7QUFDQSxxQkFBVyxNQUFYO0FBQ0Esa0JBQ0csSUFESCxDQUNRLGVBRFIsRUFDeUIsUUFEekI7QUFHRCxTQWRNOztBQWdCUCxpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLE9BQVAsQ0FBZSxnQ0FBZixFQUFpRCxPQUFqRDtBQUNBLGtCQUNHLFVBREgsQ0FDYyxlQURkLEVBRUcsR0FGSCxDQUVPLGNBRlA7QUFJRCxTQXRCTTs7QUF3QlAsaUJBQVMsWUFBVztBQUNsQixpQkFBTyxPQUFQLENBQWUsK0JBQWYsRUFBZ0QsT0FBaEQ7QUFDQSxvQkFBVSxFQUFFLE9BQUYsQ0FBVjtBQUNBLG1CQUFVLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxTQUFTLEtBQXRCLENBQVY7QUFDQSxrQkFBVSxFQUFFLElBQUYsRUFBUSxJQUFSLENBQWEsU0FBUyxJQUF0QixDQUFWO0FBQ0QsU0E3Qk07O0FBK0JQLGlCQUFTLFlBQVc7QUFDbEIsaUJBQU8sT0FBUCxDQUFlLHVCQUFmO0FBQ0EsY0FDRSxRQUFpQixPQUFPLENBQVAsS0FBYSxTQUFTLGFBQVQsQ0FBdUIsS0FBdkIsQ0FEaEM7QUFBQSxjQUVFLGlCQUFpQixNQUFNLFdBRnpCO0FBSUQsU0FyQ007O0FBdUNQLGlCQUFTLFVBQVMsY0FBVCxFQUF5QixRQUF6QixFQUFtQztBQUMxQyxpQkFBTyxPQUFQLENBQWUsK0JBQWYsRUFBZ0QsY0FBaEQ7QUFDQSxxQkFBVyxZQUFZLFVBQVMsS0FBVCxFQUFnQjtBQUNyQyxtQkFBTyxPQUFQLENBQWUsOEJBQWY7QUFDQSxnQkFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDdEIsb0JBQU0sZUFBTjtBQUNEO0FBQ0QsbUJBQU8sS0FBUDtBQUNBLG1CQUFPLEdBQVAsQ0FBVyxNQUFYO0FBQ0QsV0FQRDtBQVFBLG1CQUFTLFlBQVQsQ0FBc0IsSUFBdEIsQ0FBMkIsVUFBVSxDQUFWLENBQTNCO0FBQ0EsY0FBRyxPQUFPLEdBQVAsQ0FBVyxlQUFYLEVBQUgsRUFBaUM7QUFDL0IsbUJBQU8sT0FBUCxDQUFlLHdCQUFmO0FBQ0Esb0JBQ0csUUFESCxDQUNZLFVBQVUsU0FEdEI7QUFHQSxtQkFDRyxHQURILENBQ08sY0FEUCxFQUVHLEdBRkgsQ0FFTyxPQUFPLEdBQVAsQ0FBVyxlQUFYLEVBRlAsRUFFcUMsUUFGckM7QUFJQSxtQkFBTyxHQUFQLENBQVcsUUFBWCxDQUFvQixTQUFTLFFBQTdCO0FBQ0Esa0NBQXNCLFlBQVc7QUFDL0Isc0JBQ0csUUFESCxDQUNZLFVBQVUsU0FEdEI7QUFHQSwwQkFDRyxRQURILENBQ1ksVUFBVSxNQUR0QjtBQUdELGFBUEQ7QUFRRCxXQWxCRCxNQW1CSztBQUNIO0FBQ0Q7QUFDRixTQXhFTTs7QUEwRVAsZUFBTyxVQUFTLE1BQVQsRUFBaUI7QUFDdEIsaUJBQU8sS0FBUCxDQUFhLHVCQUFiLEVBQXNDLE1BQXRDO0FBQ0EsaUJBQ0csR0FESCxDQUNPLE9BQU8sR0FBUCxDQUFXLGVBQVgsRUFEUCxFQUNxQyxZQUFXO0FBQzVDLG1CQUFPLEtBQVAsQ0FBYSw0QkFBYjtBQUNBLHVCQUFXLFlBQVU7QUFDbkIsc0JBQVEsS0FBUixDQUFjLE1BQWQ7QUFDRCxhQUZELEVBRUcsQ0FGSDtBQUdELFdBTkg7QUFRRCxTQXBGTTs7QUFzRlAsZUFBTyxZQUFXO0FBQ2hCLGlCQUFPLE9BQVAsQ0FBZSx3QkFBZjtBQUNBLGtCQUNHLFdBREgsQ0FDZSxVQUFVLFNBRHpCLEVBRUcsSUFGSCxDQUVRLE9BRlIsRUFFaUIsRUFGakIsRUFHRyxVQUhILENBR2MsT0FIZDs7QUFNQSxpQkFDRyxJQURILENBQ1EsT0FEUixFQUNpQixFQURqQixFQUVHLFVBRkgsQ0FFYyxPQUZkO0FBSUEsZ0JBQ0csSUFESCxDQUNRLE9BRFIsRUFDaUIsRUFEakIsRUFFRyxVQUZILENBRWMsT0FGZCxFQUdHLFdBSEgsQ0FHZSxVQUFVLE1BSHpCO0FBS0Esb0JBQ0csV0FESCxDQUNlLFVBQVUsU0FEekIsRUFFRyxJQUZILENBRVEsT0FGUixFQUVpQixFQUZqQixFQUdHLFVBSEgsQ0FHYyxPQUhkO0FBS0QsU0E1R007O0FBOEdQLFlBQUk7QUFDRixvQkFBVSxZQUFXO0FBQ25CLG1CQUFRLE1BQU0sTUFBTixDQUFhLE1BQU0sVUFBVSxNQUE3QixFQUFxQyxDQUFyQyxLQUEyQyxVQUFVLENBQVYsQ0FBbkQ7QUFDRCxXQUhDO0FBSUYscUJBQVcsWUFBVztBQUNwQixtQkFBTyxRQUFRLFFBQVIsQ0FBaUIsVUFBVSxTQUEzQixDQUFQO0FBQ0Q7QUFOQyxTQTlHRzs7QUF1SFAsYUFBSzs7QUFFSCx1QkFBYSxZQUFXO0FBQ3RCLDBCQUFjLFFBQVEsSUFBUixDQUFhLE1BQU0sU0FBUyxTQUFULENBQW1CLE1BQXRDLENBQWQ7QUFDQSx3QkFBZ0IsWUFBWSxJQUFaLENBQWlCLFNBQVMsSUFBMUIsRUFBZ0MsTUFBaEMsR0FBeUMsQ0FBM0MsR0FDVixZQUFZLElBQVosQ0FBaUIsU0FBUyxJQUExQixDQURVLEdBRVYsUUFBUSxJQUFSLENBQWEsU0FBUyxJQUF0QixFQUE0QixLQUE1QixFQUZKO0FBSUEsd0JBQVksS0FBWjtBQUNBLG1CQUFPLE9BQVAsQ0FBZSxvQkFBZixFQUFxQyxXQUFyQztBQUNBLG1CQUFPLE9BQVAsQ0FBZSxrQkFBZixFQUFtQyxTQUFuQztBQUNELFdBWEU7O0FBYUgsb0JBQVUsVUFBUyxRQUFULEVBQW1CO0FBQzNCLHVCQUFXLFlBQVksU0FBUyxRQUFoQztBQUNBLHVCQUFZLE9BQU8sUUFBUCxJQUFtQixRQUFwQixHQUNQLFdBQVcsSUFESixHQUVQLFFBRko7QUFJQSxtQkFBTyxPQUFQLENBQWUsNEJBQWYsRUFBNkMsUUFBN0M7QUFDQSxnQkFBRyxTQUFTLFFBQVQsSUFBcUIsU0FBUyxRQUFULEtBQXNCLENBQTlDLEVBQWlEO0FBQy9DLHFCQUFPLEdBQVAsQ0FBVyxLQUFYLEVBQ0csR0FESCxDQUNPO0FBQ0gsK0NBQStCLFFBRDVCO0FBRUgsNENBQTRCLFFBRnpCO0FBR0gsMkNBQTJCLFFBSHhCO0FBSUgsMENBQTBCLFFBSnZCO0FBS0gsdUNBQXVCO0FBTHBCLGVBRFA7QUFTRDtBQUNGLFdBL0JFOztBQWlDSCw0QkFBa0IsWUFBVztBQUMzQixnQkFDRSxjQUFjLFFBQVEsSUFBUixDQUFhLE1BQU0sU0FBUyxTQUFULENBQW1CLE1BQXRDLENBRGhCO0FBQUEsZ0JBRUUsUUFBYyxZQUFZLFVBQVosQ0FBdUIsSUFBdkIsQ0FGaEI7QUFBQSxnQkFHRSxTQUFjLFlBQVksV0FBWixDQUF3QixJQUF4QixDQUhoQjtBQUtBLG9CQUNHLEdBREgsQ0FDTztBQUNILHFCQUFPLEtBREo7QUFFSCxzQkFBUTtBQUZMLGFBRFA7QUFNRCxXQTdDRTs7QUErQ0gscUJBQVcsWUFBVztBQUNwQixnQkFDRSxTQUFjLFFBQVEsS0FBUixHQUFnQixRQUFoQixDQUF5QixVQUFVLE9BQW5DLENBRGhCO0FBQUEsZ0JBRUUsY0FBYyxPQUFPLElBQVAsQ0FBWSxNQUFNLFNBQVMsU0FBVCxDQUFtQixNQUFyQyxDQUZoQjtBQUFBLGdCQUdFLFlBQWUsU0FBRCxHQUNWLE9BQU8sSUFBUCxDQUFZLFNBQVMsSUFBckIsRUFBMkIsRUFBM0IsQ0FBOEIsU0FBOUIsQ0FEVSxHQUVSLFlBQVksSUFBWixDQUFpQixTQUFTLElBQTFCLEVBQWdDLE1BQWhDLEdBQXlDLENBQTNDLEdBQ0UsWUFBWSxJQUFaLENBQWlCLFNBQVMsSUFBMUIsQ0FERixHQUVFLE9BQU8sSUFBUCxDQUFZLFNBQVMsSUFBckIsRUFBMkIsS0FBM0IsRUFQUjtBQUFBLGdCQVFFLFdBQWUsU0FBUyxLQUFULElBQWtCLE1BQW5CLEdBQ1YsVUFBVSxVQUFWLENBQXFCLElBQXJCLENBRFUsR0FFVCxTQUFTLEtBQVQsSUFBa0IsU0FBbkIsR0FDRSxRQUFRLEtBQVIsRUFERixHQUVFLFNBQVMsS0FaakI7QUFBQSxnQkFhRSxZQUFnQixTQUFTLE1BQVQsSUFBbUIsTUFBcEIsR0FDWCxVQUFVLFdBQVYsQ0FBc0IsSUFBdEIsQ0FEVyxHQUVWLFNBQVMsTUFBVCxJQUFtQixTQUFwQixHQUNFLFFBQVEsTUFBUixFQURGLEdBRUUsU0FBUyxNQWpCakI7QUFtQkEsd0JBQVksV0FBWixDQUF3QixVQUFVLE1BQWxDO0FBQ0Esc0JBQVUsUUFBVixDQUFtQixVQUFVLE1BQTdCO0FBQ0EsbUJBQU8sV0FBUCxDQUFtQixPQUFuQjtBQUNBLG1CQUFPLE1BQVA7QUFDQSxnQkFBRyxTQUFTLEtBQVQsSUFBa0IsTUFBckIsRUFBNkI7QUFDM0Isc0JBQVEsR0FBUixDQUFZLE9BQVosRUFBcUIsV0FBVyxTQUFTLE1BQXpDO0FBQ0EscUJBQU8sT0FBUCxDQUFlLG1DQUFmLEVBQW9ELFFBQXBEO0FBQ0Q7QUFDRCxnQkFBRyxTQUFTLE1BQVQsSUFBbUIsTUFBdEIsRUFBOEI7QUFDNUIsc0JBQVEsR0FBUixDQUFZLFFBQVosRUFBc0IsWUFBWSxTQUFTLE1BQTNDO0FBQ0EscUJBQU8sT0FBUCxDQUFlLG9DQUFmLEVBQXFELFNBQXJEO0FBQ0Q7QUFDRixXQS9FRTs7QUFpRkgsb0JBQVUsVUFBUyxRQUFULEVBQW1CO0FBQzNCLHdCQUFZLFFBQVo7QUFDQSx3QkFBWSxNQUFNLE1BQU4sQ0FBYSxRQUFiLENBQVo7QUFDQSx3QkFBWSxNQUFNLEtBQU4sQ0FBWSxTQUFaLENBQVo7QUFDQSxnQkFBRyxVQUFVLE1BQVYsS0FBcUIsQ0FBeEIsRUFBMkI7QUFDekIscUJBQU8sR0FBUCxDQUFXLFdBQVg7QUFDQSxxQkFBTyxLQUFQLENBQWEsTUFBTSxJQUFuQjtBQUNEO0FBQ0QsbUJBQU8sT0FBUCxDQUFlLDJCQUFmLEVBQTRDLFNBQTVDO0FBQ0QsV0ExRkU7O0FBNEZILGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sT0FBUCxDQUFlLDRCQUFmLEVBQTZDLFNBQTdDO0FBQ0Esa0JBQ0csV0FESCxDQUNlLFVBQVUsTUFEekI7QUFHQSxzQkFDRyxRQURILENBQ1ksVUFBVSxNQUR0QjtBQUdBLHFCQUFTLFFBQVQsQ0FBa0IsSUFBbEIsQ0FBdUIsVUFBVSxDQUFWLENBQXZCO0FBQ0EsbUJBQU8sR0FBUCxDQUFXLFdBQVg7QUFDRDtBQXRHRSxTQXZIRTs7QUFnT1AsY0FBTTs7QUFFSixjQUFJLFlBQVc7QUFDYixnQkFBRyxPQUFPLEVBQVAsQ0FBVSxRQUFWLE1BQXdCLENBQUMsT0FBTyxFQUFQLENBQVUsU0FBVixFQUF6QixJQUFrRCxDQUFDLFNBQVMsWUFBL0QsRUFBNkU7QUFDM0UscUJBQU8sS0FBUCxDQUFhLHNCQUFiLEVBQXFDLFNBQXJDO0FBQ0E7QUFDRDtBQUNELGdCQUFJLENBQUMsT0FBTyxFQUFQLENBQVUsU0FBVixFQUFMLEVBQTRCO0FBQzFCLHFCQUFPLEtBQVAsQ0FBYSxhQUFiLEVBQTRCLFNBQTVCO0FBQ0Esa0JBQ0UsWUFBWSxPQUFPLEdBQVAsQ0FBVyxTQUFYLENBQXFCLEVBQXJCLEVBRGQ7QUFHQSxxQkFBTyxHQUFQLENBQVcsU0FBWDtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxLQUFiO0FBQ0EscUJBQU8sT0FBUCxDQUFlLFNBQWY7QUFDRCxhQVJELE1BU0s7QUFDSCxxQkFBTyxLQUFQLENBQWEsU0FBYjtBQUNEO0FBQ0YsV0FuQkc7O0FBcUJKLGdCQUFNLFlBQVc7QUFDZixnQkFBRyxPQUFPLEVBQVAsQ0FBVSxRQUFWLE1BQXdCLENBQUMsT0FBTyxFQUFQLENBQVUsU0FBVixFQUF6QixJQUFrRCxDQUFDLFNBQVMsWUFBL0QsRUFBNkU7QUFDM0UscUJBQU8sS0FBUCxDQUFhLHNCQUFiLEVBQXFDLFNBQXJDO0FBQ0E7QUFDRDtBQUNELGdCQUFJLENBQUMsT0FBTyxFQUFQLENBQVUsU0FBVixFQUFMLEVBQTRCO0FBQzFCLHFCQUFPLEtBQVAsQ0FBYSxlQUFiLEVBQThCLFNBQTlCO0FBQ0Esa0JBQ0UsWUFBWSxPQUFPLEdBQVAsQ0FBVyxTQUFYLENBQXFCLElBQXJCLEVBRGQ7QUFHQSxxQkFBTyxHQUFQLENBQVcsU0FBWDtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxLQUFiO0FBQ0EscUJBQU8sT0FBUCxDQUFlLFNBQWY7QUFDRCxhQVJELE1BU0s7QUFDSCxxQkFBTyxLQUFQLENBQWEsV0FBYjtBQUNEO0FBQ0YsV0F0Q0c7O0FBd0NKLGdCQUFNLFlBQVc7QUFDZixnQkFBRyxPQUFPLEVBQVAsQ0FBVSxRQUFWLE1BQXdCLENBQUMsT0FBTyxFQUFQLENBQVUsU0FBVixFQUF6QixJQUFrRCxDQUFDLFNBQVMsWUFBL0QsRUFBNkU7QUFDM0UscUJBQU8sS0FBUCxDQUFhLHNCQUFiLEVBQXFDLFNBQXJDO0FBQ0E7QUFDRDtBQUNELGdCQUFJLENBQUMsT0FBTyxFQUFQLENBQVUsU0FBVixFQUFMLEVBQTRCO0FBQzFCLHFCQUFPLEtBQVAsQ0FBYSxlQUFiLEVBQThCLFNBQTlCO0FBQ0Esa0JBQ0UsWUFBWSxPQUFPLEdBQVAsQ0FBVyxTQUFYLENBQXFCLElBQXJCLEVBRGQ7QUFHQSxxQkFBTyxHQUFQLENBQVcsU0FBWDtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxJQUFiO0FBQ0EscUJBQU8sT0FBUCxDQUFlLFNBQWY7QUFDRCxhQVJELE1BU0s7QUFDSCxxQkFBTyxLQUFQLENBQWEsV0FBYjtBQUNEO0FBQ0YsV0F6REc7O0FBMkRKLGlCQUFPLFlBQVc7QUFDaEIsZ0JBQUcsT0FBTyxFQUFQLENBQVUsUUFBVixNQUF3QixDQUFDLE9BQU8sRUFBUCxDQUFVLFNBQVYsRUFBekIsSUFBa0QsQ0FBQyxTQUFTLFlBQS9ELEVBQTZFO0FBQzNFLHFCQUFPLEtBQVAsQ0FBYSxzQkFBYixFQUFxQyxTQUFyQztBQUNBO0FBQ0Q7QUFDRCxnQkFBSSxDQUFDLE9BQU8sRUFBUCxDQUFVLFNBQVYsRUFBTCxFQUE0QjtBQUMxQixxQkFBTyxLQUFQLENBQWEsZ0JBQWIsRUFBK0IsU0FBL0I7QUFDQSxrQkFDRSxZQUFZLE9BQU8sR0FBUCxDQUFXLFNBQVgsQ0FBcUIsS0FBckIsRUFEZDtBQUdBLHFCQUFPLEdBQVAsQ0FBVyxTQUFYO0FBQ0EscUJBQU8sS0FBUCxDQUFhLEtBQWI7QUFDQSxxQkFBTyxPQUFQLENBQWUsU0FBZjtBQUNELGFBUkQsTUFTSztBQUNILHFCQUFPLEtBQVAsQ0FBYSxZQUFiO0FBQ0Q7QUFDRixXQTVFRzs7QUE4RUosZ0JBQU0sWUFBVztBQUNmLGdCQUFHLE9BQU8sRUFBUCxDQUFVLFFBQVYsTUFBd0IsQ0FBQyxPQUFPLEVBQVAsQ0FBVSxTQUFWLEVBQXpCLElBQWtELENBQUMsU0FBUyxZQUEvRCxFQUE2RTtBQUMzRSxxQkFBTyxLQUFQLENBQWEsc0JBQWIsRUFBcUMsU0FBckM7QUFDQTtBQUNEO0FBQ0QsZ0JBQUksQ0FBQyxPQUFPLEVBQVAsQ0FBVSxTQUFWLEVBQUwsRUFBNEI7QUFDMUIscUJBQU8sS0FBUCxDQUFhLGVBQWIsRUFBOEIsU0FBOUI7QUFDQSxxQkFBTyxHQUFQLENBQVcsU0FBWDtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxNQUFiO0FBQ0EscUJBQU8sT0FBUCxDQUFlLE9BQU8sR0FBUCxDQUFXLFNBQVgsQ0FBcUIsSUFBckIsRUFBZjtBQUNELGFBTEQsTUFNSztBQUNILHFCQUFPLEtBQVAsQ0FBYSxXQUFiO0FBQ0Q7QUFDRixXQTVGRzs7QUE4RkosZ0JBQU0sWUFBVztBQUNmLGdCQUFHLE9BQU8sRUFBUCxDQUFVLFFBQVYsTUFBd0IsQ0FBQyxPQUFPLEVBQVAsQ0FBVSxTQUFWLEVBQXpCLElBQWtELENBQUMsU0FBUyxZQUEvRCxFQUE2RTtBQUMzRSxxQkFBTyxLQUFQLENBQWEsc0JBQWIsRUFBcUMsU0FBckM7QUFDQTtBQUNEO0FBQ0QsZ0JBQUksQ0FBQyxPQUFPLEVBQVAsQ0FBVSxTQUFWLEVBQUwsRUFBNEI7QUFDMUIscUJBQU8sS0FBUCxDQUFhLGVBQWIsRUFBOEIsU0FBOUI7QUFDQSxxQkFBTyxHQUFQLENBQVcsU0FBWDtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxNQUFiO0FBQ0EscUJBQU8sT0FBUCxDQUFlLE9BQU8sR0FBUCxDQUFXLFNBQVgsQ0FBcUIsSUFBckIsRUFBZjtBQUNELGFBTEQsTUFNSztBQUNILHFCQUFPLEtBQVAsQ0FBYSxXQUFiO0FBQ0Q7QUFDRjs7QUE1R0csU0FoT0M7O0FBZ1ZQLGFBQUs7O0FBRUgscUJBQVc7QUFDVCxnQkFBSSxZQUFXO0FBQ2Isa0JBQ0UsWUFBWTtBQUNWLG1CQUFHLEVBQUUsQ0FBQyxZQUFZLFdBQVosQ0FBd0IsSUFBeEIsSUFBZ0MsVUFBVSxXQUFWLENBQXNCLElBQXRCLENBQWpDLElBQWdFLENBQWxFLENBRE87QUFFVixtQkFBRyxFQUFFLFlBQVksV0FBWixDQUF3QixJQUF4QixJQUFnQyxDQUFsQztBQUZPLGVBRGQ7QUFNQSxxQkFBTztBQUNMLDJCQUFXLGdCQUFnQixVQUFVLENBQTFCLEdBQThCLGlCQUE5QixHQUFpRCxVQUFVLENBQTNELEdBQStEO0FBRHJFLGVBQVA7QUFHRCxhQVhROztBQWFULGtCQUFNLFlBQVc7QUFDZixrQkFDRSxZQUFZO0FBQ1YsbUJBQUcsRUFBRSxDQUFDLFlBQVksV0FBWixDQUF3QixJQUF4QixJQUFnQyxVQUFVLFdBQVYsQ0FBc0IsSUFBdEIsQ0FBakMsSUFBZ0UsQ0FBbEUsQ0FETztBQUVWLG1CQUFHLEVBQUUsWUFBWSxXQUFaLENBQXdCLElBQXhCLElBQWdDLENBQWxDO0FBRk8sZUFEZDtBQU1BLHFCQUFPO0FBQ0wsMkJBQVcsZ0JBQWdCLFVBQVUsQ0FBMUIsR0FBOEIsaUJBQTlCLEdBQWlELFVBQVUsQ0FBM0QsR0FBK0Q7QUFEckUsZUFBUDtBQUdELGFBdkJROztBQXlCVCxrQkFBTSxZQUFXO0FBQ2Ysa0JBQ0UsWUFBWTtBQUNWLG1CQUFJLEVBQUUsQ0FBQyxZQUFZLFVBQVosQ0FBdUIsSUFBdkIsSUFBK0IsVUFBVSxVQUFWLENBQXFCLElBQXJCLENBQWhDLElBQThELENBQWhFLENBRE07QUFFVixtQkFBSSxFQUFFLFlBQVksVUFBWixDQUF1QixJQUF2QixJQUErQixDQUFqQztBQUZNLGVBRGQ7QUFNQSxxQkFBTztBQUNMLDJCQUFXLGdCQUFnQixVQUFVLENBQTFCLEdBQThCLGlCQUE5QixHQUFrRCxVQUFVLENBQTVELEdBQWdFO0FBRHRFLGVBQVA7QUFHRCxhQW5DUTs7QUFxQ1QsbUJBQU8sWUFBVztBQUNoQixrQkFDRSxZQUFZO0FBQ1YsbUJBQUksRUFBRSxDQUFDLFlBQVksVUFBWixDQUF1QixJQUF2QixJQUErQixVQUFVLFVBQVYsQ0FBcUIsSUFBckIsQ0FBaEMsSUFBOEQsQ0FBaEUsQ0FETTtBQUVWLG1CQUFJLEVBQUUsWUFBWSxVQUFaLENBQXVCLElBQXZCLElBQStCLENBQWpDO0FBRk0sZUFEZDtBQU1BLHFCQUFPO0FBQ0wsMkJBQVcsZ0JBQWdCLFVBQVUsQ0FBMUIsR0FBOEIsaUJBQTlCLEdBQWtELFVBQVUsQ0FBNUQsR0FBZ0U7QUFEdEUsZUFBUDtBQUdELGFBL0NROztBQWlEVCxrQkFBTSxZQUFXO0FBQ2Ysa0JBQ0UsWUFBWTtBQUNWLG1CQUFJLEVBQUUsQ0FBQyxZQUFZLFVBQVosQ0FBdUIsSUFBdkIsSUFBK0IsVUFBVSxVQUFWLENBQXFCLElBQXJCLENBQWhDLElBQThELENBQWhFO0FBRE0sZUFEZDtBQUtBLHFCQUFPO0FBQ0wsMkJBQVcsZ0JBQWdCLFVBQVUsQ0FBMUIsR0FBOEI7QUFEcEMsZUFBUDtBQUdELGFBMURROztBQTREVCxrQkFBTSxZQUFXO0FBQ2Ysa0JBQ0UsWUFBWTtBQUNWLG1CQUFJLEVBQUUsQ0FBQyxZQUFZLFVBQVosQ0FBdUIsSUFBdkIsSUFBK0IsVUFBVSxVQUFWLENBQXFCLElBQXJCLENBQWhDLElBQThELENBQWhFO0FBRE0sZUFEZDtBQUtBLHFCQUFPO0FBQ0wsMkJBQVcsZ0JBQWdCLFVBQVUsQ0FBMUIsR0FBOEI7QUFEcEMsZUFBUDtBQUdEO0FBckVRLFdBRlI7O0FBMEVILDJCQUFpQixZQUFXO0FBQzFCLGdCQUNFLFVBQWMsU0FBUyxhQUFULENBQXVCLFNBQXZCLENBRGhCO0FBQUEsZ0JBRUUsY0FBYztBQUNaLDRCQUFvQixlQURSO0FBRVosNkJBQW9CLGdCQUZSO0FBR1osK0JBQW9CLGVBSFI7QUFJWixrQ0FBb0I7QUFKUixhQUZoQjtBQUFBLGdCQVFFLFVBUkY7QUFVQSxpQkFBSSxVQUFKLElBQWtCLFdBQWxCLEVBQThCO0FBQzVCLGtCQUFJLFFBQVEsS0FBUixDQUFjLFVBQWQsTUFBOEIsU0FBbEMsRUFBNkM7QUFDM0MsdUJBQU8sWUFBWSxVQUFaLENBQVA7QUFDRDtBQUNGO0FBQ0YsV0ExRkU7O0FBNEZILG9CQUFVLFlBQVc7QUFDbkIsbUJBQVMsWUFBWSxJQUFaLENBQWlCLFNBQVMsSUFBMUIsRUFBZ0MsTUFBaEMsR0FBeUMsQ0FBM0MsR0FDSCxZQUFZLElBQVosQ0FBaUIsU0FBUyxJQUExQixDQURHLEdBRUgsUUFBUSxJQUFSLENBQWEsU0FBUyxJQUF0QixFQUE0QixLQUE1QixFQUZKO0FBSUQ7O0FBakdFLFNBaFZFOztBQXFiUCxlQUFPOztBQUVMLGlCQUFPLFlBQVc7QUFDaEIsZ0JBQ0UsTUFBTTtBQUNKLHNCQUFVLENBQUMsWUFBWSxXQUFaLENBQXdCLElBQXhCLElBQWdDLFVBQVUsV0FBVixDQUFzQixJQUF0QixDQUFqQyxJQUFnRSxDQUR0RTtBQUVKLHFCQUFTO0FBQ1Asd0JBQVUsVUFBVSxXQUFWLENBQXNCLElBQXRCLElBQThCLENBRGpDO0FBRVAsc0JBQVUsWUFBWSxXQUFaLENBQXdCLElBQXhCLElBQWdDO0FBRm5DO0FBRkwsYUFEUjtBQVNBLG1CQUFPLE9BQVAsQ0FBZSxpREFBZixFQUFrRSxTQUFsRSxFQUE2RSxHQUE3RTtBQUNBLG1CQUNHLEdBREgsQ0FDTztBQUNILDJCQUFjLGlCQUFpQixJQUFJLEtBQUosQ0FBVSxNQUEzQixHQUFvQztBQUQvQyxhQURQO0FBS0Esd0JBQ0csR0FESCxDQUNPO0FBQ0gsMkJBQWMsOEJBQThCLElBQUksS0FBSixDQUFVLE1BQXhDLEdBQWlEO0FBRDVELGFBRFA7QUFLQSxzQkFDRyxRQURILENBQ1ksVUFBVSxTQUR0QixFQUVHLEdBRkgsQ0FFTztBQUNILHFCQUFjLElBQUksTUFBSixHQUFhLElBRHhCO0FBRUgsMkJBQWMsK0JBQStCLElBQUksS0FBSixDQUFVLElBQXpDLEdBQWdEO0FBRjNELGFBRlA7QUFPRCxXQTlCSTs7QUFnQ0wsaUJBQU8sWUFBVztBQUNoQixnQkFDRSxNQUFNO0FBQ0osc0JBQVUsQ0FBQyxZQUFZLFdBQVosQ0FBd0IsSUFBeEIsSUFBZ0MsVUFBVSxXQUFWLENBQXNCLElBQXRCLENBQWpDLElBQWdFLENBRHRFO0FBRUoscUJBQVM7QUFDUCx3QkFBVSxVQUFVLFdBQVYsQ0FBc0IsSUFBdEIsSUFBOEIsQ0FEakM7QUFFUCxzQkFBVSxZQUFZLFdBQVosQ0FBd0IsSUFBeEIsSUFBZ0M7QUFGbkM7QUFGTCxhQURSO0FBU0EsbUJBQU8sT0FBUCxDQUFlLGlEQUFmLEVBQWtFLFNBQWxFLEVBQTZFLEdBQTdFO0FBQ0EsbUJBQ0csR0FESCxDQUNPO0FBQ0gsMkJBQWMsaUJBQWlCLElBQUksS0FBSixDQUFVLE1BQTNCLEdBQW9DO0FBRC9DLGFBRFA7QUFLQSx3QkFDRyxHQURILENBQ087QUFDSCwyQkFBYyw4QkFBOEIsSUFBSSxLQUFKLENBQVUsTUFBeEMsR0FBaUQ7QUFENUQsYUFEUDtBQUtBLHNCQUNHLFFBREgsQ0FDWSxVQUFVLFNBRHRCLEVBRUcsR0FGSCxDQUVPO0FBQ0gscUJBQWMsSUFBSSxNQUFKLEdBQWEsSUFEeEI7QUFFSCwyQkFBYyxnQ0FBZ0MsSUFBSSxLQUFKLENBQVUsSUFBMUMsR0FBaUQ7QUFGNUQsYUFGUDtBQU9ELFdBNURJOztBQThETCxnQkFBTSxZQUFXO0FBQ2YsZ0JBQ0UsU0FBUztBQUNQLHNCQUFTLFlBQVksVUFBWixDQUF1QixJQUF2QixDQURGO0FBRVAsb0JBQVMsVUFBVSxVQUFWLENBQXFCLElBQXJCO0FBRkYsYUFEWDtBQUFBLGdCQUtFLE1BQU07QUFDSixzQkFBVyxDQUFFLE9BQU8sTUFBUCxHQUFnQixPQUFPLElBQXpCLElBQWtDLENBRHpDO0FBRUoscUJBQVM7QUFDUCx3QkFBVSxPQUFPLElBQVAsR0FBYyxDQURqQjtBQUVQLHNCQUFVLE9BQU8sTUFBUCxHQUFnQjtBQUZuQjtBQUZMLGFBTFI7QUFhQSxtQkFBTyxPQUFQLENBQWUsZ0RBQWYsRUFBaUUsU0FBakUsRUFBNEUsR0FBNUU7QUFDQSxtQkFDRyxHQURILENBQ087QUFDSCwyQkFBYyxpQkFBaUIsSUFBSSxLQUFKLENBQVUsTUFBM0IsR0FBb0M7QUFEL0MsYUFEUDtBQUtBLHdCQUNHLEdBREgsQ0FDTztBQUNILDJCQUFjLDhCQUE4QixJQUFJLEtBQUosQ0FBVSxNQUF4QyxHQUFpRDtBQUQ1RCxhQURQO0FBS0Esc0JBQ0csUUFESCxDQUNZLFVBQVUsU0FEdEIsRUFFRyxHQUZILENBRU87QUFDSCxzQkFBYyxJQUFJLE1BQUosR0FBYSxJQUR4QjtBQUVILDJCQUFjLGdDQUFnQyxJQUFJLEtBQUosQ0FBVSxJQUExQyxHQUFpRDtBQUY1RCxhQUZQO0FBT0QsV0E5Rkk7O0FBZ0dMLGlCQUFPLFlBQVc7QUFDaEIsZ0JBQ0UsU0FBUztBQUNQLHNCQUFTLFlBQVksVUFBWixDQUF1QixJQUF2QixDQURGO0FBRVAsb0JBQVMsVUFBVSxVQUFWLENBQXFCLElBQXJCO0FBRkYsYUFEWDtBQUFBLGdCQUtFLE1BQU07QUFDSixzQkFBVyxDQUFFLE9BQU8sTUFBUCxHQUFnQixPQUFPLElBQXpCLElBQWtDLENBRHpDO0FBRUoscUJBQVM7QUFDUCx3QkFBVSxPQUFPLElBQVAsR0FBYyxDQURqQjtBQUVQLHNCQUFVLE9BQU8sTUFBUCxHQUFnQjtBQUZuQjtBQUZMLGFBTFI7QUFhQSxtQkFBTyxPQUFQLENBQWUsZ0RBQWYsRUFBaUUsU0FBakUsRUFBNEUsR0FBNUU7QUFDQSxtQkFDRyxHQURILENBQ087QUFDSCwyQkFBYyxpQkFBaUIsSUFBSSxLQUFKLENBQVUsTUFBM0IsR0FBb0M7QUFEL0MsYUFEUDtBQUtBLHdCQUNHLEdBREgsQ0FDTztBQUNILDJCQUFjLDhCQUE4QixJQUFJLEtBQUosQ0FBVSxNQUF4QyxHQUFpRDtBQUQ1RCxhQURQO0FBS0Esc0JBQ0csUUFESCxDQUNZLFVBQVUsU0FEdEIsRUFFRyxHQUZILENBRU87QUFDSCxzQkFBYyxJQUFJLE1BQUosR0FBYSxJQUR4QjtBQUVILDJCQUFjLCtCQUErQixJQUFJLEtBQUosQ0FBVSxJQUF6QyxHQUFnRDtBQUYzRCxhQUZQO0FBT0QsV0FoSUk7O0FBa0lMLGtCQUFRLFlBQVc7QUFDakIsZ0JBQ0UsU0FBUztBQUNQLHNCQUFTLFlBQVksVUFBWixDQUF1QixJQUF2QixDQURGO0FBRVAsb0JBQVMsVUFBVSxVQUFWLENBQXFCLElBQXJCO0FBRkYsYUFEWDtBQUFBLGdCQUtFLE1BQU07QUFDSixzQkFBVyxDQUFFLE9BQU8sTUFBUCxHQUFnQixPQUFPLElBQXpCLElBQWtDLENBRHpDO0FBRUoscUJBQVM7QUFDUCx3QkFBVSxPQUFPLElBQVAsR0FBYyxDQURqQjtBQUVQLHNCQUFVLE9BQU8sTUFBUCxHQUFnQjtBQUZuQjtBQUZMLGFBTFI7QUFhQSxtQkFBTyxPQUFQLENBQWUsa0RBQWYsRUFBbUUsU0FBbkUsRUFBOEUsR0FBOUU7QUFDQSx3QkFDRyxHQURILENBQ087QUFDSCwyQkFBYztBQURYLGFBRFA7QUFLQSxzQkFDRyxRQURILENBQ1ksVUFBVSxTQUR0QixFQUVHLEdBRkgsQ0FFTztBQUNILHNCQUFjLElBQUksTUFBSixHQUFhLElBRHhCO0FBRUgsMkJBQWM7QUFGWCxhQUZQO0FBT0Q7QUE3SkksU0FyYkE7QUFvbEJQLGlCQUFTLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDN0IsaUJBQU8sS0FBUCxDQUFhLGtCQUFiLEVBQWlDLElBQWpDLEVBQXVDLEtBQXZDO0FBQ0EsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsUUFBZixFQUF5QixJQUF6QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixnQkFBRyxFQUFFLGFBQUYsQ0FBZ0IsU0FBUyxJQUFULENBQWhCLENBQUgsRUFBb0M7QUFDbEMsZ0JBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxTQUFTLElBQVQsQ0FBZixFQUErQixLQUEvQjtBQUNELGFBRkQsTUFHSztBQUNILHVCQUFTLElBQVQsSUFBaUIsS0FBakI7QUFDRDtBQUNGLFdBUEksTUFRQTtBQUNILG1CQUFPLFNBQVMsSUFBVCxDQUFQO0FBQ0Q7QUFDRixTQXBtQk07QUFxbUJQLGtCQUFVLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDOUIsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsTUFBZixFQUF1QixJQUF2QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixtQkFBTyxJQUFQLElBQWUsS0FBZjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLE9BQU8sSUFBUCxDQUFQO0FBQ0Q7QUFDRixTQS9tQk07QUFnbkJQLGVBQU8sWUFBVztBQUNoQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsS0FBaEMsRUFBdUM7QUFDckMsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxLQUFQLEdBQWUsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWY7QUFDQSxxQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNEO0FBQ0Y7QUFDRixTQTFuQk07QUEybkJQLGlCQUFTLFlBQVc7QUFDbEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLE9BQTdCLElBQXdDLFNBQVMsS0FBcEQsRUFBMkQ7QUFDekQsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxPQUFQLEdBQWlCLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFqQjtBQUNBLHFCQUFPLE9BQVAsQ0FBZSxLQUFmLENBQXFCLE9BQXJCLEVBQThCLFNBQTlCO0FBQ0Q7QUFDRjtBQUNGLFNBcm9CTTtBQXNvQlAsZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQWIsRUFBcUI7QUFDbkIsbUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLEtBQXJDLEVBQTRDLE9BQTVDLEVBQXFELFNBQVMsSUFBVCxHQUFnQixHQUFyRSxDQUFmO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGLFNBM29CTTtBQTRvQlAscUJBQWE7QUFDWCxlQUFLLFVBQVMsT0FBVCxFQUFrQjtBQUNyQixnQkFDRSxXQURGLEVBRUUsYUFGRixFQUdFLFlBSEY7QUFLQSxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIsNEJBQWdCLElBQUksSUFBSixHQUFXLE9BQVgsRUFBaEI7QUFDQSw2QkFBZ0IsUUFBUSxXQUF4QjtBQUNBLDhCQUFnQixjQUFjLFlBQTlCO0FBQ0EscUJBQWdCLFdBQWhCO0FBQ0EsMEJBQVksSUFBWixDQUFpQjtBQUNmLHdCQUFtQixRQUFRLENBQVIsQ0FESjtBQUVmLDZCQUFtQixHQUFHLEtBQUgsQ0FBUyxJQUFULENBQWMsT0FBZCxFQUF1QixDQUF2QixLQUE2QixFQUZqQztBQUdmLDJCQUFtQixPQUhKO0FBSWYsa0NBQW1CO0FBSkosZUFBakI7QUFNRDtBQUNELHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLG1CQUFPLFdBQVAsQ0FBbUIsS0FBbkIsR0FBMkIsV0FBVyxPQUFPLFdBQVAsQ0FBbUIsT0FBOUIsRUFBdUMsR0FBdkMsQ0FBM0I7QUFDRCxXQXJCVTtBQXNCWCxtQkFBUyxZQUFXO0FBQ2xCLGdCQUNFLFFBQVEsU0FBUyxJQUFULEdBQWdCLEdBRDFCO0FBQUEsZ0JBRUUsWUFBWSxDQUZkO0FBSUEsbUJBQU8sS0FBUDtBQUNBLHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLGNBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDJCQUFhLEtBQUssZ0JBQUwsQ0FBYjtBQUNELGFBRkQ7QUFHQSxxQkFBUyxNQUFNLFNBQU4sR0FBa0IsSUFBM0I7QUFDQSxnQkFBRyxjQUFILEVBQW1CO0FBQ2pCLHVCQUFTLFFBQVEsY0FBUixHQUF5QixJQUFsQztBQUNEO0FBQ0QsZ0JBQUcsWUFBWSxNQUFaLEdBQXFCLENBQXhCLEVBQTJCO0FBQ3pCLHVCQUFTLE1BQU0sR0FBTixHQUFZLFlBQVksTUFBeEIsR0FBaUMsR0FBMUM7QUFDRDtBQUNELGdCQUFJLENBQUMsUUFBUSxLQUFSLEtBQWtCLFNBQWxCLElBQStCLFFBQVEsS0FBUixLQUFrQixTQUFsRCxLQUFnRSxZQUFZLE1BQVosR0FBcUIsQ0FBekYsRUFBNEY7QUFDMUYsc0JBQVEsY0FBUixDQUF1QixLQUF2QjtBQUNBLGtCQUFHLFFBQVEsS0FBWCxFQUFrQjtBQUNoQix3QkFBUSxLQUFSLENBQWMsV0FBZDtBQUNELGVBRkQsTUFHSztBQUNILGtCQUFFLElBQUYsQ0FBTyxXQUFQLEVBQW9CLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjtBQUN4QywwQkFBUSxHQUFSLENBQVksS0FBSyxNQUFMLElBQWUsSUFBZixHQUFzQixLQUFLLGdCQUFMLENBQXRCLEdBQTZDLElBQXpEO0FBQ0QsaUJBRkQ7QUFHRDtBQUNELHNCQUFRLFFBQVI7QUFDRDtBQUNELDBCQUFjLEVBQWQ7QUFDRDtBQXBEVSxTQTVvQk47QUFrc0JQLGdCQUFRLFVBQVMsS0FBVCxFQUFnQixlQUFoQixFQUFpQyxPQUFqQyxFQUEwQztBQUNoRCxjQUNFLFNBQVMsUUFEWDtBQUFBLGNBRUUsUUFGRjtBQUFBLGNBR0UsS0FIRjtBQUFBLGNBSUUsUUFKRjtBQU1BLDRCQUFrQixtQkFBbUIsY0FBckM7QUFDQSxvQkFBa0IsV0FBbUIsT0FBckM7QUFDQSxjQUFHLE9BQU8sS0FBUCxJQUFnQixRQUFoQixJQUE0QixXQUFXLFNBQTFDLEVBQXFEO0FBQ25ELG9CQUFXLE1BQU0sS0FBTixDQUFZLE9BQVosQ0FBWDtBQUNBLHVCQUFXLE1BQU0sTUFBTixHQUFlLENBQTFCO0FBQ0EsY0FBRSxJQUFGLENBQU8sS0FBUCxFQUFjLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUF1QjtBQUNuQyxrQkFBSSxpQkFBa0IsU0FBUyxRQUFWLEdBQ2pCLFFBQVEsTUFBTSxRQUFRLENBQWQsRUFBaUIsTUFBakIsQ0FBd0IsQ0FBeEIsRUFBMkIsV0FBM0IsRUFBUixHQUFtRCxNQUFNLFFBQVEsQ0FBZCxFQUFpQixLQUFqQixDQUF1QixDQUF2QixDQURsQyxHQUVqQixLQUZKO0FBSUEsa0JBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sY0FBUCxDQUFqQixLQUE4QyxTQUFTLFFBQTNELEVBQXVFO0FBQ3JFLHlCQUFTLE9BQU8sY0FBUCxDQUFUO0FBQ0QsZUFGRCxNQUdLLElBQUksT0FBTyxjQUFQLE1BQTJCLFNBQS9CLEVBQTJDO0FBQzlDLHdCQUFRLE9BQU8sY0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQSxJQUFJLEVBQUUsYUFBRixDQUFpQixPQUFPLEtBQVAsQ0FBakIsS0FBcUMsU0FBUyxRQUFsRCxFQUE4RDtBQUNqRSx5QkFBUyxPQUFPLEtBQVAsQ0FBVDtBQUNELGVBRkksTUFHQSxJQUFJLE9BQU8sS0FBUCxNQUFrQixTQUF0QixFQUFrQztBQUNyQyx3QkFBUSxPQUFPLEtBQVAsQ0FBUjtBQUNBLHVCQUFPLEtBQVA7QUFDRCxlQUhJLE1BSUE7QUFDSCx1QkFBTyxLQUFQO0FBQ0Q7QUFDRixhQXRCRDtBQXVCRDtBQUNELGNBQUssRUFBRSxVQUFGLENBQWMsS0FBZCxDQUFMLEVBQTZCO0FBQzNCLHVCQUFXLE1BQU0sS0FBTixDQUFZLE9BQVosRUFBcUIsZUFBckIsQ0FBWDtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQix1QkFBVyxLQUFYO0FBQ0Q7QUFDRCxjQUFHLEVBQUUsT0FBRixDQUFVLGFBQVYsQ0FBSCxFQUE2QjtBQUMzQiwwQkFBYyxJQUFkLENBQW1CLFFBQW5CO0FBQ0QsV0FGRCxNQUdLLElBQUcsa0JBQWtCLFNBQXJCLEVBQWdDO0FBQ25DLDRCQUFnQixDQUFDLGFBQUQsRUFBZ0IsUUFBaEIsQ0FBaEI7QUFDRCxXQUZJLE1BR0EsSUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQzlCLDRCQUFnQixRQUFoQjtBQUNEO0FBQ0QsaUJBQU8sS0FBUDtBQUNEO0FBdHZCTSxPQUFUOztBQXl2QkEsVUFBRyxhQUFILEVBQWtCO0FBQ2hCLFlBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUN6QixpQkFBTyxVQUFQO0FBQ0Q7QUFDRCxlQUFPLE1BQVAsQ0FBYyxLQUFkO0FBQ0QsT0FMRCxNQU1LO0FBQ0gsWUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLG1CQUFTLE1BQVQsQ0FBZ0IsU0FBaEI7QUFDRDtBQUNELGVBQU8sVUFBUDtBQUNEO0FBQ0YsS0F2eUJIOztBQTB5QkEsV0FBUSxrQkFBa0IsU0FBbkIsR0FDSCxhQURHLEdBRUgsSUFGSjtBQUlELEdBbjBCRDs7QUFxMEJBLElBQUUsRUFBRixDQUFLLEtBQUwsQ0FBVyxRQUFYLEdBQXNCOzs7QUFHcEIsVUFBTyxPQUhhOzs7QUFNcEIsWUFBYSxLQU5POzs7QUFTcEIsV0FBYSxLQVRPOzs7QUFZcEIsYUFBYSxLQVpPOzs7QUFlcEIsWUFBYSxDQWZPOzs7QUFrQnBCLGlCQUFhLElBbEJPOzs7QUFxQnBCLGVBQWEsT0FyQk87OztBQXdCcEIsV0FBTyxTQXhCYTs7O0FBMkJwQixZQUFRLFNBM0JZOzs7QUE4QnBCLGtCQUFlLFlBQVcsQ0FBRSxDQTlCUjtBQStCcEIsY0FBZSxZQUFXLENBQUUsQ0EvQlI7OztBQWtDcEIsa0JBQWMsS0FsQ007OztBQXFDcEIsY0FBYSxLQXJDTzs7O0FBd0NwQixXQUFPO0FBQ0wsWUFBUyxvREFESjtBQUVMLGNBQVM7QUFGSixLQXhDYTs7O0FBOENwQixlQUFjO0FBQ1osaUJBQVksV0FEQTtBQUVaLGNBQVksUUFGQTtBQUdaLGVBQVksU0FIQTtBQUlaLGNBQVk7QUFKQSxLQTlDTTs7O0FBc0RwQixjQUFjO0FBQ1osYUFBUSxRQURJO0FBRVosWUFBUTtBQUZJOztBQXRETSxHQUF0QjtBQThEQyxDQTk0QkEsRUE4NEJHLE1BOTRCSCxFQTg0QlcsTUE5NEJYLEVBODRCbUIsUUE5NEJuQiIsImZpbGUiOiJzaGFwZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxuICogIyBTZW1hbnRpYyBVSSAtIFNoYXBlXG4gKiBodHRwOi8vZ2l0aHViLmNvbS9zZW1hbnRpYy1vcmcvc2VtYW50aWMtdWkvXG4gKlxuICpcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxuICogaHR0cDovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVFxuICpcbiAqL1xuXG47KGZ1bmN0aW9uICgkLCB3aW5kb3csIGRvY3VtZW50LCB1bmRlZmluZWQpIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cbndpbmRvdyA9ICh0eXBlb2Ygd2luZG93ICE9ICd1bmRlZmluZWQnICYmIHdpbmRvdy5NYXRoID09IE1hdGgpXG4gID8gd2luZG93XG4gIDogKHR5cGVvZiBzZWxmICE9ICd1bmRlZmluZWQnICYmIHNlbGYuTWF0aCA9PSBNYXRoKVxuICAgID8gc2VsZlxuICAgIDogRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKVxuO1xuXG4kLmZuLnNoYXBlID0gZnVuY3Rpb24ocGFyYW1ldGVycykge1xuICB2YXJcbiAgICAkYWxsTW9kdWxlcyAgICAgPSAkKHRoaXMpLFxuICAgICRib2R5ICAgICAgICAgICA9ICQoJ2JvZHknKSxcblxuICAgIHRpbWUgICAgICAgICAgICA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpLFxuICAgIHBlcmZvcm1hbmNlICAgICA9IFtdLFxuXG4gICAgcXVlcnkgICAgICAgICAgID0gYXJndW1lbnRzWzBdLFxuICAgIG1ldGhvZEludm9rZWQgICA9ICh0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycpLFxuICAgIHF1ZXJ5QXJndW1lbnRzICA9IFtdLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSxcblxuICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZSA9IHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWVcbiAgICAgIHx8IHdpbmRvdy5tb3pSZXF1ZXN0QW5pbWF0aW9uRnJhbWVcbiAgICAgIHx8IHdpbmRvdy53ZWJraXRSZXF1ZXN0QW5pbWF0aW9uRnJhbWVcbiAgICAgIHx8IHdpbmRvdy5tc1JlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgfHwgZnVuY3Rpb24oY2FsbGJhY2spIHsgc2V0VGltZW91dChjYWxsYmFjaywgMCk7IH0sXG5cbiAgICByZXR1cm5lZFZhbHVlXG4gIDtcblxuICAkYWxsTW9kdWxlc1xuICAgIC5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgdmFyXG4gICAgICAgIG1vZHVsZVNlbGVjdG9yID0gJGFsbE1vZHVsZXMuc2VsZWN0b3IgfHwgJycsXG4gICAgICAgIHNldHRpbmdzICAgICAgID0gKCAkLmlzUGxhaW5PYmplY3QocGFyYW1ldGVycykgKVxuICAgICAgICAgID8gJC5leHRlbmQodHJ1ZSwge30sICQuZm4uc2hhcGUuc2V0dGluZ3MsIHBhcmFtZXRlcnMpXG4gICAgICAgICAgOiAkLmV4dGVuZCh7fSwgJC5mbi5zaGFwZS5zZXR0aW5ncyksXG5cbiAgICAgICAgLy8gaW50ZXJuYWwgYWxpYXNlc1xuICAgICAgICBuYW1lc3BhY2UgICAgID0gc2V0dGluZ3MubmFtZXNwYWNlLFxuICAgICAgICBzZWxlY3RvciAgICAgID0gc2V0dGluZ3Muc2VsZWN0b3IsXG4gICAgICAgIGVycm9yICAgICAgICAgPSBzZXR0aW5ncy5lcnJvcixcbiAgICAgICAgY2xhc3NOYW1lICAgICA9IHNldHRpbmdzLmNsYXNzTmFtZSxcblxuICAgICAgICAvLyBkZWZpbmUgbmFtZXNwYWNlcyBmb3IgbW9kdWxlc1xuICAgICAgICBldmVudE5hbWVzcGFjZSAgPSAnLicgKyBuYW1lc3BhY2UsXG4gICAgICAgIG1vZHVsZU5hbWVzcGFjZSA9ICdtb2R1bGUtJyArIG5hbWVzcGFjZSxcblxuICAgICAgICAvLyBzZWxlY3RvciBjYWNoZVxuICAgICAgICAkbW9kdWxlICAgICAgID0gJCh0aGlzKSxcbiAgICAgICAgJHNpZGVzICAgICAgICA9ICRtb2R1bGUuZmluZChzZWxlY3Rvci5zaWRlcyksXG4gICAgICAgICRzaWRlICAgICAgICAgPSAkbW9kdWxlLmZpbmQoc2VsZWN0b3Iuc2lkZSksXG5cbiAgICAgICAgLy8gcHJpdmF0ZSB2YXJpYWJsZXNcbiAgICAgICAgbmV4dEluZGV4ID0gZmFsc2UsXG4gICAgICAgICRhY3RpdmVTaWRlLFxuICAgICAgICAkbmV4dFNpZGUsXG5cbiAgICAgICAgLy8gc3RhbmRhcmQgbW9kdWxlXG4gICAgICAgIGVsZW1lbnQgICAgICAgPSB0aGlzLFxuICAgICAgICBpbnN0YW5jZSAgICAgID0gJG1vZHVsZS5kYXRhKG1vZHVsZU5hbWVzcGFjZSksXG4gICAgICAgIG1vZHVsZVxuICAgICAgO1xuXG4gICAgICBtb2R1bGUgPSB7XG5cbiAgICAgICAgaW5pdGlhbGl6ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0luaXRpYWxpemluZyBtb2R1bGUgZm9yJywgZWxlbWVudCk7XG4gICAgICAgICAgbW9kdWxlLnNldC5kZWZhdWx0U2lkZSgpO1xuICAgICAgICAgIG1vZHVsZS5pbnN0YW50aWF0ZSgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGluc3RhbnRpYXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU3RvcmluZyBpbnN0YW5jZSBvZiBtb2R1bGUnLCBtb2R1bGUpO1xuICAgICAgICAgIGluc3RhbmNlID0gbW9kdWxlO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5kYXRhKG1vZHVsZU5hbWVzcGFjZSwgaW5zdGFuY2UpXG4gICAgICAgICAgO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlc3Ryb3k6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdEZXN0cm95aW5nIHByZXZpb3VzIG1vZHVsZSBmb3InLCBlbGVtZW50KTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAucmVtb3ZlRGF0YShtb2R1bGVOYW1lc3BhY2UpXG4gICAgICAgICAgICAub2ZmKGV2ZW50TmFtZXNwYWNlKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICByZWZyZXNoOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVmcmVzaGluZyBzZWxlY3RvciBjYWNoZSBmb3InLCBlbGVtZW50KTtcbiAgICAgICAgICAkbW9kdWxlID0gJChlbGVtZW50KTtcbiAgICAgICAgICAkc2lkZXMgID0gJCh0aGlzKS5maW5kKHNlbGVjdG9yLnNoYXBlKTtcbiAgICAgICAgICAkc2lkZSAgID0gJCh0aGlzKS5maW5kKHNlbGVjdG9yLnNpZGUpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHJlcGFpbnQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdGb3JjaW5nIHJlcGFpbnQgZXZlbnQnKTtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIHNoYXBlICAgICAgICAgID0gJHNpZGVzWzBdIHx8IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpLFxuICAgICAgICAgICAgZmFrZUFzc2lnbm1lbnQgPSBzaGFwZS5vZmZzZXRXaWR0aFxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBhbmltYXRlOiBmdW5jdGlvbihwcm9wZXJ0eU9iamVjdCwgY2FsbGJhY2spIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQW5pbWF0aW5nIGJveCB3aXRoIHByb3BlcnRpZXMnLCBwcm9wZXJ0eU9iamVjdCk7XG4gICAgICAgICAgY2FsbGJhY2sgPSBjYWxsYmFjayB8fCBmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0V4ZWN1dGluZyBhbmltYXRpb24gY2FsbGJhY2snKTtcbiAgICAgICAgICAgIGlmKGV2ZW50ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUucmVzZXQoKTtcbiAgICAgICAgICAgIG1vZHVsZS5zZXQuYWN0aXZlKCk7XG4gICAgICAgICAgfTtcbiAgICAgICAgICBzZXR0aW5ncy5iZWZvcmVDaGFuZ2UuY2FsbCgkbmV4dFNpZGVbMF0pO1xuICAgICAgICAgIGlmKG1vZHVsZS5nZXQudHJhbnNpdGlvbkV2ZW50KCkpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTdGFydGluZyBDU1MgYW5pbWF0aW9uJyk7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUuYW5pbWF0aW5nKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJHNpZGVzXG4gICAgICAgICAgICAgIC5jc3MocHJvcGVydHlPYmplY3QpXG4gICAgICAgICAgICAgIC5vbmUobW9kdWxlLmdldC50cmFuc2l0aW9uRXZlbnQoKSwgY2FsbGJhY2spXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBtb2R1bGUuc2V0LmR1cmF0aW9uKHNldHRpbmdzLmR1cmF0aW9uKTtcbiAgICAgICAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZShmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUuYW5pbWF0aW5nKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgICRhY3RpdmVTaWRlXG4gICAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5oaWRkZW4pXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHF1ZXVlOiBmdW5jdGlvbihtZXRob2QpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ1F1ZXVlaW5nIGFuaW1hdGlvbiBvZicsIG1ldGhvZCk7XG4gICAgICAgICAgJHNpZGVzXG4gICAgICAgICAgICAub25lKG1vZHVsZS5nZXQudHJhbnNpdGlvbkV2ZW50KCksIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0V4ZWN1dGluZyBxdWV1ZWQgYW5pbWF0aW9uJyk7XG4gICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAkbW9kdWxlLnNoYXBlKG1ldGhvZCk7XG4gICAgICAgICAgICAgIH0sIDApO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVzZXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdBbmltYXRpbmcgc3RhdGVzIHJlc2V0Jyk7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5hbmltYXRpbmcpXG4gICAgICAgICAgICAuYXR0cignc3R5bGUnLCAnJylcbiAgICAgICAgICAgIC5yZW1vdmVBdHRyKCdzdHlsZScpXG4gICAgICAgICAgO1xuICAgICAgICAgIC8vIHJlbW92ZUF0dHIgc3R5bGUgZG9lcyBub3QgY29uc2lzdGVudGx5IHdvcmsgaW4gc2FmYXJpXG4gICAgICAgICAgJHNpZGVzXG4gICAgICAgICAgICAuYXR0cignc3R5bGUnLCAnJylcbiAgICAgICAgICAgIC5yZW1vdmVBdHRyKCdzdHlsZScpXG4gICAgICAgICAgO1xuICAgICAgICAgICRzaWRlXG4gICAgICAgICAgICAuYXR0cignc3R5bGUnLCAnJylcbiAgICAgICAgICAgIC5yZW1vdmVBdHRyKCdzdHlsZScpXG4gICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmhpZGRlbilcbiAgICAgICAgICA7XG4gICAgICAgICAgJG5leHRTaWRlXG4gICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmFuaW1hdGluZylcbiAgICAgICAgICAgIC5hdHRyKCdzdHlsZScsICcnKVxuICAgICAgICAgICAgLnJlbW92ZUF0dHIoJ3N0eWxlJylcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaXM6IHtcbiAgICAgICAgICBjb21wbGV0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gKCRzaWRlLmZpbHRlcignLicgKyBjbGFzc05hbWUuYWN0aXZlKVswXSA9PSAkbmV4dFNpZGVbMF0pO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYW5pbWF0aW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkbW9kdWxlLmhhc0NsYXNzKGNsYXNzTmFtZS5hbmltYXRpbmcpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZXQ6IHtcblxuICAgICAgICAgIGRlZmF1bHRTaWRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRhY3RpdmVTaWRlID0gJG1vZHVsZS5maW5kKCcuJyArIHNldHRpbmdzLmNsYXNzTmFtZS5hY3RpdmUpO1xuICAgICAgICAgICAgJG5leHRTaWRlICAgPSAoICRhY3RpdmVTaWRlLm5leHQoc2VsZWN0b3Iuc2lkZSkubGVuZ3RoID4gMCApXG4gICAgICAgICAgICAgID8gJGFjdGl2ZVNpZGUubmV4dChzZWxlY3Rvci5zaWRlKVxuICAgICAgICAgICAgICA6ICRtb2R1bGUuZmluZChzZWxlY3Rvci5zaWRlKS5maXJzdCgpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBuZXh0SW5kZXggPSBmYWxzZTtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdBY3RpdmUgc2lkZSBzZXQgdG8nLCAkYWN0aXZlU2lkZSk7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnTmV4dCBzaWRlIHNldCB0bycsICRuZXh0U2lkZSk7XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIGR1cmF0aW9uOiBmdW5jdGlvbihkdXJhdGlvbikge1xuICAgICAgICAgICAgZHVyYXRpb24gPSBkdXJhdGlvbiB8fCBzZXR0aW5ncy5kdXJhdGlvbjtcbiAgICAgICAgICAgIGR1cmF0aW9uID0gKHR5cGVvZiBkdXJhdGlvbiA9PSAnbnVtYmVyJylcbiAgICAgICAgICAgICAgPyBkdXJhdGlvbiArICdtcydcbiAgICAgICAgICAgICAgOiBkdXJhdGlvblxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1NldHRpbmcgYW5pbWF0aW9uIGR1cmF0aW9uJywgZHVyYXRpb24pO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MuZHVyYXRpb24gfHwgc2V0dGluZ3MuZHVyYXRpb24gPT09IDApIHtcbiAgICAgICAgICAgICAgJHNpZGVzLmFkZCgkc2lkZSlcbiAgICAgICAgICAgICAgICAuY3NzKHtcbiAgICAgICAgICAgICAgICAgICctd2Via2l0LXRyYW5zaXRpb24tZHVyYXRpb24nOiBkdXJhdGlvbixcbiAgICAgICAgICAgICAgICAgICctbW96LXRyYW5zaXRpb24tZHVyYXRpb24nOiBkdXJhdGlvbixcbiAgICAgICAgICAgICAgICAgICctbXMtdHJhbnNpdGlvbi1kdXJhdGlvbic6IGR1cmF0aW9uLFxuICAgICAgICAgICAgICAgICAgJy1vLXRyYW5zaXRpb24tZHVyYXRpb24nOiBkdXJhdGlvbixcbiAgICAgICAgICAgICAgICAgICd0cmFuc2l0aW9uLWR1cmF0aW9uJzogZHVyYXRpb25cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIGN1cnJlbnRTdGFnZVNpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICRhY3RpdmVTaWRlID0gJG1vZHVsZS5maW5kKCcuJyArIHNldHRpbmdzLmNsYXNzTmFtZS5hY3RpdmUpLFxuICAgICAgICAgICAgICB3aWR0aCAgICAgICA9ICRhY3RpdmVTaWRlLm91dGVyV2lkdGgodHJ1ZSksXG4gICAgICAgICAgICAgIGhlaWdodCAgICAgID0gJGFjdGl2ZVNpZGUub3V0ZXJIZWlnaHQodHJ1ZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgLmNzcyh7XG4gICAgICAgICAgICAgICAgd2lkdGg6IHdpZHRoLFxuICAgICAgICAgICAgICAgIGhlaWdodDogaGVpZ2h0XG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIHN0YWdlU2l6ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgJGNsb25lICAgICAgPSAkbW9kdWxlLmNsb25lKCkuYWRkQ2xhc3MoY2xhc3NOYW1lLmxvYWRpbmcpLFxuICAgICAgICAgICAgICAkYWN0aXZlU2lkZSA9ICRjbG9uZS5maW5kKCcuJyArIHNldHRpbmdzLmNsYXNzTmFtZS5hY3RpdmUpLFxuICAgICAgICAgICAgICAkbmV4dFNpZGUgICA9IChuZXh0SW5kZXgpXG4gICAgICAgICAgICAgICAgPyAkY2xvbmUuZmluZChzZWxlY3Rvci5zaWRlKS5lcShuZXh0SW5kZXgpXG4gICAgICAgICAgICAgICAgOiAoICRhY3RpdmVTaWRlLm5leHQoc2VsZWN0b3Iuc2lkZSkubGVuZ3RoID4gMCApXG4gICAgICAgICAgICAgICAgICA/ICRhY3RpdmVTaWRlLm5leHQoc2VsZWN0b3Iuc2lkZSlcbiAgICAgICAgICAgICAgICAgIDogJGNsb25lLmZpbmQoc2VsZWN0b3Iuc2lkZSkuZmlyc3QoKSxcbiAgICAgICAgICAgICAgbmV3V2lkdGggICAgPSAoc2V0dGluZ3Mud2lkdGggPT0gJ25leHQnKVxuICAgICAgICAgICAgICAgID8gJG5leHRTaWRlLm91dGVyV2lkdGgodHJ1ZSlcbiAgICAgICAgICAgICAgICA6IChzZXR0aW5ncy53aWR0aCA9PSAnaW5pdGlhbCcpXG4gICAgICAgICAgICAgICAgICA/ICRtb2R1bGUud2lkdGgoKVxuICAgICAgICAgICAgICAgICAgOiBzZXR0aW5ncy53aWR0aCxcbiAgICAgICAgICAgICAgbmV3SGVpZ2h0ICAgID0gKHNldHRpbmdzLmhlaWdodCA9PSAnbmV4dCcpXG4gICAgICAgICAgICAgICAgPyAkbmV4dFNpZGUub3V0ZXJIZWlnaHQodHJ1ZSlcbiAgICAgICAgICAgICAgICA6IChzZXR0aW5ncy5oZWlnaHQgPT0gJ2luaXRpYWwnKVxuICAgICAgICAgICAgICAgICAgPyAkbW9kdWxlLmhlaWdodCgpXG4gICAgICAgICAgICAgICAgICA6IHNldHRpbmdzLmhlaWdodFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJGFjdGl2ZVNpZGUucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSk7XG4gICAgICAgICAgICAkbmV4dFNpZGUuYWRkQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSk7XG4gICAgICAgICAgICAkY2xvbmUuaW5zZXJ0QWZ0ZXIoJG1vZHVsZSk7XG4gICAgICAgICAgICAkY2xvbmUucmVtb3ZlKCk7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy53aWR0aCAhPSAnYXV0bycpIHtcbiAgICAgICAgICAgICAgJG1vZHVsZS5jc3MoJ3dpZHRoJywgbmV3V2lkdGggKyBzZXR0aW5ncy5qaXR0ZXIpO1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU3BlY2lmeWluZyB3aWR0aCBkdXJpbmcgYW5pbWF0aW9uJywgbmV3V2lkdGgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoc2V0dGluZ3MuaGVpZ2h0ICE9ICdhdXRvJykge1xuICAgICAgICAgICAgICAkbW9kdWxlLmNzcygnaGVpZ2h0JywgbmV3SGVpZ2h0ICsgc2V0dGluZ3Muaml0dGVyKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1NwZWNpZnlpbmcgaGVpZ2h0IGR1cmluZyBhbmltYXRpb24nLCBuZXdIZWlnaHQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG5cbiAgICAgICAgICBuZXh0U2lkZTogZnVuY3Rpb24oc2VsZWN0b3IpIHtcbiAgICAgICAgICAgIG5leHRJbmRleCA9IHNlbGVjdG9yO1xuICAgICAgICAgICAgJG5leHRTaWRlID0gJHNpZGUuZmlsdGVyKHNlbGVjdG9yKTtcbiAgICAgICAgICAgIG5leHRJbmRleCA9ICRzaWRlLmluZGV4KCRuZXh0U2lkZSk7XG4gICAgICAgICAgICBpZigkbmV4dFNpZGUubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5zZXQuZGVmYXVsdFNpZGUoKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLnNpZGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ05leHQgc2lkZSBtYW51YWxseSBzZXQgdG8nLCAkbmV4dFNpZGUpO1xuICAgICAgICAgIH0sXG5cbiAgICAgICAgICBhY3RpdmU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1NldHRpbmcgbmV3IHNpZGUgdG8gYWN0aXZlJywgJG5leHRTaWRlKTtcbiAgICAgICAgICAgICRzaWRlXG4gICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuYWN0aXZlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJG5leHRTaWRlXG4gICAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUuYWN0aXZlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgc2V0dGluZ3Mub25DaGFuZ2UuY2FsbCgkbmV4dFNpZGVbMF0pO1xuICAgICAgICAgICAgbW9kdWxlLnNldC5kZWZhdWx0U2lkZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBmbGlwOiB7XG5cbiAgICAgICAgICB1cDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihtb2R1bGUuaXMuY29tcGxldGUoKSAmJiAhbW9kdWxlLmlzLmFuaW1hdGluZygpICYmICFzZXR0aW5ncy5hbGxvd1JlcGVhdHMpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTaWRlIGFscmVhZHkgdmlzaWJsZScsICRuZXh0U2lkZSk7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCAhbW9kdWxlLmlzLmFuaW1hdGluZygpKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRmxpcHBpbmcgdXAnLCAkbmV4dFNpZGUpO1xuICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm0gPSBtb2R1bGUuZ2V0LnRyYW5zZm9ybS51cCgpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5zdGFnZVNpemUoKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnN0YWdlLmFib3ZlKCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5hbmltYXRlKHRyYW5zZm9ybSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnF1ZXVlKCdmbGlwIHVwJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIGRvd246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmlzLmNvbXBsZXRlKCkgJiYgIW1vZHVsZS5pcy5hbmltYXRpbmcoKSAmJiAhc2V0dGluZ3MuYWxsb3dSZXBlYXRzKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2lkZSBhbHJlYWR5IHZpc2libGUnLCAkbmV4dFNpZGUpO1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiggIW1vZHVsZS5pcy5hbmltYXRpbmcoKSkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0ZsaXBwaW5nIGRvd24nLCAkbmV4dFNpZGUpO1xuICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm0gPSBtb2R1bGUuZ2V0LnRyYW5zZm9ybS5kb3duKClcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0LnN0YWdlU2l6ZSgpO1xuICAgICAgICAgICAgICBtb2R1bGUuc3RhZ2UuYmVsb3coKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmFuaW1hdGUodHJhbnNmb3JtKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUucXVldWUoJ2ZsaXAgZG93bicpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG5cbiAgICAgICAgICBsZWZ0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKG1vZHVsZS5pcy5jb21wbGV0ZSgpICYmICFtb2R1bGUuaXMuYW5pbWF0aW5nKCkgJiYgIXNldHRpbmdzLmFsbG93UmVwZWF0cykge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NpZGUgYWxyZWFkeSB2aXNpYmxlJywgJG5leHRTaWRlKTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoICFtb2R1bGUuaXMuYW5pbWF0aW5nKCkpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdGbGlwcGluZyBsZWZ0JywgJG5leHRTaWRlKTtcbiAgICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtID0gbW9kdWxlLmdldC50cmFuc2Zvcm0ubGVmdCgpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5zdGFnZVNpemUoKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnN0YWdlLmxlZnQoKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmFuaW1hdGUodHJhbnNmb3JtKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUucXVldWUoJ2ZsaXAgbGVmdCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG5cbiAgICAgICAgICByaWdodDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihtb2R1bGUuaXMuY29tcGxldGUoKSAmJiAhbW9kdWxlLmlzLmFuaW1hdGluZygpICYmICFzZXR0aW5ncy5hbGxvd1JlcGVhdHMpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTaWRlIGFscmVhZHkgdmlzaWJsZScsICRuZXh0U2lkZSk7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCAhbW9kdWxlLmlzLmFuaW1hdGluZygpKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRmxpcHBpbmcgcmlnaHQnLCAkbmV4dFNpZGUpO1xuICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm0gPSBtb2R1bGUuZ2V0LnRyYW5zZm9ybS5yaWdodCgpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5zdGFnZVNpemUoKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnN0YWdlLnJpZ2h0KCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5hbmltYXRlKHRyYW5zZm9ybSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnF1ZXVlKCdmbGlwIHJpZ2h0Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIG92ZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmlzLmNvbXBsZXRlKCkgJiYgIW1vZHVsZS5pcy5hbmltYXRpbmcoKSAmJiAhc2V0dGluZ3MuYWxsb3dSZXBlYXRzKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2lkZSBhbHJlYWR5IHZpc2libGUnLCAkbmV4dFNpZGUpO1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiggIW1vZHVsZS5pcy5hbmltYXRpbmcoKSkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0ZsaXBwaW5nIG92ZXInLCAkbmV4dFNpZGUpO1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0LnN0YWdlU2l6ZSgpO1xuICAgICAgICAgICAgICBtb2R1bGUuc3RhZ2UuYmVoaW5kKCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5hbmltYXRlKG1vZHVsZS5nZXQudHJhbnNmb3JtLm92ZXIoKSApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5xdWV1ZSgnZmxpcCBvdmVyJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIGJhY2s6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmlzLmNvbXBsZXRlKCkgJiYgIW1vZHVsZS5pcy5hbmltYXRpbmcoKSAmJiAhc2V0dGluZ3MuYWxsb3dSZXBlYXRzKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2lkZSBhbHJlYWR5IHZpc2libGUnLCAkbmV4dFNpZGUpO1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiggIW1vZHVsZS5pcy5hbmltYXRpbmcoKSkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0ZsaXBwaW5nIGJhY2snLCAkbmV4dFNpZGUpO1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0LnN0YWdlU2l6ZSgpO1xuICAgICAgICAgICAgICBtb2R1bGUuc3RhZ2UuYmVoaW5kKCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5hbmltYXRlKG1vZHVsZS5nZXQudHJhbnNmb3JtLmJhY2soKSApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5xdWV1ZSgnZmxpcCBiYWNrJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuXG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0OiB7XG5cbiAgICAgICAgICB0cmFuc2Zvcm06IHtcbiAgICAgICAgICAgIHVwOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICAgdHJhbnNsYXRlID0ge1xuICAgICAgICAgICAgICAgICAgeTogLSgoJGFjdGl2ZVNpZGUub3V0ZXJIZWlnaHQodHJ1ZSkgLSAkbmV4dFNpZGUub3V0ZXJIZWlnaHQodHJ1ZSkpIC8gMiksXG4gICAgICAgICAgICAgICAgICB6OiAtKCRhY3RpdmVTaWRlLm91dGVySGVpZ2h0KHRydWUpIC8gMilcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVZKCcgKyB0cmFuc2xhdGUueSArICdweCkgdHJhbnNsYXRlWignKyB0cmFuc2xhdGUueiArICdweCkgcm90YXRlWCgtOTBkZWcpJ1xuICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgZG93bjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAgIHRyYW5zbGF0ZSA9IHtcbiAgICAgICAgICAgICAgICAgIHk6IC0oKCRhY3RpdmVTaWRlLm91dGVySGVpZ2h0KHRydWUpIC0gJG5leHRTaWRlLm91dGVySGVpZ2h0KHRydWUpKSAvIDIpLFxuICAgICAgICAgICAgICAgICAgejogLSgkYWN0aXZlU2lkZS5vdXRlckhlaWdodCh0cnVlKSAvIDIpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlWSgnICsgdHJhbnNsYXRlLnkgKyAncHgpIHRyYW5zbGF0ZVooJysgdHJhbnNsYXRlLnogKyAncHgpIHJvdGF0ZVgoOTBkZWcpJ1xuICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgbGVmdDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAgIHRyYW5zbGF0ZSA9IHtcbiAgICAgICAgICAgICAgICAgIHggOiAtKCgkYWN0aXZlU2lkZS5vdXRlcldpZHRoKHRydWUpIC0gJG5leHRTaWRlLm91dGVyV2lkdGgodHJ1ZSkpIC8gMiksXG4gICAgICAgICAgICAgICAgICB6IDogLSgkYWN0aXZlU2lkZS5vdXRlcldpZHRoKHRydWUpIC8gMilcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKCcgKyB0cmFuc2xhdGUueCArICdweCkgdHJhbnNsYXRlWignICsgdHJhbnNsYXRlLnogKyAncHgpIHJvdGF0ZVkoOTBkZWcpJ1xuICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgcmlnaHQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICB0cmFuc2xhdGUgPSB7XG4gICAgICAgICAgICAgICAgICB4IDogLSgoJGFjdGl2ZVNpZGUub3V0ZXJXaWR0aCh0cnVlKSAtICRuZXh0U2lkZS5vdXRlcldpZHRoKHRydWUpKSAvIDIpLFxuICAgICAgICAgICAgICAgICAgeiA6IC0oJGFjdGl2ZVNpZGUub3V0ZXJXaWR0aCh0cnVlKSAvIDIpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlWCgnICsgdHJhbnNsYXRlLnggKyAncHgpIHRyYW5zbGF0ZVooJyArIHRyYW5zbGF0ZS56ICsgJ3B4KSByb3RhdGVZKC05MGRlZyknXG4gICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBvdmVyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICAgdHJhbnNsYXRlID0ge1xuICAgICAgICAgICAgICAgICAgeCA6IC0oKCRhY3RpdmVTaWRlLm91dGVyV2lkdGgodHJ1ZSkgLSAkbmV4dFNpZGUub3V0ZXJXaWR0aCh0cnVlKSkgLyAyKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoJyArIHRyYW5zbGF0ZS54ICsgJ3B4KSByb3RhdGVZKDE4MGRlZyknXG4gICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBiYWNrOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICAgdHJhbnNsYXRlID0ge1xuICAgICAgICAgICAgICAgICAgeCA6IC0oKCRhY3RpdmVTaWRlLm91dGVyV2lkdGgodHJ1ZSkgLSAkbmV4dFNpZGUub3V0ZXJXaWR0aCh0cnVlKSkgLyAyKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoJyArIHRyYW5zbGF0ZS54ICsgJ3B4KSByb3RhdGVZKC0xODBkZWcpJ1xuICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG5cbiAgICAgICAgICB0cmFuc2l0aW9uRXZlbnQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGVsZW1lbnQgICAgID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZWxlbWVudCcpLFxuICAgICAgICAgICAgICB0cmFuc2l0aW9ucyA9IHtcbiAgICAgICAgICAgICAgICAndHJhbnNpdGlvbicgICAgICAgOid0cmFuc2l0aW9uZW5kJyxcbiAgICAgICAgICAgICAgICAnT1RyYW5zaXRpb24nICAgICAgOidvVHJhbnNpdGlvbkVuZCcsXG4gICAgICAgICAgICAgICAgJ01velRyYW5zaXRpb24nICAgIDondHJhbnNpdGlvbmVuZCcsXG4gICAgICAgICAgICAgICAgJ1dlYmtpdFRyYW5zaXRpb24nIDond2Via2l0VHJhbnNpdGlvbkVuZCdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgdHJhbnNpdGlvblxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgZm9yKHRyYW5zaXRpb24gaW4gdHJhbnNpdGlvbnMpe1xuICAgICAgICAgICAgICBpZiggZWxlbWVudC5zdHlsZVt0cmFuc2l0aW9uXSAhPT0gdW5kZWZpbmVkICl7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRyYW5zaXRpb25zW3RyYW5zaXRpb25dO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIG5leHRTaWRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAoICRhY3RpdmVTaWRlLm5leHQoc2VsZWN0b3Iuc2lkZSkubGVuZ3RoID4gMCApXG4gICAgICAgICAgICAgID8gJGFjdGl2ZVNpZGUubmV4dChzZWxlY3Rvci5zaWRlKVxuICAgICAgICAgICAgICA6ICRtb2R1bGUuZmluZChzZWxlY3Rvci5zaWRlKS5maXJzdCgpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuXG4gICAgICAgIH0sXG5cbiAgICAgICAgc3RhZ2U6IHtcblxuICAgICAgICAgIGFib3ZlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBib3ggPSB7XG4gICAgICAgICAgICAgICAgb3JpZ2luIDogKCgkYWN0aXZlU2lkZS5vdXRlckhlaWdodCh0cnVlKSAtICRuZXh0U2lkZS5vdXRlckhlaWdodCh0cnVlKSkgLyAyKSxcbiAgICAgICAgICAgICAgICBkZXB0aCAgOiB7XG4gICAgICAgICAgICAgICAgICBhY3RpdmUgOiAoJG5leHRTaWRlLm91dGVySGVpZ2h0KHRydWUpIC8gMiksXG4gICAgICAgICAgICAgICAgICBuZXh0ICAgOiAoJGFjdGl2ZVNpZGUub3V0ZXJIZWlnaHQodHJ1ZSkgLyAyKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1NldHRpbmcgdGhlIGluaXRpYWwgYW5pbWF0aW9uIHBvc2l0aW9uIGFzIGFib3ZlJywgJG5leHRTaWRlLCBib3gpO1xuICAgICAgICAgICAgJHNpZGVzXG4gICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICd0cmFuc2Zvcm0nIDogJ3RyYW5zbGF0ZVooLScgKyBib3guZGVwdGguYWN0aXZlICsgJ3B4KSdcbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgICRhY3RpdmVTaWRlXG4gICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICd0cmFuc2Zvcm0nIDogJ3JvdGF0ZVkoMGRlZykgdHJhbnNsYXRlWignICsgYm94LmRlcHRoLmFjdGl2ZSArICdweCknXG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICAkbmV4dFNpZGVcbiAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5hbmltYXRpbmcpXG4gICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICd0b3AnICAgICAgIDogYm94Lm9yaWdpbiArICdweCcsXG4gICAgICAgICAgICAgICAgJ3RyYW5zZm9ybScgOiAncm90YXRlWCg5MGRlZykgdHJhbnNsYXRlWignICsgYm94LmRlcHRoLm5leHQgKyAncHgpJ1xuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG5cbiAgICAgICAgICBiZWxvdzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgYm94ID0ge1xuICAgICAgICAgICAgICAgIG9yaWdpbiA6ICgoJGFjdGl2ZVNpZGUub3V0ZXJIZWlnaHQodHJ1ZSkgLSAkbmV4dFNpZGUub3V0ZXJIZWlnaHQodHJ1ZSkpIC8gMiksXG4gICAgICAgICAgICAgICAgZGVwdGggIDoge1xuICAgICAgICAgICAgICAgICAgYWN0aXZlIDogKCRuZXh0U2lkZS5vdXRlckhlaWdodCh0cnVlKSAvIDIpLFxuICAgICAgICAgICAgICAgICAgbmV4dCAgIDogKCRhY3RpdmVTaWRlLm91dGVySGVpZ2h0KHRydWUpIC8gMilcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIHRoZSBpbml0aWFsIGFuaW1hdGlvbiBwb3NpdGlvbiBhcyBiZWxvdycsICRuZXh0U2lkZSwgYm94KTtcbiAgICAgICAgICAgICRzaWRlc1xuICAgICAgICAgICAgICAuY3NzKHtcbiAgICAgICAgICAgICAgICAndHJhbnNmb3JtJyA6ICd0cmFuc2xhdGVaKC0nICsgYm94LmRlcHRoLmFjdGl2ZSArICdweCknXG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICAkYWN0aXZlU2lkZVxuICAgICAgICAgICAgICAuY3NzKHtcbiAgICAgICAgICAgICAgICAndHJhbnNmb3JtJyA6ICdyb3RhdGVZKDBkZWcpIHRyYW5zbGF0ZVooJyArIGJveC5kZXB0aC5hY3RpdmUgKyAncHgpJ1xuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJG5leHRTaWRlXG4gICAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUuYW5pbWF0aW5nKVxuICAgICAgICAgICAgICAuY3NzKHtcbiAgICAgICAgICAgICAgICAndG9wJyAgICAgICA6IGJveC5vcmlnaW4gKyAncHgnLFxuICAgICAgICAgICAgICAgICd0cmFuc2Zvcm0nIDogJ3JvdGF0ZVgoLTkwZGVnKSB0cmFuc2xhdGVaKCcgKyBib3guZGVwdGgubmV4dCArICdweCknXG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIGxlZnQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGhlaWdodCA9IHtcbiAgICAgICAgICAgICAgICBhY3RpdmUgOiAkYWN0aXZlU2lkZS5vdXRlcldpZHRoKHRydWUpLFxuICAgICAgICAgICAgICAgIG5leHQgICA6ICRuZXh0U2lkZS5vdXRlcldpZHRoKHRydWUpXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGJveCA9IHtcbiAgICAgICAgICAgICAgICBvcmlnaW4gOiAoICggaGVpZ2h0LmFjdGl2ZSAtIGhlaWdodC5uZXh0ICkgLyAyKSxcbiAgICAgICAgICAgICAgICBkZXB0aCAgOiB7XG4gICAgICAgICAgICAgICAgICBhY3RpdmUgOiAoaGVpZ2h0Lm5leHQgLyAyKSxcbiAgICAgICAgICAgICAgICAgIG5leHQgICA6IChoZWlnaHQuYWN0aXZlIC8gMilcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIHRoZSBpbml0aWFsIGFuaW1hdGlvbiBwb3NpdGlvbiBhcyBsZWZ0JywgJG5leHRTaWRlLCBib3gpO1xuICAgICAgICAgICAgJHNpZGVzXG4gICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICd0cmFuc2Zvcm0nIDogJ3RyYW5zbGF0ZVooLScgKyBib3guZGVwdGguYWN0aXZlICsgJ3B4KSdcbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgICRhY3RpdmVTaWRlXG4gICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICd0cmFuc2Zvcm0nIDogJ3JvdGF0ZVkoMGRlZykgdHJhbnNsYXRlWignICsgYm94LmRlcHRoLmFjdGl2ZSArICdweCknXG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICAkbmV4dFNpZGVcbiAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5hbmltYXRpbmcpXG4gICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICdsZWZ0JyAgICAgIDogYm94Lm9yaWdpbiArICdweCcsXG4gICAgICAgICAgICAgICAgJ3RyYW5zZm9ybScgOiAncm90YXRlWSgtOTBkZWcpIHRyYW5zbGF0ZVooJyArIGJveC5kZXB0aC5uZXh0ICsgJ3B4KSdcbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuXG4gICAgICAgICAgcmlnaHQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGhlaWdodCA9IHtcbiAgICAgICAgICAgICAgICBhY3RpdmUgOiAkYWN0aXZlU2lkZS5vdXRlcldpZHRoKHRydWUpLFxuICAgICAgICAgICAgICAgIG5leHQgICA6ICRuZXh0U2lkZS5vdXRlcldpZHRoKHRydWUpXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGJveCA9IHtcbiAgICAgICAgICAgICAgICBvcmlnaW4gOiAoICggaGVpZ2h0LmFjdGl2ZSAtIGhlaWdodC5uZXh0ICkgLyAyKSxcbiAgICAgICAgICAgICAgICBkZXB0aCAgOiB7XG4gICAgICAgICAgICAgICAgICBhY3RpdmUgOiAoaGVpZ2h0Lm5leHQgLyAyKSxcbiAgICAgICAgICAgICAgICAgIG5leHQgICA6IChoZWlnaHQuYWN0aXZlIC8gMilcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIHRoZSBpbml0aWFsIGFuaW1hdGlvbiBwb3NpdGlvbiBhcyBsZWZ0JywgJG5leHRTaWRlLCBib3gpO1xuICAgICAgICAgICAgJHNpZGVzXG4gICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICd0cmFuc2Zvcm0nIDogJ3RyYW5zbGF0ZVooLScgKyBib3guZGVwdGguYWN0aXZlICsgJ3B4KSdcbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgICRhY3RpdmVTaWRlXG4gICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICd0cmFuc2Zvcm0nIDogJ3JvdGF0ZVkoMGRlZykgdHJhbnNsYXRlWignICsgYm94LmRlcHRoLmFjdGl2ZSArICdweCknXG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICAkbmV4dFNpZGVcbiAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5hbmltYXRpbmcpXG4gICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICdsZWZ0JyAgICAgIDogYm94Lm9yaWdpbiArICdweCcsXG4gICAgICAgICAgICAgICAgJ3RyYW5zZm9ybScgOiAncm90YXRlWSg5MGRlZykgdHJhbnNsYXRlWignICsgYm94LmRlcHRoLm5leHQgKyAncHgpJ1xuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG5cbiAgICAgICAgICBiZWhpbmQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGhlaWdodCA9IHtcbiAgICAgICAgICAgICAgICBhY3RpdmUgOiAkYWN0aXZlU2lkZS5vdXRlcldpZHRoKHRydWUpLFxuICAgICAgICAgICAgICAgIG5leHQgICA6ICRuZXh0U2lkZS5vdXRlcldpZHRoKHRydWUpXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGJveCA9IHtcbiAgICAgICAgICAgICAgICBvcmlnaW4gOiAoICggaGVpZ2h0LmFjdGl2ZSAtIGhlaWdodC5uZXh0ICkgLyAyKSxcbiAgICAgICAgICAgICAgICBkZXB0aCAgOiB7XG4gICAgICAgICAgICAgICAgICBhY3RpdmUgOiAoaGVpZ2h0Lm5leHQgLyAyKSxcbiAgICAgICAgICAgICAgICAgIG5leHQgICA6IChoZWlnaHQuYWN0aXZlIC8gMilcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIHRoZSBpbml0aWFsIGFuaW1hdGlvbiBwb3NpdGlvbiBhcyBiZWhpbmQnLCAkbmV4dFNpZGUsIGJveCk7XG4gICAgICAgICAgICAkYWN0aXZlU2lkZVxuICAgICAgICAgICAgICAuY3NzKHtcbiAgICAgICAgICAgICAgICAndHJhbnNmb3JtJyA6ICdyb3RhdGVZKDBkZWcpJ1xuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJG5leHRTaWRlXG4gICAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUuYW5pbWF0aW5nKVxuICAgICAgICAgICAgICAuY3NzKHtcbiAgICAgICAgICAgICAgICAnbGVmdCcgICAgICA6IGJveC5vcmlnaW4gKyAncHgnLFxuICAgICAgICAgICAgICAgICd0cmFuc2Zvcm0nIDogJ3JvdGF0ZVkoLTE4MGRlZyknXG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBzZXR0aW5nOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ2hhbmdpbmcgc2V0dGluZycsIG5hbWUsIHZhbHVlKTtcbiAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgc2V0dGluZ3MsIG5hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGlmKCQuaXNQbGFpbk9iamVjdChzZXR0aW5nc1tuYW1lXSkpIHtcbiAgICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgc2V0dGluZ3NbbmFtZV0sIHZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBzZXR0aW5nc1tuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBzZXR0aW5nc1tuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludGVybmFsOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBtb2R1bGUsIG5hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIG1vZHVsZVtuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGVbbmFtZV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBkZWJ1ZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy5kZWJ1Zykge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZyA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB2ZXJib3NlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50ICYmIHNldHRpbmdzLnZlcmJvc2UgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGVycm9yOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50KSB7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuZXJyb3IsIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBwZXJmb3JtYW5jZToge1xuICAgICAgICAgIGxvZzogZnVuY3Rpb24obWVzc2FnZSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lLFxuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lLFxuICAgICAgICAgICAgICBwcmV2aW91c1RpbWVcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lICAgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lICA9IHRpbWUgfHwgY3VycmVudFRpbWU7XG4gICAgICAgICAgICAgIGV4ZWN1dGlvblRpbWUgPSBjdXJyZW50VGltZSAtIHByZXZpb3VzVGltZTtcbiAgICAgICAgICAgICAgdGltZSAgICAgICAgICA9IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBwZXJmb3JtYW5jZS5wdXNoKHtcbiAgICAgICAgICAgICAgICAnTmFtZScgICAgICAgICAgIDogbWVzc2FnZVswXSxcbiAgICAgICAgICAgICAgICAnQXJndW1lbnRzJyAgICAgIDogW10uc2xpY2UuY2FsbChtZXNzYWdlLCAxKSB8fCAnJyxcbiAgICAgICAgICAgICAgICAnRWxlbWVudCcgICAgICAgIDogZWxlbWVudCxcbiAgICAgICAgICAgICAgICAnRXhlY3V0aW9uIFRpbWUnIDogZXhlY3V0aW9uVGltZVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyID0gc2V0VGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UuZGlzcGxheSwgNTAwKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc3BsYXk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHRpdGxlID0gc2V0dGluZ3MubmFtZSArICc6JyxcbiAgICAgICAgICAgICAgdG90YWxUaW1lID0gMFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgdGltZSA9IGZhbHNlO1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lcik7XG4gICAgICAgICAgICAkLmVhY2gocGVyZm9ybWFuY2UsIGZ1bmN0aW9uKGluZGV4LCBkYXRhKSB7XG4gICAgICAgICAgICAgIHRvdGFsVGltZSArPSBkYXRhWydFeGVjdXRpb24gVGltZSddO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB0aXRsZSArPSAnICcgKyB0b3RhbFRpbWUgKyAnbXMnO1xuICAgICAgICAgICAgaWYobW9kdWxlU2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgdGl0bGUgKz0gJyBcXCcnICsgbW9kdWxlU2VsZWN0b3IgKyAnXFwnJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCRhbGxNb2R1bGVzLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgICAgICAgdGl0bGUgKz0gJyAnICsgJygnICsgJGFsbE1vZHVsZXMubGVuZ3RoICsgJyknO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoIChjb25zb2xlLmdyb3VwICE9PSB1bmRlZmluZWQgfHwgY29uc29sZS50YWJsZSAhPT0gdW5kZWZpbmVkKSAmJiBwZXJmb3JtYW5jZS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBDb2xsYXBzZWQodGl0bGUpO1xuICAgICAgICAgICAgICBpZihjb25zb2xlLnRhYmxlKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS50YWJsZShwZXJmb3JtYW5jZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YVsnTmFtZSddICsgJzogJyArIGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ10rJ21zJyk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgY29uc29sZS5ncm91cEVuZCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcGVyZm9ybWFuY2UgPSBbXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludm9rZTogZnVuY3Rpb24ocXVlcnksIHBhc3NlZEFyZ3VtZW50cywgY29udGV4dCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgb2JqZWN0ID0gaW5zdGFuY2UsXG4gICAgICAgICAgICBtYXhEZXB0aCxcbiAgICAgICAgICAgIGZvdW5kLFxuICAgICAgICAgICAgcmVzcG9uc2VcbiAgICAgICAgICA7XG4gICAgICAgICAgcGFzc2VkQXJndW1lbnRzID0gcGFzc2VkQXJndW1lbnRzIHx8IHF1ZXJ5QXJndW1lbnRzO1xuICAgICAgICAgIGNvbnRleHQgICAgICAgICA9IGVsZW1lbnQgICAgICAgICB8fCBjb250ZXh0O1xuICAgICAgICAgIGlmKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyAmJiBvYmplY3QgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnkgICAgPSBxdWVyeS5zcGxpdCgvW1xcLiBdLyk7XG4gICAgICAgICAgICBtYXhEZXB0aCA9IHF1ZXJ5Lmxlbmd0aCAtIDE7XG4gICAgICAgICAgICAkLmVhY2gocXVlcnksIGZ1bmN0aW9uKGRlcHRoLCB2YWx1ZSkge1xuICAgICAgICAgICAgICB2YXIgY2FtZWxDYXNlVmFsdWUgPSAoZGVwdGggIT0gbWF4RGVwdGgpXG4gICAgICAgICAgICAgICAgPyB2YWx1ZSArIHF1ZXJ5W2RlcHRoICsgMV0uY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBxdWVyeVtkZXB0aCArIDFdLnNsaWNlKDEpXG4gICAgICAgICAgICAgICAgOiBxdWVyeVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gKSAmJiAoZGVwdGggIT0gbWF4RGVwdGgpICkge1xuICAgICAgICAgICAgICAgIG9iamVjdCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgICAgIGZvdW5kID0gb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbdmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFt2YWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFt2YWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmICggJC5pc0Z1bmN0aW9uKCBmb3VuZCApICkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZC5hcHBseShjb250ZXh0LCBwYXNzZWRBcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKGZvdW5kICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gZm91bmQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKCQuaXNBcnJheShyZXR1cm5lZFZhbHVlKSkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZS5wdXNoKHJlc3BvbnNlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSBbcmV0dXJuZWRWYWx1ZSwgcmVzcG9uc2VdO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJlc3BvbnNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSByZXNwb25zZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGZvdW5kO1xuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICBpZihtZXRob2RJbnZva2VkKSB7XG4gICAgICAgIGlmKGluc3RhbmNlID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbnZva2UocXVlcnkpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGlmKGluc3RhbmNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBpbnN0YW5jZS5pbnZva2UoJ2Rlc3Ryb3knKTtcbiAgICAgICAgfVxuICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgfVxuICAgIH0pXG4gIDtcblxuICByZXR1cm4gKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZClcbiAgICA/IHJldHVybmVkVmFsdWVcbiAgICA6IHRoaXNcbiAgO1xufTtcblxuJC5mbi5zaGFwZS5zZXR0aW5ncyA9IHtcblxuICAvLyBtb2R1bGUgaW5mb1xuICBuYW1lIDogJ1NoYXBlJyxcblxuICAvLyBoaWRlIGFsbCBkZWJ1ZyBjb250ZW50XG4gIHNpbGVudCAgICAgOiBmYWxzZSxcblxuICAvLyBkZWJ1ZyBjb250ZW50IG91dHB1dHRlZCB0byBjb25zb2xlXG4gIGRlYnVnICAgICAgOiBmYWxzZSxcblxuICAvLyB2ZXJib3NlIGRlYnVnIG91dHB1dFxuICB2ZXJib3NlICAgIDogZmFsc2UsXG5cbiAgLy8gZnVkZ2UgZmFjdG9yIGluIHBpeGVscyB3aGVuIHN3YXBwaW5nIGZyb20gMmQgdG8gM2QgKGNhbiBiZSB1c2VmdWwgdG8gY29ycmVjdCByb3VuZGluZyBlcnJvcnMpXG4gIGppdHRlciAgICAgOiAwLFxuXG4gIC8vIHBlcmZvcm1hbmNlIGRhdGEgb3V0cHV0XG4gIHBlcmZvcm1hbmNlOiB0cnVlLFxuXG4gIC8vIGV2ZW50IG5hbWVzcGFjZVxuICBuYW1lc3BhY2UgIDogJ3NoYXBlJyxcblxuICAvLyB3aWR0aCBkdXJpbmcgYW5pbWF0aW9uLCBjYW4gYmUgc2V0IHRvICdhdXRvJywgaW5pdGlhbCcsICduZXh0JyBvciBwaXhlbCBhbW91bnRcbiAgd2lkdGg6ICdpbml0aWFsJyxcblxuICAvLyBoZWlnaHQgZHVyaW5nIGFuaW1hdGlvbiwgY2FuIGJlIHNldCB0byAnYXV0bycsICdpbml0aWFsJywgJ25leHQnIG9yIHBpeGVsIGFtb3VudFxuICBoZWlnaHQ6ICdpbml0aWFsJyxcblxuICAvLyBjYWxsYmFjayBvY2N1cnMgb24gc2lkZSBjaGFuZ2VcbiAgYmVmb3JlQ2hhbmdlIDogZnVuY3Rpb24oKSB7fSxcbiAgb25DaGFuZ2UgICAgIDogZnVuY3Rpb24oKSB7fSxcblxuICAvLyBhbGxvdyBhbmltYXRpb24gdG8gc2FtZSBzaWRlXG4gIGFsbG93UmVwZWF0czogZmFsc2UsXG5cbiAgLy8gYW5pbWF0aW9uIGR1cmF0aW9uXG4gIGR1cmF0aW9uICAgOiBmYWxzZSxcblxuICAvLyBwb3NzaWJsZSBlcnJvcnNcbiAgZXJyb3I6IHtcbiAgICBzaWRlICAgOiAnWW91IHRyaWVkIHRvIHN3aXRjaCB0byBhIHNpZGUgdGhhdCBkb2VzIG5vdCBleGlzdC4nLFxuICAgIG1ldGhvZCA6ICdUaGUgbWV0aG9kIHlvdSBjYWxsZWQgaXMgbm90IGRlZmluZWQnXG4gIH0sXG5cbiAgLy8gY2xhc3NuYW1lcyB1c2VkXG4gIGNsYXNzTmFtZSAgIDoge1xuICAgIGFuaW1hdGluZyA6ICdhbmltYXRpbmcnLFxuICAgIGhpZGRlbiAgICA6ICdoaWRkZW4nLFxuICAgIGxvYWRpbmcgICA6ICdsb2FkaW5nJyxcbiAgICBhY3RpdmUgICAgOiAnYWN0aXZlJ1xuICB9LFxuXG4gIC8vIHNlbGVjdG9ycyB1c2VkXG4gIHNlbGVjdG9yICAgIDoge1xuICAgIHNpZGVzIDogJy5zaWRlcycsXG4gICAgc2lkZSAgOiAnLnNpZGUnXG4gIH1cblxufTtcblxuXG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7XG4iXX0=