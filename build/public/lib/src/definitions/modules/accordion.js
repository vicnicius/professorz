/*!
 * # Semantic UI - Accordion
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.accordion = function (parameters) {
    var $allModules = $(this),
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
      setTimeout(callback, 0);
    },
        returnedValue;
    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.accordion.settings, parameters) : $.extend({}, $.fn.accordion.settings),
          className = settings.className,
          namespace = settings.namespace,
          selector = settings.selector,
          error = settings.error,
          eventNamespace = '.' + namespace,
          moduleNamespace = 'module-' + namespace,
          moduleSelector = $allModules.selector || '',
          $module = $(this),
          $title = $module.find(selector.title),
          $content = $module.find(selector.content),
          element = this,
          instance = $module.data(moduleNamespace),
          observer,
          module;

      module = {

        initialize: function () {
          module.debug('Initializing', $module);
          module.bind.events();
          if (settings.observeChanges) {
            module.observeChanges();
          }
          module.instantiate();
        },

        instantiate: function () {
          instance = module;
          $module.data(moduleNamespace, module);
        },

        destroy: function () {
          module.debug('Destroying previous instance', $module);
          $module.off(eventNamespace).removeData(moduleNamespace);
        },

        refresh: function () {
          $title = $module.find(selector.title);
          $content = $module.find(selector.content);
        },

        observeChanges: function () {
          if ('MutationObserver' in window) {
            observer = new MutationObserver(function (mutations) {
              module.debug('DOM tree modified, updating selector cache');
              module.refresh();
            });
            observer.observe(element, {
              childList: true,
              subtree: true
            });
            module.debug('Setting up mutation observer', observer);
          }
        },

        bind: {
          events: function () {
            module.debug('Binding delegated events');
            $module.on(settings.on + eventNamespace, selector.trigger, module.event.click);
          }
        },

        event: {
          click: function () {
            module.toggle.call(this);
          }
        },

        toggle: function (query) {
          var $activeTitle = query !== undefined ? typeof query === 'number' ? $title.eq(query) : $(query).closest(selector.title) : $(this).closest(selector.title),
              $activeContent = $activeTitle.next($content),
              isAnimating = $activeContent.hasClass(className.animating),
              isActive = $activeContent.hasClass(className.active),
              isOpen = isActive && !isAnimating,
              isOpening = !isActive && isAnimating;
          module.debug('Toggling visibility of content', $activeTitle);
          if (isOpen || isOpening) {
            if (settings.collapsible) {
              module.close.call($activeTitle);
            } else {
              module.debug('Cannot close accordion content collapsing is disabled');
            }
          } else {
            module.open.call($activeTitle);
          }
        },

        open: function (query) {
          var $activeTitle = query !== undefined ? typeof query === 'number' ? $title.eq(query) : $(query).closest(selector.title) : $(this).closest(selector.title),
              $activeContent = $activeTitle.next($content),
              isAnimating = $activeContent.hasClass(className.animating),
              isActive = $activeContent.hasClass(className.active),
              isOpen = isActive || isAnimating;
          if (isOpen) {
            module.debug('Accordion already open, skipping', $activeContent);
            return;
          }
          module.debug('Opening accordion content', $activeTitle);
          settings.onOpening.call($activeContent);
          if (settings.exclusive) {
            module.closeOthers.call($activeTitle);
          }
          $activeTitle.addClass(className.active);
          $activeContent.stop(true, true).addClass(className.animating);
          if (settings.animateChildren) {
            if ($.fn.transition !== undefined && $module.transition('is supported')) {
              $activeContent.children().transition({
                animation: 'fade in',
                queue: false,
                useFailSafe: true,
                debug: settings.debug,
                verbose: settings.verbose,
                duration: settings.duration
              });
            } else {
              $activeContent.children().stop(true, true).animate({
                opacity: 1
              }, settings.duration, module.resetOpacity);
            }
          }
          $activeContent.slideDown(settings.duration, settings.easing, function () {
            $activeContent.removeClass(className.animating).addClass(className.active);
            module.reset.display.call(this);
            settings.onOpen.call(this);
            settings.onChange.call(this);
          });
        },

        close: function (query) {
          var $activeTitle = query !== undefined ? typeof query === 'number' ? $title.eq(query) : $(query).closest(selector.title) : $(this).closest(selector.title),
              $activeContent = $activeTitle.next($content),
              isAnimating = $activeContent.hasClass(className.animating),
              isActive = $activeContent.hasClass(className.active),
              isOpening = !isActive && isAnimating,
              isClosing = isActive && isAnimating;
          if ((isActive || isOpening) && !isClosing) {
            module.debug('Closing accordion content', $activeContent);
            settings.onClosing.call($activeContent);
            $activeTitle.removeClass(className.active);
            $activeContent.stop(true, true).addClass(className.animating);
            if (settings.animateChildren) {
              if ($.fn.transition !== undefined && $module.transition('is supported')) {
                $activeContent.children().transition({
                  animation: 'fade out',
                  queue: false,
                  useFailSafe: true,
                  debug: settings.debug,
                  verbose: settings.verbose,
                  duration: settings.duration
                });
              } else {
                $activeContent.children().stop(true, true).animate({
                  opacity: 0
                }, settings.duration, module.resetOpacity);
              }
            }
            $activeContent.slideUp(settings.duration, settings.easing, function () {
              $activeContent.removeClass(className.animating).removeClass(className.active);
              module.reset.display.call(this);
              settings.onClose.call(this);
              settings.onChange.call(this);
            });
          }
        },

        closeOthers: function (index) {
          var $activeTitle = index !== undefined ? $title.eq(index) : $(this).closest(selector.title),
              $parentTitles = $activeTitle.parents(selector.content).prev(selector.title),
              $activeAccordion = $activeTitle.closest(selector.accordion),
              activeSelector = selector.title + '.' + className.active + ':visible',
              activeContent = selector.content + '.' + className.active + ':visible',
              $openTitles,
              $nestedTitles,
              $openContents;
          if (settings.closeNested) {
            $openTitles = $activeAccordion.find(activeSelector).not($parentTitles);
            $openContents = $openTitles.next($content);
          } else {
            $openTitles = $activeAccordion.find(activeSelector).not($parentTitles);
            $nestedTitles = $activeAccordion.find(activeContent).find(activeSelector).not($parentTitles);
            $openTitles = $openTitles.not($nestedTitles);
            $openContents = $openTitles.next($content);
          }
          if ($openTitles.length > 0) {
            module.debug('Exclusive enabled, closing other content', $openTitles);
            $openTitles.removeClass(className.active);
            $openContents.removeClass(className.animating).stop(true, true);
            if (settings.animateChildren) {
              if ($.fn.transition !== undefined && $module.transition('is supported')) {
                $openContents.children().transition({
                  animation: 'fade out',
                  useFailSafe: true,
                  debug: settings.debug,
                  verbose: settings.verbose,
                  duration: settings.duration
                });
              } else {
                $openContents.children().stop(true, true).animate({
                  opacity: 0
                }, settings.duration, module.resetOpacity);
              }
            }
            $openContents.slideUp(settings.duration, settings.easing, function () {
              $(this).removeClass(className.active);
              module.reset.display.call(this);
            });
          }
        },

        reset: {

          display: function () {
            module.verbose('Removing inline display from element', this);
            $(this).css('display', '');
            if ($(this).attr('style') === '') {
              $(this).attr('style', '').removeAttr('style');
            }
          },

          opacity: function () {
            module.verbose('Removing inline opacity from element', this);
            $(this).css('opacity', '');
            if ($(this).attr('style') === '') {
              $(this).attr('style', '').removeAttr('style');
            }
          }

        },

        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          module.debug('Changing internal', name, value);
          if (value !== undefined) {
            if ($.isPlainObject(name)) {
              $.extend(true, module, name);
            } else {
              module[name] = value;
            }
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                module.error(error.method, query);
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };
      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });
    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.accordion.settings = {

    name: 'Accordion',
    namespace: 'accordion',

    silent: false,
    debug: false,
    verbose: false,
    performance: true,

    on: 'click', // event on title that opens accordion

    observeChanges: true, // whether accordion should automatically refresh on DOM insertion

    exclusive: true, // whether a single accordion content panel should be open at once
    collapsible: true, // whether accordion content can be closed
    closeNested: false, // whether nested content should be closed when a panel is closed
    animateChildren: true, // whether children opacity should be animated

    duration: 350, // duration of animation
    easing: 'easeOutQuad', // easing equation for animation

    onOpening: function () {}, // callback before open animation
    onOpen: function () {}, // callback after open animation
    onClosing: function () {}, // callback before closing animation
    onClose: function () {}, // callback after closing animation
    onChange: function () {}, // callback after closing or opening animation

    error: {
      method: 'The method you called is not defined'
    },

    className: {
      active: 'active',
      animating: 'animating'
    },

    selector: {
      accordion: '.accordion',
      title: '.title',
      trigger: '.title',
      content: '.content'
    }

  };

  // Adds easing
  $.extend($.easing, {
    easeOutQuad: function (x, t, b, c, d) {
      return -c * (t /= d) * (t - 2) + b;
    }
  });
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL2FjY29yZGlvbi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBVUEsQ0FBQyxDQUFDLFVBQVUsQ0FBVixFQUFhLE1BQWIsRUFBcUIsUUFBckIsRUFBK0IsU0FBL0IsRUFBMEM7O0FBRTVDOztBQUVBLFdBQVUsT0FBTyxNQUFQLElBQWlCLFdBQWpCLElBQWdDLE9BQU8sSUFBUCxJQUFlLElBQWhELEdBQ0wsTUFESyxHQUVKLE9BQU8sSUFBUCxJQUFlLFdBQWYsSUFBOEIsS0FBSyxJQUFMLElBQWEsSUFBNUMsR0FDRSxJQURGLEdBRUUsU0FBUyxhQUFULEdBSk47O0FBT0EsSUFBRSxFQUFGLENBQUssU0FBTCxHQUFpQixVQUFTLFVBQVQsRUFBcUI7QUFDcEMsUUFDRSxjQUFrQixFQUFFLElBQUYsQ0FEcEI7QUFBQSxRQUdFLE9BQWtCLElBQUksSUFBSixHQUFXLE9BQVgsRUFIcEI7QUFBQSxRQUlFLGNBQWtCLEVBSnBCO0FBQUEsUUFNRSxRQUFrQixVQUFVLENBQVYsQ0FOcEI7QUFBQSxRQU9FLGdCQUFtQixPQUFPLEtBQVAsSUFBZ0IsUUFQckM7QUFBQSxRQVFFLGlCQUFrQixHQUFHLEtBQUgsQ0FBUyxJQUFULENBQWMsU0FBZCxFQUF5QixDQUF6QixDQVJwQjtBQUFBLFFBVUUsd0JBQXdCLE9BQU8scUJBQVAsSUFDbkIsT0FBTyx3QkFEWSxJQUVuQixPQUFPLDJCQUZZLElBR25CLE9BQU8sdUJBSFksSUFJbkIsVUFBUyxRQUFULEVBQW1CO0FBQUUsaUJBQVcsUUFBWCxFQUFxQixDQUFyQjtBQUEwQixLQWR0RDtBQUFBLFFBZ0JFLGFBaEJGO0FBa0JBLGdCQUNHLElBREgsQ0FDUSxZQUFXO0FBQ2YsVUFDRSxXQUFvQixFQUFFLGFBQUYsQ0FBZ0IsVUFBaEIsQ0FBRixHQUNkLEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLEVBQUUsRUFBRixDQUFLLFNBQUwsQ0FBZSxRQUFsQyxFQUE0QyxVQUE1QyxDQURjLEdBRWQsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLEVBQUUsRUFBRixDQUFLLFNBQUwsQ0FBZSxRQUE1QixDQUhOO0FBQUEsVUFLRSxZQUFrQixTQUFTLFNBTDdCO0FBQUEsVUFNRSxZQUFrQixTQUFTLFNBTjdCO0FBQUEsVUFPRSxXQUFrQixTQUFTLFFBUDdCO0FBQUEsVUFRRSxRQUFrQixTQUFTLEtBUjdCO0FBQUEsVUFVRSxpQkFBa0IsTUFBTSxTQVYxQjtBQUFBLFVBV0Usa0JBQWtCLFlBQVksU0FYaEM7QUFBQSxVQVlFLGlCQUFrQixZQUFZLFFBQVosSUFBd0IsRUFaNUM7QUFBQSxVQWNFLFVBQVcsRUFBRSxJQUFGLENBZGI7QUFBQSxVQWVFLFNBQVcsUUFBUSxJQUFSLENBQWEsU0FBUyxLQUF0QixDQWZiO0FBQUEsVUFnQkUsV0FBVyxRQUFRLElBQVIsQ0FBYSxTQUFTLE9BQXRCLENBaEJiO0FBQUEsVUFrQkUsVUFBVyxJQWxCYjtBQUFBLFVBbUJFLFdBQVcsUUFBUSxJQUFSLENBQWEsZUFBYixDQW5CYjtBQUFBLFVBb0JFLFFBcEJGO0FBQUEsVUFxQkUsTUFyQkY7O0FBd0JBLGVBQVM7O0FBRVAsb0JBQVksWUFBVztBQUNyQixpQkFBTyxLQUFQLENBQWEsY0FBYixFQUE2QixPQUE3QjtBQUNBLGlCQUFPLElBQVAsQ0FBWSxNQUFaO0FBQ0EsY0FBRyxTQUFTLGNBQVosRUFBNEI7QUFDMUIsbUJBQU8sY0FBUDtBQUNEO0FBQ0QsaUJBQU8sV0FBUDtBQUNELFNBVE07O0FBV1AscUJBQWEsWUFBVztBQUN0QixxQkFBVyxNQUFYO0FBQ0Esa0JBQ0csSUFESCxDQUNRLGVBRFIsRUFDeUIsTUFEekI7QUFHRCxTQWhCTTs7QUFrQlAsaUJBQVMsWUFBVztBQUNsQixpQkFBTyxLQUFQLENBQWEsOEJBQWIsRUFBNkMsT0FBN0M7QUFDQSxrQkFDRyxHQURILENBQ08sY0FEUCxFQUVHLFVBRkgsQ0FFYyxlQUZkO0FBSUQsU0F4Qk07O0FBMEJQLGlCQUFTLFlBQVc7QUFDbEIsbUJBQVcsUUFBUSxJQUFSLENBQWEsU0FBUyxLQUF0QixDQUFYO0FBQ0EscUJBQVcsUUFBUSxJQUFSLENBQWEsU0FBUyxPQUF0QixDQUFYO0FBQ0QsU0E3Qk07O0FBK0JQLHdCQUFnQixZQUFXO0FBQ3pCLGNBQUcsc0JBQXNCLE1BQXpCLEVBQWlDO0FBQy9CLHVCQUFXLElBQUksZ0JBQUosQ0FBcUIsVUFBUyxTQUFULEVBQW9CO0FBQ2xELHFCQUFPLEtBQVAsQ0FBYSw0Q0FBYjtBQUNBLHFCQUFPLE9BQVA7QUFDRCxhQUhVLENBQVg7QUFJQSxxQkFBUyxPQUFULENBQWlCLE9BQWpCLEVBQTBCO0FBQ3hCLHlCQUFZLElBRFk7QUFFeEIsdUJBQVk7QUFGWSxhQUExQjtBQUlBLG1CQUFPLEtBQVAsQ0FBYSw4QkFBYixFQUE2QyxRQUE3QztBQUNEO0FBQ0YsU0EzQ007O0FBNkNQLGNBQU07QUFDSixrQkFBUSxZQUFXO0FBQ2pCLG1CQUFPLEtBQVAsQ0FBYSwwQkFBYjtBQUNBLG9CQUNHLEVBREgsQ0FDTSxTQUFTLEVBQVQsR0FBYyxjQURwQixFQUNvQyxTQUFTLE9BRDdDLEVBQ3NELE9BQU8sS0FBUCxDQUFhLEtBRG5FO0FBR0Q7QUFORyxTQTdDQzs7QUFzRFAsZUFBTztBQUNMLGlCQUFPLFlBQVc7QUFDaEIsbUJBQU8sTUFBUCxDQUFjLElBQWQsQ0FBbUIsSUFBbkI7QUFDRDtBQUhJLFNBdERBOztBQTREUCxnQkFBUSxVQUFTLEtBQVQsRUFBZ0I7QUFDdEIsY0FDRSxlQUFnQixVQUFVLFNBQVgsR0FDVixPQUFPLEtBQVAsS0FBaUIsUUFBbEIsR0FDRSxPQUFPLEVBQVAsQ0FBVSxLQUFWLENBREYsR0FFRSxFQUFFLEtBQUYsRUFBUyxPQUFULENBQWlCLFNBQVMsS0FBMUIsQ0FIUyxHQUlYLEVBQUUsSUFBRixFQUFRLE9BQVIsQ0FBZ0IsU0FBUyxLQUF6QixDQUxOO0FBQUEsY0FNRSxpQkFBaUIsYUFBYSxJQUFiLENBQWtCLFFBQWxCLENBTm5CO0FBQUEsY0FPRSxjQUFjLGVBQWUsUUFBZixDQUF3QixVQUFVLFNBQWxDLENBUGhCO0FBQUEsY0FRRSxXQUFjLGVBQWUsUUFBZixDQUF3QixVQUFVLE1BQWxDLENBUmhCO0FBQUEsY0FTRSxTQUFlLFlBQVksQ0FBQyxXQVQ5QjtBQUFBLGNBVUUsWUFBZSxDQUFDLFFBQUQsSUFBYSxXQVY5QjtBQVlBLGlCQUFPLEtBQVAsQ0FBYSxnQ0FBYixFQUErQyxZQUEvQztBQUNBLGNBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQ3RCLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2QixxQkFBTyxLQUFQLENBQWEsSUFBYixDQUFrQixZQUFsQjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLEtBQVAsQ0FBYSx1REFBYjtBQUNEO0FBQ0YsV0FQRCxNQVFLO0FBQ0gsbUJBQU8sSUFBUCxDQUFZLElBQVosQ0FBaUIsWUFBakI7QUFDRDtBQUNGLFNBckZNOztBQXVGUCxjQUFNLFVBQVMsS0FBVCxFQUFnQjtBQUNwQixjQUNFLGVBQWdCLFVBQVUsU0FBWCxHQUNWLE9BQU8sS0FBUCxLQUFpQixRQUFsQixHQUNFLE9BQU8sRUFBUCxDQUFVLEtBQVYsQ0FERixHQUVFLEVBQUUsS0FBRixFQUFTLE9BQVQsQ0FBaUIsU0FBUyxLQUExQixDQUhTLEdBSVgsRUFBRSxJQUFGLEVBQVEsT0FBUixDQUFnQixTQUFTLEtBQXpCLENBTE47QUFBQSxjQU1FLGlCQUFpQixhQUFhLElBQWIsQ0FBa0IsUUFBbEIsQ0FObkI7QUFBQSxjQU9FLGNBQWMsZUFBZSxRQUFmLENBQXdCLFVBQVUsU0FBbEMsQ0FQaEI7QUFBQSxjQVFFLFdBQWMsZUFBZSxRQUFmLENBQXdCLFVBQVUsTUFBbEMsQ0FSaEI7QUFBQSxjQVNFLFNBQWUsWUFBWSxXQVQ3QjtBQVdBLGNBQUcsTUFBSCxFQUFXO0FBQ1QsbUJBQU8sS0FBUCxDQUFhLGtDQUFiLEVBQWlELGNBQWpEO0FBQ0E7QUFDRDtBQUNELGlCQUFPLEtBQVAsQ0FBYSwyQkFBYixFQUEwQyxZQUExQztBQUNBLG1CQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsY0FBeEI7QUFDQSxjQUFHLFNBQVMsU0FBWixFQUF1QjtBQUNyQixtQkFBTyxXQUFQLENBQW1CLElBQW5CLENBQXdCLFlBQXhCO0FBQ0Q7QUFDRCx1QkFDRyxRQURILENBQ1ksVUFBVSxNQUR0QjtBQUdBLHlCQUNHLElBREgsQ0FDUSxJQURSLEVBQ2MsSUFEZCxFQUVHLFFBRkgsQ0FFWSxVQUFVLFNBRnRCO0FBSUEsY0FBRyxTQUFTLGVBQVosRUFBNkI7QUFDM0IsZ0JBQUcsRUFBRSxFQUFGLENBQUssVUFBTCxLQUFvQixTQUFwQixJQUFpQyxRQUFRLFVBQVIsQ0FBbUIsY0FBbkIsQ0FBcEMsRUFBd0U7QUFDdEUsNkJBQ0csUUFESCxHQUVLLFVBRkwsQ0FFZ0I7QUFDViwyQkFBYyxTQURKO0FBRVYsdUJBQWMsS0FGSjtBQUdWLDZCQUFjLElBSEo7QUFJVix1QkFBYyxTQUFTLEtBSmI7QUFLVix5QkFBYyxTQUFTLE9BTGI7QUFNViwwQkFBYyxTQUFTO0FBTmIsZUFGaEI7QUFXRCxhQVpELE1BYUs7QUFDSCw2QkFDRyxRQURILEdBRUssSUFGTCxDQUVVLElBRlYsRUFFZ0IsSUFGaEIsRUFHSyxPQUhMLENBR2E7QUFDUCx5QkFBUztBQURGLGVBSGIsRUFLTyxTQUFTLFFBTGhCLEVBSzBCLE9BQU8sWUFMakM7QUFPRDtBQUNGO0FBQ0QseUJBQ0csU0FESCxDQUNhLFNBQVMsUUFEdEIsRUFDZ0MsU0FBUyxNQUR6QyxFQUNpRCxZQUFXO0FBQ3hELDJCQUNHLFdBREgsQ0FDZSxVQUFVLFNBRHpCLEVBRUcsUUFGSCxDQUVZLFVBQVUsTUFGdEI7QUFJQSxtQkFBTyxLQUFQLENBQWEsT0FBYixDQUFxQixJQUFyQixDQUEwQixJQUExQjtBQUNBLHFCQUFTLE1BQVQsQ0FBZ0IsSUFBaEIsQ0FBcUIsSUFBckI7QUFDQSxxQkFBUyxRQUFULENBQWtCLElBQWxCLENBQXVCLElBQXZCO0FBQ0QsV0FUSDtBQVdELFNBdEpNOztBQXdKUCxlQUFPLFVBQVMsS0FBVCxFQUFnQjtBQUNyQixjQUNFLGVBQWdCLFVBQVUsU0FBWCxHQUNWLE9BQU8sS0FBUCxLQUFpQixRQUFsQixHQUNFLE9BQU8sRUFBUCxDQUFVLEtBQVYsQ0FERixHQUVFLEVBQUUsS0FBRixFQUFTLE9BQVQsQ0FBaUIsU0FBUyxLQUExQixDQUhTLEdBSVgsRUFBRSxJQUFGLEVBQVEsT0FBUixDQUFnQixTQUFTLEtBQXpCLENBTE47QUFBQSxjQU1FLGlCQUFpQixhQUFhLElBQWIsQ0FBa0IsUUFBbEIsQ0FObkI7QUFBQSxjQU9FLGNBQWlCLGVBQWUsUUFBZixDQUF3QixVQUFVLFNBQWxDLENBUG5CO0FBQUEsY0FRRSxXQUFpQixlQUFlLFFBQWYsQ0FBd0IsVUFBVSxNQUFsQyxDQVJuQjtBQUFBLGNBU0UsWUFBa0IsQ0FBQyxRQUFELElBQWEsV0FUakM7QUFBQSxjQVVFLFlBQWtCLFlBQVksV0FWaEM7QUFZQSxjQUFHLENBQUMsWUFBWSxTQUFiLEtBQTJCLENBQUMsU0FBL0IsRUFBMEM7QUFDeEMsbUJBQU8sS0FBUCxDQUFhLDJCQUFiLEVBQTBDLGNBQTFDO0FBQ0EscUJBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixjQUF4QjtBQUNBLHlCQUNHLFdBREgsQ0FDZSxVQUFVLE1BRHpCO0FBR0EsMkJBQ0csSUFESCxDQUNRLElBRFIsRUFDYyxJQURkLEVBRUcsUUFGSCxDQUVZLFVBQVUsU0FGdEI7QUFJQSxnQkFBRyxTQUFTLGVBQVosRUFBNkI7QUFDM0Isa0JBQUcsRUFBRSxFQUFGLENBQUssVUFBTCxLQUFvQixTQUFwQixJQUFpQyxRQUFRLFVBQVIsQ0FBbUIsY0FBbkIsQ0FBcEMsRUFBd0U7QUFDdEUsK0JBQ0csUUFESCxHQUVLLFVBRkwsQ0FFZ0I7QUFDViw2QkFBYyxVQURKO0FBRVYseUJBQWMsS0FGSjtBQUdWLCtCQUFjLElBSEo7QUFJVix5QkFBYyxTQUFTLEtBSmI7QUFLViwyQkFBYyxTQUFTLE9BTGI7QUFNViw0QkFBYyxTQUFTO0FBTmIsaUJBRmhCO0FBV0QsZUFaRCxNQWFLO0FBQ0gsK0JBQ0csUUFESCxHQUVLLElBRkwsQ0FFVSxJQUZWLEVBRWdCLElBRmhCLEVBR0ssT0FITCxDQUdhO0FBQ1AsMkJBQVM7QUFERixpQkFIYixFQUtPLFNBQVMsUUFMaEIsRUFLMEIsT0FBTyxZQUxqQztBQU9EO0FBQ0Y7QUFDRCwyQkFDRyxPQURILENBQ1csU0FBUyxRQURwQixFQUM4QixTQUFTLE1BRHZDLEVBQytDLFlBQVc7QUFDdEQsNkJBQ0csV0FESCxDQUNlLFVBQVUsU0FEekIsRUFFRyxXQUZILENBRWUsVUFBVSxNQUZ6QjtBQUlBLHFCQUFPLEtBQVAsQ0FBYSxPQUFiLENBQXFCLElBQXJCLENBQTBCLElBQTFCO0FBQ0EsdUJBQVMsT0FBVCxDQUFpQixJQUFqQixDQUFzQixJQUF0QjtBQUNBLHVCQUFTLFFBQVQsQ0FBa0IsSUFBbEIsQ0FBdUIsSUFBdkI7QUFDRCxhQVRIO0FBV0Q7QUFDRixTQW5OTTs7QUFxTlAscUJBQWEsVUFBUyxLQUFULEVBQWdCO0FBQzNCLGNBQ0UsZUFBZ0IsVUFBVSxTQUFYLEdBQ1gsT0FBTyxFQUFQLENBQVUsS0FBVixDQURXLEdBRVgsRUFBRSxJQUFGLEVBQVEsT0FBUixDQUFnQixTQUFTLEtBQXpCLENBSE47QUFBQSxjQUlFLGdCQUFtQixhQUFhLE9BQWIsQ0FBcUIsU0FBUyxPQUE5QixFQUF1QyxJQUF2QyxDQUE0QyxTQUFTLEtBQXJELENBSnJCO0FBQUEsY0FLRSxtQkFBbUIsYUFBYSxPQUFiLENBQXFCLFNBQVMsU0FBOUIsQ0FMckI7QUFBQSxjQU1FLGlCQUFtQixTQUFTLEtBQVQsR0FBaUIsR0FBakIsR0FBdUIsVUFBVSxNQUFqQyxHQUEwQyxVQU4vRDtBQUFBLGNBT0UsZ0JBQW1CLFNBQVMsT0FBVCxHQUFtQixHQUFuQixHQUF5QixVQUFVLE1BQW5DLEdBQTRDLFVBUGpFO0FBQUEsY0FRRSxXQVJGO0FBQUEsY0FTRSxhQVRGO0FBQUEsY0FVRSxhQVZGO0FBWUEsY0FBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIsMEJBQWdCLGlCQUFpQixJQUFqQixDQUFzQixjQUF0QixFQUFzQyxHQUF0QyxDQUEwQyxhQUExQyxDQUFoQjtBQUNBLDRCQUFnQixZQUFZLElBQVosQ0FBaUIsUUFBakIsQ0FBaEI7QUFDRCxXQUhELE1BSUs7QUFDSCwwQkFBZ0IsaUJBQWlCLElBQWpCLENBQXNCLGNBQXRCLEVBQXNDLEdBQXRDLENBQTBDLGFBQTFDLENBQWhCO0FBQ0EsNEJBQWdCLGlCQUFpQixJQUFqQixDQUFzQixhQUF0QixFQUFxQyxJQUFyQyxDQUEwQyxjQUExQyxFQUEwRCxHQUExRCxDQUE4RCxhQUE5RCxDQUFoQjtBQUNBLDBCQUFnQixZQUFZLEdBQVosQ0FBZ0IsYUFBaEIsQ0FBaEI7QUFDQSw0QkFBZ0IsWUFBWSxJQUFaLENBQWlCLFFBQWpCLENBQWhCO0FBQ0Q7QUFDRCxjQUFLLFlBQVksTUFBWixHQUFxQixDQUExQixFQUErQjtBQUM3QixtQkFBTyxLQUFQLENBQWEsMENBQWIsRUFBeUQsV0FBekQ7QUFDQSx3QkFDRyxXQURILENBQ2UsVUFBVSxNQUR6QjtBQUdBLDBCQUNHLFdBREgsQ0FDZSxVQUFVLFNBRHpCLEVBRUcsSUFGSCxDQUVRLElBRlIsRUFFYyxJQUZkO0FBSUEsZ0JBQUcsU0FBUyxlQUFaLEVBQTZCO0FBQzNCLGtCQUFHLEVBQUUsRUFBRixDQUFLLFVBQUwsS0FBb0IsU0FBcEIsSUFBaUMsUUFBUSxVQUFSLENBQW1CLGNBQW5CLENBQXBDLEVBQXdFO0FBQ3RFLDhCQUNHLFFBREgsR0FFSyxVQUZMLENBRWdCO0FBQ1YsNkJBQWMsVUFESjtBQUVWLCtCQUFjLElBRko7QUFHVix5QkFBYyxTQUFTLEtBSGI7QUFJViwyQkFBYyxTQUFTLE9BSmI7QUFLViw0QkFBYyxTQUFTO0FBTGIsaUJBRmhCO0FBVUQsZUFYRCxNQVlLO0FBQ0gsOEJBQ0csUUFESCxHQUVLLElBRkwsQ0FFVSxJQUZWLEVBRWdCLElBRmhCLEVBR0ssT0FITCxDQUdhO0FBQ1AsMkJBQVM7QUFERixpQkFIYixFQUtPLFNBQVMsUUFMaEIsRUFLMEIsT0FBTyxZQUxqQztBQU9EO0FBQ0Y7QUFDRCwwQkFDRyxPQURILENBQ1csU0FBUyxRQURwQixFQUMrQixTQUFTLE1BRHhDLEVBQ2dELFlBQVc7QUFDdkQsZ0JBQUUsSUFBRixFQUFRLFdBQVIsQ0FBb0IsVUFBVSxNQUE5QjtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxPQUFiLENBQXFCLElBQXJCLENBQTBCLElBQTFCO0FBQ0QsYUFKSDtBQU1EO0FBQ0YsU0FuUk07O0FBcVJQLGVBQU87O0FBRUwsbUJBQVMsWUFBVztBQUNsQixtQkFBTyxPQUFQLENBQWUsc0NBQWYsRUFBdUQsSUFBdkQ7QUFDQSxjQUFFLElBQUYsRUFBUSxHQUFSLENBQVksU0FBWixFQUF1QixFQUF2QjtBQUNBLGdCQUFJLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxPQUFiLE1BQTBCLEVBQTlCLEVBQWtDO0FBQ2hDLGdCQUFFLElBQUYsRUFDRyxJQURILENBQ1EsT0FEUixFQUNpQixFQURqQixFQUVHLFVBRkgsQ0FFYyxPQUZkO0FBSUQ7QUFDRixXQVhJOztBQWFMLG1CQUFTLFlBQVc7QUFDbEIsbUJBQU8sT0FBUCxDQUFlLHNDQUFmLEVBQXVELElBQXZEO0FBQ0EsY0FBRSxJQUFGLEVBQVEsR0FBUixDQUFZLFNBQVosRUFBdUIsRUFBdkI7QUFDQSxnQkFBSSxFQUFFLElBQUYsRUFBUSxJQUFSLENBQWEsT0FBYixNQUEwQixFQUE5QixFQUFrQztBQUNoQyxnQkFBRSxJQUFGLEVBQ0csSUFESCxDQUNRLE9BRFIsRUFDaUIsRUFEakIsRUFFRyxVQUZILENBRWMsT0FGZDtBQUlEO0FBQ0Y7O0FBdEJJLFNBclJBOztBQStTUCxpQkFBUyxVQUFTLElBQVQsRUFBZSxLQUFmLEVBQXNCO0FBQzdCLGlCQUFPLEtBQVAsQ0FBYSxrQkFBYixFQUFpQyxJQUFqQyxFQUF1QyxLQUF2QztBQUNBLGNBQUksRUFBRSxhQUFGLENBQWdCLElBQWhCLENBQUosRUFBNEI7QUFDMUIsY0FBRSxNQUFGLENBQVMsSUFBVCxFQUFlLFFBQWYsRUFBeUIsSUFBekI7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsZ0JBQUcsRUFBRSxhQUFGLENBQWdCLFNBQVMsSUFBVCxDQUFoQixDQUFILEVBQW9DO0FBQ2xDLGdCQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsU0FBUyxJQUFULENBQWYsRUFBK0IsS0FBL0I7QUFDRCxhQUZELE1BR0s7QUFDSCx1QkFBUyxJQUFULElBQWlCLEtBQWpCO0FBQ0Q7QUFDRixXQVBJLE1BUUE7QUFDSCxtQkFBTyxTQUFTLElBQVQsQ0FBUDtBQUNEO0FBQ0YsU0EvVE07QUFnVVAsa0JBQVUsVUFBUyxJQUFULEVBQWUsS0FBZixFQUFzQjtBQUM5QixpQkFBTyxLQUFQLENBQWEsbUJBQWIsRUFBa0MsSUFBbEMsRUFBd0MsS0FBeEM7QUFDQSxjQUFHLFVBQVUsU0FBYixFQUF3QjtBQUN0QixnQkFBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixnQkFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLE1BQWYsRUFBdUIsSUFBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxJQUFQLElBQWUsS0FBZjtBQUNEO0FBQ0YsV0FQRCxNQVFLO0FBQ0gsbUJBQU8sT0FBTyxJQUFQLENBQVA7QUFDRDtBQUNGLFNBN1VNO0FBOFVQLGVBQU8sWUFBVztBQUNoQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsS0FBaEMsRUFBdUM7QUFDckMsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxLQUFQLEdBQWUsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWY7QUFDQSxxQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNEO0FBQ0Y7QUFDRixTQXhWTTtBQXlWUCxpQkFBUyxZQUFXO0FBQ2xCLGNBQUcsQ0FBQyxTQUFTLE1BQVYsSUFBb0IsU0FBUyxPQUE3QixJQUF3QyxTQUFTLEtBQXBELEVBQTJEO0FBQ3pELGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2QixxQkFBTyxXQUFQLENBQW1CLEdBQW5CLENBQXVCLFNBQXZCO0FBQ0QsYUFGRCxNQUdLO0FBQ0gscUJBQU8sT0FBUCxHQUFpQixTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxJQUFyQyxFQUEyQyxPQUEzQyxFQUFvRCxTQUFTLElBQVQsR0FBZ0IsR0FBcEUsQ0FBakI7QUFDQSxxQkFBTyxPQUFQLENBQWUsS0FBZixDQUFxQixPQUFyQixFQUE4QixTQUE5QjtBQUNEO0FBQ0Y7QUFDRixTQW5XTTtBQW9XUCxlQUFPLFlBQVc7QUFDaEIsY0FBRyxDQUFDLFNBQVMsTUFBYixFQUFxQjtBQUNuQixtQkFBTyxLQUFQLEdBQWUsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsS0FBckMsRUFBNEMsT0FBNUMsRUFBcUQsU0FBUyxJQUFULEdBQWdCLEdBQXJFLENBQWY7QUFDQSxtQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNEO0FBQ0YsU0F6V007QUEwV1AscUJBQWE7QUFDWCxlQUFLLFVBQVMsT0FBVCxFQUFrQjtBQUNyQixnQkFDRSxXQURGLEVBRUUsYUFGRixFQUdFLFlBSEY7QUFLQSxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIsNEJBQWdCLElBQUksSUFBSixHQUFXLE9BQVgsRUFBaEI7QUFDQSw2QkFBZ0IsUUFBUSxXQUF4QjtBQUNBLDhCQUFnQixjQUFjLFlBQTlCO0FBQ0EscUJBQWdCLFdBQWhCO0FBQ0EsMEJBQVksSUFBWixDQUFpQjtBQUNmLHdCQUFtQixRQUFRLENBQVIsQ0FESjtBQUVmLDZCQUFtQixHQUFHLEtBQUgsQ0FBUyxJQUFULENBQWMsT0FBZCxFQUF1QixDQUF2QixLQUE2QixFQUZqQztBQUdmLDJCQUFtQixPQUhKO0FBSWYsa0NBQW1CO0FBSkosZUFBakI7QUFNRDtBQUNELHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLG1CQUFPLFdBQVAsQ0FBbUIsS0FBbkIsR0FBMkIsV0FBVyxPQUFPLFdBQVAsQ0FBbUIsT0FBOUIsRUFBdUMsR0FBdkMsQ0FBM0I7QUFDRCxXQXJCVTtBQXNCWCxtQkFBUyxZQUFXO0FBQ2xCLGdCQUNFLFFBQVEsU0FBUyxJQUFULEdBQWdCLEdBRDFCO0FBQUEsZ0JBRUUsWUFBWSxDQUZkO0FBSUEsbUJBQU8sS0FBUDtBQUNBLHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLGNBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDJCQUFhLEtBQUssZ0JBQUwsQ0FBYjtBQUNELGFBRkQ7QUFHQSxxQkFBUyxNQUFNLFNBQU4sR0FBa0IsSUFBM0I7QUFDQSxnQkFBRyxjQUFILEVBQW1CO0FBQ2pCLHVCQUFTLFFBQVEsY0FBUixHQUF5QixJQUFsQztBQUNEO0FBQ0QsZ0JBQUksQ0FBQyxRQUFRLEtBQVIsS0FBa0IsU0FBbEIsSUFBK0IsUUFBUSxLQUFSLEtBQWtCLFNBQWxELEtBQWdFLFlBQVksTUFBWixHQUFxQixDQUF6RixFQUE0RjtBQUMxRixzQkFBUSxjQUFSLENBQXVCLEtBQXZCO0FBQ0Esa0JBQUcsUUFBUSxLQUFYLEVBQWtCO0FBQ2hCLHdCQUFRLEtBQVIsQ0FBYyxXQUFkO0FBQ0QsZUFGRCxNQUdLO0FBQ0gsa0JBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDBCQUFRLEdBQVIsQ0FBWSxLQUFLLE1BQUwsSUFBZSxJQUFmLEdBQXNCLEtBQUssZ0JBQUwsQ0FBdEIsR0FBNkMsSUFBekQ7QUFDRCxpQkFGRDtBQUdEO0FBQ0Qsc0JBQVEsUUFBUjtBQUNEO0FBQ0QsMEJBQWMsRUFBZDtBQUNEO0FBakRVLFNBMVdOO0FBNlpQLGdCQUFRLFVBQVMsS0FBVCxFQUFnQixlQUFoQixFQUFpQyxPQUFqQyxFQUEwQztBQUNoRCxjQUNFLFNBQVMsUUFEWDtBQUFBLGNBRUUsUUFGRjtBQUFBLGNBR0UsS0FIRjtBQUFBLGNBSUUsUUFKRjtBQU1BLDRCQUFrQixtQkFBbUIsY0FBckM7QUFDQSxvQkFBa0IsV0FBbUIsT0FBckM7QUFDQSxjQUFHLE9BQU8sS0FBUCxJQUFnQixRQUFoQixJQUE0QixXQUFXLFNBQTFDLEVBQXFEO0FBQ25ELG9CQUFXLE1BQU0sS0FBTixDQUFZLE9BQVosQ0FBWDtBQUNBLHVCQUFXLE1BQU0sTUFBTixHQUFlLENBQTFCO0FBQ0EsY0FBRSxJQUFGLENBQU8sS0FBUCxFQUFjLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUF1QjtBQUNuQyxrQkFBSSxpQkFBa0IsU0FBUyxRQUFWLEdBQ2pCLFFBQVEsTUFBTSxRQUFRLENBQWQsRUFBaUIsTUFBakIsQ0FBd0IsQ0FBeEIsRUFBMkIsV0FBM0IsRUFBUixHQUFtRCxNQUFNLFFBQVEsQ0FBZCxFQUFpQixLQUFqQixDQUF1QixDQUF2QixDQURsQyxHQUVqQixLQUZKO0FBSUEsa0JBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sY0FBUCxDQUFqQixLQUE4QyxTQUFTLFFBQTNELEVBQXVFO0FBQ3JFLHlCQUFTLE9BQU8sY0FBUCxDQUFUO0FBQ0QsZUFGRCxNQUdLLElBQUksT0FBTyxjQUFQLE1BQTJCLFNBQS9CLEVBQTJDO0FBQzlDLHdCQUFRLE9BQU8sY0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQSxJQUFJLEVBQUUsYUFBRixDQUFpQixPQUFPLEtBQVAsQ0FBakIsS0FBcUMsU0FBUyxRQUFsRCxFQUE4RDtBQUNqRSx5QkFBUyxPQUFPLEtBQVAsQ0FBVDtBQUNELGVBRkksTUFHQSxJQUFJLE9BQU8sS0FBUCxNQUFrQixTQUF0QixFQUFrQztBQUNyQyx3QkFBUSxPQUFPLEtBQVAsQ0FBUjtBQUNBLHVCQUFPLEtBQVA7QUFDRCxlQUhJLE1BSUE7QUFDSCx1QkFBTyxLQUFQLENBQWEsTUFBTSxNQUFuQixFQUEyQixLQUEzQjtBQUNBLHVCQUFPLEtBQVA7QUFDRDtBQUNGLGFBdkJEO0FBd0JEO0FBQ0QsY0FBSyxFQUFFLFVBQUYsQ0FBYyxLQUFkLENBQUwsRUFBNkI7QUFDM0IsdUJBQVcsTUFBTSxLQUFOLENBQVksT0FBWixFQUFxQixlQUFyQixDQUFYO0FBQ0QsV0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLHVCQUFXLEtBQVg7QUFDRDtBQUNELGNBQUcsRUFBRSxPQUFGLENBQVUsYUFBVixDQUFILEVBQTZCO0FBQzNCLDBCQUFjLElBQWQsQ0FBbUIsUUFBbkI7QUFDRCxXQUZELE1BR0ssSUFBRyxrQkFBa0IsU0FBckIsRUFBZ0M7QUFDbkMsNEJBQWdCLENBQUMsYUFBRCxFQUFnQixRQUFoQixDQUFoQjtBQUNELFdBRkksTUFHQSxJQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDOUIsNEJBQWdCLFFBQWhCO0FBQ0Q7QUFDRCxpQkFBTyxLQUFQO0FBQ0Q7QUFsZE0sT0FBVDtBQW9kQSxVQUFHLGFBQUgsRUFBa0I7QUFDaEIsWUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLGlCQUFPLFVBQVA7QUFDRDtBQUNELGVBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDRCxPQUxELE1BTUs7QUFDSCxZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsbUJBQVMsTUFBVCxDQUFnQixTQUFoQjtBQUNEO0FBQ0QsZUFBTyxVQUFQO0FBQ0Q7QUFDRixLQTFmSDtBQTRmQSxXQUFRLGtCQUFrQixTQUFuQixHQUNILGFBREcsR0FFSCxJQUZKO0FBSUQsR0FuaEJEOztBQXFoQkEsSUFBRSxFQUFGLENBQUssU0FBTCxDQUFlLFFBQWYsR0FBMEI7O0FBRXhCLFVBQWtCLFdBRk07QUFHeEIsZUFBa0IsV0FITTs7QUFLeEIsWUFBa0IsS0FMTTtBQU14QixXQUFrQixLQU5NO0FBT3hCLGFBQWtCLEtBUE07QUFReEIsaUJBQWtCLElBUk07O0FBVXhCLFFBQWtCLE9BVk0sRTs7QUFZeEIsb0JBQWtCLElBWk0sRTs7QUFjeEIsZUFBa0IsSUFkTSxFO0FBZXhCLGlCQUFrQixJQWZNLEU7QUFnQnhCLGlCQUFrQixLQWhCTSxFO0FBaUJ4QixxQkFBa0IsSUFqQk0sRTs7QUFtQnhCLGNBQWtCLEdBbkJNLEU7QUFvQnhCLFlBQWtCLGFBcEJNLEU7O0FBdUJ4QixlQUFrQixZQUFVLENBQUUsQ0F2Qk4sRTtBQXdCeEIsWUFBa0IsWUFBVSxDQUFFLENBeEJOLEU7QUF5QnhCLGVBQWtCLFlBQVUsQ0FBRSxDQXpCTixFO0FBMEJ4QixhQUFrQixZQUFVLENBQUUsQ0ExQk4sRTtBQTJCeEIsY0FBa0IsWUFBVSxDQUFFLENBM0JOLEU7O0FBNkJ4QixXQUFPO0FBQ0wsY0FBUztBQURKLEtBN0JpQjs7QUFpQ3hCLGVBQWM7QUFDWixjQUFZLFFBREE7QUFFWixpQkFBWTtBQUZBLEtBakNVOztBQXNDeEIsY0FBYztBQUNaLGlCQUFZLFlBREE7QUFFWixhQUFZLFFBRkE7QUFHWixlQUFZLFFBSEE7QUFJWixlQUFZO0FBSkE7O0FBdENVLEdBQTFCOzs7QUFnREEsSUFBRSxNQUFGLENBQVUsRUFBRSxNQUFaLEVBQW9CO0FBQ2xCLGlCQUFhLFVBQVUsQ0FBVixFQUFhLENBQWIsRUFBZ0IsQ0FBaEIsRUFBbUIsQ0FBbkIsRUFBc0IsQ0FBdEIsRUFBeUI7QUFDcEMsYUFBTyxDQUFDLENBQUQsSUFBSyxLQUFHLENBQVIsS0FBWSxJQUFFLENBQWQsSUFBbUIsQ0FBMUI7QUFDRDtBQUhpQixHQUFwQjtBQU1DLENBdGxCQSxFQXNsQkcsTUF0bEJILEVBc2xCVyxNQXRsQlgsRUFzbEJtQixRQXRsQm5CIiwiZmlsZSI6ImFjY29yZGlvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxuICogIyBTZW1hbnRpYyBVSSAtIEFjY29yZGlvblxuICogaHR0cDovL2dpdGh1Yi5jb20vc2VtYW50aWMtb3JnL3NlbWFudGljLXVpL1xuICpcbiAqXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2VcbiAqIGh0dHA6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9NSVRcbiAqXG4gKi9cblxuOyhmdW5jdGlvbiAoJCwgd2luZG93LCBkb2N1bWVudCwgdW5kZWZpbmVkKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG53aW5kb3cgPSAodHlwZW9mIHdpbmRvdyAhPSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuTWF0aCA9PSBNYXRoKVxuICA/IHdpbmRvd1xuICA6ICh0eXBlb2Ygc2VsZiAhPSAndW5kZWZpbmVkJyAmJiBzZWxmLk1hdGggPT0gTWF0aClcbiAgICA/IHNlbGZcbiAgICA6IEZ1bmN0aW9uKCdyZXR1cm4gdGhpcycpKClcbjtcblxuJC5mbi5hY2NvcmRpb24gPSBmdW5jdGlvbihwYXJhbWV0ZXJzKSB7XG4gIHZhclxuICAgICRhbGxNb2R1bGVzICAgICA9ICQodGhpcyksXG5cbiAgICB0aW1lICAgICAgICAgICAgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKSxcbiAgICBwZXJmb3JtYW5jZSAgICAgPSBbXSxcblxuICAgIHF1ZXJ5ICAgICAgICAgICA9IGFyZ3VtZW50c1swXSxcbiAgICBtZXRob2RJbnZva2VkICAgPSAodHlwZW9mIHF1ZXJ5ID09ICdzdHJpbmcnKSxcbiAgICBxdWVyeUFyZ3VtZW50cyAgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSksXG5cbiAgICByZXF1ZXN0QW5pbWF0aW9uRnJhbWUgPSB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gICAgICB8fCB3aW5kb3cubW96UmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gICAgICB8fCB3aW5kb3cud2Via2l0UmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gICAgICB8fCB3aW5kb3cubXNSZXF1ZXN0QW5pbWF0aW9uRnJhbWVcbiAgICAgIHx8IGZ1bmN0aW9uKGNhbGxiYWNrKSB7IHNldFRpbWVvdXQoY2FsbGJhY2ssIDApOyB9LFxuXG4gICAgcmV0dXJuZWRWYWx1ZVxuICA7XG4gICRhbGxNb2R1bGVzXG4gICAgLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICB2YXJcbiAgICAgICAgc2V0dGluZ3MgICAgICAgID0gKCAkLmlzUGxhaW5PYmplY3QocGFyYW1ldGVycykgKVxuICAgICAgICAgID8gJC5leHRlbmQodHJ1ZSwge30sICQuZm4uYWNjb3JkaW9uLnNldHRpbmdzLCBwYXJhbWV0ZXJzKVxuICAgICAgICAgIDogJC5leHRlbmQoe30sICQuZm4uYWNjb3JkaW9uLnNldHRpbmdzKSxcblxuICAgICAgICBjbGFzc05hbWUgICAgICAgPSBzZXR0aW5ncy5jbGFzc05hbWUsXG4gICAgICAgIG5hbWVzcGFjZSAgICAgICA9IHNldHRpbmdzLm5hbWVzcGFjZSxcbiAgICAgICAgc2VsZWN0b3IgICAgICAgID0gc2V0dGluZ3Muc2VsZWN0b3IsXG4gICAgICAgIGVycm9yICAgICAgICAgICA9IHNldHRpbmdzLmVycm9yLFxuXG4gICAgICAgIGV2ZW50TmFtZXNwYWNlICA9ICcuJyArIG5hbWVzcGFjZSxcbiAgICAgICAgbW9kdWxlTmFtZXNwYWNlID0gJ21vZHVsZS0nICsgbmFtZXNwYWNlLFxuICAgICAgICBtb2R1bGVTZWxlY3RvciAgPSAkYWxsTW9kdWxlcy5zZWxlY3RvciB8fCAnJyxcblxuICAgICAgICAkbW9kdWxlICA9ICQodGhpcyksXG4gICAgICAgICR0aXRsZSAgID0gJG1vZHVsZS5maW5kKHNlbGVjdG9yLnRpdGxlKSxcbiAgICAgICAgJGNvbnRlbnQgPSAkbW9kdWxlLmZpbmQoc2VsZWN0b3IuY29udGVudCksXG5cbiAgICAgICAgZWxlbWVudCAgPSB0aGlzLFxuICAgICAgICBpbnN0YW5jZSA9ICRtb2R1bGUuZGF0YShtb2R1bGVOYW1lc3BhY2UpLFxuICAgICAgICBvYnNlcnZlcixcbiAgICAgICAgbW9kdWxlXG4gICAgICA7XG5cbiAgICAgIG1vZHVsZSA9IHtcblxuICAgICAgICBpbml0aWFsaXplOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0luaXRpYWxpemluZycsICRtb2R1bGUpO1xuICAgICAgICAgIG1vZHVsZS5iaW5kLmV2ZW50cygpO1xuICAgICAgICAgIGlmKHNldHRpbmdzLm9ic2VydmVDaGFuZ2VzKSB7XG4gICAgICAgICAgICBtb2R1bGUub2JzZXJ2ZUNoYW5nZXMoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgbW9kdWxlLmluc3RhbnRpYXRlKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5zdGFudGlhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGluc3RhbmNlID0gbW9kdWxlO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5kYXRhKG1vZHVsZU5hbWVzcGFjZSwgbW9kdWxlKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBkZXN0cm95OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0Rlc3Ryb3lpbmcgcHJldmlvdXMgaW5zdGFuY2UnLCAkbW9kdWxlKTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAub2ZmKGV2ZW50TmFtZXNwYWNlKVxuICAgICAgICAgICAgLnJlbW92ZURhdGEobW9kdWxlTmFtZXNwYWNlKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICByZWZyZXNoOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAkdGl0bGUgICA9ICRtb2R1bGUuZmluZChzZWxlY3Rvci50aXRsZSk7XG4gICAgICAgICAgJGNvbnRlbnQgPSAkbW9kdWxlLmZpbmQoc2VsZWN0b3IuY29udGVudCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgb2JzZXJ2ZUNoYW5nZXM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCdNdXRhdGlvbk9ic2VydmVyJyBpbiB3aW5kb3cpIHtcbiAgICAgICAgICAgIG9ic2VydmVyID0gbmV3IE11dGF0aW9uT2JzZXJ2ZXIoZnVuY3Rpb24obXV0YXRpb25zKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRE9NIHRyZWUgbW9kaWZpZWQsIHVwZGF0aW5nIHNlbGVjdG9yIGNhY2hlJyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5yZWZyZXNoKCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIG9ic2VydmVyLm9ic2VydmUoZWxlbWVudCwge1xuICAgICAgICAgICAgICBjaGlsZExpc3QgOiB0cnVlLFxuICAgICAgICAgICAgICBzdWJ0cmVlICAgOiB0cnVlXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2V0dGluZyB1cCBtdXRhdGlvbiBvYnNlcnZlcicsIG9ic2VydmVyKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgYmluZDoge1xuICAgICAgICAgIGV2ZW50czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0JpbmRpbmcgZGVsZWdhdGVkIGV2ZW50cycpO1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAub24oc2V0dGluZ3Mub24gKyBldmVudE5hbWVzcGFjZSwgc2VsZWN0b3IudHJpZ2dlciwgbW9kdWxlLmV2ZW50LmNsaWNrKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBldmVudDoge1xuICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS50b2dnbGUuY2FsbCh0aGlzKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgdG9nZ2xlOiBmdW5jdGlvbihxdWVyeSkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgJGFjdGl2ZVRpdGxlID0gKHF1ZXJ5ICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgID8gKHR5cGVvZiBxdWVyeSA9PT0gJ251bWJlcicpXG4gICAgICAgICAgICAgICAgPyAkdGl0bGUuZXEocXVlcnkpXG4gICAgICAgICAgICAgICAgOiAkKHF1ZXJ5KS5jbG9zZXN0KHNlbGVjdG9yLnRpdGxlKVxuICAgICAgICAgICAgICA6ICQodGhpcykuY2xvc2VzdChzZWxlY3Rvci50aXRsZSksXG4gICAgICAgICAgICAkYWN0aXZlQ29udGVudCA9ICRhY3RpdmVUaXRsZS5uZXh0KCRjb250ZW50KSxcbiAgICAgICAgICAgIGlzQW5pbWF0aW5nID0gJGFjdGl2ZUNvbnRlbnQuaGFzQ2xhc3MoY2xhc3NOYW1lLmFuaW1hdGluZyksXG4gICAgICAgICAgICBpc0FjdGl2ZSAgICA9ICRhY3RpdmVDb250ZW50Lmhhc0NsYXNzKGNsYXNzTmFtZS5hY3RpdmUpLFxuICAgICAgICAgICAgaXNPcGVuICAgICAgPSAoaXNBY3RpdmUgJiYgIWlzQW5pbWF0aW5nKSxcbiAgICAgICAgICAgIGlzT3BlbmluZyAgID0gKCFpc0FjdGl2ZSAmJiBpc0FuaW1hdGluZylcbiAgICAgICAgICA7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdUb2dnbGluZyB2aXNpYmlsaXR5IG9mIGNvbnRlbnQnLCAkYWN0aXZlVGl0bGUpO1xuICAgICAgICAgIGlmKGlzT3BlbiB8fCBpc09wZW5pbmcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmNvbGxhcHNpYmxlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5jbG9zZS5jYWxsKCRhY3RpdmVUaXRsZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDYW5ub3QgY2xvc2UgYWNjb3JkaW9uIGNvbnRlbnQgY29sbGFwc2luZyBpcyBkaXNhYmxlZCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIG1vZHVsZS5vcGVuLmNhbGwoJGFjdGl2ZVRpdGxlKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgb3BlbjogZnVuY3Rpb24ocXVlcnkpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgICRhY3RpdmVUaXRsZSA9IChxdWVyeSAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICA/ICh0eXBlb2YgcXVlcnkgPT09ICdudW1iZXInKVxuICAgICAgICAgICAgICAgID8gJHRpdGxlLmVxKHF1ZXJ5KVxuICAgICAgICAgICAgICAgIDogJChxdWVyeSkuY2xvc2VzdChzZWxlY3Rvci50aXRsZSlcbiAgICAgICAgICAgICAgOiAkKHRoaXMpLmNsb3Nlc3Qoc2VsZWN0b3IudGl0bGUpLFxuICAgICAgICAgICAgJGFjdGl2ZUNvbnRlbnQgPSAkYWN0aXZlVGl0bGUubmV4dCgkY29udGVudCksXG4gICAgICAgICAgICBpc0FuaW1hdGluZyA9ICRhY3RpdmVDb250ZW50Lmhhc0NsYXNzKGNsYXNzTmFtZS5hbmltYXRpbmcpLFxuICAgICAgICAgICAgaXNBY3RpdmUgICAgPSAkYWN0aXZlQ29udGVudC5oYXNDbGFzcyhjbGFzc05hbWUuYWN0aXZlKSxcbiAgICAgICAgICAgIGlzT3BlbiAgICAgID0gKGlzQWN0aXZlIHx8IGlzQW5pbWF0aW5nKVxuICAgICAgICAgIDtcbiAgICAgICAgICBpZihpc09wZW4pIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWNjb3JkaW9uIGFscmVhZHkgb3Blbiwgc2tpcHBpbmcnLCAkYWN0aXZlQ29udGVudCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnT3BlbmluZyBhY2NvcmRpb24gY29udGVudCcsICRhY3RpdmVUaXRsZSk7XG4gICAgICAgICAgc2V0dGluZ3Mub25PcGVuaW5nLmNhbGwoJGFjdGl2ZUNvbnRlbnQpO1xuICAgICAgICAgIGlmKHNldHRpbmdzLmV4Y2x1c2l2ZSkge1xuICAgICAgICAgICAgbW9kdWxlLmNsb3NlT3RoZXJzLmNhbGwoJGFjdGl2ZVRpdGxlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgJGFjdGl2ZVRpdGxlXG4gICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSlcbiAgICAgICAgICA7XG4gICAgICAgICAgJGFjdGl2ZUNvbnRlbnRcbiAgICAgICAgICAgIC5zdG9wKHRydWUsIHRydWUpXG4gICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLmFuaW1hdGluZylcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYoc2V0dGluZ3MuYW5pbWF0ZUNoaWxkcmVuKSB7XG4gICAgICAgICAgICBpZigkLmZuLnRyYW5zaXRpb24gIT09IHVuZGVmaW5lZCAmJiAkbW9kdWxlLnRyYW5zaXRpb24oJ2lzIHN1cHBvcnRlZCcpKSB7XG4gICAgICAgICAgICAgICRhY3RpdmVDb250ZW50XG4gICAgICAgICAgICAgICAgLmNoaWxkcmVuKClcbiAgICAgICAgICAgICAgICAgIC50cmFuc2l0aW9uKHtcbiAgICAgICAgICAgICAgICAgICAgYW5pbWF0aW9uICAgOiAnZmFkZSBpbicsXG4gICAgICAgICAgICAgICAgICAgIHF1ZXVlICAgICAgIDogZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgIHVzZUZhaWxTYWZlIDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgZGVidWcgICAgICAgOiBzZXR0aW5ncy5kZWJ1ZyxcbiAgICAgICAgICAgICAgICAgICAgdmVyYm9zZSAgICAgOiBzZXR0aW5ncy52ZXJib3NlLFxuICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbiAgICA6IHNldHRpbmdzLmR1cmF0aW9uXG4gICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgJGFjdGl2ZUNvbnRlbnRcbiAgICAgICAgICAgICAgICAuY2hpbGRyZW4oKVxuICAgICAgICAgICAgICAgICAgLnN0b3AodHJ1ZSwgdHJ1ZSlcbiAgICAgICAgICAgICAgICAgIC5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMVxuICAgICAgICAgICAgICAgICAgfSwgc2V0dGluZ3MuZHVyYXRpb24sIG1vZHVsZS5yZXNldE9wYWNpdHkpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgJGFjdGl2ZUNvbnRlbnRcbiAgICAgICAgICAgIC5zbGlkZURvd24oc2V0dGluZ3MuZHVyYXRpb24sIHNldHRpbmdzLmVhc2luZywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICRhY3RpdmVDb250ZW50XG4gICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5hbmltYXRpbmcpXG4gICAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5hY3RpdmUpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgbW9kdWxlLnJlc2V0LmRpc3BsYXkuY2FsbCh0aGlzKTtcbiAgICAgICAgICAgICAgc2V0dGluZ3Mub25PcGVuLmNhbGwodGhpcyk7XG4gICAgICAgICAgICAgIHNldHRpbmdzLm9uQ2hhbmdlLmNhbGwodGhpcyk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBjbG9zZTogZnVuY3Rpb24ocXVlcnkpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgICRhY3RpdmVUaXRsZSA9IChxdWVyeSAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICA/ICh0eXBlb2YgcXVlcnkgPT09ICdudW1iZXInKVxuICAgICAgICAgICAgICAgID8gJHRpdGxlLmVxKHF1ZXJ5KVxuICAgICAgICAgICAgICAgIDogJChxdWVyeSkuY2xvc2VzdChzZWxlY3Rvci50aXRsZSlcbiAgICAgICAgICAgICAgOiAkKHRoaXMpLmNsb3Nlc3Qoc2VsZWN0b3IudGl0bGUpLFxuICAgICAgICAgICAgJGFjdGl2ZUNvbnRlbnQgPSAkYWN0aXZlVGl0bGUubmV4dCgkY29udGVudCksXG4gICAgICAgICAgICBpc0FuaW1hdGluZyAgICA9ICRhY3RpdmVDb250ZW50Lmhhc0NsYXNzKGNsYXNzTmFtZS5hbmltYXRpbmcpLFxuICAgICAgICAgICAgaXNBY3RpdmUgICAgICAgPSAkYWN0aXZlQ29udGVudC5oYXNDbGFzcyhjbGFzc05hbWUuYWN0aXZlKSxcbiAgICAgICAgICAgIGlzT3BlbmluZyAgICAgID0gKCFpc0FjdGl2ZSAmJiBpc0FuaW1hdGluZyksXG4gICAgICAgICAgICBpc0Nsb3NpbmcgICAgICA9IChpc0FjdGl2ZSAmJiBpc0FuaW1hdGluZylcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYoKGlzQWN0aXZlIHx8IGlzT3BlbmluZykgJiYgIWlzQ2xvc2luZykge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDbG9zaW5nIGFjY29yZGlvbiBjb250ZW50JywgJGFjdGl2ZUNvbnRlbnQpO1xuICAgICAgICAgICAgc2V0dGluZ3Mub25DbG9zaW5nLmNhbGwoJGFjdGl2ZUNvbnRlbnQpO1xuICAgICAgICAgICAgJGFjdGl2ZVRpdGxlXG4gICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuYWN0aXZlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJGFjdGl2ZUNvbnRlbnRcbiAgICAgICAgICAgICAgLnN0b3AodHJ1ZSwgdHJ1ZSlcbiAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5hbmltYXRpbmcpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5hbmltYXRlQ2hpbGRyZW4pIHtcbiAgICAgICAgICAgICAgaWYoJC5mbi50cmFuc2l0aW9uICE9PSB1bmRlZmluZWQgJiYgJG1vZHVsZS50cmFuc2l0aW9uKCdpcyBzdXBwb3J0ZWQnKSkge1xuICAgICAgICAgICAgICAgICRhY3RpdmVDb250ZW50XG4gICAgICAgICAgICAgICAgICAuY2hpbGRyZW4oKVxuICAgICAgICAgICAgICAgICAgICAudHJhbnNpdGlvbih7XG4gICAgICAgICAgICAgICAgICAgICAgYW5pbWF0aW9uICAgOiAnZmFkZSBvdXQnLFxuICAgICAgICAgICAgICAgICAgICAgIHF1ZXVlICAgICAgIDogZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgICAgdXNlRmFpbFNhZmUgOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgIGRlYnVnICAgICAgIDogc2V0dGluZ3MuZGVidWcsXG4gICAgICAgICAgICAgICAgICAgICAgdmVyYm9zZSAgICAgOiBzZXR0aW5ncy52ZXJib3NlLFxuICAgICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uICAgIDogc2V0dGluZ3MuZHVyYXRpb25cbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJGFjdGl2ZUNvbnRlbnRcbiAgICAgICAgICAgICAgICAgIC5jaGlsZHJlbigpXG4gICAgICAgICAgICAgICAgICAgIC5zdG9wKHRydWUsIHRydWUpXG4gICAgICAgICAgICAgICAgICAgIC5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAwXG4gICAgICAgICAgICAgICAgICAgIH0sIHNldHRpbmdzLmR1cmF0aW9uLCBtb2R1bGUucmVzZXRPcGFjaXR5KVxuICAgICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgJGFjdGl2ZUNvbnRlbnRcbiAgICAgICAgICAgICAgLnNsaWRlVXAoc2V0dGluZ3MuZHVyYXRpb24sIHNldHRpbmdzLmVhc2luZywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgJGFjdGl2ZUNvbnRlbnRcbiAgICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuYW5pbWF0aW5nKVxuICAgICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5hY3RpdmUpXG4gICAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICAgIG1vZHVsZS5yZXNldC5kaXNwbGF5LmNhbGwodGhpcyk7XG4gICAgICAgICAgICAgICAgc2V0dGluZ3Mub25DbG9zZS5jYWxsKHRoaXMpO1xuICAgICAgICAgICAgICAgIHNldHRpbmdzLm9uQ2hhbmdlLmNhbGwodGhpcyk7XG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGNsb3NlT3RoZXJzOiBmdW5jdGlvbihpbmRleCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgJGFjdGl2ZVRpdGxlID0gKGluZGV4ICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgID8gJHRpdGxlLmVxKGluZGV4KVxuICAgICAgICAgICAgICA6ICQodGhpcykuY2xvc2VzdChzZWxlY3Rvci50aXRsZSksXG4gICAgICAgICAgICAkcGFyZW50VGl0bGVzICAgID0gJGFjdGl2ZVRpdGxlLnBhcmVudHMoc2VsZWN0b3IuY29udGVudCkucHJldihzZWxlY3Rvci50aXRsZSksXG4gICAgICAgICAgICAkYWN0aXZlQWNjb3JkaW9uID0gJGFjdGl2ZVRpdGxlLmNsb3Nlc3Qoc2VsZWN0b3IuYWNjb3JkaW9uKSxcbiAgICAgICAgICAgIGFjdGl2ZVNlbGVjdG9yICAgPSBzZWxlY3Rvci50aXRsZSArICcuJyArIGNsYXNzTmFtZS5hY3RpdmUgKyAnOnZpc2libGUnLFxuICAgICAgICAgICAgYWN0aXZlQ29udGVudCAgICA9IHNlbGVjdG9yLmNvbnRlbnQgKyAnLicgKyBjbGFzc05hbWUuYWN0aXZlICsgJzp2aXNpYmxlJyxcbiAgICAgICAgICAgICRvcGVuVGl0bGVzLFxuICAgICAgICAgICAgJG5lc3RlZFRpdGxlcyxcbiAgICAgICAgICAgICRvcGVuQ29udGVudHNcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYoc2V0dGluZ3MuY2xvc2VOZXN0ZWQpIHtcbiAgICAgICAgICAgICRvcGVuVGl0bGVzICAgPSAkYWN0aXZlQWNjb3JkaW9uLmZpbmQoYWN0aXZlU2VsZWN0b3IpLm5vdCgkcGFyZW50VGl0bGVzKTtcbiAgICAgICAgICAgICRvcGVuQ29udGVudHMgPSAkb3BlblRpdGxlcy5uZXh0KCRjb250ZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAkb3BlblRpdGxlcyAgID0gJGFjdGl2ZUFjY29yZGlvbi5maW5kKGFjdGl2ZVNlbGVjdG9yKS5ub3QoJHBhcmVudFRpdGxlcyk7XG4gICAgICAgICAgICAkbmVzdGVkVGl0bGVzID0gJGFjdGl2ZUFjY29yZGlvbi5maW5kKGFjdGl2ZUNvbnRlbnQpLmZpbmQoYWN0aXZlU2VsZWN0b3IpLm5vdCgkcGFyZW50VGl0bGVzKTtcbiAgICAgICAgICAgICRvcGVuVGl0bGVzICAgPSAkb3BlblRpdGxlcy5ub3QoJG5lc3RlZFRpdGxlcyk7XG4gICAgICAgICAgICAkb3BlbkNvbnRlbnRzID0gJG9wZW5UaXRsZXMubmV4dCgkY29udGVudCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKCAoJG9wZW5UaXRsZXMubGVuZ3RoID4gMCkgKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0V4Y2x1c2l2ZSBlbmFibGVkLCBjbG9zaW5nIG90aGVyIGNvbnRlbnQnLCAkb3BlblRpdGxlcyk7XG4gICAgICAgICAgICAkb3BlblRpdGxlc1xuICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgICRvcGVuQ29udGVudHNcbiAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5hbmltYXRpbmcpXG4gICAgICAgICAgICAgIC5zdG9wKHRydWUsIHRydWUpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5hbmltYXRlQ2hpbGRyZW4pIHtcbiAgICAgICAgICAgICAgaWYoJC5mbi50cmFuc2l0aW9uICE9PSB1bmRlZmluZWQgJiYgJG1vZHVsZS50cmFuc2l0aW9uKCdpcyBzdXBwb3J0ZWQnKSkge1xuICAgICAgICAgICAgICAgICRvcGVuQ29udGVudHNcbiAgICAgICAgICAgICAgICAgIC5jaGlsZHJlbigpXG4gICAgICAgICAgICAgICAgICAgIC50cmFuc2l0aW9uKHtcbiAgICAgICAgICAgICAgICAgICAgICBhbmltYXRpb24gICA6ICdmYWRlIG91dCcsXG4gICAgICAgICAgICAgICAgICAgICAgdXNlRmFpbFNhZmUgOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgIGRlYnVnICAgICAgIDogc2V0dGluZ3MuZGVidWcsXG4gICAgICAgICAgICAgICAgICAgICAgdmVyYm9zZSAgICAgOiBzZXR0aW5ncy52ZXJib3NlLFxuICAgICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uICAgIDogc2V0dGluZ3MuZHVyYXRpb25cbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJG9wZW5Db250ZW50c1xuICAgICAgICAgICAgICAgICAgLmNoaWxkcmVuKClcbiAgICAgICAgICAgICAgICAgICAgLnN0b3AodHJ1ZSwgdHJ1ZSlcbiAgICAgICAgICAgICAgICAgICAgLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDBcbiAgICAgICAgICAgICAgICAgICAgfSwgc2V0dGluZ3MuZHVyYXRpb24sIG1vZHVsZS5yZXNldE9wYWNpdHkpXG4gICAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAkb3BlbkNvbnRlbnRzXG4gICAgICAgICAgICAgIC5zbGlkZVVwKHNldHRpbmdzLmR1cmF0aW9uICwgc2V0dGluZ3MuZWFzaW5nLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5hY3RpdmUpO1xuICAgICAgICAgICAgICAgIG1vZHVsZS5yZXNldC5kaXNwbGF5LmNhbGwodGhpcyk7XG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlc2V0OiB7XG5cbiAgICAgICAgICBkaXNwbGF5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZW1vdmluZyBpbmxpbmUgZGlzcGxheSBmcm9tIGVsZW1lbnQnLCB0aGlzKTtcbiAgICAgICAgICAgICQodGhpcykuY3NzKCdkaXNwbGF5JywgJycpO1xuICAgICAgICAgICAgaWYoICQodGhpcykuYXR0cignc3R5bGUnKSA9PT0gJycpIHtcbiAgICAgICAgICAgICAgJCh0aGlzKVxuICAgICAgICAgICAgICAgIC5hdHRyKCdzdHlsZScsICcnKVxuICAgICAgICAgICAgICAgIC5yZW1vdmVBdHRyKCdzdHlsZScpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuXG4gICAgICAgICAgb3BhY2l0eTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVtb3ZpbmcgaW5saW5lIG9wYWNpdHkgZnJvbSBlbGVtZW50JywgdGhpcyk7XG4gICAgICAgICAgICAkKHRoaXMpLmNzcygnb3BhY2l0eScsICcnKTtcbiAgICAgICAgICAgIGlmKCAkKHRoaXMpLmF0dHIoJ3N0eWxlJykgPT09ICcnKSB7XG4gICAgICAgICAgICAgICQodGhpcylcbiAgICAgICAgICAgICAgICAuYXR0cignc3R5bGUnLCAnJylcbiAgICAgICAgICAgICAgICAucmVtb3ZlQXR0cignc3R5bGUnKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcblxuICAgICAgICB9LFxuXG4gICAgICAgIHNldHRpbmc6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdDaGFuZ2luZyBzZXR0aW5nJywgbmFtZSwgdmFsdWUpO1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBzZXR0aW5ncywgbmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgaWYoJC5pc1BsYWluT2JqZWN0KHNldHRpbmdzW25hbWVdKSkge1xuICAgICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBzZXR0aW5nc1tuYW1lXSwgdmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHNldHRpbmdzW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHNldHRpbmdzW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgaW50ZXJuYWw6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdDaGFuZ2luZyBpbnRlcm5hbCcsIG5hbWUsIHZhbHVlKTtcbiAgICAgICAgICBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBtb2R1bGUsIG5hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZVtuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGVbbmFtZV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBkZWJ1ZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy5kZWJ1Zykge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZyA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB2ZXJib3NlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50ICYmIHNldHRpbmdzLnZlcmJvc2UgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGVycm9yOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50KSB7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuZXJyb3IsIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBwZXJmb3JtYW5jZToge1xuICAgICAgICAgIGxvZzogZnVuY3Rpb24obWVzc2FnZSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lLFxuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lLFxuICAgICAgICAgICAgICBwcmV2aW91c1RpbWVcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lICAgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lICA9IHRpbWUgfHwgY3VycmVudFRpbWU7XG4gICAgICAgICAgICAgIGV4ZWN1dGlvblRpbWUgPSBjdXJyZW50VGltZSAtIHByZXZpb3VzVGltZTtcbiAgICAgICAgICAgICAgdGltZSAgICAgICAgICA9IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBwZXJmb3JtYW5jZS5wdXNoKHtcbiAgICAgICAgICAgICAgICAnTmFtZScgICAgICAgICAgIDogbWVzc2FnZVswXSxcbiAgICAgICAgICAgICAgICAnQXJndW1lbnRzJyAgICAgIDogW10uc2xpY2UuY2FsbChtZXNzYWdlLCAxKSB8fCAnJyxcbiAgICAgICAgICAgICAgICAnRWxlbWVudCcgICAgICAgIDogZWxlbWVudCxcbiAgICAgICAgICAgICAgICAnRXhlY3V0aW9uIFRpbWUnIDogZXhlY3V0aW9uVGltZVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyID0gc2V0VGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UuZGlzcGxheSwgNTAwKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc3BsYXk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHRpdGxlID0gc2V0dGluZ3MubmFtZSArICc6JyxcbiAgICAgICAgICAgICAgdG90YWxUaW1lID0gMFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgdGltZSA9IGZhbHNlO1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lcik7XG4gICAgICAgICAgICAkLmVhY2gocGVyZm9ybWFuY2UsIGZ1bmN0aW9uKGluZGV4LCBkYXRhKSB7XG4gICAgICAgICAgICAgIHRvdGFsVGltZSArPSBkYXRhWydFeGVjdXRpb24gVGltZSddO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB0aXRsZSArPSAnICcgKyB0b3RhbFRpbWUgKyAnbXMnO1xuICAgICAgICAgICAgaWYobW9kdWxlU2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgdGl0bGUgKz0gJyBcXCcnICsgbW9kdWxlU2VsZWN0b3IgKyAnXFwnJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCAoY29uc29sZS5ncm91cCAhPT0gdW5kZWZpbmVkIHx8IGNvbnNvbGUudGFibGUgIT09IHVuZGVmaW5lZCkgJiYgcGVyZm9ybWFuY2UubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKHRpdGxlKTtcbiAgICAgICAgICAgICAgaWYoY29uc29sZS50YWJsZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUudGFibGUocGVyZm9ybWFuY2UpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGFbJ05hbWUnXSArICc6ICcgKyBkYXRhWydFeGVjdXRpb24gVGltZSddKydtcycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHBlcmZvcm1hbmNlID0gW107XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnZva2U6IGZ1bmN0aW9uKHF1ZXJ5LCBwYXNzZWRBcmd1bWVudHMsIGNvbnRleHQpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIG9iamVjdCA9IGluc3RhbmNlLFxuICAgICAgICAgICAgbWF4RGVwdGgsXG4gICAgICAgICAgICBmb3VuZCxcbiAgICAgICAgICAgIHJlc3BvbnNlXG4gICAgICAgICAgO1xuICAgICAgICAgIHBhc3NlZEFyZ3VtZW50cyA9IHBhc3NlZEFyZ3VtZW50cyB8fCBxdWVyeUFyZ3VtZW50cztcbiAgICAgICAgICBjb250ZXh0ICAgICAgICAgPSBlbGVtZW50ICAgICAgICAgfHwgY29udGV4dDtcbiAgICAgICAgICBpZih0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycgJiYgb2JqZWN0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5ICAgID0gcXVlcnkuc3BsaXQoL1tcXC4gXS8pO1xuICAgICAgICAgICAgbWF4RGVwdGggPSBxdWVyeS5sZW5ndGggLSAxO1xuICAgICAgICAgICAgJC5lYWNoKHF1ZXJ5LCBmdW5jdGlvbihkZXB0aCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgdmFyIGNhbWVsQ2FzZVZhbHVlID0gKGRlcHRoICE9IG1heERlcHRoKVxuICAgICAgICAgICAgICAgID8gdmFsdWUgKyBxdWVyeVtkZXB0aCArIDFdLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcXVlcnlbZGVwdGggKyAxXS5zbGljZSgxKVxuICAgICAgICAgICAgICAgIDogcXVlcnlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbY2FtZWxDYXNlVmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoICQuaXNQbGFpbk9iamVjdCggb2JqZWN0W3ZhbHVlXSApICYmIChkZXB0aCAhPSBtYXhEZXB0aCkgKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBvYmplY3RbdmFsdWVdICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3IubWV0aG9kLCBxdWVyeSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCAkLmlzRnVuY3Rpb24oIGZvdW5kICkgKSB7XG4gICAgICAgICAgICByZXNwb25zZSA9IGZvdW5kLmFwcGx5KGNvbnRleHQsIHBhc3NlZEFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYoZm91bmQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZDtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoJC5pc0FycmF5KHJldHVybmVkVmFsdWUpKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlLnB1c2gocmVzcG9uc2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IFtyZXR1cm5lZFZhbHVlLCByZXNwb25zZV07XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYocmVzcG9uc2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IHJlc3BvbnNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gZm91bmQ7XG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgICBpZihtZXRob2RJbnZva2VkKSB7XG4gICAgICAgIGlmKGluc3RhbmNlID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbnZva2UocXVlcnkpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGlmKGluc3RhbmNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBpbnN0YW5jZS5pbnZva2UoJ2Rlc3Ryb3knKTtcbiAgICAgICAgfVxuICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgfVxuICAgIH0pXG4gIDtcbiAgcmV0dXJuIChyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpXG4gICAgPyByZXR1cm5lZFZhbHVlXG4gICAgOiB0aGlzXG4gIDtcbn07XG5cbiQuZm4uYWNjb3JkaW9uLnNldHRpbmdzID0ge1xuXG4gIG5hbWUgICAgICAgICAgICA6ICdBY2NvcmRpb24nLFxuICBuYW1lc3BhY2UgICAgICAgOiAnYWNjb3JkaW9uJyxcblxuICBzaWxlbnQgICAgICAgICAgOiBmYWxzZSxcbiAgZGVidWcgICAgICAgICAgIDogZmFsc2UsXG4gIHZlcmJvc2UgICAgICAgICA6IGZhbHNlLFxuICBwZXJmb3JtYW5jZSAgICAgOiB0cnVlLFxuXG4gIG9uICAgICAgICAgICAgICA6ICdjbGljaycsIC8vIGV2ZW50IG9uIHRpdGxlIHRoYXQgb3BlbnMgYWNjb3JkaW9uXG5cbiAgb2JzZXJ2ZUNoYW5nZXMgIDogdHJ1ZSwgIC8vIHdoZXRoZXIgYWNjb3JkaW9uIHNob3VsZCBhdXRvbWF0aWNhbGx5IHJlZnJlc2ggb24gRE9NIGluc2VydGlvblxuXG4gIGV4Y2x1c2l2ZSAgICAgICA6IHRydWUsICAvLyB3aGV0aGVyIGEgc2luZ2xlIGFjY29yZGlvbiBjb250ZW50IHBhbmVsIHNob3VsZCBiZSBvcGVuIGF0IG9uY2VcbiAgY29sbGFwc2libGUgICAgIDogdHJ1ZSwgIC8vIHdoZXRoZXIgYWNjb3JkaW9uIGNvbnRlbnQgY2FuIGJlIGNsb3NlZFxuICBjbG9zZU5lc3RlZCAgICAgOiBmYWxzZSwgLy8gd2hldGhlciBuZXN0ZWQgY29udGVudCBzaG91bGQgYmUgY2xvc2VkIHdoZW4gYSBwYW5lbCBpcyBjbG9zZWRcbiAgYW5pbWF0ZUNoaWxkcmVuIDogdHJ1ZSwgIC8vIHdoZXRoZXIgY2hpbGRyZW4gb3BhY2l0eSBzaG91bGQgYmUgYW5pbWF0ZWRcblxuICBkdXJhdGlvbiAgICAgICAgOiAzNTAsIC8vIGR1cmF0aW9uIG9mIGFuaW1hdGlvblxuICBlYXNpbmcgICAgICAgICAgOiAnZWFzZU91dFF1YWQnLCAvLyBlYXNpbmcgZXF1YXRpb24gZm9yIGFuaW1hdGlvblxuXG5cbiAgb25PcGVuaW5nICAgICAgIDogZnVuY3Rpb24oKXt9LCAvLyBjYWxsYmFjayBiZWZvcmUgb3BlbiBhbmltYXRpb25cbiAgb25PcGVuICAgICAgICAgIDogZnVuY3Rpb24oKXt9LCAvLyBjYWxsYmFjayBhZnRlciBvcGVuIGFuaW1hdGlvblxuICBvbkNsb3NpbmcgICAgICAgOiBmdW5jdGlvbigpe30sIC8vIGNhbGxiYWNrIGJlZm9yZSBjbG9zaW5nIGFuaW1hdGlvblxuICBvbkNsb3NlICAgICAgICAgOiBmdW5jdGlvbigpe30sIC8vIGNhbGxiYWNrIGFmdGVyIGNsb3NpbmcgYW5pbWF0aW9uXG4gIG9uQ2hhbmdlICAgICAgICA6IGZ1bmN0aW9uKCl7fSwgLy8gY2FsbGJhY2sgYWZ0ZXIgY2xvc2luZyBvciBvcGVuaW5nIGFuaW1hdGlvblxuXG4gIGVycm9yOiB7XG4gICAgbWV0aG9kIDogJ1RoZSBtZXRob2QgeW91IGNhbGxlZCBpcyBub3QgZGVmaW5lZCdcbiAgfSxcblxuICBjbGFzc05hbWUgICA6IHtcbiAgICBhY3RpdmUgICAgOiAnYWN0aXZlJyxcbiAgICBhbmltYXRpbmcgOiAnYW5pbWF0aW5nJ1xuICB9LFxuXG4gIHNlbGVjdG9yICAgIDoge1xuICAgIGFjY29yZGlvbiA6ICcuYWNjb3JkaW9uJyxcbiAgICB0aXRsZSAgICAgOiAnLnRpdGxlJyxcbiAgICB0cmlnZ2VyICAgOiAnLnRpdGxlJyxcbiAgICBjb250ZW50ICAgOiAnLmNvbnRlbnQnXG4gIH1cblxufTtcblxuLy8gQWRkcyBlYXNpbmdcbiQuZXh0ZW5kKCAkLmVhc2luZywge1xuICBlYXNlT3V0UXVhZDogZnVuY3Rpb24gKHgsIHQsIGIsIGMsIGQpIHtcbiAgICByZXR1cm4gLWMgKih0Lz1kKSoodC0yKSArIGI7XG4gIH1cbn0pO1xuXG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7XG5cbiJdfQ==