/*!
 * # Semantic UI - Sidebar
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.sidebar = function (parameters) {
    var $allModules = $(this),
        $window = $(window),
        $document = $(document),
        $html = $('html'),
        $head = $('head'),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
      setTimeout(callback, 0);
    },
        returnedValue;

    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.sidebar.settings, parameters) : $.extend({}, $.fn.sidebar.settings),
          selector = settings.selector,
          className = settings.className,
          namespace = settings.namespace,
          regExp = settings.regExp,
          error = settings.error,
          eventNamespace = '.' + namespace,
          moduleNamespace = 'module-' + namespace,
          $module = $(this),
          $context = $(settings.context),
          $sidebars = $module.children(selector.sidebar),
          $fixed = $context.children(selector.fixed),
          $pusher = $context.children(selector.pusher),
          $style,
          element = this,
          instance = $module.data(moduleNamespace),
          elementNamespace,
          id,
          currentScroll,
          transitionEvent,
          module;

      module = {

        initialize: function () {
          module.debug('Initializing sidebar', parameters);

          module.create.id();

          transitionEvent = module.get.transitionEvent();

          if (module.is.ios()) {
            module.set.ios();
          }

          // avoids locking rendering if initialized in onReady
          if (settings.delaySetup) {
            requestAnimationFrame(module.setup.layout);
          } else {
            module.setup.layout();
          }

          requestAnimationFrame(function () {
            module.setup.cache();
          });

          module.instantiate();
        },

        instantiate: function () {
          module.verbose('Storing instance of module', module);
          instance = module;
          $module.data(moduleNamespace, module);
        },

        create: {
          id: function () {
            id = (Math.random().toString(16) + '000000000').substr(2, 8);
            elementNamespace = '.' + id;
            module.verbose('Creating unique id for element', id);
          }
        },

        destroy: function () {
          module.verbose('Destroying previous module for', $module);
          $module.off(eventNamespace).removeData(moduleNamespace);
          if (module.is.ios()) {
            module.remove.ios();
          }
          // bound by uuid
          $context.off(elementNamespace);
          $window.off(elementNamespace);
          $document.off(elementNamespace);
        },

        event: {
          clickaway: function (event) {
            var clickedInPusher = $pusher.find(event.target).length > 0 || $pusher.is(event.target),
                clickedContext = $context.is(event.target);
            if (clickedInPusher) {
              module.verbose('User clicked on dimmed page');
              module.hide();
            }
            if (clickedContext) {
              module.verbose('User clicked on dimmable context (scaled out page)');
              module.hide();
            }
          },
          touch: function (event) {
            //event.stopPropagation();
          },
          containScroll: function (event) {
            if (element.scrollTop <= 0) {
              element.scrollTop = 1;
            }
            if (element.scrollTop + element.offsetHeight >= element.scrollHeight) {
              element.scrollTop = element.scrollHeight - element.offsetHeight - 1;
            }
          },
          scroll: function (event) {
            if ($(event.target).closest(selector.sidebar).length === 0) {
              event.preventDefault();
            }
          }
        },

        bind: {
          clickaway: function () {
            module.verbose('Adding clickaway events to context', $context);
            if (settings.closable) {
              $context.on('click' + elementNamespace, module.event.clickaway).on('touchend' + elementNamespace, module.event.clickaway);
            }
          },
          scrollLock: function () {
            if (settings.scrollLock) {
              module.debug('Disabling page scroll');
              $window.on('DOMMouseScroll' + elementNamespace, module.event.scroll);
            }
            module.verbose('Adding events to contain sidebar scroll');
            $document.on('touchmove' + elementNamespace, module.event.touch);
            $module.on('scroll' + eventNamespace, module.event.containScroll);
          }
        },
        unbind: {
          clickaway: function () {
            module.verbose('Removing clickaway events from context', $context);
            $context.off(elementNamespace);
          },
          scrollLock: function () {
            module.verbose('Removing scroll lock from page');
            $document.off(elementNamespace);
            $window.off(elementNamespace);
            $module.off('scroll' + eventNamespace);
          }
        },

        add: {
          inlineCSS: function () {
            var width = module.cache.width || $module.outerWidth(),
                height = module.cache.height || $module.outerHeight(),
                isRTL = module.is.rtl(),
                direction = module.get.direction(),
                distance = {
              left: width,
              right: -width,
              top: height,
              bottom: -height
            },
                style;

            if (isRTL) {
              module.verbose('RTL detected, flipping widths');
              distance.left = -width;
              distance.right = width;
            }

            style = '<style>';

            if (direction === 'left' || direction === 'right') {
              module.debug('Adding CSS rules for animation distance', width);
              style += '' + ' .ui.visible.' + direction + '.sidebar ~ .fixed,' + ' .ui.visible.' + direction + '.sidebar ~ .pusher {' + '   -webkit-transform: translate3d(' + distance[direction] + 'px, 0, 0);' + '           transform: translate3d(' + distance[direction] + 'px, 0, 0);' + ' }';
            } else if (direction === 'top' || direction == 'bottom') {
              style += '' + ' .ui.visible.' + direction + '.sidebar ~ .fixed,' + ' .ui.visible.' + direction + '.sidebar ~ .pusher {' + '   -webkit-transform: translate3d(0, ' + distance[direction] + 'px, 0);' + '           transform: translate3d(0, ' + distance[direction] + 'px, 0);' + ' }';
            }

            /* IE is only browser not to create context with transforms */
            /* https://www.w3.org/Bugs/Public/show_bug.cgi?id=16328 */
            if (module.is.ie()) {
              if (direction === 'left' || direction === 'right') {
                module.debug('Adding CSS rules for animation distance', width);
                style += '' + ' body.pushable > .ui.visible.' + direction + '.sidebar ~ .pusher:after {' + '   -webkit-transform: translate3d(' + distance[direction] + 'px, 0, 0);' + '           transform: translate3d(' + distance[direction] + 'px, 0, 0);' + ' }';
              } else if (direction === 'top' || direction == 'bottom') {
                style += '' + ' body.pushable > .ui.visible.' + direction + '.sidebar ~ .pusher:after {' + '   -webkit-transform: translate3d(0, ' + distance[direction] + 'px, 0);' + '           transform: translate3d(0, ' + distance[direction] + 'px, 0);' + ' }';
              }
              /* opposite sides visible forces content overlay */
              style += '' + ' body.pushable > .ui.visible.left.sidebar ~ .ui.visible.right.sidebar ~ .pusher:after,' + ' body.pushable > .ui.visible.right.sidebar ~ .ui.visible.left.sidebar ~ .pusher:after {' + '   -webkit-transform: translate3d(0px, 0, 0);' + '           transform: translate3d(0px, 0, 0);' + ' }';
            }
            style += '</style>';
            $style = $(style).appendTo($head);
            module.debug('Adding sizing css to head', $style);
          }
        },

        refresh: function () {
          module.verbose('Refreshing selector cache');
          $context = $(settings.context);
          $sidebars = $context.children(selector.sidebar);
          $pusher = $context.children(selector.pusher);
          $fixed = $context.children(selector.fixed);
          module.clear.cache();
        },

        refreshSidebars: function () {
          module.verbose('Refreshing other sidebars');
          $sidebars = $context.children(selector.sidebar);
        },

        repaint: function () {
          module.verbose('Forcing repaint event');
          element.style.display = 'none';
          var ignored = element.offsetHeight;
          element.scrollTop = element.scrollTop;
          element.style.display = '';
        },

        setup: {
          cache: function () {
            module.cache = {
              width: $module.outerWidth(),
              height: $module.outerHeight(),
              rtl: $module.css('direction') == 'rtl'
            };
          },
          layout: function () {
            if ($context.children(selector.pusher).length === 0) {
              module.debug('Adding wrapper element for sidebar');
              module.error(error.pusher);
              $pusher = $('<div class="pusher" />');
              $context.children().not(selector.omitted).not($sidebars).wrapAll($pusher);
              module.refresh();
            }
            if ($module.nextAll(selector.pusher).length === 0 || $module.nextAll(selector.pusher)[0] !== $pusher[0]) {
              module.debug('Moved sidebar to correct parent element');
              module.error(error.movedSidebar, element);
              $module.detach().prependTo($context);
              module.refresh();
            }
            module.clear.cache();
            module.set.pushable();
            module.set.direction();
          }
        },

        attachEvents: function (selector, event) {
          var $toggle = $(selector);
          event = $.isFunction(module[event]) ? module[event] : module.toggle;
          if ($toggle.length > 0) {
            module.debug('Attaching sidebar events to element', selector, event);
            $toggle.on('click' + eventNamespace, event);
          } else {
            module.error(error.notFound, selector);
          }
        },

        show: function (callback) {
          callback = $.isFunction(callback) ? callback : function () {};
          if (module.is.hidden()) {
            module.refreshSidebars();
            if (settings.overlay) {
              module.error(error.overlay);
              settings.transition = 'overlay';
            }
            module.refresh();
            if (module.othersActive()) {
              module.debug('Other sidebars currently visible');
              if (settings.exclusive) {
                // if not overlay queue animation after hide
                if (settings.transition != 'overlay') {
                  module.hideOthers(module.show);
                  return;
                } else {
                  module.hideOthers();
                }
              } else {
                settings.transition = 'overlay';
              }
            }
            module.pushPage(function () {
              callback.call(element);
              settings.onShow.call(element);
            });
            settings.onChange.call(element);
            settings.onVisible.call(element);
          } else {
            module.debug('Sidebar is already visible');
          }
        },

        hide: function (callback) {
          callback = $.isFunction(callback) ? callback : function () {};
          if (module.is.visible() || module.is.animating()) {
            module.debug('Hiding sidebar', callback);
            module.refreshSidebars();
            module.pullPage(function () {
              callback.call(element);
              settings.onHidden.call(element);
            });
            settings.onChange.call(element);
            settings.onHide.call(element);
          }
        },

        othersAnimating: function () {
          return $sidebars.not($module).filter('.' + className.animating).length > 0;
        },
        othersVisible: function () {
          return $sidebars.not($module).filter('.' + className.visible).length > 0;
        },
        othersActive: function () {
          return module.othersVisible() || module.othersAnimating();
        },

        hideOthers: function (callback) {
          var $otherSidebars = $sidebars.not($module).filter('.' + className.visible),
              sidebarCount = $otherSidebars.length,
              callbackCount = 0;
          callback = callback || function () {};
          $otherSidebars.sidebar('hide', function () {
            callbackCount++;
            if (callbackCount == sidebarCount) {
              callback();
            }
          });
        },

        toggle: function () {
          module.verbose('Determining toggled direction');
          if (module.is.hidden()) {
            module.show();
          } else {
            module.hide();
          }
        },

        pushPage: function (callback) {
          var transition = module.get.transition(),
              $transition = transition === 'overlay' || module.othersActive() ? $module : $pusher,
              animate,
              dim,
              transitionEnd;
          callback = $.isFunction(callback) ? callback : function () {};
          if (settings.transition == 'scale down') {
            module.scrollToTop();
          }
          module.set.transition(transition);
          module.repaint();
          animate = function () {
            module.bind.clickaway();
            module.add.inlineCSS();
            module.set.animating();
            module.set.visible();
          };
          dim = function () {
            module.set.dimmed();
          };
          transitionEnd = function (event) {
            if (event.target == $transition[0]) {
              $transition.off(transitionEvent + elementNamespace, transitionEnd);
              module.remove.animating();
              module.bind.scrollLock();
              callback.call(element);
            }
          };
          $transition.off(transitionEvent + elementNamespace);
          $transition.on(transitionEvent + elementNamespace, transitionEnd);
          requestAnimationFrame(animate);
          if (settings.dimPage && !module.othersVisible()) {
            requestAnimationFrame(dim);
          }
        },

        pullPage: function (callback) {
          var transition = module.get.transition(),
              $transition = transition == 'overlay' || module.othersActive() ? $module : $pusher,
              animate,
              transitionEnd;
          callback = $.isFunction(callback) ? callback : function () {};
          module.verbose('Removing context push state', module.get.direction());

          module.unbind.clickaway();
          module.unbind.scrollLock();

          animate = function () {
            module.set.transition(transition);
            module.set.animating();
            module.remove.visible();
            if (settings.dimPage && !module.othersVisible()) {
              $pusher.removeClass(className.dimmed);
            }
          };
          transitionEnd = function (event) {
            if (event.target == $transition[0]) {
              $transition.off(transitionEvent + elementNamespace, transitionEnd);
              module.remove.animating();
              module.remove.transition();
              module.remove.inlineCSS();
              if (transition == 'scale down' || settings.returnScroll && module.is.mobile()) {
                module.scrollBack();
              }
              callback.call(element);
            }
          };
          $transition.off(transitionEvent + elementNamespace);
          $transition.on(transitionEvent + elementNamespace, transitionEnd);
          requestAnimationFrame(animate);
        },

        scrollToTop: function () {
          module.verbose('Scrolling to top of page to avoid animation issues');
          currentScroll = $(window).scrollTop();
          $module.scrollTop(0);
          window.scrollTo(0, 0);
        },

        scrollBack: function () {
          module.verbose('Scrolling back to original page position');
          window.scrollTo(0, currentScroll);
        },

        clear: {
          cache: function () {
            module.verbose('Clearing cached dimensions');
            module.cache = {};
          }
        },

        set: {

          // ios only (scroll on html not document). This prevent auto-resize canvas/scroll in ios
          ios: function () {
            $html.addClass(className.ios);
          },

          // container
          pushed: function () {
            $context.addClass(className.pushed);
          },
          pushable: function () {
            $context.addClass(className.pushable);
          },

          // pusher
          dimmed: function () {
            $pusher.addClass(className.dimmed);
          },

          // sidebar
          active: function () {
            $module.addClass(className.active);
          },
          animating: function () {
            $module.addClass(className.animating);
          },
          transition: function (transition) {
            transition = transition || module.get.transition();
            $module.addClass(transition);
          },
          direction: function (direction) {
            direction = direction || module.get.direction();
            $module.addClass(className[direction]);
          },
          visible: function () {
            $module.addClass(className.visible);
          },
          overlay: function () {
            $module.addClass(className.overlay);
          }
        },
        remove: {

          inlineCSS: function () {
            module.debug('Removing inline css styles', $style);
            if ($style && $style.length > 0) {
              $style.remove();
            }
          },

          // ios scroll on html not document
          ios: function () {
            $html.removeClass(className.ios);
          },

          // context
          pushed: function () {
            $context.removeClass(className.pushed);
          },
          pushable: function () {
            $context.removeClass(className.pushable);
          },

          // sidebar
          active: function () {
            $module.removeClass(className.active);
          },
          animating: function () {
            $module.removeClass(className.animating);
          },
          transition: function (transition) {
            transition = transition || module.get.transition();
            $module.removeClass(transition);
          },
          direction: function (direction) {
            direction = direction || module.get.direction();
            $module.removeClass(className[direction]);
          },
          visible: function () {
            $module.removeClass(className.visible);
          },
          overlay: function () {
            $module.removeClass(className.overlay);
          }
        },

        get: {
          direction: function () {
            if ($module.hasClass(className.top)) {
              return className.top;
            } else if ($module.hasClass(className.right)) {
              return className.right;
            } else if ($module.hasClass(className.bottom)) {
              return className.bottom;
            }
            return className.left;
          },
          transition: function () {
            var direction = module.get.direction(),
                transition;
            transition = module.is.mobile() ? settings.mobileTransition == 'auto' ? settings.defaultTransition.mobile[direction] : settings.mobileTransition : settings.transition == 'auto' ? settings.defaultTransition.computer[direction] : settings.transition;
            module.verbose('Determined transition', transition);
            return transition;
          },
          transitionEvent: function () {
            var element = document.createElement('element'),
                transitions = {
              'transition': 'transitionend',
              'OTransition': 'oTransitionEnd',
              'MozTransition': 'transitionend',
              'WebkitTransition': 'webkitTransitionEnd'
            },
                transition;
            for (transition in transitions) {
              if (element.style[transition] !== undefined) {
                return transitions[transition];
              }
            }
          }
        },

        is: {

          ie: function () {
            var isIE11 = !window.ActiveXObject && 'ActiveXObject' in window,
                isIE = 'ActiveXObject' in window;
            return isIE11 || isIE;
          },

          ios: function () {
            var userAgent = navigator.userAgent,
                isIOS = userAgent.match(regExp.ios),
                isMobileChrome = userAgent.match(regExp.mobileChrome);
            if (isIOS && !isMobileChrome) {
              module.verbose('Browser was found to be iOS', userAgent);
              return true;
            } else {
              return false;
            }
          },
          mobile: function () {
            var userAgent = navigator.userAgent,
                isMobile = userAgent.match(regExp.mobile);
            if (isMobile) {
              module.verbose('Browser was found to be mobile', userAgent);
              return true;
            } else {
              module.verbose('Browser is not mobile, using regular transition', userAgent);
              return false;
            }
          },
          hidden: function () {
            return !module.is.visible();
          },
          visible: function () {
            return $module.hasClass(className.visible);
          },
          // alias
          open: function () {
            return module.is.visible();
          },
          closed: function () {
            return module.is.hidden();
          },
          vertical: function () {
            return $module.hasClass(className.top);
          },
          animating: function () {
            return $context.hasClass(className.animating);
          },
          rtl: function () {
            if (module.cache.rtl === undefined) {
              module.cache.rtl = $module.css('direction') == 'rtl';
            }
            return module.cache.rtl;
          }
        },

        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                module.error(error.method, query);
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };

      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          module.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.sidebar.settings = {

    name: 'Sidebar',
    namespace: 'sidebar',

    silent: false,
    debug: false,
    verbose: false,
    performance: true,

    transition: 'auto',
    mobileTransition: 'auto',

    defaultTransition: {
      computer: {
        left: 'uncover',
        right: 'uncover',
        top: 'overlay',
        bottom: 'overlay'
      },
      mobile: {
        left: 'uncover',
        right: 'uncover',
        top: 'overlay',
        bottom: 'overlay'
      }
    },

    context: 'body',
    exclusive: false,
    closable: true,
    dimPage: true,
    scrollLock: false,
    returnScroll: false,
    delaySetup: false,

    duration: 500,

    onChange: function () {},
    onShow: function () {},
    onHide: function () {},

    onHidden: function () {},
    onVisible: function () {},

    className: {
      active: 'active',
      animating: 'animating',
      dimmed: 'dimmed',
      ios: 'ios',
      pushable: 'pushable',
      pushed: 'pushed',
      right: 'right',
      top: 'top',
      left: 'left',
      bottom: 'bottom',
      visible: 'visible'
    },

    selector: {
      fixed: '.fixed',
      omitted: 'script, link, style, .ui.modal, .ui.dimmer, .ui.nag, .ui.fixed',
      pusher: '.pusher',
      sidebar: '.ui.sidebar'
    },

    regExp: {
      ios: /(iPad|iPhone|iPod)/g,
      mobileChrome: /(CriOS)/g,
      mobile: /Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/g
    },

    error: {
      method: 'The method you called is not defined.',
      pusher: 'Had to add pusher element. For optimal performance make sure body content is inside a pusher element',
      movedSidebar: 'Had to move sidebar. For optimal performance make sure sidebar and pusher are direct children of your body tag',
      overlay: 'The overlay setting is no longer supported, use animation: overlay',
      notFound: 'There were no elements that matched the specified selector'
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL3NpZGViYXIuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQVVBLENBQUMsQ0FBQyxVQUFVLENBQVYsRUFBYSxNQUFiLEVBQXFCLFFBQXJCLEVBQStCLFNBQS9CLEVBQTBDOztBQUU1Qzs7QUFFQSxXQUFVLE9BQU8sTUFBUCxJQUFpQixXQUFqQixJQUFnQyxPQUFPLElBQVAsSUFBZSxJQUFoRCxHQUNMLE1BREssR0FFSixPQUFPLElBQVAsSUFBZSxXQUFmLElBQThCLEtBQUssSUFBTCxJQUFhLElBQTVDLEdBQ0UsSUFERixHQUVFLFNBQVMsYUFBVCxHQUpOOztBQU9BLElBQUUsRUFBRixDQUFLLE9BQUwsR0FBZSxVQUFTLFVBQVQsRUFBcUI7QUFDbEMsUUFDRSxjQUFrQixFQUFFLElBQUYsQ0FEcEI7QUFBQSxRQUVFLFVBQWtCLEVBQUUsTUFBRixDQUZwQjtBQUFBLFFBR0UsWUFBa0IsRUFBRSxRQUFGLENBSHBCO0FBQUEsUUFJRSxRQUFrQixFQUFFLE1BQUYsQ0FKcEI7QUFBQSxRQUtFLFFBQWtCLEVBQUUsTUFBRixDQUxwQjtBQUFBLFFBT0UsaUJBQWtCLFlBQVksUUFBWixJQUF3QixFQVA1QztBQUFBLFFBU0UsT0FBa0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQVRwQjtBQUFBLFFBVUUsY0FBa0IsRUFWcEI7QUFBQSxRQVlFLFFBQWtCLFVBQVUsQ0FBVixDQVpwQjtBQUFBLFFBYUUsZ0JBQW1CLE9BQU8sS0FBUCxJQUFnQixRQWJyQztBQUFBLFFBY0UsaUJBQWtCLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLENBQXpCLENBZHBCO0FBQUEsUUFnQkUsd0JBQXdCLE9BQU8scUJBQVAsSUFDbkIsT0FBTyx3QkFEWSxJQUVuQixPQUFPLDJCQUZZLElBR25CLE9BQU8sdUJBSFksSUFJbkIsVUFBUyxRQUFULEVBQW1CO0FBQUUsaUJBQVcsUUFBWCxFQUFxQixDQUFyQjtBQUEwQixLQXBCdEQ7QUFBQSxRQXNCRSxhQXRCRjs7QUF5QkEsZ0JBQ0csSUFESCxDQUNRLFlBQVc7QUFDZixVQUNFLFdBQW9CLEVBQUUsYUFBRixDQUFnQixVQUFoQixDQUFGLEdBQ2QsRUFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUIsRUFBRSxFQUFGLENBQUssT0FBTCxDQUFhLFFBQWhDLEVBQTBDLFVBQTFDLENBRGMsR0FFZCxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsRUFBRSxFQUFGLENBQUssT0FBTCxDQUFhLFFBQTFCLENBSE47QUFBQSxVQUtFLFdBQWtCLFNBQVMsUUFMN0I7QUFBQSxVQU1FLFlBQWtCLFNBQVMsU0FON0I7QUFBQSxVQU9FLFlBQWtCLFNBQVMsU0FQN0I7QUFBQSxVQVFFLFNBQWtCLFNBQVMsTUFSN0I7QUFBQSxVQVNFLFFBQWtCLFNBQVMsS0FUN0I7QUFBQSxVQVdFLGlCQUFrQixNQUFNLFNBWDFCO0FBQUEsVUFZRSxrQkFBa0IsWUFBWSxTQVpoQztBQUFBLFVBY0UsVUFBa0IsRUFBRSxJQUFGLENBZHBCO0FBQUEsVUFlRSxXQUFrQixFQUFFLFNBQVMsT0FBWCxDQWZwQjtBQUFBLFVBaUJFLFlBQWtCLFFBQVEsUUFBUixDQUFpQixTQUFTLE9BQTFCLENBakJwQjtBQUFBLFVBa0JFLFNBQWtCLFNBQVMsUUFBVCxDQUFrQixTQUFTLEtBQTNCLENBbEJwQjtBQUFBLFVBbUJFLFVBQWtCLFNBQVMsUUFBVCxDQUFrQixTQUFTLE1BQTNCLENBbkJwQjtBQUFBLFVBb0JFLE1BcEJGO0FBQUEsVUFzQkUsVUFBa0IsSUF0QnBCO0FBQUEsVUF1QkUsV0FBa0IsUUFBUSxJQUFSLENBQWEsZUFBYixDQXZCcEI7QUFBQSxVQXlCRSxnQkF6QkY7QUFBQSxVQTBCRSxFQTFCRjtBQUFBLFVBMkJFLGFBM0JGO0FBQUEsVUE0QkUsZUE1QkY7QUFBQSxVQThCRSxNQTlCRjs7QUFpQ0EsZUFBYzs7QUFFWixvQkFBWSxZQUFXO0FBQ3JCLGlCQUFPLEtBQVAsQ0FBYSxzQkFBYixFQUFxQyxVQUFyQzs7QUFFQSxpQkFBTyxNQUFQLENBQWMsRUFBZDs7QUFFQSw0QkFBa0IsT0FBTyxHQUFQLENBQVcsZUFBWCxFQUFsQjs7QUFFQSxjQUFHLE9BQU8sRUFBUCxDQUFVLEdBQVYsRUFBSCxFQUFvQjtBQUNsQixtQkFBTyxHQUFQLENBQVcsR0FBWDtBQUNEOzs7QUFHRCxjQUFHLFNBQVMsVUFBWixFQUF3QjtBQUN0QixrQ0FBc0IsT0FBTyxLQUFQLENBQWEsTUFBbkM7QUFDRCxXQUZELE1BR0s7QUFDSCxtQkFBTyxLQUFQLENBQWEsTUFBYjtBQUNEOztBQUVELGdDQUFzQixZQUFXO0FBQy9CLG1CQUFPLEtBQVAsQ0FBYSxLQUFiO0FBQ0QsV0FGRDs7QUFJQSxpQkFBTyxXQUFQO0FBQ0QsU0ExQlc7O0FBNEJaLHFCQUFhLFlBQVc7QUFDdEIsaUJBQU8sT0FBUCxDQUFlLDRCQUFmLEVBQTZDLE1BQTdDO0FBQ0EscUJBQVcsTUFBWDtBQUNBLGtCQUNHLElBREgsQ0FDUSxlQURSLEVBQ3lCLE1BRHpCO0FBR0QsU0FsQ1c7O0FBb0NaLGdCQUFRO0FBQ04sY0FBSSxZQUFXO0FBQ2IsaUJBQUssQ0FBQyxLQUFLLE1BQUwsR0FBYyxRQUFkLENBQXVCLEVBQXZCLElBQTZCLFdBQTlCLEVBQTJDLE1BQTNDLENBQWtELENBQWxELEVBQW9ELENBQXBELENBQUw7QUFDQSwrQkFBbUIsTUFBTSxFQUF6QjtBQUNBLG1CQUFPLE9BQVAsQ0FBZSxnQ0FBZixFQUFpRCxFQUFqRDtBQUNEO0FBTEssU0FwQ0k7O0FBNENaLGlCQUFTLFlBQVc7QUFDbEIsaUJBQU8sT0FBUCxDQUFlLGdDQUFmLEVBQWlELE9BQWpEO0FBQ0Esa0JBQ0csR0FESCxDQUNPLGNBRFAsRUFFRyxVQUZILENBRWMsZUFGZDtBQUlBLGNBQUcsT0FBTyxFQUFQLENBQVUsR0FBVixFQUFILEVBQW9CO0FBQ2xCLG1CQUFPLE1BQVAsQ0FBYyxHQUFkO0FBQ0Q7O0FBRUQsbUJBQVMsR0FBVCxDQUFhLGdCQUFiO0FBQ0Esa0JBQVEsR0FBUixDQUFZLGdCQUFaO0FBQ0Esb0JBQVUsR0FBVixDQUFjLGdCQUFkO0FBQ0QsU0F6RFc7O0FBMkRaLGVBQU87QUFDTCxxQkFBVyxVQUFTLEtBQVQsRUFBZ0I7QUFDekIsZ0JBQ0Usa0JBQW1CLFFBQVEsSUFBUixDQUFhLE1BQU0sTUFBbkIsRUFBMkIsTUFBM0IsR0FBb0MsQ0FBcEMsSUFBeUMsUUFBUSxFQUFSLENBQVcsTUFBTSxNQUFqQixDQUQ5RDtBQUFBLGdCQUVFLGlCQUFtQixTQUFTLEVBQVQsQ0FBWSxNQUFNLE1BQWxCLENBRnJCO0FBSUEsZ0JBQUcsZUFBSCxFQUFvQjtBQUNsQixxQkFBTyxPQUFQLENBQWUsNkJBQWY7QUFDQSxxQkFBTyxJQUFQO0FBQ0Q7QUFDRCxnQkFBRyxjQUFILEVBQW1CO0FBQ2pCLHFCQUFPLE9BQVAsQ0FBZSxvREFBZjtBQUNBLHFCQUFPLElBQVA7QUFDRDtBQUNGLFdBZEk7QUFlTCxpQkFBTyxVQUFTLEtBQVQsRUFBZ0I7O0FBRXRCLFdBakJJO0FBa0JMLHlCQUFlLFVBQVMsS0FBVCxFQUFnQjtBQUM3QixnQkFBRyxRQUFRLFNBQVIsSUFBcUIsQ0FBeEIsRUFBNEI7QUFDMUIsc0JBQVEsU0FBUixHQUFvQixDQUFwQjtBQUNEO0FBQ0QsZ0JBQUksUUFBUSxTQUFSLEdBQW9CLFFBQVEsWUFBN0IsSUFBOEMsUUFBUSxZQUF6RCxFQUF1RTtBQUNyRSxzQkFBUSxTQUFSLEdBQW9CLFFBQVEsWUFBUixHQUF1QixRQUFRLFlBQS9CLEdBQThDLENBQWxFO0FBQ0Q7QUFDRixXQXpCSTtBQTBCTCxrQkFBUSxVQUFTLEtBQVQsRUFBZ0I7QUFDdEIsZ0JBQUksRUFBRSxNQUFNLE1BQVIsRUFBZ0IsT0FBaEIsQ0FBd0IsU0FBUyxPQUFqQyxFQUEwQyxNQUExQyxLQUFxRCxDQUF6RCxFQUE2RDtBQUMzRCxvQkFBTSxjQUFOO0FBQ0Q7QUFDRjtBQTlCSSxTQTNESzs7QUE0RlosY0FBTTtBQUNKLHFCQUFXLFlBQVc7QUFDcEIsbUJBQU8sT0FBUCxDQUFlLG9DQUFmLEVBQXFELFFBQXJEO0FBQ0EsZ0JBQUcsU0FBUyxRQUFaLEVBQXNCO0FBQ3BCLHVCQUNHLEVBREgsQ0FDTSxVQUFhLGdCQURuQixFQUNxQyxPQUFPLEtBQVAsQ0FBYSxTQURsRCxFQUVHLEVBRkgsQ0FFTSxhQUFhLGdCQUZuQixFQUVxQyxPQUFPLEtBQVAsQ0FBYSxTQUZsRDtBQUlEO0FBQ0YsV0FURztBQVVKLHNCQUFZLFlBQVc7QUFDckIsZ0JBQUcsU0FBUyxVQUFaLEVBQXdCO0FBQ3RCLHFCQUFPLEtBQVAsQ0FBYSx1QkFBYjtBQUNBLHNCQUNHLEVBREgsQ0FDTSxtQkFBbUIsZ0JBRHpCLEVBQzJDLE9BQU8sS0FBUCxDQUFhLE1BRHhEO0FBR0Q7QUFDRCxtQkFBTyxPQUFQLENBQWUseUNBQWY7QUFDQSxzQkFDRyxFQURILENBQ00sY0FBYyxnQkFEcEIsRUFDc0MsT0FBTyxLQUFQLENBQWEsS0FEbkQ7QUFHQSxvQkFDRyxFQURILENBQ00sV0FBVyxjQURqQixFQUNpQyxPQUFPLEtBQVAsQ0FBYSxhQUQ5QztBQUdEO0FBeEJHLFNBNUZNO0FBc0haLGdCQUFRO0FBQ04scUJBQVcsWUFBVztBQUNwQixtQkFBTyxPQUFQLENBQWUsd0NBQWYsRUFBeUQsUUFBekQ7QUFDQSxxQkFBUyxHQUFULENBQWEsZ0JBQWI7QUFDRCxXQUpLO0FBS04sc0JBQVksWUFBVztBQUNyQixtQkFBTyxPQUFQLENBQWUsZ0NBQWY7QUFDQSxzQkFBVSxHQUFWLENBQWMsZ0JBQWQ7QUFDQSxvQkFBUSxHQUFSLENBQVksZ0JBQVo7QUFDQSxvQkFBUSxHQUFSLENBQVksV0FBVyxjQUF2QjtBQUNEO0FBVkssU0F0SEk7O0FBbUlaLGFBQUs7QUFDSCxxQkFBVyxZQUFXO0FBQ3BCLGdCQUNFLFFBQVksT0FBTyxLQUFQLENBQWEsS0FBYixJQUF1QixRQUFRLFVBQVIsRUFEckM7QUFBQSxnQkFFRSxTQUFZLE9BQU8sS0FBUCxDQUFhLE1BQWIsSUFBdUIsUUFBUSxXQUFSLEVBRnJDO0FBQUEsZ0JBR0UsUUFBWSxPQUFPLEVBQVAsQ0FBVSxHQUFWLEVBSGQ7QUFBQSxnQkFJRSxZQUFZLE9BQU8sR0FBUCxDQUFXLFNBQVgsRUFKZDtBQUFBLGdCQUtFLFdBQVk7QUFDVixvQkFBUyxLQURDO0FBRVYscUJBQVMsQ0FBQyxLQUZBO0FBR1YsbUJBQVMsTUFIQztBQUlWLHNCQUFTLENBQUM7QUFKQSxhQUxkO0FBQUEsZ0JBV0UsS0FYRjs7QUFjQSxnQkFBRyxLQUFILEVBQVM7QUFDUCxxQkFBTyxPQUFQLENBQWUsK0JBQWY7QUFDQSx1QkFBUyxJQUFULEdBQWdCLENBQUMsS0FBakI7QUFDQSx1QkFBUyxLQUFULEdBQWlCLEtBQWpCO0FBQ0Q7O0FBRUQsb0JBQVMsU0FBVDs7QUFFQSxnQkFBRyxjQUFjLE1BQWQsSUFBd0IsY0FBYyxPQUF6QyxFQUFrRDtBQUNoRCxxQkFBTyxLQUFQLENBQWEseUNBQWIsRUFBd0QsS0FBeEQ7QUFDQSx1QkFBVSxLQUNOLGVBRE0sR0FDWSxTQURaLEdBQ3dCLG9CQUR4QixHQUVOLGVBRk0sR0FFWSxTQUZaLEdBRXdCLHNCQUZ4QixHQUdOLG9DQUhNLEdBR2dDLFNBQVMsU0FBVCxDQUhoQyxHQUdzRCxZQUh0RCxHQUlOLG9DQUpNLEdBSWdDLFNBQVMsU0FBVCxDQUpoQyxHQUlzRCxZQUp0RCxHQUtOLElBTEo7QUFPRCxhQVRELE1BVUssSUFBRyxjQUFjLEtBQWQsSUFBdUIsYUFBYSxRQUF2QyxFQUFpRDtBQUNwRCx1QkFBVSxLQUNOLGVBRE0sR0FDWSxTQURaLEdBQ3dCLG9CQUR4QixHQUVOLGVBRk0sR0FFWSxTQUZaLEdBRXdCLHNCQUZ4QixHQUdOLHVDQUhNLEdBR29DLFNBQVMsU0FBVCxDQUhwQyxHQUcwRCxTQUgxRCxHQUlOLHVDQUpNLEdBSW9DLFNBQVMsU0FBVCxDQUpwQyxHQUkwRCxTQUoxRCxHQUtOLElBTEo7QUFPRDs7OztBQUlELGdCQUFJLE9BQU8sRUFBUCxDQUFVLEVBQVYsRUFBSixFQUFxQjtBQUNuQixrQkFBRyxjQUFjLE1BQWQsSUFBd0IsY0FBYyxPQUF6QyxFQUFrRDtBQUNoRCx1QkFBTyxLQUFQLENBQWEseUNBQWIsRUFBd0QsS0FBeEQ7QUFDQSx5QkFBVSxLQUNOLCtCQURNLEdBQzRCLFNBRDVCLEdBQ3dDLDRCQUR4QyxHQUVOLG9DQUZNLEdBRWdDLFNBQVMsU0FBVCxDQUZoQyxHQUVzRCxZQUZ0RCxHQUdOLG9DQUhNLEdBR2dDLFNBQVMsU0FBVCxDQUhoQyxHQUdzRCxZQUh0RCxHQUlOLElBSko7QUFNRCxlQVJELE1BU0ssSUFBRyxjQUFjLEtBQWQsSUFBdUIsYUFBYSxRQUF2QyxFQUFpRDtBQUNwRCx5QkFBVSxLQUNOLCtCQURNLEdBQzRCLFNBRDVCLEdBQ3dDLDRCQUR4QyxHQUVOLHVDQUZNLEdBRW9DLFNBQVMsU0FBVCxDQUZwQyxHQUUwRCxTQUYxRCxHQUdOLHVDQUhNLEdBR29DLFNBQVMsU0FBVCxDQUhwQyxHQUcwRCxTQUgxRCxHQUlOLElBSko7QUFNRDs7QUFFRCx1QkFBUyxLQUNMLHdGQURLLEdBRUwseUZBRkssR0FHTCwrQ0FISyxHQUlMLCtDQUpLLEdBS0wsSUFMSjtBQU9EO0FBQ0QscUJBQVMsVUFBVDtBQUNBLHFCQUFTLEVBQUUsS0FBRixFQUNOLFFBRE0sQ0FDRyxLQURILENBQVQ7QUFHQSxtQkFBTyxLQUFQLENBQWEsMkJBQWIsRUFBMEMsTUFBMUM7QUFDRDtBQTlFRSxTQW5JTzs7QUFvTlosaUJBQVMsWUFBVztBQUNsQixpQkFBTyxPQUFQLENBQWUsMkJBQWY7QUFDQSxxQkFBWSxFQUFFLFNBQVMsT0FBWCxDQUFaO0FBQ0Esc0JBQVksU0FBUyxRQUFULENBQWtCLFNBQVMsT0FBM0IsQ0FBWjtBQUNBLG9CQUFZLFNBQVMsUUFBVCxDQUFrQixTQUFTLE1BQTNCLENBQVo7QUFDQSxtQkFBWSxTQUFTLFFBQVQsQ0FBa0IsU0FBUyxLQUEzQixDQUFaO0FBQ0EsaUJBQU8sS0FBUCxDQUFhLEtBQWI7QUFDRCxTQTNOVzs7QUE2TloseUJBQWlCLFlBQVc7QUFDMUIsaUJBQU8sT0FBUCxDQUFlLDJCQUFmO0FBQ0Esc0JBQVksU0FBUyxRQUFULENBQWtCLFNBQVMsT0FBM0IsQ0FBWjtBQUNELFNBaE9XOztBQWtPWixpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLE9BQVAsQ0FBZSx1QkFBZjtBQUNBLGtCQUFRLEtBQVIsQ0FBYyxPQUFkLEdBQXdCLE1BQXhCO0FBQ0EsY0FBSSxVQUFVLFFBQVEsWUFBdEI7QUFDQSxrQkFBUSxTQUFSLEdBQW9CLFFBQVEsU0FBNUI7QUFDQSxrQkFBUSxLQUFSLENBQWMsT0FBZCxHQUF3QixFQUF4QjtBQUNELFNBeE9XOztBQTBPWixlQUFPO0FBQ0wsaUJBQU8sWUFBVztBQUNoQixtQkFBTyxLQUFQLEdBQWU7QUFDYixxQkFBUyxRQUFRLFVBQVIsRUFESTtBQUViLHNCQUFTLFFBQVEsV0FBUixFQUZJO0FBR2IsbUJBQVUsUUFBUSxHQUFSLENBQVksV0FBWixLQUE0QjtBQUh6QixhQUFmO0FBS0QsV0FQSTtBQVFMLGtCQUFRLFlBQVc7QUFDakIsZ0JBQUksU0FBUyxRQUFULENBQWtCLFNBQVMsTUFBM0IsRUFBbUMsTUFBbkMsS0FBOEMsQ0FBbEQsRUFBc0Q7QUFDcEQscUJBQU8sS0FBUCxDQUFhLG9DQUFiO0FBQ0EscUJBQU8sS0FBUCxDQUFhLE1BQU0sTUFBbkI7QUFDQSx3QkFBVSxFQUFFLHdCQUFGLENBQVY7QUFDQSx1QkFDRyxRQURILEdBRUssR0FGTCxDQUVTLFNBQVMsT0FGbEIsRUFHSyxHQUhMLENBR1MsU0FIVCxFQUlLLE9BSkwsQ0FJYSxPQUpiO0FBTUEscUJBQU8sT0FBUDtBQUNEO0FBQ0QsZ0JBQUcsUUFBUSxPQUFSLENBQWdCLFNBQVMsTUFBekIsRUFBaUMsTUFBakMsS0FBNEMsQ0FBNUMsSUFBaUQsUUFBUSxPQUFSLENBQWdCLFNBQVMsTUFBekIsRUFBaUMsQ0FBakMsTUFBd0MsUUFBUSxDQUFSLENBQTVGLEVBQXdHO0FBQ3RHLHFCQUFPLEtBQVAsQ0FBYSx5Q0FBYjtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxNQUFNLFlBQW5CLEVBQWlDLE9BQWpDO0FBQ0Esc0JBQVEsTUFBUixHQUFpQixTQUFqQixDQUEyQixRQUEzQjtBQUNBLHFCQUFPLE9BQVA7QUFDRDtBQUNELG1CQUFPLEtBQVAsQ0FBYSxLQUFiO0FBQ0EsbUJBQU8sR0FBUCxDQUFXLFFBQVg7QUFDQSxtQkFBTyxHQUFQLENBQVcsU0FBWDtBQUNEO0FBOUJJLFNBMU9LOztBQTJRWixzQkFBYyxVQUFTLFFBQVQsRUFBbUIsS0FBbkIsRUFBMEI7QUFDdEMsY0FDRSxVQUFVLEVBQUUsUUFBRixDQURaO0FBR0Esa0JBQVEsRUFBRSxVQUFGLENBQWEsT0FBTyxLQUFQLENBQWIsSUFDSixPQUFPLEtBQVAsQ0FESSxHQUVKLE9BQU8sTUFGWDtBQUlBLGNBQUcsUUFBUSxNQUFSLEdBQWlCLENBQXBCLEVBQXVCO0FBQ3JCLG1CQUFPLEtBQVAsQ0FBYSxxQ0FBYixFQUFvRCxRQUFwRCxFQUE4RCxLQUE5RDtBQUNBLG9CQUNHLEVBREgsQ0FDTSxVQUFVLGNBRGhCLEVBQ2dDLEtBRGhDO0FBR0QsV0FMRCxNQU1LO0FBQ0gsbUJBQU8sS0FBUCxDQUFhLE1BQU0sUUFBbkIsRUFBNkIsUUFBN0I7QUFDRDtBQUNGLFNBNVJXOztBQThSWixjQUFNLFVBQVMsUUFBVCxFQUFtQjtBQUN2QixxQkFBVyxFQUFFLFVBQUYsQ0FBYSxRQUFiLElBQ1AsUUFETyxHQUVQLFlBQVUsQ0FBRSxDQUZoQjtBQUlBLGNBQUcsT0FBTyxFQUFQLENBQVUsTUFBVixFQUFILEVBQXVCO0FBQ3JCLG1CQUFPLGVBQVA7QUFDQSxnQkFBRyxTQUFTLE9BQVosRUFBc0I7QUFDcEIscUJBQU8sS0FBUCxDQUFhLE1BQU0sT0FBbkI7QUFDQSx1QkFBUyxVQUFULEdBQXNCLFNBQXRCO0FBQ0Q7QUFDRCxtQkFBTyxPQUFQO0FBQ0EsZ0JBQUcsT0FBTyxZQUFQLEVBQUgsRUFBMEI7QUFDeEIscUJBQU8sS0FBUCxDQUFhLGtDQUFiO0FBQ0Esa0JBQUcsU0FBUyxTQUFaLEVBQXVCOztBQUVyQixvQkFBRyxTQUFTLFVBQVQsSUFBdUIsU0FBMUIsRUFBcUM7QUFDbkMseUJBQU8sVUFBUCxDQUFrQixPQUFPLElBQXpCO0FBQ0E7QUFDRCxpQkFIRCxNQUlLO0FBQ0gseUJBQU8sVUFBUDtBQUNEO0FBQ0YsZUFURCxNQVVLO0FBQ0gseUJBQVMsVUFBVCxHQUFzQixTQUF0QjtBQUNEO0FBQ0Y7QUFDRCxtQkFBTyxRQUFQLENBQWdCLFlBQVc7QUFDekIsdUJBQVMsSUFBVCxDQUFjLE9BQWQ7QUFDQSx1QkFBUyxNQUFULENBQWdCLElBQWhCLENBQXFCLE9BQXJCO0FBQ0QsYUFIRDtBQUlBLHFCQUFTLFFBQVQsQ0FBa0IsSUFBbEIsQ0FBdUIsT0FBdkI7QUFDQSxxQkFBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLE9BQXhCO0FBQ0QsV0E3QkQsTUE4Qks7QUFDSCxtQkFBTyxLQUFQLENBQWEsNEJBQWI7QUFDRDtBQUNGLFNBcFVXOztBQXNVWixjQUFNLFVBQVMsUUFBVCxFQUFtQjtBQUN2QixxQkFBVyxFQUFFLFVBQUYsQ0FBYSxRQUFiLElBQ1AsUUFETyxHQUVQLFlBQVUsQ0FBRSxDQUZoQjtBQUlBLGNBQUcsT0FBTyxFQUFQLENBQVUsT0FBVixNQUF1QixPQUFPLEVBQVAsQ0FBVSxTQUFWLEVBQTFCLEVBQWlEO0FBQy9DLG1CQUFPLEtBQVAsQ0FBYSxnQkFBYixFQUErQixRQUEvQjtBQUNBLG1CQUFPLGVBQVA7QUFDQSxtQkFBTyxRQUFQLENBQWdCLFlBQVc7QUFDekIsdUJBQVMsSUFBVCxDQUFjLE9BQWQ7QUFDQSx1QkFBUyxRQUFULENBQWtCLElBQWxCLENBQXVCLE9BQXZCO0FBQ0QsYUFIRDtBQUlBLHFCQUFTLFFBQVQsQ0FBa0IsSUFBbEIsQ0FBdUIsT0FBdkI7QUFDQSxxQkFBUyxNQUFULENBQWdCLElBQWhCLENBQXFCLE9BQXJCO0FBQ0Q7QUFDRixTQXJWVzs7QUF1VloseUJBQWlCLFlBQVc7QUFDMUIsaUJBQVEsVUFBVSxHQUFWLENBQWMsT0FBZCxFQUF1QixNQUF2QixDQUE4QixNQUFNLFVBQVUsU0FBOUMsRUFBeUQsTUFBekQsR0FBa0UsQ0FBMUU7QUFDRCxTQXpWVztBQTBWWix1QkFBZSxZQUFXO0FBQ3hCLGlCQUFRLFVBQVUsR0FBVixDQUFjLE9BQWQsRUFBdUIsTUFBdkIsQ0FBOEIsTUFBTSxVQUFVLE9BQTlDLEVBQXVELE1BQXZELEdBQWdFLENBQXhFO0FBQ0QsU0E1Vlc7QUE2Vlosc0JBQWMsWUFBVztBQUN2QixpQkFBTyxPQUFPLGFBQVAsTUFBMEIsT0FBTyxlQUFQLEVBQWpDO0FBQ0QsU0EvVlc7O0FBaVdaLG9CQUFZLFVBQVMsUUFBVCxFQUFtQjtBQUM3QixjQUNFLGlCQUFpQixVQUFVLEdBQVYsQ0FBYyxPQUFkLEVBQXVCLE1BQXZCLENBQThCLE1BQU0sVUFBVSxPQUE5QyxDQURuQjtBQUFBLGNBRUUsZUFBaUIsZUFBZSxNQUZsQztBQUFBLGNBR0UsZ0JBQWlCLENBSG5CO0FBS0EscUJBQVcsWUFBWSxZQUFVLENBQUUsQ0FBbkM7QUFDQSx5QkFDRyxPQURILENBQ1csTUFEWCxFQUNtQixZQUFXO0FBQzFCO0FBQ0EsZ0JBQUcsaUJBQWlCLFlBQXBCLEVBQWtDO0FBQ2hDO0FBQ0Q7QUFDRixXQU5IO0FBUUQsU0FoWFc7O0FBa1haLGdCQUFRLFlBQVc7QUFDakIsaUJBQU8sT0FBUCxDQUFlLCtCQUFmO0FBQ0EsY0FBRyxPQUFPLEVBQVAsQ0FBVSxNQUFWLEVBQUgsRUFBdUI7QUFDckIsbUJBQU8sSUFBUDtBQUNELFdBRkQsTUFHSztBQUNILG1CQUFPLElBQVA7QUFDRDtBQUNGLFNBMVhXOztBQTRYWixrQkFBVSxVQUFTLFFBQVQsRUFBbUI7QUFDM0IsY0FDRSxhQUFhLE9BQU8sR0FBUCxDQUFXLFVBQVgsRUFEZjtBQUFBLGNBRUUsY0FBZSxlQUFlLFNBQWYsSUFBNEIsT0FBTyxZQUFQLEVBQTdCLEdBQ1YsT0FEVSxHQUVWLE9BSk47QUFBQSxjQUtFLE9BTEY7QUFBQSxjQU1FLEdBTkY7QUFBQSxjQU9FLGFBUEY7QUFTQSxxQkFBVyxFQUFFLFVBQUYsQ0FBYSxRQUFiLElBQ1AsUUFETyxHQUVQLFlBQVUsQ0FBRSxDQUZoQjtBQUlBLGNBQUcsU0FBUyxVQUFULElBQXVCLFlBQTFCLEVBQXdDO0FBQ3RDLG1CQUFPLFdBQVA7QUFDRDtBQUNELGlCQUFPLEdBQVAsQ0FBVyxVQUFYLENBQXNCLFVBQXRCO0FBQ0EsaUJBQU8sT0FBUDtBQUNBLG9CQUFVLFlBQVc7QUFDbkIsbUJBQU8sSUFBUCxDQUFZLFNBQVo7QUFDQSxtQkFBTyxHQUFQLENBQVcsU0FBWDtBQUNBLG1CQUFPLEdBQVAsQ0FBVyxTQUFYO0FBQ0EsbUJBQU8sR0FBUCxDQUFXLE9BQVg7QUFDRCxXQUxEO0FBTUEsZ0JBQU0sWUFBVztBQUNmLG1CQUFPLEdBQVAsQ0FBVyxNQUFYO0FBQ0QsV0FGRDtBQUdBLDBCQUFnQixVQUFTLEtBQVQsRUFBZ0I7QUFDOUIsZ0JBQUksTUFBTSxNQUFOLElBQWdCLFlBQVksQ0FBWixDQUFwQixFQUFxQztBQUNuQywwQkFBWSxHQUFaLENBQWdCLGtCQUFrQixnQkFBbEMsRUFBb0QsYUFBcEQ7QUFDQSxxQkFBTyxNQUFQLENBQWMsU0FBZDtBQUNBLHFCQUFPLElBQVAsQ0FBWSxVQUFaO0FBQ0EsdUJBQVMsSUFBVCxDQUFjLE9BQWQ7QUFDRDtBQUNGLFdBUEQ7QUFRQSxzQkFBWSxHQUFaLENBQWdCLGtCQUFrQixnQkFBbEM7QUFDQSxzQkFBWSxFQUFaLENBQWUsa0JBQWtCLGdCQUFqQyxFQUFtRCxhQUFuRDtBQUNBLGdDQUFzQixPQUF0QjtBQUNBLGNBQUcsU0FBUyxPQUFULElBQW9CLENBQUMsT0FBTyxhQUFQLEVBQXhCLEVBQWdEO0FBQzlDLGtDQUFzQixHQUF0QjtBQUNEO0FBQ0YsU0F0YVc7O0FBd2FaLGtCQUFVLFVBQVMsUUFBVCxFQUFtQjtBQUMzQixjQUNFLGFBQWEsT0FBTyxHQUFQLENBQVcsVUFBWCxFQURmO0FBQUEsY0FFRSxjQUFlLGNBQWMsU0FBZCxJQUEyQixPQUFPLFlBQVAsRUFBNUIsR0FDVixPQURVLEdBRVYsT0FKTjtBQUFBLGNBS0UsT0FMRjtBQUFBLGNBTUUsYUFORjtBQVFBLHFCQUFXLEVBQUUsVUFBRixDQUFhLFFBQWIsSUFDUCxRQURPLEdBRVAsWUFBVSxDQUFFLENBRmhCO0FBSUEsaUJBQU8sT0FBUCxDQUFlLDZCQUFmLEVBQThDLE9BQU8sR0FBUCxDQUFXLFNBQVgsRUFBOUM7O0FBRUEsaUJBQU8sTUFBUCxDQUFjLFNBQWQ7QUFDQSxpQkFBTyxNQUFQLENBQWMsVUFBZDs7QUFFQSxvQkFBVSxZQUFXO0FBQ25CLG1CQUFPLEdBQVAsQ0FBVyxVQUFYLENBQXNCLFVBQXRCO0FBQ0EsbUJBQU8sR0FBUCxDQUFXLFNBQVg7QUFDQSxtQkFBTyxNQUFQLENBQWMsT0FBZDtBQUNBLGdCQUFHLFNBQVMsT0FBVCxJQUFvQixDQUFDLE9BQU8sYUFBUCxFQUF4QixFQUFnRDtBQUM5QyxzQkFBUSxXQUFSLENBQW9CLFVBQVUsTUFBOUI7QUFDRDtBQUNGLFdBUEQ7QUFRQSwwQkFBZ0IsVUFBUyxLQUFULEVBQWdCO0FBQzlCLGdCQUFJLE1BQU0sTUFBTixJQUFnQixZQUFZLENBQVosQ0FBcEIsRUFBcUM7QUFDbkMsMEJBQVksR0FBWixDQUFnQixrQkFBa0IsZ0JBQWxDLEVBQW9ELGFBQXBEO0FBQ0EscUJBQU8sTUFBUCxDQUFjLFNBQWQ7QUFDQSxxQkFBTyxNQUFQLENBQWMsVUFBZDtBQUNBLHFCQUFPLE1BQVAsQ0FBYyxTQUFkO0FBQ0Esa0JBQUcsY0FBYyxZQUFkLElBQStCLFNBQVMsWUFBVCxJQUF5QixPQUFPLEVBQVAsQ0FBVSxNQUFWLEVBQTNELEVBQWlGO0FBQy9FLHVCQUFPLFVBQVA7QUFDRDtBQUNELHVCQUFTLElBQVQsQ0FBYyxPQUFkO0FBQ0Q7QUFDRixXQVhEO0FBWUEsc0JBQVksR0FBWixDQUFnQixrQkFBa0IsZ0JBQWxDO0FBQ0Esc0JBQVksRUFBWixDQUFlLGtCQUFrQixnQkFBakMsRUFBbUQsYUFBbkQ7QUFDQSxnQ0FBc0IsT0FBdEI7QUFDRCxTQWpkVzs7QUFtZFoscUJBQWEsWUFBVztBQUN0QixpQkFBTyxPQUFQLENBQWUsb0RBQWY7QUFDQSwwQkFBZ0IsRUFBRSxNQUFGLEVBQVUsU0FBVixFQUFoQjtBQUNBLGtCQUFRLFNBQVIsQ0FBa0IsQ0FBbEI7QUFDQSxpQkFBTyxRQUFQLENBQWdCLENBQWhCLEVBQW1CLENBQW5CO0FBQ0QsU0F4ZFc7O0FBMGRaLG9CQUFZLFlBQVc7QUFDckIsaUJBQU8sT0FBUCxDQUFlLDBDQUFmO0FBQ0EsaUJBQU8sUUFBUCxDQUFnQixDQUFoQixFQUFtQixhQUFuQjtBQUNELFNBN2RXOztBQStkWixlQUFPO0FBQ0wsaUJBQU8sWUFBVztBQUNoQixtQkFBTyxPQUFQLENBQWUsNEJBQWY7QUFDQSxtQkFBTyxLQUFQLEdBQWUsRUFBZjtBQUNEO0FBSkksU0EvZEs7O0FBc2VaLGFBQUs7OztBQUdILGVBQUssWUFBVztBQUNkLGtCQUFNLFFBQU4sQ0FBZSxVQUFVLEdBQXpCO0FBQ0QsV0FMRTs7O0FBUUgsa0JBQVEsWUFBVztBQUNqQixxQkFBUyxRQUFULENBQWtCLFVBQVUsTUFBNUI7QUFDRCxXQVZFO0FBV0gsb0JBQVUsWUFBVztBQUNuQixxQkFBUyxRQUFULENBQWtCLFVBQVUsUUFBNUI7QUFDRCxXQWJFOzs7QUFnQkgsa0JBQVEsWUFBVztBQUNqQixvQkFBUSxRQUFSLENBQWlCLFVBQVUsTUFBM0I7QUFDRCxXQWxCRTs7O0FBcUJILGtCQUFRLFlBQVc7QUFDakIsb0JBQVEsUUFBUixDQUFpQixVQUFVLE1BQTNCO0FBQ0QsV0F2QkU7QUF3QkgscUJBQVcsWUFBVztBQUNwQixvQkFBUSxRQUFSLENBQWlCLFVBQVUsU0FBM0I7QUFDRCxXQTFCRTtBQTJCSCxzQkFBWSxVQUFTLFVBQVQsRUFBcUI7QUFDL0IseUJBQWEsY0FBYyxPQUFPLEdBQVAsQ0FBVyxVQUFYLEVBQTNCO0FBQ0Esb0JBQVEsUUFBUixDQUFpQixVQUFqQjtBQUNELFdBOUJFO0FBK0JILHFCQUFXLFVBQVMsU0FBVCxFQUFvQjtBQUM3Qix3QkFBWSxhQUFhLE9BQU8sR0FBUCxDQUFXLFNBQVgsRUFBekI7QUFDQSxvQkFBUSxRQUFSLENBQWlCLFVBQVUsU0FBVixDQUFqQjtBQUNELFdBbENFO0FBbUNILG1CQUFTLFlBQVc7QUFDbEIsb0JBQVEsUUFBUixDQUFpQixVQUFVLE9BQTNCO0FBQ0QsV0FyQ0U7QUFzQ0gsbUJBQVMsWUFBVztBQUNsQixvQkFBUSxRQUFSLENBQWlCLFVBQVUsT0FBM0I7QUFDRDtBQXhDRSxTQXRlTztBQWdoQlosZ0JBQVE7O0FBRU4scUJBQVcsWUFBVztBQUNwQixtQkFBTyxLQUFQLENBQWEsNEJBQWIsRUFBMkMsTUFBM0M7QUFDQSxnQkFBRyxVQUFVLE9BQU8sTUFBUCxHQUFnQixDQUE3QixFQUFnQztBQUM5QixxQkFBTyxNQUFQO0FBQ0Q7QUFDRixXQVBLOzs7QUFVTixlQUFLLFlBQVc7QUFDZCxrQkFBTSxXQUFOLENBQWtCLFVBQVUsR0FBNUI7QUFDRCxXQVpLOzs7QUFlTixrQkFBUSxZQUFXO0FBQ2pCLHFCQUFTLFdBQVQsQ0FBcUIsVUFBVSxNQUEvQjtBQUNELFdBakJLO0FBa0JOLG9CQUFVLFlBQVc7QUFDbkIscUJBQVMsV0FBVCxDQUFxQixVQUFVLFFBQS9CO0FBQ0QsV0FwQks7OztBQXVCTixrQkFBUSxZQUFXO0FBQ2pCLG9CQUFRLFdBQVIsQ0FBb0IsVUFBVSxNQUE5QjtBQUNELFdBekJLO0FBMEJOLHFCQUFXLFlBQVc7QUFDcEIsb0JBQVEsV0FBUixDQUFvQixVQUFVLFNBQTlCO0FBQ0QsV0E1Qks7QUE2Qk4sc0JBQVksVUFBUyxVQUFULEVBQXFCO0FBQy9CLHlCQUFhLGNBQWMsT0FBTyxHQUFQLENBQVcsVUFBWCxFQUEzQjtBQUNBLG9CQUFRLFdBQVIsQ0FBb0IsVUFBcEI7QUFDRCxXQWhDSztBQWlDTixxQkFBVyxVQUFTLFNBQVQsRUFBb0I7QUFDN0Isd0JBQVksYUFBYSxPQUFPLEdBQVAsQ0FBVyxTQUFYLEVBQXpCO0FBQ0Esb0JBQVEsV0FBUixDQUFvQixVQUFVLFNBQVYsQ0FBcEI7QUFDRCxXQXBDSztBQXFDTixtQkFBUyxZQUFXO0FBQ2xCLG9CQUFRLFdBQVIsQ0FBb0IsVUFBVSxPQUE5QjtBQUNELFdBdkNLO0FBd0NOLG1CQUFTLFlBQVc7QUFDbEIsb0JBQVEsV0FBUixDQUFvQixVQUFVLE9BQTlCO0FBQ0Q7QUExQ0ssU0FoaEJJOztBQTZqQlosYUFBSztBQUNILHFCQUFXLFlBQVc7QUFDcEIsZ0JBQUcsUUFBUSxRQUFSLENBQWlCLFVBQVUsR0FBM0IsQ0FBSCxFQUFvQztBQUNsQyxxQkFBTyxVQUFVLEdBQWpCO0FBQ0QsYUFGRCxNQUdLLElBQUcsUUFBUSxRQUFSLENBQWlCLFVBQVUsS0FBM0IsQ0FBSCxFQUFzQztBQUN6QyxxQkFBTyxVQUFVLEtBQWpCO0FBQ0QsYUFGSSxNQUdBLElBQUcsUUFBUSxRQUFSLENBQWlCLFVBQVUsTUFBM0IsQ0FBSCxFQUF1QztBQUMxQyxxQkFBTyxVQUFVLE1BQWpCO0FBQ0Q7QUFDRCxtQkFBTyxVQUFVLElBQWpCO0FBQ0QsV0FaRTtBQWFILHNCQUFZLFlBQVc7QUFDckIsZ0JBQ0UsWUFBWSxPQUFPLEdBQVAsQ0FBVyxTQUFYLEVBRGQ7QUFBQSxnQkFFRSxVQUZGO0FBSUEseUJBQWUsT0FBTyxFQUFQLENBQVUsTUFBVixFQUFGLEdBQ1IsU0FBUyxnQkFBVCxJQUE2QixNQUE5QixHQUNFLFNBQVMsaUJBQVQsQ0FBMkIsTUFBM0IsQ0FBa0MsU0FBbEMsQ0FERixHQUVFLFNBQVMsZ0JBSEYsR0FJUixTQUFTLFVBQVQsSUFBdUIsTUFBeEIsR0FDRSxTQUFTLGlCQUFULENBQTJCLFFBQTNCLENBQW9DLFNBQXBDLENBREYsR0FFRSxTQUFTLFVBTmY7QUFRQSxtQkFBTyxPQUFQLENBQWUsdUJBQWYsRUFBd0MsVUFBeEM7QUFDQSxtQkFBTyxVQUFQO0FBQ0QsV0E1QkU7QUE2QkgsMkJBQWlCLFlBQVc7QUFDMUIsZ0JBQ0UsVUFBYyxTQUFTLGFBQVQsQ0FBdUIsU0FBdkIsQ0FEaEI7QUFBQSxnQkFFRSxjQUFjO0FBQ1osNEJBQW9CLGVBRFI7QUFFWiw2QkFBb0IsZ0JBRlI7QUFHWiwrQkFBb0IsZUFIUjtBQUlaLGtDQUFvQjtBQUpSLGFBRmhCO0FBQUEsZ0JBUUUsVUFSRjtBQVVBLGlCQUFJLFVBQUosSUFBa0IsV0FBbEIsRUFBOEI7QUFDNUIsa0JBQUksUUFBUSxLQUFSLENBQWMsVUFBZCxNQUE4QixTQUFsQyxFQUE2QztBQUMzQyx1QkFBTyxZQUFZLFVBQVosQ0FBUDtBQUNEO0FBQ0Y7QUFDRjtBQTdDRSxTQTdqQk87O0FBNm1CWixZQUFJOztBQUVGLGNBQUksWUFBVztBQUNiLGdCQUNFLFNBQVUsQ0FBRSxPQUFPLGFBQVQsSUFBMkIsbUJBQW1CLE1BRDFEO0FBQUEsZ0JBRUUsT0FBVSxtQkFBbUIsTUFGL0I7QUFJQSxtQkFBUSxVQUFVLElBQWxCO0FBQ0QsV0FSQzs7QUFVRixlQUFLLFlBQVc7QUFDZCxnQkFDRSxZQUFpQixVQUFVLFNBRDdCO0FBQUEsZ0JBRUUsUUFBaUIsVUFBVSxLQUFWLENBQWdCLE9BQU8sR0FBdkIsQ0FGbkI7QUFBQSxnQkFHRSxpQkFBaUIsVUFBVSxLQUFWLENBQWdCLE9BQU8sWUFBdkIsQ0FIbkI7QUFLQSxnQkFBRyxTQUFTLENBQUMsY0FBYixFQUE2QjtBQUMzQixxQkFBTyxPQUFQLENBQWUsNkJBQWYsRUFBOEMsU0FBOUM7QUFDQSxxQkFBTyxJQUFQO0FBQ0QsYUFIRCxNQUlLO0FBQ0gscUJBQU8sS0FBUDtBQUNEO0FBQ0YsV0F2QkM7QUF3QkYsa0JBQVEsWUFBVztBQUNqQixnQkFDRSxZQUFlLFVBQVUsU0FEM0I7QUFBQSxnQkFFRSxXQUFlLFVBQVUsS0FBVixDQUFnQixPQUFPLE1BQXZCLENBRmpCO0FBSUEsZ0JBQUcsUUFBSCxFQUFhO0FBQ1gscUJBQU8sT0FBUCxDQUFlLGdDQUFmLEVBQWlELFNBQWpEO0FBQ0EscUJBQU8sSUFBUDtBQUNELGFBSEQsTUFJSztBQUNILHFCQUFPLE9BQVAsQ0FBZSxpREFBZixFQUFrRSxTQUFsRTtBQUNBLHFCQUFPLEtBQVA7QUFDRDtBQUNGLFdBckNDO0FBc0NGLGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sQ0FBQyxPQUFPLEVBQVAsQ0FBVSxPQUFWLEVBQVI7QUFDRCxXQXhDQztBQXlDRixtQkFBUyxZQUFXO0FBQ2xCLG1CQUFPLFFBQVEsUUFBUixDQUFpQixVQUFVLE9BQTNCLENBQVA7QUFDRCxXQTNDQzs7QUE2Q0YsZ0JBQU0sWUFBVztBQUNmLG1CQUFPLE9BQU8sRUFBUCxDQUFVLE9BQVYsRUFBUDtBQUNELFdBL0NDO0FBZ0RGLGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sT0FBTyxFQUFQLENBQVUsTUFBVixFQUFQO0FBQ0QsV0FsREM7QUFtREYsb0JBQVUsWUFBVztBQUNuQixtQkFBTyxRQUFRLFFBQVIsQ0FBaUIsVUFBVSxHQUEzQixDQUFQO0FBQ0QsV0FyREM7QUFzREYscUJBQVcsWUFBVztBQUNwQixtQkFBTyxTQUFTLFFBQVQsQ0FBa0IsVUFBVSxTQUE1QixDQUFQO0FBQ0QsV0F4REM7QUF5REYsZUFBSyxZQUFZO0FBQ2YsZ0JBQUcsT0FBTyxLQUFQLENBQWEsR0FBYixLQUFxQixTQUF4QixFQUFtQztBQUNqQyxxQkFBTyxLQUFQLENBQWEsR0FBYixHQUFvQixRQUFRLEdBQVIsQ0FBWSxXQUFaLEtBQTRCLEtBQWhEO0FBQ0Q7QUFDRCxtQkFBTyxPQUFPLEtBQVAsQ0FBYSxHQUFwQjtBQUNEO0FBOURDLFNBN21CUTs7QUE4cUJaLGlCQUFTLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDN0IsaUJBQU8sS0FBUCxDQUFhLGtCQUFiLEVBQWlDLElBQWpDLEVBQXVDLEtBQXZDO0FBQ0EsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsUUFBZixFQUF5QixJQUF6QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixnQkFBRyxFQUFFLGFBQUYsQ0FBZ0IsU0FBUyxJQUFULENBQWhCLENBQUgsRUFBb0M7QUFDbEMsZ0JBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxTQUFTLElBQVQsQ0FBZixFQUErQixLQUEvQjtBQUNELGFBRkQsTUFHSztBQUNILHVCQUFTLElBQVQsSUFBaUIsS0FBakI7QUFDRDtBQUNGLFdBUEksTUFRQTtBQUNILG1CQUFPLFNBQVMsSUFBVCxDQUFQO0FBQ0Q7QUFDRixTQTlyQlc7QUErckJaLGtCQUFVLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDOUIsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsTUFBZixFQUF1QixJQUF2QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixtQkFBTyxJQUFQLElBQWUsS0FBZjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLE9BQU8sSUFBUCxDQUFQO0FBQ0Q7QUFDRixTQXpzQlc7QUEwc0JaLGVBQU8sWUFBVztBQUNoQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsS0FBaEMsRUFBdUM7QUFDckMsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxLQUFQLEdBQWUsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWY7QUFDQSxxQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNEO0FBQ0Y7QUFDRixTQXB0Qlc7QUFxdEJaLGlCQUFTLFlBQVc7QUFDbEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLE9BQTdCLElBQXdDLFNBQVMsS0FBcEQsRUFBMkQ7QUFDekQsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxPQUFQLEdBQWlCLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFqQjtBQUNBLHFCQUFPLE9BQVAsQ0FBZSxLQUFmLENBQXFCLE9BQXJCLEVBQThCLFNBQTlCO0FBQ0Q7QUFDRjtBQUNGLFNBL3RCVztBQWd1QlosZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQWIsRUFBcUI7QUFDbkIsbUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLEtBQXJDLEVBQTRDLE9BQTVDLEVBQXFELFNBQVMsSUFBVCxHQUFnQixHQUFyRSxDQUFmO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGLFNBcnVCVztBQXN1QloscUJBQWE7QUFDWCxlQUFLLFVBQVMsT0FBVCxFQUFrQjtBQUNyQixnQkFDRSxXQURGLEVBRUUsYUFGRixFQUdFLFlBSEY7QUFLQSxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIsNEJBQWdCLElBQUksSUFBSixHQUFXLE9BQVgsRUFBaEI7QUFDQSw2QkFBZ0IsUUFBUSxXQUF4QjtBQUNBLDhCQUFnQixjQUFjLFlBQTlCO0FBQ0EscUJBQWdCLFdBQWhCO0FBQ0EsMEJBQVksSUFBWixDQUFpQjtBQUNmLHdCQUFtQixRQUFRLENBQVIsQ0FESjtBQUVmLDZCQUFtQixHQUFHLEtBQUgsQ0FBUyxJQUFULENBQWMsT0FBZCxFQUF1QixDQUF2QixLQUE2QixFQUZqQztBQUdmLDJCQUFtQixPQUhKO0FBSWYsa0NBQW1CO0FBSkosZUFBakI7QUFNRDtBQUNELHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLG1CQUFPLFdBQVAsQ0FBbUIsS0FBbkIsR0FBMkIsV0FBVyxPQUFPLFdBQVAsQ0FBbUIsT0FBOUIsRUFBdUMsR0FBdkMsQ0FBM0I7QUFDRCxXQXJCVTtBQXNCWCxtQkFBUyxZQUFXO0FBQ2xCLGdCQUNFLFFBQVEsU0FBUyxJQUFULEdBQWdCLEdBRDFCO0FBQUEsZ0JBRUUsWUFBWSxDQUZkO0FBSUEsbUJBQU8sS0FBUDtBQUNBLHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLGNBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDJCQUFhLEtBQUssZ0JBQUwsQ0FBYjtBQUNELGFBRkQ7QUFHQSxxQkFBUyxNQUFNLFNBQU4sR0FBa0IsSUFBM0I7QUFDQSxnQkFBRyxjQUFILEVBQW1CO0FBQ2pCLHVCQUFTLFFBQVEsY0FBUixHQUF5QixJQUFsQztBQUNEO0FBQ0QsZ0JBQUksQ0FBQyxRQUFRLEtBQVIsS0FBa0IsU0FBbEIsSUFBK0IsUUFBUSxLQUFSLEtBQWtCLFNBQWxELEtBQWdFLFlBQVksTUFBWixHQUFxQixDQUF6RixFQUE0RjtBQUMxRixzQkFBUSxjQUFSLENBQXVCLEtBQXZCO0FBQ0Esa0JBQUcsUUFBUSxLQUFYLEVBQWtCO0FBQ2hCLHdCQUFRLEtBQVIsQ0FBYyxXQUFkO0FBQ0QsZUFGRCxNQUdLO0FBQ0gsa0JBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDBCQUFRLEdBQVIsQ0FBWSxLQUFLLE1BQUwsSUFBZSxJQUFmLEdBQXNCLEtBQUssZ0JBQUwsQ0FBdEIsR0FBNkMsSUFBekQ7QUFDRCxpQkFGRDtBQUdEO0FBQ0Qsc0JBQVEsUUFBUjtBQUNEO0FBQ0QsMEJBQWMsRUFBZDtBQUNEO0FBakRVLFNBdHVCRDtBQXl4QlosZ0JBQVEsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDLE9BQWpDLEVBQTBDO0FBQ2hELGNBQ0UsU0FBUyxRQURYO0FBQUEsY0FFRSxRQUZGO0FBQUEsY0FHRSxLQUhGO0FBQUEsY0FJRSxRQUpGO0FBTUEsNEJBQWtCLG1CQUFtQixjQUFyQztBQUNBLG9CQUFrQixXQUFtQixPQUFyQztBQUNBLGNBQUcsT0FBTyxLQUFQLElBQWdCLFFBQWhCLElBQTRCLFdBQVcsU0FBMUMsRUFBcUQ7QUFDbkQsb0JBQVcsTUFBTSxLQUFOLENBQVksT0FBWixDQUFYO0FBQ0EsdUJBQVcsTUFBTSxNQUFOLEdBQWUsQ0FBMUI7QUFDQSxjQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ25DLGtCQUFJLGlCQUFrQixTQUFTLFFBQVYsR0FDakIsUUFBUSxNQUFNLFFBQVEsQ0FBZCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixFQUEyQixXQUEzQixFQUFSLEdBQW1ELE1BQU0sUUFBUSxDQUFkLEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBRGxDLEdBRWpCLEtBRko7QUFJQSxrQkFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxjQUFQLENBQWpCLEtBQThDLFNBQVMsUUFBM0QsRUFBdUU7QUFDckUseUJBQVMsT0FBTyxjQUFQLENBQVQ7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLGNBQVAsTUFBMkIsU0FBL0IsRUFBMkM7QUFDOUMsd0JBQVEsT0FBTyxjQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBLElBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sS0FBUCxDQUFqQixLQUFxQyxTQUFTLFFBQWxELEVBQThEO0FBQ2pFLHlCQUFTLE9BQU8sS0FBUCxDQUFUO0FBQ0QsZUFGSSxNQUdBLElBQUksT0FBTyxLQUFQLE1BQWtCLFNBQXRCLEVBQWtDO0FBQ3JDLHdCQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQTtBQUNILHVCQUFPLEtBQVAsQ0FBYSxNQUFNLE1BQW5CLEVBQTJCLEtBQTNCO0FBQ0EsdUJBQU8sS0FBUDtBQUNEO0FBQ0YsYUF2QkQ7QUF3QkQ7QUFDRCxjQUFLLEVBQUUsVUFBRixDQUFjLEtBQWQsQ0FBTCxFQUE2QjtBQUMzQix1QkFBVyxNQUFNLEtBQU4sQ0FBWSxPQUFaLEVBQXFCLGVBQXJCLENBQVg7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsdUJBQVcsS0FBWDtBQUNEO0FBQ0QsY0FBRyxFQUFFLE9BQUYsQ0FBVSxhQUFWLENBQUgsRUFBNkI7QUFDM0IsMEJBQWMsSUFBZCxDQUFtQixRQUFuQjtBQUNELFdBRkQsTUFHSyxJQUFHLGtCQUFrQixTQUFyQixFQUFnQztBQUNuQyw0QkFBZ0IsQ0FBQyxhQUFELEVBQWdCLFFBQWhCLENBQWhCO0FBQ0QsV0FGSSxNQUdBLElBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUM5Qiw0QkFBZ0IsUUFBaEI7QUFDRDtBQUNELGlCQUFPLEtBQVA7QUFDRDtBQTkwQlcsT0FBZDs7QUFrMUJGLFVBQUcsYUFBSCxFQUFrQjtBQUNoQixZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsaUJBQU8sVUFBUDtBQUNEO0FBQ0QsZUFBTyxNQUFQLENBQWMsS0FBZDtBQUNELE9BTEQsTUFNSztBQUNILFlBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUN6QixpQkFBTyxNQUFQLENBQWMsU0FBZDtBQUNEO0FBQ0QsZUFBTyxVQUFQO0FBQ0Q7QUFDRixLQWo0QkQ7O0FBbTRCQSxXQUFRLGtCQUFrQixTQUFuQixHQUNILGFBREcsR0FFSCxJQUZKO0FBSUQsR0FqNkJEOztBQW02QkEsSUFBRSxFQUFGLENBQUssT0FBTCxDQUFhLFFBQWIsR0FBd0I7O0FBRXRCLFVBQW9CLFNBRkU7QUFHdEIsZUFBb0IsU0FIRTs7QUFLdEIsWUFBb0IsS0FMRTtBQU10QixXQUFvQixLQU5FO0FBT3RCLGFBQW9CLEtBUEU7QUFRdEIsaUJBQW9CLElBUkU7O0FBVXRCLGdCQUFvQixNQVZFO0FBV3RCLHNCQUFvQixNQVhFOztBQWF0Qix1QkFBb0I7QUFDbEIsZ0JBQVU7QUFDUixjQUFTLFNBREQ7QUFFUixlQUFTLFNBRkQ7QUFHUixhQUFTLFNBSEQ7QUFJUixnQkFBUztBQUpELE9BRFE7QUFPbEIsY0FBUTtBQUNOLGNBQVMsU0FESDtBQUVOLGVBQVMsU0FGSDtBQUdOLGFBQVMsU0FISDtBQUlOLGdCQUFTO0FBSkg7QUFQVSxLQWJFOztBQTRCdEIsYUFBb0IsTUE1QkU7QUE2QnRCLGVBQW9CLEtBN0JFO0FBOEJ0QixjQUFvQixJQTlCRTtBQStCdEIsYUFBb0IsSUEvQkU7QUFnQ3RCLGdCQUFvQixLQWhDRTtBQWlDdEIsa0JBQW9CLEtBakNFO0FBa0N0QixnQkFBb0IsS0FsQ0U7O0FBb0N0QixjQUFvQixHQXBDRTs7QUFzQ3RCLGNBQW9CLFlBQVUsQ0FBRSxDQXRDVjtBQXVDdEIsWUFBb0IsWUFBVSxDQUFFLENBdkNWO0FBd0N0QixZQUFvQixZQUFVLENBQUUsQ0F4Q1Y7O0FBMEN0QixjQUFvQixZQUFVLENBQUUsQ0ExQ1Y7QUEyQ3RCLGVBQW9CLFlBQVUsQ0FBRSxDQTNDVjs7QUE2Q3RCLGVBQW9CO0FBQ2xCLGNBQVksUUFETTtBQUVsQixpQkFBWSxXQUZNO0FBR2xCLGNBQVksUUFITTtBQUlsQixXQUFZLEtBSk07QUFLbEIsZ0JBQVksVUFMTTtBQU1sQixjQUFZLFFBTk07QUFPbEIsYUFBWSxPQVBNO0FBUWxCLFdBQVksS0FSTTtBQVNsQixZQUFZLE1BVE07QUFVbEIsY0FBWSxRQVZNO0FBV2xCLGVBQVk7QUFYTSxLQTdDRTs7QUEyRHRCLGNBQVU7QUFDUixhQUFVLFFBREY7QUFFUixlQUFVLGdFQUZGO0FBR1IsY0FBVSxTQUhGO0FBSVIsZUFBVTtBQUpGLEtBM0RZOztBQWtFdEIsWUFBUTtBQUNOLFdBQWUscUJBRFQ7QUFFTixvQkFBZSxVQUZUO0FBR04sY0FBZTtBQUhULEtBbEVjOztBQXdFdEIsV0FBVTtBQUNSLGNBQWUsdUNBRFA7QUFFUixjQUFlLHNHQUZQO0FBR1Isb0JBQWUsZ0hBSFA7QUFJUixlQUFlLG9FQUpQO0FBS1IsZ0JBQWU7QUFMUDs7QUF4RVksR0FBeEI7QUFtRkMsQ0FqZ0NBLEVBaWdDRyxNQWpnQ0gsRUFpZ0NXLE1BamdDWCxFQWlnQ21CLFFBamdDbkIiLCJmaWxlIjoic2lkZWJhci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxuICogIyBTZW1hbnRpYyBVSSAtIFNpZGViYXJcbiAqIGh0dHA6Ly9naXRodWIuY29tL3NlbWFudGljLW9yZy9zZW1hbnRpYy11aS9cbiAqXG4gKlxuICogUmVsZWFzZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlXG4gKiBodHRwOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvTUlUXG4gKlxuICovXG5cbjsoZnVuY3Rpb24gKCQsIHdpbmRvdywgZG9jdW1lbnQsIHVuZGVmaW5lZCkge1xuXG5cInVzZSBzdHJpY3RcIjtcblxud2luZG93ID0gKHR5cGVvZiB3aW5kb3cgIT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93Lk1hdGggPT0gTWF0aClcbiAgPyB3aW5kb3dcbiAgOiAodHlwZW9mIHNlbGYgIT0gJ3VuZGVmaW5lZCcgJiYgc2VsZi5NYXRoID09IE1hdGgpXG4gICAgPyBzZWxmXG4gICAgOiBGdW5jdGlvbigncmV0dXJuIHRoaXMnKSgpXG47XG5cbiQuZm4uc2lkZWJhciA9IGZ1bmN0aW9uKHBhcmFtZXRlcnMpIHtcbiAgdmFyXG4gICAgJGFsbE1vZHVsZXMgICAgID0gJCh0aGlzKSxcbiAgICAkd2luZG93ICAgICAgICAgPSAkKHdpbmRvdyksXG4gICAgJGRvY3VtZW50ICAgICAgID0gJChkb2N1bWVudCksXG4gICAgJGh0bWwgICAgICAgICAgID0gJCgnaHRtbCcpLFxuICAgICRoZWFkICAgICAgICAgICA9ICQoJ2hlYWQnKSxcblxuICAgIG1vZHVsZVNlbGVjdG9yICA9ICRhbGxNb2R1bGVzLnNlbGVjdG9yIHx8ICcnLFxuXG4gICAgdGltZSAgICAgICAgICAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCksXG4gICAgcGVyZm9ybWFuY2UgICAgID0gW10sXG5cbiAgICBxdWVyeSAgICAgICAgICAgPSBhcmd1bWVudHNbMF0sXG4gICAgbWV0aG9kSW52b2tlZCAgID0gKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyksXG4gICAgcXVlcnlBcmd1bWVudHMgID0gW10uc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpLFxuXG4gICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lID0gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgfHwgd2luZG93Lm1velJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgfHwgd2luZG93LndlYmtpdFJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgfHwgd2luZG93Lm1zUmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gICAgICB8fCBmdW5jdGlvbihjYWxsYmFjaykgeyBzZXRUaW1lb3V0KGNhbGxiYWNrLCAwKTsgfSxcblxuICAgIHJldHVybmVkVmFsdWVcbiAgO1xuXG4gICRhbGxNb2R1bGVzXG4gICAgLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICB2YXJcbiAgICAgICAgc2V0dGluZ3MgICAgICAgID0gKCAkLmlzUGxhaW5PYmplY3QocGFyYW1ldGVycykgKVxuICAgICAgICAgID8gJC5leHRlbmQodHJ1ZSwge30sICQuZm4uc2lkZWJhci5zZXR0aW5ncywgcGFyYW1ldGVycylcbiAgICAgICAgICA6ICQuZXh0ZW5kKHt9LCAkLmZuLnNpZGViYXIuc2V0dGluZ3MpLFxuXG4gICAgICAgIHNlbGVjdG9yICAgICAgICA9IHNldHRpbmdzLnNlbGVjdG9yLFxuICAgICAgICBjbGFzc05hbWUgICAgICAgPSBzZXR0aW5ncy5jbGFzc05hbWUsXG4gICAgICAgIG5hbWVzcGFjZSAgICAgICA9IHNldHRpbmdzLm5hbWVzcGFjZSxcbiAgICAgICAgcmVnRXhwICAgICAgICAgID0gc2V0dGluZ3MucmVnRXhwLFxuICAgICAgICBlcnJvciAgICAgICAgICAgPSBzZXR0aW5ncy5lcnJvcixcblxuICAgICAgICBldmVudE5hbWVzcGFjZSAgPSAnLicgKyBuYW1lc3BhY2UsXG4gICAgICAgIG1vZHVsZU5hbWVzcGFjZSA9ICdtb2R1bGUtJyArIG5hbWVzcGFjZSxcblxuICAgICAgICAkbW9kdWxlICAgICAgICAgPSAkKHRoaXMpLFxuICAgICAgICAkY29udGV4dCAgICAgICAgPSAkKHNldHRpbmdzLmNvbnRleHQpLFxuXG4gICAgICAgICRzaWRlYmFycyAgICAgICA9ICRtb2R1bGUuY2hpbGRyZW4oc2VsZWN0b3Iuc2lkZWJhciksXG4gICAgICAgICRmaXhlZCAgICAgICAgICA9ICRjb250ZXh0LmNoaWxkcmVuKHNlbGVjdG9yLmZpeGVkKSxcbiAgICAgICAgJHB1c2hlciAgICAgICAgID0gJGNvbnRleHQuY2hpbGRyZW4oc2VsZWN0b3IucHVzaGVyKSxcbiAgICAgICAgJHN0eWxlLFxuXG4gICAgICAgIGVsZW1lbnQgICAgICAgICA9IHRoaXMsXG4gICAgICAgIGluc3RhbmNlICAgICAgICA9ICRtb2R1bGUuZGF0YShtb2R1bGVOYW1lc3BhY2UpLFxuXG4gICAgICAgIGVsZW1lbnROYW1lc3BhY2UsXG4gICAgICAgIGlkLFxuICAgICAgICBjdXJyZW50U2Nyb2xsLFxuICAgICAgICB0cmFuc2l0aW9uRXZlbnQsXG5cbiAgICAgICAgbW9kdWxlXG4gICAgICA7XG5cbiAgICAgIG1vZHVsZSAgICAgID0ge1xuXG4gICAgICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW5pdGlhbGl6aW5nIHNpZGViYXInLCBwYXJhbWV0ZXJzKTtcblxuICAgICAgICAgIG1vZHVsZS5jcmVhdGUuaWQoKTtcblxuICAgICAgICAgIHRyYW5zaXRpb25FdmVudCA9IG1vZHVsZS5nZXQudHJhbnNpdGlvbkV2ZW50KCk7XG5cbiAgICAgICAgICBpZihtb2R1bGUuaXMuaW9zKCkpIHtcbiAgICAgICAgICAgIG1vZHVsZS5zZXQuaW9zKCk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gYXZvaWRzIGxvY2tpbmcgcmVuZGVyaW5nIGlmIGluaXRpYWxpemVkIGluIG9uUmVhZHlcbiAgICAgICAgICBpZihzZXR0aW5ncy5kZWxheVNldHVwKSB7XG4gICAgICAgICAgICByZXF1ZXN0QW5pbWF0aW9uRnJhbWUobW9kdWxlLnNldHVwLmxheW91dCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgbW9kdWxlLnNldHVwLmxheW91dCgpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZShmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS5zZXR1cC5jYWNoZSgpO1xuICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgbW9kdWxlLmluc3RhbnRpYXRlKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5zdGFudGlhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTdG9yaW5nIGluc3RhbmNlIG9mIG1vZHVsZScsIG1vZHVsZSk7XG4gICAgICAgICAgaW5zdGFuY2UgPSBtb2R1bGU7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLmRhdGEobW9kdWxlTmFtZXNwYWNlLCBtb2R1bGUpXG4gICAgICAgICAgO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNyZWF0ZToge1xuICAgICAgICAgIGlkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlkID0gKE1hdGgucmFuZG9tKCkudG9TdHJpbmcoMTYpICsgJzAwMDAwMDAwMCcpLnN1YnN0cigyLDgpO1xuICAgICAgICAgICAgZWxlbWVudE5hbWVzcGFjZSA9ICcuJyArIGlkO1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0NyZWF0aW5nIHVuaXF1ZSBpZCBmb3IgZWxlbWVudCcsIGlkKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVzdHJveTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0Rlc3Ryb3lpbmcgcHJldmlvdXMgbW9kdWxlIGZvcicsICRtb2R1bGUpO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5vZmYoZXZlbnROYW1lc3BhY2UpXG4gICAgICAgICAgICAucmVtb3ZlRGF0YShtb2R1bGVOYW1lc3BhY2UpXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKG1vZHVsZS5pcy5pb3MoKSkge1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5pb3MoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLy8gYm91bmQgYnkgdXVpZFxuICAgICAgICAgICRjb250ZXh0Lm9mZihlbGVtZW50TmFtZXNwYWNlKTtcbiAgICAgICAgICAkd2luZG93Lm9mZihlbGVtZW50TmFtZXNwYWNlKTtcbiAgICAgICAgICAkZG9jdW1lbnQub2ZmKGVsZW1lbnROYW1lc3BhY2UpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGV2ZW50OiB7XG4gICAgICAgICAgY2xpY2thd2F5OiBmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGNsaWNrZWRJblB1c2hlciA9ICgkcHVzaGVyLmZpbmQoZXZlbnQudGFyZ2V0KS5sZW5ndGggPiAwIHx8ICRwdXNoZXIuaXMoZXZlbnQudGFyZ2V0KSksXG4gICAgICAgICAgICAgIGNsaWNrZWRDb250ZXh0ICA9ICgkY29udGV4dC5pcyhldmVudC50YXJnZXQpKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoY2xpY2tlZEluUHVzaGVyKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdVc2VyIGNsaWNrZWQgb24gZGltbWVkIHBhZ2UnKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmhpZGUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKGNsaWNrZWRDb250ZXh0KSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdVc2VyIGNsaWNrZWQgb24gZGltbWFibGUgY29udGV4dCAoc2NhbGVkIG91dCBwYWdlKScpO1xuICAgICAgICAgICAgICBtb2R1bGUuaGlkZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgdG91Y2g6IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICAvL2V2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgY29udGFpblNjcm9sbDogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgIGlmKGVsZW1lbnQuc2Nyb2xsVG9wIDw9IDApICB7XG4gICAgICAgICAgICAgIGVsZW1lbnQuc2Nyb2xsVG9wID0gMTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKChlbGVtZW50LnNjcm9sbFRvcCArIGVsZW1lbnQub2Zmc2V0SGVpZ2h0KSA+PSBlbGVtZW50LnNjcm9sbEhlaWdodCkge1xuICAgICAgICAgICAgICBlbGVtZW50LnNjcm9sbFRvcCA9IGVsZW1lbnQuc2Nyb2xsSGVpZ2h0IC0gZWxlbWVudC5vZmZzZXRIZWlnaHQgLSAxO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgc2Nyb2xsOiBmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgaWYoICQoZXZlbnQudGFyZ2V0KS5jbG9zZXN0KHNlbGVjdG9yLnNpZGViYXIpLmxlbmd0aCA9PT0gMCApIHtcbiAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgYmluZDoge1xuICAgICAgICAgIGNsaWNrYXdheTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQWRkaW5nIGNsaWNrYXdheSBldmVudHMgdG8gY29udGV4dCcsICRjb250ZXh0KTtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmNsb3NhYmxlKSB7XG4gICAgICAgICAgICAgICRjb250ZXh0XG4gICAgICAgICAgICAgICAgLm9uKCdjbGljaycgICAgKyBlbGVtZW50TmFtZXNwYWNlLCBtb2R1bGUuZXZlbnQuY2xpY2thd2F5KVxuICAgICAgICAgICAgICAgIC5vbigndG91Y2hlbmQnICsgZWxlbWVudE5hbWVzcGFjZSwgbW9kdWxlLmV2ZW50LmNsaWNrYXdheSlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgc2Nyb2xsTG9jazogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5zY3JvbGxMb2NrKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRGlzYWJsaW5nIHBhZ2Ugc2Nyb2xsJyk7XG4gICAgICAgICAgICAgICR3aW5kb3dcbiAgICAgICAgICAgICAgICAub24oJ0RPTU1vdXNlU2Nyb2xsJyArIGVsZW1lbnROYW1lc3BhY2UsIG1vZHVsZS5ldmVudC5zY3JvbGwpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdBZGRpbmcgZXZlbnRzIHRvIGNvbnRhaW4gc2lkZWJhciBzY3JvbGwnKTtcbiAgICAgICAgICAgICRkb2N1bWVudFxuICAgICAgICAgICAgICAub24oJ3RvdWNobW92ZScgKyBlbGVtZW50TmFtZXNwYWNlLCBtb2R1bGUuZXZlbnQudG91Y2gpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5vbignc2Nyb2xsJyArIGV2ZW50TmFtZXNwYWNlLCBtb2R1bGUuZXZlbnQuY29udGFpblNjcm9sbClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHVuYmluZDoge1xuICAgICAgICAgIGNsaWNrYXdheTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVtb3ZpbmcgY2xpY2thd2F5IGV2ZW50cyBmcm9tIGNvbnRleHQnLCAkY29udGV4dCk7XG4gICAgICAgICAgICAkY29udGV4dC5vZmYoZWxlbWVudE5hbWVzcGFjZSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzY3JvbGxMb2NrOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZW1vdmluZyBzY3JvbGwgbG9jayBmcm9tIHBhZ2UnKTtcbiAgICAgICAgICAgICRkb2N1bWVudC5vZmYoZWxlbWVudE5hbWVzcGFjZSk7XG4gICAgICAgICAgICAkd2luZG93Lm9mZihlbGVtZW50TmFtZXNwYWNlKTtcbiAgICAgICAgICAgICRtb2R1bGUub2ZmKCdzY3JvbGwnICsgZXZlbnROYW1lc3BhY2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBhZGQ6IHtcbiAgICAgICAgICBpbmxpbmVDU1M6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHdpZHRoICAgICA9IG1vZHVsZS5jYWNoZS53aWR0aCAgfHwgJG1vZHVsZS5vdXRlcldpZHRoKCksXG4gICAgICAgICAgICAgIGhlaWdodCAgICA9IG1vZHVsZS5jYWNoZS5oZWlnaHQgfHwgJG1vZHVsZS5vdXRlckhlaWdodCgpLFxuICAgICAgICAgICAgICBpc1JUTCAgICAgPSBtb2R1bGUuaXMucnRsKCksXG4gICAgICAgICAgICAgIGRpcmVjdGlvbiA9IG1vZHVsZS5nZXQuZGlyZWN0aW9uKCksXG4gICAgICAgICAgICAgIGRpc3RhbmNlICA9IHtcbiAgICAgICAgICAgICAgICBsZWZ0ICAgOiB3aWR0aCxcbiAgICAgICAgICAgICAgICByaWdodCAgOiAtd2lkdGgsXG4gICAgICAgICAgICAgICAgdG9wICAgIDogaGVpZ2h0LFxuICAgICAgICAgICAgICAgIGJvdHRvbSA6IC1oZWlnaHRcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgc3R5bGVcbiAgICAgICAgICAgIDtcblxuICAgICAgICAgICAgaWYoaXNSVEwpe1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUlRMIGRldGVjdGVkLCBmbGlwcGluZyB3aWR0aHMnKTtcbiAgICAgICAgICAgICAgZGlzdGFuY2UubGVmdCA9IC13aWR0aDtcbiAgICAgICAgICAgICAgZGlzdGFuY2UucmlnaHQgPSB3aWR0aDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgc3R5bGUgID0gJzxzdHlsZT4nO1xuXG4gICAgICAgICAgICBpZihkaXJlY3Rpb24gPT09ICdsZWZ0JyB8fCBkaXJlY3Rpb24gPT09ICdyaWdodCcpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdBZGRpbmcgQ1NTIHJ1bGVzIGZvciBhbmltYXRpb24gZGlzdGFuY2UnLCB3aWR0aCk7XG4gICAgICAgICAgICAgIHN0eWxlICArPSAnJ1xuICAgICAgICAgICAgICAgICsgJyAudWkudmlzaWJsZS4nICsgZGlyZWN0aW9uICsgJy5zaWRlYmFyIH4gLmZpeGVkLCdcbiAgICAgICAgICAgICAgICArICcgLnVpLnZpc2libGUuJyArIGRpcmVjdGlvbiArICcuc2lkZWJhciB+IC5wdXNoZXIgeydcbiAgICAgICAgICAgICAgICArICcgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoJysgZGlzdGFuY2VbZGlyZWN0aW9uXSArICdweCwgMCwgMCk7J1xuICAgICAgICAgICAgICAgICsgJyAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgnKyBkaXN0YW5jZVtkaXJlY3Rpb25dICsgJ3B4LCAwLCAwKTsnXG4gICAgICAgICAgICAgICAgKyAnIH0nXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoZGlyZWN0aW9uID09PSAndG9wJyB8fCBkaXJlY3Rpb24gPT0gJ2JvdHRvbScpIHtcbiAgICAgICAgICAgICAgc3R5bGUgICs9ICcnXG4gICAgICAgICAgICAgICAgKyAnIC51aS52aXNpYmxlLicgKyBkaXJlY3Rpb24gKyAnLnNpZGViYXIgfiAuZml4ZWQsJ1xuICAgICAgICAgICAgICAgICsgJyAudWkudmlzaWJsZS4nICsgZGlyZWN0aW9uICsgJy5zaWRlYmFyIH4gLnB1c2hlciB7J1xuICAgICAgICAgICAgICAgICsgJyAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAnICsgZGlzdGFuY2VbZGlyZWN0aW9uXSArICdweCwgMCk7J1xuICAgICAgICAgICAgICAgICsgJyAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAnICsgZGlzdGFuY2VbZGlyZWN0aW9uXSArICdweCwgMCk7J1xuICAgICAgICAgICAgICAgICsgJyB9J1xuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8qIElFIGlzIG9ubHkgYnJvd3NlciBub3QgdG8gY3JlYXRlIGNvbnRleHQgd2l0aCB0cmFuc2Zvcm1zICovXG4gICAgICAgICAgICAvKiBodHRwczovL3d3dy53My5vcmcvQnVncy9QdWJsaWMvc2hvd19idWcuY2dpP2lkPTE2MzI4ICovXG4gICAgICAgICAgICBpZiggbW9kdWxlLmlzLmllKCkgKSB7XG4gICAgICAgICAgICAgIGlmKGRpcmVjdGlvbiA9PT0gJ2xlZnQnIHx8IGRpcmVjdGlvbiA9PT0gJ3JpZ2h0Jykge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWRkaW5nIENTUyBydWxlcyBmb3IgYW5pbWF0aW9uIGRpc3RhbmNlJywgd2lkdGgpO1xuICAgICAgICAgICAgICAgIHN0eWxlICArPSAnJ1xuICAgICAgICAgICAgICAgICAgKyAnIGJvZHkucHVzaGFibGUgPiAudWkudmlzaWJsZS4nICsgZGlyZWN0aW9uICsgJy5zaWRlYmFyIH4gLnB1c2hlcjphZnRlciB7J1xuICAgICAgICAgICAgICAgICAgKyAnICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKCcrIGRpc3RhbmNlW2RpcmVjdGlvbl0gKyAncHgsIDAsIDApOydcbiAgICAgICAgICAgICAgICAgICsgJyAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgnKyBkaXN0YW5jZVtkaXJlY3Rpb25dICsgJ3B4LCAwLCAwKTsnXG4gICAgICAgICAgICAgICAgICArICcgfSdcbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZihkaXJlY3Rpb24gPT09ICd0b3AnIHx8IGRpcmVjdGlvbiA9PSAnYm90dG9tJykge1xuICAgICAgICAgICAgICAgIHN0eWxlICArPSAnJ1xuICAgICAgICAgICAgICAgICAgKyAnIGJvZHkucHVzaGFibGUgPiAudWkudmlzaWJsZS4nICsgZGlyZWN0aW9uICsgJy5zaWRlYmFyIH4gLnB1c2hlcjphZnRlciB7J1xuICAgICAgICAgICAgICAgICAgKyAnICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsICcgKyBkaXN0YW5jZVtkaXJlY3Rpb25dICsgJ3B4LCAwKTsnXG4gICAgICAgICAgICAgICAgICArICcgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgJyArIGRpc3RhbmNlW2RpcmVjdGlvbl0gKyAncHgsIDApOydcbiAgICAgICAgICAgICAgICAgICsgJyB9J1xuICAgICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAvKiBvcHBvc2l0ZSBzaWRlcyB2aXNpYmxlIGZvcmNlcyBjb250ZW50IG92ZXJsYXkgKi9cbiAgICAgICAgICAgICAgc3R5bGUgKz0gJydcbiAgICAgICAgICAgICAgICArICcgYm9keS5wdXNoYWJsZSA+IC51aS52aXNpYmxlLmxlZnQuc2lkZWJhciB+IC51aS52aXNpYmxlLnJpZ2h0LnNpZGViYXIgfiAucHVzaGVyOmFmdGVyLCdcbiAgICAgICAgICAgICAgICArICcgYm9keS5wdXNoYWJsZSA+IC51aS52aXNpYmxlLnJpZ2h0LnNpZGViYXIgfiAudWkudmlzaWJsZS5sZWZ0LnNpZGViYXIgfiAucHVzaGVyOmFmdGVyIHsnXG4gICAgICAgICAgICAgICAgKyAnICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDBweCwgMCwgMCk7J1xuICAgICAgICAgICAgICAgICsgJyAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwcHgsIDAsIDApOydcbiAgICAgICAgICAgICAgICArICcgfSdcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc3R5bGUgKz0gJzwvc3R5bGU+JztcbiAgICAgICAgICAgICRzdHlsZSA9ICQoc3R5bGUpXG4gICAgICAgICAgICAgIC5hcHBlbmRUbygkaGVhZClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWRkaW5nIHNpemluZyBjc3MgdG8gaGVhZCcsICRzdHlsZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlZnJlc2g6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZWZyZXNoaW5nIHNlbGVjdG9yIGNhY2hlJyk7XG4gICAgICAgICAgJGNvbnRleHQgID0gJChzZXR0aW5ncy5jb250ZXh0KTtcbiAgICAgICAgICAkc2lkZWJhcnMgPSAkY29udGV4dC5jaGlsZHJlbihzZWxlY3Rvci5zaWRlYmFyKTtcbiAgICAgICAgICAkcHVzaGVyICAgPSAkY29udGV4dC5jaGlsZHJlbihzZWxlY3Rvci5wdXNoZXIpO1xuICAgICAgICAgICRmaXhlZCAgICA9ICRjb250ZXh0LmNoaWxkcmVuKHNlbGVjdG9yLmZpeGVkKTtcbiAgICAgICAgICBtb2R1bGUuY2xlYXIuY2FjaGUoKTtcbiAgICAgICAgfSxcblxuICAgICAgICByZWZyZXNoU2lkZWJhcnM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZWZyZXNoaW5nIG90aGVyIHNpZGViYXJzJyk7XG4gICAgICAgICAgJHNpZGViYXJzID0gJGNvbnRleHQuY2hpbGRyZW4oc2VsZWN0b3Iuc2lkZWJhcik7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVwYWludDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0ZvcmNpbmcgcmVwYWludCBldmVudCcpO1xuICAgICAgICAgIGVsZW1lbnQuc3R5bGUuZGlzcGxheSA9ICdub25lJztcbiAgICAgICAgICB2YXIgaWdub3JlZCA9IGVsZW1lbnQub2Zmc2V0SGVpZ2h0O1xuICAgICAgICAgIGVsZW1lbnQuc2Nyb2xsVG9wID0gZWxlbWVudC5zY3JvbGxUb3A7XG4gICAgICAgICAgZWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gJyc7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0dXA6IHtcbiAgICAgICAgICBjYWNoZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuY2FjaGUgPSB7XG4gICAgICAgICAgICAgIHdpZHRoICA6ICRtb2R1bGUub3V0ZXJXaWR0aCgpLFxuICAgICAgICAgICAgICBoZWlnaHQgOiAkbW9kdWxlLm91dGVySGVpZ2h0KCksXG4gICAgICAgICAgICAgIHJ0bCAgICA6ICgkbW9kdWxlLmNzcygnZGlyZWN0aW9uJykgPT0gJ3J0bCcpXG4gICAgICAgICAgICB9O1xuICAgICAgICAgIH0sXG4gICAgICAgICAgbGF5b3V0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKCAkY29udGV4dC5jaGlsZHJlbihzZWxlY3Rvci5wdXNoZXIpLmxlbmd0aCA9PT0gMCApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdBZGRpbmcgd3JhcHBlciBlbGVtZW50IGZvciBzaWRlYmFyJyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5wdXNoZXIpO1xuICAgICAgICAgICAgICAkcHVzaGVyID0gJCgnPGRpdiBjbGFzcz1cInB1c2hlclwiIC8+Jyk7XG4gICAgICAgICAgICAgICRjb250ZXh0XG4gICAgICAgICAgICAgICAgLmNoaWxkcmVuKClcbiAgICAgICAgICAgICAgICAgIC5ub3Qoc2VsZWN0b3Iub21pdHRlZClcbiAgICAgICAgICAgICAgICAgIC5ub3QoJHNpZGViYXJzKVxuICAgICAgICAgICAgICAgICAgLndyYXBBbGwoJHB1c2hlcilcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBtb2R1bGUucmVmcmVzaCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoJG1vZHVsZS5uZXh0QWxsKHNlbGVjdG9yLnB1c2hlcikubGVuZ3RoID09PSAwIHx8ICRtb2R1bGUubmV4dEFsbChzZWxlY3Rvci5wdXNoZXIpWzBdICE9PSAkcHVzaGVyWzBdKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnTW92ZWQgc2lkZWJhciB0byBjb3JyZWN0IHBhcmVudCBlbGVtZW50Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5tb3ZlZFNpZGViYXIsIGVsZW1lbnQpO1xuICAgICAgICAgICAgICAkbW9kdWxlLmRldGFjaCgpLnByZXBlbmRUbygkY29udGV4dCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5yZWZyZXNoKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUuY2xlYXIuY2FjaGUoKTtcbiAgICAgICAgICAgIG1vZHVsZS5zZXQucHVzaGFibGUoKTtcbiAgICAgICAgICAgIG1vZHVsZS5zZXQuZGlyZWN0aW9uKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGF0dGFjaEV2ZW50czogZnVuY3Rpb24oc2VsZWN0b3IsIGV2ZW50KSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICAkdG9nZ2xlID0gJChzZWxlY3RvcilcbiAgICAgICAgICA7XG4gICAgICAgICAgZXZlbnQgPSAkLmlzRnVuY3Rpb24obW9kdWxlW2V2ZW50XSlcbiAgICAgICAgICAgID8gbW9kdWxlW2V2ZW50XVxuICAgICAgICAgICAgOiBtb2R1bGUudG9nZ2xlXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKCR0b2dnbGUubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdBdHRhY2hpbmcgc2lkZWJhciBldmVudHMgdG8gZWxlbWVudCcsIHNlbGVjdG9yLCBldmVudCk7XG4gICAgICAgICAgICAkdG9nZ2xlXG4gICAgICAgICAgICAgIC5vbignY2xpY2snICsgZXZlbnROYW1lc3BhY2UsIGV2ZW50KVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5ub3RGb3VuZCwgc2VsZWN0b3IpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzaG93OiBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICAgIGNhbGxiYWNrID0gJC5pc0Z1bmN0aW9uKGNhbGxiYWNrKVxuICAgICAgICAgICAgPyBjYWxsYmFja1xuICAgICAgICAgICAgOiBmdW5jdGlvbigpe31cbiAgICAgICAgICA7XG4gICAgICAgICAgaWYobW9kdWxlLmlzLmhpZGRlbigpKSB7XG4gICAgICAgICAgICBtb2R1bGUucmVmcmVzaFNpZGViYXJzKCk7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5vdmVybGF5KSAge1xuICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3Iub3ZlcmxheSk7XG4gICAgICAgICAgICAgIHNldHRpbmdzLnRyYW5zaXRpb24gPSAnb3ZlcmxheSc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUucmVmcmVzaCgpO1xuICAgICAgICAgICAgaWYobW9kdWxlLm90aGVyc0FjdGl2ZSgpKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnT3RoZXIgc2lkZWJhcnMgY3VycmVudGx5IHZpc2libGUnKTtcbiAgICAgICAgICAgICAgaWYoc2V0dGluZ3MuZXhjbHVzaXZlKSB7XG4gICAgICAgICAgICAgICAgLy8gaWYgbm90IG92ZXJsYXkgcXVldWUgYW5pbWF0aW9uIGFmdGVyIGhpZGVcbiAgICAgICAgICAgICAgICBpZihzZXR0aW5ncy50cmFuc2l0aW9uICE9ICdvdmVybGF5Jykge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmhpZGVPdGhlcnMobW9kdWxlLnNob3cpO1xuICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5oaWRlT3RoZXJzKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHNldHRpbmdzLnRyYW5zaXRpb24gPSAnb3ZlcmxheSc7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS5wdXNoUGFnZShmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgY2FsbGJhY2suY2FsbChlbGVtZW50KTtcbiAgICAgICAgICAgICAgc2V0dGluZ3Mub25TaG93LmNhbGwoZWxlbWVudCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHNldHRpbmdzLm9uQ2hhbmdlLmNhbGwoZWxlbWVudCk7XG4gICAgICAgICAgICBzZXR0aW5ncy5vblZpc2libGUuY2FsbChlbGVtZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NpZGViYXIgaXMgYWxyZWFkeSB2aXNpYmxlJyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGhpZGU6IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG4gICAgICAgICAgY2FsbGJhY2sgPSAkLmlzRnVuY3Rpb24oY2FsbGJhY2spXG4gICAgICAgICAgICA/IGNhbGxiYWNrXG4gICAgICAgICAgICA6IGZ1bmN0aW9uKCl7fVxuICAgICAgICAgIDtcbiAgICAgICAgICBpZihtb2R1bGUuaXMudmlzaWJsZSgpIHx8IG1vZHVsZS5pcy5hbmltYXRpbmcoKSkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdIaWRpbmcgc2lkZWJhcicsIGNhbGxiYWNrKTtcbiAgICAgICAgICAgIG1vZHVsZS5yZWZyZXNoU2lkZWJhcnMoKTtcbiAgICAgICAgICAgIG1vZHVsZS5wdWxsUGFnZShmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgY2FsbGJhY2suY2FsbChlbGVtZW50KTtcbiAgICAgICAgICAgICAgc2V0dGluZ3Mub25IaWRkZW4uY2FsbChlbGVtZW50KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgc2V0dGluZ3Mub25DaGFuZ2UuY2FsbChlbGVtZW50KTtcbiAgICAgICAgICAgIHNldHRpbmdzLm9uSGlkZS5jYWxsKGVsZW1lbnQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBvdGhlcnNBbmltYXRpbmc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHJldHVybiAoJHNpZGViYXJzLm5vdCgkbW9kdWxlKS5maWx0ZXIoJy4nICsgY2xhc3NOYW1lLmFuaW1hdGluZykubGVuZ3RoID4gMCk7XG4gICAgICAgIH0sXG4gICAgICAgIG90aGVyc1Zpc2libGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHJldHVybiAoJHNpZGViYXJzLm5vdCgkbW9kdWxlKS5maWx0ZXIoJy4nICsgY2xhc3NOYW1lLnZpc2libGUpLmxlbmd0aCA+IDApO1xuICAgICAgICB9LFxuICAgICAgICBvdGhlcnNBY3RpdmU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHJldHVybihtb2R1bGUub3RoZXJzVmlzaWJsZSgpIHx8IG1vZHVsZS5vdGhlcnNBbmltYXRpbmcoKSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaGlkZU90aGVyczogZnVuY3Rpb24oY2FsbGJhY2spIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgICRvdGhlclNpZGViYXJzID0gJHNpZGViYXJzLm5vdCgkbW9kdWxlKS5maWx0ZXIoJy4nICsgY2xhc3NOYW1lLnZpc2libGUpLFxuICAgICAgICAgICAgc2lkZWJhckNvdW50ICAgPSAkb3RoZXJTaWRlYmFycy5sZW5ndGgsXG4gICAgICAgICAgICBjYWxsYmFja0NvdW50ICA9IDBcbiAgICAgICAgICA7XG4gICAgICAgICAgY2FsbGJhY2sgPSBjYWxsYmFjayB8fCBmdW5jdGlvbigpe307XG4gICAgICAgICAgJG90aGVyU2lkZWJhcnNcbiAgICAgICAgICAgIC5zaWRlYmFyKCdoaWRlJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIGNhbGxiYWNrQ291bnQrKztcbiAgICAgICAgICAgICAgaWYoY2FsbGJhY2tDb3VudCA9PSBzaWRlYmFyQ291bnQpIHtcbiAgICAgICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICB0b2dnbGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdEZXRlcm1pbmluZyB0b2dnbGVkIGRpcmVjdGlvbicpO1xuICAgICAgICAgIGlmKG1vZHVsZS5pcy5oaWRkZW4oKSkge1xuICAgICAgICAgICAgbW9kdWxlLnNob3coKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBtb2R1bGUuaGlkZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBwdXNoUGFnZTogZnVuY3Rpb24oY2FsbGJhY2spIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIHRyYW5zaXRpb24gPSBtb2R1bGUuZ2V0LnRyYW5zaXRpb24oKSxcbiAgICAgICAgICAgICR0cmFuc2l0aW9uID0gKHRyYW5zaXRpb24gPT09ICdvdmVybGF5JyB8fCBtb2R1bGUub3RoZXJzQWN0aXZlKCkpXG4gICAgICAgICAgICAgID8gJG1vZHVsZVxuICAgICAgICAgICAgICA6ICRwdXNoZXIsXG4gICAgICAgICAgICBhbmltYXRlLFxuICAgICAgICAgICAgZGltLFxuICAgICAgICAgICAgdHJhbnNpdGlvbkVuZFxuICAgICAgICAgIDtcbiAgICAgICAgICBjYWxsYmFjayA9ICQuaXNGdW5jdGlvbihjYWxsYmFjaylcbiAgICAgICAgICAgID8gY2FsbGJhY2tcbiAgICAgICAgICAgIDogZnVuY3Rpb24oKXt9XG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKHNldHRpbmdzLnRyYW5zaXRpb24gPT0gJ3NjYWxlIGRvd24nKSB7XG4gICAgICAgICAgICBtb2R1bGUuc2Nyb2xsVG9Ub3AoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgbW9kdWxlLnNldC50cmFuc2l0aW9uKHRyYW5zaXRpb24pO1xuICAgICAgICAgIG1vZHVsZS5yZXBhaW50KCk7XG4gICAgICAgICAgYW5pbWF0ZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLmJpbmQuY2xpY2thd2F5KCk7XG4gICAgICAgICAgICBtb2R1bGUuYWRkLmlubGluZUNTUygpO1xuICAgICAgICAgICAgbW9kdWxlLnNldC5hbmltYXRpbmcoKTtcbiAgICAgICAgICAgIG1vZHVsZS5zZXQudmlzaWJsZSgpO1xuICAgICAgICAgIH07XG4gICAgICAgICAgZGltID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuc2V0LmRpbW1lZCgpO1xuICAgICAgICAgIH07XG4gICAgICAgICAgdHJhbnNpdGlvbkVuZCA9IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICBpZiggZXZlbnQudGFyZ2V0ID09ICR0cmFuc2l0aW9uWzBdICkge1xuICAgICAgICAgICAgICAkdHJhbnNpdGlvbi5vZmYodHJhbnNpdGlvbkV2ZW50ICsgZWxlbWVudE5hbWVzcGFjZSwgdHJhbnNpdGlvbkVuZCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUuYW5pbWF0aW5nKCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5iaW5kLnNjcm9sbExvY2soKTtcbiAgICAgICAgICAgICAgY2FsbGJhY2suY2FsbChlbGVtZW50KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9O1xuICAgICAgICAgICR0cmFuc2l0aW9uLm9mZih0cmFuc2l0aW9uRXZlbnQgKyBlbGVtZW50TmFtZXNwYWNlKTtcbiAgICAgICAgICAkdHJhbnNpdGlvbi5vbih0cmFuc2l0aW9uRXZlbnQgKyBlbGVtZW50TmFtZXNwYWNlLCB0cmFuc2l0aW9uRW5kKTtcbiAgICAgICAgICByZXF1ZXN0QW5pbWF0aW9uRnJhbWUoYW5pbWF0ZSk7XG4gICAgICAgICAgaWYoc2V0dGluZ3MuZGltUGFnZSAmJiAhbW9kdWxlLm90aGVyc1Zpc2libGUoKSkge1xuICAgICAgICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKGRpbSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHB1bGxQYWdlOiBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgdHJhbnNpdGlvbiA9IG1vZHVsZS5nZXQudHJhbnNpdGlvbigpLFxuICAgICAgICAgICAgJHRyYW5zaXRpb24gPSAodHJhbnNpdGlvbiA9PSAnb3ZlcmxheScgfHwgbW9kdWxlLm90aGVyc0FjdGl2ZSgpKVxuICAgICAgICAgICAgICA/ICRtb2R1bGVcbiAgICAgICAgICAgICAgOiAkcHVzaGVyLFxuICAgICAgICAgICAgYW5pbWF0ZSxcbiAgICAgICAgICAgIHRyYW5zaXRpb25FbmRcbiAgICAgICAgICA7XG4gICAgICAgICAgY2FsbGJhY2sgPSAkLmlzRnVuY3Rpb24oY2FsbGJhY2spXG4gICAgICAgICAgICA/IGNhbGxiYWNrXG4gICAgICAgICAgICA6IGZ1bmN0aW9uKCl7fVxuICAgICAgICAgIDtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVtb3ZpbmcgY29udGV4dCBwdXNoIHN0YXRlJywgbW9kdWxlLmdldC5kaXJlY3Rpb24oKSk7XG5cbiAgICAgICAgICBtb2R1bGUudW5iaW5kLmNsaWNrYXdheSgpO1xuICAgICAgICAgIG1vZHVsZS51bmJpbmQuc2Nyb2xsTG9jaygpO1xuXG4gICAgICAgICAgYW5pbWF0ZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnNldC50cmFuc2l0aW9uKHRyYW5zaXRpb24pO1xuICAgICAgICAgICAgbW9kdWxlLnNldC5hbmltYXRpbmcoKTtcbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUudmlzaWJsZSgpO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MuZGltUGFnZSAmJiAhbW9kdWxlLm90aGVyc1Zpc2libGUoKSkge1xuICAgICAgICAgICAgICAkcHVzaGVyLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5kaW1tZWQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH07XG4gICAgICAgICAgdHJhbnNpdGlvbkVuZCA9IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICBpZiggZXZlbnQudGFyZ2V0ID09ICR0cmFuc2l0aW9uWzBdICkge1xuICAgICAgICAgICAgICAkdHJhbnNpdGlvbi5vZmYodHJhbnNpdGlvbkV2ZW50ICsgZWxlbWVudE5hbWVzcGFjZSwgdHJhbnNpdGlvbkVuZCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUuYW5pbWF0aW5nKCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUudHJhbnNpdGlvbigpO1xuICAgICAgICAgICAgICBtb2R1bGUucmVtb3ZlLmlubGluZUNTUygpO1xuICAgICAgICAgICAgICBpZih0cmFuc2l0aW9uID09ICdzY2FsZSBkb3duJyB8fCAoc2V0dGluZ3MucmV0dXJuU2Nyb2xsICYmIG1vZHVsZS5pcy5tb2JpbGUoKSkgKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnNjcm9sbEJhY2soKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBjYWxsYmFjay5jYWxsKGVsZW1lbnQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH07XG4gICAgICAgICAgJHRyYW5zaXRpb24ub2ZmKHRyYW5zaXRpb25FdmVudCArIGVsZW1lbnROYW1lc3BhY2UpO1xuICAgICAgICAgICR0cmFuc2l0aW9uLm9uKHRyYW5zaXRpb25FdmVudCArIGVsZW1lbnROYW1lc3BhY2UsIHRyYW5zaXRpb25FbmQpO1xuICAgICAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZShhbmltYXRlKTtcbiAgICAgICAgfSxcblxuICAgICAgICBzY3JvbGxUb1RvcDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1Njcm9sbGluZyB0byB0b3Agb2YgcGFnZSB0byBhdm9pZCBhbmltYXRpb24gaXNzdWVzJyk7XG4gICAgICAgICAgY3VycmVudFNjcm9sbCA9ICQod2luZG93KS5zY3JvbGxUb3AoKTtcbiAgICAgICAgICAkbW9kdWxlLnNjcm9sbFRvcCgwKTtcbiAgICAgICAgICB3aW5kb3cuc2Nyb2xsVG8oMCwgMCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2Nyb2xsQmFjazogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1Njcm9sbGluZyBiYWNrIHRvIG9yaWdpbmFsIHBhZ2UgcG9zaXRpb24nKTtcbiAgICAgICAgICB3aW5kb3cuc2Nyb2xsVG8oMCwgY3VycmVudFNjcm9sbCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2xlYXI6IHtcbiAgICAgICAgICBjYWNoZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQ2xlYXJpbmcgY2FjaGVkIGRpbWVuc2lvbnMnKTtcbiAgICAgICAgICAgIG1vZHVsZS5jYWNoZSA9IHt9O1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZXQ6IHtcblxuICAgICAgICAgIC8vIGlvcyBvbmx5IChzY3JvbGwgb24gaHRtbCBub3QgZG9jdW1lbnQpLiBUaGlzIHByZXZlbnQgYXV0by1yZXNpemUgY2FudmFzL3Njcm9sbCBpbiBpb3NcbiAgICAgICAgICBpb3M6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJGh0bWwuYWRkQ2xhc3MoY2xhc3NOYW1lLmlvcyk7XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIC8vIGNvbnRhaW5lclxuICAgICAgICAgIHB1c2hlZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkY29udGV4dC5hZGRDbGFzcyhjbGFzc05hbWUucHVzaGVkKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHB1c2hhYmxlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRjb250ZXh0LmFkZENsYXNzKGNsYXNzTmFtZS5wdXNoYWJsZSk7XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIC8vIHB1c2hlclxuICAgICAgICAgIGRpbW1lZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkcHVzaGVyLmFkZENsYXNzKGNsYXNzTmFtZS5kaW1tZWQpO1xuICAgICAgICAgIH0sXG5cbiAgICAgICAgICAvLyBzaWRlYmFyXG4gICAgICAgICAgYWN0aXZlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRtb2R1bGUuYWRkQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBhbmltYXRpbmc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZS5hZGRDbGFzcyhjbGFzc05hbWUuYW5pbWF0aW5nKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHRyYW5zaXRpb246IGZ1bmN0aW9uKHRyYW5zaXRpb24pIHtcbiAgICAgICAgICAgIHRyYW5zaXRpb24gPSB0cmFuc2l0aW9uIHx8IG1vZHVsZS5nZXQudHJhbnNpdGlvbigpO1xuICAgICAgICAgICAgJG1vZHVsZS5hZGRDbGFzcyh0cmFuc2l0aW9uKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpcmVjdGlvbjogZnVuY3Rpb24oZGlyZWN0aW9uKSB7XG4gICAgICAgICAgICBkaXJlY3Rpb24gPSBkaXJlY3Rpb24gfHwgbW9kdWxlLmdldC5kaXJlY3Rpb24oKTtcbiAgICAgICAgICAgICRtb2R1bGUuYWRkQ2xhc3MoY2xhc3NOYW1lW2RpcmVjdGlvbl0pO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgdmlzaWJsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkbW9kdWxlLmFkZENsYXNzKGNsYXNzTmFtZS52aXNpYmxlKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIG92ZXJsYXk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZS5hZGRDbGFzcyhjbGFzc05hbWUub3ZlcmxheSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICByZW1vdmU6IHtcblxuICAgICAgICAgIGlubGluZUNTUzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1JlbW92aW5nIGlubGluZSBjc3Mgc3R5bGVzJywgJHN0eWxlKTtcbiAgICAgICAgICAgIGlmKCRzdHlsZSAmJiAkc3R5bGUubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAkc3R5bGUucmVtb3ZlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIC8vIGlvcyBzY3JvbGwgb24gaHRtbCBub3QgZG9jdW1lbnRcbiAgICAgICAgICBpb3M6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJGh0bWwucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmlvcyk7XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIC8vIGNvbnRleHRcbiAgICAgICAgICBwdXNoZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJGNvbnRleHQucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLnB1c2hlZCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBwdXNoYWJsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkY29udGV4dC5yZW1vdmVDbGFzcyhjbGFzc05hbWUucHVzaGFibGUpO1xuICAgICAgICAgIH0sXG5cbiAgICAgICAgICAvLyBzaWRlYmFyXG4gICAgICAgICAgYWN0aXZlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRtb2R1bGUucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBhbmltYXRpbmc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZS5yZW1vdmVDbGFzcyhjbGFzc05hbWUuYW5pbWF0aW5nKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHRyYW5zaXRpb246IGZ1bmN0aW9uKHRyYW5zaXRpb24pIHtcbiAgICAgICAgICAgIHRyYW5zaXRpb24gPSB0cmFuc2l0aW9uIHx8IG1vZHVsZS5nZXQudHJhbnNpdGlvbigpO1xuICAgICAgICAgICAgJG1vZHVsZS5yZW1vdmVDbGFzcyh0cmFuc2l0aW9uKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpcmVjdGlvbjogZnVuY3Rpb24oZGlyZWN0aW9uKSB7XG4gICAgICAgICAgICBkaXJlY3Rpb24gPSBkaXJlY3Rpb24gfHwgbW9kdWxlLmdldC5kaXJlY3Rpb24oKTtcbiAgICAgICAgICAgICRtb2R1bGUucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lW2RpcmVjdGlvbl0pO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgdmlzaWJsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkbW9kdWxlLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS52aXNpYmxlKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIG92ZXJsYXk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZS5yZW1vdmVDbGFzcyhjbGFzc05hbWUub3ZlcmxheSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGdldDoge1xuICAgICAgICAgIGRpcmVjdGlvbjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZigkbW9kdWxlLmhhc0NsYXNzKGNsYXNzTmFtZS50b3ApKSB7XG4gICAgICAgICAgICAgIHJldHVybiBjbGFzc05hbWUudG9wO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZigkbW9kdWxlLmhhc0NsYXNzKGNsYXNzTmFtZS5yaWdodCkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGNsYXNzTmFtZS5yaWdodDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoJG1vZHVsZS5oYXNDbGFzcyhjbGFzc05hbWUuYm90dG9tKSkge1xuICAgICAgICAgICAgICByZXR1cm4gY2xhc3NOYW1lLmJvdHRvbTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBjbGFzc05hbWUubGVmdDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHRyYW5zaXRpb246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGRpcmVjdGlvbiA9IG1vZHVsZS5nZXQuZGlyZWN0aW9uKCksXG4gICAgICAgICAgICAgIHRyYW5zaXRpb25cbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHRyYW5zaXRpb24gPSAoIG1vZHVsZS5pcy5tb2JpbGUoKSApXG4gICAgICAgICAgICAgID8gKHNldHRpbmdzLm1vYmlsZVRyYW5zaXRpb24gPT0gJ2F1dG8nKVxuICAgICAgICAgICAgICAgID8gc2V0dGluZ3MuZGVmYXVsdFRyYW5zaXRpb24ubW9iaWxlW2RpcmVjdGlvbl1cbiAgICAgICAgICAgICAgICA6IHNldHRpbmdzLm1vYmlsZVRyYW5zaXRpb25cbiAgICAgICAgICAgICAgOiAoc2V0dGluZ3MudHJhbnNpdGlvbiA9PSAnYXV0bycpXG4gICAgICAgICAgICAgICAgPyBzZXR0aW5ncy5kZWZhdWx0VHJhbnNpdGlvbi5jb21wdXRlcltkaXJlY3Rpb25dXG4gICAgICAgICAgICAgICAgOiBzZXR0aW5ncy50cmFuc2l0aW9uXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnRGV0ZXJtaW5lZCB0cmFuc2l0aW9uJywgdHJhbnNpdGlvbik7XG4gICAgICAgICAgICByZXR1cm4gdHJhbnNpdGlvbjtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHRyYW5zaXRpb25FdmVudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgZWxlbWVudCAgICAgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdlbGVtZW50JyksXG4gICAgICAgICAgICAgIHRyYW5zaXRpb25zID0ge1xuICAgICAgICAgICAgICAgICd0cmFuc2l0aW9uJyAgICAgICA6J3RyYW5zaXRpb25lbmQnLFxuICAgICAgICAgICAgICAgICdPVHJhbnNpdGlvbicgICAgICA6J29UcmFuc2l0aW9uRW5kJyxcbiAgICAgICAgICAgICAgICAnTW96VHJhbnNpdGlvbicgICAgOid0cmFuc2l0aW9uZW5kJyxcbiAgICAgICAgICAgICAgICAnV2Via2l0VHJhbnNpdGlvbicgOid3ZWJraXRUcmFuc2l0aW9uRW5kJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB0cmFuc2l0aW9uXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBmb3IodHJhbnNpdGlvbiBpbiB0cmFuc2l0aW9ucyl7XG4gICAgICAgICAgICAgIGlmKCBlbGVtZW50LnN0eWxlW3RyYW5zaXRpb25dICE9PSB1bmRlZmluZWQgKXtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJhbnNpdGlvbnNbdHJhbnNpdGlvbl07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgaXM6IHtcblxuICAgICAgICAgIGllOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBpc0lFMTEgPSAoISh3aW5kb3cuQWN0aXZlWE9iamVjdCkgJiYgJ0FjdGl2ZVhPYmplY3QnIGluIHdpbmRvdyksXG4gICAgICAgICAgICAgIGlzSUUgICA9ICgnQWN0aXZlWE9iamVjdCcgaW4gd2luZG93KVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgcmV0dXJuIChpc0lFMTEgfHwgaXNJRSk7XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIGlvczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgdXNlckFnZW50ICAgICAgPSBuYXZpZ2F0b3IudXNlckFnZW50LFxuICAgICAgICAgICAgICBpc0lPUyAgICAgICAgICA9IHVzZXJBZ2VudC5tYXRjaChyZWdFeHAuaW9zKSxcbiAgICAgICAgICAgICAgaXNNb2JpbGVDaHJvbWUgPSB1c2VyQWdlbnQubWF0Y2gocmVnRXhwLm1vYmlsZUNocm9tZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKGlzSU9TICYmICFpc01vYmlsZUNocm9tZSkge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQnJvd3NlciB3YXMgZm91bmQgdG8gYmUgaU9TJywgdXNlckFnZW50KTtcbiAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgbW9iaWxlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICB1c2VyQWdlbnQgICAgPSBuYXZpZ2F0b3IudXNlckFnZW50LFxuICAgICAgICAgICAgICBpc01vYmlsZSAgICAgPSB1c2VyQWdlbnQubWF0Y2gocmVnRXhwLm1vYmlsZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKGlzTW9iaWxlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdCcm93c2VyIHdhcyBmb3VuZCB0byBiZSBtb2JpbGUnLCB1c2VyQWdlbnQpO1xuICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQnJvd3NlciBpcyBub3QgbW9iaWxlLCB1c2luZyByZWd1bGFyIHRyYW5zaXRpb24nLCB1c2VyQWdlbnQpO1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBoaWRkZW46IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICFtb2R1bGUuaXMudmlzaWJsZSgpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgdmlzaWJsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5oYXNDbGFzcyhjbGFzc05hbWUudmlzaWJsZSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICAvLyBhbGlhc1xuICAgICAgICAgIG9wZW46IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZS5pcy52aXNpYmxlKCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBjbG9zZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZS5pcy5oaWRkZW4oKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHZlcnRpY2FsOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkbW9kdWxlLmhhc0NsYXNzKGNsYXNzTmFtZS50b3ApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYW5pbWF0aW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkY29udGV4dC5oYXNDbGFzcyhjbGFzc05hbWUuYW5pbWF0aW5nKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHJ0bDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmNhY2hlLnJ0bCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5jYWNoZS5ydGwgPSAoJG1vZHVsZS5jc3MoJ2RpcmVjdGlvbicpID09ICdydGwnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBtb2R1bGUuY2FjaGUucnRsO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZXR0aW5nOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ2hhbmdpbmcgc2V0dGluZycsIG5hbWUsIHZhbHVlKTtcbiAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgc2V0dGluZ3MsIG5hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGlmKCQuaXNQbGFpbk9iamVjdChzZXR0aW5nc1tuYW1lXSkpIHtcbiAgICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgc2V0dGluZ3NbbmFtZV0sIHZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBzZXR0aW5nc1tuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBzZXR0aW5nc1tuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludGVybmFsOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBtb2R1bGUsIG5hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIG1vZHVsZVtuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGVbbmFtZV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBkZWJ1ZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy5kZWJ1Zykge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZyA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB2ZXJib3NlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50ICYmIHNldHRpbmdzLnZlcmJvc2UgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGVycm9yOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50KSB7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuZXJyb3IsIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBwZXJmb3JtYW5jZToge1xuICAgICAgICAgIGxvZzogZnVuY3Rpb24obWVzc2FnZSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lLFxuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lLFxuICAgICAgICAgICAgICBwcmV2aW91c1RpbWVcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lICAgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lICA9IHRpbWUgfHwgY3VycmVudFRpbWU7XG4gICAgICAgICAgICAgIGV4ZWN1dGlvblRpbWUgPSBjdXJyZW50VGltZSAtIHByZXZpb3VzVGltZTtcbiAgICAgICAgICAgICAgdGltZSAgICAgICAgICA9IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBwZXJmb3JtYW5jZS5wdXNoKHtcbiAgICAgICAgICAgICAgICAnTmFtZScgICAgICAgICAgIDogbWVzc2FnZVswXSxcbiAgICAgICAgICAgICAgICAnQXJndW1lbnRzJyAgICAgIDogW10uc2xpY2UuY2FsbChtZXNzYWdlLCAxKSB8fCAnJyxcbiAgICAgICAgICAgICAgICAnRWxlbWVudCcgICAgICAgIDogZWxlbWVudCxcbiAgICAgICAgICAgICAgICAnRXhlY3V0aW9uIFRpbWUnIDogZXhlY3V0aW9uVGltZVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyID0gc2V0VGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UuZGlzcGxheSwgNTAwKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc3BsYXk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHRpdGxlID0gc2V0dGluZ3MubmFtZSArICc6JyxcbiAgICAgICAgICAgICAgdG90YWxUaW1lID0gMFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgdGltZSA9IGZhbHNlO1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lcik7XG4gICAgICAgICAgICAkLmVhY2gocGVyZm9ybWFuY2UsIGZ1bmN0aW9uKGluZGV4LCBkYXRhKSB7XG4gICAgICAgICAgICAgIHRvdGFsVGltZSArPSBkYXRhWydFeGVjdXRpb24gVGltZSddO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB0aXRsZSArPSAnICcgKyB0b3RhbFRpbWUgKyAnbXMnO1xuICAgICAgICAgICAgaWYobW9kdWxlU2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgdGl0bGUgKz0gJyBcXCcnICsgbW9kdWxlU2VsZWN0b3IgKyAnXFwnJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCAoY29uc29sZS5ncm91cCAhPT0gdW5kZWZpbmVkIHx8IGNvbnNvbGUudGFibGUgIT09IHVuZGVmaW5lZCkgJiYgcGVyZm9ybWFuY2UubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKHRpdGxlKTtcbiAgICAgICAgICAgICAgaWYoY29uc29sZS50YWJsZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUudGFibGUocGVyZm9ybWFuY2UpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGFbJ05hbWUnXSArICc6ICcgKyBkYXRhWydFeGVjdXRpb24gVGltZSddKydtcycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHBlcmZvcm1hbmNlID0gW107XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnZva2U6IGZ1bmN0aW9uKHF1ZXJ5LCBwYXNzZWRBcmd1bWVudHMsIGNvbnRleHQpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIG9iamVjdCA9IGluc3RhbmNlLFxuICAgICAgICAgICAgbWF4RGVwdGgsXG4gICAgICAgICAgICBmb3VuZCxcbiAgICAgICAgICAgIHJlc3BvbnNlXG4gICAgICAgICAgO1xuICAgICAgICAgIHBhc3NlZEFyZ3VtZW50cyA9IHBhc3NlZEFyZ3VtZW50cyB8fCBxdWVyeUFyZ3VtZW50cztcbiAgICAgICAgICBjb250ZXh0ICAgICAgICAgPSBlbGVtZW50ICAgICAgICAgfHwgY29udGV4dDtcbiAgICAgICAgICBpZih0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycgJiYgb2JqZWN0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5ICAgID0gcXVlcnkuc3BsaXQoL1tcXC4gXS8pO1xuICAgICAgICAgICAgbWF4RGVwdGggPSBxdWVyeS5sZW5ndGggLSAxO1xuICAgICAgICAgICAgJC5lYWNoKHF1ZXJ5LCBmdW5jdGlvbihkZXB0aCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgdmFyIGNhbWVsQ2FzZVZhbHVlID0gKGRlcHRoICE9IG1heERlcHRoKVxuICAgICAgICAgICAgICAgID8gdmFsdWUgKyBxdWVyeVtkZXB0aCArIDFdLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcXVlcnlbZGVwdGggKyAxXS5zbGljZSgxKVxuICAgICAgICAgICAgICAgIDogcXVlcnlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbY2FtZWxDYXNlVmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoICQuaXNQbGFpbk9iamVjdCggb2JqZWN0W3ZhbHVlXSApICYmIChkZXB0aCAhPSBtYXhEZXB0aCkgKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBvYmplY3RbdmFsdWVdICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3IubWV0aG9kLCBxdWVyeSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCAkLmlzRnVuY3Rpb24oIGZvdW5kICkgKSB7XG4gICAgICAgICAgICByZXNwb25zZSA9IGZvdW5kLmFwcGx5KGNvbnRleHQsIHBhc3NlZEFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYoZm91bmQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZDtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoJC5pc0FycmF5KHJldHVybmVkVmFsdWUpKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlLnB1c2gocmVzcG9uc2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IFtyZXR1cm5lZFZhbHVlLCByZXNwb25zZV07XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYocmVzcG9uc2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IHJlc3BvbnNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gZm91bmQ7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICA7XG5cbiAgICBpZihtZXRob2RJbnZva2VkKSB7XG4gICAgICBpZihpbnN0YW5jZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICB9XG4gICAgICBtb2R1bGUuaW52b2tlKHF1ZXJ5KTtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICBpZihpbnN0YW5jZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIG1vZHVsZS5pbnZva2UoJ2Rlc3Ryb3knKTtcbiAgICAgIH1cbiAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgfVxuICB9KTtcblxuICByZXR1cm4gKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZClcbiAgICA/IHJldHVybmVkVmFsdWVcbiAgICA6IHRoaXNcbiAgO1xufTtcblxuJC5mbi5zaWRlYmFyLnNldHRpbmdzID0ge1xuXG4gIG5hbWUgICAgICAgICAgICAgIDogJ1NpZGViYXInLFxuICBuYW1lc3BhY2UgICAgICAgICA6ICdzaWRlYmFyJyxcblxuICBzaWxlbnQgICAgICAgICAgICA6IGZhbHNlLFxuICBkZWJ1ZyAgICAgICAgICAgICA6IGZhbHNlLFxuICB2ZXJib3NlICAgICAgICAgICA6IGZhbHNlLFxuICBwZXJmb3JtYW5jZSAgICAgICA6IHRydWUsXG5cbiAgdHJhbnNpdGlvbiAgICAgICAgOiAnYXV0bycsXG4gIG1vYmlsZVRyYW5zaXRpb24gIDogJ2F1dG8nLFxuXG4gIGRlZmF1bHRUcmFuc2l0aW9uIDoge1xuICAgIGNvbXB1dGVyOiB7XG4gICAgICBsZWZ0ICAgOiAndW5jb3ZlcicsXG4gICAgICByaWdodCAgOiAndW5jb3ZlcicsXG4gICAgICB0b3AgICAgOiAnb3ZlcmxheScsXG4gICAgICBib3R0b20gOiAnb3ZlcmxheSdcbiAgICB9LFxuICAgIG1vYmlsZToge1xuICAgICAgbGVmdCAgIDogJ3VuY292ZXInLFxuICAgICAgcmlnaHQgIDogJ3VuY292ZXInLFxuICAgICAgdG9wICAgIDogJ292ZXJsYXknLFxuICAgICAgYm90dG9tIDogJ292ZXJsYXknXG4gICAgfVxuICB9LFxuXG4gIGNvbnRleHQgICAgICAgICAgIDogJ2JvZHknLFxuICBleGNsdXNpdmUgICAgICAgICA6IGZhbHNlLFxuICBjbG9zYWJsZSAgICAgICAgICA6IHRydWUsXG4gIGRpbVBhZ2UgICAgICAgICAgIDogdHJ1ZSxcbiAgc2Nyb2xsTG9jayAgICAgICAgOiBmYWxzZSxcbiAgcmV0dXJuU2Nyb2xsICAgICAgOiBmYWxzZSxcbiAgZGVsYXlTZXR1cCAgICAgICAgOiBmYWxzZSxcblxuICBkdXJhdGlvbiAgICAgICAgICA6IDUwMCxcblxuICBvbkNoYW5nZSAgICAgICAgICA6IGZ1bmN0aW9uKCl7fSxcbiAgb25TaG93ICAgICAgICAgICAgOiBmdW5jdGlvbigpe30sXG4gIG9uSGlkZSAgICAgICAgICAgIDogZnVuY3Rpb24oKXt9LFxuXG4gIG9uSGlkZGVuICAgICAgICAgIDogZnVuY3Rpb24oKXt9LFxuICBvblZpc2libGUgICAgICAgICA6IGZ1bmN0aW9uKCl7fSxcblxuICBjbGFzc05hbWUgICAgICAgICA6IHtcbiAgICBhY3RpdmUgICAgOiAnYWN0aXZlJyxcbiAgICBhbmltYXRpbmcgOiAnYW5pbWF0aW5nJyxcbiAgICBkaW1tZWQgICAgOiAnZGltbWVkJyxcbiAgICBpb3MgICAgICAgOiAnaW9zJyxcbiAgICBwdXNoYWJsZSAgOiAncHVzaGFibGUnLFxuICAgIHB1c2hlZCAgICA6ICdwdXNoZWQnLFxuICAgIHJpZ2h0ICAgICA6ICdyaWdodCcsXG4gICAgdG9wICAgICAgIDogJ3RvcCcsXG4gICAgbGVmdCAgICAgIDogJ2xlZnQnLFxuICAgIGJvdHRvbSAgICA6ICdib3R0b20nLFxuICAgIHZpc2libGUgICA6ICd2aXNpYmxlJ1xuICB9LFxuXG4gIHNlbGVjdG9yOiB7XG4gICAgZml4ZWQgICA6ICcuZml4ZWQnLFxuICAgIG9taXR0ZWQgOiAnc2NyaXB0LCBsaW5rLCBzdHlsZSwgLnVpLm1vZGFsLCAudWkuZGltbWVyLCAudWkubmFnLCAudWkuZml4ZWQnLFxuICAgIHB1c2hlciAgOiAnLnB1c2hlcicsXG4gICAgc2lkZWJhciA6ICcudWkuc2lkZWJhcidcbiAgfSxcblxuICByZWdFeHA6IHtcbiAgICBpb3MgICAgICAgICAgOiAvKGlQYWR8aVBob25lfGlQb2QpL2csXG4gICAgbW9iaWxlQ2hyb21lIDogLyhDcmlPUykvZyxcbiAgICBtb2JpbGUgICAgICAgOiAvTW9iaWxlfGlQKGhvbmV8b2R8YWQpfEFuZHJvaWR8QmxhY2tCZXJyeXxJRU1vYmlsZXxLaW5kbGV8TmV0RnJvbnR8U2lsay1BY2NlbGVyYXRlZHwoaHB3fHdlYilPU3xGZW5uZWN8TWluaW1vfE9wZXJhIE0ob2JpfGluaSl8QmxhemVyfERvbGZpbnxEb2xwaGlufFNreWZpcmV8WnVuZS9nXG4gIH0sXG5cbiAgZXJyb3IgICA6IHtcbiAgICBtZXRob2QgICAgICAgOiAnVGhlIG1ldGhvZCB5b3UgY2FsbGVkIGlzIG5vdCBkZWZpbmVkLicsXG4gICAgcHVzaGVyICAgICAgIDogJ0hhZCB0byBhZGQgcHVzaGVyIGVsZW1lbnQuIEZvciBvcHRpbWFsIHBlcmZvcm1hbmNlIG1ha2Ugc3VyZSBib2R5IGNvbnRlbnQgaXMgaW5zaWRlIGEgcHVzaGVyIGVsZW1lbnQnLFxuICAgIG1vdmVkU2lkZWJhciA6ICdIYWQgdG8gbW92ZSBzaWRlYmFyLiBGb3Igb3B0aW1hbCBwZXJmb3JtYW5jZSBtYWtlIHN1cmUgc2lkZWJhciBhbmQgcHVzaGVyIGFyZSBkaXJlY3QgY2hpbGRyZW4gb2YgeW91ciBib2R5IHRhZycsXG4gICAgb3ZlcmxheSAgICAgIDogJ1RoZSBvdmVybGF5IHNldHRpbmcgaXMgbm8gbG9uZ2VyIHN1cHBvcnRlZCwgdXNlIGFuaW1hdGlvbjogb3ZlcmxheScsXG4gICAgbm90Rm91bmQgICAgIDogJ1RoZXJlIHdlcmUgbm8gZWxlbWVudHMgdGhhdCBtYXRjaGVkIHRoZSBzcGVjaWZpZWQgc2VsZWN0b3InXG4gIH1cblxufTtcblxuXG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7XG4iXX0=