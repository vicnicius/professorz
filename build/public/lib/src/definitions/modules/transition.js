/*!
 * # Semantic UI - Transition
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.transition = function () {
    var $allModules = $(this),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        moduleArguments = arguments,
        query = moduleArguments[0],
        queryArguments = [].slice.call(arguments, 1),
        methodInvoked = typeof query === 'string',
        requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
      setTimeout(callback, 0);
    },
        returnedValue;
    $allModules.each(function (index) {
      var $module = $(this),
          element = this,


      // set at run time
      settings,
          instance,
          error,
          className,
          metadata,
          animationEnd,
          animationName,
          namespace,
          moduleNamespace,
          eventNamespace,
          module;

      module = {

        initialize: function () {

          // get full settings
          settings = module.get.settings.apply(element, moduleArguments);

          // shorthand
          className = settings.className;
          error = settings.error;
          metadata = settings.metadata;

          // define namespace
          eventNamespace = '.' + settings.namespace;
          moduleNamespace = 'module-' + settings.namespace;
          instance = $module.data(moduleNamespace) || module;

          // get vendor specific events
          animationEnd = module.get.animationEndEvent();

          if (methodInvoked) {
            methodInvoked = module.invoke(query);
          }

          // method not invoked, lets run an animation
          if (methodInvoked === false) {
            module.verbose('Converted arguments into settings object', settings);
            if (settings.interval) {
              module.delay(settings.animate);
            } else {
              module.animate();
            }
            module.instantiate();
          }
        },

        instantiate: function () {
          module.verbose('Storing instance of module', module);
          instance = module;
          $module.data(moduleNamespace, instance);
        },

        destroy: function () {
          module.verbose('Destroying previous module for', element);
          $module.removeData(moduleNamespace);
        },

        refresh: function () {
          module.verbose('Refreshing display type on next animation');
          delete module.displayType;
        },

        forceRepaint: function () {
          module.verbose('Forcing element repaint');
          var $parentElement = $module.parent(),
              $nextElement = $module.next();
          if ($nextElement.length === 0) {
            $module.detach().appendTo($parentElement);
          } else {
            $module.detach().insertBefore($nextElement);
          }
        },

        repaint: function () {
          module.verbose('Repainting element');
          var fakeAssignment = element.offsetWidth;
        },

        delay: function (interval) {
          var direction = module.get.animationDirection(),
              shouldReverse,
              delay;
          if (!direction) {
            direction = module.can.transition() ? module.get.direction() : 'static';
          }
          interval = interval !== undefined ? interval : settings.interval;
          shouldReverse = settings.reverse == 'auto' && direction == className.outward;
          delay = shouldReverse || settings.reverse == true ? ($allModules.length - index) * settings.interval : index * settings.interval;
          module.debug('Delaying animation by', delay);
          setTimeout(module.animate, delay);
        },

        animate: function (overrideSettings) {
          settings = overrideSettings || settings;
          if (!module.is.supported()) {
            module.error(error.support);
            return false;
          }
          module.debug('Preparing animation', settings.animation);
          if (module.is.animating()) {
            if (settings.queue) {
              if (!settings.allowRepeats && module.has.direction() && module.is.occurring() && module.queuing !== true) {
                module.debug('Animation is currently occurring, preventing queueing same animation', settings.animation);
              } else {
                module.queue(settings.animation);
              }
              return false;
            } else if (!settings.allowRepeats && module.is.occurring()) {
              module.debug('Animation is already occurring, will not execute repeated animation', settings.animation);
              return false;
            } else {
              module.debug('New animation started, completing previous early', settings.animation);
              instance.complete();
            }
          }
          if (module.can.animate()) {
            module.set.animating(settings.animation);
          } else {
            module.error(error.noAnimation, settings.animation, element);
          }
        },

        reset: function () {
          module.debug('Resetting animation to beginning conditions');
          module.remove.animationCallbacks();
          module.restore.conditions();
          module.remove.animating();
        },

        queue: function (animation) {
          module.debug('Queueing animation of', animation);
          module.queuing = true;
          $module.one(animationEnd + '.queue' + eventNamespace, function () {
            module.queuing = false;
            module.repaint();
            module.animate.apply(this, settings);
          });
        },

        complete: function (event) {
          module.debug('Animation complete', settings.animation);
          module.remove.completeCallback();
          module.remove.failSafe();
          if (!module.is.looping()) {
            if (module.is.outward()) {
              module.verbose('Animation is outward, hiding element');
              module.restore.conditions();
              module.hide();
            } else if (module.is.inward()) {
              module.verbose('Animation is outward, showing element');
              module.restore.conditions();
              module.show();
            } else {
              module.verbose('Static animation completed');
              module.restore.conditions();
              settings.onComplete.call(element);
            }
          }
        },

        force: {
          visible: function () {
            var style = $module.attr('style'),
                userStyle = module.get.userStyle(),
                displayType = module.get.displayType(),
                overrideStyle = userStyle + 'display: ' + displayType + ' !important;',
                currentDisplay = $module.css('display'),
                emptyStyle = style === undefined || style === '';
            if (currentDisplay !== displayType) {
              module.verbose('Overriding default display to show element', displayType);
              $module.attr('style', overrideStyle);
            } else if (emptyStyle) {
              $module.removeAttr('style');
            }
          },
          hidden: function () {
            var style = $module.attr('style'),
                currentDisplay = $module.css('display'),
                emptyStyle = style === undefined || style === '';
            if (currentDisplay !== 'none' && !module.is.hidden()) {
              module.verbose('Overriding default display to hide element');
              $module.css('display', 'none');
            } else if (emptyStyle) {
              $module.removeAttr('style');
            }
          }
        },

        has: {
          direction: function (animation) {
            var hasDirection = false;
            animation = animation || settings.animation;
            if (typeof animation === 'string') {
              animation = animation.split(' ');
              $.each(animation, function (index, word) {
                if (word === className.inward || word === className.outward) {
                  hasDirection = true;
                }
              });
            }
            return hasDirection;
          },
          inlineDisplay: function () {
            var style = $module.attr('style') || '';
            return $.isArray(style.match(/display.*?;/, ''));
          }
        },

        set: {
          animating: function (animation) {
            var animationClass, direction;
            // remove previous callbacks
            module.remove.completeCallback();

            // determine exact animation
            animation = animation || settings.animation;
            animationClass = module.get.animationClass(animation);

            // save animation class in cache to restore class names
            module.save.animation(animationClass);

            // override display if necessary so animation appears visibly
            module.force.visible();

            module.remove.hidden();
            module.remove.direction();

            module.start.animation(animationClass);
          },
          duration: function (animationName, duration) {
            duration = duration || settings.duration;
            duration = typeof duration == 'number' ? duration + 'ms' : duration;
            if (duration || duration === 0) {
              module.verbose('Setting animation duration', duration);
              $module.css({
                'animation-duration': duration
              });
            }
          },
          direction: function (direction) {
            direction = direction || module.get.direction();
            if (direction == className.inward) {
              module.set.inward();
            } else {
              module.set.outward();
            }
          },
          looping: function () {
            module.debug('Transition set to loop');
            $module.addClass(className.looping);
          },
          hidden: function () {
            $module.addClass(className.transition).addClass(className.hidden);
          },
          inward: function () {
            module.debug('Setting direction to inward');
            $module.removeClass(className.outward).addClass(className.inward);
          },
          outward: function () {
            module.debug('Setting direction to outward');
            $module.removeClass(className.inward).addClass(className.outward);
          },
          visible: function () {
            $module.addClass(className.transition).addClass(className.visible);
          }
        },

        start: {
          animation: function (animationClass) {
            animationClass = animationClass || module.get.animationClass();
            module.debug('Starting tween', animationClass);
            $module.addClass(animationClass).one(animationEnd + '.complete' + eventNamespace, module.complete);
            if (settings.useFailSafe) {
              module.add.failSafe();
            }
            module.set.duration(settings.duration);
            settings.onStart.call(element);
          }
        },

        save: {
          animation: function (animation) {
            if (!module.cache) {
              module.cache = {};
            }
            module.cache.animation = animation;
          },
          displayType: function (displayType) {
            if (displayType !== 'none') {
              $module.data(metadata.displayType, displayType);
            }
          },
          transitionExists: function (animation, exists) {
            $.fn.transition.exists[animation] = exists;
            module.verbose('Saving existence of transition', animation, exists);
          }
        },

        restore: {
          conditions: function () {
            var animation = module.get.currentAnimation();
            if (animation) {
              $module.removeClass(animation);
              module.verbose('Removing animation class', module.cache);
            }
            module.remove.duration();
          }
        },

        add: {
          failSafe: function () {
            var duration = module.get.duration();
            module.timer = setTimeout(function () {
              $module.triggerHandler(animationEnd);
            }, duration + settings.failSafeDelay);
            module.verbose('Adding fail safe timer', module.timer);
          }
        },

        remove: {
          animating: function () {
            $module.removeClass(className.animating);
          },
          animationCallbacks: function () {
            module.remove.queueCallback();
            module.remove.completeCallback();
          },
          queueCallback: function () {
            $module.off('.queue' + eventNamespace);
          },
          completeCallback: function () {
            $module.off('.complete' + eventNamespace);
          },
          display: function () {
            $module.css('display', '');
          },
          direction: function () {
            $module.removeClass(className.inward).removeClass(className.outward);
          },
          duration: function () {
            $module.css('animation-duration', '');
          },
          failSafe: function () {
            module.verbose('Removing fail safe timer', module.timer);
            if (module.timer) {
              clearTimeout(module.timer);
            }
          },
          hidden: function () {
            $module.removeClass(className.hidden);
          },
          visible: function () {
            $module.removeClass(className.visible);
          },
          looping: function () {
            module.debug('Transitions are no longer looping');
            if (module.is.looping()) {
              module.reset();
              $module.removeClass(className.looping);
            }
          },
          transition: function () {
            $module.removeClass(className.visible).removeClass(className.hidden);
          }
        },
        get: {
          settings: function (animation, duration, onComplete) {
            // single settings object
            if (typeof animation == 'object') {
              return $.extend(true, {}, $.fn.transition.settings, animation);
            }
            // all arguments provided
            else if (typeof onComplete == 'function') {
                return $.extend({}, $.fn.transition.settings, {
                  animation: animation,
                  onComplete: onComplete,
                  duration: duration
                });
              }
              // only duration provided
              else if (typeof duration == 'string' || typeof duration == 'number') {
                  return $.extend({}, $.fn.transition.settings, {
                    animation: animation,
                    duration: duration
                  });
                }
                // duration is actually settings object
                else if (typeof duration == 'object') {
                    return $.extend({}, $.fn.transition.settings, duration, {
                      animation: animation
                    });
                  }
                  // duration is actually callback
                  else if (typeof duration == 'function') {
                      return $.extend({}, $.fn.transition.settings, {
                        animation: animation,
                        onComplete: duration
                      });
                    }
                    // only animation provided
                    else {
                        return $.extend({}, $.fn.transition.settings, {
                          animation: animation
                        });
                      }
            return $.fn.transition.settings;
          },
          animationClass: function (animation) {
            var animationClass = animation || settings.animation,
                directionClass = module.can.transition() && !module.has.direction() ? module.get.direction() + ' ' : '';
            return className.animating + ' ' + className.transition + ' ' + directionClass + animationClass;
          },
          currentAnimation: function () {
            return module.cache && module.cache.animation !== undefined ? module.cache.animation : false;
          },
          currentDirection: function () {
            return module.is.inward() ? className.inward : className.outward;
          },
          direction: function () {
            return module.is.hidden() || !module.is.visible() ? className.inward : className.outward;
          },
          animationDirection: function (animation) {
            var direction;
            animation = animation || settings.animation;
            if (typeof animation === 'string') {
              animation = animation.split(' ');
              // search animation name for out/in class
              $.each(animation, function (index, word) {
                if (word === className.inward) {
                  direction = className.inward;
                } else if (word === className.outward) {
                  direction = className.outward;
                }
              });
            }
            // return found direction
            if (direction) {
              return direction;
            }
            return false;
          },
          duration: function (duration) {
            duration = duration || settings.duration;
            if (duration === false) {
              duration = $module.css('animation-duration') || 0;
            }
            return typeof duration === 'string' ? duration.indexOf('ms') > -1 ? parseFloat(duration) : parseFloat(duration) * 1000 : duration;
          },
          displayType: function () {
            if (settings.displayType) {
              return settings.displayType;
            }
            if ($module.data(metadata.displayType) === undefined) {
              // create fake element to determine display state
              module.can.transition(true);
            }
            return $module.data(metadata.displayType);
          },
          userStyle: function (style) {
            style = style || $module.attr('style') || '';
            return style.replace(/display.*?;/, '');
          },
          transitionExists: function (animation) {
            return $.fn.transition.exists[animation];
          },
          animationStartEvent: function () {
            var element = document.createElement('div'),
                animations = {
              'animation': 'animationstart',
              'OAnimation': 'oAnimationStart',
              'MozAnimation': 'mozAnimationStart',
              'WebkitAnimation': 'webkitAnimationStart'
            },
                animation;
            for (animation in animations) {
              if (element.style[animation] !== undefined) {
                return animations[animation];
              }
            }
            return false;
          },
          animationEndEvent: function () {
            var element = document.createElement('div'),
                animations = {
              'animation': 'animationend',
              'OAnimation': 'oAnimationEnd',
              'MozAnimation': 'mozAnimationEnd',
              'WebkitAnimation': 'webkitAnimationEnd'
            },
                animation;
            for (animation in animations) {
              if (element.style[animation] !== undefined) {
                return animations[animation];
              }
            }
            return false;
          }

        },

        can: {
          transition: function (forced) {
            var animation = settings.animation,
                transitionExists = module.get.transitionExists(animation),
                elementClass,
                tagName,
                $clone,
                currentAnimation,
                inAnimation,
                directionExists,
                displayType;
            if (transitionExists === undefined || forced) {
              module.verbose('Determining whether animation exists');
              elementClass = $module.attr('class');
              tagName = $module.prop('tagName');

              $clone = $('<' + tagName + ' />').addClass(elementClass).insertAfter($module);
              currentAnimation = $clone.addClass(animation).removeClass(className.inward).removeClass(className.outward).addClass(className.animating).addClass(className.transition).css('animationName');
              inAnimation = $clone.addClass(className.inward).css('animationName');
              displayType = $clone.attr('class', elementClass).removeAttr('style').removeClass(className.hidden).removeClass(className.visible).show().css('display');
              module.verbose('Determining final display state', displayType);
              module.save.displayType(displayType);

              $clone.remove();
              if (currentAnimation != inAnimation) {
                module.debug('Direction exists for animation', animation);
                directionExists = true;
              } else if (currentAnimation == 'none' || !currentAnimation) {
                module.debug('No animation defined in css', animation);
                return;
              } else {
                module.debug('Static animation found', animation, displayType);
                directionExists = false;
              }
              module.save.transitionExists(animation, directionExists);
            }
            return transitionExists !== undefined ? transitionExists : directionExists;
          },
          animate: function () {
            // can transition does not return a value if animation does not exist
            return module.can.transition() !== undefined;
          }
        },

        is: {
          animating: function () {
            return $module.hasClass(className.animating);
          },
          inward: function () {
            return $module.hasClass(className.inward);
          },
          outward: function () {
            return $module.hasClass(className.outward);
          },
          looping: function () {
            return $module.hasClass(className.looping);
          },
          occurring: function (animation) {
            animation = animation || settings.animation;
            animation = '.' + animation.replace(' ', '.');
            return $module.filter(animation).length > 0;
          },
          visible: function () {
            return $module.is(':visible');
          },
          hidden: function () {
            return $module.css('visibility') === 'hidden';
          },
          supported: function () {
            return animationEnd !== false;
          }
        },

        hide: function () {
          module.verbose('Hiding element');
          if (module.is.animating()) {
            module.reset();
          }
          element.blur(); // IE will trigger focus change if element is not blurred before hiding
          module.remove.display();
          module.remove.visible();
          module.set.hidden();
          module.force.hidden();
          settings.onHide.call(element);
          settings.onComplete.call(element);
          // module.repaint();
        },

        show: function (display) {
          module.verbose('Showing element', display);
          module.remove.hidden();
          module.set.visible();
          module.force.visible();
          settings.onShow.call(element);
          settings.onComplete.call(element);
          // module.repaint();
        },

        toggle: function () {
          if (module.is.visible()) {
            module.hide();
          } else {
            module.show();
          }
        },

        stop: function () {
          module.debug('Stopping current animation');
          $module.triggerHandler(animationEnd);
        },

        stopAll: function () {
          module.debug('Stopping all animation');
          module.remove.queueCallback();
          $module.triggerHandler(animationEnd);
        },

        clear: {
          queue: function () {
            module.debug('Clearing animation queue');
            module.remove.queueCallback();
          }
        },

        enable: function () {
          module.verbose('Starting animation');
          $module.removeClass(className.disabled);
        },

        disable: function () {
          module.debug('Stopping animation');
          $module.addClass(className.disabled);
        },

        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ($allModules.length > 1) {
              title += ' ' + '(' + $allModules.length + ')';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        // modified for transition to return invoke success
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }

          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found !== undefined ? found : false;
        }
      };
      module.initialize();
    });
    return returnedValue !== undefined ? returnedValue : this;
  };

  // Records if CSS transition is available
  $.fn.transition.exists = {};

  $.fn.transition.settings = {

    // module info
    name: 'Transition',

    // hide all output from this component regardless of other settings
    silent: false,

    // debug content outputted to console
    debug: false,

    // verbose debug output
    verbose: false,

    // performance data output
    performance: true,

    // event namespace
    namespace: 'transition',

    // delay between animations in group
    interval: 0,

    // whether group animations should be reversed
    reverse: 'auto',

    // animation callback event
    onStart: function () {},
    onComplete: function () {},
    onShow: function () {},
    onHide: function () {},

    // whether timeout should be used to ensure callback fires in cases animationend does not
    useFailSafe: true,

    // delay in ms for fail safe
    failSafeDelay: 100,

    // whether EXACT animation can occur twice in a row
    allowRepeats: false,

    // Override final display type on visible
    displayType: false,

    // animation duration
    animation: 'fade',
    duration: false,

    // new animations will occur after previous ones
    queue: true,

    metadata: {
      displayType: 'display'
    },

    className: {
      animating: 'animating',
      disabled: 'disabled',
      hidden: 'hidden',
      inward: 'in',
      loading: 'loading',
      looping: 'looping',
      outward: 'out',
      transition: 'transition',
      visible: 'visible'
    },

    // possible errors
    error: {
      noAnimation: 'Element is no longer attached to DOM. Unable to animate.  Use silent setting to surpress this warning in production.',
      repeated: 'That animation is already occurring, cancelling repeated animation',
      method: 'The method you called is not defined',
      support: 'This browser does not support CSS animations'
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL3RyYW5zaXRpb24uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQVVBLENBQUMsQ0FBQyxVQUFVLENBQVYsRUFBYSxNQUFiLEVBQXFCLFFBQXJCLEVBQStCLFNBQS9CLEVBQTBDOztBQUU1Qzs7QUFFQSxXQUFVLE9BQU8sTUFBUCxJQUFpQixXQUFqQixJQUFnQyxPQUFPLElBQVAsSUFBZSxJQUFoRCxHQUNMLE1BREssR0FFSixPQUFPLElBQVAsSUFBZSxXQUFmLElBQThCLEtBQUssSUFBTCxJQUFhLElBQTVDLEdBQ0UsSUFERixHQUVFLFNBQVMsYUFBVCxHQUpOOztBQU9BLElBQUUsRUFBRixDQUFLLFVBQUwsR0FBa0IsWUFBVztBQUMzQixRQUNFLGNBQWtCLEVBQUUsSUFBRixDQURwQjtBQUFBLFFBRUUsaUJBQWtCLFlBQVksUUFBWixJQUF3QixFQUY1QztBQUFBLFFBSUUsT0FBa0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUpwQjtBQUFBLFFBS0UsY0FBa0IsRUFMcEI7QUFBQSxRQU9FLGtCQUFrQixTQVBwQjtBQUFBLFFBUUUsUUFBa0IsZ0JBQWdCLENBQWhCLENBUnBCO0FBQUEsUUFTRSxpQkFBa0IsR0FBRyxLQUFILENBQVMsSUFBVCxDQUFjLFNBQWQsRUFBeUIsQ0FBekIsQ0FUcEI7QUFBQSxRQVVFLGdCQUFtQixPQUFPLEtBQVAsS0FBaUIsUUFWdEM7QUFBQSxRQVlFLHdCQUF3QixPQUFPLHFCQUFQLElBQ25CLE9BQU8sd0JBRFksSUFFbkIsT0FBTywyQkFGWSxJQUduQixPQUFPLHVCQUhZLElBSW5CLFVBQVMsUUFBVCxFQUFtQjtBQUFFLGlCQUFXLFFBQVgsRUFBcUIsQ0FBckI7QUFBMEIsS0FoQnREO0FBQUEsUUFrQkUsYUFsQkY7QUFvQkEsZ0JBQ0csSUFESCxDQUNRLFVBQVMsS0FBVCxFQUFnQjtBQUNwQixVQUNFLFVBQVcsRUFBRSxJQUFGLENBRGI7QUFBQSxVQUVFLFVBQVcsSUFGYjtBQUFBOzs7QUFLRSxjQUxGO0FBQUEsVUFNRSxRQU5GO0FBQUEsVUFRRSxLQVJGO0FBQUEsVUFTRSxTQVRGO0FBQUEsVUFVRSxRQVZGO0FBQUEsVUFXRSxZQVhGO0FBQUEsVUFZRSxhQVpGO0FBQUEsVUFjRSxTQWRGO0FBQUEsVUFlRSxlQWZGO0FBQUEsVUFnQkUsY0FoQkY7QUFBQSxVQWlCRSxNQWpCRjs7QUFvQkEsZUFBUzs7QUFFUCxvQkFBWSxZQUFXOzs7QUFHckIscUJBQWtCLE9BQU8sR0FBUCxDQUFXLFFBQVgsQ0FBb0IsS0FBcEIsQ0FBMEIsT0FBMUIsRUFBbUMsZUFBbkMsQ0FBbEI7OztBQUdBLHNCQUFrQixTQUFTLFNBQTNCO0FBQ0Esa0JBQWtCLFNBQVMsS0FBM0I7QUFDQSxxQkFBa0IsU0FBUyxRQUEzQjs7O0FBR0EsMkJBQWtCLE1BQU0sU0FBUyxTQUFqQztBQUNBLDRCQUFrQixZQUFZLFNBQVMsU0FBdkM7QUFDQSxxQkFBa0IsUUFBUSxJQUFSLENBQWEsZUFBYixLQUFpQyxNQUFuRDs7O0FBR0EseUJBQWtCLE9BQU8sR0FBUCxDQUFXLGlCQUFYLEVBQWxCOztBQUVBLGNBQUcsYUFBSCxFQUFrQjtBQUNoQiw0QkFBZ0IsT0FBTyxNQUFQLENBQWMsS0FBZCxDQUFoQjtBQUNEOzs7QUFHRCxjQUFHLGtCQUFrQixLQUFyQixFQUE0QjtBQUMxQixtQkFBTyxPQUFQLENBQWUsMENBQWYsRUFBMkQsUUFBM0Q7QUFDQSxnQkFBRyxTQUFTLFFBQVosRUFBc0I7QUFDcEIscUJBQU8sS0FBUCxDQUFhLFNBQVMsT0FBdEI7QUFDRCxhQUZELE1BR007QUFDSixxQkFBTyxPQUFQO0FBQ0Q7QUFDRCxtQkFBTyxXQUFQO0FBQ0Q7QUFDRixTQW5DTTs7QUFxQ1AscUJBQWEsWUFBVztBQUN0QixpQkFBTyxPQUFQLENBQWUsNEJBQWYsRUFBNkMsTUFBN0M7QUFDQSxxQkFBVyxNQUFYO0FBQ0Esa0JBQ0csSUFESCxDQUNRLGVBRFIsRUFDeUIsUUFEekI7QUFHRCxTQTNDTTs7QUE2Q1AsaUJBQVMsWUFBVztBQUNsQixpQkFBTyxPQUFQLENBQWUsZ0NBQWYsRUFBaUQsT0FBakQ7QUFDQSxrQkFDRyxVQURILENBQ2MsZUFEZDtBQUdELFNBbERNOztBQW9EUCxpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLE9BQVAsQ0FBZSwyQ0FBZjtBQUNBLGlCQUFPLE9BQU8sV0FBZDtBQUNELFNBdkRNOztBQXlEUCxzQkFBYyxZQUFXO0FBQ3ZCLGlCQUFPLE9BQVAsQ0FBZSx5QkFBZjtBQUNBLGNBQ0UsaUJBQWlCLFFBQVEsTUFBUixFQURuQjtBQUFBLGNBRUUsZUFBZSxRQUFRLElBQVIsRUFGakI7QUFJQSxjQUFHLGFBQWEsTUFBYixLQUF3QixDQUEzQixFQUE4QjtBQUM1QixvQkFBUSxNQUFSLEdBQWlCLFFBQWpCLENBQTBCLGNBQTFCO0FBQ0QsV0FGRCxNQUdLO0FBQ0gsb0JBQVEsTUFBUixHQUFpQixZQUFqQixDQUE4QixZQUE5QjtBQUNEO0FBQ0YsU0FyRU07O0FBdUVQLGlCQUFTLFlBQVc7QUFDbEIsaUJBQU8sT0FBUCxDQUFlLG9CQUFmO0FBQ0EsY0FDRSxpQkFBaUIsUUFBUSxXQUQzQjtBQUdELFNBNUVNOztBQThFUCxlQUFPLFVBQVMsUUFBVCxFQUFtQjtBQUN4QixjQUNFLFlBQVksT0FBTyxHQUFQLENBQVcsa0JBQVgsRUFEZDtBQUFBLGNBRUUsYUFGRjtBQUFBLGNBR0UsS0FIRjtBQUtBLGNBQUcsQ0FBQyxTQUFKLEVBQWU7QUFDYix3QkFBWSxPQUFPLEdBQVAsQ0FBVyxVQUFYLEtBQ1IsT0FBTyxHQUFQLENBQVcsU0FBWCxFQURRLEdBRVIsUUFGSjtBQUlEO0FBQ0QscUJBQVksYUFBYSxTQUFkLEdBQ1AsUUFETyxHQUVQLFNBQVMsUUFGYjtBQUlBLDBCQUFpQixTQUFTLE9BQVQsSUFBb0IsTUFBcEIsSUFBOEIsYUFBYSxVQUFVLE9BQXRFO0FBQ0Esa0JBQVMsaUJBQWlCLFNBQVMsT0FBVCxJQUFvQixJQUF0QyxHQUNKLENBQUMsWUFBWSxNQUFaLEdBQXFCLEtBQXRCLElBQStCLFNBQVMsUUFEcEMsR0FFSixRQUFRLFNBQVMsUUFGckI7QUFJQSxpQkFBTyxLQUFQLENBQWEsdUJBQWIsRUFBc0MsS0FBdEM7QUFDQSxxQkFBVyxPQUFPLE9BQWxCLEVBQTJCLEtBQTNCO0FBQ0QsU0FyR007O0FBdUdQLGlCQUFTLFVBQVMsZ0JBQVQsRUFBMkI7QUFDbEMscUJBQVcsb0JBQW9CLFFBQS9CO0FBQ0EsY0FBRyxDQUFDLE9BQU8sRUFBUCxDQUFVLFNBQVYsRUFBSixFQUEyQjtBQUN6QixtQkFBTyxLQUFQLENBQWEsTUFBTSxPQUFuQjtBQUNBLG1CQUFPLEtBQVA7QUFDRDtBQUNELGlCQUFPLEtBQVAsQ0FBYSxxQkFBYixFQUFvQyxTQUFTLFNBQTdDO0FBQ0EsY0FBRyxPQUFPLEVBQVAsQ0FBVSxTQUFWLEVBQUgsRUFBMEI7QUFDeEIsZ0JBQUcsU0FBUyxLQUFaLEVBQW1CO0FBQ2pCLGtCQUFHLENBQUMsU0FBUyxZQUFWLElBQTBCLE9BQU8sR0FBUCxDQUFXLFNBQVgsRUFBMUIsSUFBb0QsT0FBTyxFQUFQLENBQVUsU0FBVixFQUFwRCxJQUE2RSxPQUFPLE9BQVAsS0FBbUIsSUFBbkcsRUFBeUc7QUFDdkcsdUJBQU8sS0FBUCxDQUFhLHNFQUFiLEVBQXFGLFNBQVMsU0FBOUY7QUFDRCxlQUZELE1BR0s7QUFDSCx1QkFBTyxLQUFQLENBQWEsU0FBUyxTQUF0QjtBQUNEO0FBQ0QscUJBQU8sS0FBUDtBQUNELGFBUkQsTUFTSyxJQUFHLENBQUMsU0FBUyxZQUFWLElBQTBCLE9BQU8sRUFBUCxDQUFVLFNBQVYsRUFBN0IsRUFBb0Q7QUFDdkQscUJBQU8sS0FBUCxDQUFhLHFFQUFiLEVBQW9GLFNBQVMsU0FBN0Y7QUFDQSxxQkFBTyxLQUFQO0FBQ0QsYUFISSxNQUlBO0FBQ0gscUJBQU8sS0FBUCxDQUFhLGtEQUFiLEVBQWlFLFNBQVMsU0FBMUU7QUFDQSx1QkFBUyxRQUFUO0FBQ0Q7QUFDRjtBQUNELGNBQUksT0FBTyxHQUFQLENBQVcsT0FBWCxFQUFKLEVBQTJCO0FBQ3pCLG1CQUFPLEdBQVAsQ0FBVyxTQUFYLENBQXFCLFNBQVMsU0FBOUI7QUFDRCxXQUZELE1BR0s7QUFDSCxtQkFBTyxLQUFQLENBQWEsTUFBTSxXQUFuQixFQUFnQyxTQUFTLFNBQXpDLEVBQW9ELE9BQXBEO0FBQ0Q7QUFDRixTQXZJTTs7QUF5SVAsZUFBTyxZQUFXO0FBQ2hCLGlCQUFPLEtBQVAsQ0FBYSw2Q0FBYjtBQUNBLGlCQUFPLE1BQVAsQ0FBYyxrQkFBZDtBQUNBLGlCQUFPLE9BQVAsQ0FBZSxVQUFmO0FBQ0EsaUJBQU8sTUFBUCxDQUFjLFNBQWQ7QUFDRCxTQTlJTTs7QUFnSlAsZUFBTyxVQUFTLFNBQVQsRUFBb0I7QUFDekIsaUJBQU8sS0FBUCxDQUFhLHVCQUFiLEVBQXNDLFNBQXRDO0FBQ0EsaUJBQU8sT0FBUCxHQUFpQixJQUFqQjtBQUNBLGtCQUNHLEdBREgsQ0FDTyxlQUFlLFFBQWYsR0FBMEIsY0FEakMsRUFDaUQsWUFBVztBQUN4RCxtQkFBTyxPQUFQLEdBQWlCLEtBQWpCO0FBQ0EsbUJBQU8sT0FBUDtBQUNBLG1CQUFPLE9BQVAsQ0FBZSxLQUFmLENBQXFCLElBQXJCLEVBQTJCLFFBQTNCO0FBQ0QsV0FMSDtBQU9ELFNBMUpNOztBQTRKUCxrQkFBVSxVQUFVLEtBQVYsRUFBaUI7QUFDekIsaUJBQU8sS0FBUCxDQUFhLG9CQUFiLEVBQW1DLFNBQVMsU0FBNUM7QUFDQSxpQkFBTyxNQUFQLENBQWMsZ0JBQWQ7QUFDQSxpQkFBTyxNQUFQLENBQWMsUUFBZDtBQUNBLGNBQUcsQ0FBQyxPQUFPLEVBQVAsQ0FBVSxPQUFWLEVBQUosRUFBeUI7QUFDdkIsZ0JBQUksT0FBTyxFQUFQLENBQVUsT0FBVixFQUFKLEVBQTBCO0FBQ3hCLHFCQUFPLE9BQVAsQ0FBZSxzQ0FBZjtBQUNBLHFCQUFPLE9BQVAsQ0FBZSxVQUFmO0FBQ0EscUJBQU8sSUFBUDtBQUNELGFBSkQsTUFLSyxJQUFJLE9BQU8sRUFBUCxDQUFVLE1BQVYsRUFBSixFQUF5QjtBQUM1QixxQkFBTyxPQUFQLENBQWUsdUNBQWY7QUFDQSxxQkFBTyxPQUFQLENBQWUsVUFBZjtBQUNBLHFCQUFPLElBQVA7QUFDRCxhQUpJLE1BS0E7QUFDSCxxQkFBTyxPQUFQLENBQWUsNEJBQWY7QUFDQSxxQkFBTyxPQUFQLENBQWUsVUFBZjtBQUNBLHVCQUFTLFVBQVQsQ0FBb0IsSUFBcEIsQ0FBeUIsT0FBekI7QUFDRDtBQUNGO0FBQ0YsU0FqTE07O0FBbUxQLGVBQU87QUFDTCxtQkFBUyxZQUFXO0FBQ2xCLGdCQUNFLFFBQWlCLFFBQVEsSUFBUixDQUFhLE9BQWIsQ0FEbkI7QUFBQSxnQkFFRSxZQUFpQixPQUFPLEdBQVAsQ0FBVyxTQUFYLEVBRm5CO0FBQUEsZ0JBR0UsY0FBaUIsT0FBTyxHQUFQLENBQVcsV0FBWCxFQUhuQjtBQUFBLGdCQUlFLGdCQUFpQixZQUFZLFdBQVosR0FBMEIsV0FBMUIsR0FBd0MsY0FKM0Q7QUFBQSxnQkFLRSxpQkFBaUIsUUFBUSxHQUFSLENBQVksU0FBWixDQUxuQjtBQUFBLGdCQU1FLGFBQWtCLFVBQVUsU0FBVixJQUF1QixVQUFVLEVBTnJEO0FBUUEsZ0JBQUcsbUJBQW1CLFdBQXRCLEVBQW1DO0FBQ2pDLHFCQUFPLE9BQVAsQ0FBZSw0Q0FBZixFQUE2RCxXQUE3RDtBQUNBLHNCQUNHLElBREgsQ0FDUSxPQURSLEVBQ2lCLGFBRGpCO0FBR0QsYUFMRCxNQU1LLElBQUcsVUFBSCxFQUFlO0FBQ2xCLHNCQUFRLFVBQVIsQ0FBbUIsT0FBbkI7QUFDRDtBQUNGLFdBbkJJO0FBb0JMLGtCQUFRLFlBQVc7QUFDakIsZ0JBQ0UsUUFBaUIsUUFBUSxJQUFSLENBQWEsT0FBYixDQURuQjtBQUFBLGdCQUVFLGlCQUFpQixRQUFRLEdBQVIsQ0FBWSxTQUFaLENBRm5CO0FBQUEsZ0JBR0UsYUFBa0IsVUFBVSxTQUFWLElBQXVCLFVBQVUsRUFIckQ7QUFLQSxnQkFBRyxtQkFBbUIsTUFBbkIsSUFBNkIsQ0FBQyxPQUFPLEVBQVAsQ0FBVSxNQUFWLEVBQWpDLEVBQXFEO0FBQ25ELHFCQUFPLE9BQVAsQ0FBZSw0Q0FBZjtBQUNBLHNCQUNHLEdBREgsQ0FDTyxTQURQLEVBQ2tCLE1BRGxCO0FBR0QsYUFMRCxNQU1LLElBQUcsVUFBSCxFQUFlO0FBQ2xCLHNCQUNHLFVBREgsQ0FDYyxPQURkO0FBR0Q7QUFDRjtBQXJDSSxTQW5MQTs7QUEyTlAsYUFBSztBQUNILHFCQUFXLFVBQVMsU0FBVCxFQUFvQjtBQUM3QixnQkFDRSxlQUFlLEtBRGpCO0FBR0Esd0JBQVksYUFBYSxTQUFTLFNBQWxDO0FBQ0EsZ0JBQUcsT0FBTyxTQUFQLEtBQXFCLFFBQXhCLEVBQWtDO0FBQ2hDLDBCQUFZLFVBQVUsS0FBVixDQUFnQixHQUFoQixDQUFaO0FBQ0EsZ0JBQUUsSUFBRixDQUFPLFNBQVAsRUFBa0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXFCO0FBQ3JDLG9CQUFHLFNBQVMsVUFBVSxNQUFuQixJQUE2QixTQUFTLFVBQVUsT0FBbkQsRUFBNEQ7QUFDMUQsaUNBQWUsSUFBZjtBQUNEO0FBQ0YsZUFKRDtBQUtEO0FBQ0QsbUJBQU8sWUFBUDtBQUNELFdBZkU7QUFnQkgseUJBQWUsWUFBVztBQUN4QixnQkFDRSxRQUFRLFFBQVEsSUFBUixDQUFhLE9BQWIsS0FBeUIsRUFEbkM7QUFHQSxtQkFBTyxFQUFFLE9BQUYsQ0FBVSxNQUFNLEtBQU4sQ0FBWSxhQUFaLEVBQTJCLEVBQTNCLENBQVYsQ0FBUDtBQUNEO0FBckJFLFNBM05FOztBQW1QUCxhQUFLO0FBQ0gscUJBQVcsVUFBUyxTQUFULEVBQW9CO0FBQzdCLGdCQUNFLGNBREYsRUFFRSxTQUZGOztBQUtBLG1CQUFPLE1BQVAsQ0FBYyxnQkFBZDs7O0FBR0Esd0JBQWlCLGFBQWEsU0FBUyxTQUF2QztBQUNBLDZCQUFpQixPQUFPLEdBQVAsQ0FBVyxjQUFYLENBQTBCLFNBQTFCLENBQWpCOzs7QUFHQSxtQkFBTyxJQUFQLENBQVksU0FBWixDQUFzQixjQUF0Qjs7O0FBR0EsbUJBQU8sS0FBUCxDQUFhLE9BQWI7O0FBRUEsbUJBQU8sTUFBUCxDQUFjLE1BQWQ7QUFDQSxtQkFBTyxNQUFQLENBQWMsU0FBZDs7QUFFQSxtQkFBTyxLQUFQLENBQWEsU0FBYixDQUF1QixjQUF2QjtBQUVELFdBeEJFO0FBeUJILG9CQUFVLFVBQVMsYUFBVCxFQUF3QixRQUF4QixFQUFrQztBQUMxQyx1QkFBVyxZQUFZLFNBQVMsUUFBaEM7QUFDQSx1QkFBWSxPQUFPLFFBQVAsSUFBbUIsUUFBcEIsR0FDUCxXQUFXLElBREosR0FFUCxRQUZKO0FBSUEsZ0JBQUcsWUFBWSxhQUFhLENBQTVCLEVBQStCO0FBQzdCLHFCQUFPLE9BQVAsQ0FBZSw0QkFBZixFQUE2QyxRQUE3QztBQUNBLHNCQUNHLEdBREgsQ0FDTztBQUNILHNDQUF1QjtBQURwQixlQURQO0FBS0Q7QUFDRixXQXZDRTtBQXdDSCxxQkFBVyxVQUFTLFNBQVQsRUFBb0I7QUFDN0Isd0JBQVksYUFBYSxPQUFPLEdBQVAsQ0FBVyxTQUFYLEVBQXpCO0FBQ0EsZ0JBQUcsYUFBYSxVQUFVLE1BQTFCLEVBQWtDO0FBQ2hDLHFCQUFPLEdBQVAsQ0FBVyxNQUFYO0FBQ0QsYUFGRCxNQUdLO0FBQ0gscUJBQU8sR0FBUCxDQUFXLE9BQVg7QUFDRDtBQUNGLFdBaERFO0FBaURILG1CQUFTLFlBQVc7QUFDbEIsbUJBQU8sS0FBUCxDQUFhLHdCQUFiO0FBQ0Esb0JBQ0csUUFESCxDQUNZLFVBQVUsT0FEdEI7QUFHRCxXQXRERTtBQXVESCxrQkFBUSxZQUFXO0FBQ2pCLG9CQUNHLFFBREgsQ0FDWSxVQUFVLFVBRHRCLEVBRUcsUUFGSCxDQUVZLFVBQVUsTUFGdEI7QUFJRCxXQTVERTtBQTZESCxrQkFBUSxZQUFXO0FBQ2pCLG1CQUFPLEtBQVAsQ0FBYSw2QkFBYjtBQUNBLG9CQUNHLFdBREgsQ0FDZSxVQUFVLE9BRHpCLEVBRUcsUUFGSCxDQUVZLFVBQVUsTUFGdEI7QUFJRCxXQW5FRTtBQW9FSCxtQkFBUyxZQUFXO0FBQ2xCLG1CQUFPLEtBQVAsQ0FBYSw4QkFBYjtBQUNBLG9CQUNHLFdBREgsQ0FDZSxVQUFVLE1BRHpCLEVBRUcsUUFGSCxDQUVZLFVBQVUsT0FGdEI7QUFJRCxXQTFFRTtBQTJFSCxtQkFBUyxZQUFXO0FBQ2xCLG9CQUNHLFFBREgsQ0FDWSxVQUFVLFVBRHRCLEVBRUcsUUFGSCxDQUVZLFVBQVUsT0FGdEI7QUFJRDtBQWhGRSxTQW5QRTs7QUFzVVAsZUFBTztBQUNMLHFCQUFXLFVBQVMsY0FBVCxFQUF5QjtBQUNsQyw2QkFBaUIsa0JBQWtCLE9BQU8sR0FBUCxDQUFXLGNBQVgsRUFBbkM7QUFDQSxtQkFBTyxLQUFQLENBQWEsZ0JBQWIsRUFBK0IsY0FBL0I7QUFDQSxvQkFDRyxRQURILENBQ1ksY0FEWixFQUVHLEdBRkgsQ0FFTyxlQUFlLFdBQWYsR0FBNkIsY0FGcEMsRUFFb0QsT0FBTyxRQUYzRDtBQUlBLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2QixxQkFBTyxHQUFQLENBQVcsUUFBWDtBQUNEO0FBQ0QsbUJBQU8sR0FBUCxDQUFXLFFBQVgsQ0FBb0IsU0FBUyxRQUE3QjtBQUNBLHFCQUFTLE9BQVQsQ0FBaUIsSUFBakIsQ0FBc0IsT0FBdEI7QUFDRDtBQWJJLFNBdFVBOztBQXNWUCxjQUFNO0FBQ0oscUJBQVcsVUFBUyxTQUFULEVBQW9CO0FBQzdCLGdCQUFHLENBQUMsT0FBTyxLQUFYLEVBQWtCO0FBQ2hCLHFCQUFPLEtBQVAsR0FBZSxFQUFmO0FBQ0Q7QUFDRCxtQkFBTyxLQUFQLENBQWEsU0FBYixHQUF5QixTQUF6QjtBQUNELFdBTkc7QUFPSix1QkFBYSxVQUFTLFdBQVQsRUFBc0I7QUFDakMsZ0JBQUcsZ0JBQWdCLE1BQW5CLEVBQTJCO0FBQ3pCLHNCQUFRLElBQVIsQ0FBYSxTQUFTLFdBQXRCLEVBQW1DLFdBQW5DO0FBQ0Q7QUFDRixXQVhHO0FBWUosNEJBQWtCLFVBQVMsU0FBVCxFQUFvQixNQUFwQixFQUE0QjtBQUM1QyxjQUFFLEVBQUYsQ0FBSyxVQUFMLENBQWdCLE1BQWhCLENBQXVCLFNBQXZCLElBQW9DLE1BQXBDO0FBQ0EsbUJBQU8sT0FBUCxDQUFlLGdDQUFmLEVBQWlELFNBQWpELEVBQTRELE1BQTVEO0FBQ0Q7QUFmRyxTQXRWQzs7QUF3V1AsaUJBQVM7QUFDUCxzQkFBWSxZQUFXO0FBQ3JCLGdCQUNFLFlBQVksT0FBTyxHQUFQLENBQVcsZ0JBQVgsRUFEZDtBQUdBLGdCQUFHLFNBQUgsRUFBYztBQUNaLHNCQUNHLFdBREgsQ0FDZSxTQURmO0FBR0EscUJBQU8sT0FBUCxDQUFlLDBCQUFmLEVBQTJDLE9BQU8sS0FBbEQ7QUFDRDtBQUNELG1CQUFPLE1BQVAsQ0FBYyxRQUFkO0FBQ0Q7QUFaTSxTQXhXRjs7QUF1WFAsYUFBSztBQUNILG9CQUFVLFlBQVc7QUFDbkIsZ0JBQ0UsV0FBVyxPQUFPLEdBQVAsQ0FBVyxRQUFYLEVBRGI7QUFHQSxtQkFBTyxLQUFQLEdBQWUsV0FBVyxZQUFXO0FBQ25DLHNCQUFRLGNBQVIsQ0FBdUIsWUFBdkI7QUFDRCxhQUZjLEVBRVosV0FBVyxTQUFTLGFBRlIsQ0FBZjtBQUdBLG1CQUFPLE9BQVAsQ0FBZSx3QkFBZixFQUF5QyxPQUFPLEtBQWhEO0FBQ0Q7QUFURSxTQXZYRTs7QUFtWVAsZ0JBQVE7QUFDTixxQkFBVyxZQUFXO0FBQ3BCLG9CQUFRLFdBQVIsQ0FBb0IsVUFBVSxTQUE5QjtBQUNELFdBSEs7QUFJTiw4QkFBb0IsWUFBVztBQUM3QixtQkFBTyxNQUFQLENBQWMsYUFBZDtBQUNBLG1CQUFPLE1BQVAsQ0FBYyxnQkFBZDtBQUNELFdBUEs7QUFRTix5QkFBZSxZQUFXO0FBQ3hCLG9CQUFRLEdBQVIsQ0FBWSxXQUFXLGNBQXZCO0FBQ0QsV0FWSztBQVdOLDRCQUFrQixZQUFXO0FBQzNCLG9CQUFRLEdBQVIsQ0FBWSxjQUFjLGNBQTFCO0FBQ0QsV0FiSztBQWNOLG1CQUFTLFlBQVc7QUFDbEIsb0JBQVEsR0FBUixDQUFZLFNBQVosRUFBdUIsRUFBdkI7QUFDRCxXQWhCSztBQWlCTixxQkFBVyxZQUFXO0FBQ3BCLG9CQUNHLFdBREgsQ0FDZSxVQUFVLE1BRHpCLEVBRUcsV0FGSCxDQUVlLFVBQVUsT0FGekI7QUFJRCxXQXRCSztBQXVCTixvQkFBVSxZQUFXO0FBQ25CLG9CQUNHLEdBREgsQ0FDTyxvQkFEUCxFQUM2QixFQUQ3QjtBQUdELFdBM0JLO0FBNEJOLG9CQUFVLFlBQVc7QUFDbkIsbUJBQU8sT0FBUCxDQUFlLDBCQUFmLEVBQTJDLE9BQU8sS0FBbEQ7QUFDQSxnQkFBRyxPQUFPLEtBQVYsRUFBaUI7QUFDZiwyQkFBYSxPQUFPLEtBQXBCO0FBQ0Q7QUFDRixXQWpDSztBQWtDTixrQkFBUSxZQUFXO0FBQ2pCLG9CQUFRLFdBQVIsQ0FBb0IsVUFBVSxNQUE5QjtBQUNELFdBcENLO0FBcUNOLG1CQUFTLFlBQVc7QUFDbEIsb0JBQVEsV0FBUixDQUFvQixVQUFVLE9BQTlCO0FBQ0QsV0F2Q0s7QUF3Q04sbUJBQVMsWUFBVztBQUNsQixtQkFBTyxLQUFQLENBQWEsbUNBQWI7QUFDQSxnQkFBSSxPQUFPLEVBQVAsQ0FBVSxPQUFWLEVBQUosRUFBMEI7QUFDeEIscUJBQU8sS0FBUDtBQUNBLHNCQUNHLFdBREgsQ0FDZSxVQUFVLE9BRHpCO0FBR0Q7QUFDRixXQWhESztBQWlETixzQkFBWSxZQUFXO0FBQ3JCLG9CQUNHLFdBREgsQ0FDZSxVQUFVLE9BRHpCLEVBRUcsV0FGSCxDQUVlLFVBQVUsTUFGekI7QUFJRDtBQXRESyxTQW5ZRDtBQTJiUCxhQUFLO0FBQ0gsb0JBQVUsVUFBUyxTQUFULEVBQW9CLFFBQXBCLEVBQThCLFVBQTlCLEVBQTBDOztBQUVsRCxnQkFBRyxPQUFPLFNBQVAsSUFBb0IsUUFBdkIsRUFBaUM7QUFDL0IscUJBQU8sRUFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUIsRUFBRSxFQUFGLENBQUssVUFBTCxDQUFnQixRQUFuQyxFQUE2QyxTQUE3QyxDQUFQO0FBQ0Q7O0FBRkQsaUJBSUssSUFBRyxPQUFPLFVBQVAsSUFBcUIsVUFBeEIsRUFBb0M7QUFDdkMsdUJBQU8sRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLEVBQUUsRUFBRixDQUFLLFVBQUwsQ0FBZ0IsUUFBN0IsRUFBdUM7QUFDNUMsNkJBQWEsU0FEK0I7QUFFNUMsOEJBQWEsVUFGK0I7QUFHNUMsNEJBQWE7QUFIK0IsaUJBQXZDLENBQVA7QUFLRDs7QUFOSSxtQkFRQSxJQUFHLE9BQU8sUUFBUCxJQUFtQixRQUFuQixJQUErQixPQUFPLFFBQVAsSUFBbUIsUUFBckQsRUFBK0Q7QUFDbEUseUJBQU8sRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLEVBQUUsRUFBRixDQUFLLFVBQUwsQ0FBZ0IsUUFBN0IsRUFBdUM7QUFDNUMsK0JBQVksU0FEZ0M7QUFFNUMsOEJBQVk7QUFGZ0MsbUJBQXZDLENBQVA7QUFJRDs7QUFMSSxxQkFPQSxJQUFHLE9BQU8sUUFBUCxJQUFtQixRQUF0QixFQUFnQztBQUNuQywyQkFBTyxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsRUFBRSxFQUFGLENBQUssVUFBTCxDQUFnQixRQUE3QixFQUF1QyxRQUF2QyxFQUFpRDtBQUN0RCxpQ0FBWTtBQUQwQyxxQkFBakQsQ0FBUDtBQUdEOztBQUpJLHVCQU1BLElBQUcsT0FBTyxRQUFQLElBQW1CLFVBQXRCLEVBQWtDO0FBQ3JDLDZCQUFPLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxFQUFFLEVBQUYsQ0FBSyxVQUFMLENBQWdCLFFBQTdCLEVBQXVDO0FBQzVDLG1DQUFhLFNBRCtCO0FBRTVDLG9DQUFhO0FBRitCLHVCQUF2QyxDQUFQO0FBSUQ7O0FBTEkseUJBT0E7QUFDSCwrQkFBTyxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsRUFBRSxFQUFGLENBQUssVUFBTCxDQUFnQixRQUE3QixFQUF1QztBQUM1QyxxQ0FBWTtBQURnQyx5QkFBdkMsQ0FBUDtBQUdEO0FBQ0QsbUJBQU8sRUFBRSxFQUFGLENBQUssVUFBTCxDQUFnQixRQUF2QjtBQUNELFdBekNFO0FBMENILDBCQUFnQixVQUFTLFNBQVQsRUFBb0I7QUFDbEMsZ0JBQ0UsaUJBQWlCLGFBQWEsU0FBUyxTQUR6QztBQUFBLGdCQUVFLGlCQUFrQixPQUFPLEdBQVAsQ0FBVyxVQUFYLE1BQTJCLENBQUMsT0FBTyxHQUFQLENBQVcsU0FBWCxFQUE3QixHQUNiLE9BQU8sR0FBUCxDQUFXLFNBQVgsS0FBeUIsR0FEWixHQUViLEVBSk47QUFNQSxtQkFBTyxVQUFVLFNBQVYsR0FBc0IsR0FBdEIsR0FDSCxVQUFVLFVBRFAsR0FDb0IsR0FEcEIsR0FFSCxjQUZHLEdBR0gsY0FISjtBQUtELFdBdERFO0FBdURILDRCQUFrQixZQUFXO0FBQzNCLG1CQUFRLE9BQU8sS0FBUCxJQUFnQixPQUFPLEtBQVAsQ0FBYSxTQUFiLEtBQTJCLFNBQTVDLEdBQ0gsT0FBTyxLQUFQLENBQWEsU0FEVixHQUVILEtBRko7QUFJRCxXQTVERTtBQTZESCw0QkFBa0IsWUFBVztBQUMzQixtQkFBTyxPQUFPLEVBQVAsQ0FBVSxNQUFWLEtBQ0gsVUFBVSxNQURQLEdBRUgsVUFBVSxPQUZkO0FBSUQsV0FsRUU7QUFtRUgscUJBQVcsWUFBVztBQUNwQixtQkFBTyxPQUFPLEVBQVAsQ0FBVSxNQUFWLE1BQXNCLENBQUMsT0FBTyxFQUFQLENBQVUsT0FBVixFQUF2QixHQUNILFVBQVUsTUFEUCxHQUVILFVBQVUsT0FGZDtBQUlELFdBeEVFO0FBeUVILDhCQUFvQixVQUFTLFNBQVQsRUFBb0I7QUFDdEMsZ0JBQ0UsU0FERjtBQUdBLHdCQUFZLGFBQWEsU0FBUyxTQUFsQztBQUNBLGdCQUFHLE9BQU8sU0FBUCxLQUFxQixRQUF4QixFQUFrQztBQUNoQywwQkFBWSxVQUFVLEtBQVYsQ0FBZ0IsR0FBaEIsQ0FBWjs7QUFFQSxnQkFBRSxJQUFGLENBQU8sU0FBUCxFQUFrQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBcUI7QUFDckMsb0JBQUcsU0FBUyxVQUFVLE1BQXRCLEVBQThCO0FBQzVCLDhCQUFZLFVBQVUsTUFBdEI7QUFDRCxpQkFGRCxNQUdLLElBQUcsU0FBUyxVQUFVLE9BQXRCLEVBQStCO0FBQ2xDLDhCQUFZLFVBQVUsT0FBdEI7QUFDRDtBQUNGLGVBUEQ7QUFRRDs7QUFFRCxnQkFBRyxTQUFILEVBQWM7QUFDWixxQkFBTyxTQUFQO0FBQ0Q7QUFDRCxtQkFBTyxLQUFQO0FBQ0QsV0EvRkU7QUFnR0gsb0JBQVUsVUFBUyxRQUFULEVBQW1CO0FBQzNCLHVCQUFXLFlBQVksU0FBUyxRQUFoQztBQUNBLGdCQUFHLGFBQWEsS0FBaEIsRUFBdUI7QUFDckIseUJBQVcsUUFBUSxHQUFSLENBQVksb0JBQVosS0FBcUMsQ0FBaEQ7QUFDRDtBQUNELG1CQUFRLE9BQU8sUUFBUCxLQUFvQixRQUFyQixHQUNGLFNBQVMsT0FBVCxDQUFpQixJQUFqQixJQUF5QixDQUFDLENBQTNCLEdBQ0UsV0FBVyxRQUFYLENBREYsR0FFRSxXQUFXLFFBQVgsSUFBdUIsSUFIdEIsR0FJSCxRQUpKO0FBTUQsV0EzR0U7QUE0R0gsdUJBQWEsWUFBVztBQUN0QixnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sU0FBUyxXQUFoQjtBQUNEO0FBQ0QsZ0JBQUcsUUFBUSxJQUFSLENBQWEsU0FBUyxXQUF0QixNQUF1QyxTQUExQyxFQUFxRDs7QUFFbkQscUJBQU8sR0FBUCxDQUFXLFVBQVgsQ0FBc0IsSUFBdEI7QUFDRDtBQUNELG1CQUFPLFFBQVEsSUFBUixDQUFhLFNBQVMsV0FBdEIsQ0FBUDtBQUNELFdBckhFO0FBc0hILHFCQUFXLFVBQVMsS0FBVCxFQUFnQjtBQUN6QixvQkFBUSxTQUFTLFFBQVEsSUFBUixDQUFhLE9BQWIsQ0FBVCxJQUFrQyxFQUExQztBQUNBLG1CQUFPLE1BQU0sT0FBTixDQUFjLGFBQWQsRUFBNkIsRUFBN0IsQ0FBUDtBQUNELFdBekhFO0FBMEhILDRCQUFrQixVQUFTLFNBQVQsRUFBb0I7QUFDcEMsbUJBQU8sRUFBRSxFQUFGLENBQUssVUFBTCxDQUFnQixNQUFoQixDQUF1QixTQUF2QixDQUFQO0FBQ0QsV0E1SEU7QUE2SEgsK0JBQXFCLFlBQVc7QUFDOUIsZ0JBQ0UsVUFBYyxTQUFTLGFBQVQsQ0FBdUIsS0FBdkIsQ0FEaEI7QUFBQSxnQkFFRSxhQUFjO0FBQ1osMkJBQW1CLGdCQURQO0FBRVosNEJBQW1CLGlCQUZQO0FBR1osOEJBQW1CLG1CQUhQO0FBSVosaUNBQW1CO0FBSlAsYUFGaEI7QUFBQSxnQkFRRSxTQVJGO0FBVUEsaUJBQUksU0FBSixJQUFpQixVQUFqQixFQUE0QjtBQUMxQixrQkFBSSxRQUFRLEtBQVIsQ0FBYyxTQUFkLE1BQTZCLFNBQWpDLEVBQTRDO0FBQzFDLHVCQUFPLFdBQVcsU0FBWCxDQUFQO0FBQ0Q7QUFDRjtBQUNELG1CQUFPLEtBQVA7QUFDRCxXQTlJRTtBQStJSCw2QkFBbUIsWUFBVztBQUM1QixnQkFDRSxVQUFjLFNBQVMsYUFBVCxDQUF1QixLQUF2QixDQURoQjtBQUFBLGdCQUVFLGFBQWM7QUFDWiwyQkFBbUIsY0FEUDtBQUVaLDRCQUFtQixlQUZQO0FBR1osOEJBQW1CLGlCQUhQO0FBSVosaUNBQW1CO0FBSlAsYUFGaEI7QUFBQSxnQkFRRSxTQVJGO0FBVUEsaUJBQUksU0FBSixJQUFpQixVQUFqQixFQUE0QjtBQUMxQixrQkFBSSxRQUFRLEtBQVIsQ0FBYyxTQUFkLE1BQTZCLFNBQWpDLEVBQTRDO0FBQzFDLHVCQUFPLFdBQVcsU0FBWCxDQUFQO0FBQ0Q7QUFDRjtBQUNELG1CQUFPLEtBQVA7QUFDRDs7QUFoS0UsU0EzYkU7O0FBK2xCUCxhQUFLO0FBQ0gsc0JBQVksVUFBUyxNQUFULEVBQWlCO0FBQzNCLGdCQUNFLFlBQW9CLFNBQVMsU0FEL0I7QUFBQSxnQkFFRSxtQkFBb0IsT0FBTyxHQUFQLENBQVcsZ0JBQVgsQ0FBNEIsU0FBNUIsQ0FGdEI7QUFBQSxnQkFHRSxZQUhGO0FBQUEsZ0JBSUUsT0FKRjtBQUFBLGdCQUtFLE1BTEY7QUFBQSxnQkFNRSxnQkFORjtBQUFBLGdCQU9FLFdBUEY7QUFBQSxnQkFRRSxlQVJGO0FBQUEsZ0JBU0UsV0FURjtBQVdBLGdCQUFJLHFCQUFxQixTQUFyQixJQUFrQyxNQUF0QyxFQUE4QztBQUM1QyxxQkFBTyxPQUFQLENBQWUsc0NBQWY7QUFDQSw2QkFBZSxRQUFRLElBQVIsQ0FBYSxPQUFiLENBQWY7QUFDQSx3QkFBZSxRQUFRLElBQVIsQ0FBYSxTQUFiLENBQWY7O0FBRUEsdUJBQVMsRUFBRSxNQUFNLE9BQU4sR0FBZ0IsS0FBbEIsRUFBeUIsUUFBekIsQ0FBbUMsWUFBbkMsRUFBa0QsV0FBbEQsQ0FBOEQsT0FBOUQsQ0FBVDtBQUNBLGlDQUFtQixPQUNoQixRQURnQixDQUNQLFNBRE8sRUFFaEIsV0FGZ0IsQ0FFSixVQUFVLE1BRk4sRUFHaEIsV0FIZ0IsQ0FHSixVQUFVLE9BSE4sRUFJaEIsUUFKZ0IsQ0FJUCxVQUFVLFNBSkgsRUFLaEIsUUFMZ0IsQ0FLUCxVQUFVLFVBTEgsRUFNaEIsR0FOZ0IsQ0FNWixlQU5ZLENBQW5CO0FBUUEsNEJBQWMsT0FDWCxRQURXLENBQ0YsVUFBVSxNQURSLEVBRVgsR0FGVyxDQUVQLGVBRk8sQ0FBZDtBQUlBLDRCQUFjLE9BQ1gsSUFEVyxDQUNOLE9BRE0sRUFDRyxZQURILEVBRVgsVUFGVyxDQUVBLE9BRkEsRUFHWCxXQUhXLENBR0MsVUFBVSxNQUhYLEVBSVgsV0FKVyxDQUlDLFVBQVUsT0FKWCxFQUtYLElBTFcsR0FNWCxHQU5XLENBTVAsU0FOTyxDQUFkO0FBUUEscUJBQU8sT0FBUCxDQUFlLGlDQUFmLEVBQWtELFdBQWxEO0FBQ0EscUJBQU8sSUFBUCxDQUFZLFdBQVosQ0FBd0IsV0FBeEI7O0FBRUEscUJBQU8sTUFBUDtBQUNBLGtCQUFHLG9CQUFvQixXQUF2QixFQUFvQztBQUNsQyx1QkFBTyxLQUFQLENBQWEsZ0NBQWIsRUFBK0MsU0FBL0M7QUFDQSxrQ0FBa0IsSUFBbEI7QUFDRCxlQUhELE1BSUssSUFBRyxvQkFBb0IsTUFBcEIsSUFBOEIsQ0FBQyxnQkFBbEMsRUFBb0Q7QUFDdkQsdUJBQU8sS0FBUCxDQUFhLDZCQUFiLEVBQTRDLFNBQTVDO0FBQ0E7QUFDRCxlQUhJLE1BSUE7QUFDSCx1QkFBTyxLQUFQLENBQWEsd0JBQWIsRUFBdUMsU0FBdkMsRUFBa0QsV0FBbEQ7QUFDQSxrQ0FBa0IsS0FBbEI7QUFDRDtBQUNELHFCQUFPLElBQVAsQ0FBWSxnQkFBWixDQUE2QixTQUE3QixFQUF3QyxlQUF4QztBQUNEO0FBQ0QsbUJBQVEscUJBQXFCLFNBQXRCLEdBQ0gsZ0JBREcsR0FFSCxlQUZKO0FBSUQsV0E3REU7QUE4REgsbUJBQVMsWUFBVzs7QUFFbEIsbUJBQVEsT0FBTyxHQUFQLENBQVcsVUFBWCxPQUE0QixTQUFwQztBQUNEO0FBakVFLFNBL2xCRTs7QUFtcUJQLFlBQUk7QUFDRixxQkFBVyxZQUFXO0FBQ3BCLG1CQUFPLFFBQVEsUUFBUixDQUFpQixVQUFVLFNBQTNCLENBQVA7QUFDRCxXQUhDO0FBSUYsa0JBQVEsWUFBVztBQUNqQixtQkFBTyxRQUFRLFFBQVIsQ0FBaUIsVUFBVSxNQUEzQixDQUFQO0FBQ0QsV0FOQztBQU9GLG1CQUFTLFlBQVc7QUFDbEIsbUJBQU8sUUFBUSxRQUFSLENBQWlCLFVBQVUsT0FBM0IsQ0FBUDtBQUNELFdBVEM7QUFVRixtQkFBUyxZQUFXO0FBQ2xCLG1CQUFPLFFBQVEsUUFBUixDQUFpQixVQUFVLE9BQTNCLENBQVA7QUFDRCxXQVpDO0FBYUYscUJBQVcsVUFBUyxTQUFULEVBQW9CO0FBQzdCLHdCQUFZLGFBQWEsU0FBUyxTQUFsQztBQUNBLHdCQUFZLE1BQU0sVUFBVSxPQUFWLENBQWtCLEdBQWxCLEVBQXVCLEdBQXZCLENBQWxCO0FBQ0EsbUJBQVMsUUFBUSxNQUFSLENBQWUsU0FBZixFQUEwQixNQUExQixHQUFtQyxDQUE1QztBQUNELFdBakJDO0FBa0JGLG1CQUFTLFlBQVc7QUFDbEIsbUJBQU8sUUFBUSxFQUFSLENBQVcsVUFBWCxDQUFQO0FBQ0QsV0FwQkM7QUFxQkYsa0JBQVEsWUFBVztBQUNqQixtQkFBTyxRQUFRLEdBQVIsQ0FBWSxZQUFaLE1BQThCLFFBQXJDO0FBQ0QsV0F2QkM7QUF3QkYscUJBQVcsWUFBVztBQUNwQixtQkFBTyxpQkFBaUIsS0FBeEI7QUFDRDtBQTFCQyxTQW5xQkc7O0FBZ3NCUCxjQUFNLFlBQVc7QUFDZixpQkFBTyxPQUFQLENBQWUsZ0JBQWY7QUFDQSxjQUFJLE9BQU8sRUFBUCxDQUFVLFNBQVYsRUFBSixFQUE0QjtBQUMxQixtQkFBTyxLQUFQO0FBQ0Q7QUFDRCxrQkFBUSxJQUFSLEc7QUFDQSxpQkFBTyxNQUFQLENBQWMsT0FBZDtBQUNBLGlCQUFPLE1BQVAsQ0FBYyxPQUFkO0FBQ0EsaUJBQU8sR0FBUCxDQUFXLE1BQVg7QUFDQSxpQkFBTyxLQUFQLENBQWEsTUFBYjtBQUNBLG1CQUFTLE1BQVQsQ0FBZ0IsSUFBaEIsQ0FBcUIsT0FBckI7QUFDQSxtQkFBUyxVQUFULENBQW9CLElBQXBCLENBQXlCLE9BQXpCOztBQUVELFNBN3NCTTs7QUErc0JQLGNBQU0sVUFBUyxPQUFULEVBQWtCO0FBQ3RCLGlCQUFPLE9BQVAsQ0FBZSxpQkFBZixFQUFrQyxPQUFsQztBQUNBLGlCQUFPLE1BQVAsQ0FBYyxNQUFkO0FBQ0EsaUJBQU8sR0FBUCxDQUFXLE9BQVg7QUFDQSxpQkFBTyxLQUFQLENBQWEsT0FBYjtBQUNBLG1CQUFTLE1BQVQsQ0FBZ0IsSUFBaEIsQ0FBcUIsT0FBckI7QUFDQSxtQkFBUyxVQUFULENBQW9CLElBQXBCLENBQXlCLE9BQXpCOztBQUVELFNBdnRCTTs7QUF5dEJQLGdCQUFRLFlBQVc7QUFDakIsY0FBSSxPQUFPLEVBQVAsQ0FBVSxPQUFWLEVBQUosRUFBMEI7QUFDeEIsbUJBQU8sSUFBUDtBQUNELFdBRkQsTUFHSztBQUNILG1CQUFPLElBQVA7QUFDRDtBQUNGLFNBaHVCTTs7QUFrdUJQLGNBQU0sWUFBVztBQUNmLGlCQUFPLEtBQVAsQ0FBYSw0QkFBYjtBQUNBLGtCQUFRLGNBQVIsQ0FBdUIsWUFBdkI7QUFDRCxTQXJ1Qk07O0FBdXVCUCxpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLEtBQVAsQ0FBYSx3QkFBYjtBQUNBLGlCQUFPLE1BQVAsQ0FBYyxhQUFkO0FBQ0Esa0JBQVEsY0FBUixDQUF1QixZQUF2QjtBQUNELFNBM3VCTTs7QUE2dUJQLGVBQU87QUFDTCxpQkFBTyxZQUFXO0FBQ2hCLG1CQUFPLEtBQVAsQ0FBYSwwQkFBYjtBQUNBLG1CQUFPLE1BQVAsQ0FBYyxhQUFkO0FBQ0Q7QUFKSSxTQTd1QkE7O0FBb3ZCUCxnQkFBUSxZQUFXO0FBQ2pCLGlCQUFPLE9BQVAsQ0FBZSxvQkFBZjtBQUNBLGtCQUFRLFdBQVIsQ0FBb0IsVUFBVSxRQUE5QjtBQUNELFNBdnZCTTs7QUF5dkJQLGlCQUFTLFlBQVc7QUFDbEIsaUJBQU8sS0FBUCxDQUFhLG9CQUFiO0FBQ0Esa0JBQVEsUUFBUixDQUFpQixVQUFVLFFBQTNCO0FBQ0QsU0E1dkJNOztBQTh2QlAsaUJBQVMsVUFBUyxJQUFULEVBQWUsS0FBZixFQUFzQjtBQUM3QixpQkFBTyxLQUFQLENBQWEsa0JBQWIsRUFBaUMsSUFBakMsRUFBdUMsS0FBdkM7QUFDQSxjQUFJLEVBQUUsYUFBRixDQUFnQixJQUFoQixDQUFKLEVBQTRCO0FBQzFCLGNBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxRQUFmLEVBQXlCLElBQXpCO0FBQ0QsV0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLGdCQUFHLEVBQUUsYUFBRixDQUFnQixTQUFTLElBQVQsQ0FBaEIsQ0FBSCxFQUFvQztBQUNsQyxnQkFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLFNBQVMsSUFBVCxDQUFmLEVBQStCLEtBQS9CO0FBQ0QsYUFGRCxNQUdLO0FBQ0gsdUJBQVMsSUFBVCxJQUFpQixLQUFqQjtBQUNEO0FBQ0YsV0FQSSxNQVFBO0FBQ0gsbUJBQU8sU0FBUyxJQUFULENBQVA7QUFDRDtBQUNGLFNBOXdCTTtBQSt3QlAsa0JBQVUsVUFBUyxJQUFULEVBQWUsS0FBZixFQUFzQjtBQUM5QixjQUFJLEVBQUUsYUFBRixDQUFnQixJQUFoQixDQUFKLEVBQTRCO0FBQzFCLGNBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxNQUFmLEVBQXVCLElBQXZCO0FBQ0QsV0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLG1CQUFPLElBQVAsSUFBZSxLQUFmO0FBQ0QsV0FGSSxNQUdBO0FBQ0gsbUJBQU8sT0FBTyxJQUFQLENBQVA7QUFDRDtBQUNGLFNBenhCTTtBQTB4QlAsZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQVYsSUFBb0IsU0FBUyxLQUFoQyxFQUF1QztBQUNyQyxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLEtBQVAsR0FBZSxTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxJQUFyQyxFQUEyQyxPQUEzQyxFQUFvRCxTQUFTLElBQVQsR0FBZ0IsR0FBcEUsQ0FBZjtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLE9BQW5CLEVBQTRCLFNBQTVCO0FBQ0Q7QUFDRjtBQUNGLFNBcHlCTTtBQXF5QlAsaUJBQVMsWUFBVztBQUNsQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsT0FBN0IsSUFBd0MsU0FBUyxLQUFwRCxFQUEyRDtBQUN6RCxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLE9BQVAsR0FBaUIsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWpCO0FBQ0EscUJBQU8sT0FBUCxDQUFlLEtBQWYsQ0FBcUIsT0FBckIsRUFBOEIsU0FBOUI7QUFDRDtBQUNGO0FBQ0YsU0EveUJNO0FBZ3pCUCxlQUFPLFlBQVc7QUFDaEIsY0FBRyxDQUFDLFNBQVMsTUFBYixFQUFxQjtBQUNuQixtQkFBTyxLQUFQLEdBQWUsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsS0FBckMsRUFBNEMsT0FBNUMsRUFBcUQsU0FBUyxJQUFULEdBQWdCLEdBQXJFLENBQWY7QUFDQSxtQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNEO0FBQ0YsU0FyekJNO0FBc3pCUCxxQkFBYTtBQUNYLGVBQUssVUFBUyxPQUFULEVBQWtCO0FBQ3JCLGdCQUNFLFdBREYsRUFFRSxhQUZGLEVBR0UsWUFIRjtBQUtBLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2Qiw0QkFBZ0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUFoQjtBQUNBLDZCQUFnQixRQUFRLFdBQXhCO0FBQ0EsOEJBQWdCLGNBQWMsWUFBOUI7QUFDQSxxQkFBZ0IsV0FBaEI7QUFDQSwwQkFBWSxJQUFaLENBQWlCO0FBQ2Ysd0JBQW1CLFFBQVEsQ0FBUixDQURKO0FBRWYsNkJBQW1CLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxPQUFkLEVBQXVCLENBQXZCLEtBQTZCLEVBRmpDO0FBR2YsMkJBQW1CLE9BSEo7QUFJZixrQ0FBbUI7QUFKSixlQUFqQjtBQU1EO0FBQ0QseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsbUJBQU8sV0FBUCxDQUFtQixLQUFuQixHQUEyQixXQUFXLE9BQU8sV0FBUCxDQUFtQixPQUE5QixFQUF1QyxHQUF2QyxDQUEzQjtBQUNELFdBckJVO0FBc0JYLG1CQUFTLFlBQVc7QUFDbEIsZ0JBQ0UsUUFBUSxTQUFTLElBQVQsR0FBZ0IsR0FEMUI7QUFBQSxnQkFFRSxZQUFZLENBRmQ7QUFJQSxtQkFBTyxLQUFQO0FBQ0EseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsY0FBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMkJBQWEsS0FBSyxnQkFBTCxDQUFiO0FBQ0QsYUFGRDtBQUdBLHFCQUFTLE1BQU0sU0FBTixHQUFrQixJQUEzQjtBQUNBLGdCQUFHLGNBQUgsRUFBbUI7QUFDakIsdUJBQVMsUUFBUSxjQUFSLEdBQXlCLElBQWxDO0FBQ0Q7QUFDRCxnQkFBRyxZQUFZLE1BQVosR0FBcUIsQ0FBeEIsRUFBMkI7QUFDekIsdUJBQVMsTUFBTSxHQUFOLEdBQVksWUFBWSxNQUF4QixHQUFpQyxHQUExQztBQUNEO0FBQ0QsZ0JBQUksQ0FBQyxRQUFRLEtBQVIsS0FBa0IsU0FBbEIsSUFBK0IsUUFBUSxLQUFSLEtBQWtCLFNBQWxELEtBQWdFLFlBQVksTUFBWixHQUFxQixDQUF6RixFQUE0RjtBQUMxRixzQkFBUSxjQUFSLENBQXVCLEtBQXZCO0FBQ0Esa0JBQUcsUUFBUSxLQUFYLEVBQWtCO0FBQ2hCLHdCQUFRLEtBQVIsQ0FBYyxXQUFkO0FBQ0QsZUFGRCxNQUdLO0FBQ0gsa0JBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDBCQUFRLEdBQVIsQ0FBWSxLQUFLLE1BQUwsSUFBZSxJQUFmLEdBQXNCLEtBQUssZ0JBQUwsQ0FBdEIsR0FBNkMsSUFBekQ7QUFDRCxpQkFGRDtBQUdEO0FBQ0Qsc0JBQVEsUUFBUjtBQUNEO0FBQ0QsMEJBQWMsRUFBZDtBQUNEO0FBcERVLFNBdHpCTjs7QUE2MkJQLGdCQUFRLFVBQVMsS0FBVCxFQUFnQixlQUFoQixFQUFpQyxPQUFqQyxFQUEwQztBQUNoRCxjQUNFLFNBQVMsUUFEWDtBQUFBLGNBRUUsUUFGRjtBQUFBLGNBR0UsS0FIRjtBQUFBLGNBSUUsUUFKRjtBQU1BLDRCQUFrQixtQkFBbUIsY0FBckM7QUFDQSxvQkFBa0IsV0FBbUIsT0FBckM7QUFDQSxjQUFHLE9BQU8sS0FBUCxJQUFnQixRQUFoQixJQUE0QixXQUFXLFNBQTFDLEVBQXFEO0FBQ25ELG9CQUFXLE1BQU0sS0FBTixDQUFZLE9BQVosQ0FBWDtBQUNBLHVCQUFXLE1BQU0sTUFBTixHQUFlLENBQTFCO0FBQ0EsY0FBRSxJQUFGLENBQU8sS0FBUCxFQUFjLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUF1QjtBQUNuQyxrQkFBSSxpQkFBa0IsU0FBUyxRQUFWLEdBQ2pCLFFBQVEsTUFBTSxRQUFRLENBQWQsRUFBaUIsTUFBakIsQ0FBd0IsQ0FBeEIsRUFBMkIsV0FBM0IsRUFBUixHQUFtRCxNQUFNLFFBQVEsQ0FBZCxFQUFpQixLQUFqQixDQUF1QixDQUF2QixDQURsQyxHQUVqQixLQUZKO0FBSUEsa0JBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sY0FBUCxDQUFqQixLQUE4QyxTQUFTLFFBQTNELEVBQXVFO0FBQ3JFLHlCQUFTLE9BQU8sY0FBUCxDQUFUO0FBQ0QsZUFGRCxNQUdLLElBQUksT0FBTyxjQUFQLE1BQTJCLFNBQS9CLEVBQTJDO0FBQzlDLHdCQUFRLE9BQU8sY0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQSxJQUFJLEVBQUUsYUFBRixDQUFpQixPQUFPLEtBQVAsQ0FBakIsS0FBcUMsU0FBUyxRQUFsRCxFQUE4RDtBQUNqRSx5QkFBUyxPQUFPLEtBQVAsQ0FBVDtBQUNELGVBRkksTUFHQSxJQUFJLE9BQU8sS0FBUCxNQUFrQixTQUF0QixFQUFrQztBQUNyQyx3QkFBUSxPQUFPLEtBQVAsQ0FBUjtBQUNBLHVCQUFPLEtBQVA7QUFDRCxlQUhJLE1BSUE7QUFDSCx1QkFBTyxLQUFQO0FBQ0Q7QUFDRixhQXRCRDtBQXVCRDtBQUNELGNBQUssRUFBRSxVQUFGLENBQWMsS0FBZCxDQUFMLEVBQTZCO0FBQzNCLHVCQUFXLE1BQU0sS0FBTixDQUFZLE9BQVosRUFBcUIsZUFBckIsQ0FBWDtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQix1QkFBVyxLQUFYO0FBQ0Q7O0FBRUQsY0FBRyxFQUFFLE9BQUYsQ0FBVSxhQUFWLENBQUgsRUFBNkI7QUFDM0IsMEJBQWMsSUFBZCxDQUFtQixRQUFuQjtBQUNELFdBRkQsTUFHSyxJQUFHLGtCQUFrQixTQUFyQixFQUFnQztBQUNuQyw0QkFBZ0IsQ0FBQyxhQUFELEVBQWdCLFFBQWhCLENBQWhCO0FBQ0QsV0FGSSxNQUdBLElBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUM5Qiw0QkFBZ0IsUUFBaEI7QUFDRDtBQUNELGlCQUFRLFVBQVUsU0FBWCxHQUNILEtBREcsR0FFSCxLQUZKO0FBSUQ7QUFyNkJNLE9BQVQ7QUF1NkJBLGFBQU8sVUFBUDtBQUNELEtBOTdCSDtBQWc4QkEsV0FBUSxrQkFBa0IsU0FBbkIsR0FDSCxhQURHLEdBRUgsSUFGSjtBQUlELEdBejlCRDs7O0FBNDlCQSxJQUFFLEVBQUYsQ0FBSyxVQUFMLENBQWdCLE1BQWhCLEdBQXlCLEVBQXpCOztBQUVBLElBQUUsRUFBRixDQUFLLFVBQUwsQ0FBZ0IsUUFBaEIsR0FBMkI7OztBQUd6QixVQUFnQixZQUhTOzs7QUFNekIsWUFBZ0IsS0FOUzs7O0FBU3pCLFdBQWdCLEtBVFM7OztBQVl6QixhQUFnQixLQVpTOzs7QUFlekIsaUJBQWdCLElBZlM7OztBQWtCekIsZUFBZ0IsWUFsQlM7OztBQXFCekIsY0FBZ0IsQ0FyQlM7OztBQXdCekIsYUFBZ0IsTUF4QlM7OztBQTJCekIsYUFBZ0IsWUFBVyxDQUFFLENBM0JKO0FBNEJ6QixnQkFBZ0IsWUFBVyxDQUFFLENBNUJKO0FBNkJ6QixZQUFnQixZQUFXLENBQUUsQ0E3Qko7QUE4QnpCLFlBQWdCLFlBQVcsQ0FBRSxDQTlCSjs7O0FBaUN6QixpQkFBZ0IsSUFqQ1M7OztBQW9DekIsbUJBQWdCLEdBcENTOzs7QUF1Q3pCLGtCQUFnQixLQXZDUzs7O0FBMEN6QixpQkFBZ0IsS0ExQ1M7OztBQTZDekIsZUFBZ0IsTUE3Q1M7QUE4Q3pCLGNBQWdCLEtBOUNTOzs7QUFpRHpCLFdBQWdCLElBakRTOztBQW1EekIsY0FBVztBQUNULG1CQUFhO0FBREosS0FuRGM7O0FBdUR6QixlQUFjO0FBQ1osaUJBQWEsV0FERDtBQUVaLGdCQUFhLFVBRkQ7QUFHWixjQUFhLFFBSEQ7QUFJWixjQUFhLElBSkQ7QUFLWixlQUFhLFNBTEQ7QUFNWixlQUFhLFNBTkQ7QUFPWixlQUFhLEtBUEQ7QUFRWixrQkFBYSxZQVJEO0FBU1osZUFBYTtBQVRELEtBdkRXOzs7QUFvRXpCLFdBQU87QUFDTCxtQkFBYyxzSEFEVDtBQUVMLGdCQUFjLG9FQUZUO0FBR0wsY0FBYyxzQ0FIVDtBQUlMLGVBQWM7QUFKVDs7QUFwRWtCLEdBQTNCO0FBOEVDLENBdmpDQSxFQXVqQ0csTUF2akNILEVBdWpDVyxNQXZqQ1gsRUF1akNtQixRQXZqQ25CIiwiZmlsZSI6InRyYW5zaXRpb24uanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcbiAqICMgU2VtYW50aWMgVUkgLSBUcmFuc2l0aW9uXG4gKiBodHRwOi8vZ2l0aHViLmNvbS9zZW1hbnRpYy1vcmcvc2VtYW50aWMtdWkvXG4gKlxuICpcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxuICogaHR0cDovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVFxuICpcbiAqL1xuXG47KGZ1bmN0aW9uICgkLCB3aW5kb3csIGRvY3VtZW50LCB1bmRlZmluZWQpIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cbndpbmRvdyA9ICh0eXBlb2Ygd2luZG93ICE9ICd1bmRlZmluZWQnICYmIHdpbmRvdy5NYXRoID09IE1hdGgpXG4gID8gd2luZG93XG4gIDogKHR5cGVvZiBzZWxmICE9ICd1bmRlZmluZWQnICYmIHNlbGYuTWF0aCA9PSBNYXRoKVxuICAgID8gc2VsZlxuICAgIDogRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKVxuO1xuXG4kLmZuLnRyYW5zaXRpb24gPSBmdW5jdGlvbigpIHtcbiAgdmFyXG4gICAgJGFsbE1vZHVsZXMgICAgID0gJCh0aGlzKSxcbiAgICBtb2R1bGVTZWxlY3RvciAgPSAkYWxsTW9kdWxlcy5zZWxlY3RvciB8fCAnJyxcblxuICAgIHRpbWUgICAgICAgICAgICA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpLFxuICAgIHBlcmZvcm1hbmNlICAgICA9IFtdLFxuXG4gICAgbW9kdWxlQXJndW1lbnRzID0gYXJndW1lbnRzLFxuICAgIHF1ZXJ5ICAgICAgICAgICA9IG1vZHVsZUFyZ3VtZW50c1swXSxcbiAgICBxdWVyeUFyZ3VtZW50cyAgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSksXG4gICAgbWV0aG9kSW52b2tlZCAgID0gKHR5cGVvZiBxdWVyeSA9PT0gJ3N0cmluZycpLFxuXG4gICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lID0gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgfHwgd2luZG93Lm1velJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgfHwgd2luZG93LndlYmtpdFJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgfHwgd2luZG93Lm1zUmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gICAgICB8fCBmdW5jdGlvbihjYWxsYmFjaykgeyBzZXRUaW1lb3V0KGNhbGxiYWNrLCAwKTsgfSxcblxuICAgIHJldHVybmVkVmFsdWVcbiAgO1xuICAkYWxsTW9kdWxlc1xuICAgIC5lYWNoKGZ1bmN0aW9uKGluZGV4KSB7XG4gICAgICB2YXJcbiAgICAgICAgJG1vZHVsZSAgPSAkKHRoaXMpLFxuICAgICAgICBlbGVtZW50ICA9IHRoaXMsXG5cbiAgICAgICAgLy8gc2V0IGF0IHJ1biB0aW1lXG4gICAgICAgIHNldHRpbmdzLFxuICAgICAgICBpbnN0YW5jZSxcblxuICAgICAgICBlcnJvcixcbiAgICAgICAgY2xhc3NOYW1lLFxuICAgICAgICBtZXRhZGF0YSxcbiAgICAgICAgYW5pbWF0aW9uRW5kLFxuICAgICAgICBhbmltYXRpb25OYW1lLFxuXG4gICAgICAgIG5hbWVzcGFjZSxcbiAgICAgICAgbW9kdWxlTmFtZXNwYWNlLFxuICAgICAgICBldmVudE5hbWVzcGFjZSxcbiAgICAgICAgbW9kdWxlXG4gICAgICA7XG5cbiAgICAgIG1vZHVsZSA9IHtcblxuICAgICAgICBpbml0aWFsaXplOiBmdW5jdGlvbigpIHtcblxuICAgICAgICAgIC8vIGdldCBmdWxsIHNldHRpbmdzXG4gICAgICAgICAgc2V0dGluZ3MgICAgICAgID0gbW9kdWxlLmdldC5zZXR0aW5ncy5hcHBseShlbGVtZW50LCBtb2R1bGVBcmd1bWVudHMpO1xuXG4gICAgICAgICAgLy8gc2hvcnRoYW5kXG4gICAgICAgICAgY2xhc3NOYW1lICAgICAgID0gc2V0dGluZ3MuY2xhc3NOYW1lO1xuICAgICAgICAgIGVycm9yICAgICAgICAgICA9IHNldHRpbmdzLmVycm9yO1xuICAgICAgICAgIG1ldGFkYXRhICAgICAgICA9IHNldHRpbmdzLm1ldGFkYXRhO1xuXG4gICAgICAgICAgLy8gZGVmaW5lIG5hbWVzcGFjZVxuICAgICAgICAgIGV2ZW50TmFtZXNwYWNlICA9ICcuJyArIHNldHRpbmdzLm5hbWVzcGFjZTtcbiAgICAgICAgICBtb2R1bGVOYW1lc3BhY2UgPSAnbW9kdWxlLScgKyBzZXR0aW5ncy5uYW1lc3BhY2U7XG4gICAgICAgICAgaW5zdGFuY2UgICAgICAgID0gJG1vZHVsZS5kYXRhKG1vZHVsZU5hbWVzcGFjZSkgfHwgbW9kdWxlO1xuXG4gICAgICAgICAgLy8gZ2V0IHZlbmRvciBzcGVjaWZpYyBldmVudHNcbiAgICAgICAgICBhbmltYXRpb25FbmQgICAgPSBtb2R1bGUuZ2V0LmFuaW1hdGlvbkVuZEV2ZW50KCk7XG5cbiAgICAgICAgICBpZihtZXRob2RJbnZva2VkKSB7XG4gICAgICAgICAgICBtZXRob2RJbnZva2VkID0gbW9kdWxlLmludm9rZShxdWVyeSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gbWV0aG9kIG5vdCBpbnZva2VkLCBsZXRzIHJ1biBhbiBhbmltYXRpb25cbiAgICAgICAgICBpZihtZXRob2RJbnZva2VkID09PSBmYWxzZSkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0NvbnZlcnRlZCBhcmd1bWVudHMgaW50byBzZXR0aW5ncyBvYmplY3QnLCBzZXR0aW5ncyk7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5pbnRlcnZhbCkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVsYXkoc2V0dGluZ3MuYW5pbWF0ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlICB7XG4gICAgICAgICAgICAgIG1vZHVsZS5hbmltYXRlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUuaW5zdGFudGlhdGUoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5zdGFudGlhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTdG9yaW5nIGluc3RhbmNlIG9mIG1vZHVsZScsIG1vZHVsZSk7XG4gICAgICAgICAgaW5zdGFuY2UgPSBtb2R1bGU7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLmRhdGEobW9kdWxlTmFtZXNwYWNlLCBpbnN0YW5jZSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVzdHJveTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0Rlc3Ryb3lpbmcgcHJldmlvdXMgbW9kdWxlIGZvcicsIGVsZW1lbnQpO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5yZW1vdmVEYXRhKG1vZHVsZU5hbWVzcGFjZSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVmcmVzaDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1JlZnJlc2hpbmcgZGlzcGxheSB0eXBlIG9uIG5leHQgYW5pbWF0aW9uJyk7XG4gICAgICAgICAgZGVsZXRlIG1vZHVsZS5kaXNwbGF5VHlwZTtcbiAgICAgICAgfSxcblxuICAgICAgICBmb3JjZVJlcGFpbnQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdGb3JjaW5nIGVsZW1lbnQgcmVwYWludCcpO1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgJHBhcmVudEVsZW1lbnQgPSAkbW9kdWxlLnBhcmVudCgpLFxuICAgICAgICAgICAgJG5leHRFbGVtZW50ID0gJG1vZHVsZS5uZXh0KClcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYoJG5leHRFbGVtZW50Lmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgJG1vZHVsZS5kZXRhY2goKS5hcHBlbmRUbygkcGFyZW50RWxlbWVudCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgJG1vZHVsZS5kZXRhY2goKS5pbnNlcnRCZWZvcmUoJG5leHRFbGVtZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVwYWludDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1JlcGFpbnRpbmcgZWxlbWVudCcpO1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgZmFrZUFzc2lnbm1lbnQgPSBlbGVtZW50Lm9mZnNldFdpZHRoXG4gICAgICAgICAgO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlbGF5OiBmdW5jdGlvbihpbnRlcnZhbCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgZGlyZWN0aW9uID0gbW9kdWxlLmdldC5hbmltYXRpb25EaXJlY3Rpb24oKSxcbiAgICAgICAgICAgIHNob3VsZFJldmVyc2UsXG4gICAgICAgICAgICBkZWxheVxuICAgICAgICAgIDtcbiAgICAgICAgICBpZighZGlyZWN0aW9uKSB7XG4gICAgICAgICAgICBkaXJlY3Rpb24gPSBtb2R1bGUuY2FuLnRyYW5zaXRpb24oKVxuICAgICAgICAgICAgICA/IG1vZHVsZS5nZXQuZGlyZWN0aW9uKClcbiAgICAgICAgICAgICAgOiAnc3RhdGljJ1xuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpbnRlcnZhbCA9IChpbnRlcnZhbCAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgPyBpbnRlcnZhbFxuICAgICAgICAgICAgOiBzZXR0aW5ncy5pbnRlcnZhbFxuICAgICAgICAgIDtcbiAgICAgICAgICBzaG91bGRSZXZlcnNlID0gKHNldHRpbmdzLnJldmVyc2UgPT0gJ2F1dG8nICYmIGRpcmVjdGlvbiA9PSBjbGFzc05hbWUub3V0d2FyZCk7XG4gICAgICAgICAgZGVsYXkgPSAoc2hvdWxkUmV2ZXJzZSB8fCBzZXR0aW5ncy5yZXZlcnNlID09IHRydWUpXG4gICAgICAgICAgICA/ICgkYWxsTW9kdWxlcy5sZW5ndGggLSBpbmRleCkgKiBzZXR0aW5ncy5pbnRlcnZhbFxuICAgICAgICAgICAgOiBpbmRleCAqIHNldHRpbmdzLmludGVydmFsXG4gICAgICAgICAgO1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRGVsYXlpbmcgYW5pbWF0aW9uIGJ5JywgZGVsYXkpO1xuICAgICAgICAgIHNldFRpbWVvdXQobW9kdWxlLmFuaW1hdGUsIGRlbGF5KTtcbiAgICAgICAgfSxcblxuICAgICAgICBhbmltYXRlOiBmdW5jdGlvbihvdmVycmlkZVNldHRpbmdzKSB7XG4gICAgICAgICAgc2V0dGluZ3MgPSBvdmVycmlkZVNldHRpbmdzIHx8IHNldHRpbmdzO1xuICAgICAgICAgIGlmKCFtb2R1bGUuaXMuc3VwcG9ydGVkKCkpIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5zdXBwb3J0KTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdQcmVwYXJpbmcgYW5pbWF0aW9uJywgc2V0dGluZ3MuYW5pbWF0aW9uKTtcbiAgICAgICAgICBpZihtb2R1bGUuaXMuYW5pbWF0aW5nKCkpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnF1ZXVlKSB7XG4gICAgICAgICAgICAgIGlmKCFzZXR0aW5ncy5hbGxvd1JlcGVhdHMgJiYgbW9kdWxlLmhhcy5kaXJlY3Rpb24oKSAmJiBtb2R1bGUuaXMub2NjdXJyaW5nKCkgJiYgbW9kdWxlLnF1ZXVpbmcgIT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0FuaW1hdGlvbiBpcyBjdXJyZW50bHkgb2NjdXJyaW5nLCBwcmV2ZW50aW5nIHF1ZXVlaW5nIHNhbWUgYW5pbWF0aW9uJywgc2V0dGluZ3MuYW5pbWF0aW9uKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUucXVldWUoc2V0dGluZ3MuYW5pbWF0aW9uKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKCFzZXR0aW5ncy5hbGxvd1JlcGVhdHMgJiYgbW9kdWxlLmlzLm9jY3VycmluZygpKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQW5pbWF0aW9uIGlzIGFscmVhZHkgb2NjdXJyaW5nLCB3aWxsIG5vdCBleGVjdXRlIHJlcGVhdGVkIGFuaW1hdGlvbicsIHNldHRpbmdzLmFuaW1hdGlvbik7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ05ldyBhbmltYXRpb24gc3RhcnRlZCwgY29tcGxldGluZyBwcmV2aW91cyBlYXJseScsIHNldHRpbmdzLmFuaW1hdGlvbik7XG4gICAgICAgICAgICAgIGluc3RhbmNlLmNvbXBsZXRlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKCBtb2R1bGUuY2FuLmFuaW1hdGUoKSApIHtcbiAgICAgICAgICAgIG1vZHVsZS5zZXQuYW5pbWF0aW5nKHNldHRpbmdzLmFuaW1hdGlvbik7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm5vQW5pbWF0aW9uLCBzZXR0aW5ncy5hbmltYXRpb24sIGVsZW1lbnQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICByZXNldDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZXNldHRpbmcgYW5pbWF0aW9uIHRvIGJlZ2lubmluZyBjb25kaXRpb25zJyk7XG4gICAgICAgICAgbW9kdWxlLnJlbW92ZS5hbmltYXRpb25DYWxsYmFja3MoKTtcbiAgICAgICAgICBtb2R1bGUucmVzdG9yZS5jb25kaXRpb25zKCk7XG4gICAgICAgICAgbW9kdWxlLnJlbW92ZS5hbmltYXRpbmcoKTtcbiAgICAgICAgfSxcblxuICAgICAgICBxdWV1ZTogZnVuY3Rpb24oYW5pbWF0aW9uKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdRdWV1ZWluZyBhbmltYXRpb24gb2YnLCBhbmltYXRpb24pO1xuICAgICAgICAgIG1vZHVsZS5xdWV1aW5nID0gdHJ1ZTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAub25lKGFuaW1hdGlvbkVuZCArICcucXVldWUnICsgZXZlbnROYW1lc3BhY2UsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICBtb2R1bGUucXVldWluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICBtb2R1bGUucmVwYWludCgpO1xuICAgICAgICAgICAgICBtb2R1bGUuYW5pbWF0ZS5hcHBseSh0aGlzLCBzZXR0aW5ncyk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBjb21wbGV0ZTogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdBbmltYXRpb24gY29tcGxldGUnLCBzZXR0aW5ncy5hbmltYXRpb24pO1xuICAgICAgICAgIG1vZHVsZS5yZW1vdmUuY29tcGxldGVDYWxsYmFjaygpO1xuICAgICAgICAgIG1vZHVsZS5yZW1vdmUuZmFpbFNhZmUoKTtcbiAgICAgICAgICBpZighbW9kdWxlLmlzLmxvb3BpbmcoKSkge1xuICAgICAgICAgICAgaWYoIG1vZHVsZS5pcy5vdXR3YXJkKCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdBbmltYXRpb24gaXMgb3V0d2FyZCwgaGlkaW5nIGVsZW1lbnQnKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnJlc3RvcmUuY29uZGl0aW9ucygpO1xuICAgICAgICAgICAgICBtb2R1bGUuaGlkZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiggbW9kdWxlLmlzLmlud2FyZCgpICkge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQW5pbWF0aW9uIGlzIG91dHdhcmQsIHNob3dpbmcgZWxlbWVudCcpO1xuICAgICAgICAgICAgICBtb2R1bGUucmVzdG9yZS5jb25kaXRpb25zKCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5zaG93KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1N0YXRpYyBhbmltYXRpb24gY29tcGxldGVkJyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5yZXN0b3JlLmNvbmRpdGlvbnMoKTtcbiAgICAgICAgICAgICAgc2V0dGluZ3Mub25Db21wbGV0ZS5jYWxsKGVsZW1lbnQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBmb3JjZToge1xuICAgICAgICAgIHZpc2libGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHN0eWxlICAgICAgICAgID0gJG1vZHVsZS5hdHRyKCdzdHlsZScpLFxuICAgICAgICAgICAgICB1c2VyU3R5bGUgICAgICA9IG1vZHVsZS5nZXQudXNlclN0eWxlKCksXG4gICAgICAgICAgICAgIGRpc3BsYXlUeXBlICAgID0gbW9kdWxlLmdldC5kaXNwbGF5VHlwZSgpLFxuICAgICAgICAgICAgICBvdmVycmlkZVN0eWxlICA9IHVzZXJTdHlsZSArICdkaXNwbGF5OiAnICsgZGlzcGxheVR5cGUgKyAnICFpbXBvcnRhbnQ7JyxcbiAgICAgICAgICAgICAgY3VycmVudERpc3BsYXkgPSAkbW9kdWxlLmNzcygnZGlzcGxheScpLFxuICAgICAgICAgICAgICBlbXB0eVN0eWxlICAgICA9IChzdHlsZSA9PT0gdW5kZWZpbmVkIHx8IHN0eWxlID09PSAnJylcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKGN1cnJlbnREaXNwbGF5ICE9PSBkaXNwbGF5VHlwZSkge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnT3ZlcnJpZGluZyBkZWZhdWx0IGRpc3BsYXkgdG8gc2hvdyBlbGVtZW50JywgZGlzcGxheVR5cGUpO1xuICAgICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgICAgLmF0dHIoJ3N0eWxlJywgb3ZlcnJpZGVTdHlsZSlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZihlbXB0eVN0eWxlKSB7XG4gICAgICAgICAgICAgICRtb2R1bGUucmVtb3ZlQXR0cignc3R5bGUnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGhpZGRlbjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgc3R5bGUgICAgICAgICAgPSAkbW9kdWxlLmF0dHIoJ3N0eWxlJyksXG4gICAgICAgICAgICAgIGN1cnJlbnREaXNwbGF5ID0gJG1vZHVsZS5jc3MoJ2Rpc3BsYXknKSxcbiAgICAgICAgICAgICAgZW1wdHlTdHlsZSAgICAgPSAoc3R5bGUgPT09IHVuZGVmaW5lZCB8fCBzdHlsZSA9PT0gJycpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihjdXJyZW50RGlzcGxheSAhPT0gJ25vbmUnICYmICFtb2R1bGUuaXMuaGlkZGVuKCkpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ092ZXJyaWRpbmcgZGVmYXVsdCBkaXNwbGF5IHRvIGhpZGUgZWxlbWVudCcpO1xuICAgICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgICAgLmNzcygnZGlzcGxheScsICdub25lJylcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZihlbXB0eVN0eWxlKSB7XG4gICAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgICAucmVtb3ZlQXR0cignc3R5bGUnKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGhhczoge1xuICAgICAgICAgIGRpcmVjdGlvbjogZnVuY3Rpb24oYW5pbWF0aW9uKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgaGFzRGlyZWN0aW9uID0gZmFsc2VcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGFuaW1hdGlvbiA9IGFuaW1hdGlvbiB8fCBzZXR0aW5ncy5hbmltYXRpb247XG4gICAgICAgICAgICBpZih0eXBlb2YgYW5pbWF0aW9uID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgICBhbmltYXRpb24gPSBhbmltYXRpb24uc3BsaXQoJyAnKTtcbiAgICAgICAgICAgICAgJC5lYWNoKGFuaW1hdGlvbiwgZnVuY3Rpb24oaW5kZXgsIHdvcmQpe1xuICAgICAgICAgICAgICAgIGlmKHdvcmQgPT09IGNsYXNzTmFtZS5pbndhcmQgfHwgd29yZCA9PT0gY2xhc3NOYW1lLm91dHdhcmQpIHtcbiAgICAgICAgICAgICAgICAgIGhhc0RpcmVjdGlvbiA9IHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBoYXNEaXJlY3Rpb247XG4gICAgICAgICAgfSxcbiAgICAgICAgICBpbmxpbmVEaXNwbGF5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBzdHlsZSA9ICRtb2R1bGUuYXR0cignc3R5bGUnKSB8fCAnJ1xuICAgICAgICAgICAgO1xuICAgICAgICAgICAgcmV0dXJuICQuaXNBcnJheShzdHlsZS5tYXRjaCgvZGlzcGxheS4qPzsvLCAnJykpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZXQ6IHtcbiAgICAgICAgICBhbmltYXRpbmc6IGZ1bmN0aW9uKGFuaW1hdGlvbikge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGFuaW1hdGlvbkNsYXNzLFxuICAgICAgICAgICAgICBkaXJlY3Rpb25cbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIC8vIHJlbW92ZSBwcmV2aW91cyBjYWxsYmFja3NcbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUuY29tcGxldGVDYWxsYmFjaygpO1xuXG4gICAgICAgICAgICAvLyBkZXRlcm1pbmUgZXhhY3QgYW5pbWF0aW9uXG4gICAgICAgICAgICBhbmltYXRpb24gICAgICA9IGFuaW1hdGlvbiB8fCBzZXR0aW5ncy5hbmltYXRpb247XG4gICAgICAgICAgICBhbmltYXRpb25DbGFzcyA9IG1vZHVsZS5nZXQuYW5pbWF0aW9uQ2xhc3MoYW5pbWF0aW9uKTtcblxuICAgICAgICAgICAgLy8gc2F2ZSBhbmltYXRpb24gY2xhc3MgaW4gY2FjaGUgdG8gcmVzdG9yZSBjbGFzcyBuYW1lc1xuICAgICAgICAgICAgbW9kdWxlLnNhdmUuYW5pbWF0aW9uKGFuaW1hdGlvbkNsYXNzKTtcblxuICAgICAgICAgICAgLy8gb3ZlcnJpZGUgZGlzcGxheSBpZiBuZWNlc3Nhcnkgc28gYW5pbWF0aW9uIGFwcGVhcnMgdmlzaWJseVxuICAgICAgICAgICAgbW9kdWxlLmZvcmNlLnZpc2libGUoKTtcblxuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5oaWRkZW4oKTtcbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUuZGlyZWN0aW9uKCk7XG5cbiAgICAgICAgICAgIG1vZHVsZS5zdGFydC5hbmltYXRpb24oYW5pbWF0aW9uQ2xhc3MpO1xuXG4gICAgICAgICAgfSxcbiAgICAgICAgICBkdXJhdGlvbjogZnVuY3Rpb24oYW5pbWF0aW9uTmFtZSwgZHVyYXRpb24pIHtcbiAgICAgICAgICAgIGR1cmF0aW9uID0gZHVyYXRpb24gfHwgc2V0dGluZ3MuZHVyYXRpb247XG4gICAgICAgICAgICBkdXJhdGlvbiA9ICh0eXBlb2YgZHVyYXRpb24gPT0gJ251bWJlcicpXG4gICAgICAgICAgICAgID8gZHVyYXRpb24gKyAnbXMnXG4gICAgICAgICAgICAgIDogZHVyYXRpb25cbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKGR1cmF0aW9uIHx8IGR1cmF0aW9uID09PSAwKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIGFuaW1hdGlvbiBkdXJhdGlvbicsIGR1cmF0aW9uKTtcbiAgICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICAgJ2FuaW1hdGlvbi1kdXJhdGlvbic6ICBkdXJhdGlvblxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpcmVjdGlvbjogZnVuY3Rpb24oZGlyZWN0aW9uKSB7XG4gICAgICAgICAgICBkaXJlY3Rpb24gPSBkaXJlY3Rpb24gfHwgbW9kdWxlLmdldC5kaXJlY3Rpb24oKTtcbiAgICAgICAgICAgIGlmKGRpcmVjdGlvbiA9PSBjbGFzc05hbWUuaW53YXJkKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5zZXQuaW53YXJkKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5vdXR3YXJkKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBsb29waW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVHJhbnNpdGlvbiBzZXQgdG8gbG9vcCcpO1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLmxvb3BpbmcpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBoaWRkZW46IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLnRyYW5zaXRpb24pXG4gICAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUuaGlkZGVuKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgaW53YXJkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2V0dGluZyBkaXJlY3Rpb24gdG8gaW53YXJkJyk7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUub3V0d2FyZClcbiAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5pbndhcmQpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBvdXR3YXJkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2V0dGluZyBkaXJlY3Rpb24gdG8gb3V0d2FyZCcpO1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmlud2FyZClcbiAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5vdXR3YXJkKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgdmlzaWJsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUudHJhbnNpdGlvbilcbiAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS52aXNpYmxlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzdGFydDoge1xuICAgICAgICAgIGFuaW1hdGlvbjogZnVuY3Rpb24oYW5pbWF0aW9uQ2xhc3MpIHtcbiAgICAgICAgICAgIGFuaW1hdGlvbkNsYXNzID0gYW5pbWF0aW9uQ2xhc3MgfHwgbW9kdWxlLmdldC5hbmltYXRpb25DbGFzcygpO1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTdGFydGluZyB0d2VlbicsIGFuaW1hdGlvbkNsYXNzKTtcbiAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgLmFkZENsYXNzKGFuaW1hdGlvbkNsYXNzKVxuICAgICAgICAgICAgICAub25lKGFuaW1hdGlvbkVuZCArICcuY29tcGxldGUnICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS5jb21wbGV0ZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnVzZUZhaWxTYWZlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5hZGQuZmFpbFNhZmUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS5zZXQuZHVyYXRpb24oc2V0dGluZ3MuZHVyYXRpb24pO1xuICAgICAgICAgICAgc2V0dGluZ3Mub25TdGFydC5jYWxsKGVsZW1lbnQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzYXZlOiB7XG4gICAgICAgICAgYW5pbWF0aW9uOiBmdW5jdGlvbihhbmltYXRpb24pIHtcbiAgICAgICAgICAgIGlmKCFtb2R1bGUuY2FjaGUpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmNhY2hlID0ge307XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUuY2FjaGUuYW5pbWF0aW9uID0gYW5pbWF0aW9uO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZGlzcGxheVR5cGU6IGZ1bmN0aW9uKGRpc3BsYXlUeXBlKSB7XG4gICAgICAgICAgICBpZihkaXNwbGF5VHlwZSAhPT0gJ25vbmUnKSB7XG4gICAgICAgICAgICAgICRtb2R1bGUuZGF0YShtZXRhZGF0YS5kaXNwbGF5VHlwZSwgZGlzcGxheVR5cGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgdHJhbnNpdGlvbkV4aXN0czogZnVuY3Rpb24oYW5pbWF0aW9uLCBleGlzdHMpIHtcbiAgICAgICAgICAgICQuZm4udHJhbnNpdGlvbi5leGlzdHNbYW5pbWF0aW9uXSA9IGV4aXN0cztcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTYXZpbmcgZXhpc3RlbmNlIG9mIHRyYW5zaXRpb24nLCBhbmltYXRpb24sIGV4aXN0cyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlc3RvcmU6IHtcbiAgICAgICAgICBjb25kaXRpb25zOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBhbmltYXRpb24gPSBtb2R1bGUuZ2V0LmN1cnJlbnRBbmltYXRpb24oKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoYW5pbWF0aW9uKSB7XG4gICAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoYW5pbWF0aW9uKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZW1vdmluZyBhbmltYXRpb24gY2xhc3MnLCBtb2R1bGUuY2FjaGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5kdXJhdGlvbigpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBhZGQ6IHtcbiAgICAgICAgICBmYWlsU2FmZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgZHVyYXRpb24gPSBtb2R1bGUuZ2V0LmR1cmF0aW9uKClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIG1vZHVsZS50aW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICRtb2R1bGUudHJpZ2dlckhhbmRsZXIoYW5pbWF0aW9uRW5kKTtcbiAgICAgICAgICAgIH0sIGR1cmF0aW9uICsgc2V0dGluZ3MuZmFpbFNhZmVEZWxheSk7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQWRkaW5nIGZhaWwgc2FmZSB0aW1lcicsIG1vZHVsZS50aW1lcik7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlbW92ZToge1xuICAgICAgICAgIGFuaW1hdGluZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkbW9kdWxlLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5hbmltYXRpbmcpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYW5pbWF0aW9uQ2FsbGJhY2tzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUucXVldWVDYWxsYmFjaygpO1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5jb21wbGV0ZUNhbGxiYWNrKCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBxdWV1ZUNhbGxiYWNrOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRtb2R1bGUub2ZmKCcucXVldWUnICsgZXZlbnROYW1lc3BhY2UpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgY29tcGxldGVDYWxsYmFjazogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkbW9kdWxlLm9mZignLmNvbXBsZXRlJyArIGV2ZW50TmFtZXNwYWNlKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc3BsYXk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZS5jc3MoJ2Rpc3BsYXknLCAnJyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXJlY3Rpb246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmlud2FyZClcbiAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5vdXR3YXJkKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZHVyYXRpb246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAuY3NzKCdhbmltYXRpb24tZHVyYXRpb24nLCAnJylcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGZhaWxTYWZlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZW1vdmluZyBmYWlsIHNhZmUgdGltZXInLCBtb2R1bGUudGltZXIpO1xuICAgICAgICAgICAgaWYobW9kdWxlLnRpbWVyKSB7XG4gICAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUudGltZXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgaGlkZGVuOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRtb2R1bGUucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmhpZGRlbik7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB2aXNpYmxlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRtb2R1bGUucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLnZpc2libGUpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgbG9vcGluZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1RyYW5zaXRpb25zIGFyZSBubyBsb25nZXIgbG9vcGluZycpO1xuICAgICAgICAgICAgaWYoIG1vZHVsZS5pcy5sb29waW5nKCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5yZXNldCgpO1xuICAgICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5sb29waW5nKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICB0cmFuc2l0aW9uOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS52aXNpYmxlKVxuICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmhpZGRlbilcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGdldDoge1xuICAgICAgICAgIHNldHRpbmdzOiBmdW5jdGlvbihhbmltYXRpb24sIGR1cmF0aW9uLCBvbkNvbXBsZXRlKSB7XG4gICAgICAgICAgICAvLyBzaW5nbGUgc2V0dGluZ3Mgb2JqZWN0XG4gICAgICAgICAgICBpZih0eXBlb2YgYW5pbWF0aW9uID09ICdvYmplY3QnKSB7XG4gICAgICAgICAgICAgIHJldHVybiAkLmV4dGVuZCh0cnVlLCB7fSwgJC5mbi50cmFuc2l0aW9uLnNldHRpbmdzLCBhbmltYXRpb24pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gYWxsIGFyZ3VtZW50cyBwcm92aWRlZFxuICAgICAgICAgICAgZWxzZSBpZih0eXBlb2Ygb25Db21wbGV0ZSA9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgIHJldHVybiAkLmV4dGVuZCh7fSwgJC5mbi50cmFuc2l0aW9uLnNldHRpbmdzLCB7XG4gICAgICAgICAgICAgICAgYW5pbWF0aW9uICA6IGFuaW1hdGlvbixcbiAgICAgICAgICAgICAgICBvbkNvbXBsZXRlIDogb25Db21wbGV0ZSxcbiAgICAgICAgICAgICAgICBkdXJhdGlvbiAgIDogZHVyYXRpb25cbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBvbmx5IGR1cmF0aW9uIHByb3ZpZGVkXG4gICAgICAgICAgICBlbHNlIGlmKHR5cGVvZiBkdXJhdGlvbiA9PSAnc3RyaW5nJyB8fCB0eXBlb2YgZHVyYXRpb24gPT0gJ251bWJlcicpIHtcbiAgICAgICAgICAgICAgcmV0dXJuICQuZXh0ZW5kKHt9LCAkLmZuLnRyYW5zaXRpb24uc2V0dGluZ3MsIHtcbiAgICAgICAgICAgICAgICBhbmltYXRpb24gOiBhbmltYXRpb24sXG4gICAgICAgICAgICAgICAgZHVyYXRpb24gIDogZHVyYXRpb25cbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBkdXJhdGlvbiBpcyBhY3R1YWxseSBzZXR0aW5ncyBvYmplY3RcbiAgICAgICAgICAgIGVsc2UgaWYodHlwZW9mIGR1cmF0aW9uID09ICdvYmplY3QnKSB7XG4gICAgICAgICAgICAgIHJldHVybiAkLmV4dGVuZCh7fSwgJC5mbi50cmFuc2l0aW9uLnNldHRpbmdzLCBkdXJhdGlvbiwge1xuICAgICAgICAgICAgICAgIGFuaW1hdGlvbiA6IGFuaW1hdGlvblxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vIGR1cmF0aW9uIGlzIGFjdHVhbGx5IGNhbGxiYWNrXG4gICAgICAgICAgICBlbHNlIGlmKHR5cGVvZiBkdXJhdGlvbiA9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgIHJldHVybiAkLmV4dGVuZCh7fSwgJC5mbi50cmFuc2l0aW9uLnNldHRpbmdzLCB7XG4gICAgICAgICAgICAgICAgYW5pbWF0aW9uICA6IGFuaW1hdGlvbixcbiAgICAgICAgICAgICAgICBvbkNvbXBsZXRlIDogZHVyYXRpb25cbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBvbmx5IGFuaW1hdGlvbiBwcm92aWRlZFxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHJldHVybiAkLmV4dGVuZCh7fSwgJC5mbi50cmFuc2l0aW9uLnNldHRpbmdzLCB7XG4gICAgICAgICAgICAgICAgYW5pbWF0aW9uIDogYW5pbWF0aW9uXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuICQuZm4udHJhbnNpdGlvbi5zZXR0aW5ncztcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFuaW1hdGlvbkNsYXNzOiBmdW5jdGlvbihhbmltYXRpb24pIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBhbmltYXRpb25DbGFzcyA9IGFuaW1hdGlvbiB8fCBzZXR0aW5ncy5hbmltYXRpb24sXG4gICAgICAgICAgICAgIGRpcmVjdGlvbkNsYXNzID0gKG1vZHVsZS5jYW4udHJhbnNpdGlvbigpICYmICFtb2R1bGUuaGFzLmRpcmVjdGlvbigpKVxuICAgICAgICAgICAgICAgID8gbW9kdWxlLmdldC5kaXJlY3Rpb24oKSArICcgJ1xuICAgICAgICAgICAgICAgIDogJydcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHJldHVybiBjbGFzc05hbWUuYW5pbWF0aW5nICsgJyAnXG4gICAgICAgICAgICAgICsgY2xhc3NOYW1lLnRyYW5zaXRpb24gKyAnICdcbiAgICAgICAgICAgICAgKyBkaXJlY3Rpb25DbGFzc1xuICAgICAgICAgICAgICArIGFuaW1hdGlvbkNsYXNzXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBjdXJyZW50QW5pbWF0aW9uOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAobW9kdWxlLmNhY2hlICYmIG1vZHVsZS5jYWNoZS5hbmltYXRpb24gIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgPyBtb2R1bGUuY2FjaGUuYW5pbWF0aW9uXG4gICAgICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGN1cnJlbnREaXJlY3Rpb246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZS5pcy5pbndhcmQoKVxuICAgICAgICAgICAgICA/IGNsYXNzTmFtZS5pbndhcmRcbiAgICAgICAgICAgICAgOiBjbGFzc05hbWUub3V0d2FyZFxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZGlyZWN0aW9uOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGUuaXMuaGlkZGVuKCkgfHwgIW1vZHVsZS5pcy52aXNpYmxlKClcbiAgICAgICAgICAgICAgPyBjbGFzc05hbWUuaW53YXJkXG4gICAgICAgICAgICAgIDogY2xhc3NOYW1lLm91dHdhcmRcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFuaW1hdGlvbkRpcmVjdGlvbjogZnVuY3Rpb24oYW5pbWF0aW9uKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgZGlyZWN0aW9uXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBhbmltYXRpb24gPSBhbmltYXRpb24gfHwgc2V0dGluZ3MuYW5pbWF0aW9uO1xuICAgICAgICAgICAgaWYodHlwZW9mIGFuaW1hdGlvbiA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgYW5pbWF0aW9uID0gYW5pbWF0aW9uLnNwbGl0KCcgJyk7XG4gICAgICAgICAgICAgIC8vIHNlYXJjaCBhbmltYXRpb24gbmFtZSBmb3Igb3V0L2luIGNsYXNzXG4gICAgICAgICAgICAgICQuZWFjaChhbmltYXRpb24sIGZ1bmN0aW9uKGluZGV4LCB3b3JkKXtcbiAgICAgICAgICAgICAgICBpZih3b3JkID09PSBjbGFzc05hbWUuaW53YXJkKSB7XG4gICAgICAgICAgICAgICAgICBkaXJlY3Rpb24gPSBjbGFzc05hbWUuaW53YXJkO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmKHdvcmQgPT09IGNsYXNzTmFtZS5vdXR3YXJkKSB7XG4gICAgICAgICAgICAgICAgICBkaXJlY3Rpb24gPSBjbGFzc05hbWUub3V0d2FyZDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gcmV0dXJuIGZvdW5kIGRpcmVjdGlvblxuICAgICAgICAgICAgaWYoZGlyZWN0aW9uKSB7XG4gICAgICAgICAgICAgIHJldHVybiBkaXJlY3Rpb247XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkdXJhdGlvbjogZnVuY3Rpb24oZHVyYXRpb24pIHtcbiAgICAgICAgICAgIGR1cmF0aW9uID0gZHVyYXRpb24gfHwgc2V0dGluZ3MuZHVyYXRpb247XG4gICAgICAgICAgICBpZihkdXJhdGlvbiA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgZHVyYXRpb24gPSAkbW9kdWxlLmNzcygnYW5pbWF0aW9uLWR1cmF0aW9uJykgfHwgMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiAodHlwZW9mIGR1cmF0aW9uID09PSAnc3RyaW5nJylcbiAgICAgICAgICAgICAgPyAoZHVyYXRpb24uaW5kZXhPZignbXMnKSA+IC0xKVxuICAgICAgICAgICAgICAgID8gcGFyc2VGbG9hdChkdXJhdGlvbilcbiAgICAgICAgICAgICAgICA6IHBhcnNlRmxvYXQoZHVyYXRpb24pICogMTAwMFxuICAgICAgICAgICAgICA6IGR1cmF0aW9uXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXNwbGF5VHlwZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5kaXNwbGF5VHlwZSkge1xuICAgICAgICAgICAgICByZXR1cm4gc2V0dGluZ3MuZGlzcGxheVR5cGU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZigkbW9kdWxlLmRhdGEobWV0YWRhdGEuZGlzcGxheVR5cGUpID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgLy8gY3JlYXRlIGZha2UgZWxlbWVudCB0byBkZXRlcm1pbmUgZGlzcGxheSBzdGF0ZVxuICAgICAgICAgICAgICBtb2R1bGUuY2FuLnRyYW5zaXRpb24odHJ1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5kYXRhKG1ldGFkYXRhLmRpc3BsYXlUeXBlKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHVzZXJTdHlsZTogZnVuY3Rpb24oc3R5bGUpIHtcbiAgICAgICAgICAgIHN0eWxlID0gc3R5bGUgfHwgJG1vZHVsZS5hdHRyKCdzdHlsZScpIHx8ICcnO1xuICAgICAgICAgICAgcmV0dXJuIHN0eWxlLnJlcGxhY2UoL2Rpc3BsYXkuKj87LywgJycpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgdHJhbnNpdGlvbkV4aXN0czogZnVuY3Rpb24oYW5pbWF0aW9uKSB7XG4gICAgICAgICAgICByZXR1cm4gJC5mbi50cmFuc2l0aW9uLmV4aXN0c1thbmltYXRpb25dO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYW5pbWF0aW9uU3RhcnRFdmVudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgZWxlbWVudCAgICAgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKSxcbiAgICAgICAgICAgICAgYW5pbWF0aW9ucyAgPSB7XG4gICAgICAgICAgICAgICAgJ2FuaW1hdGlvbicgICAgICAgOidhbmltYXRpb25zdGFydCcsXG4gICAgICAgICAgICAgICAgJ09BbmltYXRpb24nICAgICAgOidvQW5pbWF0aW9uU3RhcnQnLFxuICAgICAgICAgICAgICAgICdNb3pBbmltYXRpb24nICAgIDonbW96QW5pbWF0aW9uU3RhcnQnLFxuICAgICAgICAgICAgICAgICdXZWJraXRBbmltYXRpb24nIDond2Via2l0QW5pbWF0aW9uU3RhcnQnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGFuaW1hdGlvblxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgZm9yKGFuaW1hdGlvbiBpbiBhbmltYXRpb25zKXtcbiAgICAgICAgICAgICAgaWYoIGVsZW1lbnQuc3R5bGVbYW5pbWF0aW9uXSAhPT0gdW5kZWZpbmVkICl7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGFuaW1hdGlvbnNbYW5pbWF0aW9uXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYW5pbWF0aW9uRW5kRXZlbnQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGVsZW1lbnQgICAgID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JyksXG4gICAgICAgICAgICAgIGFuaW1hdGlvbnMgID0ge1xuICAgICAgICAgICAgICAgICdhbmltYXRpb24nICAgICAgIDonYW5pbWF0aW9uZW5kJyxcbiAgICAgICAgICAgICAgICAnT0FuaW1hdGlvbicgICAgICA6J29BbmltYXRpb25FbmQnLFxuICAgICAgICAgICAgICAgICdNb3pBbmltYXRpb24nICAgIDonbW96QW5pbWF0aW9uRW5kJyxcbiAgICAgICAgICAgICAgICAnV2Via2l0QW5pbWF0aW9uJyA6J3dlYmtpdEFuaW1hdGlvbkVuZCdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgYW5pbWF0aW9uXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBmb3IoYW5pbWF0aW9uIGluIGFuaW1hdGlvbnMpe1xuICAgICAgICAgICAgICBpZiggZWxlbWVudC5zdHlsZVthbmltYXRpb25dICE9PSB1bmRlZmluZWQgKXtcbiAgICAgICAgICAgICAgICByZXR1cm4gYW5pbWF0aW9uc1thbmltYXRpb25dO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgfVxuXG4gICAgICAgIH0sXG5cbiAgICAgICAgY2FuOiB7XG4gICAgICAgICAgdHJhbnNpdGlvbjogZnVuY3Rpb24oZm9yY2VkKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgYW5pbWF0aW9uICAgICAgICAgPSBzZXR0aW5ncy5hbmltYXRpb24sXG4gICAgICAgICAgICAgIHRyYW5zaXRpb25FeGlzdHMgID0gbW9kdWxlLmdldC50cmFuc2l0aW9uRXhpc3RzKGFuaW1hdGlvbiksXG4gICAgICAgICAgICAgIGVsZW1lbnRDbGFzcyxcbiAgICAgICAgICAgICAgdGFnTmFtZSxcbiAgICAgICAgICAgICAgJGNsb25lLFxuICAgICAgICAgICAgICBjdXJyZW50QW5pbWF0aW9uLFxuICAgICAgICAgICAgICBpbkFuaW1hdGlvbixcbiAgICAgICAgICAgICAgZGlyZWN0aW9uRXhpc3RzLFxuICAgICAgICAgICAgICBkaXNwbGF5VHlwZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoIHRyYW5zaXRpb25FeGlzdHMgPT09IHVuZGVmaW5lZCB8fCBmb3JjZWQpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0RldGVybWluaW5nIHdoZXRoZXIgYW5pbWF0aW9uIGV4aXN0cycpO1xuICAgICAgICAgICAgICBlbGVtZW50Q2xhc3MgPSAkbW9kdWxlLmF0dHIoJ2NsYXNzJyk7XG4gICAgICAgICAgICAgIHRhZ05hbWUgICAgICA9ICRtb2R1bGUucHJvcCgndGFnTmFtZScpO1xuXG4gICAgICAgICAgICAgICRjbG9uZSA9ICQoJzwnICsgdGFnTmFtZSArICcgLz4nKS5hZGRDbGFzcyggZWxlbWVudENsYXNzICkuaW5zZXJ0QWZ0ZXIoJG1vZHVsZSk7XG4gICAgICAgICAgICAgIGN1cnJlbnRBbmltYXRpb24gPSAkY2xvbmVcbiAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoYW5pbWF0aW9uKVxuICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuaW53YXJkKVxuICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUub3V0d2FyZClcbiAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLmFuaW1hdGluZylcbiAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLnRyYW5zaXRpb24pXG4gICAgICAgICAgICAgICAgLmNzcygnYW5pbWF0aW9uTmFtZScpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgaW5BbmltYXRpb24gPSAkY2xvbmVcbiAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLmlud2FyZClcbiAgICAgICAgICAgICAgICAuY3NzKCdhbmltYXRpb25OYW1lJylcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBkaXNwbGF5VHlwZSA9ICRjbG9uZVxuICAgICAgICAgICAgICAgIC5hdHRyKCdjbGFzcycsIGVsZW1lbnRDbGFzcylcbiAgICAgICAgICAgICAgICAucmVtb3ZlQXR0cignc3R5bGUnKVxuICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuaGlkZGVuKVxuICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUudmlzaWJsZSlcbiAgICAgICAgICAgICAgICAuc2hvdygpXG4gICAgICAgICAgICAgICAgLmNzcygnZGlzcGxheScpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0RldGVybWluaW5nIGZpbmFsIGRpc3BsYXkgc3RhdGUnLCBkaXNwbGF5VHlwZSk7XG4gICAgICAgICAgICAgIG1vZHVsZS5zYXZlLmRpc3BsYXlUeXBlKGRpc3BsYXlUeXBlKTtcblxuICAgICAgICAgICAgICAkY2xvbmUucmVtb3ZlKCk7XG4gICAgICAgICAgICAgIGlmKGN1cnJlbnRBbmltYXRpb24gIT0gaW5BbmltYXRpb24pIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0RpcmVjdGlvbiBleGlzdHMgZm9yIGFuaW1hdGlvbicsIGFuaW1hdGlvbik7XG4gICAgICAgICAgICAgICAgZGlyZWN0aW9uRXhpc3RzID0gdHJ1ZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKGN1cnJlbnRBbmltYXRpb24gPT0gJ25vbmUnIHx8ICFjdXJyZW50QW5pbWF0aW9uKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdObyBhbmltYXRpb24gZGVmaW5lZCBpbiBjc3MnLCBhbmltYXRpb24pO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1N0YXRpYyBhbmltYXRpb24gZm91bmQnLCBhbmltYXRpb24sIGRpc3BsYXlUeXBlKTtcbiAgICAgICAgICAgICAgICBkaXJlY3Rpb25FeGlzdHMgPSBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBtb2R1bGUuc2F2ZS50cmFuc2l0aW9uRXhpc3RzKGFuaW1hdGlvbiwgZGlyZWN0aW9uRXhpc3RzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiAodHJhbnNpdGlvbkV4aXN0cyAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICA/IHRyYW5zaXRpb25FeGlzdHNcbiAgICAgICAgICAgICAgOiBkaXJlY3Rpb25FeGlzdHNcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFuaW1hdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgLy8gY2FuIHRyYW5zaXRpb24gZG9lcyBub3QgcmV0dXJuIGEgdmFsdWUgaWYgYW5pbWF0aW9uIGRvZXMgbm90IGV4aXN0XG4gICAgICAgICAgICByZXR1cm4gKG1vZHVsZS5jYW4udHJhbnNpdGlvbigpICE9PSB1bmRlZmluZWQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBpczoge1xuICAgICAgICAgIGFuaW1hdGluZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5oYXNDbGFzcyhjbGFzc05hbWUuYW5pbWF0aW5nKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGlud2FyZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5oYXNDbGFzcyhjbGFzc05hbWUuaW53YXJkKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIG91dHdhcmQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRtb2R1bGUuaGFzQ2xhc3MoY2xhc3NOYW1lLm91dHdhcmQpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgbG9vcGluZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5oYXNDbGFzcyhjbGFzc05hbWUubG9vcGluZyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBvY2N1cnJpbmc6IGZ1bmN0aW9uKGFuaW1hdGlvbikge1xuICAgICAgICAgICAgYW5pbWF0aW9uID0gYW5pbWF0aW9uIHx8IHNldHRpbmdzLmFuaW1hdGlvbjtcbiAgICAgICAgICAgIGFuaW1hdGlvbiA9ICcuJyArIGFuaW1hdGlvbi5yZXBsYWNlKCcgJywgJy4nKTtcbiAgICAgICAgICAgIHJldHVybiAoICRtb2R1bGUuZmlsdGVyKGFuaW1hdGlvbikubGVuZ3RoID4gMCApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgdmlzaWJsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5pcygnOnZpc2libGUnKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGhpZGRlbjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5jc3MoJ3Zpc2liaWxpdHknKSA9PT0gJ2hpZGRlbic7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzdXBwb3J0ZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuKGFuaW1hdGlvbkVuZCAhPT0gZmFsc2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBoaWRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnSGlkaW5nIGVsZW1lbnQnKTtcbiAgICAgICAgICBpZiggbW9kdWxlLmlzLmFuaW1hdGluZygpICkge1xuICAgICAgICAgICAgbW9kdWxlLnJlc2V0KCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsZW1lbnQuYmx1cigpOyAvLyBJRSB3aWxsIHRyaWdnZXIgZm9jdXMgY2hhbmdlIGlmIGVsZW1lbnQgaXMgbm90IGJsdXJyZWQgYmVmb3JlIGhpZGluZ1xuICAgICAgICAgIG1vZHVsZS5yZW1vdmUuZGlzcGxheSgpO1xuICAgICAgICAgIG1vZHVsZS5yZW1vdmUudmlzaWJsZSgpO1xuICAgICAgICAgIG1vZHVsZS5zZXQuaGlkZGVuKCk7XG4gICAgICAgICAgbW9kdWxlLmZvcmNlLmhpZGRlbigpO1xuICAgICAgICAgIHNldHRpbmdzLm9uSGlkZS5jYWxsKGVsZW1lbnQpO1xuICAgICAgICAgIHNldHRpbmdzLm9uQ29tcGxldGUuY2FsbChlbGVtZW50KTtcbiAgICAgICAgICAvLyBtb2R1bGUucmVwYWludCgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHNob3c6IGZ1bmN0aW9uKGRpc3BsYXkpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU2hvd2luZyBlbGVtZW50JywgZGlzcGxheSk7XG4gICAgICAgICAgbW9kdWxlLnJlbW92ZS5oaWRkZW4oKTtcbiAgICAgICAgICBtb2R1bGUuc2V0LnZpc2libGUoKTtcbiAgICAgICAgICBtb2R1bGUuZm9yY2UudmlzaWJsZSgpO1xuICAgICAgICAgIHNldHRpbmdzLm9uU2hvdy5jYWxsKGVsZW1lbnQpO1xuICAgICAgICAgIHNldHRpbmdzLm9uQ29tcGxldGUuY2FsbChlbGVtZW50KTtcbiAgICAgICAgICAvLyBtb2R1bGUucmVwYWludCgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHRvZ2dsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIG1vZHVsZS5pcy52aXNpYmxlKCkgKSB7XG4gICAgICAgICAgICBtb2R1bGUuaGlkZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIG1vZHVsZS5zaG93KCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHN0b3A6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU3RvcHBpbmcgY3VycmVudCBhbmltYXRpb24nKTtcbiAgICAgICAgICAkbW9kdWxlLnRyaWdnZXJIYW5kbGVyKGFuaW1hdGlvbkVuZCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc3RvcEFsbDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdTdG9wcGluZyBhbGwgYW5pbWF0aW9uJyk7XG4gICAgICAgICAgbW9kdWxlLnJlbW92ZS5xdWV1ZUNhbGxiYWNrKCk7XG4gICAgICAgICAgJG1vZHVsZS50cmlnZ2VySGFuZGxlcihhbmltYXRpb25FbmQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNsZWFyOiB7XG4gICAgICAgICAgcXVldWU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDbGVhcmluZyBhbmltYXRpb24gcXVldWUnKTtcbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUucXVldWVDYWxsYmFjaygpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBlbmFibGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTdGFydGluZyBhbmltYXRpb24nKTtcbiAgICAgICAgICAkbW9kdWxlLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5kaXNhYmxlZCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGlzYWJsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdTdG9wcGluZyBhbmltYXRpb24nKTtcbiAgICAgICAgICAkbW9kdWxlLmFkZENsYXNzKGNsYXNzTmFtZS5kaXNhYmxlZCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0dGluZzogZnVuY3Rpb24obmFtZSwgdmFsdWUpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NoYW5naW5nIHNldHRpbmcnLCBuYW1lLCB2YWx1ZSk7XG4gICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChuYW1lKSApIHtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIHNldHRpbmdzLCBuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBpZigkLmlzUGxhaW5PYmplY3Qoc2V0dGluZ3NbbmFtZV0pKSB7XG4gICAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIHNldHRpbmdzW25hbWVdLCB2YWx1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgc2V0dGluZ3NbbmFtZV0gPSB2YWx1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gc2V0dGluZ3NbbmFtZV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnRlcm5hbDogZnVuY3Rpb24obmFtZSwgdmFsdWUpIHtcbiAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgbW9kdWxlLCBuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBtb2R1bGVbbmFtZV0gPSB2YWx1ZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZGVidWc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1Zy5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdmVyYm9zZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy52ZXJib3NlICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UubG9nKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBlcnJvcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCkge1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmVycm9yLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvci5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgcGVyZm9ybWFuY2U6IHtcbiAgICAgICAgICBsb2c6IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBjdXJyZW50VGltZSxcbiAgICAgICAgICAgICAgZXhlY3V0aW9uVGltZSxcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBjdXJyZW50VGltZSAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG4gICAgICAgICAgICAgIHByZXZpb3VzVGltZSAgPSB0aW1lIHx8IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lID0gY3VycmVudFRpbWUgLSBwcmV2aW91c1RpbWU7XG4gICAgICAgICAgICAgIHRpbWUgICAgICAgICAgPSBjdXJyZW50VGltZTtcbiAgICAgICAgICAgICAgcGVyZm9ybWFuY2UucHVzaCh7XG4gICAgICAgICAgICAgICAgJ05hbWUnICAgICAgICAgICA6IG1lc3NhZ2VbMF0sXG4gICAgICAgICAgICAgICAgJ0FyZ3VtZW50cycgICAgICA6IFtdLnNsaWNlLmNhbGwobWVzc2FnZSwgMSkgfHwgJycsXG4gICAgICAgICAgICAgICAgJ0VsZW1lbnQnICAgICAgICA6IGVsZW1lbnQsXG4gICAgICAgICAgICAgICAgJ0V4ZWN1dGlvbiBUaW1lJyA6IGV4ZWN1dGlvblRpbWVcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lciA9IHNldFRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLmRpc3BsYXksIDUwMCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXNwbGF5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICB0aXRsZSA9IHNldHRpbmdzLm5hbWUgKyAnOicsXG4gICAgICAgICAgICAgIHRvdGFsVGltZSA9IDBcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHRpbWUgPSBmYWxzZTtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICB0b3RhbFRpbWUgKz0gZGF0YVsnRXhlY3V0aW9uIFRpbWUnXTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgdGl0bGUgKz0gJyAnICsgdG90YWxUaW1lICsgJ21zJztcbiAgICAgICAgICAgIGlmKG1vZHVsZVNlbGVjdG9yKSB7XG4gICAgICAgICAgICAgIHRpdGxlICs9ICcgXFwnJyArIG1vZHVsZVNlbGVjdG9yICsgJ1xcJyc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZigkYWxsTW9kdWxlcy5sZW5ndGggPiAxKSB7XG4gICAgICAgICAgICAgIHRpdGxlICs9ICcgJyArICcoJyArICRhbGxNb2R1bGVzLmxlbmd0aCArICcpJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCAoY29uc29sZS5ncm91cCAhPT0gdW5kZWZpbmVkIHx8IGNvbnNvbGUudGFibGUgIT09IHVuZGVmaW5lZCkgJiYgcGVyZm9ybWFuY2UubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKHRpdGxlKTtcbiAgICAgICAgICAgICAgaWYoY29uc29sZS50YWJsZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUudGFibGUocGVyZm9ybWFuY2UpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGFbJ05hbWUnXSArICc6ICcgKyBkYXRhWydFeGVjdXRpb24gVGltZSddKydtcycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHBlcmZvcm1hbmNlID0gW107XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICAvLyBtb2RpZmllZCBmb3IgdHJhbnNpdGlvbiB0byByZXR1cm4gaW52b2tlIHN1Y2Nlc3NcbiAgICAgICAgaW52b2tlOiBmdW5jdGlvbihxdWVyeSwgcGFzc2VkQXJndW1lbnRzLCBjb250ZXh0KSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICBvYmplY3QgPSBpbnN0YW5jZSxcbiAgICAgICAgICAgIG1heERlcHRoLFxuICAgICAgICAgICAgZm91bmQsXG4gICAgICAgICAgICByZXNwb25zZVxuICAgICAgICAgIDtcbiAgICAgICAgICBwYXNzZWRBcmd1bWVudHMgPSBwYXNzZWRBcmd1bWVudHMgfHwgcXVlcnlBcmd1bWVudHM7XG4gICAgICAgICAgY29udGV4dCAgICAgICAgID0gZWxlbWVudCAgICAgICAgIHx8IGNvbnRleHQ7XG4gICAgICAgICAgaWYodHlwZW9mIHF1ZXJ5ID09ICdzdHJpbmcnICYmIG9iamVjdCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBxdWVyeSAgICA9IHF1ZXJ5LnNwbGl0KC9bXFwuIF0vKTtcbiAgICAgICAgICAgIG1heERlcHRoID0gcXVlcnkubGVuZ3RoIC0gMTtcbiAgICAgICAgICAgICQuZWFjaChxdWVyeSwgZnVuY3Rpb24oZGVwdGgsIHZhbHVlKSB7XG4gICAgICAgICAgICAgIHZhciBjYW1lbENhc2VWYWx1ZSA9IChkZXB0aCAhPSBtYXhEZXB0aClcbiAgICAgICAgICAgICAgICA/IHZhbHVlICsgcXVlcnlbZGVwdGggKyAxXS5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIHF1ZXJ5W2RlcHRoICsgMV0uc2xpY2UoMSlcbiAgICAgICAgICAgICAgICA6IHF1ZXJ5XG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdCggb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXSApICYmIChkZXB0aCAhPSBtYXhEZXB0aCkgKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBvYmplY3RbY2FtZWxDYXNlVmFsdWVdICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCAkLmlzUGxhaW5PYmplY3QoIG9iamVjdFt2YWx1ZV0gKSAmJiAoZGVwdGggIT0gbWF4RGVwdGgpICkge1xuICAgICAgICAgICAgICAgIG9iamVjdCA9IG9iamVjdFt2YWx1ZV07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggb2JqZWN0W3ZhbHVlXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgICAgIGZvdW5kID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCAkLmlzRnVuY3Rpb24oIGZvdW5kICkgKSB7XG4gICAgICAgICAgICByZXNwb25zZSA9IGZvdW5kLmFwcGx5KGNvbnRleHQsIHBhc3NlZEFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYoZm91bmQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZigkLmlzQXJyYXkocmV0dXJuZWRWYWx1ZSkpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUucHVzaChyZXNwb25zZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYocmV0dXJuZWRWYWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlID0gW3JldHVybmVkVmFsdWUsIHJlc3BvbnNlXTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihyZXNwb25zZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlID0gcmVzcG9uc2U7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiAoZm91bmQgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgID8gZm91bmRcbiAgICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgICA7XG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgIH0pXG4gIDtcbiAgcmV0dXJuIChyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpXG4gICAgPyByZXR1cm5lZFZhbHVlXG4gICAgOiB0aGlzXG4gIDtcbn07XG5cbi8vIFJlY29yZHMgaWYgQ1NTIHRyYW5zaXRpb24gaXMgYXZhaWxhYmxlXG4kLmZuLnRyYW5zaXRpb24uZXhpc3RzID0ge307XG5cbiQuZm4udHJhbnNpdGlvbi5zZXR0aW5ncyA9IHtcblxuICAvLyBtb2R1bGUgaW5mb1xuICBuYW1lICAgICAgICAgIDogJ1RyYW5zaXRpb24nLFxuXG4gIC8vIGhpZGUgYWxsIG91dHB1dCBmcm9tIHRoaXMgY29tcG9uZW50IHJlZ2FyZGxlc3Mgb2Ygb3RoZXIgc2V0dGluZ3NcbiAgc2lsZW50ICAgICAgICA6IGZhbHNlLFxuXG4gIC8vIGRlYnVnIGNvbnRlbnQgb3V0cHV0dGVkIHRvIGNvbnNvbGVcbiAgZGVidWcgICAgICAgICA6IGZhbHNlLFxuXG4gIC8vIHZlcmJvc2UgZGVidWcgb3V0cHV0XG4gIHZlcmJvc2UgICAgICAgOiBmYWxzZSxcblxuICAvLyBwZXJmb3JtYW5jZSBkYXRhIG91dHB1dFxuICBwZXJmb3JtYW5jZSAgIDogdHJ1ZSxcblxuICAvLyBldmVudCBuYW1lc3BhY2VcbiAgbmFtZXNwYWNlICAgICA6ICd0cmFuc2l0aW9uJyxcblxuICAvLyBkZWxheSBiZXR3ZWVuIGFuaW1hdGlvbnMgaW4gZ3JvdXBcbiAgaW50ZXJ2YWwgICAgICA6IDAsXG5cbiAgLy8gd2hldGhlciBncm91cCBhbmltYXRpb25zIHNob3VsZCBiZSByZXZlcnNlZFxuICByZXZlcnNlICAgICAgIDogJ2F1dG8nLFxuXG4gIC8vIGFuaW1hdGlvbiBjYWxsYmFjayBldmVudFxuICBvblN0YXJ0ICAgICAgIDogZnVuY3Rpb24oKSB7fSxcbiAgb25Db21wbGV0ZSAgICA6IGZ1bmN0aW9uKCkge30sXG4gIG9uU2hvdyAgICAgICAgOiBmdW5jdGlvbigpIHt9LFxuICBvbkhpZGUgICAgICAgIDogZnVuY3Rpb24oKSB7fSxcblxuICAvLyB3aGV0aGVyIHRpbWVvdXQgc2hvdWxkIGJlIHVzZWQgdG8gZW5zdXJlIGNhbGxiYWNrIGZpcmVzIGluIGNhc2VzIGFuaW1hdGlvbmVuZCBkb2VzIG5vdFxuICB1c2VGYWlsU2FmZSAgIDogdHJ1ZSxcblxuICAvLyBkZWxheSBpbiBtcyBmb3IgZmFpbCBzYWZlXG4gIGZhaWxTYWZlRGVsYXkgOiAxMDAsXG5cbiAgLy8gd2hldGhlciBFWEFDVCBhbmltYXRpb24gY2FuIG9jY3VyIHR3aWNlIGluIGEgcm93XG4gIGFsbG93UmVwZWF0cyAgOiBmYWxzZSxcblxuICAvLyBPdmVycmlkZSBmaW5hbCBkaXNwbGF5IHR5cGUgb24gdmlzaWJsZVxuICBkaXNwbGF5VHlwZSAgIDogZmFsc2UsXG5cbiAgLy8gYW5pbWF0aW9uIGR1cmF0aW9uXG4gIGFuaW1hdGlvbiAgICAgOiAnZmFkZScsXG4gIGR1cmF0aW9uICAgICAgOiBmYWxzZSxcblxuICAvLyBuZXcgYW5pbWF0aW9ucyB3aWxsIG9jY3VyIGFmdGVyIHByZXZpb3VzIG9uZXNcbiAgcXVldWUgICAgICAgICA6IHRydWUsXG5cbiAgbWV0YWRhdGEgOiB7XG4gICAgZGlzcGxheVR5cGU6ICdkaXNwbGF5J1xuICB9LFxuXG4gIGNsYXNzTmFtZSAgIDoge1xuICAgIGFuaW1hdGluZyAgOiAnYW5pbWF0aW5nJyxcbiAgICBkaXNhYmxlZCAgIDogJ2Rpc2FibGVkJyxcbiAgICBoaWRkZW4gICAgIDogJ2hpZGRlbicsXG4gICAgaW53YXJkICAgICA6ICdpbicsXG4gICAgbG9hZGluZyAgICA6ICdsb2FkaW5nJyxcbiAgICBsb29waW5nICAgIDogJ2xvb3BpbmcnLFxuICAgIG91dHdhcmQgICAgOiAnb3V0JyxcbiAgICB0cmFuc2l0aW9uIDogJ3RyYW5zaXRpb24nLFxuICAgIHZpc2libGUgICAgOiAndmlzaWJsZSdcbiAgfSxcblxuICAvLyBwb3NzaWJsZSBlcnJvcnNcbiAgZXJyb3I6IHtcbiAgICBub0FuaW1hdGlvbiA6ICdFbGVtZW50IGlzIG5vIGxvbmdlciBhdHRhY2hlZCB0byBET00uIFVuYWJsZSB0byBhbmltYXRlLiAgVXNlIHNpbGVudCBzZXR0aW5nIHRvIHN1cnByZXNzIHRoaXMgd2FybmluZyBpbiBwcm9kdWN0aW9uLicsXG4gICAgcmVwZWF0ZWQgICAgOiAnVGhhdCBhbmltYXRpb24gaXMgYWxyZWFkeSBvY2N1cnJpbmcsIGNhbmNlbGxpbmcgcmVwZWF0ZWQgYW5pbWF0aW9uJyxcbiAgICBtZXRob2QgICAgICA6ICdUaGUgbWV0aG9kIHlvdSBjYWxsZWQgaXMgbm90IGRlZmluZWQnLFxuICAgIHN1cHBvcnQgICAgIDogJ1RoaXMgYnJvd3NlciBkb2VzIG5vdCBzdXBwb3J0IENTUyBhbmltYXRpb25zJ1xuICB9XG5cbn07XG5cblxufSkoIGpRdWVyeSwgd2luZG93LCBkb2N1bWVudCApO1xuIl19