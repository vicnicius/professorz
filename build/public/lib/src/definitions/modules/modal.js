/*!
 * # Semantic UI - Modal
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.modal = function (parameters) {
    var $allModules = $(this),
        $window = $(window),
        $document = $(document),
        $body = $('body'),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
      setTimeout(callback, 0);
    },
        returnedValue;

    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.modal.settings, parameters) : $.extend({}, $.fn.modal.settings),
          selector = settings.selector,
          className = settings.className,
          namespace = settings.namespace,
          error = settings.error,
          eventNamespace = '.' + namespace,
          moduleNamespace = 'module-' + namespace,
          $module = $(this),
          $context = $(settings.context),
          $close = $module.find(selector.close),
          $allModals,
          $otherModals,
          $focusedElement,
          $dimmable,
          $dimmer,
          element = this,
          instance = $module.data(moduleNamespace),
          elementEventNamespace,
          id,
          observer,
          module;
      module = {

        initialize: function () {
          module.verbose('Initializing dimmer', $context);

          module.create.id();
          module.create.dimmer();
          module.refreshModals();

          module.bind.events();
          if (settings.observeChanges) {
            module.observeChanges();
          }
          module.instantiate();
        },

        instantiate: function () {
          module.verbose('Storing instance of modal');
          instance = module;
          $module.data(moduleNamespace, instance);
        },

        create: {
          dimmer: function () {
            var defaultSettings = {
              debug: settings.debug,
              dimmerName: 'modals',
              duration: {
                show: settings.duration,
                hide: settings.duration
              }
            },
                dimmerSettings = $.extend(true, defaultSettings, settings.dimmerSettings);
            if (settings.inverted) {
              dimmerSettings.variation = dimmerSettings.variation !== undefined ? dimmerSettings.variation + ' inverted' : 'inverted';
            }
            if ($.fn.dimmer === undefined) {
              module.error(error.dimmer);
              return;
            }
            module.debug('Creating dimmer with settings', dimmerSettings);
            $dimmable = $context.dimmer(dimmerSettings);
            if (settings.detachable) {
              module.verbose('Modal is detachable, moving content into dimmer');
              $dimmable.dimmer('add content', $module);
            } else {
              module.set.undetached();
            }
            if (settings.blurring) {
              $dimmable.addClass(className.blurring);
            }
            $dimmer = $dimmable.dimmer('get dimmer');
          },
          id: function () {
            id = (Math.random().toString(16) + '000000000').substr(2, 8);
            elementEventNamespace = '.' + id;
            module.verbose('Creating unique id for element', id);
          }
        },

        destroy: function () {
          module.verbose('Destroying previous modal');
          $module.removeData(moduleNamespace).off(eventNamespace);
          $window.off(elementEventNamespace);
          $dimmer.off(elementEventNamespace);
          $close.off(eventNamespace);
          $context.dimmer('destroy');
        },

        observeChanges: function () {
          if ('MutationObserver' in window) {
            observer = new MutationObserver(function (mutations) {
              module.debug('DOM tree modified, refreshing');
              module.refresh();
            });
            observer.observe(element, {
              childList: true,
              subtree: true
            });
            module.debug('Setting up mutation observer', observer);
          }
        },

        refresh: function () {
          module.remove.scrolling();
          module.cacheSizes();
          module.set.screenHeight();
          module.set.type();
          module.set.position();
        },

        refreshModals: function () {
          $otherModals = $module.siblings(selector.modal);
          $allModals = $otherModals.add($module);
        },

        attachEvents: function (selector, event) {
          var $toggle = $(selector);
          event = $.isFunction(module[event]) ? module[event] : module.toggle;
          if ($toggle.length > 0) {
            module.debug('Attaching modal events to element', selector, event);
            $toggle.off(eventNamespace).on('click' + eventNamespace, event);
          } else {
            module.error(error.notFound, selector);
          }
        },

        bind: {
          events: function () {
            module.verbose('Attaching events');
            $module.on('click' + eventNamespace, selector.close, module.event.close).on('click' + eventNamespace, selector.approve, module.event.approve).on('click' + eventNamespace, selector.deny, module.event.deny);
            $window.on('resize' + elementEventNamespace, module.event.resize);
          }
        },

        get: {
          id: function () {
            return (Math.random().toString(16) + '000000000').substr(2, 8);
          }
        },

        event: {
          approve: function () {
            if (settings.onApprove.call(element, $(this)) === false) {
              module.verbose('Approve callback returned false cancelling hide');
              return;
            }
            module.hide();
          },
          deny: function () {
            if (settings.onDeny.call(element, $(this)) === false) {
              module.verbose('Deny callback returned false cancelling hide');
              return;
            }
            module.hide();
          },
          close: function () {
            module.hide();
          },
          click: function (event) {
            var $target = $(event.target),
                isInModal = $target.closest(selector.modal).length > 0,
                isInDOM = $.contains(document.documentElement, event.target);
            if (!isInModal && isInDOM) {
              module.debug('Dimmer clicked, hiding all modals');
              if (module.is.active()) {
                module.remove.clickaway();
                if (settings.allowMultiple) {
                  module.hide();
                } else {
                  module.hideAll();
                }
              }
            }
          },
          debounce: function (method, delay) {
            clearTimeout(module.timer);
            module.timer = setTimeout(method, delay);
          },
          keyboard: function (event) {
            var keyCode = event.which,
                escapeKey = 27;
            if (keyCode == escapeKey) {
              if (settings.closable) {
                module.debug('Escape key pressed hiding modal');
                module.hide();
              } else {
                module.debug('Escape key pressed, but closable is set to false');
              }
              event.preventDefault();
            }
          },
          resize: function () {
            if ($dimmable.dimmer('is active')) {
              requestAnimationFrame(module.refresh);
            }
          }
        },

        toggle: function () {
          if (module.is.active() || module.is.animating()) {
            module.hide();
          } else {
            module.show();
          }
        },

        show: function (callback) {
          callback = $.isFunction(callback) ? callback : function () {};
          module.refreshModals();
          module.showModal(callback);
        },

        hide: function (callback) {
          callback = $.isFunction(callback) ? callback : function () {};
          module.refreshModals();
          module.hideModal(callback);
        },

        showModal: function (callback) {
          callback = $.isFunction(callback) ? callback : function () {};
          if (module.is.animating() || !module.is.active()) {

            module.showDimmer();
            module.cacheSizes();
            module.set.position();
            module.set.screenHeight();
            module.set.type();
            module.set.clickaway();

            if (!settings.allowMultiple && module.others.active()) {
              module.hideOthers(module.showModal);
            } else {
              settings.onShow.call(element);
              if (settings.transition && $.fn.transition !== undefined && $module.transition('is supported')) {
                module.debug('Showing modal with css animations');
                $module.transition({
                  debug: settings.debug,
                  animation: settings.transition + ' in',
                  queue: settings.queue,
                  duration: settings.duration,
                  useFailSafe: true,
                  onComplete: function () {
                    settings.onVisible.apply(element);
                    module.add.keyboardShortcuts();
                    module.save.focus();
                    module.set.active();
                    if (settings.autofocus) {
                      module.set.autofocus();
                    }
                    callback();
                  }
                });
              } else {
                module.error(error.noTransition);
              }
            }
          } else {
            module.debug('Modal is already visible');
          }
        },

        hideModal: function (callback, keepDimmed) {
          callback = $.isFunction(callback) ? callback : function () {};
          module.debug('Hiding modal');
          if (settings.onHide.call(element, $(this)) === false) {
            module.verbose('Hide callback returned false cancelling hide');
            return;
          }

          if (module.is.animating() || module.is.active()) {
            if (settings.transition && $.fn.transition !== undefined && $module.transition('is supported')) {
              module.remove.active();
              $module.transition({
                debug: settings.debug,
                animation: settings.transition + ' out',
                queue: settings.queue,
                duration: settings.duration,
                useFailSafe: true,
                onStart: function () {
                  if (!module.others.active() && !keepDimmed) {
                    module.hideDimmer();
                  }
                  module.remove.keyboardShortcuts();
                },
                onComplete: function () {
                  settings.onHidden.call(element);
                  module.restore.focus();
                  callback();
                }
              });
            } else {
              module.error(error.noTransition);
            }
          }
        },

        showDimmer: function () {
          if ($dimmable.dimmer('is animating') || !$dimmable.dimmer('is active')) {
            module.debug('Showing dimmer');
            $dimmable.dimmer('show');
          } else {
            module.debug('Dimmer already visible');
          }
        },

        hideDimmer: function () {
          if ($dimmable.dimmer('is animating') || $dimmable.dimmer('is active')) {
            $dimmable.dimmer('hide', function () {
              module.remove.clickaway();
              module.remove.screenHeight();
            });
          } else {
            module.debug('Dimmer is not visible cannot hide');
            return;
          }
        },

        hideAll: function (callback) {
          var $visibleModals = $allModals.filter('.' + className.active + ', .' + className.animating);
          callback = $.isFunction(callback) ? callback : function () {};
          if ($visibleModals.length > 0) {
            module.debug('Hiding all visible modals');
            module.hideDimmer();
            $visibleModals.modal('hide modal', callback);
          }
        },

        hideOthers: function (callback) {
          var $visibleModals = $otherModals.filter('.' + className.active + ', .' + className.animating);
          callback = $.isFunction(callback) ? callback : function () {};
          if ($visibleModals.length > 0) {
            module.debug('Hiding other modals', $otherModals);
            $visibleModals.modal('hide modal', callback, true);
          }
        },

        others: {
          active: function () {
            return $otherModals.filter('.' + className.active).length > 0;
          },
          animating: function () {
            return $otherModals.filter('.' + className.animating).length > 0;
          }
        },

        add: {
          keyboardShortcuts: function () {
            module.verbose('Adding keyboard shortcuts');
            $document.on('keyup' + eventNamespace, module.event.keyboard);
          }
        },

        save: {
          focus: function () {
            $focusedElement = $(document.activeElement).blur();
          }
        },

        restore: {
          focus: function () {
            if ($focusedElement && $focusedElement.length > 0) {
              $focusedElement.focus();
            }
          }
        },

        remove: {
          active: function () {
            $module.removeClass(className.active);
          },
          clickaway: function () {
            if (settings.closable) {
              $dimmer.off('click' + elementEventNamespace);
            }
          },
          bodyStyle: function () {
            if ($body.attr('style') === '') {
              module.verbose('Removing style attribute');
              $body.removeAttr('style');
            }
          },
          screenHeight: function () {
            module.debug('Removing page height');
            $body.css('height', '');
          },
          keyboardShortcuts: function () {
            module.verbose('Removing keyboard shortcuts');
            $document.off('keyup' + eventNamespace);
          },
          scrolling: function () {
            $dimmable.removeClass(className.scrolling);
            $module.removeClass(className.scrolling);
          }
        },

        cacheSizes: function () {
          var modalHeight = $module.outerHeight();
          if (module.cache === undefined || modalHeight !== 0) {
            module.cache = {
              pageHeight: $(document).outerHeight(),
              height: modalHeight + settings.offset,
              contextHeight: settings.context == 'body' ? $(window).height() : $dimmable.height()
            };
          }
          module.debug('Caching modal and container sizes', module.cache);
        },

        can: {
          fit: function () {
            return module.cache.height + settings.padding * 2 < module.cache.contextHeight;
          }
        },

        is: {
          active: function () {
            return $module.hasClass(className.active);
          },
          animating: function () {
            return $module.transition('is supported') ? $module.transition('is animating') : $module.is(':visible');
          },
          scrolling: function () {
            return $dimmable.hasClass(className.scrolling);
          },
          modernBrowser: function () {
            // appName for IE11 reports 'Netscape' can no longer use
            return !(window.ActiveXObject || "ActiveXObject" in window);
          }
        },

        set: {
          autofocus: function () {
            var $inputs = $module.find(':input').filter(':visible'),
                $autofocus = $inputs.filter('[autofocus]'),
                $input = $autofocus.length > 0 ? $autofocus.first() : $inputs.first();
            if ($input.length > 0) {
              $input.focus();
            }
          },
          clickaway: function () {
            if (settings.closable) {
              $dimmer.on('click' + elementEventNamespace, module.event.click);
            }
          },
          screenHeight: function () {
            if (module.can.fit()) {
              $body.css('height', '');
            } else {
              module.debug('Modal is taller than page content, resizing page height');
              $body.css('height', module.cache.height + settings.padding * 2);
            }
          },
          active: function () {
            $module.addClass(className.active);
          },
          scrolling: function () {
            $dimmable.addClass(className.scrolling);
            $module.addClass(className.scrolling);
          },
          type: function () {
            if (module.can.fit()) {
              module.verbose('Modal fits on screen');
              if (!module.others.active() && !module.others.animating()) {
                module.remove.scrolling();
              }
            } else {
              module.verbose('Modal cannot fit on screen setting to scrolling');
              module.set.scrolling();
            }
          },
          position: function () {
            module.verbose('Centering modal on page', module.cache);
            if (module.can.fit()) {
              $module.css({
                top: '',
                marginTop: -(module.cache.height / 2)
              });
            } else {
              $module.css({
                marginTop: '',
                top: $document.scrollTop()
              });
            }
          },
          undetached: function () {
            $dimmable.addClass(className.undetached);
          }
        },

        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };

      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.modal.settings = {

    name: 'Modal',
    namespace: 'modal',

    silent: false,
    debug: false,
    verbose: false,
    performance: true,

    observeChanges: false,

    allowMultiple: false,
    detachable: true,
    closable: true,
    autofocus: true,

    inverted: false,
    blurring: false,

    dimmerSettings: {
      closable: false,
      useCSS: true
    },

    context: 'body',

    queue: false,
    duration: 500,
    offset: 0,
    transition: 'scale',

    // padding with edge of page
    padding: 50,

    // called before show animation
    onShow: function () {},

    // called after show animation
    onVisible: function () {},

    // called before hide animation
    onHide: function () {
      return true;
    },

    // called after hide animation
    onHidden: function () {},

    // called after approve selector match
    onApprove: function () {
      return true;
    },

    // called after deny selector match
    onDeny: function () {
      return true;
    },

    selector: {
      close: '> .close',
      approve: '.actions .positive, .actions .approve, .actions .ok',
      deny: '.actions .negative, .actions .deny, .actions .cancel',
      modal: '.ui.modal'
    },
    error: {
      dimmer: 'UI Dimmer, a required component is not included in this page',
      method: 'The method you called is not defined.',
      notFound: 'The element you specified could not be found'
    },
    className: {
      active: 'active',
      animating: 'animating',
      blurring: 'blurring',
      scrolling: 'scrolling',
      undetached: 'undetached'
    }
  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL21vZGFsLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFVQSxDQUFDLENBQUMsVUFBVSxDQUFWLEVBQWEsTUFBYixFQUFxQixRQUFyQixFQUErQixTQUEvQixFQUEwQzs7QUFFNUM7O0FBRUEsV0FBVSxPQUFPLE1BQVAsSUFBaUIsV0FBakIsSUFBZ0MsT0FBTyxJQUFQLElBQWUsSUFBaEQsR0FDTCxNQURLLEdBRUosT0FBTyxJQUFQLElBQWUsV0FBZixJQUE4QixLQUFLLElBQUwsSUFBYSxJQUE1QyxHQUNFLElBREYsR0FFRSxTQUFTLGFBQVQsR0FKTjs7QUFPQSxJQUFFLEVBQUYsQ0FBSyxLQUFMLEdBQWEsVUFBUyxVQUFULEVBQXFCO0FBQ2hDLFFBQ0UsY0FBaUIsRUFBRSxJQUFGLENBRG5CO0FBQUEsUUFFRSxVQUFpQixFQUFFLE1BQUYsQ0FGbkI7QUFBQSxRQUdFLFlBQWlCLEVBQUUsUUFBRixDQUhuQjtBQUFBLFFBSUUsUUFBaUIsRUFBRSxNQUFGLENBSm5CO0FBQUEsUUFNRSxpQkFBaUIsWUFBWSxRQUFaLElBQXdCLEVBTjNDO0FBQUEsUUFRRSxPQUFpQixJQUFJLElBQUosR0FBVyxPQUFYLEVBUm5CO0FBQUEsUUFTRSxjQUFpQixFQVRuQjtBQUFBLFFBV0UsUUFBaUIsVUFBVSxDQUFWLENBWG5CO0FBQUEsUUFZRSxnQkFBa0IsT0FBTyxLQUFQLElBQWdCLFFBWnBDO0FBQUEsUUFhRSxpQkFBaUIsR0FBRyxLQUFILENBQVMsSUFBVCxDQUFjLFNBQWQsRUFBeUIsQ0FBekIsQ0FibkI7QUFBQSxRQWVFLHdCQUF3QixPQUFPLHFCQUFQLElBQ25CLE9BQU8sd0JBRFksSUFFbkIsT0FBTywyQkFGWSxJQUduQixPQUFPLHVCQUhZLElBSW5CLFVBQVMsUUFBVCxFQUFtQjtBQUFFLGlCQUFXLFFBQVgsRUFBcUIsQ0FBckI7QUFBMEIsS0FuQnREO0FBQUEsUUFxQkUsYUFyQkY7O0FBd0JBLGdCQUNHLElBREgsQ0FDUSxZQUFXO0FBQ2YsVUFDRSxXQUFnQixFQUFFLGFBQUYsQ0FBZ0IsVUFBaEIsQ0FBRixHQUNWLEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLEVBQUUsRUFBRixDQUFLLEtBQUwsQ0FBVyxRQUE5QixFQUF3QyxVQUF4QyxDQURVLEdBRVYsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLEVBQUUsRUFBRixDQUFLLEtBQUwsQ0FBVyxRQUF4QixDQUhOO0FBQUEsVUFLRSxXQUFrQixTQUFTLFFBTDdCO0FBQUEsVUFNRSxZQUFrQixTQUFTLFNBTjdCO0FBQUEsVUFPRSxZQUFrQixTQUFTLFNBUDdCO0FBQUEsVUFRRSxRQUFrQixTQUFTLEtBUjdCO0FBQUEsVUFVRSxpQkFBa0IsTUFBTSxTQVYxQjtBQUFBLFVBV0Usa0JBQWtCLFlBQVksU0FYaEM7QUFBQSxVQWFFLFVBQWtCLEVBQUUsSUFBRixDQWJwQjtBQUFBLFVBY0UsV0FBa0IsRUFBRSxTQUFTLE9BQVgsQ0FkcEI7QUFBQSxVQWVFLFNBQWtCLFFBQVEsSUFBUixDQUFhLFNBQVMsS0FBdEIsQ0FmcEI7QUFBQSxVQWlCRSxVQWpCRjtBQUFBLFVBa0JFLFlBbEJGO0FBQUEsVUFtQkUsZUFuQkY7QUFBQSxVQW9CRSxTQXBCRjtBQUFBLFVBcUJFLE9BckJGO0FBQUEsVUF1QkUsVUFBa0IsSUF2QnBCO0FBQUEsVUF3QkUsV0FBa0IsUUFBUSxJQUFSLENBQWEsZUFBYixDQXhCcEI7QUFBQSxVQTBCRSxxQkExQkY7QUFBQSxVQTJCRSxFQTNCRjtBQUFBLFVBNEJFLFFBNUJGO0FBQUEsVUE2QkUsTUE3QkY7QUErQkEsZUFBVTs7QUFFUixvQkFBWSxZQUFXO0FBQ3JCLGlCQUFPLE9BQVAsQ0FBZSxxQkFBZixFQUFzQyxRQUF0Qzs7QUFFQSxpQkFBTyxNQUFQLENBQWMsRUFBZDtBQUNBLGlCQUFPLE1BQVAsQ0FBYyxNQUFkO0FBQ0EsaUJBQU8sYUFBUDs7QUFFQSxpQkFBTyxJQUFQLENBQVksTUFBWjtBQUNBLGNBQUcsU0FBUyxjQUFaLEVBQTRCO0FBQzFCLG1CQUFPLGNBQVA7QUFDRDtBQUNELGlCQUFPLFdBQVA7QUFDRCxTQWRPOztBQWdCUixxQkFBYSxZQUFXO0FBQ3RCLGlCQUFPLE9BQVAsQ0FBZSwyQkFBZjtBQUNBLHFCQUFXLE1BQVg7QUFDQSxrQkFDRyxJQURILENBQ1EsZUFEUixFQUN5QixRQUR6QjtBQUdELFNBdEJPOztBQXdCUixnQkFBUTtBQUNOLGtCQUFRLFlBQVc7QUFDakIsZ0JBQ0Usa0JBQWtCO0FBQ2hCLHFCQUFhLFNBQVMsS0FETjtBQUVoQiwwQkFBYSxRQUZHO0FBR2hCLHdCQUFhO0FBQ1gsc0JBQVcsU0FBUyxRQURUO0FBRVgsc0JBQVcsU0FBUztBQUZUO0FBSEcsYUFEcEI7QUFBQSxnQkFTRSxpQkFBaUIsRUFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLGVBQWYsRUFBZ0MsU0FBUyxjQUF6QyxDQVRuQjtBQVdBLGdCQUFHLFNBQVMsUUFBWixFQUFzQjtBQUNwQiw2QkFBZSxTQUFmLEdBQTRCLGVBQWUsU0FBZixLQUE2QixTQUE5QixHQUN2QixlQUFlLFNBQWYsR0FBMkIsV0FESixHQUV2QixVQUZKO0FBSUQ7QUFDRCxnQkFBRyxFQUFFLEVBQUYsQ0FBSyxNQUFMLEtBQWdCLFNBQW5CLEVBQThCO0FBQzVCLHFCQUFPLEtBQVAsQ0FBYSxNQUFNLE1BQW5CO0FBQ0E7QUFDRDtBQUNELG1CQUFPLEtBQVAsQ0FBYSwrQkFBYixFQUE4QyxjQUE5QztBQUNBLHdCQUFZLFNBQVMsTUFBVCxDQUFnQixjQUFoQixDQUFaO0FBQ0EsZ0JBQUcsU0FBUyxVQUFaLEVBQXdCO0FBQ3RCLHFCQUFPLE9BQVAsQ0FBZSxpREFBZjtBQUNBLHdCQUFVLE1BQVYsQ0FBaUIsYUFBakIsRUFBZ0MsT0FBaEM7QUFDRCxhQUhELE1BSUs7QUFDSCxxQkFBTyxHQUFQLENBQVcsVUFBWDtBQUNEO0FBQ0QsZ0JBQUcsU0FBUyxRQUFaLEVBQXNCO0FBQ3BCLHdCQUFVLFFBQVYsQ0FBbUIsVUFBVSxRQUE3QjtBQUNEO0FBQ0Qsc0JBQVUsVUFBVSxNQUFWLENBQWlCLFlBQWpCLENBQVY7QUFDRCxXQXBDSztBQXFDTixjQUFJLFlBQVc7QUFDYixpQkFBSyxDQUFDLEtBQUssTUFBTCxHQUFjLFFBQWQsQ0FBdUIsRUFBdkIsSUFBNkIsV0FBOUIsRUFBMkMsTUFBM0MsQ0FBa0QsQ0FBbEQsRUFBb0QsQ0FBcEQsQ0FBTDtBQUNBLG9DQUF3QixNQUFNLEVBQTlCO0FBQ0EsbUJBQU8sT0FBUCxDQUFlLGdDQUFmLEVBQWlELEVBQWpEO0FBQ0Q7QUF6Q0ssU0F4QkE7O0FBb0VSLGlCQUFTLFlBQVc7QUFDbEIsaUJBQU8sT0FBUCxDQUFlLDJCQUFmO0FBQ0Esa0JBQ0csVUFESCxDQUNjLGVBRGQsRUFFRyxHQUZILENBRU8sY0FGUDtBQUlBLGtCQUFRLEdBQVIsQ0FBWSxxQkFBWjtBQUNBLGtCQUFRLEdBQVIsQ0FBWSxxQkFBWjtBQUNBLGlCQUFPLEdBQVAsQ0FBVyxjQUFYO0FBQ0EsbUJBQVMsTUFBVCxDQUFnQixTQUFoQjtBQUNELFNBOUVPOztBQWdGUix3QkFBZ0IsWUFBVztBQUN6QixjQUFHLHNCQUFzQixNQUF6QixFQUFpQztBQUMvQix1QkFBVyxJQUFJLGdCQUFKLENBQXFCLFVBQVMsU0FBVCxFQUFvQjtBQUNsRCxxQkFBTyxLQUFQLENBQWEsK0JBQWI7QUFDQSxxQkFBTyxPQUFQO0FBQ0QsYUFIVSxDQUFYO0FBSUEscUJBQVMsT0FBVCxDQUFpQixPQUFqQixFQUEwQjtBQUN4Qix5QkFBWSxJQURZO0FBRXhCLHVCQUFZO0FBRlksYUFBMUI7QUFJQSxtQkFBTyxLQUFQLENBQWEsOEJBQWIsRUFBNkMsUUFBN0M7QUFDRDtBQUNGLFNBNUZPOztBQThGUixpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLE1BQVAsQ0FBYyxTQUFkO0FBQ0EsaUJBQU8sVUFBUDtBQUNBLGlCQUFPLEdBQVAsQ0FBVyxZQUFYO0FBQ0EsaUJBQU8sR0FBUCxDQUFXLElBQVg7QUFDQSxpQkFBTyxHQUFQLENBQVcsUUFBWDtBQUNELFNBcEdPOztBQXNHUix1QkFBZSxZQUFXO0FBQ3hCLHlCQUFlLFFBQVEsUUFBUixDQUFpQixTQUFTLEtBQTFCLENBQWY7QUFDQSx1QkFBZSxhQUFhLEdBQWIsQ0FBaUIsT0FBakIsQ0FBZjtBQUNELFNBekdPOztBQTJHUixzQkFBYyxVQUFTLFFBQVQsRUFBbUIsS0FBbkIsRUFBMEI7QUFDdEMsY0FDRSxVQUFVLEVBQUUsUUFBRixDQURaO0FBR0Esa0JBQVEsRUFBRSxVQUFGLENBQWEsT0FBTyxLQUFQLENBQWIsSUFDSixPQUFPLEtBQVAsQ0FESSxHQUVKLE9BQU8sTUFGWDtBQUlBLGNBQUcsUUFBUSxNQUFSLEdBQWlCLENBQXBCLEVBQXVCO0FBQ3JCLG1CQUFPLEtBQVAsQ0FBYSxtQ0FBYixFQUFrRCxRQUFsRCxFQUE0RCxLQUE1RDtBQUNBLG9CQUNHLEdBREgsQ0FDTyxjQURQLEVBRUcsRUFGSCxDQUVNLFVBQVUsY0FGaEIsRUFFZ0MsS0FGaEM7QUFJRCxXQU5ELE1BT0s7QUFDSCxtQkFBTyxLQUFQLENBQWEsTUFBTSxRQUFuQixFQUE2QixRQUE3QjtBQUNEO0FBQ0YsU0E3SE87O0FBK0hSLGNBQU07QUFDSixrQkFBUSxZQUFXO0FBQ2pCLG1CQUFPLE9BQVAsQ0FBZSxrQkFBZjtBQUNBLG9CQUNHLEVBREgsQ0FDTSxVQUFVLGNBRGhCLEVBQ2dDLFNBQVMsS0FEekMsRUFDZ0QsT0FBTyxLQUFQLENBQWEsS0FEN0QsRUFFRyxFQUZILENBRU0sVUFBVSxjQUZoQixFQUVnQyxTQUFTLE9BRnpDLEVBRWtELE9BQU8sS0FBUCxDQUFhLE9BRi9ELEVBR0csRUFISCxDQUdNLFVBQVUsY0FIaEIsRUFHZ0MsU0FBUyxJQUh6QyxFQUcrQyxPQUFPLEtBQVAsQ0FBYSxJQUg1RDtBQUtBLG9CQUNHLEVBREgsQ0FDTSxXQUFXLHFCQURqQixFQUN3QyxPQUFPLEtBQVAsQ0FBYSxNQURyRDtBQUdEO0FBWEcsU0EvSEU7O0FBNklSLGFBQUs7QUFDSCxjQUFJLFlBQVc7QUFDYixtQkFBTyxDQUFDLEtBQUssTUFBTCxHQUFjLFFBQWQsQ0FBdUIsRUFBdkIsSUFBNkIsV0FBOUIsRUFBMkMsTUFBM0MsQ0FBa0QsQ0FBbEQsRUFBb0QsQ0FBcEQsQ0FBUDtBQUNEO0FBSEUsU0E3SUc7O0FBbUpSLGVBQU87QUFDTCxtQkFBUyxZQUFXO0FBQ2xCLGdCQUFHLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixPQUF4QixFQUFpQyxFQUFFLElBQUYsQ0FBakMsTUFBOEMsS0FBakQsRUFBd0Q7QUFDdEQscUJBQU8sT0FBUCxDQUFlLGlEQUFmO0FBQ0E7QUFDRDtBQUNELG1CQUFPLElBQVA7QUFDRCxXQVBJO0FBUUwsZ0JBQU0sWUFBVztBQUNmLGdCQUFHLFNBQVMsTUFBVCxDQUFnQixJQUFoQixDQUFxQixPQUFyQixFQUE4QixFQUFFLElBQUYsQ0FBOUIsTUFBMkMsS0FBOUMsRUFBcUQ7QUFDbkQscUJBQU8sT0FBUCxDQUFlLDhDQUFmO0FBQ0E7QUFDRDtBQUNELG1CQUFPLElBQVA7QUFDRCxXQWRJO0FBZUwsaUJBQU8sWUFBVztBQUNoQixtQkFBTyxJQUFQO0FBQ0QsV0FqQkk7QUFrQkwsaUJBQU8sVUFBUyxLQUFULEVBQWdCO0FBQ3JCLGdCQUNFLFVBQVksRUFBRSxNQUFNLE1BQVIsQ0FEZDtBQUFBLGdCQUVFLFlBQWEsUUFBUSxPQUFSLENBQWdCLFNBQVMsS0FBekIsRUFBZ0MsTUFBaEMsR0FBeUMsQ0FGeEQ7QUFBQSxnQkFHRSxVQUFZLEVBQUUsUUFBRixDQUFXLFNBQVMsZUFBcEIsRUFBcUMsTUFBTSxNQUEzQyxDQUhkO0FBS0EsZ0JBQUcsQ0FBQyxTQUFELElBQWMsT0FBakIsRUFBMEI7QUFDeEIscUJBQU8sS0FBUCxDQUFhLG1DQUFiO0FBQ0Esa0JBQUksT0FBTyxFQUFQLENBQVUsTUFBVixFQUFKLEVBQXlCO0FBQ3ZCLHVCQUFPLE1BQVAsQ0FBYyxTQUFkO0FBQ0Esb0JBQUcsU0FBUyxhQUFaLEVBQTJCO0FBQ3pCLHlCQUFPLElBQVA7QUFDRCxpQkFGRCxNQUdLO0FBQ0gseUJBQU8sT0FBUDtBQUNEO0FBQ0Y7QUFDRjtBQUNGLFdBcENJO0FBcUNMLG9CQUFVLFVBQVMsTUFBVCxFQUFpQixLQUFqQixFQUF3QjtBQUNoQyx5QkFBYSxPQUFPLEtBQXBCO0FBQ0EsbUJBQU8sS0FBUCxHQUFlLFdBQVcsTUFBWCxFQUFtQixLQUFuQixDQUFmO0FBQ0QsV0F4Q0k7QUF5Q0wsb0JBQVUsVUFBUyxLQUFULEVBQWdCO0FBQ3hCLGdCQUNFLFVBQVksTUFBTSxLQURwQjtBQUFBLGdCQUVFLFlBQVksRUFGZDtBQUlBLGdCQUFHLFdBQVcsU0FBZCxFQUF5QjtBQUN2QixrQkFBRyxTQUFTLFFBQVosRUFBc0I7QUFDcEIsdUJBQU8sS0FBUCxDQUFhLGlDQUFiO0FBQ0EsdUJBQU8sSUFBUDtBQUNELGVBSEQsTUFJSztBQUNILHVCQUFPLEtBQVAsQ0FBYSxrREFBYjtBQUNEO0FBQ0Qsb0JBQU0sY0FBTjtBQUNEO0FBQ0YsV0F4REk7QUF5REwsa0JBQVEsWUFBVztBQUNqQixnQkFBSSxVQUFVLE1BQVYsQ0FBaUIsV0FBakIsQ0FBSixFQUFvQztBQUNsQyxvQ0FBc0IsT0FBTyxPQUE3QjtBQUNEO0FBQ0Y7QUE3REksU0FuSkM7O0FBbU5SLGdCQUFRLFlBQVc7QUFDakIsY0FBSSxPQUFPLEVBQVAsQ0FBVSxNQUFWLE1BQXNCLE9BQU8sRUFBUCxDQUFVLFNBQVYsRUFBMUIsRUFBa0Q7QUFDaEQsbUJBQU8sSUFBUDtBQUNELFdBRkQsTUFHSztBQUNILG1CQUFPLElBQVA7QUFDRDtBQUNGLFNBMU5POztBQTROUixjQUFNLFVBQVMsUUFBVCxFQUFtQjtBQUN2QixxQkFBVyxFQUFFLFVBQUYsQ0FBYSxRQUFiLElBQ1AsUUFETyxHQUVQLFlBQVUsQ0FBRSxDQUZoQjtBQUlBLGlCQUFPLGFBQVA7QUFDQSxpQkFBTyxTQUFQLENBQWlCLFFBQWpCO0FBQ0QsU0FuT087O0FBcU9SLGNBQU0sVUFBUyxRQUFULEVBQW1CO0FBQ3ZCLHFCQUFXLEVBQUUsVUFBRixDQUFhLFFBQWIsSUFDUCxRQURPLEdBRVAsWUFBVSxDQUFFLENBRmhCO0FBSUEsaUJBQU8sYUFBUDtBQUNBLGlCQUFPLFNBQVAsQ0FBaUIsUUFBakI7QUFDRCxTQTVPTzs7QUE4T1IsbUJBQVcsVUFBUyxRQUFULEVBQW1CO0FBQzVCLHFCQUFXLEVBQUUsVUFBRixDQUFhLFFBQWIsSUFDUCxRQURPLEdBRVAsWUFBVSxDQUFFLENBRmhCO0FBSUEsY0FBSSxPQUFPLEVBQVAsQ0FBVSxTQUFWLE1BQXlCLENBQUMsT0FBTyxFQUFQLENBQVUsTUFBVixFQUE5QixFQUFtRDs7QUFFakQsbUJBQU8sVUFBUDtBQUNBLG1CQUFPLFVBQVA7QUFDQSxtQkFBTyxHQUFQLENBQVcsUUFBWDtBQUNBLG1CQUFPLEdBQVAsQ0FBVyxZQUFYO0FBQ0EsbUJBQU8sR0FBUCxDQUFXLElBQVg7QUFDQSxtQkFBTyxHQUFQLENBQVcsU0FBWDs7QUFFQSxnQkFBSSxDQUFDLFNBQVMsYUFBVixJQUEyQixPQUFPLE1BQVAsQ0FBYyxNQUFkLEVBQS9CLEVBQXdEO0FBQ3RELHFCQUFPLFVBQVAsQ0FBa0IsT0FBTyxTQUF6QjtBQUNELGFBRkQsTUFHSztBQUNILHVCQUFTLE1BQVQsQ0FBZ0IsSUFBaEIsQ0FBcUIsT0FBckI7QUFDQSxrQkFBRyxTQUFTLFVBQVQsSUFBdUIsRUFBRSxFQUFGLENBQUssVUFBTCxLQUFvQixTQUEzQyxJQUF3RCxRQUFRLFVBQVIsQ0FBbUIsY0FBbkIsQ0FBM0QsRUFBK0Y7QUFDN0YsdUJBQU8sS0FBUCxDQUFhLG1DQUFiO0FBQ0Esd0JBQ0csVUFESCxDQUNjO0FBQ1YseUJBQWMsU0FBUyxLQURiO0FBRVYsNkJBQWMsU0FBUyxVQUFULEdBQXNCLEtBRjFCO0FBR1YseUJBQWMsU0FBUyxLQUhiO0FBSVYsNEJBQWMsU0FBUyxRQUpiO0FBS1YsK0JBQWMsSUFMSjtBQU1WLDhCQUFhLFlBQVc7QUFDdEIsNkJBQVMsU0FBVCxDQUFtQixLQUFuQixDQUF5QixPQUF6QjtBQUNBLDJCQUFPLEdBQVAsQ0FBVyxpQkFBWDtBQUNBLDJCQUFPLElBQVAsQ0FBWSxLQUFaO0FBQ0EsMkJBQU8sR0FBUCxDQUFXLE1BQVg7QUFDQSx3QkFBRyxTQUFTLFNBQVosRUFBdUI7QUFDckIsNkJBQU8sR0FBUCxDQUFXLFNBQVg7QUFDRDtBQUNEO0FBQ0Q7QUFmUyxpQkFEZDtBQW1CRCxlQXJCRCxNQXNCSztBQUNILHVCQUFPLEtBQVAsQ0FBYSxNQUFNLFlBQW5CO0FBQ0Q7QUFDRjtBQUNGLFdBeENELE1BeUNLO0FBQ0gsbUJBQU8sS0FBUCxDQUFhLDBCQUFiO0FBQ0Q7QUFDRixTQS9STzs7QUFpU1IsbUJBQVcsVUFBUyxRQUFULEVBQW1CLFVBQW5CLEVBQStCO0FBQ3hDLHFCQUFXLEVBQUUsVUFBRixDQUFhLFFBQWIsSUFDUCxRQURPLEdBRVAsWUFBVSxDQUFFLENBRmhCO0FBSUEsaUJBQU8sS0FBUCxDQUFhLGNBQWI7QUFDQSxjQUFHLFNBQVMsTUFBVCxDQUFnQixJQUFoQixDQUFxQixPQUFyQixFQUE4QixFQUFFLElBQUYsQ0FBOUIsTUFBMkMsS0FBOUMsRUFBcUQ7QUFDbkQsbUJBQU8sT0FBUCxDQUFlLDhDQUFmO0FBQ0E7QUFDRDs7QUFFRCxjQUFJLE9BQU8sRUFBUCxDQUFVLFNBQVYsTUFBeUIsT0FBTyxFQUFQLENBQVUsTUFBVixFQUE3QixFQUFrRDtBQUNoRCxnQkFBRyxTQUFTLFVBQVQsSUFBdUIsRUFBRSxFQUFGLENBQUssVUFBTCxLQUFvQixTQUEzQyxJQUF3RCxRQUFRLFVBQVIsQ0FBbUIsY0FBbkIsQ0FBM0QsRUFBK0Y7QUFDN0YscUJBQU8sTUFBUCxDQUFjLE1BQWQ7QUFDQSxzQkFDRyxVQURILENBQ2M7QUFDVix1QkFBYyxTQUFTLEtBRGI7QUFFViwyQkFBYyxTQUFTLFVBQVQsR0FBc0IsTUFGMUI7QUFHVix1QkFBYyxTQUFTLEtBSGI7QUFJViwwQkFBYyxTQUFTLFFBSmI7QUFLViw2QkFBYyxJQUxKO0FBTVYseUJBQWMsWUFBVztBQUN2QixzQkFBRyxDQUFDLE9BQU8sTUFBUCxDQUFjLE1BQWQsRUFBRCxJQUEyQixDQUFDLFVBQS9CLEVBQTJDO0FBQ3pDLDJCQUFPLFVBQVA7QUFDRDtBQUNELHlCQUFPLE1BQVAsQ0FBYyxpQkFBZDtBQUNELGlCQVhTO0FBWVYsNEJBQWEsWUFBVztBQUN0QiwyQkFBUyxRQUFULENBQWtCLElBQWxCLENBQXVCLE9BQXZCO0FBQ0EseUJBQU8sT0FBUCxDQUFlLEtBQWY7QUFDQTtBQUNEO0FBaEJTLGVBRGQ7QUFvQkQsYUF0QkQsTUF1Qks7QUFDSCxxQkFBTyxLQUFQLENBQWEsTUFBTSxZQUFuQjtBQUNEO0FBQ0Y7QUFDRixTQXhVTzs7QUEwVVIsb0JBQVksWUFBVztBQUNyQixjQUFHLFVBQVUsTUFBVixDQUFpQixjQUFqQixLQUFvQyxDQUFDLFVBQVUsTUFBVixDQUFpQixXQUFqQixDQUF4QyxFQUF3RTtBQUN0RSxtQkFBTyxLQUFQLENBQWEsZ0JBQWI7QUFDQSxzQkFBVSxNQUFWLENBQWlCLE1BQWpCO0FBQ0QsV0FIRCxNQUlLO0FBQ0gsbUJBQU8sS0FBUCxDQUFhLHdCQUFiO0FBQ0Q7QUFDRixTQWxWTzs7QUFvVlIsb0JBQVksWUFBVztBQUNyQixjQUFJLFVBQVUsTUFBVixDQUFpQixjQUFqQixLQUFxQyxVQUFVLE1BQVYsQ0FBaUIsV0FBakIsQ0FBekMsRUFBMEU7QUFDeEUsc0JBQVUsTUFBVixDQUFpQixNQUFqQixFQUF5QixZQUFXO0FBQ2xDLHFCQUFPLE1BQVAsQ0FBYyxTQUFkO0FBQ0EscUJBQU8sTUFBUCxDQUFjLFlBQWQ7QUFDRCxhQUhEO0FBSUQsV0FMRCxNQU1LO0FBQ0gsbUJBQU8sS0FBUCxDQUFhLG1DQUFiO0FBQ0E7QUFDRDtBQUNGLFNBL1ZPOztBQWlXUixpQkFBUyxVQUFTLFFBQVQsRUFBbUI7QUFDMUIsY0FDRSxpQkFBaUIsV0FBVyxNQUFYLENBQWtCLE1BQU0sVUFBVSxNQUFoQixHQUF5QixLQUF6QixHQUFpQyxVQUFVLFNBQTdELENBRG5CO0FBR0EscUJBQVcsRUFBRSxVQUFGLENBQWEsUUFBYixJQUNQLFFBRE8sR0FFUCxZQUFVLENBQUUsQ0FGaEI7QUFJQSxjQUFJLGVBQWUsTUFBZixHQUF3QixDQUE1QixFQUFnQztBQUM5QixtQkFBTyxLQUFQLENBQWEsMkJBQWI7QUFDQSxtQkFBTyxVQUFQO0FBQ0EsMkJBQ0csS0FESCxDQUNTLFlBRFQsRUFDdUIsUUFEdkI7QUFHRDtBQUNGLFNBaFhPOztBQWtYUixvQkFBWSxVQUFTLFFBQVQsRUFBbUI7QUFDN0IsY0FDRSxpQkFBaUIsYUFBYSxNQUFiLENBQW9CLE1BQU0sVUFBVSxNQUFoQixHQUF5QixLQUF6QixHQUFpQyxVQUFVLFNBQS9ELENBRG5CO0FBR0EscUJBQVcsRUFBRSxVQUFGLENBQWEsUUFBYixJQUNQLFFBRE8sR0FFUCxZQUFVLENBQUUsQ0FGaEI7QUFJQSxjQUFJLGVBQWUsTUFBZixHQUF3QixDQUE1QixFQUFnQztBQUM5QixtQkFBTyxLQUFQLENBQWEscUJBQWIsRUFBb0MsWUFBcEM7QUFDQSwyQkFDRyxLQURILENBQ1MsWUFEVCxFQUN1QixRQUR2QixFQUNpQyxJQURqQztBQUdEO0FBQ0YsU0FoWU87O0FBa1lSLGdCQUFRO0FBQ04sa0JBQVEsWUFBVztBQUNqQixtQkFBUSxhQUFhLE1BQWIsQ0FBb0IsTUFBTSxVQUFVLE1BQXBDLEVBQTRDLE1BQTVDLEdBQXFELENBQTdEO0FBQ0QsV0FISztBQUlOLHFCQUFXLFlBQVc7QUFDcEIsbUJBQVEsYUFBYSxNQUFiLENBQW9CLE1BQU0sVUFBVSxTQUFwQyxFQUErQyxNQUEvQyxHQUF3RCxDQUFoRTtBQUNEO0FBTkssU0FsWUE7O0FBNFlSLGFBQUs7QUFDSCw2QkFBbUIsWUFBVztBQUM1QixtQkFBTyxPQUFQLENBQWUsMkJBQWY7QUFDQSxzQkFDRyxFQURILENBQ00sVUFBVSxjQURoQixFQUNnQyxPQUFPLEtBQVAsQ0FBYSxRQUQ3QztBQUdEO0FBTkUsU0E1WUc7O0FBcVpSLGNBQU07QUFDSixpQkFBTyxZQUFXO0FBQ2hCLDhCQUFrQixFQUFFLFNBQVMsYUFBWCxFQUEwQixJQUExQixFQUFsQjtBQUNEO0FBSEcsU0FyWkU7O0FBMlpSLGlCQUFTO0FBQ1AsaUJBQU8sWUFBVztBQUNoQixnQkFBRyxtQkFBbUIsZ0JBQWdCLE1BQWhCLEdBQXlCLENBQS9DLEVBQWtEO0FBQ2hELDhCQUFnQixLQUFoQjtBQUNEO0FBQ0Y7QUFMTSxTQTNaRDs7QUFtYVIsZ0JBQVE7QUFDTixrQkFBUSxZQUFXO0FBQ2pCLG9CQUFRLFdBQVIsQ0FBb0IsVUFBVSxNQUE5QjtBQUNELFdBSEs7QUFJTixxQkFBVyxZQUFXO0FBQ3BCLGdCQUFHLFNBQVMsUUFBWixFQUFzQjtBQUNwQixzQkFDRyxHQURILENBQ08sVUFBVSxxQkFEakI7QUFHRDtBQUNGLFdBVks7QUFXTixxQkFBVyxZQUFXO0FBQ3BCLGdCQUFHLE1BQU0sSUFBTixDQUFXLE9BQVgsTUFBd0IsRUFBM0IsRUFBK0I7QUFDN0IscUJBQU8sT0FBUCxDQUFlLDBCQUFmO0FBQ0Esb0JBQU0sVUFBTixDQUFpQixPQUFqQjtBQUNEO0FBQ0YsV0FoQks7QUFpQk4sd0JBQWMsWUFBVztBQUN2QixtQkFBTyxLQUFQLENBQWEsc0JBQWI7QUFDQSxrQkFDRyxHQURILENBQ08sUUFEUCxFQUNpQixFQURqQjtBQUdELFdBdEJLO0FBdUJOLDZCQUFtQixZQUFXO0FBQzVCLG1CQUFPLE9BQVAsQ0FBZSw2QkFBZjtBQUNBLHNCQUNHLEdBREgsQ0FDTyxVQUFVLGNBRGpCO0FBR0QsV0E1Qks7QUE2Qk4scUJBQVcsWUFBVztBQUNwQixzQkFBVSxXQUFWLENBQXNCLFVBQVUsU0FBaEM7QUFDQSxvQkFBUSxXQUFSLENBQW9CLFVBQVUsU0FBOUI7QUFDRDtBQWhDSyxTQW5hQTs7QUFzY1Isb0JBQVksWUFBVztBQUNyQixjQUNFLGNBQWMsUUFBUSxXQUFSLEVBRGhCO0FBR0EsY0FBRyxPQUFPLEtBQVAsS0FBaUIsU0FBakIsSUFBOEIsZ0JBQWdCLENBQWpELEVBQW9EO0FBQ2xELG1CQUFPLEtBQVAsR0FBZTtBQUNiLDBCQUFnQixFQUFFLFFBQUYsRUFBWSxXQUFaLEVBREg7QUFFYixzQkFBZ0IsY0FBYyxTQUFTLE1BRjFCO0FBR2IsNkJBQWlCLFNBQVMsT0FBVCxJQUFvQixNQUFyQixHQUNaLEVBQUUsTUFBRixFQUFVLE1BQVYsRUFEWSxHQUVaLFVBQVUsTUFBVjtBQUxTLGFBQWY7QUFPRDtBQUNELGlCQUFPLEtBQVAsQ0FBYSxtQ0FBYixFQUFrRCxPQUFPLEtBQXpEO0FBQ0QsU0FwZE87O0FBc2RSLGFBQUs7QUFDSCxlQUFLLFlBQVc7QUFDZCxtQkFBVyxPQUFPLEtBQVAsQ0FBYSxNQUFiLEdBQXVCLFNBQVMsT0FBVCxHQUFtQixDQUE1QyxHQUFtRCxPQUFPLEtBQVAsQ0FBYSxhQUF6RTtBQUNEO0FBSEUsU0F0ZEc7O0FBNGRSLFlBQUk7QUFDRixrQkFBUSxZQUFXO0FBQ2pCLG1CQUFPLFFBQVEsUUFBUixDQUFpQixVQUFVLE1BQTNCLENBQVA7QUFDRCxXQUhDO0FBSUYscUJBQVcsWUFBVztBQUNwQixtQkFBTyxRQUFRLFVBQVIsQ0FBbUIsY0FBbkIsSUFDSCxRQUFRLFVBQVIsQ0FBbUIsY0FBbkIsQ0FERyxHQUVILFFBQVEsRUFBUixDQUFXLFVBQVgsQ0FGSjtBQUlELFdBVEM7QUFVRixxQkFBVyxZQUFXO0FBQ3BCLG1CQUFPLFVBQVUsUUFBVixDQUFtQixVQUFVLFNBQTdCLENBQVA7QUFDRCxXQVpDO0FBYUYseUJBQWUsWUFBVzs7QUFFeEIsbUJBQU8sRUFBRSxPQUFPLGFBQVAsSUFBd0IsbUJBQW1CLE1BQTdDLENBQVA7QUFDRDtBQWhCQyxTQTVkSTs7QUErZVIsYUFBSztBQUNILHFCQUFXLFlBQVc7QUFDcEIsZ0JBQ0UsVUFBYSxRQUFRLElBQVIsQ0FBYSxRQUFiLEVBQXVCLE1BQXZCLENBQThCLFVBQTlCLENBRGY7QUFBQSxnQkFFRSxhQUFhLFFBQVEsTUFBUixDQUFlLGFBQWYsQ0FGZjtBQUFBLGdCQUdFLFNBQWMsV0FBVyxNQUFYLEdBQW9CLENBQXJCLEdBQ1QsV0FBVyxLQUFYLEVBRFMsR0FFVCxRQUFRLEtBQVIsRUFMTjtBQU9BLGdCQUFHLE9BQU8sTUFBUCxHQUFnQixDQUFuQixFQUFzQjtBQUNwQixxQkFBTyxLQUFQO0FBQ0Q7QUFDRixXQVpFO0FBYUgscUJBQVcsWUFBVztBQUNwQixnQkFBRyxTQUFTLFFBQVosRUFBc0I7QUFDcEIsc0JBQ0csRUFESCxDQUNNLFVBQVUscUJBRGhCLEVBQ3VDLE9BQU8sS0FBUCxDQUFhLEtBRHBEO0FBR0Q7QUFDRixXQW5CRTtBQW9CSCx3QkFBYyxZQUFXO0FBQ3ZCLGdCQUFJLE9BQU8sR0FBUCxDQUFXLEdBQVgsRUFBSixFQUF1QjtBQUNyQixvQkFBTSxHQUFOLENBQVUsUUFBVixFQUFvQixFQUFwQjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLEtBQVAsQ0FBYSx5REFBYjtBQUNBLG9CQUNHLEdBREgsQ0FDTyxRQURQLEVBQ2lCLE9BQU8sS0FBUCxDQUFhLE1BQWIsR0FBdUIsU0FBUyxPQUFULEdBQW1CLENBRDNEO0FBR0Q7QUFDRixXQTlCRTtBQStCSCxrQkFBUSxZQUFXO0FBQ2pCLG9CQUFRLFFBQVIsQ0FBaUIsVUFBVSxNQUEzQjtBQUNELFdBakNFO0FBa0NILHFCQUFXLFlBQVc7QUFDcEIsc0JBQVUsUUFBVixDQUFtQixVQUFVLFNBQTdCO0FBQ0Esb0JBQVEsUUFBUixDQUFpQixVQUFVLFNBQTNCO0FBQ0QsV0FyQ0U7QUFzQ0gsZ0JBQU0sWUFBVztBQUNmLGdCQUFHLE9BQU8sR0FBUCxDQUFXLEdBQVgsRUFBSCxFQUFxQjtBQUNuQixxQkFBTyxPQUFQLENBQWUsc0JBQWY7QUFDQSxrQkFBRyxDQUFDLE9BQU8sTUFBUCxDQUFjLE1BQWQsRUFBRCxJQUEyQixDQUFDLE9BQU8sTUFBUCxDQUFjLFNBQWQsRUFBL0IsRUFBMEQ7QUFDeEQsdUJBQU8sTUFBUCxDQUFjLFNBQWQ7QUFDRDtBQUNGLGFBTEQsTUFNSztBQUNILHFCQUFPLE9BQVAsQ0FBZSxpREFBZjtBQUNBLHFCQUFPLEdBQVAsQ0FBVyxTQUFYO0FBQ0Q7QUFDRixXQWpERTtBQWtESCxvQkFBVSxZQUFXO0FBQ25CLG1CQUFPLE9BQVAsQ0FBZSx5QkFBZixFQUEwQyxPQUFPLEtBQWpEO0FBQ0EsZ0JBQUcsT0FBTyxHQUFQLENBQVcsR0FBWCxFQUFILEVBQXFCO0FBQ25CLHNCQUNHLEdBREgsQ0FDTztBQUNILHFCQUFLLEVBREY7QUFFSCwyQkFBVyxFQUFFLE9BQU8sS0FBUCxDQUFhLE1BQWIsR0FBc0IsQ0FBeEI7QUFGUixlQURQO0FBTUQsYUFQRCxNQVFLO0FBQ0gsc0JBQ0csR0FESCxDQUNPO0FBQ0gsMkJBQVksRUFEVDtBQUVILHFCQUFZLFVBQVUsU0FBVjtBQUZULGVBRFA7QUFNRDtBQUNGLFdBcEVFO0FBcUVILHNCQUFZLFlBQVc7QUFDckIsc0JBQVUsUUFBVixDQUFtQixVQUFVLFVBQTdCO0FBQ0Q7QUF2RUUsU0EvZUc7O0FBeWpCUixpQkFBUyxVQUFTLElBQVQsRUFBZSxLQUFmLEVBQXNCO0FBQzdCLGlCQUFPLEtBQVAsQ0FBYSxrQkFBYixFQUFpQyxJQUFqQyxFQUF1QyxLQUF2QztBQUNBLGNBQUksRUFBRSxhQUFGLENBQWdCLElBQWhCLENBQUosRUFBNEI7QUFDMUIsY0FBRSxNQUFGLENBQVMsSUFBVCxFQUFlLFFBQWYsRUFBeUIsSUFBekI7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsZ0JBQUcsRUFBRSxhQUFGLENBQWdCLFNBQVMsSUFBVCxDQUFoQixDQUFILEVBQW9DO0FBQ2xDLGdCQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsU0FBUyxJQUFULENBQWYsRUFBK0IsS0FBL0I7QUFDRCxhQUZELE1BR0s7QUFDSCx1QkFBUyxJQUFULElBQWlCLEtBQWpCO0FBQ0Q7QUFDRixXQVBJLE1BUUE7QUFDSCxtQkFBTyxTQUFTLElBQVQsQ0FBUDtBQUNEO0FBQ0YsU0F6a0JPO0FBMGtCUixrQkFBVSxVQUFTLElBQVQsRUFBZSxLQUFmLEVBQXNCO0FBQzlCLGNBQUksRUFBRSxhQUFGLENBQWdCLElBQWhCLENBQUosRUFBNEI7QUFDMUIsY0FBRSxNQUFGLENBQVMsSUFBVCxFQUFlLE1BQWYsRUFBdUIsSUFBdkI7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsbUJBQU8sSUFBUCxJQUFlLEtBQWY7QUFDRCxXQUZJLE1BR0E7QUFDSCxtQkFBTyxPQUFPLElBQVAsQ0FBUDtBQUNEO0FBQ0YsU0FwbEJPO0FBcWxCUixlQUFPLFlBQVc7QUFDaEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLEtBQWhDLEVBQXVDO0FBQ3JDLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2QixxQkFBTyxXQUFQLENBQW1CLEdBQW5CLENBQXVCLFNBQXZCO0FBQ0QsYUFGRCxNQUdLO0FBQ0gscUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFmO0FBQ0EscUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGO0FBQ0YsU0EvbEJPO0FBZ21CUixpQkFBUyxZQUFXO0FBQ2xCLGNBQUcsQ0FBQyxTQUFTLE1BQVYsSUFBb0IsU0FBUyxPQUE3QixJQUF3QyxTQUFTLEtBQXBELEVBQTJEO0FBQ3pELGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2QixxQkFBTyxXQUFQLENBQW1CLEdBQW5CLENBQXVCLFNBQXZCO0FBQ0QsYUFGRCxNQUdLO0FBQ0gscUJBQU8sT0FBUCxHQUFpQixTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxJQUFyQyxFQUEyQyxPQUEzQyxFQUFvRCxTQUFTLElBQVQsR0FBZ0IsR0FBcEUsQ0FBakI7QUFDQSxxQkFBTyxPQUFQLENBQWUsS0FBZixDQUFxQixPQUFyQixFQUE4QixTQUE5QjtBQUNEO0FBQ0Y7QUFDRixTQTFtQk87QUEybUJSLGVBQU8sWUFBVztBQUNoQixjQUFHLENBQUMsU0FBUyxNQUFiLEVBQXFCO0FBQ25CLG1CQUFPLEtBQVAsR0FBZSxTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxLQUFyQyxFQUE0QyxPQUE1QyxFQUFxRCxTQUFTLElBQVQsR0FBZ0IsR0FBckUsQ0FBZjtBQUNBLG1CQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLE9BQW5CLEVBQTRCLFNBQTVCO0FBQ0Q7QUFDRixTQWhuQk87QUFpbkJSLHFCQUFhO0FBQ1gsZUFBSyxVQUFTLE9BQVQsRUFBa0I7QUFDckIsZ0JBQ0UsV0FERixFQUVFLGFBRkYsRUFHRSxZQUhGO0FBS0EsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLDRCQUFnQixJQUFJLElBQUosR0FBVyxPQUFYLEVBQWhCO0FBQ0EsNkJBQWdCLFFBQVEsV0FBeEI7QUFDQSw4QkFBZ0IsY0FBYyxZQUE5QjtBQUNBLHFCQUFnQixXQUFoQjtBQUNBLDBCQUFZLElBQVosQ0FBaUI7QUFDZix3QkFBbUIsUUFBUSxDQUFSLENBREo7QUFFZiw2QkFBbUIsR0FBRyxLQUFILENBQVMsSUFBVCxDQUFjLE9BQWQsRUFBdUIsQ0FBdkIsS0FBNkIsRUFGakM7QUFHZiwyQkFBbUIsT0FISjtBQUlmLGtDQUFtQjtBQUpKLGVBQWpCO0FBTUQ7QUFDRCx5QkFBYSxPQUFPLFdBQVAsQ0FBbUIsS0FBaEM7QUFDQSxtQkFBTyxXQUFQLENBQW1CLEtBQW5CLEdBQTJCLFdBQVcsT0FBTyxXQUFQLENBQW1CLE9BQTlCLEVBQXVDLEdBQXZDLENBQTNCO0FBQ0QsV0FyQlU7QUFzQlgsbUJBQVMsWUFBVztBQUNsQixnQkFDRSxRQUFRLFNBQVMsSUFBVCxHQUFnQixHQUQxQjtBQUFBLGdCQUVFLFlBQVksQ0FGZDtBQUlBLG1CQUFPLEtBQVA7QUFDQSx5QkFBYSxPQUFPLFdBQVAsQ0FBbUIsS0FBaEM7QUFDQSxjQUFFLElBQUYsQ0FBTyxXQUFQLEVBQW9CLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjtBQUN4QywyQkFBYSxLQUFLLGdCQUFMLENBQWI7QUFDRCxhQUZEO0FBR0EscUJBQVMsTUFBTSxTQUFOLEdBQWtCLElBQTNCO0FBQ0EsZ0JBQUcsY0FBSCxFQUFtQjtBQUNqQix1QkFBUyxRQUFRLGNBQVIsR0FBeUIsSUFBbEM7QUFDRDtBQUNELGdCQUFJLENBQUMsUUFBUSxLQUFSLEtBQWtCLFNBQWxCLElBQStCLFFBQVEsS0FBUixLQUFrQixTQUFsRCxLQUFnRSxZQUFZLE1BQVosR0FBcUIsQ0FBekYsRUFBNEY7QUFDMUYsc0JBQVEsY0FBUixDQUF1QixLQUF2QjtBQUNBLGtCQUFHLFFBQVEsS0FBWCxFQUFrQjtBQUNoQix3QkFBUSxLQUFSLENBQWMsV0FBZDtBQUNELGVBRkQsTUFHSztBQUNILGtCQUFFLElBQUYsQ0FBTyxXQUFQLEVBQW9CLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjtBQUN4QywwQkFBUSxHQUFSLENBQVksS0FBSyxNQUFMLElBQWUsSUFBZixHQUFzQixLQUFLLGdCQUFMLENBQXRCLEdBQTZDLElBQXpEO0FBQ0QsaUJBRkQ7QUFHRDtBQUNELHNCQUFRLFFBQVI7QUFDRDtBQUNELDBCQUFjLEVBQWQ7QUFDRDtBQWpEVSxTQWpuQkw7QUFvcUJSLGdCQUFRLFVBQVMsS0FBVCxFQUFnQixlQUFoQixFQUFpQyxPQUFqQyxFQUEwQztBQUNoRCxjQUNFLFNBQVMsUUFEWDtBQUFBLGNBRUUsUUFGRjtBQUFBLGNBR0UsS0FIRjtBQUFBLGNBSUUsUUFKRjtBQU1BLDRCQUFrQixtQkFBbUIsY0FBckM7QUFDQSxvQkFBa0IsV0FBbUIsT0FBckM7QUFDQSxjQUFHLE9BQU8sS0FBUCxJQUFnQixRQUFoQixJQUE0QixXQUFXLFNBQTFDLEVBQXFEO0FBQ25ELG9CQUFXLE1BQU0sS0FBTixDQUFZLE9BQVosQ0FBWDtBQUNBLHVCQUFXLE1BQU0sTUFBTixHQUFlLENBQTFCO0FBQ0EsY0FBRSxJQUFGLENBQU8sS0FBUCxFQUFjLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUF1QjtBQUNuQyxrQkFBSSxpQkFBa0IsU0FBUyxRQUFWLEdBQ2pCLFFBQVEsTUFBTSxRQUFRLENBQWQsRUFBaUIsTUFBakIsQ0FBd0IsQ0FBeEIsRUFBMkIsV0FBM0IsRUFBUixHQUFtRCxNQUFNLFFBQVEsQ0FBZCxFQUFpQixLQUFqQixDQUF1QixDQUF2QixDQURsQyxHQUVqQixLQUZKO0FBSUEsa0JBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sY0FBUCxDQUFqQixLQUE4QyxTQUFTLFFBQTNELEVBQXVFO0FBQ3JFLHlCQUFTLE9BQU8sY0FBUCxDQUFUO0FBQ0QsZUFGRCxNQUdLLElBQUksT0FBTyxjQUFQLE1BQTJCLFNBQS9CLEVBQTJDO0FBQzlDLHdCQUFRLE9BQU8sY0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQSxJQUFJLEVBQUUsYUFBRixDQUFpQixPQUFPLEtBQVAsQ0FBakIsS0FBcUMsU0FBUyxRQUFsRCxFQUE4RDtBQUNqRSx5QkFBUyxPQUFPLEtBQVAsQ0FBVDtBQUNELGVBRkksTUFHQSxJQUFJLE9BQU8sS0FBUCxNQUFrQixTQUF0QixFQUFrQztBQUNyQyx3QkFBUSxPQUFPLEtBQVAsQ0FBUjtBQUNBLHVCQUFPLEtBQVA7QUFDRCxlQUhJLE1BSUE7QUFDSCx1QkFBTyxLQUFQO0FBQ0Q7QUFDRixhQXRCRDtBQXVCRDtBQUNELGNBQUssRUFBRSxVQUFGLENBQWMsS0FBZCxDQUFMLEVBQTZCO0FBQzNCLHVCQUFXLE1BQU0sS0FBTixDQUFZLE9BQVosRUFBcUIsZUFBckIsQ0FBWDtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQix1QkFBVyxLQUFYO0FBQ0Q7QUFDRCxjQUFHLEVBQUUsT0FBRixDQUFVLGFBQVYsQ0FBSCxFQUE2QjtBQUMzQiwwQkFBYyxJQUFkLENBQW1CLFFBQW5CO0FBQ0QsV0FGRCxNQUdLLElBQUcsa0JBQWtCLFNBQXJCLEVBQWdDO0FBQ25DLDRCQUFnQixDQUFDLGFBQUQsRUFBZ0IsUUFBaEIsQ0FBaEI7QUFDRCxXQUZJLE1BR0EsSUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQzlCLDRCQUFnQixRQUFoQjtBQUNEO0FBQ0QsaUJBQU8sS0FBUDtBQUNEO0FBeHRCTyxPQUFWOztBQTJ0QkEsVUFBRyxhQUFILEVBQWtCO0FBQ2hCLFlBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUN6QixpQkFBTyxVQUFQO0FBQ0Q7QUFDRCxlQUFPLE1BQVAsQ0FBYyxLQUFkO0FBQ0QsT0FMRCxNQU1LO0FBQ0gsWUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLG1CQUFTLE1BQVQsQ0FBZ0IsU0FBaEI7QUFDRDtBQUNELGVBQU8sVUFBUDtBQUNEO0FBQ0YsS0F4d0JIOztBQTJ3QkEsV0FBUSxrQkFBa0IsU0FBbkIsR0FDSCxhQURHLEdBRUgsSUFGSjtBQUlELEdBeHlCRDs7QUEweUJBLElBQUUsRUFBRixDQUFLLEtBQUwsQ0FBVyxRQUFYLEdBQXNCOztBQUVwQixVQUFpQixPQUZHO0FBR3BCLGVBQWlCLE9BSEc7O0FBS3BCLFlBQWlCLEtBTEc7QUFNcEIsV0FBaUIsS0FORztBQU9wQixhQUFpQixLQVBHO0FBUXBCLGlCQUFpQixJQVJHOztBQVVwQixvQkFBaUIsS0FWRzs7QUFZcEIsbUJBQWlCLEtBWkc7QUFhcEIsZ0JBQWlCLElBYkc7QUFjcEIsY0FBaUIsSUFkRztBQWVwQixlQUFpQixJQWZHOztBQWlCcEIsY0FBaUIsS0FqQkc7QUFrQnBCLGNBQWlCLEtBbEJHOztBQW9CcEIsb0JBQWlCO0FBQ2YsZ0JBQVcsS0FESTtBQUVmLGNBQVc7QUFGSSxLQXBCRzs7QUEwQnBCLGFBQWEsTUExQk87O0FBNEJwQixXQUFhLEtBNUJPO0FBNkJwQixjQUFhLEdBN0JPO0FBOEJwQixZQUFhLENBOUJPO0FBK0JwQixnQkFBYSxPQS9CTzs7O0FBa0NwQixhQUFhLEVBbENPOzs7QUFxQ3BCLFlBQWEsWUFBVSxDQUFFLENBckNMOzs7QUF3Q3BCLGVBQWEsWUFBVSxDQUFFLENBeENMOzs7QUEyQ3BCLFlBQWEsWUFBVTtBQUFFLGFBQU8sSUFBUDtBQUFjLEtBM0NuQjs7O0FBOENwQixjQUFhLFlBQVUsQ0FBRSxDQTlDTDs7O0FBaURwQixlQUFhLFlBQVU7QUFBRSxhQUFPLElBQVA7QUFBYyxLQWpEbkI7OztBQW9EcEIsWUFBYSxZQUFVO0FBQUUsYUFBTyxJQUFQO0FBQWMsS0FwRG5COztBQXNEcEIsY0FBYztBQUNaLGFBQVcsVUFEQztBQUVaLGVBQVcscURBRkM7QUFHWixZQUFXLHNEQUhDO0FBSVosYUFBVztBQUpDLEtBdERNO0FBNERwQixXQUFRO0FBQ04sY0FBWSw4REFETjtBQUVOLGNBQVksdUNBRk47QUFHTixnQkFBWTtBQUhOLEtBNURZO0FBaUVwQixlQUFZO0FBQ1YsY0FBYSxRQURIO0FBRVYsaUJBQWEsV0FGSDtBQUdWLGdCQUFhLFVBSEg7QUFJVixpQkFBYSxXQUpIO0FBS1Ysa0JBQWE7QUFMSDtBQWpFUSxHQUF0QjtBQTJFQyxDQWg0QkEsRUFnNEJHLE1BaDRCSCxFQWc0QlcsTUFoNEJYLEVBZzRCbUIsUUFoNEJuQiIsImZpbGUiOiJtb2RhbC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxuICogIyBTZW1hbnRpYyBVSSAtIE1vZGFsXG4gKiBodHRwOi8vZ2l0aHViLmNvbS9zZW1hbnRpYy1vcmcvc2VtYW50aWMtdWkvXG4gKlxuICpcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxuICogaHR0cDovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVFxuICpcbiAqL1xuXG47KGZ1bmN0aW9uICgkLCB3aW5kb3csIGRvY3VtZW50LCB1bmRlZmluZWQpIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cbndpbmRvdyA9ICh0eXBlb2Ygd2luZG93ICE9ICd1bmRlZmluZWQnICYmIHdpbmRvdy5NYXRoID09IE1hdGgpXG4gID8gd2luZG93XG4gIDogKHR5cGVvZiBzZWxmICE9ICd1bmRlZmluZWQnICYmIHNlbGYuTWF0aCA9PSBNYXRoKVxuICAgID8gc2VsZlxuICAgIDogRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKVxuO1xuXG4kLmZuLm1vZGFsID0gZnVuY3Rpb24ocGFyYW1ldGVycykge1xuICB2YXJcbiAgICAkYWxsTW9kdWxlcyAgICA9ICQodGhpcyksXG4gICAgJHdpbmRvdyAgICAgICAgPSAkKHdpbmRvdyksXG4gICAgJGRvY3VtZW50ICAgICAgPSAkKGRvY3VtZW50KSxcbiAgICAkYm9keSAgICAgICAgICA9ICQoJ2JvZHknKSxcblxuICAgIG1vZHVsZVNlbGVjdG9yID0gJGFsbE1vZHVsZXMuc2VsZWN0b3IgfHwgJycsXG5cbiAgICB0aW1lICAgICAgICAgICA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpLFxuICAgIHBlcmZvcm1hbmNlICAgID0gW10sXG5cbiAgICBxdWVyeSAgICAgICAgICA9IGFyZ3VtZW50c1swXSxcbiAgICBtZXRob2RJbnZva2VkICA9ICh0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycpLFxuICAgIHF1ZXJ5QXJndW1lbnRzID0gW10uc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpLFxuXG4gICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lID0gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgfHwgd2luZG93Lm1velJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgfHwgd2luZG93LndlYmtpdFJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgfHwgd2luZG93Lm1zUmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gICAgICB8fCBmdW5jdGlvbihjYWxsYmFjaykgeyBzZXRUaW1lb3V0KGNhbGxiYWNrLCAwKTsgfSxcblxuICAgIHJldHVybmVkVmFsdWVcbiAgO1xuXG4gICRhbGxNb2R1bGVzXG4gICAgLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICB2YXJcbiAgICAgICAgc2V0dGluZ3MgICAgPSAoICQuaXNQbGFpbk9iamVjdChwYXJhbWV0ZXJzKSApXG4gICAgICAgICAgPyAkLmV4dGVuZCh0cnVlLCB7fSwgJC5mbi5tb2RhbC5zZXR0aW5ncywgcGFyYW1ldGVycylcbiAgICAgICAgICA6ICQuZXh0ZW5kKHt9LCAkLmZuLm1vZGFsLnNldHRpbmdzKSxcblxuICAgICAgICBzZWxlY3RvciAgICAgICAgPSBzZXR0aW5ncy5zZWxlY3RvcixcbiAgICAgICAgY2xhc3NOYW1lICAgICAgID0gc2V0dGluZ3MuY2xhc3NOYW1lLFxuICAgICAgICBuYW1lc3BhY2UgICAgICAgPSBzZXR0aW5ncy5uYW1lc3BhY2UsXG4gICAgICAgIGVycm9yICAgICAgICAgICA9IHNldHRpbmdzLmVycm9yLFxuXG4gICAgICAgIGV2ZW50TmFtZXNwYWNlICA9ICcuJyArIG5hbWVzcGFjZSxcbiAgICAgICAgbW9kdWxlTmFtZXNwYWNlID0gJ21vZHVsZS0nICsgbmFtZXNwYWNlLFxuXG4gICAgICAgICRtb2R1bGUgICAgICAgICA9ICQodGhpcyksXG4gICAgICAgICRjb250ZXh0ICAgICAgICA9ICQoc2V0dGluZ3MuY29udGV4dCksXG4gICAgICAgICRjbG9zZSAgICAgICAgICA9ICRtb2R1bGUuZmluZChzZWxlY3Rvci5jbG9zZSksXG5cbiAgICAgICAgJGFsbE1vZGFscyxcbiAgICAgICAgJG90aGVyTW9kYWxzLFxuICAgICAgICAkZm9jdXNlZEVsZW1lbnQsXG4gICAgICAgICRkaW1tYWJsZSxcbiAgICAgICAgJGRpbW1lcixcblxuICAgICAgICBlbGVtZW50ICAgICAgICAgPSB0aGlzLFxuICAgICAgICBpbnN0YW5jZSAgICAgICAgPSAkbW9kdWxlLmRhdGEobW9kdWxlTmFtZXNwYWNlKSxcblxuICAgICAgICBlbGVtZW50RXZlbnROYW1lc3BhY2UsXG4gICAgICAgIGlkLFxuICAgICAgICBvYnNlcnZlcixcbiAgICAgICAgbW9kdWxlXG4gICAgICA7XG4gICAgICBtb2R1bGUgID0ge1xuXG4gICAgICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdJbml0aWFsaXppbmcgZGltbWVyJywgJGNvbnRleHQpO1xuXG4gICAgICAgICAgbW9kdWxlLmNyZWF0ZS5pZCgpO1xuICAgICAgICAgIG1vZHVsZS5jcmVhdGUuZGltbWVyKCk7XG4gICAgICAgICAgbW9kdWxlLnJlZnJlc2hNb2RhbHMoKTtcblxuICAgICAgICAgIG1vZHVsZS5iaW5kLmV2ZW50cygpO1xuICAgICAgICAgIGlmKHNldHRpbmdzLm9ic2VydmVDaGFuZ2VzKSB7XG4gICAgICAgICAgICBtb2R1bGUub2JzZXJ2ZUNoYW5nZXMoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgbW9kdWxlLmluc3RhbnRpYXRlKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5zdGFudGlhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTdG9yaW5nIGluc3RhbmNlIG9mIG1vZGFsJyk7XG4gICAgICAgICAgaW5zdGFuY2UgPSBtb2R1bGU7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLmRhdGEobW9kdWxlTmFtZXNwYWNlLCBpbnN0YW5jZSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY3JlYXRlOiB7XG4gICAgICAgICAgZGltbWVyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBkZWZhdWx0U2V0dGluZ3MgPSB7XG4gICAgICAgICAgICAgICAgZGVidWcgICAgICA6IHNldHRpbmdzLmRlYnVnLFxuICAgICAgICAgICAgICAgIGRpbW1lck5hbWUgOiAnbW9kYWxzJyxcbiAgICAgICAgICAgICAgICBkdXJhdGlvbiAgIDoge1xuICAgICAgICAgICAgICAgICAgc2hvdyAgICAgOiBzZXR0aW5ncy5kdXJhdGlvbixcbiAgICAgICAgICAgICAgICAgIGhpZGUgICAgIDogc2V0dGluZ3MuZHVyYXRpb25cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGRpbW1lclNldHRpbmdzID0gJC5leHRlbmQodHJ1ZSwgZGVmYXVsdFNldHRpbmdzLCBzZXR0aW5ncy5kaW1tZXJTZXR0aW5ncylcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmludmVydGVkKSB7XG4gICAgICAgICAgICAgIGRpbW1lclNldHRpbmdzLnZhcmlhdGlvbiA9IChkaW1tZXJTZXR0aW5ncy52YXJpYXRpb24gIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICA/IGRpbW1lclNldHRpbmdzLnZhcmlhdGlvbiArICcgaW52ZXJ0ZWQnXG4gICAgICAgICAgICAgICAgOiAnaW52ZXJ0ZWQnXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCQuZm4uZGltbWVyID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLmRpbW1lcik7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ3JlYXRpbmcgZGltbWVyIHdpdGggc2V0dGluZ3MnLCBkaW1tZXJTZXR0aW5ncyk7XG4gICAgICAgICAgICAkZGltbWFibGUgPSAkY29udGV4dC5kaW1tZXIoZGltbWVyU2V0dGluZ3MpO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MuZGV0YWNoYWJsZSkge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnTW9kYWwgaXMgZGV0YWNoYWJsZSwgbW92aW5nIGNvbnRlbnQgaW50byBkaW1tZXInKTtcbiAgICAgICAgICAgICAgJGRpbW1hYmxlLmRpbW1lcignYWRkIGNvbnRlbnQnLCAkbW9kdWxlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0LnVuZGV0YWNoZWQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmJsdXJyaW5nKSB7XG4gICAgICAgICAgICAgICRkaW1tYWJsZS5hZGRDbGFzcyhjbGFzc05hbWUuYmx1cnJpbmcpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgJGRpbW1lciA9ICRkaW1tYWJsZS5kaW1tZXIoJ2dldCBkaW1tZXInKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGlkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlkID0gKE1hdGgucmFuZG9tKCkudG9TdHJpbmcoMTYpICsgJzAwMDAwMDAwMCcpLnN1YnN0cigyLDgpO1xuICAgICAgICAgICAgZWxlbWVudEV2ZW50TmFtZXNwYWNlID0gJy4nICsgaWQ7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQ3JlYXRpbmcgdW5pcXVlIGlkIGZvciBlbGVtZW50JywgaWQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBkZXN0cm95OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnRGVzdHJveWluZyBwcmV2aW91cyBtb2RhbCcpO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5yZW1vdmVEYXRhKG1vZHVsZU5hbWVzcGFjZSlcbiAgICAgICAgICAgIC5vZmYoZXZlbnROYW1lc3BhY2UpXG4gICAgICAgICAgO1xuICAgICAgICAgICR3aW5kb3cub2ZmKGVsZW1lbnRFdmVudE5hbWVzcGFjZSk7XG4gICAgICAgICAgJGRpbW1lci5vZmYoZWxlbWVudEV2ZW50TmFtZXNwYWNlKTtcbiAgICAgICAgICAkY2xvc2Uub2ZmKGV2ZW50TmFtZXNwYWNlKTtcbiAgICAgICAgICAkY29udGV4dC5kaW1tZXIoJ2Rlc3Ryb3knKTtcbiAgICAgICAgfSxcblxuICAgICAgICBvYnNlcnZlQ2hhbmdlczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoJ011dGF0aW9uT2JzZXJ2ZXInIGluIHdpbmRvdykge1xuICAgICAgICAgICAgb2JzZXJ2ZXIgPSBuZXcgTXV0YXRpb25PYnNlcnZlcihmdW5jdGlvbihtdXRhdGlvbnMpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdET00gdHJlZSBtb2RpZmllZCwgcmVmcmVzaGluZycpO1xuICAgICAgICAgICAgICBtb2R1bGUucmVmcmVzaCgpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBvYnNlcnZlci5vYnNlcnZlKGVsZW1lbnQsIHtcbiAgICAgICAgICAgICAgY2hpbGRMaXN0IDogdHJ1ZSxcbiAgICAgICAgICAgICAgc3VidHJlZSAgIDogdHJ1ZVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NldHRpbmcgdXAgbXV0YXRpb24gb2JzZXJ2ZXInLCBvYnNlcnZlcik7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlZnJlc2g6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5yZW1vdmUuc2Nyb2xsaW5nKCk7XG4gICAgICAgICAgbW9kdWxlLmNhY2hlU2l6ZXMoKTtcbiAgICAgICAgICBtb2R1bGUuc2V0LnNjcmVlbkhlaWdodCgpO1xuICAgICAgICAgIG1vZHVsZS5zZXQudHlwZSgpO1xuICAgICAgICAgIG1vZHVsZS5zZXQucG9zaXRpb24oKTtcbiAgICAgICAgfSxcblxuICAgICAgICByZWZyZXNoTW9kYWxzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAkb3RoZXJNb2RhbHMgPSAkbW9kdWxlLnNpYmxpbmdzKHNlbGVjdG9yLm1vZGFsKTtcbiAgICAgICAgICAkYWxsTW9kYWxzICAgPSAkb3RoZXJNb2RhbHMuYWRkKCRtb2R1bGUpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGF0dGFjaEV2ZW50czogZnVuY3Rpb24oc2VsZWN0b3IsIGV2ZW50KSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICAkdG9nZ2xlID0gJChzZWxlY3RvcilcbiAgICAgICAgICA7XG4gICAgICAgICAgZXZlbnQgPSAkLmlzRnVuY3Rpb24obW9kdWxlW2V2ZW50XSlcbiAgICAgICAgICAgID8gbW9kdWxlW2V2ZW50XVxuICAgICAgICAgICAgOiBtb2R1bGUudG9nZ2xlXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKCR0b2dnbGUubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdBdHRhY2hpbmcgbW9kYWwgZXZlbnRzIHRvIGVsZW1lbnQnLCBzZWxlY3RvciwgZXZlbnQpO1xuICAgICAgICAgICAgJHRvZ2dsZVxuICAgICAgICAgICAgICAub2ZmKGV2ZW50TmFtZXNwYWNlKVxuICAgICAgICAgICAgICAub24oJ2NsaWNrJyArIGV2ZW50TmFtZXNwYWNlLCBldmVudClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3Iubm90Rm91bmQsIHNlbGVjdG9yKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgYmluZDoge1xuICAgICAgICAgIGV2ZW50czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQXR0YWNoaW5nIGV2ZW50cycpO1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAub24oJ2NsaWNrJyArIGV2ZW50TmFtZXNwYWNlLCBzZWxlY3Rvci5jbG9zZSwgbW9kdWxlLmV2ZW50LmNsb3NlKVxuICAgICAgICAgICAgICAub24oJ2NsaWNrJyArIGV2ZW50TmFtZXNwYWNlLCBzZWxlY3Rvci5hcHByb3ZlLCBtb2R1bGUuZXZlbnQuYXBwcm92ZSlcbiAgICAgICAgICAgICAgLm9uKCdjbGljaycgKyBldmVudE5hbWVzcGFjZSwgc2VsZWN0b3IuZGVueSwgbW9kdWxlLmV2ZW50LmRlbnkpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICAkd2luZG93XG4gICAgICAgICAgICAgIC5vbigncmVzaXplJyArIGVsZW1lbnRFdmVudE5hbWVzcGFjZSwgbW9kdWxlLmV2ZW50LnJlc2l6ZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0OiB7XG4gICAgICAgICAgaWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIChNYXRoLnJhbmRvbSgpLnRvU3RyaW5nKDE2KSArICcwMDAwMDAwMDAnKS5zdWJzdHIoMiw4KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZXZlbnQ6IHtcbiAgICAgICAgICBhcHByb3ZlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLm9uQXBwcm92ZS5jYWxsKGVsZW1lbnQsICQodGhpcykpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQXBwcm92ZSBjYWxsYmFjayByZXR1cm5lZCBmYWxzZSBjYW5jZWxsaW5nIGhpZGUnKTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLmhpZGUoKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRlbnk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3Mub25EZW55LmNhbGwoZWxlbWVudCwgJCh0aGlzKSkgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdEZW55IGNhbGxiYWNrIHJldHVybmVkIGZhbHNlIGNhbmNlbGxpbmcgaGlkZScpO1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUuaGlkZSgpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgY2xvc2U6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLmhpZGUoKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICR0YXJnZXQgICA9ICQoZXZlbnQudGFyZ2V0KSxcbiAgICAgICAgICAgICAgaXNJbk1vZGFsID0gKCR0YXJnZXQuY2xvc2VzdChzZWxlY3Rvci5tb2RhbCkubGVuZ3RoID4gMCksXG4gICAgICAgICAgICAgIGlzSW5ET00gICA9ICQuY29udGFpbnMoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCBldmVudC50YXJnZXQpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZighaXNJbk1vZGFsICYmIGlzSW5ET00pIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdEaW1tZXIgY2xpY2tlZCwgaGlkaW5nIGFsbCBtb2RhbHMnKTtcbiAgICAgICAgICAgICAgaWYoIG1vZHVsZS5pcy5hY3RpdmUoKSApIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUucmVtb3ZlLmNsaWNrYXdheSgpO1xuICAgICAgICAgICAgICAgIGlmKHNldHRpbmdzLmFsbG93TXVsdGlwbGUpIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5oaWRlKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmhpZGVBbGwoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGRlYm91bmNlOiBmdW5jdGlvbihtZXRob2QsIGRlbGF5KSB7XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnRpbWVyKTtcbiAgICAgICAgICAgIG1vZHVsZS50aW1lciA9IHNldFRpbWVvdXQobWV0aG9kLCBkZWxheSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBrZXlib2FyZDogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBrZXlDb2RlICAgPSBldmVudC53aGljaCxcbiAgICAgICAgICAgICAgZXNjYXBlS2V5ID0gMjdcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKGtleUNvZGUgPT0gZXNjYXBlS2V5KSB7XG4gICAgICAgICAgICAgIGlmKHNldHRpbmdzLmNsb3NhYmxlKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdFc2NhcGUga2V5IHByZXNzZWQgaGlkaW5nIG1vZGFsJyk7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmhpZGUoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0VzY2FwZSBrZXkgcHJlc3NlZCwgYnV0IGNsb3NhYmxlIGlzIHNldCB0byBmYWxzZScpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICByZXNpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoICRkaW1tYWJsZS5kaW1tZXIoJ2lzIGFjdGl2ZScpICkge1xuICAgICAgICAgICAgICByZXF1ZXN0QW5pbWF0aW9uRnJhbWUobW9kdWxlLnJlZnJlc2gpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICB0b2dnbGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCBtb2R1bGUuaXMuYWN0aXZlKCkgfHwgbW9kdWxlLmlzLmFuaW1hdGluZygpICkge1xuICAgICAgICAgICAgbW9kdWxlLmhpZGUoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBtb2R1bGUuc2hvdygpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzaG93OiBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICAgIGNhbGxiYWNrID0gJC5pc0Z1bmN0aW9uKGNhbGxiYWNrKVxuICAgICAgICAgICAgPyBjYWxsYmFja1xuICAgICAgICAgICAgOiBmdW5jdGlvbigpe31cbiAgICAgICAgICA7XG4gICAgICAgICAgbW9kdWxlLnJlZnJlc2hNb2RhbHMoKTtcbiAgICAgICAgICBtb2R1bGUuc2hvd01vZGFsKGNhbGxiYWNrKTtcbiAgICAgICAgfSxcblxuICAgICAgICBoaWRlOiBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICAgIGNhbGxiYWNrID0gJC5pc0Z1bmN0aW9uKGNhbGxiYWNrKVxuICAgICAgICAgICAgPyBjYWxsYmFja1xuICAgICAgICAgICAgOiBmdW5jdGlvbigpe31cbiAgICAgICAgICA7XG4gICAgICAgICAgbW9kdWxlLnJlZnJlc2hNb2RhbHMoKTtcbiAgICAgICAgICBtb2R1bGUuaGlkZU1vZGFsKGNhbGxiYWNrKTtcbiAgICAgICAgfSxcblxuICAgICAgICBzaG93TW9kYWw6IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG4gICAgICAgICAgY2FsbGJhY2sgPSAkLmlzRnVuY3Rpb24oY2FsbGJhY2spXG4gICAgICAgICAgICA/IGNhbGxiYWNrXG4gICAgICAgICAgICA6IGZ1bmN0aW9uKCl7fVxuICAgICAgICAgIDtcbiAgICAgICAgICBpZiggbW9kdWxlLmlzLmFuaW1hdGluZygpIHx8ICFtb2R1bGUuaXMuYWN0aXZlKCkgKSB7XG5cbiAgICAgICAgICAgIG1vZHVsZS5zaG93RGltbWVyKCk7XG4gICAgICAgICAgICBtb2R1bGUuY2FjaGVTaXplcygpO1xuICAgICAgICAgICAgbW9kdWxlLnNldC5wb3NpdGlvbigpO1xuICAgICAgICAgICAgbW9kdWxlLnNldC5zY3JlZW5IZWlnaHQoKTtcbiAgICAgICAgICAgIG1vZHVsZS5zZXQudHlwZSgpO1xuICAgICAgICAgICAgbW9kdWxlLnNldC5jbGlja2F3YXkoKTtcblxuICAgICAgICAgICAgaWYoICFzZXR0aW5ncy5hbGxvd011bHRpcGxlICYmIG1vZHVsZS5vdGhlcnMuYWN0aXZlKCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5oaWRlT3RoZXJzKG1vZHVsZS5zaG93TW9kYWwpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHNldHRpbmdzLm9uU2hvdy5jYWxsKGVsZW1lbnQpO1xuICAgICAgICAgICAgICBpZihzZXR0aW5ncy50cmFuc2l0aW9uICYmICQuZm4udHJhbnNpdGlvbiAhPT0gdW5kZWZpbmVkICYmICRtb2R1bGUudHJhbnNpdGlvbignaXMgc3VwcG9ydGVkJykpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Nob3dpbmcgbW9kYWwgd2l0aCBjc3MgYW5pbWF0aW9ucycpO1xuICAgICAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgICAgIC50cmFuc2l0aW9uKHtcbiAgICAgICAgICAgICAgICAgICAgZGVidWcgICAgICAgOiBzZXR0aW5ncy5kZWJ1ZyxcbiAgICAgICAgICAgICAgICAgICAgYW5pbWF0aW9uICAgOiBzZXR0aW5ncy50cmFuc2l0aW9uICsgJyBpbicsXG4gICAgICAgICAgICAgICAgICAgIHF1ZXVlICAgICAgIDogc2V0dGluZ3MucXVldWUsXG4gICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uICAgIDogc2V0dGluZ3MuZHVyYXRpb24sXG4gICAgICAgICAgICAgICAgICAgIHVzZUZhaWxTYWZlIDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgb25Db21wbGV0ZSA6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgIHNldHRpbmdzLm9uVmlzaWJsZS5hcHBseShlbGVtZW50KTtcbiAgICAgICAgICAgICAgICAgICAgICBtb2R1bGUuYWRkLmtleWJvYXJkU2hvcnRjdXRzKCk7XG4gICAgICAgICAgICAgICAgICAgICAgbW9kdWxlLnNhdmUuZm9jdXMoKTtcbiAgICAgICAgICAgICAgICAgICAgICBtb2R1bGUuc2V0LmFjdGl2ZSgpO1xuICAgICAgICAgICAgICAgICAgICAgIGlmKHNldHRpbmdzLmF1dG9mb2N1cykge1xuICAgICAgICAgICAgICAgICAgICAgICAgbW9kdWxlLnNldC5hdXRvZm9jdXMoKTtcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm5vVHJhbnNpdGlvbik7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ01vZGFsIGlzIGFscmVhZHkgdmlzaWJsZScpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBoaWRlTW9kYWw6IGZ1bmN0aW9uKGNhbGxiYWNrLCBrZWVwRGltbWVkKSB7XG4gICAgICAgICAgY2FsbGJhY2sgPSAkLmlzRnVuY3Rpb24oY2FsbGJhY2spXG4gICAgICAgICAgICA/IGNhbGxiYWNrXG4gICAgICAgICAgICA6IGZ1bmN0aW9uKCl7fVxuICAgICAgICAgIDtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0hpZGluZyBtb2RhbCcpO1xuICAgICAgICAgIGlmKHNldHRpbmdzLm9uSGlkZS5jYWxsKGVsZW1lbnQsICQodGhpcykpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0hpZGUgY2FsbGJhY2sgcmV0dXJuZWQgZmFsc2UgY2FuY2VsbGluZyBoaWRlJyk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYoIG1vZHVsZS5pcy5hbmltYXRpbmcoKSB8fCBtb2R1bGUuaXMuYWN0aXZlKCkgKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy50cmFuc2l0aW9uICYmICQuZm4udHJhbnNpdGlvbiAhPT0gdW5kZWZpbmVkICYmICRtb2R1bGUudHJhbnNpdGlvbignaXMgc3VwcG9ydGVkJykpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5hY3RpdmUoKTtcbiAgICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAgIC50cmFuc2l0aW9uKHtcbiAgICAgICAgICAgICAgICAgIGRlYnVnICAgICAgIDogc2V0dGluZ3MuZGVidWcsXG4gICAgICAgICAgICAgICAgICBhbmltYXRpb24gICA6IHNldHRpbmdzLnRyYW5zaXRpb24gKyAnIG91dCcsXG4gICAgICAgICAgICAgICAgICBxdWV1ZSAgICAgICA6IHNldHRpbmdzLnF1ZXVlLFxuICAgICAgICAgICAgICAgICAgZHVyYXRpb24gICAgOiBzZXR0aW5ncy5kdXJhdGlvbixcbiAgICAgICAgICAgICAgICAgIHVzZUZhaWxTYWZlIDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgIG9uU3RhcnQgICAgIDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKCFtb2R1bGUub3RoZXJzLmFjdGl2ZSgpICYmICFrZWVwRGltbWVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgbW9kdWxlLmhpZGVEaW1tZXIoKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBtb2R1bGUucmVtb3ZlLmtleWJvYXJkU2hvcnRjdXRzKCk7XG4gICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgb25Db21wbGV0ZSA6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICBzZXR0aW5ncy5vbkhpZGRlbi5jYWxsKGVsZW1lbnQpO1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUucmVzdG9yZS5mb2N1cygpO1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3Iubm9UcmFuc2l0aW9uKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2hvd0RpbW1lcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoJGRpbW1hYmxlLmRpbW1lcignaXMgYW5pbWF0aW5nJykgfHwgISRkaW1tYWJsZS5kaW1tZXIoJ2lzIGFjdGl2ZScpICkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTaG93aW5nIGRpbW1lcicpO1xuICAgICAgICAgICAgJGRpbW1hYmxlLmRpbW1lcignc2hvdycpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRGltbWVyIGFscmVhZHkgdmlzaWJsZScpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBoaWRlRGltbWVyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZiggJGRpbW1hYmxlLmRpbW1lcignaXMgYW5pbWF0aW5nJykgfHwgKCRkaW1tYWJsZS5kaW1tZXIoJ2lzIGFjdGl2ZScpKSApIHtcbiAgICAgICAgICAgICRkaW1tYWJsZS5kaW1tZXIoJ2hpZGUnLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5jbGlja2F3YXkoKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5zY3JlZW5IZWlnaHQoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRGltbWVyIGlzIG5vdCB2aXNpYmxlIGNhbm5vdCBoaWRlJyk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGhpZGVBbGw6IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICAkdmlzaWJsZU1vZGFscyA9ICRhbGxNb2RhbHMuZmlsdGVyKCcuJyArIGNsYXNzTmFtZS5hY3RpdmUgKyAnLCAuJyArIGNsYXNzTmFtZS5hbmltYXRpbmcpXG4gICAgICAgICAgO1xuICAgICAgICAgIGNhbGxiYWNrID0gJC5pc0Z1bmN0aW9uKGNhbGxiYWNrKVxuICAgICAgICAgICAgPyBjYWxsYmFja1xuICAgICAgICAgICAgOiBmdW5jdGlvbigpe31cbiAgICAgICAgICA7XG4gICAgICAgICAgaWYoICR2aXNpYmxlTW9kYWxzLmxlbmd0aCA+IDAgKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0hpZGluZyBhbGwgdmlzaWJsZSBtb2RhbHMnKTtcbiAgICAgICAgICAgIG1vZHVsZS5oaWRlRGltbWVyKCk7XG4gICAgICAgICAgICAkdmlzaWJsZU1vZGFsc1xuICAgICAgICAgICAgICAubW9kYWwoJ2hpZGUgbW9kYWwnLCBjYWxsYmFjaylcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgaGlkZU90aGVyczogZnVuY3Rpb24oY2FsbGJhY2spIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgICR2aXNpYmxlTW9kYWxzID0gJG90aGVyTW9kYWxzLmZpbHRlcignLicgKyBjbGFzc05hbWUuYWN0aXZlICsgJywgLicgKyBjbGFzc05hbWUuYW5pbWF0aW5nKVxuICAgICAgICAgIDtcbiAgICAgICAgICBjYWxsYmFjayA9ICQuaXNGdW5jdGlvbihjYWxsYmFjaylcbiAgICAgICAgICAgID8gY2FsbGJhY2tcbiAgICAgICAgICAgIDogZnVuY3Rpb24oKXt9XG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKCAkdmlzaWJsZU1vZGFscy5sZW5ndGggPiAwICkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdIaWRpbmcgb3RoZXIgbW9kYWxzJywgJG90aGVyTW9kYWxzKTtcbiAgICAgICAgICAgICR2aXNpYmxlTW9kYWxzXG4gICAgICAgICAgICAgIC5tb2RhbCgnaGlkZSBtb2RhbCcsIGNhbGxiYWNrLCB0cnVlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBvdGhlcnM6IHtcbiAgICAgICAgICBhY3RpdmU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICgkb3RoZXJNb2RhbHMuZmlsdGVyKCcuJyArIGNsYXNzTmFtZS5hY3RpdmUpLmxlbmd0aCA+IDApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYW5pbWF0aW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAoJG90aGVyTW9kYWxzLmZpbHRlcignLicgKyBjbGFzc05hbWUuYW5pbWF0aW5nKS5sZW5ndGggPiAwKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cblxuICAgICAgICBhZGQ6IHtcbiAgICAgICAgICBrZXlib2FyZFNob3J0Y3V0czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQWRkaW5nIGtleWJvYXJkIHNob3J0Y3V0cycpO1xuICAgICAgICAgICAgJGRvY3VtZW50XG4gICAgICAgICAgICAgIC5vbigna2V5dXAnICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS5ldmVudC5rZXlib2FyZClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2F2ZToge1xuICAgICAgICAgIGZvY3VzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRmb2N1c2VkRWxlbWVudCA9ICQoZG9jdW1lbnQuYWN0aXZlRWxlbWVudCkuYmx1cigpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICByZXN0b3JlOiB7XG4gICAgICAgICAgZm9jdXM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoJGZvY3VzZWRFbGVtZW50ICYmICRmb2N1c2VkRWxlbWVudC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICRmb2N1c2VkRWxlbWVudC5mb2N1cygpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICByZW1vdmU6IHtcbiAgICAgICAgICBhY3RpdmU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZS5yZW1vdmVDbGFzcyhjbGFzc05hbWUuYWN0aXZlKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGNsaWNrYXdheTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5jbG9zYWJsZSkge1xuICAgICAgICAgICAgICAkZGltbWVyXG4gICAgICAgICAgICAgICAgLm9mZignY2xpY2snICsgZWxlbWVudEV2ZW50TmFtZXNwYWNlKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBib2R5U3R5bGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoJGJvZHkuYXR0cignc3R5bGUnKSA9PT0gJycpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1JlbW92aW5nIHN0eWxlIGF0dHJpYnV0ZScpO1xuICAgICAgICAgICAgICAkYm9keS5yZW1vdmVBdHRyKCdzdHlsZScpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgc2NyZWVuSGVpZ2h0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnUmVtb3ZpbmcgcGFnZSBoZWlnaHQnKTtcbiAgICAgICAgICAgICRib2R5XG4gICAgICAgICAgICAgIC5jc3MoJ2hlaWdodCcsICcnKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAga2V5Ym9hcmRTaG9ydGN1dHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1JlbW92aW5nIGtleWJvYXJkIHNob3J0Y3V0cycpO1xuICAgICAgICAgICAgJGRvY3VtZW50XG4gICAgICAgICAgICAgIC5vZmYoJ2tleXVwJyArIGV2ZW50TmFtZXNwYWNlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc2Nyb2xsaW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRkaW1tYWJsZS5yZW1vdmVDbGFzcyhjbGFzc05hbWUuc2Nyb2xsaW5nKTtcbiAgICAgICAgICAgICRtb2R1bGUucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLnNjcm9sbGluZyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGNhY2hlU2l6ZXM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgbW9kYWxIZWlnaHQgPSAkbW9kdWxlLm91dGVySGVpZ2h0KClcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYobW9kdWxlLmNhY2hlID09PSB1bmRlZmluZWQgfHwgbW9kYWxIZWlnaHQgIT09IDApIHtcbiAgICAgICAgICAgIG1vZHVsZS5jYWNoZSA9IHtcbiAgICAgICAgICAgICAgcGFnZUhlaWdodCAgICA6ICQoZG9jdW1lbnQpLm91dGVySGVpZ2h0KCksXG4gICAgICAgICAgICAgIGhlaWdodCAgICAgICAgOiBtb2RhbEhlaWdodCArIHNldHRpbmdzLm9mZnNldCxcbiAgICAgICAgICAgICAgY29udGV4dEhlaWdodCA6IChzZXR0aW5ncy5jb250ZXh0ID09ICdib2R5JylcbiAgICAgICAgICAgICAgICA/ICQod2luZG93KS5oZWlnaHQoKVxuICAgICAgICAgICAgICAgIDogJGRpbW1hYmxlLmhlaWdodCgpXG4gICAgICAgICAgICB9O1xuICAgICAgICAgIH1cbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NhY2hpbmcgbW9kYWwgYW5kIGNvbnRhaW5lciBzaXplcycsIG1vZHVsZS5jYWNoZSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2FuOiB7XG4gICAgICAgICAgZml0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAoICggbW9kdWxlLmNhY2hlLmhlaWdodCArIChzZXR0aW5ncy5wYWRkaW5nICogMikgKSA8IG1vZHVsZS5jYWNoZS5jb250ZXh0SGVpZ2h0KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgaXM6IHtcbiAgICAgICAgICBhY3RpdmU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRtb2R1bGUuaGFzQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBhbmltYXRpbmc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRtb2R1bGUudHJhbnNpdGlvbignaXMgc3VwcG9ydGVkJylcbiAgICAgICAgICAgICAgPyAkbW9kdWxlLnRyYW5zaXRpb24oJ2lzIGFuaW1hdGluZycpXG4gICAgICAgICAgICAgIDogJG1vZHVsZS5pcygnOnZpc2libGUnKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc2Nyb2xsaW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkZGltbWFibGUuaGFzQ2xhc3MoY2xhc3NOYW1lLnNjcm9sbGluZyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBtb2Rlcm5Ccm93c2VyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIC8vIGFwcE5hbWUgZm9yIElFMTEgcmVwb3J0cyAnTmV0c2NhcGUnIGNhbiBubyBsb25nZXIgdXNlXG4gICAgICAgICAgICByZXR1cm4gISh3aW5kb3cuQWN0aXZlWE9iamVjdCB8fCBcIkFjdGl2ZVhPYmplY3RcIiBpbiB3aW5kb3cpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZXQ6IHtcbiAgICAgICAgICBhdXRvZm9jdXM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICRpbnB1dHMgICAgPSAkbW9kdWxlLmZpbmQoJzppbnB1dCcpLmZpbHRlcignOnZpc2libGUnKSxcbiAgICAgICAgICAgICAgJGF1dG9mb2N1cyA9ICRpbnB1dHMuZmlsdGVyKCdbYXV0b2ZvY3VzXScpLFxuICAgICAgICAgICAgICAkaW5wdXQgICAgID0gKCRhdXRvZm9jdXMubGVuZ3RoID4gMClcbiAgICAgICAgICAgICAgICA/ICRhdXRvZm9jdXMuZmlyc3QoKVxuICAgICAgICAgICAgICAgIDogJGlucHV0cy5maXJzdCgpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZigkaW5wdXQubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAkaW5wdXQuZm9jdXMoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGNsaWNrYXdheTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5jbG9zYWJsZSkge1xuICAgICAgICAgICAgICAkZGltbWVyXG4gICAgICAgICAgICAgICAgLm9uKCdjbGljaycgKyBlbGVtZW50RXZlbnROYW1lc3BhY2UsIG1vZHVsZS5ldmVudC5jbGljaylcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgc2NyZWVuSGVpZ2h0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKCBtb2R1bGUuY2FuLmZpdCgpICkge1xuICAgICAgICAgICAgICAkYm9keS5jc3MoJ2hlaWdodCcsICcnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ01vZGFsIGlzIHRhbGxlciB0aGFuIHBhZ2UgY29udGVudCwgcmVzaXppbmcgcGFnZSBoZWlnaHQnKTtcbiAgICAgICAgICAgICAgJGJvZHlcbiAgICAgICAgICAgICAgICAuY3NzKCdoZWlnaHQnLCBtb2R1bGUuY2FjaGUuaGVpZ2h0ICsgKHNldHRpbmdzLnBhZGRpbmcgKiAyKSApXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGFjdGl2ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkbW9kdWxlLmFkZENsYXNzKGNsYXNzTmFtZS5hY3RpdmUpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc2Nyb2xsaW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRkaW1tYWJsZS5hZGRDbGFzcyhjbGFzc05hbWUuc2Nyb2xsaW5nKTtcbiAgICAgICAgICAgICRtb2R1bGUuYWRkQ2xhc3MoY2xhc3NOYW1lLnNjcm9sbGluZyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB0eXBlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKG1vZHVsZS5jYW4uZml0KCkpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ01vZGFsIGZpdHMgb24gc2NyZWVuJyk7XG4gICAgICAgICAgICAgIGlmKCFtb2R1bGUub3RoZXJzLmFjdGl2ZSgpICYmICFtb2R1bGUub3RoZXJzLmFuaW1hdGluZygpKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5zY3JvbGxpbmcoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdNb2RhbCBjYW5ub3QgZml0IG9uIHNjcmVlbiBzZXR0aW5nIHRvIHNjcm9sbGluZycpO1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0LnNjcm9sbGluZygpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgcG9zaXRpb246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0NlbnRlcmluZyBtb2RhbCBvbiBwYWdlJywgbW9kdWxlLmNhY2hlKTtcbiAgICAgICAgICAgIGlmKG1vZHVsZS5jYW4uZml0KCkpIHtcbiAgICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICAgdG9wOiAnJyxcbiAgICAgICAgICAgICAgICAgIG1hcmdpblRvcDogLShtb2R1bGUuY2FjaGUuaGVpZ2h0IC8gMilcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgICAgbWFyZ2luVG9wIDogJycsXG4gICAgICAgICAgICAgICAgICB0b3AgICAgICAgOiAkZG9jdW1lbnQuc2Nyb2xsVG9wKClcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICB1bmRldGFjaGVkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRkaW1tYWJsZS5hZGRDbGFzcyhjbGFzc05hbWUudW5kZXRhY2hlZCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldHRpbmc6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdDaGFuZ2luZyBzZXR0aW5nJywgbmFtZSwgdmFsdWUpO1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBzZXR0aW5ncywgbmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgaWYoJC5pc1BsYWluT2JqZWN0KHNldHRpbmdzW25hbWVdKSkge1xuICAgICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBzZXR0aW5nc1tuYW1lXSwgdmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHNldHRpbmdzW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHNldHRpbmdzW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgaW50ZXJuYWw6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChuYW1lKSApIHtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIG1vZHVsZSwgbmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgbW9kdWxlW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZVtuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGRlYnVnOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50ICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UubG9nKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmluZm8sIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHZlcmJvc2U6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQgJiYgc2V0dGluZ3MudmVyYm9zZSAmJiBzZXR0aW5ncy5kZWJ1Zykge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmluZm8sIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZS5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQpIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvciA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5lcnJvciwgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHBlcmZvcm1hbmNlOiB7XG4gICAgICAgICAgbG9nOiBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgY3VycmVudFRpbWUsXG4gICAgICAgICAgICAgIGV4ZWN1dGlvblRpbWUsXG4gICAgICAgICAgICAgIHByZXZpb3VzVGltZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgY3VycmVudFRpbWUgICA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgICAgICAgICAgICBwcmV2aW91c1RpbWUgID0gdGltZSB8fCBjdXJyZW50VGltZTtcbiAgICAgICAgICAgICAgZXhlY3V0aW9uVGltZSA9IGN1cnJlbnRUaW1lIC0gcHJldmlvdXNUaW1lO1xuICAgICAgICAgICAgICB0aW1lICAgICAgICAgID0gY3VycmVudFRpbWU7XG4gICAgICAgICAgICAgIHBlcmZvcm1hbmNlLnB1c2goe1xuICAgICAgICAgICAgICAgICdOYW1lJyAgICAgICAgICAgOiBtZXNzYWdlWzBdLFxuICAgICAgICAgICAgICAgICdBcmd1bWVudHMnICAgICAgOiBbXS5zbGljZS5jYWxsKG1lc3NhZ2UsIDEpIHx8ICcnLFxuICAgICAgICAgICAgICAgICdFbGVtZW50JyAgICAgICAgOiBlbGVtZW50LFxuICAgICAgICAgICAgICAgICdFeGVjdXRpb24gVGltZScgOiBleGVjdXRpb25UaW1lXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lcik7XG4gICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UudGltZXIgPSBzZXRUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS5kaXNwbGF5LCA1MDApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZGlzcGxheTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgdGl0bGUgPSBzZXR0aW5ncy5uYW1lICsgJzonLFxuICAgICAgICAgICAgICB0b3RhbFRpbWUgPSAwXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICB0aW1lID0gZmFsc2U7XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgdG90YWxUaW1lICs9IGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ107XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHRpdGxlICs9ICcgJyArIHRvdGFsVGltZSArICdtcyc7XG4gICAgICAgICAgICBpZihtb2R1bGVTZWxlY3Rvcikge1xuICAgICAgICAgICAgICB0aXRsZSArPSAnIFxcJycgKyBtb2R1bGVTZWxlY3RvciArICdcXCcnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoIChjb25zb2xlLmdyb3VwICE9PSB1bmRlZmluZWQgfHwgY29uc29sZS50YWJsZSAhPT0gdW5kZWZpbmVkKSAmJiBwZXJmb3JtYW5jZS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBDb2xsYXBzZWQodGl0bGUpO1xuICAgICAgICAgICAgICBpZihjb25zb2xlLnRhYmxlKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS50YWJsZShwZXJmb3JtYW5jZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YVsnTmFtZSddICsgJzogJyArIGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ10rJ21zJyk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgY29uc29sZS5ncm91cEVuZCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcGVyZm9ybWFuY2UgPSBbXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludm9rZTogZnVuY3Rpb24ocXVlcnksIHBhc3NlZEFyZ3VtZW50cywgY29udGV4dCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgb2JqZWN0ID0gaW5zdGFuY2UsXG4gICAgICAgICAgICBtYXhEZXB0aCxcbiAgICAgICAgICAgIGZvdW5kLFxuICAgICAgICAgICAgcmVzcG9uc2VcbiAgICAgICAgICA7XG4gICAgICAgICAgcGFzc2VkQXJndW1lbnRzID0gcGFzc2VkQXJndW1lbnRzIHx8IHF1ZXJ5QXJndW1lbnRzO1xuICAgICAgICAgIGNvbnRleHQgICAgICAgICA9IGVsZW1lbnQgICAgICAgICB8fCBjb250ZXh0O1xuICAgICAgICAgIGlmKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyAmJiBvYmplY3QgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnkgICAgPSBxdWVyeS5zcGxpdCgvW1xcLiBdLyk7XG4gICAgICAgICAgICBtYXhEZXB0aCA9IHF1ZXJ5Lmxlbmd0aCAtIDE7XG4gICAgICAgICAgICAkLmVhY2gocXVlcnksIGZ1bmN0aW9uKGRlcHRoLCB2YWx1ZSkge1xuICAgICAgICAgICAgICB2YXIgY2FtZWxDYXNlVmFsdWUgPSAoZGVwdGggIT0gbWF4RGVwdGgpXG4gICAgICAgICAgICAgICAgPyB2YWx1ZSArIHF1ZXJ5W2RlcHRoICsgMV0uY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBxdWVyeVtkZXB0aCArIDFdLnNsaWNlKDEpXG4gICAgICAgICAgICAgICAgOiBxdWVyeVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gKSAmJiAoZGVwdGggIT0gbWF4RGVwdGgpICkge1xuICAgICAgICAgICAgICAgIG9iamVjdCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgICAgIGZvdW5kID0gb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbdmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFt2YWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFt2YWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmICggJC5pc0Z1bmN0aW9uKCBmb3VuZCApICkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZC5hcHBseShjb250ZXh0LCBwYXNzZWRBcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKGZvdW5kICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gZm91bmQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKCQuaXNBcnJheShyZXR1cm5lZFZhbHVlKSkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZS5wdXNoKHJlc3BvbnNlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSBbcmV0dXJuZWRWYWx1ZSwgcmVzcG9uc2VdO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJlc3BvbnNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSByZXNwb25zZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGZvdW5kO1xuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICBpZihtZXRob2RJbnZva2VkKSB7XG4gICAgICAgIGlmKGluc3RhbmNlID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbnZva2UocXVlcnkpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGlmKGluc3RhbmNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBpbnN0YW5jZS5pbnZva2UoJ2Rlc3Ryb3knKTtcbiAgICAgICAgfVxuICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgfVxuICAgIH0pXG4gIDtcblxuICByZXR1cm4gKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZClcbiAgICA/IHJldHVybmVkVmFsdWVcbiAgICA6IHRoaXNcbiAgO1xufTtcblxuJC5mbi5tb2RhbC5zZXR0aW5ncyA9IHtcblxuICBuYW1lICAgICAgICAgICA6ICdNb2RhbCcsXG4gIG5hbWVzcGFjZSAgICAgIDogJ21vZGFsJyxcblxuICBzaWxlbnQgICAgICAgICA6IGZhbHNlLFxuICBkZWJ1ZyAgICAgICAgICA6IGZhbHNlLFxuICB2ZXJib3NlICAgICAgICA6IGZhbHNlLFxuICBwZXJmb3JtYW5jZSAgICA6IHRydWUsXG5cbiAgb2JzZXJ2ZUNoYW5nZXMgOiBmYWxzZSxcblxuICBhbGxvd011bHRpcGxlICA6IGZhbHNlLFxuICBkZXRhY2hhYmxlICAgICA6IHRydWUsXG4gIGNsb3NhYmxlICAgICAgIDogdHJ1ZSxcbiAgYXV0b2ZvY3VzICAgICAgOiB0cnVlLFxuXG4gIGludmVydGVkICAgICAgIDogZmFsc2UsXG4gIGJsdXJyaW5nICAgICAgIDogZmFsc2UsXG5cbiAgZGltbWVyU2V0dGluZ3MgOiB7XG4gICAgY2xvc2FibGUgOiBmYWxzZSxcbiAgICB1c2VDU1MgICA6IHRydWVcbiAgfSxcblxuXG4gIGNvbnRleHQgICAgOiAnYm9keScsXG5cbiAgcXVldWUgICAgICA6IGZhbHNlLFxuICBkdXJhdGlvbiAgIDogNTAwLFxuICBvZmZzZXQgICAgIDogMCxcbiAgdHJhbnNpdGlvbiA6ICdzY2FsZScsXG5cbiAgLy8gcGFkZGluZyB3aXRoIGVkZ2Ugb2YgcGFnZVxuICBwYWRkaW5nICAgIDogNTAsXG5cbiAgLy8gY2FsbGVkIGJlZm9yZSBzaG93IGFuaW1hdGlvblxuICBvblNob3cgICAgIDogZnVuY3Rpb24oKXt9LFxuXG4gIC8vIGNhbGxlZCBhZnRlciBzaG93IGFuaW1hdGlvblxuICBvblZpc2libGUgIDogZnVuY3Rpb24oKXt9LFxuXG4gIC8vIGNhbGxlZCBiZWZvcmUgaGlkZSBhbmltYXRpb25cbiAgb25IaWRlICAgICA6IGZ1bmN0aW9uKCl7IHJldHVybiB0cnVlOyB9LFxuXG4gIC8vIGNhbGxlZCBhZnRlciBoaWRlIGFuaW1hdGlvblxuICBvbkhpZGRlbiAgIDogZnVuY3Rpb24oKXt9LFxuXG4gIC8vIGNhbGxlZCBhZnRlciBhcHByb3ZlIHNlbGVjdG9yIG1hdGNoXG4gIG9uQXBwcm92ZSAgOiBmdW5jdGlvbigpeyByZXR1cm4gdHJ1ZTsgfSxcblxuICAvLyBjYWxsZWQgYWZ0ZXIgZGVueSBzZWxlY3RvciBtYXRjaFxuICBvbkRlbnkgICAgIDogZnVuY3Rpb24oKXsgcmV0dXJuIHRydWU7IH0sXG5cbiAgc2VsZWN0b3IgICAgOiB7XG4gICAgY2xvc2UgICAgOiAnPiAuY2xvc2UnLFxuICAgIGFwcHJvdmUgIDogJy5hY3Rpb25zIC5wb3NpdGl2ZSwgLmFjdGlvbnMgLmFwcHJvdmUsIC5hY3Rpb25zIC5vaycsXG4gICAgZGVueSAgICAgOiAnLmFjdGlvbnMgLm5lZ2F0aXZlLCAuYWN0aW9ucyAuZGVueSwgLmFjdGlvbnMgLmNhbmNlbCcsXG4gICAgbW9kYWwgICAgOiAnLnVpLm1vZGFsJ1xuICB9LFxuICBlcnJvciA6IHtcbiAgICBkaW1tZXIgICAgOiAnVUkgRGltbWVyLCBhIHJlcXVpcmVkIGNvbXBvbmVudCBpcyBub3QgaW5jbHVkZWQgaW4gdGhpcyBwYWdlJyxcbiAgICBtZXRob2QgICAgOiAnVGhlIG1ldGhvZCB5b3UgY2FsbGVkIGlzIG5vdCBkZWZpbmVkLicsXG4gICAgbm90Rm91bmQgIDogJ1RoZSBlbGVtZW50IHlvdSBzcGVjaWZpZWQgY291bGQgbm90IGJlIGZvdW5kJ1xuICB9LFxuICBjbGFzc05hbWUgOiB7XG4gICAgYWN0aXZlICAgICA6ICdhY3RpdmUnLFxuICAgIGFuaW1hdGluZyAgOiAnYW5pbWF0aW5nJyxcbiAgICBibHVycmluZyAgIDogJ2JsdXJyaW5nJyxcbiAgICBzY3JvbGxpbmcgIDogJ3Njcm9sbGluZycsXG4gICAgdW5kZXRhY2hlZCA6ICd1bmRldGFjaGVkJ1xuICB9XG59O1xuXG5cbn0pKCBqUXVlcnksIHdpbmRvdywgZG9jdW1lbnQgKTtcbiJdfQ==