/*!
 * # Semantic UI - Embed
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.embed = function (parameters) {

    var $allModules = $(this),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue;

    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.embed.settings, parameters) : $.extend({}, $.fn.embed.settings),
          selector = settings.selector,
          className = settings.className,
          sources = settings.sources,
          error = settings.error,
          metadata = settings.metadata,
          namespace = settings.namespace,
          templates = settings.templates,
          eventNamespace = '.' + namespace,
          moduleNamespace = 'module-' + namespace,
          $window = $(window),
          $module = $(this),
          $placeholder = $module.find(selector.placeholder),
          $icon = $module.find(selector.icon),
          $embed = $module.find(selector.embed),
          element = this,
          instance = $module.data(moduleNamespace),
          module;

      module = {

        initialize: function () {
          module.debug('Initializing embed');
          module.determine.autoplay();
          module.create();
          module.bind.events();
          module.instantiate();
        },

        instantiate: function () {
          module.verbose('Storing instance of module', module);
          instance = module;
          $module.data(moduleNamespace, module);
        },

        destroy: function () {
          module.verbose('Destroying previous instance of embed');
          module.reset();
          $module.removeData(moduleNamespace).off(eventNamespace);
        },

        refresh: function () {
          module.verbose('Refreshing selector cache');
          $placeholder = $module.find(selector.placeholder);
          $icon = $module.find(selector.icon);
          $embed = $module.find(selector.embed);
        },

        bind: {
          events: function () {
            if (module.has.placeholder()) {
              module.debug('Adding placeholder events');
              $module.on('click' + eventNamespace, selector.placeholder, module.createAndShow).on('click' + eventNamespace, selector.icon, module.createAndShow);
            }
          }
        },

        create: function () {
          var placeholder = module.get.placeholder();
          if (placeholder) {
            module.createPlaceholder();
          } else {
            module.createAndShow();
          }
        },

        createPlaceholder: function (placeholder) {
          var icon = module.get.icon(),
              url = module.get.url(),
              embed = module.generate.embed(url);
          placeholder = placeholder || module.get.placeholder();
          $module.html(templates.placeholder(placeholder, icon));
          module.debug('Creating placeholder for embed', placeholder, icon);
        },

        createEmbed: function (url) {
          module.refresh();
          url = url || module.get.url();
          $embed = $('<div/>').addClass(className.embed).html(module.generate.embed(url)).appendTo($module);
          settings.onCreate.call(element, url);
          module.debug('Creating embed object', $embed);
        },

        changeEmbed: function (url) {
          $embed.html(module.generate.embed(url));
        },

        createAndShow: function () {
          module.createEmbed();
          module.show();
        },

        // sets new embed
        change: function (source, id, url) {
          module.debug('Changing video to ', source, id, url);
          $module.data(metadata.source, source).data(metadata.id, id);
          if (url) {
            $module.data(metadata.url, url);
          } else {
            $module.removeData(metadata.url);
          }
          if (module.has.embed()) {
            module.changeEmbed();
          } else {
            module.create();
          }
        },

        // clears embed
        reset: function () {
          module.debug('Clearing embed and showing placeholder');
          module.remove.active();
          module.remove.embed();
          module.showPlaceholder();
          settings.onReset.call(element);
        },

        // shows current embed
        show: function () {
          module.debug('Showing embed');
          module.set.active();
          settings.onDisplay.call(element);
        },

        hide: function () {
          module.debug('Hiding embed');
          module.showPlaceholder();
        },

        showPlaceholder: function () {
          module.debug('Showing placeholder image');
          module.remove.active();
          settings.onPlaceholderDisplay.call(element);
        },

        get: {
          id: function () {
            return settings.id || $module.data(metadata.id);
          },
          placeholder: function () {
            return settings.placeholder || $module.data(metadata.placeholder);
          },
          icon: function () {
            return settings.icon ? settings.icon : $module.data(metadata.icon) !== undefined ? $module.data(metadata.icon) : module.determine.icon();
          },
          source: function (url) {
            return settings.source ? settings.source : $module.data(metadata.source) !== undefined ? $module.data(metadata.source) : module.determine.source();
          },
          type: function () {
            var source = module.get.source();
            return sources[source] !== undefined ? sources[source].type : false;
          },
          url: function () {
            return settings.url ? settings.url : $module.data(metadata.url) !== undefined ? $module.data(metadata.url) : module.determine.url();
          }
        },

        determine: {
          autoplay: function () {
            if (module.should.autoplay()) {
              settings.autoplay = true;
            }
          },
          source: function (url) {
            var matchedSource = false;
            url = url || module.get.url();
            if (url) {
              $.each(sources, function (name, source) {
                if (url.search(source.domain) !== -1) {
                  matchedSource = name;
                  return false;
                }
              });
            }
            return matchedSource;
          },
          icon: function () {
            var source = module.get.source();
            return sources[source] !== undefined ? sources[source].icon : false;
          },
          url: function () {
            var id = settings.id || $module.data(metadata.id),
                source = settings.source || $module.data(metadata.source),
                url;
            url = sources[source] !== undefined ? sources[source].url.replace('{id}', id) : false;
            if (url) {
              $module.data(metadata.url, url);
            }
            return url;
          }
        },

        set: {
          active: function () {
            $module.addClass(className.active);
          }
        },

        remove: {
          active: function () {
            $module.removeClass(className.active);
          },
          embed: function () {
            $embed.empty();
          }
        },

        encode: {
          parameters: function (parameters) {
            var urlString = [],
                index;
            for (index in parameters) {
              urlString.push(encodeURIComponent(index) + '=' + encodeURIComponent(parameters[index]));
            }
            return urlString.join('&amp;');
          }
        },

        generate: {
          embed: function (url) {
            module.debug('Generating embed html');
            var source = module.get.source(),
                html,
                parameters;
            url = module.get.url(url);
            if (url) {
              parameters = module.generate.parameters(source);
              html = templates.iframe(url, parameters);
            } else {
              module.error(error.noURL, $module);
            }
            return html;
          },
          parameters: function (source, extraParameters) {
            var parameters = sources[source] && sources[source].parameters !== undefined ? sources[source].parameters(settings) : {};
            extraParameters = extraParameters || settings.parameters;
            if (extraParameters) {
              parameters = $.extend({}, parameters, extraParameters);
            }
            parameters = settings.onEmbed(parameters);
            return module.encode.parameters(parameters);
          }
        },

        has: {
          embed: function () {
            return $embed.length > 0;
          },
          placeholder: function () {
            return settings.placeholder || $module.data(metadata.placeholder);
          }
        },

        should: {
          autoplay: function () {
            return settings.autoplay === 'auto' ? settings.placeholder || $module.data(metadata.placeholder) !== undefined : settings.autoplay;
          }
        },

        is: {
          video: function () {
            return module.get.type() == 'video';
          }
        },

        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ($allModules.length > 1) {
              title += ' ' + '(' + $allModules.length + ')';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                module.error(error.method, query);
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };

      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });
    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.embed.settings = {

    name: 'Embed',
    namespace: 'embed',

    silent: false,
    debug: false,
    verbose: false,
    performance: true,

    icon: false,
    source: false,
    url: false,
    id: false,

    // standard video settings
    autoplay: 'auto',
    color: '#444444',
    hd: true,
    brandedUI: false,

    // additional parameters to include with the embed
    parameters: false,

    onDisplay: function () {},
    onPlaceholderDisplay: function () {},
    onReset: function () {},
    onCreate: function (url) {},
    onEmbed: function (parameters) {
      return parameters;
    },

    metadata: {
      id: 'id',
      icon: 'icon',
      placeholder: 'placeholder',
      source: 'source',
      url: 'url'
    },

    error: {
      noURL: 'No URL specified',
      method: 'The method you called is not defined'
    },

    className: {
      active: 'active',
      embed: 'embed'
    },

    selector: {
      embed: '.embed',
      placeholder: '.placeholder',
      icon: '.icon'
    },

    sources: {
      youtube: {
        name: 'youtube',
        type: 'video',
        icon: 'video play',
        domain: 'youtube.com',
        url: '//www.youtube.com/embed/{id}',
        parameters: function (settings) {
          return {
            autohide: !settings.brandedUI,
            autoplay: settings.autoplay,
            color: settings.color || undefined,
            hq: settings.hd,
            jsapi: settings.api,
            modestbranding: !settings.brandedUI
          };
        }
      },
      vimeo: {
        name: 'vimeo',
        type: 'video',
        icon: 'video play',
        domain: 'vimeo.com',
        url: '//player.vimeo.com/video/{id}',
        parameters: function (settings) {
          return {
            api: settings.api,
            autoplay: settings.autoplay,
            byline: settings.brandedUI,
            color: settings.color || undefined,
            portrait: settings.brandedUI,
            title: settings.brandedUI
          };
        }
      }
    },

    templates: {
      iframe: function (url, parameters) {
        var src = url;
        if (parameters) {
          src += '?' + parameters;
        }
        return '' + '<iframe src="' + src + '"' + ' width="100%" height="100%"' + ' frameborder="0" scrolling="no" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
      },
      placeholder: function (image, icon) {
        var html = '';
        if (icon) {
          html += '<i class="' + icon + ' icon"></i>';
        }
        if (image) {
          html += '<img class="placeholder" src="' + image + '">';
        }
        return html;
      }
    },

    // NOT YET IMPLEMENTED
    api: false,
    onPause: function () {},
    onPlay: function () {},
    onStop: function () {}

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL2VtYmVkLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFVQSxDQUFDLENBQUMsVUFBVSxDQUFWLEVBQWEsTUFBYixFQUFxQixRQUFyQixFQUErQixTQUEvQixFQUEwQzs7QUFFNUM7O0FBRUEsV0FBVSxPQUFPLE1BQVAsSUFBaUIsV0FBakIsSUFBZ0MsT0FBTyxJQUFQLElBQWUsSUFBaEQsR0FDTCxNQURLLEdBRUosT0FBTyxJQUFQLElBQWUsV0FBZixJQUE4QixLQUFLLElBQUwsSUFBYSxJQUE1QyxHQUNFLElBREYsR0FFRSxTQUFTLGFBQVQsR0FKTjs7QUFPQSxJQUFFLEVBQUYsQ0FBSyxLQUFMLEdBQWEsVUFBUyxVQUFULEVBQXFCOztBQUVoQyxRQUNFLGNBQWtCLEVBQUUsSUFBRixDQURwQjtBQUFBLFFBR0UsaUJBQWtCLFlBQVksUUFBWixJQUF3QixFQUg1QztBQUFBLFFBS0UsT0FBa0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUxwQjtBQUFBLFFBTUUsY0FBa0IsRUFOcEI7QUFBQSxRQVFFLFFBQWtCLFVBQVUsQ0FBVixDQVJwQjtBQUFBLFFBU0UsZ0JBQW1CLE9BQU8sS0FBUCxJQUFnQixRQVRyQztBQUFBLFFBVUUsaUJBQWtCLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLENBQXpCLENBVnBCO0FBQUEsUUFZRSxhQVpGOztBQWVBLGdCQUNHLElBREgsQ0FDUSxZQUFXO0FBQ2YsVUFDRSxXQUFvQixFQUFFLGFBQUYsQ0FBZ0IsVUFBaEIsQ0FBRixHQUNkLEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLEVBQUUsRUFBRixDQUFLLEtBQUwsQ0FBVyxRQUE5QixFQUF3QyxVQUF4QyxDQURjLEdBRWQsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLEVBQUUsRUFBRixDQUFLLEtBQUwsQ0FBVyxRQUF4QixDQUhOO0FBQUEsVUFLRSxXQUFrQixTQUFTLFFBTDdCO0FBQUEsVUFNRSxZQUFrQixTQUFTLFNBTjdCO0FBQUEsVUFPRSxVQUFrQixTQUFTLE9BUDdCO0FBQUEsVUFRRSxRQUFrQixTQUFTLEtBUjdCO0FBQUEsVUFTRSxXQUFrQixTQUFTLFFBVDdCO0FBQUEsVUFVRSxZQUFrQixTQUFTLFNBVjdCO0FBQUEsVUFXRSxZQUFrQixTQUFTLFNBWDdCO0FBQUEsVUFhRSxpQkFBa0IsTUFBTSxTQWIxQjtBQUFBLFVBY0Usa0JBQWtCLFlBQVksU0FkaEM7QUFBQSxVQWdCRSxVQUFrQixFQUFFLE1BQUYsQ0FoQnBCO0FBQUEsVUFpQkUsVUFBa0IsRUFBRSxJQUFGLENBakJwQjtBQUFBLFVBa0JFLGVBQWtCLFFBQVEsSUFBUixDQUFhLFNBQVMsV0FBdEIsQ0FsQnBCO0FBQUEsVUFtQkUsUUFBa0IsUUFBUSxJQUFSLENBQWEsU0FBUyxJQUF0QixDQW5CcEI7QUFBQSxVQW9CRSxTQUFrQixRQUFRLElBQVIsQ0FBYSxTQUFTLEtBQXRCLENBcEJwQjtBQUFBLFVBc0JFLFVBQWtCLElBdEJwQjtBQUFBLFVBdUJFLFdBQWtCLFFBQVEsSUFBUixDQUFhLGVBQWIsQ0F2QnBCO0FBQUEsVUF3QkUsTUF4QkY7O0FBMkJBLGVBQVM7O0FBRVAsb0JBQVksWUFBVztBQUNyQixpQkFBTyxLQUFQLENBQWEsb0JBQWI7QUFDQSxpQkFBTyxTQUFQLENBQWlCLFFBQWpCO0FBQ0EsaUJBQU8sTUFBUDtBQUNBLGlCQUFPLElBQVAsQ0FBWSxNQUFaO0FBQ0EsaUJBQU8sV0FBUDtBQUNELFNBUk07O0FBVVAscUJBQWEsWUFBVztBQUN0QixpQkFBTyxPQUFQLENBQWUsNEJBQWYsRUFBNkMsTUFBN0M7QUFDQSxxQkFBVyxNQUFYO0FBQ0Esa0JBQ0csSUFESCxDQUNRLGVBRFIsRUFDeUIsTUFEekI7QUFHRCxTQWhCTTs7QUFrQlAsaUJBQVMsWUFBVztBQUNsQixpQkFBTyxPQUFQLENBQWUsdUNBQWY7QUFDQSxpQkFBTyxLQUFQO0FBQ0Esa0JBQ0csVUFESCxDQUNjLGVBRGQsRUFFRyxHQUZILENBRU8sY0FGUDtBQUlELFNBekJNOztBQTJCUCxpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLE9BQVAsQ0FBZSwyQkFBZjtBQUNBLHlCQUFlLFFBQVEsSUFBUixDQUFhLFNBQVMsV0FBdEIsQ0FBZjtBQUNBLGtCQUFlLFFBQVEsSUFBUixDQUFhLFNBQVMsSUFBdEIsQ0FBZjtBQUNBLG1CQUFlLFFBQVEsSUFBUixDQUFhLFNBQVMsS0FBdEIsQ0FBZjtBQUNELFNBaENNOztBQWtDUCxjQUFNO0FBQ0osa0JBQVEsWUFBVztBQUNqQixnQkFBSSxPQUFPLEdBQVAsQ0FBVyxXQUFYLEVBQUosRUFBK0I7QUFDN0IscUJBQU8sS0FBUCxDQUFhLDJCQUFiO0FBQ0Esc0JBQ0csRUFESCxDQUNNLFVBQVUsY0FEaEIsRUFDZ0MsU0FBUyxXQUR6QyxFQUNzRCxPQUFPLGFBRDdELEVBRUcsRUFGSCxDQUVNLFVBQVUsY0FGaEIsRUFFZ0MsU0FBUyxJQUZ6QyxFQUUrQyxPQUFPLGFBRnREO0FBSUQ7QUFDRjtBQVRHLFNBbENDOztBQThDUCxnQkFBUSxZQUFXO0FBQ2pCLGNBQ0UsY0FBYyxPQUFPLEdBQVAsQ0FBVyxXQUFYLEVBRGhCO0FBR0EsY0FBRyxXQUFILEVBQWdCO0FBQ2QsbUJBQU8saUJBQVA7QUFDRCxXQUZELE1BR0s7QUFDSCxtQkFBTyxhQUFQO0FBQ0Q7QUFDRixTQXhETTs7QUEwRFAsMkJBQW1CLFVBQVMsV0FBVCxFQUFzQjtBQUN2QyxjQUNFLE9BQVEsT0FBTyxHQUFQLENBQVcsSUFBWCxFQURWO0FBQUEsY0FFRSxNQUFRLE9BQU8sR0FBUCxDQUFXLEdBQVgsRUFGVjtBQUFBLGNBR0UsUUFBUSxPQUFPLFFBQVAsQ0FBZ0IsS0FBaEIsQ0FBc0IsR0FBdEIsQ0FIVjtBQUtBLHdCQUFjLGVBQWUsT0FBTyxHQUFQLENBQVcsV0FBWCxFQUE3QjtBQUNBLGtCQUFRLElBQVIsQ0FBYyxVQUFVLFdBQVYsQ0FBc0IsV0FBdEIsRUFBbUMsSUFBbkMsQ0FBZDtBQUNBLGlCQUFPLEtBQVAsQ0FBYSxnQ0FBYixFQUErQyxXQUEvQyxFQUE0RCxJQUE1RDtBQUNELFNBbkVNOztBQXFFUCxxQkFBYSxVQUFTLEdBQVQsRUFBYztBQUN6QixpQkFBTyxPQUFQO0FBQ0EsZ0JBQU0sT0FBTyxPQUFPLEdBQVAsQ0FBVyxHQUFYLEVBQWI7QUFDQSxtQkFBUyxFQUFFLFFBQUYsRUFDTixRQURNLENBQ0csVUFBVSxLQURiLEVBRU4sSUFGTSxDQUVBLE9BQU8sUUFBUCxDQUFnQixLQUFoQixDQUFzQixHQUF0QixDQUZBLEVBR04sUUFITSxDQUdHLE9BSEgsQ0FBVDtBQUtBLG1CQUFTLFFBQVQsQ0FBa0IsSUFBbEIsQ0FBdUIsT0FBdkIsRUFBZ0MsR0FBaEM7QUFDQSxpQkFBTyxLQUFQLENBQWEsdUJBQWIsRUFBc0MsTUFBdEM7QUFDRCxTQS9FTTs7QUFpRlAscUJBQWEsVUFBUyxHQUFULEVBQWM7QUFDekIsaUJBQ0csSUFESCxDQUNTLE9BQU8sUUFBUCxDQUFnQixLQUFoQixDQUFzQixHQUF0QixDQURUO0FBR0QsU0FyRk07O0FBdUZQLHVCQUFlLFlBQVc7QUFDeEIsaUJBQU8sV0FBUDtBQUNBLGlCQUFPLElBQVA7QUFDRCxTQTFGTTs7O0FBNkZQLGdCQUFRLFVBQVMsTUFBVCxFQUFpQixFQUFqQixFQUFxQixHQUFyQixFQUEwQjtBQUNoQyxpQkFBTyxLQUFQLENBQWEsb0JBQWIsRUFBbUMsTUFBbkMsRUFBMkMsRUFBM0MsRUFBK0MsR0FBL0M7QUFDQSxrQkFDRyxJQURILENBQ1EsU0FBUyxNQURqQixFQUN5QixNQUR6QixFQUVHLElBRkgsQ0FFUSxTQUFTLEVBRmpCLEVBRXFCLEVBRnJCO0FBSUEsY0FBRyxHQUFILEVBQVE7QUFDTixvQkFBUSxJQUFSLENBQWEsU0FBUyxHQUF0QixFQUEyQixHQUEzQjtBQUNELFdBRkQsTUFHSztBQUNILG9CQUFRLFVBQVIsQ0FBbUIsU0FBUyxHQUE1QjtBQUNEO0FBQ0QsY0FBRyxPQUFPLEdBQVAsQ0FBVyxLQUFYLEVBQUgsRUFBdUI7QUFDckIsbUJBQU8sV0FBUDtBQUNELFdBRkQsTUFHSztBQUNILG1CQUFPLE1BQVA7QUFDRDtBQUNGLFNBL0dNOzs7QUFrSFAsZUFBTyxZQUFXO0FBQ2hCLGlCQUFPLEtBQVAsQ0FBYSx3Q0FBYjtBQUNBLGlCQUFPLE1BQVAsQ0FBYyxNQUFkO0FBQ0EsaUJBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDQSxpQkFBTyxlQUFQO0FBQ0EsbUJBQVMsT0FBVCxDQUFpQixJQUFqQixDQUFzQixPQUF0QjtBQUNELFNBeEhNOzs7QUEySFAsY0FBTSxZQUFXO0FBQ2YsaUJBQU8sS0FBUCxDQUFhLGVBQWI7QUFDQSxpQkFBTyxHQUFQLENBQVcsTUFBWDtBQUNBLG1CQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsT0FBeEI7QUFDRCxTQS9ITTs7QUFpSVAsY0FBTSxZQUFXO0FBQ2YsaUJBQU8sS0FBUCxDQUFhLGNBQWI7QUFDQSxpQkFBTyxlQUFQO0FBQ0QsU0FwSU07O0FBc0lQLHlCQUFpQixZQUFXO0FBQzFCLGlCQUFPLEtBQVAsQ0FBYSwyQkFBYjtBQUNBLGlCQUFPLE1BQVAsQ0FBYyxNQUFkO0FBQ0EsbUJBQVMsb0JBQVQsQ0FBOEIsSUFBOUIsQ0FBbUMsT0FBbkM7QUFDRCxTQTFJTTs7QUE0SVAsYUFBSztBQUNILGNBQUksWUFBVztBQUNiLG1CQUFPLFNBQVMsRUFBVCxJQUFlLFFBQVEsSUFBUixDQUFhLFNBQVMsRUFBdEIsQ0FBdEI7QUFDRCxXQUhFO0FBSUgsdUJBQWEsWUFBVztBQUN0QixtQkFBTyxTQUFTLFdBQVQsSUFBd0IsUUFBUSxJQUFSLENBQWEsU0FBUyxXQUF0QixDQUEvQjtBQUNELFdBTkU7QUFPSCxnQkFBTSxZQUFXO0FBQ2YsbUJBQVEsU0FBUyxJQUFWLEdBQ0gsU0FBUyxJQUROLEdBRUYsUUFBUSxJQUFSLENBQWEsU0FBUyxJQUF0QixNQUFnQyxTQUFqQyxHQUNFLFFBQVEsSUFBUixDQUFhLFNBQVMsSUFBdEIsQ0FERixHQUVFLE9BQU8sU0FBUCxDQUFpQixJQUFqQixFQUpOO0FBTUQsV0FkRTtBQWVILGtCQUFRLFVBQVMsR0FBVCxFQUFjO0FBQ3BCLG1CQUFRLFNBQVMsTUFBVixHQUNILFNBQVMsTUFETixHQUVGLFFBQVEsSUFBUixDQUFhLFNBQVMsTUFBdEIsTUFBa0MsU0FBbkMsR0FDRSxRQUFRLElBQVIsQ0FBYSxTQUFTLE1BQXRCLENBREYsR0FFRSxPQUFPLFNBQVAsQ0FBaUIsTUFBakIsRUFKTjtBQU1ELFdBdEJFO0FBdUJILGdCQUFNLFlBQVc7QUFDZixnQkFBSSxTQUFTLE9BQU8sR0FBUCxDQUFXLE1BQVgsRUFBYjtBQUNBLG1CQUFRLFFBQVEsTUFBUixNQUFvQixTQUFyQixHQUNILFFBQVEsTUFBUixFQUFnQixJQURiLEdBRUgsS0FGSjtBQUlELFdBN0JFO0FBOEJILGVBQUssWUFBVztBQUNkLG1CQUFRLFNBQVMsR0FBVixHQUNILFNBQVMsR0FETixHQUVGLFFBQVEsSUFBUixDQUFhLFNBQVMsR0FBdEIsTUFBK0IsU0FBaEMsR0FDRSxRQUFRLElBQVIsQ0FBYSxTQUFTLEdBQXRCLENBREYsR0FFRSxPQUFPLFNBQVAsQ0FBaUIsR0FBakIsRUFKTjtBQU1EO0FBckNFLFNBNUlFOztBQW9MUCxtQkFBVztBQUNULG9CQUFVLFlBQVc7QUFDbkIsZ0JBQUcsT0FBTyxNQUFQLENBQWMsUUFBZCxFQUFILEVBQTZCO0FBQzNCLHVCQUFTLFFBQVQsR0FBb0IsSUFBcEI7QUFDRDtBQUNGLFdBTFE7QUFNVCxrQkFBUSxVQUFTLEdBQVQsRUFBYztBQUNwQixnQkFDRSxnQkFBZ0IsS0FEbEI7QUFHQSxrQkFBTSxPQUFPLE9BQU8sR0FBUCxDQUFXLEdBQVgsRUFBYjtBQUNBLGdCQUFHLEdBQUgsRUFBUTtBQUNOLGdCQUFFLElBQUYsQ0FBTyxPQUFQLEVBQWdCLFVBQVMsSUFBVCxFQUFlLE1BQWYsRUFBdUI7QUFDckMsb0JBQUcsSUFBSSxNQUFKLENBQVcsT0FBTyxNQUFsQixNQUE4QixDQUFDLENBQWxDLEVBQXFDO0FBQ25DLGtDQUFnQixJQUFoQjtBQUNBLHlCQUFPLEtBQVA7QUFDRDtBQUNGLGVBTEQ7QUFNRDtBQUNELG1CQUFPLGFBQVA7QUFDRCxXQXBCUTtBQXFCVCxnQkFBTSxZQUFXO0FBQ2YsZ0JBQ0UsU0FBUyxPQUFPLEdBQVAsQ0FBVyxNQUFYLEVBRFg7QUFHQSxtQkFBUSxRQUFRLE1BQVIsTUFBb0IsU0FBckIsR0FDSCxRQUFRLE1BQVIsRUFBZ0IsSUFEYixHQUVILEtBRko7QUFJRCxXQTdCUTtBQThCVCxlQUFLLFlBQVc7QUFDZCxnQkFDRSxLQUFTLFNBQVMsRUFBVCxJQUFtQixRQUFRLElBQVIsQ0FBYSxTQUFTLEVBQXRCLENBRDlCO0FBQUEsZ0JBRUUsU0FBUyxTQUFTLE1BQVQsSUFBbUIsUUFBUSxJQUFSLENBQWEsU0FBUyxNQUF0QixDQUY5QjtBQUFBLGdCQUdFLEdBSEY7QUFLQSxrQkFBTyxRQUFRLE1BQVIsTUFBb0IsU0FBckIsR0FDRixRQUFRLE1BQVIsRUFBZ0IsR0FBaEIsQ0FBb0IsT0FBcEIsQ0FBNEIsTUFBNUIsRUFBb0MsRUFBcEMsQ0FERSxHQUVGLEtBRko7QUFJQSxnQkFBRyxHQUFILEVBQVE7QUFDTixzQkFBUSxJQUFSLENBQWEsU0FBUyxHQUF0QixFQUEyQixHQUEzQjtBQUNEO0FBQ0QsbUJBQU8sR0FBUDtBQUNEO0FBNUNRLFNBcExKOztBQW9PUCxhQUFLO0FBQ0gsa0JBQVEsWUFBVztBQUNqQixvQkFBUSxRQUFSLENBQWlCLFVBQVUsTUFBM0I7QUFDRDtBQUhFLFNBcE9FOztBQTBPUCxnQkFBUTtBQUNOLGtCQUFRLFlBQVc7QUFDakIsb0JBQVEsV0FBUixDQUFvQixVQUFVLE1BQTlCO0FBQ0QsV0FISztBQUlOLGlCQUFPLFlBQVc7QUFDaEIsbUJBQU8sS0FBUDtBQUNEO0FBTkssU0ExT0Q7O0FBbVBQLGdCQUFRO0FBQ04sc0JBQVksVUFBUyxVQUFULEVBQXFCO0FBQy9CLGdCQUNFLFlBQVksRUFEZDtBQUFBLGdCQUVFLEtBRkY7QUFJQSxpQkFBSyxLQUFMLElBQWMsVUFBZCxFQUEwQjtBQUN4Qix3QkFBVSxJQUFWLENBQWdCLG1CQUFtQixLQUFuQixJQUE0QixHQUE1QixHQUFrQyxtQkFBb0IsV0FBVyxLQUFYLENBQXBCLENBQWxEO0FBQ0Q7QUFDRCxtQkFBTyxVQUFVLElBQVYsQ0FBZSxPQUFmLENBQVA7QUFDRDtBQVZLLFNBblBEOztBQWdRUCxrQkFBVTtBQUNSLGlCQUFPLFVBQVMsR0FBVCxFQUFjO0FBQ25CLG1CQUFPLEtBQVAsQ0FBYSx1QkFBYjtBQUNBLGdCQUNFLFNBQVMsT0FBTyxHQUFQLENBQVcsTUFBWCxFQURYO0FBQUEsZ0JBRUUsSUFGRjtBQUFBLGdCQUdFLFVBSEY7QUFLQSxrQkFBTSxPQUFPLEdBQVAsQ0FBVyxHQUFYLENBQWUsR0FBZixDQUFOO0FBQ0EsZ0JBQUcsR0FBSCxFQUFRO0FBQ04sMkJBQWEsT0FBTyxRQUFQLENBQWdCLFVBQWhCLENBQTJCLE1BQTNCLENBQWI7QUFDQSxxQkFBYSxVQUFVLE1BQVYsQ0FBaUIsR0FBakIsRUFBc0IsVUFBdEIsQ0FBYjtBQUNELGFBSEQsTUFJSztBQUNILHFCQUFPLEtBQVAsQ0FBYSxNQUFNLEtBQW5CLEVBQTBCLE9BQTFCO0FBQ0Q7QUFDRCxtQkFBTyxJQUFQO0FBQ0QsV0FqQk87QUFrQlIsc0JBQVksVUFBUyxNQUFULEVBQWlCLGVBQWpCLEVBQWtDO0FBQzVDLGdCQUNFLGFBQWMsUUFBUSxNQUFSLEtBQW1CLFFBQVEsTUFBUixFQUFnQixVQUFoQixLQUErQixTQUFuRCxHQUNULFFBQVEsTUFBUixFQUFnQixVQUFoQixDQUEyQixRQUEzQixDQURTLEdBRVQsRUFITjtBQUtBLDhCQUFrQixtQkFBbUIsU0FBUyxVQUE5QztBQUNBLGdCQUFHLGVBQUgsRUFBb0I7QUFDbEIsMkJBQWEsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLFVBQWIsRUFBeUIsZUFBekIsQ0FBYjtBQUNEO0FBQ0QseUJBQWEsU0FBUyxPQUFULENBQWlCLFVBQWpCLENBQWI7QUFDQSxtQkFBTyxPQUFPLE1BQVAsQ0FBYyxVQUFkLENBQXlCLFVBQXpCLENBQVA7QUFDRDtBQTlCTyxTQWhRSDs7QUFpU1AsYUFBSztBQUNILGlCQUFPLFlBQVc7QUFDaEIsbUJBQVEsT0FBTyxNQUFQLEdBQWdCLENBQXhCO0FBQ0QsV0FIRTtBQUlILHVCQUFhLFlBQVc7QUFDdEIsbUJBQU8sU0FBUyxXQUFULElBQXdCLFFBQVEsSUFBUixDQUFhLFNBQVMsV0FBdEIsQ0FBL0I7QUFDRDtBQU5FLFNBalNFOztBQTBTUCxnQkFBUTtBQUNOLG9CQUFVLFlBQVc7QUFDbkIsbUJBQVEsU0FBUyxRQUFULEtBQXNCLE1BQXZCLEdBQ0YsU0FBUyxXQUFULElBQXdCLFFBQVEsSUFBUixDQUFhLFNBQVMsV0FBdEIsTUFBdUMsU0FEN0QsR0FFSCxTQUFTLFFBRmI7QUFJRDtBQU5LLFNBMVNEOztBQW1UUCxZQUFJO0FBQ0YsaUJBQU8sWUFBVztBQUNoQixtQkFBTyxPQUFPLEdBQVAsQ0FBVyxJQUFYLE1BQXFCLE9BQTVCO0FBQ0Q7QUFIQyxTQW5URzs7QUF5VFAsaUJBQVMsVUFBUyxJQUFULEVBQWUsS0FBZixFQUFzQjtBQUM3QixpQkFBTyxLQUFQLENBQWEsa0JBQWIsRUFBaUMsSUFBakMsRUFBdUMsS0FBdkM7QUFDQSxjQUFJLEVBQUUsYUFBRixDQUFnQixJQUFoQixDQUFKLEVBQTRCO0FBQzFCLGNBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxRQUFmLEVBQXlCLElBQXpCO0FBQ0QsV0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLGdCQUFHLEVBQUUsYUFBRixDQUFnQixTQUFTLElBQVQsQ0FBaEIsQ0FBSCxFQUFvQztBQUNsQyxnQkFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLFNBQVMsSUFBVCxDQUFmLEVBQStCLEtBQS9CO0FBQ0QsYUFGRCxNQUdLO0FBQ0gsdUJBQVMsSUFBVCxJQUFpQixLQUFqQjtBQUNEO0FBQ0YsV0FQSSxNQVFBO0FBQ0gsbUJBQU8sU0FBUyxJQUFULENBQVA7QUFDRDtBQUNGLFNBelVNO0FBMFVQLGtCQUFVLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDOUIsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsTUFBZixFQUF1QixJQUF2QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixtQkFBTyxJQUFQLElBQWUsS0FBZjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLE9BQU8sSUFBUCxDQUFQO0FBQ0Q7QUFDRixTQXBWTTtBQXFWUCxlQUFPLFlBQVc7QUFDaEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLEtBQWhDLEVBQXVDO0FBQ3JDLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2QixxQkFBTyxXQUFQLENBQW1CLEdBQW5CLENBQXVCLFNBQXZCO0FBQ0QsYUFGRCxNQUdLO0FBQ0gscUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFmO0FBQ0EscUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGO0FBQ0YsU0EvVk07QUFnV1AsaUJBQVMsWUFBVztBQUNsQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsT0FBN0IsSUFBd0MsU0FBUyxLQUFwRCxFQUEyRDtBQUN6RCxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLE9BQVAsR0FBaUIsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWpCO0FBQ0EscUJBQU8sT0FBUCxDQUFlLEtBQWYsQ0FBcUIsT0FBckIsRUFBOEIsU0FBOUI7QUFDRDtBQUNGO0FBQ0YsU0ExV007QUEyV1AsZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQWIsRUFBcUI7QUFDbkIsbUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLEtBQXJDLEVBQTRDLE9BQTVDLEVBQXFELFNBQVMsSUFBVCxHQUFnQixHQUFyRSxDQUFmO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGLFNBaFhNO0FBaVhQLHFCQUFhO0FBQ1gsZUFBSyxVQUFTLE9BQVQsRUFBa0I7QUFDckIsZ0JBQ0UsV0FERixFQUVFLGFBRkYsRUFHRSxZQUhGO0FBS0EsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLDRCQUFnQixJQUFJLElBQUosR0FBVyxPQUFYLEVBQWhCO0FBQ0EsNkJBQWdCLFFBQVEsV0FBeEI7QUFDQSw4QkFBZ0IsY0FBYyxZQUE5QjtBQUNBLHFCQUFnQixXQUFoQjtBQUNBLDBCQUFZLElBQVosQ0FBaUI7QUFDZix3QkFBbUIsUUFBUSxDQUFSLENBREo7QUFFZiw2QkFBbUIsR0FBRyxLQUFILENBQVMsSUFBVCxDQUFjLE9BQWQsRUFBdUIsQ0FBdkIsS0FBNkIsRUFGakM7QUFHZiwyQkFBbUIsT0FISjtBQUlmLGtDQUFtQjtBQUpKLGVBQWpCO0FBTUQ7QUFDRCx5QkFBYSxPQUFPLFdBQVAsQ0FBbUIsS0FBaEM7QUFDQSxtQkFBTyxXQUFQLENBQW1CLEtBQW5CLEdBQTJCLFdBQVcsT0FBTyxXQUFQLENBQW1CLE9BQTlCLEVBQXVDLEdBQXZDLENBQTNCO0FBQ0QsV0FyQlU7QUFzQlgsbUJBQVMsWUFBVztBQUNsQixnQkFDRSxRQUFRLFNBQVMsSUFBVCxHQUFnQixHQUQxQjtBQUFBLGdCQUVFLFlBQVksQ0FGZDtBQUlBLG1CQUFPLEtBQVA7QUFDQSx5QkFBYSxPQUFPLFdBQVAsQ0FBbUIsS0FBaEM7QUFDQSxjQUFFLElBQUYsQ0FBTyxXQUFQLEVBQW9CLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjtBQUN4QywyQkFBYSxLQUFLLGdCQUFMLENBQWI7QUFDRCxhQUZEO0FBR0EscUJBQVMsTUFBTSxTQUFOLEdBQWtCLElBQTNCO0FBQ0EsZ0JBQUcsY0FBSCxFQUFtQjtBQUNqQix1QkFBUyxRQUFRLGNBQVIsR0FBeUIsSUFBbEM7QUFDRDtBQUNELGdCQUFHLFlBQVksTUFBWixHQUFxQixDQUF4QixFQUEyQjtBQUN6Qix1QkFBUyxNQUFNLEdBQU4sR0FBWSxZQUFZLE1BQXhCLEdBQWlDLEdBQTFDO0FBQ0Q7QUFDRCxnQkFBSSxDQUFDLFFBQVEsS0FBUixLQUFrQixTQUFsQixJQUErQixRQUFRLEtBQVIsS0FBa0IsU0FBbEQsS0FBZ0UsWUFBWSxNQUFaLEdBQXFCLENBQXpGLEVBQTRGO0FBQzFGLHNCQUFRLGNBQVIsQ0FBdUIsS0FBdkI7QUFDQSxrQkFBRyxRQUFRLEtBQVgsRUFBa0I7QUFDaEIsd0JBQVEsS0FBUixDQUFjLFdBQWQ7QUFDRCxlQUZELE1BR0s7QUFDSCxrQkFBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMEJBQVEsR0FBUixDQUFZLEtBQUssTUFBTCxJQUFlLElBQWYsR0FBc0IsS0FBSyxnQkFBTCxDQUF0QixHQUE2QyxJQUF6RDtBQUNELGlCQUZEO0FBR0Q7QUFDRCxzQkFBUSxRQUFSO0FBQ0Q7QUFDRCwwQkFBYyxFQUFkO0FBQ0Q7QUFwRFUsU0FqWE47QUF1YVAsZ0JBQVEsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDLE9BQWpDLEVBQTBDO0FBQ2hELGNBQ0UsU0FBUyxRQURYO0FBQUEsY0FFRSxRQUZGO0FBQUEsY0FHRSxLQUhGO0FBQUEsY0FJRSxRQUpGO0FBTUEsNEJBQWtCLG1CQUFtQixjQUFyQztBQUNBLG9CQUFrQixXQUFtQixPQUFyQztBQUNBLGNBQUcsT0FBTyxLQUFQLElBQWdCLFFBQWhCLElBQTRCLFdBQVcsU0FBMUMsRUFBcUQ7QUFDbkQsb0JBQVcsTUFBTSxLQUFOLENBQVksT0FBWixDQUFYO0FBQ0EsdUJBQVcsTUFBTSxNQUFOLEdBQWUsQ0FBMUI7QUFDQSxjQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ25DLGtCQUFJLGlCQUFrQixTQUFTLFFBQVYsR0FDakIsUUFBUSxNQUFNLFFBQVEsQ0FBZCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixFQUEyQixXQUEzQixFQUFSLEdBQW1ELE1BQU0sUUFBUSxDQUFkLEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBRGxDLEdBRWpCLEtBRko7QUFJQSxrQkFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxjQUFQLENBQWpCLEtBQThDLFNBQVMsUUFBM0QsRUFBdUU7QUFDckUseUJBQVMsT0FBTyxjQUFQLENBQVQ7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLGNBQVAsTUFBMkIsU0FBL0IsRUFBMkM7QUFDOUMsd0JBQVEsT0FBTyxjQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBLElBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sS0FBUCxDQUFqQixLQUFxQyxTQUFTLFFBQWxELEVBQThEO0FBQ2pFLHlCQUFTLE9BQU8sS0FBUCxDQUFUO0FBQ0QsZUFGSSxNQUdBLElBQUksT0FBTyxLQUFQLE1BQWtCLFNBQXRCLEVBQWtDO0FBQ3JDLHdCQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQTtBQUNILHVCQUFPLEtBQVAsQ0FBYSxNQUFNLE1BQW5CLEVBQTJCLEtBQTNCO0FBQ0EsdUJBQU8sS0FBUDtBQUNEO0FBQ0YsYUF2QkQ7QUF3QkQ7QUFDRCxjQUFLLEVBQUUsVUFBRixDQUFjLEtBQWQsQ0FBTCxFQUE2QjtBQUMzQix1QkFBVyxNQUFNLEtBQU4sQ0FBWSxPQUFaLEVBQXFCLGVBQXJCLENBQVg7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsdUJBQVcsS0FBWDtBQUNEO0FBQ0QsY0FBRyxFQUFFLE9BQUYsQ0FBVSxhQUFWLENBQUgsRUFBNkI7QUFDM0IsMEJBQWMsSUFBZCxDQUFtQixRQUFuQjtBQUNELFdBRkQsTUFHSyxJQUFHLGtCQUFrQixTQUFyQixFQUFnQztBQUNuQyw0QkFBZ0IsQ0FBQyxhQUFELEVBQWdCLFFBQWhCLENBQWhCO0FBQ0QsV0FGSSxNQUdBLElBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUM5Qiw0QkFBZ0IsUUFBaEI7QUFDRDtBQUNELGlCQUFPLEtBQVA7QUFDRDtBQTVkTSxPQUFUOztBQStkQSxVQUFHLGFBQUgsRUFBa0I7QUFDaEIsWUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLGlCQUFPLFVBQVA7QUFDRDtBQUNELGVBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDRCxPQUxELE1BTUs7QUFDSCxZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsbUJBQVMsTUFBVCxDQUFnQixTQUFoQjtBQUNEO0FBQ0QsZUFBTyxVQUFQO0FBQ0Q7QUFDRixLQXhnQkg7QUEwZ0JBLFdBQVEsa0JBQWtCLFNBQW5CLEdBQ0gsYUFERyxHQUVILElBRko7QUFJRCxHQS9oQkQ7O0FBaWlCQSxJQUFFLEVBQUYsQ0FBSyxLQUFMLENBQVcsUUFBWCxHQUFzQjs7QUFFcEIsVUFBYyxPQUZNO0FBR3BCLGVBQWMsT0FITTs7QUFLcEIsWUFBYyxLQUxNO0FBTXBCLFdBQWMsS0FOTTtBQU9wQixhQUFjLEtBUE07QUFRcEIsaUJBQWMsSUFSTTs7QUFVcEIsVUFBVyxLQVZTO0FBV3BCLFlBQVcsS0FYUztBQVlwQixTQUFXLEtBWlM7QUFhcEIsUUFBVyxLQWJTOzs7QUFnQnBCLGNBQVksTUFoQlE7QUFpQnBCLFdBQVksU0FqQlE7QUFrQnBCLFFBQVksSUFsQlE7QUFtQnBCLGVBQVksS0FuQlE7OztBQXNCcEIsZ0JBQVksS0F0QlE7O0FBd0JwQixlQUF1QixZQUFXLENBQUUsQ0F4QmhCO0FBeUJwQiwwQkFBdUIsWUFBVyxDQUFFLENBekJoQjtBQTBCcEIsYUFBdUIsWUFBVyxDQUFFLENBMUJoQjtBQTJCcEIsY0FBdUIsVUFBUyxHQUFULEVBQWMsQ0FBRSxDQTNCbkI7QUE0QnBCLGFBQXVCLFVBQVMsVUFBVCxFQUFxQjtBQUMxQyxhQUFPLFVBQVA7QUFDRCxLQTlCbUI7O0FBZ0NwQixjQUFjO0FBQ1osVUFBYyxJQURGO0FBRVosWUFBYyxNQUZGO0FBR1osbUJBQWMsYUFIRjtBQUlaLGNBQWMsUUFKRjtBQUtaLFdBQWM7QUFMRixLQWhDTTs7QUF3Q3BCLFdBQVE7QUFDTixhQUFTLGtCQURIO0FBRU4sY0FBUztBQUZILEtBeENZOztBQTZDcEIsZUFBWTtBQUNWLGNBQVMsUUFEQztBQUVWLGFBQVM7QUFGQyxLQTdDUTs7QUFrRHBCLGNBQVc7QUFDVCxhQUFjLFFBREw7QUFFVCxtQkFBYyxjQUZMO0FBR1QsWUFBYztBQUhMLEtBbERTOztBQXdEcEIsYUFBUztBQUNQLGVBQVM7QUFDUCxjQUFTLFNBREY7QUFFUCxjQUFTLE9BRkY7QUFHUCxjQUFTLFlBSEY7QUFJUCxnQkFBUyxhQUpGO0FBS1AsYUFBUyw4QkFMRjtBQU1QLG9CQUFZLFVBQVMsUUFBVCxFQUFtQjtBQUM3QixpQkFBTztBQUNMLHNCQUFpQixDQUFDLFNBQVMsU0FEdEI7QUFFTCxzQkFBaUIsU0FBUyxRQUZyQjtBQUdMLG1CQUFpQixTQUFTLEtBQVQsSUFBa0IsU0FIOUI7QUFJTCxnQkFBaUIsU0FBUyxFQUpyQjtBQUtMLG1CQUFpQixTQUFTLEdBTHJCO0FBTUwsNEJBQWlCLENBQUMsU0FBUztBQU50QixXQUFQO0FBUUQ7QUFmTSxPQURGO0FBa0JQLGFBQU87QUFDTCxjQUFTLE9BREo7QUFFTCxjQUFTLE9BRko7QUFHTCxjQUFTLFlBSEo7QUFJTCxnQkFBUyxXQUpKO0FBS0wsYUFBUywrQkFMSjtBQU1MLG9CQUFZLFVBQVMsUUFBVCxFQUFtQjtBQUM3QixpQkFBTztBQUNMLGlCQUFXLFNBQVMsR0FEZjtBQUVMLHNCQUFXLFNBQVMsUUFGZjtBQUdMLG9CQUFXLFNBQVMsU0FIZjtBQUlMLG1CQUFXLFNBQVMsS0FBVCxJQUFrQixTQUp4QjtBQUtMLHNCQUFXLFNBQVMsU0FMZjtBQU1MLG1CQUFXLFNBQVM7QUFOZixXQUFQO0FBUUQ7QUFmSTtBQWxCQSxLQXhEVzs7QUE2RnBCLGVBQVc7QUFDVCxjQUFTLFVBQVMsR0FBVCxFQUFjLFVBQWQsRUFBMEI7QUFDakMsWUFBSSxNQUFNLEdBQVY7QUFDQSxZQUFJLFVBQUosRUFBZ0I7QUFDWixpQkFBTyxNQUFNLFVBQWI7QUFDSDtBQUNELGVBQU8sS0FDSCxlQURHLEdBQ2UsR0FEZixHQUNxQixHQURyQixHQUVILDZCQUZHLEdBR0gsb0dBSEo7QUFLRCxPQVhRO0FBWVQsbUJBQWMsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ2xDLFlBQ0UsT0FBTyxFQURUO0FBR0EsWUFBRyxJQUFILEVBQVM7QUFDUCxrQkFBUSxlQUFlLElBQWYsR0FBc0IsYUFBOUI7QUFDRDtBQUNELFlBQUcsS0FBSCxFQUFVO0FBQ1Isa0JBQVEsbUNBQW1DLEtBQW5DLEdBQTJDLElBQW5EO0FBQ0Q7QUFDRCxlQUFPLElBQVA7QUFDRDtBQXZCUSxLQTdGUzs7O0FBd0hwQixTQUFVLEtBeEhVO0FBeUhwQixhQUFVLFlBQVcsQ0FBRSxDQXpISDtBQTBIcEIsWUFBVSxZQUFXLENBQUUsQ0ExSEg7QUEySHBCLFlBQVUsWUFBVyxDQUFFOztBQTNISCxHQUF0QjtBQWlJQyxDQTdxQkEsRUE2cUJHLE1BN3FCSCxFQTZxQlcsTUE3cUJYLEVBNnFCbUIsUUE3cUJuQiIsImZpbGUiOiJlbWJlZC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxuICogIyBTZW1hbnRpYyBVSSAtIEVtYmVkXG4gKiBodHRwOi8vZ2l0aHViLmNvbS9zZW1hbnRpYy1vcmcvc2VtYW50aWMtdWkvXG4gKlxuICpcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxuICogaHR0cDovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVFxuICpcbiAqL1xuXG47KGZ1bmN0aW9uICgkLCB3aW5kb3csIGRvY3VtZW50LCB1bmRlZmluZWQpIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cbndpbmRvdyA9ICh0eXBlb2Ygd2luZG93ICE9ICd1bmRlZmluZWQnICYmIHdpbmRvdy5NYXRoID09IE1hdGgpXG4gID8gd2luZG93XG4gIDogKHR5cGVvZiBzZWxmICE9ICd1bmRlZmluZWQnICYmIHNlbGYuTWF0aCA9PSBNYXRoKVxuICAgID8gc2VsZlxuICAgIDogRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKVxuO1xuXG4kLmZuLmVtYmVkID0gZnVuY3Rpb24ocGFyYW1ldGVycykge1xuXG4gIHZhclxuICAgICRhbGxNb2R1bGVzICAgICA9ICQodGhpcyksXG5cbiAgICBtb2R1bGVTZWxlY3RvciAgPSAkYWxsTW9kdWxlcy5zZWxlY3RvciB8fCAnJyxcblxuICAgIHRpbWUgICAgICAgICAgICA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpLFxuICAgIHBlcmZvcm1hbmNlICAgICA9IFtdLFxuXG4gICAgcXVlcnkgICAgICAgICAgID0gYXJndW1lbnRzWzBdLFxuICAgIG1ldGhvZEludm9rZWQgICA9ICh0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycpLFxuICAgIHF1ZXJ5QXJndW1lbnRzICA9IFtdLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSxcblxuICAgIHJldHVybmVkVmFsdWVcbiAgO1xuXG4gICRhbGxNb2R1bGVzXG4gICAgLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICB2YXJcbiAgICAgICAgc2V0dGluZ3MgICAgICAgID0gKCAkLmlzUGxhaW5PYmplY3QocGFyYW1ldGVycykgKVxuICAgICAgICAgID8gJC5leHRlbmQodHJ1ZSwge30sICQuZm4uZW1iZWQuc2V0dGluZ3MsIHBhcmFtZXRlcnMpXG4gICAgICAgICAgOiAkLmV4dGVuZCh7fSwgJC5mbi5lbWJlZC5zZXR0aW5ncyksXG5cbiAgICAgICAgc2VsZWN0b3IgICAgICAgID0gc2V0dGluZ3Muc2VsZWN0b3IsXG4gICAgICAgIGNsYXNzTmFtZSAgICAgICA9IHNldHRpbmdzLmNsYXNzTmFtZSxcbiAgICAgICAgc291cmNlcyAgICAgICAgID0gc2V0dGluZ3Muc291cmNlcyxcbiAgICAgICAgZXJyb3IgICAgICAgICAgID0gc2V0dGluZ3MuZXJyb3IsXG4gICAgICAgIG1ldGFkYXRhICAgICAgICA9IHNldHRpbmdzLm1ldGFkYXRhLFxuICAgICAgICBuYW1lc3BhY2UgICAgICAgPSBzZXR0aW5ncy5uYW1lc3BhY2UsXG4gICAgICAgIHRlbXBsYXRlcyAgICAgICA9IHNldHRpbmdzLnRlbXBsYXRlcyxcblxuICAgICAgICBldmVudE5hbWVzcGFjZSAgPSAnLicgKyBuYW1lc3BhY2UsXG4gICAgICAgIG1vZHVsZU5hbWVzcGFjZSA9ICdtb2R1bGUtJyArIG5hbWVzcGFjZSxcblxuICAgICAgICAkd2luZG93ICAgICAgICAgPSAkKHdpbmRvdyksXG4gICAgICAgICRtb2R1bGUgICAgICAgICA9ICQodGhpcyksXG4gICAgICAgICRwbGFjZWhvbGRlciAgICA9ICRtb2R1bGUuZmluZChzZWxlY3Rvci5wbGFjZWhvbGRlciksXG4gICAgICAgICRpY29uICAgICAgICAgICA9ICRtb2R1bGUuZmluZChzZWxlY3Rvci5pY29uKSxcbiAgICAgICAgJGVtYmVkICAgICAgICAgID0gJG1vZHVsZS5maW5kKHNlbGVjdG9yLmVtYmVkKSxcblxuICAgICAgICBlbGVtZW50ICAgICAgICAgPSB0aGlzLFxuICAgICAgICBpbnN0YW5jZSAgICAgICAgPSAkbW9kdWxlLmRhdGEobW9kdWxlTmFtZXNwYWNlKSxcbiAgICAgICAgbW9kdWxlXG4gICAgICA7XG5cbiAgICAgIG1vZHVsZSA9IHtcblxuICAgICAgICBpbml0aWFsaXplOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0luaXRpYWxpemluZyBlbWJlZCcpO1xuICAgICAgICAgIG1vZHVsZS5kZXRlcm1pbmUuYXV0b3BsYXkoKTtcbiAgICAgICAgICBtb2R1bGUuY3JlYXRlKCk7XG4gICAgICAgICAgbW9kdWxlLmJpbmQuZXZlbnRzKCk7XG4gICAgICAgICAgbW9kdWxlLmluc3RhbnRpYXRlKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5zdGFudGlhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTdG9yaW5nIGluc3RhbmNlIG9mIG1vZHVsZScsIG1vZHVsZSk7XG4gICAgICAgICAgaW5zdGFuY2UgPSBtb2R1bGU7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLmRhdGEobW9kdWxlTmFtZXNwYWNlLCBtb2R1bGUpXG4gICAgICAgICAgO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlc3Ryb3k6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdEZXN0cm95aW5nIHByZXZpb3VzIGluc3RhbmNlIG9mIGVtYmVkJyk7XG4gICAgICAgICAgbW9kdWxlLnJlc2V0KCk7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLnJlbW92ZURhdGEobW9kdWxlTmFtZXNwYWNlKVxuICAgICAgICAgICAgLm9mZihldmVudE5hbWVzcGFjZSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVmcmVzaDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1JlZnJlc2hpbmcgc2VsZWN0b3IgY2FjaGUnKTtcbiAgICAgICAgICAkcGxhY2Vob2xkZXIgPSAkbW9kdWxlLmZpbmQoc2VsZWN0b3IucGxhY2Vob2xkZXIpO1xuICAgICAgICAgICRpY29uICAgICAgICA9ICRtb2R1bGUuZmluZChzZWxlY3Rvci5pY29uKTtcbiAgICAgICAgICAkZW1iZWQgICAgICAgPSAkbW9kdWxlLmZpbmQoc2VsZWN0b3IuZW1iZWQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGJpbmQ6IHtcbiAgICAgICAgICBldmVudHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoIG1vZHVsZS5oYXMucGxhY2Vob2xkZXIoKSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdBZGRpbmcgcGxhY2Vob2xkZXIgZXZlbnRzJyk7XG4gICAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgICAub24oJ2NsaWNrJyArIGV2ZW50TmFtZXNwYWNlLCBzZWxlY3Rvci5wbGFjZWhvbGRlciwgbW9kdWxlLmNyZWF0ZUFuZFNob3cpXG4gICAgICAgICAgICAgICAgLm9uKCdjbGljaycgKyBldmVudE5hbWVzcGFjZSwgc2VsZWN0b3IuaWNvbiwgbW9kdWxlLmNyZWF0ZUFuZFNob3cpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgY3JlYXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyID0gbW9kdWxlLmdldC5wbGFjZWhvbGRlcigpXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKHBsYWNlaG9sZGVyKSB7XG4gICAgICAgICAgICBtb2R1bGUuY3JlYXRlUGxhY2Vob2xkZXIoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBtb2R1bGUuY3JlYXRlQW5kU2hvdygpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBjcmVhdGVQbGFjZWhvbGRlcjogZnVuY3Rpb24ocGxhY2Vob2xkZXIpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIGljb24gID0gbW9kdWxlLmdldC5pY29uKCksXG4gICAgICAgICAgICB1cmwgICA9IG1vZHVsZS5nZXQudXJsKCksXG4gICAgICAgICAgICBlbWJlZCA9IG1vZHVsZS5nZW5lcmF0ZS5lbWJlZCh1cmwpXG4gICAgICAgICAgO1xuICAgICAgICAgIHBsYWNlaG9sZGVyID0gcGxhY2Vob2xkZXIgfHwgbW9kdWxlLmdldC5wbGFjZWhvbGRlcigpO1xuICAgICAgICAgICRtb2R1bGUuaHRtbCggdGVtcGxhdGVzLnBsYWNlaG9sZGVyKHBsYWNlaG9sZGVyLCBpY29uKSApO1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ3JlYXRpbmcgcGxhY2Vob2xkZXIgZm9yIGVtYmVkJywgcGxhY2Vob2xkZXIsIGljb24pO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNyZWF0ZUVtYmVkOiBmdW5jdGlvbih1cmwpIHtcbiAgICAgICAgICBtb2R1bGUucmVmcmVzaCgpO1xuICAgICAgICAgIHVybCA9IHVybCB8fCBtb2R1bGUuZ2V0LnVybCgpO1xuICAgICAgICAgICRlbWJlZCA9ICQoJzxkaXYvPicpXG4gICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLmVtYmVkKVxuICAgICAgICAgICAgLmh0bWwoIG1vZHVsZS5nZW5lcmF0ZS5lbWJlZCh1cmwpIClcbiAgICAgICAgICAgIC5hcHBlbmRUbygkbW9kdWxlKVxuICAgICAgICAgIDtcbiAgICAgICAgICBzZXR0aW5ncy5vbkNyZWF0ZS5jYWxsKGVsZW1lbnQsIHVybCk7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdDcmVhdGluZyBlbWJlZCBvYmplY3QnLCAkZW1iZWQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNoYW5nZUVtYmVkOiBmdW5jdGlvbih1cmwpIHtcbiAgICAgICAgICAkZW1iZWRcbiAgICAgICAgICAgIC5odG1sKCBtb2R1bGUuZ2VuZXJhdGUuZW1iZWQodXJsKSApXG4gICAgICAgICAgO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNyZWF0ZUFuZFNob3c6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5jcmVhdGVFbWJlZCgpO1xuICAgICAgICAgIG1vZHVsZS5zaG93KCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLy8gc2V0cyBuZXcgZW1iZWRcbiAgICAgICAgY2hhbmdlOiBmdW5jdGlvbihzb3VyY2UsIGlkLCB1cmwpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NoYW5naW5nIHZpZGVvIHRvICcsIHNvdXJjZSwgaWQsIHVybCk7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLmRhdGEobWV0YWRhdGEuc291cmNlLCBzb3VyY2UpXG4gICAgICAgICAgICAuZGF0YShtZXRhZGF0YS5pZCwgaWQpXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKHVybCkge1xuICAgICAgICAgICAgJG1vZHVsZS5kYXRhKG1ldGFkYXRhLnVybCwgdXJsKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAkbW9kdWxlLnJlbW92ZURhdGEobWV0YWRhdGEudXJsKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYobW9kdWxlLmhhcy5lbWJlZCgpKSB7XG4gICAgICAgICAgICBtb2R1bGUuY2hhbmdlRW1iZWQoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBtb2R1bGUuY3JlYXRlKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIC8vIGNsZWFycyBlbWJlZFxuICAgICAgICByZXNldDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdDbGVhcmluZyBlbWJlZCBhbmQgc2hvd2luZyBwbGFjZWhvbGRlcicpO1xuICAgICAgICAgIG1vZHVsZS5yZW1vdmUuYWN0aXZlKCk7XG4gICAgICAgICAgbW9kdWxlLnJlbW92ZS5lbWJlZCgpO1xuICAgICAgICAgIG1vZHVsZS5zaG93UGxhY2Vob2xkZXIoKTtcbiAgICAgICAgICBzZXR0aW5ncy5vblJlc2V0LmNhbGwoZWxlbWVudCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLy8gc2hvd3MgY3VycmVudCBlbWJlZFxuICAgICAgICBzaG93OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Nob3dpbmcgZW1iZWQnKTtcbiAgICAgICAgICBtb2R1bGUuc2V0LmFjdGl2ZSgpO1xuICAgICAgICAgIHNldHRpbmdzLm9uRGlzcGxheS5jYWxsKGVsZW1lbnQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGhpZGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSGlkaW5nIGVtYmVkJyk7XG4gICAgICAgICAgbW9kdWxlLnNob3dQbGFjZWhvbGRlcigpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHNob3dQbGFjZWhvbGRlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdTaG93aW5nIHBsYWNlaG9sZGVyIGltYWdlJyk7XG4gICAgICAgICAgbW9kdWxlLnJlbW92ZS5hY3RpdmUoKTtcbiAgICAgICAgICBzZXR0aW5ncy5vblBsYWNlaG9sZGVyRGlzcGxheS5jYWxsKGVsZW1lbnQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGdldDoge1xuICAgICAgICAgIGlkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBzZXR0aW5ncy5pZCB8fCAkbW9kdWxlLmRhdGEobWV0YWRhdGEuaWQpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcGxhY2Vob2xkZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIHNldHRpbmdzLnBsYWNlaG9sZGVyIHx8ICRtb2R1bGUuZGF0YShtZXRhZGF0YS5wbGFjZWhvbGRlcik7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBpY29uOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAoc2V0dGluZ3MuaWNvbilcbiAgICAgICAgICAgICAgPyBzZXR0aW5ncy5pY29uXG4gICAgICAgICAgICAgIDogKCRtb2R1bGUuZGF0YShtZXRhZGF0YS5pY29uKSAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICAgID8gJG1vZHVsZS5kYXRhKG1ldGFkYXRhLmljb24pXG4gICAgICAgICAgICAgICAgOiBtb2R1bGUuZGV0ZXJtaW5lLmljb24oKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc291cmNlOiBmdW5jdGlvbih1cmwpIHtcbiAgICAgICAgICAgIHJldHVybiAoc2V0dGluZ3Muc291cmNlKVxuICAgICAgICAgICAgICA/IHNldHRpbmdzLnNvdXJjZVxuICAgICAgICAgICAgICA6ICgkbW9kdWxlLmRhdGEobWV0YWRhdGEuc291cmNlKSAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICAgID8gJG1vZHVsZS5kYXRhKG1ldGFkYXRhLnNvdXJjZSlcbiAgICAgICAgICAgICAgICA6IG1vZHVsZS5kZXRlcm1pbmUuc291cmNlKClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHR5cGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIHNvdXJjZSA9IG1vZHVsZS5nZXQuc291cmNlKCk7XG4gICAgICAgICAgICByZXR1cm4gKHNvdXJjZXNbc291cmNlXSAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICA/IHNvdXJjZXNbc291cmNlXS50eXBlXG4gICAgICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHVybDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gKHNldHRpbmdzLnVybClcbiAgICAgICAgICAgICAgPyBzZXR0aW5ncy51cmxcbiAgICAgICAgICAgICAgOiAoJG1vZHVsZS5kYXRhKG1ldGFkYXRhLnVybCkgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICA/ICRtb2R1bGUuZGF0YShtZXRhZGF0YS51cmwpXG4gICAgICAgICAgICAgICAgOiBtb2R1bGUuZGV0ZXJtaW5lLnVybCgpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGRldGVybWluZToge1xuICAgICAgICAgIGF1dG9wbGF5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKG1vZHVsZS5zaG91bGQuYXV0b3BsYXkoKSkge1xuICAgICAgICAgICAgICBzZXR0aW5ncy5hdXRvcGxheSA9IHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzb3VyY2U6IGZ1bmN0aW9uKHVybCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIG1hdGNoZWRTb3VyY2UgPSBmYWxzZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgdXJsID0gdXJsIHx8IG1vZHVsZS5nZXQudXJsKCk7XG4gICAgICAgICAgICBpZih1cmwpIHtcbiAgICAgICAgICAgICAgJC5lYWNoKHNvdXJjZXMsIGZ1bmN0aW9uKG5hbWUsIHNvdXJjZSkge1xuICAgICAgICAgICAgICAgIGlmKHVybC5zZWFyY2goc291cmNlLmRvbWFpbikgIT09IC0xKSB7XG4gICAgICAgICAgICAgICAgICBtYXRjaGVkU291cmNlID0gbmFtZTtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG1hdGNoZWRTb3VyY2U7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBpY29uOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBzb3VyY2UgPSBtb2R1bGUuZ2V0LnNvdXJjZSgpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICByZXR1cm4gKHNvdXJjZXNbc291cmNlXSAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICA/IHNvdXJjZXNbc291cmNlXS5pY29uXG4gICAgICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHVybDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgaWQgICAgID0gc2V0dGluZ3MuaWQgICAgIHx8ICRtb2R1bGUuZGF0YShtZXRhZGF0YS5pZCksXG4gICAgICAgICAgICAgIHNvdXJjZSA9IHNldHRpbmdzLnNvdXJjZSB8fCAkbW9kdWxlLmRhdGEobWV0YWRhdGEuc291cmNlKSxcbiAgICAgICAgICAgICAgdXJsXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICB1cmwgPSAoc291cmNlc1tzb3VyY2VdICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgID8gc291cmNlc1tzb3VyY2VdLnVybC5yZXBsYWNlKCd7aWR9JywgaWQpXG4gICAgICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHVybCkge1xuICAgICAgICAgICAgICAkbW9kdWxlLmRhdGEobWV0YWRhdGEudXJsLCB1cmwpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHVybDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cblxuICAgICAgICBzZXQ6IHtcbiAgICAgICAgICBhY3RpdmU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZS5hZGRDbGFzcyhjbGFzc05hbWUuYWN0aXZlKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVtb3ZlOiB7XG4gICAgICAgICAgYWN0aXZlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRtb2R1bGUucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBlbWJlZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkZW1iZWQuZW1wdHkoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZW5jb2RlOiB7XG4gICAgICAgICAgcGFyYW1ldGVyczogZnVuY3Rpb24ocGFyYW1ldGVycykge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHVybFN0cmluZyA9IFtdLFxuICAgICAgICAgICAgICBpbmRleFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgZm9yIChpbmRleCBpbiBwYXJhbWV0ZXJzKSB7XG4gICAgICAgICAgICAgIHVybFN0cmluZy5wdXNoKCBlbmNvZGVVUklDb21wb25lbnQoaW5kZXgpICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KCBwYXJhbWV0ZXJzW2luZGV4XSApICk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gdXJsU3RyaW5nLmpvaW4oJyZhbXA7Jyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGdlbmVyYXRlOiB7XG4gICAgICAgICAgZW1iZWQ6IGZ1bmN0aW9uKHVybCkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdHZW5lcmF0aW5nIGVtYmVkIGh0bWwnKTtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBzb3VyY2UgPSBtb2R1bGUuZ2V0LnNvdXJjZSgpLFxuICAgICAgICAgICAgICBodG1sLFxuICAgICAgICAgICAgICBwYXJhbWV0ZXJzXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICB1cmwgPSBtb2R1bGUuZ2V0LnVybCh1cmwpO1xuICAgICAgICAgICAgaWYodXJsKSB7XG4gICAgICAgICAgICAgIHBhcmFtZXRlcnMgPSBtb2R1bGUuZ2VuZXJhdGUucGFyYW1ldGVycyhzb3VyY2UpO1xuICAgICAgICAgICAgICBodG1sICAgICAgID0gdGVtcGxhdGVzLmlmcmFtZSh1cmwsIHBhcmFtZXRlcnMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5ub1VSTCwgJG1vZHVsZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gaHRtbDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHBhcmFtZXRlcnM6IGZ1bmN0aW9uKHNvdXJjZSwgZXh0cmFQYXJhbWV0ZXJzKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgcGFyYW1ldGVycyA9IChzb3VyY2VzW3NvdXJjZV0gJiYgc291cmNlc1tzb3VyY2VdLnBhcmFtZXRlcnMgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICA/IHNvdXJjZXNbc291cmNlXS5wYXJhbWV0ZXJzKHNldHRpbmdzKVxuICAgICAgICAgICAgICAgIDoge31cbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGV4dHJhUGFyYW1ldGVycyA9IGV4dHJhUGFyYW1ldGVycyB8fCBzZXR0aW5ncy5wYXJhbWV0ZXJzO1xuICAgICAgICAgICAgaWYoZXh0cmFQYXJhbWV0ZXJzKSB7XG4gICAgICAgICAgICAgIHBhcmFtZXRlcnMgPSAkLmV4dGVuZCh7fSwgcGFyYW1ldGVycywgZXh0cmFQYXJhbWV0ZXJzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHBhcmFtZXRlcnMgPSBzZXR0aW5ncy5vbkVtYmVkKHBhcmFtZXRlcnMpO1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZS5lbmNvZGUucGFyYW1ldGVycyhwYXJhbWV0ZXJzKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgaGFzOiB7XG4gICAgICAgICAgZW1iZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICgkZW1iZWQubGVuZ3RoID4gMCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBwbGFjZWhvbGRlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gc2V0dGluZ3MucGxhY2Vob2xkZXIgfHwgJG1vZHVsZS5kYXRhKG1ldGFkYXRhLnBsYWNlaG9sZGVyKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2hvdWxkOiB7XG4gICAgICAgICAgYXV0b3BsYXk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIChzZXR0aW5ncy5hdXRvcGxheSA9PT0gJ2F1dG8nKVxuICAgICAgICAgICAgICA/IChzZXR0aW5ncy5wbGFjZWhvbGRlciB8fCAkbW9kdWxlLmRhdGEobWV0YWRhdGEucGxhY2Vob2xkZXIpICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgIDogc2V0dGluZ3MuYXV0b3BsYXlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgaXM6IHtcbiAgICAgICAgICB2aWRlbzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlLmdldC50eXBlKCkgPT0gJ3ZpZGVvJztcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0dGluZzogZnVuY3Rpb24obmFtZSwgdmFsdWUpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NoYW5naW5nIHNldHRpbmcnLCBuYW1lLCB2YWx1ZSk7XG4gICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChuYW1lKSApIHtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIHNldHRpbmdzLCBuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBpZigkLmlzUGxhaW5PYmplY3Qoc2V0dGluZ3NbbmFtZV0pKSB7XG4gICAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIHNldHRpbmdzW25hbWVdLCB2YWx1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgc2V0dGluZ3NbbmFtZV0gPSB2YWx1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gc2V0dGluZ3NbbmFtZV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnRlcm5hbDogZnVuY3Rpb24obmFtZSwgdmFsdWUpIHtcbiAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgbW9kdWxlLCBuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBtb2R1bGVbbmFtZV0gPSB2YWx1ZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZGVidWc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1Zy5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdmVyYm9zZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy52ZXJib3NlICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UubG9nKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBlcnJvcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCkge1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmVycm9yLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvci5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgcGVyZm9ybWFuY2U6IHtcbiAgICAgICAgICBsb2c6IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBjdXJyZW50VGltZSxcbiAgICAgICAgICAgICAgZXhlY3V0aW9uVGltZSxcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBjdXJyZW50VGltZSAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG4gICAgICAgICAgICAgIHByZXZpb3VzVGltZSAgPSB0aW1lIHx8IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lID0gY3VycmVudFRpbWUgLSBwcmV2aW91c1RpbWU7XG4gICAgICAgICAgICAgIHRpbWUgICAgICAgICAgPSBjdXJyZW50VGltZTtcbiAgICAgICAgICAgICAgcGVyZm9ybWFuY2UucHVzaCh7XG4gICAgICAgICAgICAgICAgJ05hbWUnICAgICAgICAgICA6IG1lc3NhZ2VbMF0sXG4gICAgICAgICAgICAgICAgJ0FyZ3VtZW50cycgICAgICA6IFtdLnNsaWNlLmNhbGwobWVzc2FnZSwgMSkgfHwgJycsXG4gICAgICAgICAgICAgICAgJ0VsZW1lbnQnICAgICAgICA6IGVsZW1lbnQsXG4gICAgICAgICAgICAgICAgJ0V4ZWN1dGlvbiBUaW1lJyA6IGV4ZWN1dGlvblRpbWVcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lciA9IHNldFRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLmRpc3BsYXksIDUwMCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXNwbGF5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICB0aXRsZSA9IHNldHRpbmdzLm5hbWUgKyAnOicsXG4gICAgICAgICAgICAgIHRvdGFsVGltZSA9IDBcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHRpbWUgPSBmYWxzZTtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICB0b3RhbFRpbWUgKz0gZGF0YVsnRXhlY3V0aW9uIFRpbWUnXTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgdGl0bGUgKz0gJyAnICsgdG90YWxUaW1lICsgJ21zJztcbiAgICAgICAgICAgIGlmKG1vZHVsZVNlbGVjdG9yKSB7XG4gICAgICAgICAgICAgIHRpdGxlICs9ICcgXFwnJyArIG1vZHVsZVNlbGVjdG9yICsgJ1xcJyc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZigkYWxsTW9kdWxlcy5sZW5ndGggPiAxKSB7XG4gICAgICAgICAgICAgIHRpdGxlICs9ICcgJyArICcoJyArICRhbGxNb2R1bGVzLmxlbmd0aCArICcpJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCAoY29uc29sZS5ncm91cCAhPT0gdW5kZWZpbmVkIHx8IGNvbnNvbGUudGFibGUgIT09IHVuZGVmaW5lZCkgJiYgcGVyZm9ybWFuY2UubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKHRpdGxlKTtcbiAgICAgICAgICAgICAgaWYoY29uc29sZS50YWJsZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUudGFibGUocGVyZm9ybWFuY2UpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGFbJ05hbWUnXSArICc6ICcgKyBkYXRhWydFeGVjdXRpb24gVGltZSddKydtcycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHBlcmZvcm1hbmNlID0gW107XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnZva2U6IGZ1bmN0aW9uKHF1ZXJ5LCBwYXNzZWRBcmd1bWVudHMsIGNvbnRleHQpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIG9iamVjdCA9IGluc3RhbmNlLFxuICAgICAgICAgICAgbWF4RGVwdGgsXG4gICAgICAgICAgICBmb3VuZCxcbiAgICAgICAgICAgIHJlc3BvbnNlXG4gICAgICAgICAgO1xuICAgICAgICAgIHBhc3NlZEFyZ3VtZW50cyA9IHBhc3NlZEFyZ3VtZW50cyB8fCBxdWVyeUFyZ3VtZW50cztcbiAgICAgICAgICBjb250ZXh0ICAgICAgICAgPSBlbGVtZW50ICAgICAgICAgfHwgY29udGV4dDtcbiAgICAgICAgICBpZih0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycgJiYgb2JqZWN0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5ICAgID0gcXVlcnkuc3BsaXQoL1tcXC4gXS8pO1xuICAgICAgICAgICAgbWF4RGVwdGggPSBxdWVyeS5sZW5ndGggLSAxO1xuICAgICAgICAgICAgJC5lYWNoKHF1ZXJ5LCBmdW5jdGlvbihkZXB0aCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgdmFyIGNhbWVsQ2FzZVZhbHVlID0gKGRlcHRoICE9IG1heERlcHRoKVxuICAgICAgICAgICAgICAgID8gdmFsdWUgKyBxdWVyeVtkZXB0aCArIDFdLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcXVlcnlbZGVwdGggKyAxXS5zbGljZSgxKVxuICAgICAgICAgICAgICAgIDogcXVlcnlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbY2FtZWxDYXNlVmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoICQuaXNQbGFpbk9iamVjdCggb2JqZWN0W3ZhbHVlXSApICYmIChkZXB0aCAhPSBtYXhEZXB0aCkgKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBvYmplY3RbdmFsdWVdICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3IubWV0aG9kLCBxdWVyeSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCAkLmlzRnVuY3Rpb24oIGZvdW5kICkgKSB7XG4gICAgICAgICAgICByZXNwb25zZSA9IGZvdW5kLmFwcGx5KGNvbnRleHQsIHBhc3NlZEFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYoZm91bmQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZDtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoJC5pc0FycmF5KHJldHVybmVkVmFsdWUpKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlLnB1c2gocmVzcG9uc2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IFtyZXR1cm5lZFZhbHVlLCByZXNwb25zZV07XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYocmVzcG9uc2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IHJlc3BvbnNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gZm91bmQ7XG4gICAgICAgIH1cbiAgICAgIH07XG5cbiAgICAgIGlmKG1ldGhvZEludm9rZWQpIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICAgIH1cbiAgICAgICAgbW9kdWxlLmludm9rZShxdWVyeSk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIGluc3RhbmNlLmludm9rZSgnZGVzdHJveScpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICB9XG4gICAgfSlcbiAgO1xuICByZXR1cm4gKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZClcbiAgICA/IHJldHVybmVkVmFsdWVcbiAgICA6IHRoaXNcbiAgO1xufTtcblxuJC5mbi5lbWJlZC5zZXR0aW5ncyA9IHtcblxuICBuYW1lICAgICAgICA6ICdFbWJlZCcsXG4gIG5hbWVzcGFjZSAgIDogJ2VtYmVkJyxcblxuICBzaWxlbnQgICAgICA6IGZhbHNlLFxuICBkZWJ1ZyAgICAgICA6IGZhbHNlLFxuICB2ZXJib3NlICAgICA6IGZhbHNlLFxuICBwZXJmb3JtYW5jZSA6IHRydWUsXG5cbiAgaWNvbiAgICAgOiBmYWxzZSxcbiAgc291cmNlICAgOiBmYWxzZSxcbiAgdXJsICAgICAgOiBmYWxzZSxcbiAgaWQgICAgICAgOiBmYWxzZSxcblxuICAvLyBzdGFuZGFyZCB2aWRlbyBzZXR0aW5nc1xuICBhdXRvcGxheSAgOiAnYXV0bycsXG4gIGNvbG9yICAgICA6ICcjNDQ0NDQ0JyxcbiAgaGQgICAgICAgIDogdHJ1ZSxcbiAgYnJhbmRlZFVJIDogZmFsc2UsXG5cbiAgLy8gYWRkaXRpb25hbCBwYXJhbWV0ZXJzIHRvIGluY2x1ZGUgd2l0aCB0aGUgZW1iZWRcbiAgcGFyYW1ldGVyczogZmFsc2UsXG5cbiAgb25EaXNwbGF5ICAgICAgICAgICAgOiBmdW5jdGlvbigpIHt9LFxuICBvblBsYWNlaG9sZGVyRGlzcGxheSA6IGZ1bmN0aW9uKCkge30sXG4gIG9uUmVzZXQgICAgICAgICAgICAgIDogZnVuY3Rpb24oKSB7fSxcbiAgb25DcmVhdGUgICAgICAgICAgICAgOiBmdW5jdGlvbih1cmwpIHt9LFxuICBvbkVtYmVkICAgICAgICAgICAgICA6IGZ1bmN0aW9uKHBhcmFtZXRlcnMpIHtcbiAgICByZXR1cm4gcGFyYW1ldGVycztcbiAgfSxcblxuICBtZXRhZGF0YSAgICA6IHtcbiAgICBpZCAgICAgICAgICA6ICdpZCcsXG4gICAgaWNvbiAgICAgICAgOiAnaWNvbicsXG4gICAgcGxhY2Vob2xkZXIgOiAncGxhY2Vob2xkZXInLFxuICAgIHNvdXJjZSAgICAgIDogJ3NvdXJjZScsXG4gICAgdXJsICAgICAgICAgOiAndXJsJ1xuICB9LFxuXG4gIGVycm9yIDoge1xuICAgIG5vVVJMICA6ICdObyBVUkwgc3BlY2lmaWVkJyxcbiAgICBtZXRob2QgOiAnVGhlIG1ldGhvZCB5b3UgY2FsbGVkIGlzIG5vdCBkZWZpbmVkJ1xuICB9LFxuXG4gIGNsYXNzTmFtZSA6IHtcbiAgICBhY3RpdmUgOiAnYWN0aXZlJyxcbiAgICBlbWJlZCAgOiAnZW1iZWQnXG4gIH0sXG5cbiAgc2VsZWN0b3IgOiB7XG4gICAgZW1iZWQgICAgICAgOiAnLmVtYmVkJyxcbiAgICBwbGFjZWhvbGRlciA6ICcucGxhY2Vob2xkZXInLFxuICAgIGljb24gICAgICAgIDogJy5pY29uJ1xuICB9LFxuXG4gIHNvdXJjZXM6IHtcbiAgICB5b3V0dWJlOiB7XG4gICAgICBuYW1lICAgOiAneW91dHViZScsXG4gICAgICB0eXBlICAgOiAndmlkZW8nLFxuICAgICAgaWNvbiAgIDogJ3ZpZGVvIHBsYXknLFxuICAgICAgZG9tYWluIDogJ3lvdXR1YmUuY29tJyxcbiAgICAgIHVybCAgICA6ICcvL3d3dy55b3V0dWJlLmNvbS9lbWJlZC97aWR9JyxcbiAgICAgIHBhcmFtZXRlcnM6IGZ1bmN0aW9uKHNldHRpbmdzKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgYXV0b2hpZGUgICAgICAgOiAhc2V0dGluZ3MuYnJhbmRlZFVJLFxuICAgICAgICAgIGF1dG9wbGF5ICAgICAgIDogc2V0dGluZ3MuYXV0b3BsYXksXG4gICAgICAgICAgY29sb3IgICAgICAgICAgOiBzZXR0aW5ncy5jb2xvciB8fCB1bmRlZmluZWQsXG4gICAgICAgICAgaHEgICAgICAgICAgICAgOiBzZXR0aW5ncy5oZCxcbiAgICAgICAgICBqc2FwaSAgICAgICAgICA6IHNldHRpbmdzLmFwaSxcbiAgICAgICAgICBtb2Rlc3RicmFuZGluZyA6ICFzZXR0aW5ncy5icmFuZGVkVUlcbiAgICAgICAgfTtcbiAgICAgIH1cbiAgICB9LFxuICAgIHZpbWVvOiB7XG4gICAgICBuYW1lICAgOiAndmltZW8nLFxuICAgICAgdHlwZSAgIDogJ3ZpZGVvJyxcbiAgICAgIGljb24gICA6ICd2aWRlbyBwbGF5JyxcbiAgICAgIGRvbWFpbiA6ICd2aW1lby5jb20nLFxuICAgICAgdXJsICAgIDogJy8vcGxheWVyLnZpbWVvLmNvbS92aWRlby97aWR9JyxcbiAgICAgIHBhcmFtZXRlcnM6IGZ1bmN0aW9uKHNldHRpbmdzKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgYXBpICAgICAgOiBzZXR0aW5ncy5hcGksXG4gICAgICAgICAgYXV0b3BsYXkgOiBzZXR0aW5ncy5hdXRvcGxheSxcbiAgICAgICAgICBieWxpbmUgICA6IHNldHRpbmdzLmJyYW5kZWRVSSxcbiAgICAgICAgICBjb2xvciAgICA6IHNldHRpbmdzLmNvbG9yIHx8IHVuZGVmaW5lZCxcbiAgICAgICAgICBwb3J0cmFpdCA6IHNldHRpbmdzLmJyYW5kZWRVSSxcbiAgICAgICAgICB0aXRsZSAgICA6IHNldHRpbmdzLmJyYW5kZWRVSVxuICAgICAgICB9O1xuICAgICAgfVxuICAgIH1cbiAgfSxcblxuICB0ZW1wbGF0ZXM6IHtcbiAgICBpZnJhbWUgOiBmdW5jdGlvbih1cmwsIHBhcmFtZXRlcnMpIHtcbiAgICAgIHZhciBzcmMgPSB1cmw7XG4gICAgICBpZiAocGFyYW1ldGVycykge1xuICAgICAgICAgIHNyYyArPSAnPycgKyBwYXJhbWV0ZXJzO1xuICAgICAgfVxuICAgICAgcmV0dXJuICcnXG4gICAgICAgICsgJzxpZnJhbWUgc3JjPVwiJyArIHNyYyArICdcIidcbiAgICAgICAgKyAnIHdpZHRoPVwiMTAwJVwiIGhlaWdodD1cIjEwMCVcIidcbiAgICAgICAgKyAnIGZyYW1lYm9yZGVyPVwiMFwiIHNjcm9sbGluZz1cIm5vXCIgd2Via2l0QWxsb3dGdWxsU2NyZWVuIG1vemFsbG93ZnVsbHNjcmVlbiBhbGxvd0Z1bGxTY3JlZW4+PC9pZnJhbWU+J1xuICAgICAgO1xuICAgIH0sXG4gICAgcGxhY2Vob2xkZXIgOiBmdW5jdGlvbihpbWFnZSwgaWNvbikge1xuICAgICAgdmFyXG4gICAgICAgIGh0bWwgPSAnJ1xuICAgICAgO1xuICAgICAgaWYoaWNvbikge1xuICAgICAgICBodG1sICs9ICc8aSBjbGFzcz1cIicgKyBpY29uICsgJyBpY29uXCI+PC9pPic7XG4gICAgICB9XG4gICAgICBpZihpbWFnZSkge1xuICAgICAgICBodG1sICs9ICc8aW1nIGNsYXNzPVwicGxhY2Vob2xkZXJcIiBzcmM9XCInICsgaW1hZ2UgKyAnXCI+JztcbiAgICAgIH1cbiAgICAgIHJldHVybiBodG1sO1xuICAgIH1cbiAgfSxcblxuICAvLyBOT1QgWUVUIElNUExFTUVOVEVEXG4gIGFwaSAgICAgOiBmYWxzZSxcbiAgb25QYXVzZSA6IGZ1bmN0aW9uKCkge30sXG4gIG9uUGxheSAgOiBmdW5jdGlvbigpIHt9LFxuICBvblN0b3AgIDogZnVuY3Rpb24oKSB7fVxuXG59O1xuXG5cblxufSkoIGpRdWVyeSwgd2luZG93LCBkb2N1bWVudCApO1xuIl19