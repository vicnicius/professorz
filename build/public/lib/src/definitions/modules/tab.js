/*!
 * # Semantic UI - Tab
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.tab = function (parameters) {

    var
    // use window context if none specified
    $allModules = $.isFunction(this) ? $(window) : $(this),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        initializedHistory = false,
        returnedValue;

    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.tab.settings, parameters) : $.extend({}, $.fn.tab.settings),
          className = settings.className,
          metadata = settings.metadata,
          selector = settings.selector,
          error = settings.error,
          eventNamespace = '.' + settings.namespace,
          moduleNamespace = 'module-' + settings.namespace,
          $module = $(this),
          $context,
          $tabs,
          cache = {},
          firstLoad = true,
          recursionDepth = 0,
          element = this,
          instance = $module.data(moduleNamespace),
          activeTabPath,
          parameterArray,
          module,
          historyEvent;

      module = {

        initialize: function () {
          module.debug('Initializing tab menu item', $module);
          module.fix.callbacks();
          module.determineTabs();

          module.debug('Determining tabs', settings.context, $tabs);
          // set up automatic routing
          if (settings.auto) {
            module.set.auto();
          }
          module.bind.events();

          if (settings.history && !initializedHistory) {
            module.initializeHistory();
            initializedHistory = true;
          }

          module.instantiate();
        },

        instantiate: function () {
          module.verbose('Storing instance of module', module);
          instance = module;
          $module.data(moduleNamespace, module);
        },

        destroy: function () {
          module.debug('Destroying tabs', $module);
          $module.removeData(moduleNamespace).off(eventNamespace);
        },

        bind: {
          events: function () {
            // if using $.tab don't add events
            if (!$.isWindow(element)) {
              module.debug('Attaching tab activation events to element', $module);
              $module.on('click' + eventNamespace, module.event.click);
            }
          }
        },

        determineTabs: function () {
          var $reference;

          // determine tab context
          if (settings.context === 'parent') {
            if ($module.closest(selector.ui).length > 0) {
              $reference = $module.closest(selector.ui);
              module.verbose('Using closest UI element as parent', $reference);
            } else {
              $reference = $module;
            }
            $context = $reference.parent();
            module.verbose('Determined parent element for creating context', $context);
          } else if (settings.context) {
            $context = $(settings.context);
            module.verbose('Using selector for tab context', settings.context, $context);
          } else {
            $context = $('body');
          }
          // find tabs
          if (settings.childrenOnly) {
            $tabs = $context.children(selector.tabs);
            module.debug('Searching tab context children for tabs', $context, $tabs);
          } else {
            $tabs = $context.find(selector.tabs);
            module.debug('Searching tab context for tabs', $context, $tabs);
          }
        },

        fix: {
          callbacks: function () {
            if ($.isPlainObject(parameters) && (parameters.onTabLoad || parameters.onTabInit)) {
              if (parameters.onTabLoad) {
                parameters.onLoad = parameters.onTabLoad;
                delete parameters.onTabLoad;
                module.error(error.legacyLoad, parameters.onLoad);
              }
              if (parameters.onTabInit) {
                parameters.onFirstLoad = parameters.onTabInit;
                delete parameters.onTabInit;
                module.error(error.legacyInit, parameters.onFirstLoad);
              }
              settings = $.extend(true, {}, $.fn.tab.settings, parameters);
            }
          }
        },

        initializeHistory: function () {
          module.debug('Initializing page state');
          if ($.address === undefined) {
            module.error(error.state);
            return false;
          } else {
            if (settings.historyType == 'state') {
              module.debug('Using HTML5 to manage state');
              if (settings.path !== false) {
                $.address.history(true).state(settings.path);
              } else {
                module.error(error.path);
                return false;
              }
            }
            $.address.bind('change', module.event.history.change);
          }
        },

        event: {
          click: function (event) {
            var tabPath = $(this).data(metadata.tab);
            if (tabPath !== undefined) {
              if (settings.history) {
                module.verbose('Updating page state', event);
                $.address.value(tabPath);
              } else {
                module.verbose('Changing tab', event);
                module.changeTab(tabPath);
              }
              event.preventDefault();
            } else {
              module.debug('No tab specified');
            }
          },
          history: {
            change: function (event) {
              var tabPath = event.pathNames.join('/') || module.get.initialPath(),
                  pageTitle = settings.templates.determineTitle(tabPath) || false;
              module.performance.display();
              module.debug('History change event', tabPath, event);
              historyEvent = event;
              if (tabPath !== undefined) {
                module.changeTab(tabPath);
              }
              if (pageTitle) {
                $.address.title(pageTitle);
              }
            }
          }
        },

        refresh: function () {
          if (activeTabPath) {
            module.debug('Refreshing tab', activeTabPath);
            module.changeTab(activeTabPath);
          }
        },

        cache: {

          read: function (cacheKey) {
            return cacheKey !== undefined ? cache[cacheKey] : false;
          },
          add: function (cacheKey, content) {
            cacheKey = cacheKey || activeTabPath;
            module.debug('Adding cached content for', cacheKey);
            cache[cacheKey] = content;
          },
          remove: function (cacheKey) {
            cacheKey = cacheKey || activeTabPath;
            module.debug('Removing cached content for', cacheKey);
            delete cache[cacheKey];
          }
        },

        set: {
          auto: function () {
            var url = typeof settings.path == 'string' ? settings.path.replace(/\/$/, '') + '/{$tab}' : '/{$tab}';
            module.verbose('Setting up automatic tab retrieval from server', url);
            if ($.isPlainObject(settings.apiSettings)) {
              settings.apiSettings.url = url;
            } else {
              settings.apiSettings = {
                url: url
              };
            }
          },
          loading: function (tabPath) {
            var $tab = module.get.tabElement(tabPath),
                isLoading = $tab.hasClass(className.loading);
            if (!isLoading) {
              module.verbose('Setting loading state for', $tab);
              $tab.addClass(className.loading).siblings($tabs).removeClass(className.active + ' ' + className.loading);
              if ($tab.length > 0) {
                settings.onRequest.call($tab[0], tabPath);
              }
            }
          },
          state: function (state) {
            $.address.value(state);
          }
        },

        changeTab: function (tabPath) {
          var pushStateAvailable = window.history && window.history.pushState,
              shouldIgnoreLoad = pushStateAvailable && settings.ignoreFirstLoad && firstLoad,
              remoteContent = settings.auto || $.isPlainObject(settings.apiSettings),

          // only add default path if not remote content
          pathArray = remoteContent && !shouldIgnoreLoad ? module.utilities.pathToArray(tabPath) : module.get.defaultPathArray(tabPath);
          tabPath = module.utilities.arrayToPath(pathArray);
          $.each(pathArray, function (index, tab) {
            var currentPathArray = pathArray.slice(0, index + 1),
                currentPath = module.utilities.arrayToPath(currentPathArray),
                isTab = module.is.tab(currentPath),
                isLastIndex = index + 1 == pathArray.length,
                $tab = module.get.tabElement(currentPath),
                $anchor,
                nextPathArray,
                nextPath,
                isLastTab;
            module.verbose('Looking for tab', tab);
            if (isTab) {
              module.verbose('Tab was found', tab);
              // scope up
              activeTabPath = currentPath;
              parameterArray = module.utilities.filterArray(pathArray, currentPathArray);

              if (isLastIndex) {
                isLastTab = true;
              } else {
                nextPathArray = pathArray.slice(0, index + 2);
                nextPath = module.utilities.arrayToPath(nextPathArray);
                isLastTab = !module.is.tab(nextPath);
                if (isLastTab) {
                  module.verbose('Tab parameters found', nextPathArray);
                }
              }
              if (isLastTab && remoteContent) {
                if (!shouldIgnoreLoad) {
                  module.activate.navigation(currentPath);
                  module.fetch.content(currentPath, tabPath);
                } else {
                  module.debug('Ignoring remote content on first tab load', currentPath);
                  firstLoad = false;
                  module.cache.add(tabPath, $tab.html());
                  module.activate.all(currentPath);
                  settings.onFirstLoad.call($tab[0], currentPath, parameterArray, historyEvent);
                  settings.onLoad.call($tab[0], currentPath, parameterArray, historyEvent);
                }
                return false;
              } else {
                module.debug('Opened local tab', currentPath);
                module.activate.all(currentPath);
                if (!module.cache.read(currentPath)) {
                  module.cache.add(currentPath, true);
                  module.debug('First time tab loaded calling tab init');
                  settings.onFirstLoad.call($tab[0], currentPath, parameterArray, historyEvent);
                }
                settings.onLoad.call($tab[0], currentPath, parameterArray, historyEvent);
              }
            } else if (tabPath.search('/') == -1 && tabPath !== '') {
              // look for in page anchor
              $anchor = $('#' + tabPath + ', a[name="' + tabPath + '"]');
              currentPath = $anchor.closest('[data-tab]').data(metadata.tab);
              $tab = module.get.tabElement(currentPath);
              // if anchor exists use parent tab
              if ($anchor && $anchor.length > 0 && currentPath) {
                module.debug('Anchor link used, opening parent tab', $tab, $anchor);
                if (!$tab.hasClass(className.active)) {
                  setTimeout(function () {
                    module.scrollTo($anchor);
                  }, 0);
                }
                module.activate.all(currentPath);
                if (!module.cache.read(currentPath)) {
                  module.cache.add(currentPath, true);
                  module.debug('First time tab loaded calling tab init');
                  settings.onFirstLoad.call($tab[0], currentPath, parameterArray, historyEvent);
                }
                settings.onLoad.call($tab[0], currentPath, parameterArray, historyEvent);
                return false;
              }
            } else {
              module.error(error.missingTab, $module, $context, currentPath);
              return false;
            }
          });
        },

        scrollTo: function ($element) {
          var scrollOffset = $element && $element.length > 0 ? $element.offset().top : false;
          if (scrollOffset !== false) {
            module.debug('Forcing scroll to an in-page link in a hidden tab', scrollOffset, $element);
            $(document).scrollTop(scrollOffset);
          }
        },

        update: {
          content: function (tabPath, html, evaluateScripts) {
            var $tab = module.get.tabElement(tabPath),
                tab = $tab[0];
            evaluateScripts = evaluateScripts !== undefined ? evaluateScripts : settings.evaluateScripts;
            if (evaluateScripts) {
              module.debug('Updating HTML and evaluating inline scripts', tabPath, html);
              $tab.html(html);
            } else {
              module.debug('Updating HTML', tabPath, html);
              tab.innerHTML = html;
            }
          }
        },

        fetch: {

          content: function (tabPath, fullTabPath) {
            var $tab = module.get.tabElement(tabPath),
                apiSettings = {
              dataType: 'html',
              encodeParameters: false,
              on: 'now',
              cache: settings.alwaysRefresh,
              headers: {
                'X-Remote': true
              },
              onSuccess: function (response) {
                if (settings.cacheType == 'response') {
                  module.cache.add(fullTabPath, response);
                }
                module.update.content(tabPath, response);
                if (tabPath == activeTabPath) {
                  module.debug('Content loaded', tabPath);
                  module.activate.tab(tabPath);
                } else {
                  module.debug('Content loaded in background', tabPath);
                }
                settings.onFirstLoad.call($tab[0], tabPath, parameterArray, historyEvent);
                settings.onLoad.call($tab[0], tabPath, parameterArray, historyEvent);
                if (settings.cacheType != 'response') {
                  module.cache.add(fullTabPath, $tab.html());
                }
              },
              urlData: {
                tab: fullTabPath
              }
            },
                request = $tab.api('get request') || false,
                existingRequest = request && request.state() === 'pending',
                requestSettings,
                cachedContent;

            fullTabPath = fullTabPath || tabPath;
            cachedContent = module.cache.read(fullTabPath);

            if (settings.cache && cachedContent) {
              module.activate.tab(tabPath);
              module.debug('Adding cached content', fullTabPath);
              if (settings.evaluateScripts == 'once') {
                module.update.content(tabPath, cachedContent, false);
              } else {
                module.update.content(tabPath, cachedContent);
              }
              settings.onLoad.call($tab[0], tabPath, parameterArray, historyEvent);
            } else if (existingRequest) {
              module.set.loading(tabPath);
              module.debug('Content is already loading', fullTabPath);
            } else if ($.api !== undefined) {
              requestSettings = $.extend(true, {}, settings.apiSettings, apiSettings);
              module.debug('Retrieving remote content', fullTabPath, requestSettings);
              module.set.loading(tabPath);
              $tab.api(requestSettings);
            } else {
              module.error(error.api);
            }
          }
        },

        activate: {
          all: function (tabPath) {
            module.activate.tab(tabPath);
            module.activate.navigation(tabPath);
          },
          tab: function (tabPath) {
            var $tab = module.get.tabElement(tabPath),
                $deactiveTabs = settings.deactivate == 'siblings' ? $tab.siblings($tabs) : $tabs.not($tab),
                isActive = $tab.hasClass(className.active);
            module.verbose('Showing tab content for', $tab);
            if (!isActive) {
              $tab.addClass(className.active);
              $deactiveTabs.removeClass(className.active + ' ' + className.loading);
              if ($tab.length > 0) {
                settings.onVisible.call($tab[0], tabPath);
              }
            }
          },
          navigation: function (tabPath) {
            var $navigation = module.get.navElement(tabPath),
                $deactiveNavigation = settings.deactivate == 'siblings' ? $navigation.siblings($allModules) : $allModules.not($navigation),
                isActive = $navigation.hasClass(className.active);
            module.verbose('Activating tab navigation for', $navigation, tabPath);
            if (!isActive) {
              $navigation.addClass(className.active);
              $deactiveNavigation.removeClass(className.active + ' ' + className.loading);
            }
          }
        },

        deactivate: {
          all: function () {
            module.deactivate.navigation();
            module.deactivate.tabs();
          },
          navigation: function () {
            $allModules.removeClass(className.active);
          },
          tabs: function () {
            $tabs.removeClass(className.active + ' ' + className.loading);
          }
        },

        is: {
          tab: function (tabName) {
            return tabName !== undefined ? module.get.tabElement(tabName).length > 0 : false;
          }
        },

        get: {
          initialPath: function () {
            return $allModules.eq(0).data(metadata.tab) || $tabs.eq(0).data(metadata.tab);
          },
          path: function () {
            return $.address.value();
          },
          // adds default tabs to tab path
          defaultPathArray: function (tabPath) {
            return module.utilities.pathToArray(module.get.defaultPath(tabPath));
          },
          defaultPath: function (tabPath) {
            var $defaultNav = $allModules.filter('[data-' + metadata.tab + '^="' + tabPath + '/"]').eq(0),
                defaultTab = $defaultNav.data(metadata.tab) || false;
            if (defaultTab) {
              module.debug('Found default tab', defaultTab);
              if (recursionDepth < settings.maxDepth) {
                recursionDepth++;
                return module.get.defaultPath(defaultTab);
              }
              module.error(error.recursion);
            } else {
              module.debug('No default tabs found for', tabPath, $tabs);
            }
            recursionDepth = 0;
            return tabPath;
          },
          navElement: function (tabPath) {
            tabPath = tabPath || activeTabPath;
            return $allModules.filter('[data-' + metadata.tab + '="' + tabPath + '"]');
          },
          tabElement: function (tabPath) {
            var $fullPathTab, $simplePathTab, tabPathArray, lastTab;
            tabPath = tabPath || activeTabPath;
            tabPathArray = module.utilities.pathToArray(tabPath);
            lastTab = module.utilities.last(tabPathArray);
            $fullPathTab = $tabs.filter('[data-' + metadata.tab + '="' + tabPath + '"]');
            $simplePathTab = $tabs.filter('[data-' + metadata.tab + '="' + lastTab + '"]');
            return $fullPathTab.length > 0 ? $fullPathTab : $simplePathTab;
          },
          tab: function () {
            return activeTabPath;
          }
        },

        utilities: {
          filterArray: function (keepArray, removeArray) {
            return $.grep(keepArray, function (keepValue) {
              return $.inArray(keepValue, removeArray) == -1;
            });
          },
          last: function (array) {
            return $.isArray(array) ? array[array.length - 1] : false;
          },
          pathToArray: function (pathName) {
            if (pathName === undefined) {
              pathName = activeTabPath;
            }
            return typeof pathName == 'string' ? pathName.split('/') : [pathName];
          },
          arrayToPath: function (pathArray) {
            return $.isArray(pathArray) ? pathArray.join('/') : false;
          }
        },

        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                module.error(error.method, query);
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };
      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });
    return returnedValue !== undefined ? returnedValue : this;
  };

  // shortcut for tabbed content with no defined navigation
  $.tab = function () {
    $(window).tab.apply(this, arguments);
  };

  $.fn.tab.settings = {

    name: 'Tab',
    namespace: 'tab',

    silent: false,
    debug: false,
    verbose: false,
    performance: true,

    auto: false, // uses pjax style endpoints fetching content from same url with remote-content headers
    history: false, // use browser history
    historyType: 'hash', // #/ or html5 state
    path: false, // base path of url

    context: false, // specify a context that tabs must appear inside
    childrenOnly: false, // use only tabs that are children of context
    maxDepth: 25, // max depth a tab can be nested

    deactivate: 'siblings', // whether tabs should deactivate sibling menu elements or all elements initialized together

    alwaysRefresh: false, // load tab content new every tab click
    cache: true, // cache the content requests to pull locally
    cacheType: 'response', // Whether to cache exact response, or to html cache contents after scripts execute
    ignoreFirstLoad: false, // don't load remote content on first load

    apiSettings: false, // settings for api call
    evaluateScripts: 'once', // whether inline scripts should be parsed (true/false/once). Once will not re-evaluate on cached content

    onFirstLoad: function (tabPath, parameterArray, historyEvent) {}, // called first time loaded
    onLoad: function (tabPath, parameterArray, historyEvent) {}, // called on every load
    onVisible: function (tabPath, parameterArray, historyEvent) {}, // called every time tab visible
    onRequest: function (tabPath, parameterArray, historyEvent) {}, // called ever time a tab beings loading remote content

    templates: {
      determineTitle: function (tabArray) {} // returns page title for path
    },

    error: {
      api: 'You attempted to load content without API module',
      method: 'The method you called is not defined',
      missingTab: 'Activated tab cannot be found. Tabs are case-sensitive.',
      noContent: 'The tab you specified is missing a content url.',
      path: 'History enabled, but no path was specified',
      recursion: 'Max recursive depth reached',
      legacyInit: 'onTabInit has been renamed to onFirstLoad in 2.0, please adjust your code.',
      legacyLoad: 'onTabLoad has been renamed to onLoad in 2.0. Please adjust your code',
      state: 'History requires Asual\'s Address library <https://github.com/asual/jquery-address>'
    },

    metadata: {
      tab: 'tab',
      loaded: 'loaded',
      promise: 'promise'
    },

    className: {
      loading: 'loading',
      active: 'active'
    },

    selector: {
      tabs: '.ui.tab',
      ui: '.ui'
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL3RhYi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBVUEsQ0FBQyxDQUFDLFVBQVUsQ0FBVixFQUFhLE1BQWIsRUFBcUIsUUFBckIsRUFBK0IsU0FBL0IsRUFBMEM7O0FBRTVDOztBQUVBLFdBQVUsT0FBTyxNQUFQLElBQWlCLFdBQWpCLElBQWdDLE9BQU8sSUFBUCxJQUFlLElBQWhELEdBQ0wsTUFESyxHQUVKLE9BQU8sSUFBUCxJQUFlLFdBQWYsSUFBOEIsS0FBSyxJQUFMLElBQWEsSUFBNUMsR0FDRSxJQURGLEdBRUUsU0FBUyxhQUFULEdBSk47O0FBT0EsSUFBRSxFQUFGLENBQUssR0FBTCxHQUFXLFVBQVMsVUFBVCxFQUFxQjs7QUFFOUI7O0FBRUUsa0JBQWtCLEVBQUUsVUFBRixDQUFhLElBQWIsSUFDWixFQUFFLE1BQUYsQ0FEWSxHQUVaLEVBQUUsSUFBRixDQUpSO0FBQUEsUUFNRSxpQkFBa0IsWUFBWSxRQUFaLElBQXdCLEVBTjVDO0FBQUEsUUFPRSxPQUFrQixJQUFJLElBQUosR0FBVyxPQUFYLEVBUHBCO0FBQUEsUUFRRSxjQUFrQixFQVJwQjtBQUFBLFFBVUUsUUFBa0IsVUFBVSxDQUFWLENBVnBCO0FBQUEsUUFXRSxnQkFBbUIsT0FBTyxLQUFQLElBQWdCLFFBWHJDO0FBQUEsUUFZRSxpQkFBa0IsR0FBRyxLQUFILENBQVMsSUFBVCxDQUFjLFNBQWQsRUFBeUIsQ0FBekIsQ0FacEI7QUFBQSxRQWNFLHFCQUFxQixLQWR2QjtBQUFBLFFBZUUsYUFmRjs7QUFrQkEsZ0JBQ0csSUFESCxDQUNRLFlBQVc7QUFDZixVQUVFLFdBQW9CLEVBQUUsYUFBRixDQUFnQixVQUFoQixDQUFGLEdBQ2QsRUFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUIsRUFBRSxFQUFGLENBQUssR0FBTCxDQUFTLFFBQTVCLEVBQXNDLFVBQXRDLENBRGMsR0FFZCxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsRUFBRSxFQUFGLENBQUssR0FBTCxDQUFTLFFBQXRCLENBSk47QUFBQSxVQU1FLFlBQWtCLFNBQVMsU0FON0I7QUFBQSxVQU9FLFdBQWtCLFNBQVMsUUFQN0I7QUFBQSxVQVFFLFdBQWtCLFNBQVMsUUFSN0I7QUFBQSxVQVNFLFFBQWtCLFNBQVMsS0FUN0I7QUFBQSxVQVdFLGlCQUFrQixNQUFNLFNBQVMsU0FYbkM7QUFBQSxVQVlFLGtCQUFrQixZQUFZLFNBQVMsU0FaekM7QUFBQSxVQWNFLFVBQWtCLEVBQUUsSUFBRixDQWRwQjtBQUFBLFVBZUUsUUFmRjtBQUFBLFVBZ0JFLEtBaEJGO0FBQUEsVUFrQkUsUUFBa0IsRUFsQnBCO0FBQUEsVUFtQkUsWUFBa0IsSUFuQnBCO0FBQUEsVUFvQkUsaUJBQWtCLENBcEJwQjtBQUFBLFVBcUJFLFVBQWtCLElBckJwQjtBQUFBLFVBc0JFLFdBQWtCLFFBQVEsSUFBUixDQUFhLGVBQWIsQ0F0QnBCO0FBQUEsVUF3QkUsYUF4QkY7QUFBQSxVQXlCRSxjQXpCRjtBQUFBLFVBMEJFLE1BMUJGO0FBQUEsVUE0QkUsWUE1QkY7O0FBZ0NBLGVBQVM7O0FBRVAsb0JBQVksWUFBVztBQUNyQixpQkFBTyxLQUFQLENBQWEsNEJBQWIsRUFBMkMsT0FBM0M7QUFDQSxpQkFBTyxHQUFQLENBQVcsU0FBWDtBQUNBLGlCQUFPLGFBQVA7O0FBRUEsaUJBQU8sS0FBUCxDQUFhLGtCQUFiLEVBQWlDLFNBQVMsT0FBMUMsRUFBbUQsS0FBbkQ7O0FBRUEsY0FBRyxTQUFTLElBQVosRUFBa0I7QUFDaEIsbUJBQU8sR0FBUCxDQUFXLElBQVg7QUFDRDtBQUNELGlCQUFPLElBQVAsQ0FBWSxNQUFaOztBQUVBLGNBQUcsU0FBUyxPQUFULElBQW9CLENBQUMsa0JBQXhCLEVBQTRDO0FBQzFDLG1CQUFPLGlCQUFQO0FBQ0EsaUNBQXFCLElBQXJCO0FBQ0Q7O0FBRUQsaUJBQU8sV0FBUDtBQUNELFNBcEJNOztBQXNCUCxxQkFBYSxZQUFZO0FBQ3ZCLGlCQUFPLE9BQVAsQ0FBZSw0QkFBZixFQUE2QyxNQUE3QztBQUNBLHFCQUFXLE1BQVg7QUFDQSxrQkFDRyxJQURILENBQ1EsZUFEUixFQUN5QixNQUR6QjtBQUdELFNBNUJNOztBQThCUCxpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLEtBQVAsQ0FBYSxpQkFBYixFQUFnQyxPQUFoQztBQUNBLGtCQUNHLFVBREgsQ0FDYyxlQURkLEVBRUcsR0FGSCxDQUVPLGNBRlA7QUFJRCxTQXBDTTs7QUFzQ1AsY0FBTTtBQUNKLGtCQUFRLFlBQVc7O0FBRWpCLGdCQUFJLENBQUMsRUFBRSxRQUFGLENBQVksT0FBWixDQUFMLEVBQTZCO0FBQzNCLHFCQUFPLEtBQVAsQ0FBYSw0Q0FBYixFQUEyRCxPQUEzRDtBQUNBLHNCQUNHLEVBREgsQ0FDTSxVQUFVLGNBRGhCLEVBQ2dDLE9BQU8sS0FBUCxDQUFhLEtBRDdDO0FBR0Q7QUFDRjtBQVRHLFNBdENDOztBQWtEUCx1QkFBZSxZQUFXO0FBQ3hCLGNBQ0UsVUFERjs7O0FBS0EsY0FBRyxTQUFTLE9BQVQsS0FBcUIsUUFBeEIsRUFBa0M7QUFDaEMsZ0JBQUcsUUFBUSxPQUFSLENBQWdCLFNBQVMsRUFBekIsRUFBNkIsTUFBN0IsR0FBc0MsQ0FBekMsRUFBNEM7QUFDMUMsMkJBQWEsUUFBUSxPQUFSLENBQWdCLFNBQVMsRUFBekIsQ0FBYjtBQUNBLHFCQUFPLE9BQVAsQ0FBZSxvQ0FBZixFQUFxRCxVQUFyRDtBQUNELGFBSEQsTUFJSztBQUNILDJCQUFhLE9BQWI7QUFDRDtBQUNELHVCQUFXLFdBQVcsTUFBWCxFQUFYO0FBQ0EsbUJBQU8sT0FBUCxDQUFlLGdEQUFmLEVBQWlFLFFBQWpFO0FBQ0QsV0FWRCxNQVdLLElBQUcsU0FBUyxPQUFaLEVBQXFCO0FBQ3hCLHVCQUFXLEVBQUUsU0FBUyxPQUFYLENBQVg7QUFDQSxtQkFBTyxPQUFQLENBQWUsZ0NBQWYsRUFBaUQsU0FBUyxPQUExRCxFQUFtRSxRQUFuRTtBQUNELFdBSEksTUFJQTtBQUNILHVCQUFXLEVBQUUsTUFBRixDQUFYO0FBQ0Q7O0FBRUQsY0FBRyxTQUFTLFlBQVosRUFBMEI7QUFDeEIsb0JBQVEsU0FBUyxRQUFULENBQWtCLFNBQVMsSUFBM0IsQ0FBUjtBQUNBLG1CQUFPLEtBQVAsQ0FBYSx5Q0FBYixFQUF3RCxRQUF4RCxFQUFrRSxLQUFsRTtBQUNELFdBSEQsTUFJSztBQUNILG9CQUFRLFNBQVMsSUFBVCxDQUFjLFNBQVMsSUFBdkIsQ0FBUjtBQUNBLG1CQUFPLEtBQVAsQ0FBYSxnQ0FBYixFQUErQyxRQUEvQyxFQUF5RCxLQUF6RDtBQUNEO0FBQ0YsU0FuRk07O0FBcUZQLGFBQUs7QUFDSCxxQkFBVyxZQUFXO0FBQ3BCLGdCQUFJLEVBQUUsYUFBRixDQUFnQixVQUFoQixNQUFnQyxXQUFXLFNBQVgsSUFBd0IsV0FBVyxTQUFuRSxDQUFKLEVBQW9GO0FBQ2xGLGtCQUFHLFdBQVcsU0FBZCxFQUF5QjtBQUN2QiwyQkFBVyxNQUFYLEdBQW9CLFdBQVcsU0FBL0I7QUFDQSx1QkFBTyxXQUFXLFNBQWxCO0FBQ0EsdUJBQU8sS0FBUCxDQUFhLE1BQU0sVUFBbkIsRUFBK0IsV0FBVyxNQUExQztBQUNEO0FBQ0Qsa0JBQUcsV0FBVyxTQUFkLEVBQXlCO0FBQ3ZCLDJCQUFXLFdBQVgsR0FBeUIsV0FBVyxTQUFwQztBQUNBLHVCQUFPLFdBQVcsU0FBbEI7QUFDQSx1QkFBTyxLQUFQLENBQWEsTUFBTSxVQUFuQixFQUErQixXQUFXLFdBQTFDO0FBQ0Q7QUFDRCx5QkFBVyxFQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQixFQUFFLEVBQUYsQ0FBSyxHQUFMLENBQVMsUUFBNUIsRUFBc0MsVUFBdEMsQ0FBWDtBQUNEO0FBQ0Y7QUFmRSxTQXJGRTs7QUF1R1AsMkJBQW1CLFlBQVc7QUFDNUIsaUJBQU8sS0FBUCxDQUFhLHlCQUFiO0FBQ0EsY0FBSSxFQUFFLE9BQUYsS0FBYyxTQUFsQixFQUE4QjtBQUM1QixtQkFBTyxLQUFQLENBQWEsTUFBTSxLQUFuQjtBQUNBLG1CQUFPLEtBQVA7QUFDRCxXQUhELE1BSUs7QUFDSCxnQkFBRyxTQUFTLFdBQVQsSUFBd0IsT0FBM0IsRUFBb0M7QUFDbEMscUJBQU8sS0FBUCxDQUFhLDZCQUFiO0FBQ0Esa0JBQUcsU0FBUyxJQUFULEtBQWtCLEtBQXJCLEVBQTRCO0FBQzFCLGtCQUFFLE9BQUYsQ0FDRyxPQURILENBQ1csSUFEWCxFQUVHLEtBRkgsQ0FFUyxTQUFTLElBRmxCO0FBSUQsZUFMRCxNQU1LO0FBQ0gsdUJBQU8sS0FBUCxDQUFhLE1BQU0sSUFBbkI7QUFDQSx1QkFBTyxLQUFQO0FBQ0Q7QUFDRjtBQUNELGNBQUUsT0FBRixDQUNHLElBREgsQ0FDUSxRQURSLEVBQ2tCLE9BQU8sS0FBUCxDQUFhLE9BQWIsQ0FBcUIsTUFEdkM7QUFHRDtBQUNGLFNBL0hNOztBQWlJUCxlQUFPO0FBQ0wsaUJBQU8sVUFBUyxLQUFULEVBQWdCO0FBQ3JCLGdCQUNFLFVBQVUsRUFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLFNBQVMsR0FBdEIsQ0FEWjtBQUdBLGdCQUFHLFlBQVksU0FBZixFQUEwQjtBQUN4QixrQkFBRyxTQUFTLE9BQVosRUFBcUI7QUFDbkIsdUJBQU8sT0FBUCxDQUFlLHFCQUFmLEVBQXNDLEtBQXRDO0FBQ0Esa0JBQUUsT0FBRixDQUFVLEtBQVYsQ0FBZ0IsT0FBaEI7QUFDRCxlQUhELE1BSUs7QUFDSCx1QkFBTyxPQUFQLENBQWUsY0FBZixFQUErQixLQUEvQjtBQUNBLHVCQUFPLFNBQVAsQ0FBaUIsT0FBakI7QUFDRDtBQUNELG9CQUFNLGNBQU47QUFDRCxhQVZELE1BV0s7QUFDSCxxQkFBTyxLQUFQLENBQWEsa0JBQWI7QUFDRDtBQUNGLFdBbkJJO0FBb0JMLG1CQUFTO0FBQ1Asb0JBQVEsVUFBUyxLQUFULEVBQWdCO0FBQ3RCLGtCQUNFLFVBQVksTUFBTSxTQUFOLENBQWdCLElBQWhCLENBQXFCLEdBQXJCLEtBQTZCLE9BQU8sR0FBUCxDQUFXLFdBQVgsRUFEM0M7QUFBQSxrQkFFRSxZQUFZLFNBQVMsU0FBVCxDQUFtQixjQUFuQixDQUFrQyxPQUFsQyxLQUE4QyxLQUY1RDtBQUlBLHFCQUFPLFdBQVAsQ0FBbUIsT0FBbkI7QUFDQSxxQkFBTyxLQUFQLENBQWEsc0JBQWIsRUFBcUMsT0FBckMsRUFBOEMsS0FBOUM7QUFDQSw2QkFBZSxLQUFmO0FBQ0Esa0JBQUcsWUFBWSxTQUFmLEVBQTBCO0FBQ3hCLHVCQUFPLFNBQVAsQ0FBaUIsT0FBakI7QUFDRDtBQUNELGtCQUFHLFNBQUgsRUFBYztBQUNaLGtCQUFFLE9BQUYsQ0FBVSxLQUFWLENBQWdCLFNBQWhCO0FBQ0Q7QUFDRjtBQWZNO0FBcEJKLFNBaklBOztBQXdLUCxpQkFBUyxZQUFXO0FBQ2xCLGNBQUcsYUFBSCxFQUFrQjtBQUNoQixtQkFBTyxLQUFQLENBQWEsZ0JBQWIsRUFBK0IsYUFBL0I7QUFDQSxtQkFBTyxTQUFQLENBQWlCLGFBQWpCO0FBQ0Q7QUFDRixTQTdLTTs7QUErS1AsZUFBTzs7QUFFTCxnQkFBTSxVQUFTLFFBQVQsRUFBbUI7QUFDdkIsbUJBQVEsYUFBYSxTQUFkLEdBQ0gsTUFBTSxRQUFOLENBREcsR0FFSCxLQUZKO0FBSUQsV0FQSTtBQVFMLGVBQUssVUFBUyxRQUFULEVBQW1CLE9BQW5CLEVBQTRCO0FBQy9CLHVCQUFXLFlBQVksYUFBdkI7QUFDQSxtQkFBTyxLQUFQLENBQWEsMkJBQWIsRUFBMEMsUUFBMUM7QUFDQSxrQkFBTSxRQUFOLElBQWtCLE9BQWxCO0FBQ0QsV0FaSTtBQWFMLGtCQUFRLFVBQVMsUUFBVCxFQUFtQjtBQUN6Qix1QkFBVyxZQUFZLGFBQXZCO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLDZCQUFiLEVBQTRDLFFBQTVDO0FBQ0EsbUJBQU8sTUFBTSxRQUFOLENBQVA7QUFDRDtBQWpCSSxTQS9LQTs7QUFtTVAsYUFBSztBQUNILGdCQUFNLFlBQVc7QUFDZixnQkFDRSxNQUFPLE9BQU8sU0FBUyxJQUFoQixJQUF3QixRQUF6QixHQUNGLFNBQVMsSUFBVCxDQUFjLE9BQWQsQ0FBc0IsS0FBdEIsRUFBNkIsRUFBN0IsSUFBbUMsU0FEakMsR0FFRixTQUhOO0FBS0EsbUJBQU8sT0FBUCxDQUFlLGdEQUFmLEVBQWlFLEdBQWpFO0FBQ0EsZ0JBQUcsRUFBRSxhQUFGLENBQWdCLFNBQVMsV0FBekIsQ0FBSCxFQUEwQztBQUN4Qyx1QkFBUyxXQUFULENBQXFCLEdBQXJCLEdBQTJCLEdBQTNCO0FBQ0QsYUFGRCxNQUdLO0FBQ0gsdUJBQVMsV0FBVCxHQUF1QjtBQUNyQixxQkFBSztBQURnQixlQUF2QjtBQUdEO0FBQ0YsV0FoQkU7QUFpQkgsbUJBQVMsVUFBUyxPQUFULEVBQWtCO0FBQ3pCLGdCQUNFLE9BQVksT0FBTyxHQUFQLENBQVcsVUFBWCxDQUFzQixPQUF0QixDQURkO0FBQUEsZ0JBRUUsWUFBWSxLQUFLLFFBQUwsQ0FBYyxVQUFVLE9BQXhCLENBRmQ7QUFJQSxnQkFBRyxDQUFDLFNBQUosRUFBZTtBQUNiLHFCQUFPLE9BQVAsQ0FBZSwyQkFBZixFQUE0QyxJQUE1QztBQUNBLG1CQUNHLFFBREgsQ0FDWSxVQUFVLE9BRHRCLEVBRUcsUUFGSCxDQUVZLEtBRlosRUFHSyxXQUhMLENBR2lCLFVBQVUsTUFBVixHQUFtQixHQUFuQixHQUF5QixVQUFVLE9BSHBEO0FBS0Esa0JBQUcsS0FBSyxNQUFMLEdBQWMsQ0FBakIsRUFBb0I7QUFDbEIseUJBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixLQUFLLENBQUwsQ0FBeEIsRUFBaUMsT0FBakM7QUFDRDtBQUNGO0FBQ0YsV0FqQ0U7QUFrQ0gsaUJBQU8sVUFBUyxLQUFULEVBQWdCO0FBQ3JCLGNBQUUsT0FBRixDQUFVLEtBQVYsQ0FBZ0IsS0FBaEI7QUFDRDtBQXBDRSxTQW5NRTs7QUEwT1AsbUJBQVcsVUFBUyxPQUFULEVBQWtCO0FBQzNCLGNBQ0UscUJBQXNCLE9BQU8sT0FBUCxJQUFrQixPQUFPLE9BQVAsQ0FBZSxTQUR6RDtBQUFBLGNBRUUsbUJBQXNCLHNCQUFzQixTQUFTLGVBQS9CLElBQWtELFNBRjFFO0FBQUEsY0FHRSxnQkFBc0IsU0FBUyxJQUFULElBQWlCLEVBQUUsYUFBRixDQUFnQixTQUFTLFdBQXpCLENBSHpDO0FBQUE7O0FBS0Usc0JBQWEsaUJBQWlCLENBQUMsZ0JBQW5CLEdBQ1IsT0FBTyxTQUFQLENBQWlCLFdBQWpCLENBQTZCLE9BQTdCLENBRFEsR0FFUixPQUFPLEdBQVAsQ0FBVyxnQkFBWCxDQUE0QixPQUE1QixDQVBOO0FBU0Esb0JBQVUsT0FBTyxTQUFQLENBQWlCLFdBQWpCLENBQTZCLFNBQTdCLENBQVY7QUFDQSxZQUFFLElBQUYsQ0FBTyxTQUFQLEVBQWtCLFVBQVMsS0FBVCxFQUFnQixHQUFoQixFQUFxQjtBQUNyQyxnQkFDRSxtQkFBcUIsVUFBVSxLQUFWLENBQWdCLENBQWhCLEVBQW1CLFFBQVEsQ0FBM0IsQ0FEdkI7QUFBQSxnQkFFRSxjQUFxQixPQUFPLFNBQVAsQ0FBaUIsV0FBakIsQ0FBNkIsZ0JBQTdCLENBRnZCO0FBQUEsZ0JBSUUsUUFBcUIsT0FBTyxFQUFQLENBQVUsR0FBVixDQUFjLFdBQWQsQ0FKdkI7QUFBQSxnQkFLRSxjQUFzQixRQUFRLENBQVIsSUFBYSxVQUFVLE1BTC9DO0FBQUEsZ0JBT0UsT0FBcUIsT0FBTyxHQUFQLENBQVcsVUFBWCxDQUFzQixXQUF0QixDQVB2QjtBQUFBLGdCQVFFLE9BUkY7QUFBQSxnQkFTRSxhQVRGO0FBQUEsZ0JBVUUsUUFWRjtBQUFBLGdCQVdFLFNBWEY7QUFhQSxtQkFBTyxPQUFQLENBQWUsaUJBQWYsRUFBa0MsR0FBbEM7QUFDQSxnQkFBRyxLQUFILEVBQVU7QUFDUixxQkFBTyxPQUFQLENBQWUsZUFBZixFQUFnQyxHQUFoQzs7QUFFQSw4QkFBaUIsV0FBakI7QUFDQSwrQkFBaUIsT0FBTyxTQUFQLENBQWlCLFdBQWpCLENBQTZCLFNBQTdCLEVBQXdDLGdCQUF4QyxDQUFqQjs7QUFFQSxrQkFBRyxXQUFILEVBQWdCO0FBQ2QsNEJBQVksSUFBWjtBQUNELGVBRkQsTUFHSztBQUNILGdDQUFnQixVQUFVLEtBQVYsQ0FBZ0IsQ0FBaEIsRUFBbUIsUUFBUSxDQUEzQixDQUFoQjtBQUNBLDJCQUFnQixPQUFPLFNBQVAsQ0FBaUIsV0FBakIsQ0FBNkIsYUFBN0IsQ0FBaEI7QUFDQSw0QkFBa0IsQ0FBQyxPQUFPLEVBQVAsQ0FBVSxHQUFWLENBQWMsUUFBZCxDQUFuQjtBQUNBLG9CQUFHLFNBQUgsRUFBYztBQUNaLHlCQUFPLE9BQVAsQ0FBZSxzQkFBZixFQUF1QyxhQUF2QztBQUNEO0FBQ0Y7QUFDRCxrQkFBRyxhQUFhLGFBQWhCLEVBQStCO0FBQzdCLG9CQUFHLENBQUMsZ0JBQUosRUFBc0I7QUFDcEIseUJBQU8sUUFBUCxDQUFnQixVQUFoQixDQUEyQixXQUEzQjtBQUNBLHlCQUFPLEtBQVAsQ0FBYSxPQUFiLENBQXFCLFdBQXJCLEVBQWtDLE9BQWxDO0FBQ0QsaUJBSEQsTUFJSztBQUNILHlCQUFPLEtBQVAsQ0FBYSwyQ0FBYixFQUEwRCxXQUExRDtBQUNBLDhCQUFZLEtBQVo7QUFDQSx5QkFBTyxLQUFQLENBQWEsR0FBYixDQUFpQixPQUFqQixFQUEwQixLQUFLLElBQUwsRUFBMUI7QUFDQSx5QkFBTyxRQUFQLENBQWdCLEdBQWhCLENBQW9CLFdBQXBCO0FBQ0EsMkJBQVMsV0FBVCxDQUFxQixJQUFyQixDQUEwQixLQUFLLENBQUwsQ0FBMUIsRUFBbUMsV0FBbkMsRUFBZ0QsY0FBaEQsRUFBZ0UsWUFBaEU7QUFDQSwyQkFBUyxNQUFULENBQWdCLElBQWhCLENBQXFCLEtBQUssQ0FBTCxDQUFyQixFQUE4QixXQUE5QixFQUEyQyxjQUEzQyxFQUEyRCxZQUEzRDtBQUNEO0FBQ0QsdUJBQU8sS0FBUDtBQUNELGVBZEQsTUFlSztBQUNILHVCQUFPLEtBQVAsQ0FBYSxrQkFBYixFQUFpQyxXQUFqQztBQUNBLHVCQUFPLFFBQVAsQ0FBZ0IsR0FBaEIsQ0FBb0IsV0FBcEI7QUFDQSxvQkFBSSxDQUFDLE9BQU8sS0FBUCxDQUFhLElBQWIsQ0FBa0IsV0FBbEIsQ0FBTCxFQUFzQztBQUNwQyx5QkFBTyxLQUFQLENBQWEsR0FBYixDQUFpQixXQUFqQixFQUE4QixJQUE5QjtBQUNBLHlCQUFPLEtBQVAsQ0FBYSx3Q0FBYjtBQUNBLDJCQUFTLFdBQVQsQ0FBcUIsSUFBckIsQ0FBMEIsS0FBSyxDQUFMLENBQTFCLEVBQW1DLFdBQW5DLEVBQWdELGNBQWhELEVBQWdFLFlBQWhFO0FBQ0Q7QUFDRCx5QkFBUyxNQUFULENBQWdCLElBQWhCLENBQXFCLEtBQUssQ0FBTCxDQUFyQixFQUE4QixXQUE5QixFQUEyQyxjQUEzQyxFQUEyRCxZQUEzRDtBQUNEO0FBRUYsYUEzQ0QsTUE0Q0ssSUFBRyxRQUFRLE1BQVIsQ0FBZSxHQUFmLEtBQXVCLENBQUMsQ0FBeEIsSUFBNkIsWUFBWSxFQUE1QyxFQUFnRDs7QUFFbkQsd0JBQWMsRUFBRSxNQUFNLE9BQU4sR0FBZ0IsWUFBaEIsR0FBK0IsT0FBL0IsR0FBeUMsSUFBM0MsQ0FBZDtBQUNBLDRCQUFjLFFBQVEsT0FBUixDQUFnQixZQUFoQixFQUE4QixJQUE5QixDQUFtQyxTQUFTLEdBQTVDLENBQWQ7QUFDQSxxQkFBYyxPQUFPLEdBQVAsQ0FBVyxVQUFYLENBQXNCLFdBQXRCLENBQWQ7O0FBRUEsa0JBQUcsV0FBVyxRQUFRLE1BQVIsR0FBaUIsQ0FBNUIsSUFBaUMsV0FBcEMsRUFBaUQ7QUFDL0MsdUJBQU8sS0FBUCxDQUFhLHNDQUFiLEVBQXFELElBQXJELEVBQTJELE9BQTNEO0FBQ0Esb0JBQUksQ0FBQyxLQUFLLFFBQUwsQ0FBYyxVQUFVLE1BQXhCLENBQUwsRUFBdUM7QUFDckMsNkJBQVcsWUFBVztBQUNwQiwyQkFBTyxRQUFQLENBQWdCLE9BQWhCO0FBQ0QsbUJBRkQsRUFFRyxDQUZIO0FBR0Q7QUFDRCx1QkFBTyxRQUFQLENBQWdCLEdBQWhCLENBQW9CLFdBQXBCO0FBQ0Esb0JBQUksQ0FBQyxPQUFPLEtBQVAsQ0FBYSxJQUFiLENBQWtCLFdBQWxCLENBQUwsRUFBc0M7QUFDcEMseUJBQU8sS0FBUCxDQUFhLEdBQWIsQ0FBaUIsV0FBakIsRUFBOEIsSUFBOUI7QUFDQSx5QkFBTyxLQUFQLENBQWEsd0NBQWI7QUFDQSwyQkFBUyxXQUFULENBQXFCLElBQXJCLENBQTBCLEtBQUssQ0FBTCxDQUExQixFQUFtQyxXQUFuQyxFQUFnRCxjQUFoRCxFQUFnRSxZQUFoRTtBQUNEO0FBQ0QseUJBQVMsTUFBVCxDQUFnQixJQUFoQixDQUFxQixLQUFLLENBQUwsQ0FBckIsRUFBOEIsV0FBOUIsRUFBMkMsY0FBM0MsRUFBMkQsWUFBM0Q7QUFDQSx1QkFBTyxLQUFQO0FBQ0Q7QUFDRixhQXRCSSxNQXVCQTtBQUNILHFCQUFPLEtBQVAsQ0FBYSxNQUFNLFVBQW5CLEVBQStCLE9BQS9CLEVBQXdDLFFBQXhDLEVBQWtELFdBQWxEO0FBQ0EscUJBQU8sS0FBUDtBQUNEO0FBQ0YsV0F0RkQ7QUF1RkQsU0E1VU07O0FBOFVQLGtCQUFVLFVBQVMsUUFBVCxFQUFtQjtBQUMzQixjQUNFLGVBQWdCLFlBQVksU0FBUyxNQUFULEdBQWtCLENBQS9CLEdBQ1gsU0FBUyxNQUFULEdBQWtCLEdBRFAsR0FFWCxLQUhOO0FBS0EsY0FBRyxpQkFBaUIsS0FBcEIsRUFBMkI7QUFDekIsbUJBQU8sS0FBUCxDQUFhLG1EQUFiLEVBQWtFLFlBQWxFLEVBQWdGLFFBQWhGO0FBQ0EsY0FBRSxRQUFGLEVBQVksU0FBWixDQUFzQixZQUF0QjtBQUNEO0FBQ0YsU0F4Vk07O0FBMFZQLGdCQUFRO0FBQ04sbUJBQVMsVUFBUyxPQUFULEVBQWtCLElBQWxCLEVBQXdCLGVBQXhCLEVBQXlDO0FBQ2hELGdCQUNFLE9BQU8sT0FBTyxHQUFQLENBQVcsVUFBWCxDQUFzQixPQUF0QixDQURUO0FBQUEsZ0JBRUUsTUFBTyxLQUFLLENBQUwsQ0FGVDtBQUlBLDhCQUFtQixvQkFBb0IsU0FBckIsR0FDZCxlQURjLEdBRWQsU0FBUyxlQUZiO0FBSUEsZ0JBQUcsZUFBSCxFQUFvQjtBQUNsQixxQkFBTyxLQUFQLENBQWEsNkNBQWIsRUFBNEQsT0FBNUQsRUFBcUUsSUFBckU7QUFDQSxtQkFBSyxJQUFMLENBQVUsSUFBVjtBQUNELGFBSEQsTUFJSztBQUNILHFCQUFPLEtBQVAsQ0FBYSxlQUFiLEVBQThCLE9BQTlCLEVBQXVDLElBQXZDO0FBQ0Esa0JBQUksU0FBSixHQUFnQixJQUFoQjtBQUNEO0FBQ0Y7QUFsQkssU0ExVkQ7O0FBK1dQLGVBQU87O0FBRUwsbUJBQVMsVUFBUyxPQUFULEVBQWtCLFdBQWxCLEVBQStCO0FBQ3RDLGdCQUNFLE9BQWMsT0FBTyxHQUFQLENBQVcsVUFBWCxDQUFzQixPQUF0QixDQURoQjtBQUFBLGdCQUVFLGNBQWM7QUFDWix3QkFBbUIsTUFEUDtBQUVaLGdDQUFtQixLQUZQO0FBR1osa0JBQW1CLEtBSFA7QUFJWixxQkFBbUIsU0FBUyxhQUpoQjtBQUtaLHVCQUFtQjtBQUNqQiw0QkFBWTtBQURLLGVBTFA7QUFRWix5QkFBWSxVQUFTLFFBQVQsRUFBbUI7QUFDN0Isb0JBQUcsU0FBUyxTQUFULElBQXNCLFVBQXpCLEVBQXFDO0FBQ25DLHlCQUFPLEtBQVAsQ0FBYSxHQUFiLENBQWlCLFdBQWpCLEVBQThCLFFBQTlCO0FBQ0Q7QUFDRCx1QkFBTyxNQUFQLENBQWMsT0FBZCxDQUFzQixPQUF0QixFQUErQixRQUEvQjtBQUNBLG9CQUFHLFdBQVcsYUFBZCxFQUE2QjtBQUMzQix5QkFBTyxLQUFQLENBQWEsZ0JBQWIsRUFBK0IsT0FBL0I7QUFDQSx5QkFBTyxRQUFQLENBQWdCLEdBQWhCLENBQW9CLE9BQXBCO0FBQ0QsaUJBSEQsTUFJSztBQUNILHlCQUFPLEtBQVAsQ0FBYSw4QkFBYixFQUE2QyxPQUE3QztBQUNEO0FBQ0QseUJBQVMsV0FBVCxDQUFxQixJQUFyQixDQUEwQixLQUFLLENBQUwsQ0FBMUIsRUFBbUMsT0FBbkMsRUFBNEMsY0FBNUMsRUFBNEQsWUFBNUQ7QUFDQSx5QkFBUyxNQUFULENBQWdCLElBQWhCLENBQXFCLEtBQUssQ0FBTCxDQUFyQixFQUE4QixPQUE5QixFQUF1QyxjQUF2QyxFQUF1RCxZQUF2RDtBQUNBLG9CQUFHLFNBQVMsU0FBVCxJQUFzQixVQUF6QixFQUFxQztBQUNuQyx5QkFBTyxLQUFQLENBQWEsR0FBYixDQUFpQixXQUFqQixFQUE4QixLQUFLLElBQUwsRUFBOUI7QUFDRDtBQUNGLGVBekJXO0FBMEJaLHVCQUFTO0FBQ1AscUJBQUs7QUFERTtBQTFCRyxhQUZoQjtBQUFBLGdCQWdDRSxVQUFrQixLQUFLLEdBQUwsQ0FBUyxhQUFULEtBQTJCLEtBaEMvQztBQUFBLGdCQWlDRSxrQkFBb0IsV0FBVyxRQUFRLEtBQVIsT0FBb0IsU0FqQ3JEO0FBQUEsZ0JBa0NFLGVBbENGO0FBQUEsZ0JBbUNFLGFBbkNGOztBQXNDQSwwQkFBZ0IsZUFBZSxPQUEvQjtBQUNBLDRCQUFnQixPQUFPLEtBQVAsQ0FBYSxJQUFiLENBQWtCLFdBQWxCLENBQWhCOztBQUdBLGdCQUFHLFNBQVMsS0FBVCxJQUFrQixhQUFyQixFQUFvQztBQUNsQyxxQkFBTyxRQUFQLENBQWdCLEdBQWhCLENBQW9CLE9BQXBCO0FBQ0EscUJBQU8sS0FBUCxDQUFhLHVCQUFiLEVBQXNDLFdBQXRDO0FBQ0Esa0JBQUcsU0FBUyxlQUFULElBQTRCLE1BQS9CLEVBQXVDO0FBQ3JDLHVCQUFPLE1BQVAsQ0FBYyxPQUFkLENBQXNCLE9BQXRCLEVBQStCLGFBQS9CLEVBQThDLEtBQTlDO0FBQ0QsZUFGRCxNQUdLO0FBQ0gsdUJBQU8sTUFBUCxDQUFjLE9BQWQsQ0FBc0IsT0FBdEIsRUFBK0IsYUFBL0I7QUFDRDtBQUNELHVCQUFTLE1BQVQsQ0FBZ0IsSUFBaEIsQ0FBcUIsS0FBSyxDQUFMLENBQXJCLEVBQThCLE9BQTlCLEVBQXVDLGNBQXZDLEVBQXVELFlBQXZEO0FBQ0QsYUFWRCxNQVdLLElBQUcsZUFBSCxFQUFvQjtBQUN2QixxQkFBTyxHQUFQLENBQVcsT0FBWCxDQUFtQixPQUFuQjtBQUNBLHFCQUFPLEtBQVAsQ0FBYSw0QkFBYixFQUEyQyxXQUEzQztBQUNELGFBSEksTUFJQSxJQUFHLEVBQUUsR0FBRixLQUFVLFNBQWIsRUFBd0I7QUFDM0IsZ0NBQWtCLEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLFNBQVMsV0FBNUIsRUFBeUMsV0FBekMsQ0FBbEI7QUFDQSxxQkFBTyxLQUFQLENBQWEsMkJBQWIsRUFBMEMsV0FBMUMsRUFBdUQsZUFBdkQ7QUFDQSxxQkFBTyxHQUFQLENBQVcsT0FBWCxDQUFtQixPQUFuQjtBQUNBLG1CQUFLLEdBQUwsQ0FBUyxlQUFUO0FBQ0QsYUFMSSxNQU1BO0FBQ0gscUJBQU8sS0FBUCxDQUFhLE1BQU0sR0FBbkI7QUFDRDtBQUNGO0FBckVJLFNBL1dBOztBQXViUCxrQkFBVTtBQUNSLGVBQUssVUFBUyxPQUFULEVBQWtCO0FBQ3JCLG1CQUFPLFFBQVAsQ0FBZ0IsR0FBaEIsQ0FBb0IsT0FBcEI7QUFDQSxtQkFBTyxRQUFQLENBQWdCLFVBQWhCLENBQTJCLE9BQTNCO0FBQ0QsV0FKTztBQUtSLGVBQUssVUFBUyxPQUFULEVBQWtCO0FBQ3JCLGdCQUNFLE9BQWdCLE9BQU8sR0FBUCxDQUFXLFVBQVgsQ0FBc0IsT0FBdEIsQ0FEbEI7QUFBQSxnQkFFRSxnQkFBaUIsU0FBUyxVQUFULElBQXVCLFVBQXhCLEdBQ1osS0FBSyxRQUFMLENBQWMsS0FBZCxDQURZLEdBRVosTUFBTSxHQUFOLENBQVUsSUFBVixDQUpOO0FBQUEsZ0JBS0UsV0FBZ0IsS0FBSyxRQUFMLENBQWMsVUFBVSxNQUF4QixDQUxsQjtBQU9BLG1CQUFPLE9BQVAsQ0FBZSx5QkFBZixFQUEwQyxJQUExQztBQUNBLGdCQUFHLENBQUMsUUFBSixFQUFjO0FBQ1osbUJBQ0csUUFESCxDQUNZLFVBQVUsTUFEdEI7QUFHQSw0QkFDRyxXQURILENBQ2UsVUFBVSxNQUFWLEdBQW1CLEdBQW5CLEdBQXlCLFVBQVUsT0FEbEQ7QUFHQSxrQkFBRyxLQUFLLE1BQUwsR0FBYyxDQUFqQixFQUFvQjtBQUNsQix5QkFBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLEtBQUssQ0FBTCxDQUF4QixFQUFpQyxPQUFqQztBQUNEO0FBQ0Y7QUFDRixXQXpCTztBQTBCUixzQkFBWSxVQUFTLE9BQVQsRUFBa0I7QUFDNUIsZ0JBQ0UsY0FBc0IsT0FBTyxHQUFQLENBQVcsVUFBWCxDQUFzQixPQUF0QixDQUR4QjtBQUFBLGdCQUVFLHNCQUF1QixTQUFTLFVBQVQsSUFBdUIsVUFBeEIsR0FDbEIsWUFBWSxRQUFaLENBQXFCLFdBQXJCLENBRGtCLEdBRWxCLFlBQVksR0FBWixDQUFnQixXQUFoQixDQUpOO0FBQUEsZ0JBS0UsV0FBYyxZQUFZLFFBQVosQ0FBcUIsVUFBVSxNQUEvQixDQUxoQjtBQU9BLG1CQUFPLE9BQVAsQ0FBZSwrQkFBZixFQUFnRCxXQUFoRCxFQUE2RCxPQUE3RDtBQUNBLGdCQUFHLENBQUMsUUFBSixFQUFjO0FBQ1osMEJBQ0csUUFESCxDQUNZLFVBQVUsTUFEdEI7QUFHQSxrQ0FDRyxXQURILENBQ2UsVUFBVSxNQUFWLEdBQW1CLEdBQW5CLEdBQXlCLFVBQVUsT0FEbEQ7QUFHRDtBQUNGO0FBM0NPLFNBdmJIOztBQXFlUCxvQkFBWTtBQUNWLGVBQUssWUFBVztBQUNkLG1CQUFPLFVBQVAsQ0FBa0IsVUFBbEI7QUFDQSxtQkFBTyxVQUFQLENBQWtCLElBQWxCO0FBQ0QsV0FKUztBQUtWLHNCQUFZLFlBQVc7QUFDckIsd0JBQ0csV0FESCxDQUNlLFVBQVUsTUFEekI7QUFHRCxXQVRTO0FBVVYsZ0JBQU0sWUFBVztBQUNmLGtCQUNHLFdBREgsQ0FDZSxVQUFVLE1BQVYsR0FBbUIsR0FBbkIsR0FBeUIsVUFBVSxPQURsRDtBQUdEO0FBZFMsU0FyZUw7O0FBc2ZQLFlBQUk7QUFDRixlQUFLLFVBQVMsT0FBVCxFQUFrQjtBQUNyQixtQkFBUSxZQUFZLFNBQWIsR0FDRCxPQUFPLEdBQVAsQ0FBVyxVQUFYLENBQXNCLE9BQXRCLEVBQStCLE1BQS9CLEdBQXdDLENBRHZDLEdBRUgsS0FGSjtBQUlEO0FBTkMsU0F0Zkc7O0FBK2ZQLGFBQUs7QUFDSCx1QkFBYSxZQUFXO0FBQ3RCLG1CQUFPLFlBQVksRUFBWixDQUFlLENBQWYsRUFBa0IsSUFBbEIsQ0FBdUIsU0FBUyxHQUFoQyxLQUF3QyxNQUFNLEVBQU4sQ0FBUyxDQUFULEVBQVksSUFBWixDQUFpQixTQUFTLEdBQTFCLENBQS9DO0FBQ0QsV0FIRTtBQUlILGdCQUFNLFlBQVc7QUFDZixtQkFBTyxFQUFFLE9BQUYsQ0FBVSxLQUFWLEVBQVA7QUFDRCxXQU5FOztBQVFILDRCQUFrQixVQUFTLE9BQVQsRUFBa0I7QUFDbEMsbUJBQU8sT0FBTyxTQUFQLENBQWlCLFdBQWpCLENBQThCLE9BQU8sR0FBUCxDQUFXLFdBQVgsQ0FBdUIsT0FBdkIsQ0FBOUIsQ0FBUDtBQUNELFdBVkU7QUFXSCx1QkFBYSxVQUFTLE9BQVQsRUFBa0I7QUFDN0IsZ0JBQ0UsY0FBYyxZQUFZLE1BQVosQ0FBbUIsV0FBVyxTQUFTLEdBQXBCLEdBQTBCLEtBQTFCLEdBQWtDLE9BQWxDLEdBQTRDLEtBQS9ELEVBQXNFLEVBQXRFLENBQXlFLENBQXpFLENBRGhCO0FBQUEsZ0JBRUUsYUFBYyxZQUFZLElBQVosQ0FBaUIsU0FBUyxHQUExQixLQUFrQyxLQUZsRDtBQUlBLGdCQUFJLFVBQUosRUFBaUI7QUFDZixxQkFBTyxLQUFQLENBQWEsbUJBQWIsRUFBa0MsVUFBbEM7QUFDQSxrQkFBRyxpQkFBaUIsU0FBUyxRQUE3QixFQUF1QztBQUNyQztBQUNBLHVCQUFPLE9BQU8sR0FBUCxDQUFXLFdBQVgsQ0FBdUIsVUFBdkIsQ0FBUDtBQUNEO0FBQ0QscUJBQU8sS0FBUCxDQUFhLE1BQU0sU0FBbkI7QUFDRCxhQVBELE1BUUs7QUFDSCxxQkFBTyxLQUFQLENBQWEsMkJBQWIsRUFBMEMsT0FBMUMsRUFBbUQsS0FBbkQ7QUFDRDtBQUNELDZCQUFpQixDQUFqQjtBQUNBLG1CQUFPLE9BQVA7QUFDRCxXQTdCRTtBQThCSCxzQkFBWSxVQUFTLE9BQVQsRUFBa0I7QUFDNUIsc0JBQVUsV0FBVyxhQUFyQjtBQUNBLG1CQUFPLFlBQVksTUFBWixDQUFtQixXQUFXLFNBQVMsR0FBcEIsR0FBMEIsSUFBMUIsR0FBaUMsT0FBakMsR0FBMkMsSUFBOUQsQ0FBUDtBQUNELFdBakNFO0FBa0NILHNCQUFZLFVBQVMsT0FBVCxFQUFrQjtBQUM1QixnQkFDRSxZQURGLEVBRUUsY0FGRixFQUdFLFlBSEYsRUFJRSxPQUpGO0FBTUEsc0JBQWlCLFdBQVcsYUFBNUI7QUFDQSwyQkFBaUIsT0FBTyxTQUFQLENBQWlCLFdBQWpCLENBQTZCLE9BQTdCLENBQWpCO0FBQ0Esc0JBQWlCLE9BQU8sU0FBUCxDQUFpQixJQUFqQixDQUFzQixZQUF0QixDQUFqQjtBQUNBLDJCQUFpQixNQUFNLE1BQU4sQ0FBYSxXQUFXLFNBQVMsR0FBcEIsR0FBMEIsSUFBMUIsR0FBaUMsT0FBakMsR0FBMkMsSUFBeEQsQ0FBakI7QUFDQSw2QkFBaUIsTUFBTSxNQUFOLENBQWEsV0FBVyxTQUFTLEdBQXBCLEdBQTBCLElBQTFCLEdBQWlDLE9BQWpDLEdBQTJDLElBQXhELENBQWpCO0FBQ0EsbUJBQVEsYUFBYSxNQUFiLEdBQXNCLENBQXZCLEdBQ0gsWUFERyxHQUVILGNBRko7QUFJRCxXQWxERTtBQW1ESCxlQUFLLFlBQVc7QUFDZCxtQkFBTyxhQUFQO0FBQ0Q7QUFyREUsU0EvZkU7O0FBdWpCUCxtQkFBVztBQUNULHVCQUFhLFVBQVMsU0FBVCxFQUFvQixXQUFwQixFQUFpQztBQUM1QyxtQkFBTyxFQUFFLElBQUYsQ0FBTyxTQUFQLEVBQWtCLFVBQVMsU0FBVCxFQUFvQjtBQUMzQyxxQkFBUyxFQUFFLE9BQUYsQ0FBVSxTQUFWLEVBQXFCLFdBQXJCLEtBQXFDLENBQUMsQ0FBL0M7QUFDRCxhQUZNLENBQVA7QUFHRCxXQUxRO0FBTVQsZ0JBQU0sVUFBUyxLQUFULEVBQWdCO0FBQ3BCLG1CQUFPLEVBQUUsT0FBRixDQUFVLEtBQVYsSUFDSCxNQUFPLE1BQU0sTUFBTixHQUFlLENBQXRCLENBREcsR0FFSCxLQUZKO0FBSUQsV0FYUTtBQVlULHVCQUFhLFVBQVMsUUFBVCxFQUFtQjtBQUM5QixnQkFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLHlCQUFXLGFBQVg7QUFDRDtBQUNELG1CQUFPLE9BQU8sUUFBUCxJQUFtQixRQUFuQixHQUNILFNBQVMsS0FBVCxDQUFlLEdBQWYsQ0FERyxHQUVILENBQUMsUUFBRCxDQUZKO0FBSUQsV0FwQlE7QUFxQlQsdUJBQWEsVUFBUyxTQUFULEVBQW9CO0FBQy9CLG1CQUFPLEVBQUUsT0FBRixDQUFVLFNBQVYsSUFDSCxVQUFVLElBQVYsQ0FBZSxHQUFmLENBREcsR0FFSCxLQUZKO0FBSUQ7QUExQlEsU0F2akJKOztBQW9sQlAsaUJBQVMsVUFBUyxJQUFULEVBQWUsS0FBZixFQUFzQjtBQUM3QixpQkFBTyxLQUFQLENBQWEsa0JBQWIsRUFBaUMsSUFBakMsRUFBdUMsS0FBdkM7QUFDQSxjQUFJLEVBQUUsYUFBRixDQUFnQixJQUFoQixDQUFKLEVBQTRCO0FBQzFCLGNBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxRQUFmLEVBQXlCLElBQXpCO0FBQ0QsV0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLGdCQUFHLEVBQUUsYUFBRixDQUFnQixTQUFTLElBQVQsQ0FBaEIsQ0FBSCxFQUFvQztBQUNsQyxnQkFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLFNBQVMsSUFBVCxDQUFmLEVBQStCLEtBQS9CO0FBQ0QsYUFGRCxNQUdLO0FBQ0gsdUJBQVMsSUFBVCxJQUFpQixLQUFqQjtBQUNEO0FBQ0YsV0FQSSxNQVFBO0FBQ0gsbUJBQU8sU0FBUyxJQUFULENBQVA7QUFDRDtBQUNGLFNBcG1CTTtBQXFtQlAsa0JBQVUsVUFBUyxJQUFULEVBQWUsS0FBZixFQUFzQjtBQUM5QixjQUFJLEVBQUUsYUFBRixDQUFnQixJQUFoQixDQUFKLEVBQTRCO0FBQzFCLGNBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxNQUFmLEVBQXVCLElBQXZCO0FBQ0QsV0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLG1CQUFPLElBQVAsSUFBZSxLQUFmO0FBQ0QsV0FGSSxNQUdBO0FBQ0gsbUJBQU8sT0FBTyxJQUFQLENBQVA7QUFDRDtBQUNGLFNBL21CTTtBQWduQlAsZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQVYsSUFBb0IsU0FBUyxLQUFoQyxFQUF1QztBQUNyQyxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLEtBQVAsR0FBZSxTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxJQUFyQyxFQUEyQyxPQUEzQyxFQUFvRCxTQUFTLElBQVQsR0FBZ0IsR0FBcEUsQ0FBZjtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLE9BQW5CLEVBQTRCLFNBQTVCO0FBQ0Q7QUFDRjtBQUNGLFNBMW5CTTtBQTJuQlAsaUJBQVMsWUFBVztBQUNsQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsT0FBN0IsSUFBd0MsU0FBUyxLQUFwRCxFQUEyRDtBQUN6RCxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLE9BQVAsR0FBaUIsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWpCO0FBQ0EscUJBQU8sT0FBUCxDQUFlLEtBQWYsQ0FBcUIsT0FBckIsRUFBOEIsU0FBOUI7QUFDRDtBQUNGO0FBQ0YsU0Fyb0JNO0FBc29CUCxlQUFPLFlBQVc7QUFDaEIsY0FBRyxDQUFDLFNBQVMsTUFBYixFQUFxQjtBQUNuQixtQkFBTyxLQUFQLEdBQWUsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsS0FBckMsRUFBNEMsT0FBNUMsRUFBcUQsU0FBUyxJQUFULEdBQWdCLEdBQXJFLENBQWY7QUFDQSxtQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNEO0FBQ0YsU0Ezb0JNO0FBNG9CUCxxQkFBYTtBQUNYLGVBQUssVUFBUyxPQUFULEVBQWtCO0FBQ3JCLGdCQUNFLFdBREYsRUFFRSxhQUZGLEVBR0UsWUFIRjtBQUtBLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2Qiw0QkFBZ0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUFoQjtBQUNBLDZCQUFnQixRQUFRLFdBQXhCO0FBQ0EsOEJBQWdCLGNBQWMsWUFBOUI7QUFDQSxxQkFBZ0IsV0FBaEI7QUFDQSwwQkFBWSxJQUFaLENBQWlCO0FBQ2Ysd0JBQW1CLFFBQVEsQ0FBUixDQURKO0FBRWYsNkJBQW1CLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxPQUFkLEVBQXVCLENBQXZCLEtBQTZCLEVBRmpDO0FBR2YsMkJBQW1CLE9BSEo7QUFJZixrQ0FBbUI7QUFKSixlQUFqQjtBQU1EO0FBQ0QseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsbUJBQU8sV0FBUCxDQUFtQixLQUFuQixHQUEyQixXQUFXLE9BQU8sV0FBUCxDQUFtQixPQUE5QixFQUF1QyxHQUF2QyxDQUEzQjtBQUNELFdBckJVO0FBc0JYLG1CQUFTLFlBQVc7QUFDbEIsZ0JBQ0UsUUFBUSxTQUFTLElBQVQsR0FBZ0IsR0FEMUI7QUFBQSxnQkFFRSxZQUFZLENBRmQ7QUFJQSxtQkFBTyxLQUFQO0FBQ0EseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsY0FBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMkJBQWEsS0FBSyxnQkFBTCxDQUFiO0FBQ0QsYUFGRDtBQUdBLHFCQUFTLE1BQU0sU0FBTixHQUFrQixJQUEzQjtBQUNBLGdCQUFHLGNBQUgsRUFBbUI7QUFDakIsdUJBQVMsUUFBUSxjQUFSLEdBQXlCLElBQWxDO0FBQ0Q7QUFDRCxnQkFBSSxDQUFDLFFBQVEsS0FBUixLQUFrQixTQUFsQixJQUErQixRQUFRLEtBQVIsS0FBa0IsU0FBbEQsS0FBZ0UsWUFBWSxNQUFaLEdBQXFCLENBQXpGLEVBQTRGO0FBQzFGLHNCQUFRLGNBQVIsQ0FBdUIsS0FBdkI7QUFDQSxrQkFBRyxRQUFRLEtBQVgsRUFBa0I7QUFDaEIsd0JBQVEsS0FBUixDQUFjLFdBQWQ7QUFDRCxlQUZELE1BR0s7QUFDSCxrQkFBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMEJBQVEsR0FBUixDQUFZLEtBQUssTUFBTCxJQUFlLElBQWYsR0FBc0IsS0FBSyxnQkFBTCxDQUF0QixHQUE2QyxJQUF6RDtBQUNELGlCQUZEO0FBR0Q7QUFDRCxzQkFBUSxRQUFSO0FBQ0Q7QUFDRCwwQkFBYyxFQUFkO0FBQ0Q7QUFqRFUsU0E1b0JOO0FBK3JCUCxnQkFBUSxVQUFTLEtBQVQsRUFBZ0IsZUFBaEIsRUFBaUMsT0FBakMsRUFBMEM7QUFDaEQsY0FDRSxTQUFTLFFBRFg7QUFBQSxjQUVFLFFBRkY7QUFBQSxjQUdFLEtBSEY7QUFBQSxjQUlFLFFBSkY7QUFNQSw0QkFBa0IsbUJBQW1CLGNBQXJDO0FBQ0Esb0JBQWtCLFdBQW1CLE9BQXJDO0FBQ0EsY0FBRyxPQUFPLEtBQVAsSUFBZ0IsUUFBaEIsSUFBNEIsV0FBVyxTQUExQyxFQUFxRDtBQUNuRCxvQkFBVyxNQUFNLEtBQU4sQ0FBWSxPQUFaLENBQVg7QUFDQSx1QkFBVyxNQUFNLE1BQU4sR0FBZSxDQUExQjtBQUNBLGNBQUUsSUFBRixDQUFPLEtBQVAsRUFBYyxVQUFTLEtBQVQsRUFBZ0IsS0FBaEIsRUFBdUI7QUFDbkMsa0JBQUksaUJBQWtCLFNBQVMsUUFBVixHQUNqQixRQUFRLE1BQU0sUUFBUSxDQUFkLEVBQWlCLE1BQWpCLENBQXdCLENBQXhCLEVBQTJCLFdBQTNCLEVBQVIsR0FBbUQsTUFBTSxRQUFRLENBQWQsRUFBaUIsS0FBakIsQ0FBdUIsQ0FBdkIsQ0FEbEMsR0FFakIsS0FGSjtBQUlBLGtCQUFJLEVBQUUsYUFBRixDQUFpQixPQUFPLGNBQVAsQ0FBakIsS0FBOEMsU0FBUyxRQUEzRCxFQUF1RTtBQUNyRSx5QkFBUyxPQUFPLGNBQVAsQ0FBVDtBQUNELGVBRkQsTUFHSyxJQUFJLE9BQU8sY0FBUCxNQUEyQixTQUEvQixFQUEyQztBQUM5Qyx3QkFBUSxPQUFPLGNBQVAsQ0FBUjtBQUNBLHVCQUFPLEtBQVA7QUFDRCxlQUhJLE1BSUEsSUFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxLQUFQLENBQWpCLEtBQXFDLFNBQVMsUUFBbEQsRUFBOEQ7QUFDakUseUJBQVMsT0FBTyxLQUFQLENBQVQ7QUFDRCxlQUZJLE1BR0EsSUFBSSxPQUFPLEtBQVAsTUFBa0IsU0FBdEIsRUFBa0M7QUFDckMsd0JBQVEsT0FBTyxLQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBO0FBQ0gsdUJBQU8sS0FBUCxDQUFhLE1BQU0sTUFBbkIsRUFBMkIsS0FBM0I7QUFDQSx1QkFBTyxLQUFQO0FBQ0Q7QUFDRixhQXZCRDtBQXdCRDtBQUNELGNBQUssRUFBRSxVQUFGLENBQWMsS0FBZCxDQUFMLEVBQTZCO0FBQzNCLHVCQUFXLE1BQU0sS0FBTixDQUFZLE9BQVosRUFBcUIsZUFBckIsQ0FBWDtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQix1QkFBVyxLQUFYO0FBQ0Q7QUFDRCxjQUFHLEVBQUUsT0FBRixDQUFVLGFBQVYsQ0FBSCxFQUE2QjtBQUMzQiwwQkFBYyxJQUFkLENBQW1CLFFBQW5CO0FBQ0QsV0FGRCxNQUdLLElBQUcsa0JBQWtCLFNBQXJCLEVBQWdDO0FBQ25DLDRCQUFnQixDQUFDLGFBQUQsRUFBZ0IsUUFBaEIsQ0FBaEI7QUFDRCxXQUZJLE1BR0EsSUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQzlCLDRCQUFnQixRQUFoQjtBQUNEO0FBQ0QsaUJBQU8sS0FBUDtBQUNEO0FBcHZCTSxPQUFUO0FBc3ZCQSxVQUFHLGFBQUgsRUFBa0I7QUFDaEIsWUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLGlCQUFPLFVBQVA7QUFDRDtBQUNELGVBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDRCxPQUxELE1BTUs7QUFDSCxZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsbUJBQVMsTUFBVCxDQUFnQixTQUFoQjtBQUNEO0FBQ0QsZUFBTyxVQUFQO0FBQ0Q7QUFDRixLQXB5Qkg7QUFzeUJBLFdBQVEsa0JBQWtCLFNBQW5CLEdBQ0gsYUFERyxHQUVILElBRko7QUFLRCxHQS96QkQ7OztBQWswQkEsSUFBRSxHQUFGLEdBQVEsWUFBVztBQUNqQixNQUFFLE1BQUYsRUFBVSxHQUFWLENBQWMsS0FBZCxDQUFvQixJQUFwQixFQUEwQixTQUExQjtBQUNELEdBRkQ7O0FBSUEsSUFBRSxFQUFGLENBQUssR0FBTCxDQUFTLFFBQVQsR0FBb0I7O0FBRWxCLFVBQWtCLEtBRkE7QUFHbEIsZUFBa0IsS0FIQTs7QUFLbEIsWUFBa0IsS0FMQTtBQU1sQixXQUFrQixLQU5BO0FBT2xCLGFBQWtCLEtBUEE7QUFRbEIsaUJBQWtCLElBUkE7O0FBVWxCLFVBQWtCLEtBVkEsRTtBQVdsQixhQUFrQixLQVhBLEU7QUFZbEIsaUJBQWtCLE1BWkEsRTtBQWFsQixVQUFrQixLQWJBLEU7O0FBZWxCLGFBQWtCLEtBZkEsRTtBQWdCbEIsa0JBQWtCLEtBaEJBLEU7QUFpQmxCLGNBQWtCLEVBakJBLEU7O0FBbUJsQixnQkFBa0IsVUFuQkEsRTs7QUFxQmxCLG1CQUFrQixLQXJCQSxFO0FBc0JsQixXQUFrQixJQXRCQSxFO0FBdUJsQixlQUFrQixVQXZCQSxFO0FBd0JsQixxQkFBa0IsS0F4QkEsRTs7QUEwQmxCLGlCQUFrQixLQTFCQSxFO0FBMkJsQixxQkFBa0IsTUEzQkEsRTs7QUE2QmxCLGlCQUFjLFVBQVMsT0FBVCxFQUFrQixjQUFsQixFQUFrQyxZQUFsQyxFQUFnRCxDQUFFLENBN0I5QyxFO0FBOEJsQixZQUFjLFVBQVMsT0FBVCxFQUFrQixjQUFsQixFQUFrQyxZQUFsQyxFQUFnRCxDQUFFLENBOUI5QyxFO0FBK0JsQixlQUFjLFVBQVMsT0FBVCxFQUFrQixjQUFsQixFQUFrQyxZQUFsQyxFQUFnRCxDQUFFLENBL0I5QyxFO0FBZ0NsQixlQUFjLFVBQVMsT0FBVCxFQUFrQixjQUFsQixFQUFrQyxZQUFsQyxFQUFnRCxDQUFFLENBaEM5QyxFOztBQWtDbEIsZUFBWTtBQUNWLHNCQUFnQixVQUFTLFFBQVQsRUFBbUIsQ0FBRSxDO0FBRDNCLEtBbENNOztBQXNDbEIsV0FBTztBQUNMLFdBQWEsa0RBRFI7QUFFTCxjQUFhLHNDQUZSO0FBR0wsa0JBQWEseURBSFI7QUFJTCxpQkFBYSxpREFKUjtBQUtMLFlBQWEsNENBTFI7QUFNTCxpQkFBYSw2QkFOUjtBQU9MLGtCQUFhLDRFQVBSO0FBUUwsa0JBQWEsc0VBUlI7QUFTTCxhQUFhO0FBVFIsS0F0Q1c7O0FBa0RsQixjQUFXO0FBQ1QsV0FBUyxLQURBO0FBRVQsY0FBUyxRQUZBO0FBR1QsZUFBUztBQUhBLEtBbERPOztBQXdEbEIsZUFBYztBQUNaLGVBQVUsU0FERTtBQUVaLGNBQVU7QUFGRSxLQXhESTs7QUE2RGxCLGNBQWM7QUFDWixZQUFPLFNBREs7QUFFWixVQUFPO0FBRks7O0FBN0RJLEdBQXBCO0FBb0VDLENBcjVCQSxFQXE1QkcsTUFyNUJILEVBcTVCVyxNQXI1QlgsRUFxNUJtQixRQXI1Qm5CIiwiZmlsZSI6InRhYi5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxuICogIyBTZW1hbnRpYyBVSSAtIFRhYlxuICogaHR0cDovL2dpdGh1Yi5jb20vc2VtYW50aWMtb3JnL3NlbWFudGljLXVpL1xuICpcbiAqXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2VcbiAqIGh0dHA6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9NSVRcbiAqXG4gKi9cblxuOyhmdW5jdGlvbiAoJCwgd2luZG93LCBkb2N1bWVudCwgdW5kZWZpbmVkKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG53aW5kb3cgPSAodHlwZW9mIHdpbmRvdyAhPSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuTWF0aCA9PSBNYXRoKVxuICA/IHdpbmRvd1xuICA6ICh0eXBlb2Ygc2VsZiAhPSAndW5kZWZpbmVkJyAmJiBzZWxmLk1hdGggPT0gTWF0aClcbiAgICA/IHNlbGZcbiAgICA6IEZ1bmN0aW9uKCdyZXR1cm4gdGhpcycpKClcbjtcblxuJC5mbi50YWIgPSBmdW5jdGlvbihwYXJhbWV0ZXJzKSB7XG5cbiAgdmFyXG4gICAgLy8gdXNlIHdpbmRvdyBjb250ZXh0IGlmIG5vbmUgc3BlY2lmaWVkXG4gICAgJGFsbE1vZHVsZXMgICAgID0gJC5pc0Z1bmN0aW9uKHRoaXMpXG4gICAgICAgID8gJCh3aW5kb3cpXG4gICAgICAgIDogJCh0aGlzKSxcblxuICAgIG1vZHVsZVNlbGVjdG9yICA9ICRhbGxNb2R1bGVzLnNlbGVjdG9yIHx8ICcnLFxuICAgIHRpbWUgICAgICAgICAgICA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpLFxuICAgIHBlcmZvcm1hbmNlICAgICA9IFtdLFxuXG4gICAgcXVlcnkgICAgICAgICAgID0gYXJndW1lbnRzWzBdLFxuICAgIG1ldGhvZEludm9rZWQgICA9ICh0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycpLFxuICAgIHF1ZXJ5QXJndW1lbnRzICA9IFtdLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSxcblxuICAgIGluaXRpYWxpemVkSGlzdG9yeSA9IGZhbHNlLFxuICAgIHJldHVybmVkVmFsdWVcbiAgO1xuXG4gICRhbGxNb2R1bGVzXG4gICAgLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICB2YXJcblxuICAgICAgICBzZXR0aW5ncyAgICAgICAgPSAoICQuaXNQbGFpbk9iamVjdChwYXJhbWV0ZXJzKSApXG4gICAgICAgICAgPyAkLmV4dGVuZCh0cnVlLCB7fSwgJC5mbi50YWIuc2V0dGluZ3MsIHBhcmFtZXRlcnMpXG4gICAgICAgICAgOiAkLmV4dGVuZCh7fSwgJC5mbi50YWIuc2V0dGluZ3MpLFxuXG4gICAgICAgIGNsYXNzTmFtZSAgICAgICA9IHNldHRpbmdzLmNsYXNzTmFtZSxcbiAgICAgICAgbWV0YWRhdGEgICAgICAgID0gc2V0dGluZ3MubWV0YWRhdGEsXG4gICAgICAgIHNlbGVjdG9yICAgICAgICA9IHNldHRpbmdzLnNlbGVjdG9yLFxuICAgICAgICBlcnJvciAgICAgICAgICAgPSBzZXR0aW5ncy5lcnJvcixcblxuICAgICAgICBldmVudE5hbWVzcGFjZSAgPSAnLicgKyBzZXR0aW5ncy5uYW1lc3BhY2UsXG4gICAgICAgIG1vZHVsZU5hbWVzcGFjZSA9ICdtb2R1bGUtJyArIHNldHRpbmdzLm5hbWVzcGFjZSxcblxuICAgICAgICAkbW9kdWxlICAgICAgICAgPSAkKHRoaXMpLFxuICAgICAgICAkY29udGV4dCxcbiAgICAgICAgJHRhYnMsXG5cbiAgICAgICAgY2FjaGUgICAgICAgICAgID0ge30sXG4gICAgICAgIGZpcnN0TG9hZCAgICAgICA9IHRydWUsXG4gICAgICAgIHJlY3Vyc2lvbkRlcHRoICA9IDAsXG4gICAgICAgIGVsZW1lbnQgICAgICAgICA9IHRoaXMsXG4gICAgICAgIGluc3RhbmNlICAgICAgICA9ICRtb2R1bGUuZGF0YShtb2R1bGVOYW1lc3BhY2UpLFxuXG4gICAgICAgIGFjdGl2ZVRhYlBhdGgsXG4gICAgICAgIHBhcmFtZXRlckFycmF5LFxuICAgICAgICBtb2R1bGUsXG5cbiAgICAgICAgaGlzdG9yeUV2ZW50XG5cbiAgICAgIDtcblxuICAgICAgbW9kdWxlID0ge1xuXG4gICAgICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW5pdGlhbGl6aW5nIHRhYiBtZW51IGl0ZW0nLCAkbW9kdWxlKTtcbiAgICAgICAgICBtb2R1bGUuZml4LmNhbGxiYWNrcygpO1xuICAgICAgICAgIG1vZHVsZS5kZXRlcm1pbmVUYWJzKCk7XG5cbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0RldGVybWluaW5nIHRhYnMnLCBzZXR0aW5ncy5jb250ZXh0LCAkdGFicyk7XG4gICAgICAgICAgLy8gc2V0IHVwIGF1dG9tYXRpYyByb3V0aW5nXG4gICAgICAgICAgaWYoc2V0dGluZ3MuYXV0bykge1xuICAgICAgICAgICAgbW9kdWxlLnNldC5hdXRvKCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIG1vZHVsZS5iaW5kLmV2ZW50cygpO1xuXG4gICAgICAgICAgaWYoc2V0dGluZ3MuaGlzdG9yeSAmJiAhaW5pdGlhbGl6ZWRIaXN0b3J5KSB7XG4gICAgICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZUhpc3RvcnkoKTtcbiAgICAgICAgICAgIGluaXRpYWxpemVkSGlzdG9yeSA9IHRydWU7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgbW9kdWxlLmluc3RhbnRpYXRlKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5zdGFudGlhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU3RvcmluZyBpbnN0YW5jZSBvZiBtb2R1bGUnLCBtb2R1bGUpO1xuICAgICAgICAgIGluc3RhbmNlID0gbW9kdWxlO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5kYXRhKG1vZHVsZU5hbWVzcGFjZSwgbW9kdWxlKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBkZXN0cm95OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0Rlc3Ryb3lpbmcgdGFicycsICRtb2R1bGUpO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5yZW1vdmVEYXRhKG1vZHVsZU5hbWVzcGFjZSlcbiAgICAgICAgICAgIC5vZmYoZXZlbnROYW1lc3BhY2UpXG4gICAgICAgICAgO1xuICAgICAgICB9LFxuXG4gICAgICAgIGJpbmQ6IHtcbiAgICAgICAgICBldmVudHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgLy8gaWYgdXNpbmcgJC50YWIgZG9uJ3QgYWRkIGV2ZW50c1xuICAgICAgICAgICAgaWYoICEkLmlzV2luZG93KCBlbGVtZW50ICkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQXR0YWNoaW5nIHRhYiBhY3RpdmF0aW9uIGV2ZW50cyB0byBlbGVtZW50JywgJG1vZHVsZSk7XG4gICAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgICAub24oJ2NsaWNrJyArIGV2ZW50TmFtZXNwYWNlLCBtb2R1bGUuZXZlbnQuY2xpY2spXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGV0ZXJtaW5lVGFiczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICAkcmVmZXJlbmNlXG4gICAgICAgICAgO1xuXG4gICAgICAgICAgLy8gZGV0ZXJtaW5lIHRhYiBjb250ZXh0XG4gICAgICAgICAgaWYoc2V0dGluZ3MuY29udGV4dCA9PT0gJ3BhcmVudCcpIHtcbiAgICAgICAgICAgIGlmKCRtb2R1bGUuY2xvc2VzdChzZWxlY3Rvci51aSkubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAkcmVmZXJlbmNlID0gJG1vZHVsZS5jbG9zZXN0KHNlbGVjdG9yLnVpKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1VzaW5nIGNsb3Nlc3QgVUkgZWxlbWVudCBhcyBwYXJlbnQnLCAkcmVmZXJlbmNlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAkcmVmZXJlbmNlID0gJG1vZHVsZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICRjb250ZXh0ID0gJHJlZmVyZW5jZS5wYXJlbnQoKTtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdEZXRlcm1pbmVkIHBhcmVudCBlbGVtZW50IGZvciBjcmVhdGluZyBjb250ZXh0JywgJGNvbnRleHQpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHNldHRpbmdzLmNvbnRleHQpIHtcbiAgICAgICAgICAgICRjb250ZXh0ID0gJChzZXR0aW5ncy5jb250ZXh0KTtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdVc2luZyBzZWxlY3RvciBmb3IgdGFiIGNvbnRleHQnLCBzZXR0aW5ncy5jb250ZXh0LCAkY29udGV4dCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgJGNvbnRleHQgPSAkKCdib2R5Jyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIC8vIGZpbmQgdGFic1xuICAgICAgICAgIGlmKHNldHRpbmdzLmNoaWxkcmVuT25seSkge1xuICAgICAgICAgICAgJHRhYnMgPSAkY29udGV4dC5jaGlsZHJlbihzZWxlY3Rvci50YWJzKTtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2VhcmNoaW5nIHRhYiBjb250ZXh0IGNoaWxkcmVuIGZvciB0YWJzJywgJGNvbnRleHQsICR0YWJzKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAkdGFicyA9ICRjb250ZXh0LmZpbmQoc2VsZWN0b3IudGFicyk7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NlYXJjaGluZyB0YWIgY29udGV4dCBmb3IgdGFicycsICRjb250ZXh0LCAkdGFicyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGZpeDoge1xuICAgICAgICAgIGNhbGxiYWNrczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KHBhcmFtZXRlcnMpICYmIChwYXJhbWV0ZXJzLm9uVGFiTG9hZCB8fCBwYXJhbWV0ZXJzLm9uVGFiSW5pdCkgKSB7XG4gICAgICAgICAgICAgIGlmKHBhcmFtZXRlcnMub25UYWJMb2FkKSB7XG4gICAgICAgICAgICAgICAgcGFyYW1ldGVycy5vbkxvYWQgPSBwYXJhbWV0ZXJzLm9uVGFiTG9hZDtcbiAgICAgICAgICAgICAgICBkZWxldGUgcGFyYW1ldGVycy5vblRhYkxvYWQ7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLmxlZ2FjeUxvYWQsIHBhcmFtZXRlcnMub25Mb2FkKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBpZihwYXJhbWV0ZXJzLm9uVGFiSW5pdCkge1xuICAgICAgICAgICAgICAgIHBhcmFtZXRlcnMub25GaXJzdExvYWQgPSBwYXJhbWV0ZXJzLm9uVGFiSW5pdDtcbiAgICAgICAgICAgICAgICBkZWxldGUgcGFyYW1ldGVycy5vblRhYkluaXQ7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLmxlZ2FjeUluaXQsIHBhcmFtZXRlcnMub25GaXJzdExvYWQpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHNldHRpbmdzID0gJC5leHRlbmQodHJ1ZSwge30sICQuZm4udGFiLnNldHRpbmdzLCBwYXJhbWV0ZXJzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5pdGlhbGl6ZUhpc3Rvcnk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW5pdGlhbGl6aW5nIHBhZ2Ugc3RhdGUnKTtcbiAgICAgICAgICBpZiggJC5hZGRyZXNzID09PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3Iuc3RhdGUpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmhpc3RvcnlUeXBlID09ICdzdGF0ZScpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdVc2luZyBIVE1MNSB0byBtYW5hZ2Ugc3RhdGUnKTtcbiAgICAgICAgICAgICAgaWYoc2V0dGluZ3MucGF0aCAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAkLmFkZHJlc3NcbiAgICAgICAgICAgICAgICAgIC5oaXN0b3J5KHRydWUpXG4gICAgICAgICAgICAgICAgICAuc3RhdGUoc2V0dGluZ3MucGF0aClcbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLnBhdGgpO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgJC5hZGRyZXNzXG4gICAgICAgICAgICAgIC5iaW5kKCdjaGFuZ2UnLCBtb2R1bGUuZXZlbnQuaGlzdG9yeS5jaGFuZ2UpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGV2ZW50OiB7XG4gICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgdGFiUGF0aCA9ICQodGhpcykuZGF0YShtZXRhZGF0YS50YWIpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZih0YWJQYXRoICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgaWYoc2V0dGluZ3MuaGlzdG9yeSkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdVcGRhdGluZyBwYWdlIHN0YXRlJywgZXZlbnQpO1xuICAgICAgICAgICAgICAgICQuYWRkcmVzcy52YWx1ZSh0YWJQYXRoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQ2hhbmdpbmcgdGFiJywgZXZlbnQpO1xuICAgICAgICAgICAgICAgIG1vZHVsZS5jaGFuZ2VUYWIodGFiUGF0aCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ05vIHRhYiBzcGVjaWZpZWQnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGhpc3Rvcnk6IHtcbiAgICAgICAgICAgIGNoYW5nZTogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICAgdGFiUGF0aCAgID0gZXZlbnQucGF0aE5hbWVzLmpvaW4oJy8nKSB8fCBtb2R1bGUuZ2V0LmluaXRpYWxQYXRoKCksXG4gICAgICAgICAgICAgICAgcGFnZVRpdGxlID0gc2V0dGluZ3MudGVtcGxhdGVzLmRldGVybWluZVRpdGxlKHRhYlBhdGgpIHx8IGZhbHNlXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmRpc3BsYXkoKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdIaXN0b3J5IGNoYW5nZSBldmVudCcsIHRhYlBhdGgsIGV2ZW50KTtcbiAgICAgICAgICAgICAgaGlzdG9yeUV2ZW50ID0gZXZlbnQ7XG4gICAgICAgICAgICAgIGlmKHRhYlBhdGggIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5jaGFuZ2VUYWIodGFiUGF0aCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYocGFnZVRpdGxlKSB7XG4gICAgICAgICAgICAgICAgJC5hZGRyZXNzLnRpdGxlKHBhZ2VUaXRsZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVmcmVzaDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoYWN0aXZlVGFiUGF0aCkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZWZyZXNoaW5nIHRhYicsIGFjdGl2ZVRhYlBhdGgpO1xuICAgICAgICAgICAgbW9kdWxlLmNoYW5nZVRhYihhY3RpdmVUYWJQYXRoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2FjaGU6IHtcblxuICAgICAgICAgIHJlYWQ6IGZ1bmN0aW9uKGNhY2hlS2V5KSB7XG4gICAgICAgICAgICByZXR1cm4gKGNhY2hlS2V5ICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgID8gY2FjaGVbY2FjaGVLZXldXG4gICAgICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFkZDogZnVuY3Rpb24oY2FjaGVLZXksIGNvbnRlbnQpIHtcbiAgICAgICAgICAgIGNhY2hlS2V5ID0gY2FjaGVLZXkgfHwgYWN0aXZlVGFiUGF0aDtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWRkaW5nIGNhY2hlZCBjb250ZW50IGZvcicsIGNhY2hlS2V5KTtcbiAgICAgICAgICAgIGNhY2hlW2NhY2hlS2V5XSA9IGNvbnRlbnQ7XG4gICAgICAgICAgfSxcbiAgICAgICAgICByZW1vdmU6IGZ1bmN0aW9uKGNhY2hlS2V5KSB7XG4gICAgICAgICAgICBjYWNoZUtleSA9IGNhY2hlS2V5IHx8IGFjdGl2ZVRhYlBhdGg7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1JlbW92aW5nIGNhY2hlZCBjb250ZW50IGZvcicsIGNhY2hlS2V5KTtcbiAgICAgICAgICAgIGRlbGV0ZSBjYWNoZVtjYWNoZUtleV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldDoge1xuICAgICAgICAgIGF1dG86IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHVybCA9ICh0eXBlb2Ygc2V0dGluZ3MucGF0aCA9PSAnc3RyaW5nJylcbiAgICAgICAgICAgICAgICA/IHNldHRpbmdzLnBhdGgucmVwbGFjZSgvXFwvJC8sICcnKSArICcveyR0YWJ9J1xuICAgICAgICAgICAgICAgIDogJy97JHRhYn0nXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU2V0dGluZyB1cCBhdXRvbWF0aWMgdGFiIHJldHJpZXZhbCBmcm9tIHNlcnZlcicsIHVybCk7XG4gICAgICAgICAgICBpZigkLmlzUGxhaW5PYmplY3Qoc2V0dGluZ3MuYXBpU2V0dGluZ3MpKSB7XG4gICAgICAgICAgICAgIHNldHRpbmdzLmFwaVNldHRpbmdzLnVybCA9IHVybDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBzZXR0aW5ncy5hcGlTZXR0aW5ncyA9IHtcbiAgICAgICAgICAgICAgICB1cmw6IHVybFxuICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgbG9hZGluZzogZnVuY3Rpb24odGFiUGF0aCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICR0YWIgICAgICA9IG1vZHVsZS5nZXQudGFiRWxlbWVudCh0YWJQYXRoKSxcbiAgICAgICAgICAgICAgaXNMb2FkaW5nID0gJHRhYi5oYXNDbGFzcyhjbGFzc05hbWUubG9hZGluZylcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKCFpc0xvYWRpbmcpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1NldHRpbmcgbG9hZGluZyBzdGF0ZSBmb3InLCAkdGFiKTtcbiAgICAgICAgICAgICAgJHRhYlxuICAgICAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUubG9hZGluZylcbiAgICAgICAgICAgICAgICAuc2libGluZ3MoJHRhYnMpXG4gICAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSArICcgJyArIGNsYXNzTmFtZS5sb2FkaW5nKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKCR0YWIubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgIHNldHRpbmdzLm9uUmVxdWVzdC5jYWxsKCR0YWJbMF0sIHRhYlBhdGgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzdGF0ZTogZnVuY3Rpb24oc3RhdGUpIHtcbiAgICAgICAgICAgICQuYWRkcmVzcy52YWx1ZShzdGF0ZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGNoYW5nZVRhYjogZnVuY3Rpb24odGFiUGF0aCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgcHVzaFN0YXRlQXZhaWxhYmxlID0gKHdpbmRvdy5oaXN0b3J5ICYmIHdpbmRvdy5oaXN0b3J5LnB1c2hTdGF0ZSksXG4gICAgICAgICAgICBzaG91bGRJZ25vcmVMb2FkICAgPSAocHVzaFN0YXRlQXZhaWxhYmxlICYmIHNldHRpbmdzLmlnbm9yZUZpcnN0TG9hZCAmJiBmaXJzdExvYWQpLFxuICAgICAgICAgICAgcmVtb3RlQ29udGVudCAgICAgID0gKHNldHRpbmdzLmF1dG8gfHwgJC5pc1BsYWluT2JqZWN0KHNldHRpbmdzLmFwaVNldHRpbmdzKSApLFxuICAgICAgICAgICAgLy8gb25seSBhZGQgZGVmYXVsdCBwYXRoIGlmIG5vdCByZW1vdGUgY29udGVudFxuICAgICAgICAgICAgcGF0aEFycmF5ID0gKHJlbW90ZUNvbnRlbnQgJiYgIXNob3VsZElnbm9yZUxvYWQpXG4gICAgICAgICAgICAgID8gbW9kdWxlLnV0aWxpdGllcy5wYXRoVG9BcnJheSh0YWJQYXRoKVxuICAgICAgICAgICAgICA6IG1vZHVsZS5nZXQuZGVmYXVsdFBhdGhBcnJheSh0YWJQYXRoKVxuICAgICAgICAgIDtcbiAgICAgICAgICB0YWJQYXRoID0gbW9kdWxlLnV0aWxpdGllcy5hcnJheVRvUGF0aChwYXRoQXJyYXkpO1xuICAgICAgICAgICQuZWFjaChwYXRoQXJyYXksIGZ1bmN0aW9uKGluZGV4LCB0YWIpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBjdXJyZW50UGF0aEFycmF5ICAgPSBwYXRoQXJyYXkuc2xpY2UoMCwgaW5kZXggKyAxKSxcbiAgICAgICAgICAgICAgY3VycmVudFBhdGggICAgICAgID0gbW9kdWxlLnV0aWxpdGllcy5hcnJheVRvUGF0aChjdXJyZW50UGF0aEFycmF5KSxcblxuICAgICAgICAgICAgICBpc1RhYiAgICAgICAgICAgICAgPSBtb2R1bGUuaXMudGFiKGN1cnJlbnRQYXRoKSxcbiAgICAgICAgICAgICAgaXNMYXN0SW5kZXggICAgICAgID0gKGluZGV4ICsgMSA9PSBwYXRoQXJyYXkubGVuZ3RoKSxcblxuICAgICAgICAgICAgICAkdGFiICAgICAgICAgICAgICAgPSBtb2R1bGUuZ2V0LnRhYkVsZW1lbnQoY3VycmVudFBhdGgpLFxuICAgICAgICAgICAgICAkYW5jaG9yLFxuICAgICAgICAgICAgICBuZXh0UGF0aEFycmF5LFxuICAgICAgICAgICAgICBuZXh0UGF0aCxcbiAgICAgICAgICAgICAgaXNMYXN0VGFiXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnTG9va2luZyBmb3IgdGFiJywgdGFiKTtcbiAgICAgICAgICAgIGlmKGlzVGFiKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdUYWIgd2FzIGZvdW5kJywgdGFiKTtcbiAgICAgICAgICAgICAgLy8gc2NvcGUgdXBcbiAgICAgICAgICAgICAgYWN0aXZlVGFiUGF0aCAgPSBjdXJyZW50UGF0aDtcbiAgICAgICAgICAgICAgcGFyYW1ldGVyQXJyYXkgPSBtb2R1bGUudXRpbGl0aWVzLmZpbHRlckFycmF5KHBhdGhBcnJheSwgY3VycmVudFBhdGhBcnJheSk7XG5cbiAgICAgICAgICAgICAgaWYoaXNMYXN0SW5kZXgpIHtcbiAgICAgICAgICAgICAgICBpc0xhc3RUYWIgPSB0cnVlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIG5leHRQYXRoQXJyYXkgPSBwYXRoQXJyYXkuc2xpY2UoMCwgaW5kZXggKyAyKTtcbiAgICAgICAgICAgICAgICBuZXh0UGF0aCAgICAgID0gbW9kdWxlLnV0aWxpdGllcy5hcnJheVRvUGF0aChuZXh0UGF0aEFycmF5KTtcbiAgICAgICAgICAgICAgICBpc0xhc3RUYWIgICAgID0gKCAhbW9kdWxlLmlzLnRhYihuZXh0UGF0aCkgKTtcbiAgICAgICAgICAgICAgICBpZihpc0xhc3RUYWIpIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdUYWIgcGFyYW1ldGVycyBmb3VuZCcsIG5leHRQYXRoQXJyYXkpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBpZihpc0xhc3RUYWIgJiYgcmVtb3RlQ29udGVudCkge1xuICAgICAgICAgICAgICAgIGlmKCFzaG91bGRJZ25vcmVMb2FkKSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuYWN0aXZhdGUubmF2aWdhdGlvbihjdXJyZW50UGF0aCk7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuZmV0Y2guY29udGVudChjdXJyZW50UGF0aCwgdGFiUGF0aCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdJZ25vcmluZyByZW1vdGUgY29udGVudCBvbiBmaXJzdCB0YWIgbG9hZCcsIGN1cnJlbnRQYXRoKTtcbiAgICAgICAgICAgICAgICAgIGZpcnN0TG9hZCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmNhY2hlLmFkZCh0YWJQYXRoLCAkdGFiLmh0bWwoKSk7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuYWN0aXZhdGUuYWxsKGN1cnJlbnRQYXRoKTtcbiAgICAgICAgICAgICAgICAgIHNldHRpbmdzLm9uRmlyc3RMb2FkLmNhbGwoJHRhYlswXSwgY3VycmVudFBhdGgsIHBhcmFtZXRlckFycmF5LCBoaXN0b3J5RXZlbnQpO1xuICAgICAgICAgICAgICAgICAgc2V0dGluZ3Mub25Mb2FkLmNhbGwoJHRhYlswXSwgY3VycmVudFBhdGgsIHBhcmFtZXRlckFycmF5LCBoaXN0b3J5RXZlbnQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdPcGVuZWQgbG9jYWwgdGFiJywgY3VycmVudFBhdGgpO1xuICAgICAgICAgICAgICAgIG1vZHVsZS5hY3RpdmF0ZS5hbGwoY3VycmVudFBhdGgpO1xuICAgICAgICAgICAgICAgIGlmKCAhbW9kdWxlLmNhY2hlLnJlYWQoY3VycmVudFBhdGgpICkge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmNhY2hlLmFkZChjdXJyZW50UGF0aCwgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0ZpcnN0IHRpbWUgdGFiIGxvYWRlZCBjYWxsaW5nIHRhYiBpbml0Jyk7XG4gICAgICAgICAgICAgICAgICBzZXR0aW5ncy5vbkZpcnN0TG9hZC5jYWxsKCR0YWJbMF0sIGN1cnJlbnRQYXRoLCBwYXJhbWV0ZXJBcnJheSwgaGlzdG9yeUV2ZW50KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgc2V0dGluZ3Mub25Mb2FkLmNhbGwoJHRhYlswXSwgY3VycmVudFBhdGgsIHBhcmFtZXRlckFycmF5LCBoaXN0b3J5RXZlbnQpO1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYodGFiUGF0aC5zZWFyY2goJy8nKSA9PSAtMSAmJiB0YWJQYXRoICE9PSAnJykge1xuICAgICAgICAgICAgICAvLyBsb29rIGZvciBpbiBwYWdlIGFuY2hvclxuICAgICAgICAgICAgICAkYW5jaG9yICAgICA9ICQoJyMnICsgdGFiUGF0aCArICcsIGFbbmFtZT1cIicgKyB0YWJQYXRoICsgJ1wiXScpO1xuICAgICAgICAgICAgICBjdXJyZW50UGF0aCA9ICRhbmNob3IuY2xvc2VzdCgnW2RhdGEtdGFiXScpLmRhdGEobWV0YWRhdGEudGFiKTtcbiAgICAgICAgICAgICAgJHRhYiAgICAgICAgPSBtb2R1bGUuZ2V0LnRhYkVsZW1lbnQoY3VycmVudFBhdGgpO1xuICAgICAgICAgICAgICAvLyBpZiBhbmNob3IgZXhpc3RzIHVzZSBwYXJlbnQgdGFiXG4gICAgICAgICAgICAgIGlmKCRhbmNob3IgJiYgJGFuY2hvci5sZW5ndGggPiAwICYmIGN1cnJlbnRQYXRoKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdBbmNob3IgbGluayB1c2VkLCBvcGVuaW5nIHBhcmVudCB0YWInLCAkdGFiLCAkYW5jaG9yKTtcbiAgICAgICAgICAgICAgICBpZiggISR0YWIuaGFzQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSkgKSB7XG4gICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUuc2Nyb2xsVG8oJGFuY2hvcik7XG4gICAgICAgICAgICAgICAgICB9LCAwKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgbW9kdWxlLmFjdGl2YXRlLmFsbChjdXJyZW50UGF0aCk7XG4gICAgICAgICAgICAgICAgaWYoICFtb2R1bGUuY2FjaGUucmVhZChjdXJyZW50UGF0aCkgKSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuY2FjaGUuYWRkKGN1cnJlbnRQYXRoLCB0cnVlKTtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRmlyc3QgdGltZSB0YWIgbG9hZGVkIGNhbGxpbmcgdGFiIGluaXQnKTtcbiAgICAgICAgICAgICAgICAgIHNldHRpbmdzLm9uRmlyc3RMb2FkLmNhbGwoJHRhYlswXSwgY3VycmVudFBhdGgsIHBhcmFtZXRlckFycmF5LCBoaXN0b3J5RXZlbnQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBzZXR0aW5ncy5vbkxvYWQuY2FsbCgkdGFiWzBdLCBjdXJyZW50UGF0aCwgcGFyYW1ldGVyQXJyYXksIGhpc3RvcnlFdmVudCk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm1pc3NpbmdUYWIsICRtb2R1bGUsICRjb250ZXh0LCBjdXJyZW50UGF0aCk7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICBzY3JvbGxUbzogZnVuY3Rpb24oJGVsZW1lbnQpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIHNjcm9sbE9mZnNldCA9ICgkZWxlbWVudCAmJiAkZWxlbWVudC5sZW5ndGggPiAwKVxuICAgICAgICAgICAgICA/ICRlbGVtZW50Lm9mZnNldCgpLnRvcFxuICAgICAgICAgICAgICA6IGZhbHNlXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKHNjcm9sbE9mZnNldCAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRm9yY2luZyBzY3JvbGwgdG8gYW4gaW4tcGFnZSBsaW5rIGluIGEgaGlkZGVuIHRhYicsIHNjcm9sbE9mZnNldCwgJGVsZW1lbnQpO1xuICAgICAgICAgICAgJChkb2N1bWVudCkuc2Nyb2xsVG9wKHNjcm9sbE9mZnNldCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHVwZGF0ZToge1xuICAgICAgICAgIGNvbnRlbnQ6IGZ1bmN0aW9uKHRhYlBhdGgsIGh0bWwsIGV2YWx1YXRlU2NyaXB0cykge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICR0YWIgPSBtb2R1bGUuZ2V0LnRhYkVsZW1lbnQodGFiUGF0aCksXG4gICAgICAgICAgICAgIHRhYiAgPSAkdGFiWzBdXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBldmFsdWF0ZVNjcmlwdHMgPSAoZXZhbHVhdGVTY3JpcHRzICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgID8gZXZhbHVhdGVTY3JpcHRzXG4gICAgICAgICAgICAgIDogc2V0dGluZ3MuZXZhbHVhdGVTY3JpcHRzXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihldmFsdWF0ZVNjcmlwdHMpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdVcGRhdGluZyBIVE1MIGFuZCBldmFsdWF0aW5nIGlubGluZSBzY3JpcHRzJywgdGFiUGF0aCwgaHRtbCk7XG4gICAgICAgICAgICAgICR0YWIuaHRtbChodG1sKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1VwZGF0aW5nIEhUTUwnLCB0YWJQYXRoLCBodG1sKTtcbiAgICAgICAgICAgICAgdGFiLmlubmVySFRNTCA9IGh0bWw7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGZldGNoOiB7XG5cbiAgICAgICAgICBjb250ZW50OiBmdW5jdGlvbih0YWJQYXRoLCBmdWxsVGFiUGF0aCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICR0YWIgICAgICAgID0gbW9kdWxlLmdldC50YWJFbGVtZW50KHRhYlBhdGgpLFxuICAgICAgICAgICAgICBhcGlTZXR0aW5ncyA9IHtcbiAgICAgICAgICAgICAgICBkYXRhVHlwZSAgICAgICAgIDogJ2h0bWwnLFxuICAgICAgICAgICAgICAgIGVuY29kZVBhcmFtZXRlcnMgOiBmYWxzZSxcbiAgICAgICAgICAgICAgICBvbiAgICAgICAgICAgICAgIDogJ25vdycsXG4gICAgICAgICAgICAgICAgY2FjaGUgICAgICAgICAgICA6IHNldHRpbmdzLmFsd2F5c1JlZnJlc2gsXG4gICAgICAgICAgICAgICAgaGVhZGVycyAgICAgICAgICA6IHtcbiAgICAgICAgICAgICAgICAgICdYLVJlbW90ZSc6IHRydWVcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9uU3VjY2VzcyA6IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICBpZihzZXR0aW5ncy5jYWNoZVR5cGUgPT0gJ3Jlc3BvbnNlJykge1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUuY2FjaGUuYWRkKGZ1bGxUYWJQYXRoLCByZXNwb25zZSk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBtb2R1bGUudXBkYXRlLmNvbnRlbnQodGFiUGF0aCwgcmVzcG9uc2UpO1xuICAgICAgICAgICAgICAgICAgaWYodGFiUGF0aCA9PSBhY3RpdmVUYWJQYXRoKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ29udGVudCBsb2FkZWQnLCB0YWJQYXRoKTtcbiAgICAgICAgICAgICAgICAgICAgbW9kdWxlLmFjdGl2YXRlLnRhYih0YWJQYXRoKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NvbnRlbnQgbG9hZGVkIGluIGJhY2tncm91bmQnLCB0YWJQYXRoKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIHNldHRpbmdzLm9uRmlyc3RMb2FkLmNhbGwoJHRhYlswXSwgdGFiUGF0aCwgcGFyYW1ldGVyQXJyYXksIGhpc3RvcnlFdmVudCk7XG4gICAgICAgICAgICAgICAgICBzZXR0aW5ncy5vbkxvYWQuY2FsbCgkdGFiWzBdLCB0YWJQYXRoLCBwYXJhbWV0ZXJBcnJheSwgaGlzdG9yeUV2ZW50KTtcbiAgICAgICAgICAgICAgICAgIGlmKHNldHRpbmdzLmNhY2hlVHlwZSAhPSAncmVzcG9uc2UnKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vZHVsZS5jYWNoZS5hZGQoZnVsbFRhYlBhdGgsICR0YWIuaHRtbCgpKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHVybERhdGE6IHtcbiAgICAgICAgICAgICAgICAgIHRhYjogZnVsbFRhYlBhdGhcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHJlcXVlc3QgICAgICAgICA9ICR0YWIuYXBpKCdnZXQgcmVxdWVzdCcpIHx8IGZhbHNlLFxuICAgICAgICAgICAgICBleGlzdGluZ1JlcXVlc3QgPSAoIHJlcXVlc3QgJiYgcmVxdWVzdC5zdGF0ZSgpID09PSAncGVuZGluZycgKSxcbiAgICAgICAgICAgICAgcmVxdWVzdFNldHRpbmdzLFxuICAgICAgICAgICAgICBjYWNoZWRDb250ZW50XG4gICAgICAgICAgICA7XG5cbiAgICAgICAgICAgIGZ1bGxUYWJQYXRoICAgPSBmdWxsVGFiUGF0aCB8fCB0YWJQYXRoO1xuICAgICAgICAgICAgY2FjaGVkQ29udGVudCA9IG1vZHVsZS5jYWNoZS5yZWFkKGZ1bGxUYWJQYXRoKTtcblxuXG4gICAgICAgICAgICBpZihzZXR0aW5ncy5jYWNoZSAmJiBjYWNoZWRDb250ZW50KSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5hY3RpdmF0ZS50YWIodGFiUGF0aCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWRkaW5nIGNhY2hlZCBjb250ZW50JywgZnVsbFRhYlBhdGgpO1xuICAgICAgICAgICAgICBpZihzZXR0aW5ncy5ldmFsdWF0ZVNjcmlwdHMgPT0gJ29uY2UnKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnVwZGF0ZS5jb250ZW50KHRhYlBhdGgsIGNhY2hlZENvbnRlbnQsIGZhbHNlKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUudXBkYXRlLmNvbnRlbnQodGFiUGF0aCwgY2FjaGVkQ29udGVudCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgc2V0dGluZ3Mub25Mb2FkLmNhbGwoJHRhYlswXSwgdGFiUGF0aCwgcGFyYW1ldGVyQXJyYXksIGhpc3RvcnlFdmVudCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKGV4aXN0aW5nUmVxdWVzdCkge1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0LmxvYWRpbmcodGFiUGF0aCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ29udGVudCBpcyBhbHJlYWR5IGxvYWRpbmcnLCBmdWxsVGFiUGF0aCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKCQuYXBpICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgcmVxdWVzdFNldHRpbmdzID0gJC5leHRlbmQodHJ1ZSwge30sIHNldHRpbmdzLmFwaVNldHRpbmdzLCBhcGlTZXR0aW5ncyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnUmV0cmlldmluZyByZW1vdGUgY29udGVudCcsIGZ1bGxUYWJQYXRoLCByZXF1ZXN0U2V0dGluZ3MpO1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0LmxvYWRpbmcodGFiUGF0aCk7XG4gICAgICAgICAgICAgICR0YWIuYXBpKHJlcXVlc3RTZXR0aW5ncyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLmFwaSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGFjdGl2YXRlOiB7XG4gICAgICAgICAgYWxsOiBmdW5jdGlvbih0YWJQYXRoKSB7XG4gICAgICAgICAgICBtb2R1bGUuYWN0aXZhdGUudGFiKHRhYlBhdGgpO1xuICAgICAgICAgICAgbW9kdWxlLmFjdGl2YXRlLm5hdmlnYXRpb24odGFiUGF0aCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB0YWI6IGZ1bmN0aW9uKHRhYlBhdGgpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAkdGFiICAgICAgICAgID0gbW9kdWxlLmdldC50YWJFbGVtZW50KHRhYlBhdGgpLFxuICAgICAgICAgICAgICAkZGVhY3RpdmVUYWJzID0gKHNldHRpbmdzLmRlYWN0aXZhdGUgPT0gJ3NpYmxpbmdzJylcbiAgICAgICAgICAgICAgICA/ICR0YWIuc2libGluZ3MoJHRhYnMpXG4gICAgICAgICAgICAgICAgOiAkdGFicy5ub3QoJHRhYiksXG4gICAgICAgICAgICAgIGlzQWN0aXZlICAgICAgPSAkdGFiLmhhc0NsYXNzKGNsYXNzTmFtZS5hY3RpdmUpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU2hvd2luZyB0YWIgY29udGVudCBmb3InLCAkdGFiKTtcbiAgICAgICAgICAgIGlmKCFpc0FjdGl2ZSkge1xuICAgICAgICAgICAgICAkdGFiXG4gICAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5hY3RpdmUpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgJGRlYWN0aXZlVGFic1xuICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuYWN0aXZlICsgJyAnICsgY2xhc3NOYW1lLmxvYWRpbmcpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgaWYoJHRhYi5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgc2V0dGluZ3Mub25WaXNpYmxlLmNhbGwoJHRhYlswXSwgdGFiUGF0aCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIG5hdmlnYXRpb246IGZ1bmN0aW9uKHRhYlBhdGgpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAkbmF2aWdhdGlvbiAgICAgICAgID0gbW9kdWxlLmdldC5uYXZFbGVtZW50KHRhYlBhdGgpLFxuICAgICAgICAgICAgICAkZGVhY3RpdmVOYXZpZ2F0aW9uID0gKHNldHRpbmdzLmRlYWN0aXZhdGUgPT0gJ3NpYmxpbmdzJylcbiAgICAgICAgICAgICAgICA/ICRuYXZpZ2F0aW9uLnNpYmxpbmdzKCRhbGxNb2R1bGVzKVxuICAgICAgICAgICAgICAgIDogJGFsbE1vZHVsZXMubm90KCRuYXZpZ2F0aW9uKSxcbiAgICAgICAgICAgICAgaXNBY3RpdmUgICAgPSAkbmF2aWdhdGlvbi5oYXNDbGFzcyhjbGFzc05hbWUuYWN0aXZlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0FjdGl2YXRpbmcgdGFiIG5hdmlnYXRpb24gZm9yJywgJG5hdmlnYXRpb24sIHRhYlBhdGgpO1xuICAgICAgICAgICAgaWYoIWlzQWN0aXZlKSB7XG4gICAgICAgICAgICAgICRuYXZpZ2F0aW9uXG4gICAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5hY3RpdmUpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgJGRlYWN0aXZlTmF2aWdhdGlvblxuICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuYWN0aXZlICsgJyAnICsgY2xhc3NOYW1lLmxvYWRpbmcpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVhY3RpdmF0ZToge1xuICAgICAgICAgIGFsbDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVhY3RpdmF0ZS5uYXZpZ2F0aW9uKCk7XG4gICAgICAgICAgICBtb2R1bGUuZGVhY3RpdmF0ZS50YWJzKCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBuYXZpZ2F0aW9uOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRhbGxNb2R1bGVzXG4gICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuYWN0aXZlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgdGFiczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkdGFic1xuICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSArICcgJyArIGNsYXNzTmFtZS5sb2FkaW5nKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBpczoge1xuICAgICAgICAgIHRhYjogZnVuY3Rpb24odGFiTmFtZSkge1xuICAgICAgICAgICAgcmV0dXJuICh0YWJOYW1lICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgID8gKCBtb2R1bGUuZ2V0LnRhYkVsZW1lbnQodGFiTmFtZSkubGVuZ3RoID4gMCApXG4gICAgICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0OiB7XG4gICAgICAgICAgaW5pdGlhbFBhdGg6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRhbGxNb2R1bGVzLmVxKDApLmRhdGEobWV0YWRhdGEudGFiKSB8fCAkdGFicy5lcSgwKS5kYXRhKG1ldGFkYXRhLnRhYik7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBwYXRoOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkLmFkZHJlc3MudmFsdWUoKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIC8vIGFkZHMgZGVmYXVsdCB0YWJzIHRvIHRhYiBwYXRoXG4gICAgICAgICAgZGVmYXVsdFBhdGhBcnJheTogZnVuY3Rpb24odGFiUGF0aCkge1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZS51dGlsaXRpZXMucGF0aFRvQXJyYXkoIG1vZHVsZS5nZXQuZGVmYXVsdFBhdGgodGFiUGF0aCkgKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRlZmF1bHRQYXRoOiBmdW5jdGlvbih0YWJQYXRoKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgJGRlZmF1bHROYXYgPSAkYWxsTW9kdWxlcy5maWx0ZXIoJ1tkYXRhLScgKyBtZXRhZGF0YS50YWIgKyAnXj1cIicgKyB0YWJQYXRoICsgJy9cIl0nKS5lcSgwKSxcbiAgICAgICAgICAgICAgZGVmYXVsdFRhYiAgPSAkZGVmYXVsdE5hdi5kYXRhKG1ldGFkYXRhLnRhYikgfHwgZmFsc2VcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKCBkZWZhdWx0VGFiICkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0ZvdW5kIGRlZmF1bHQgdGFiJywgZGVmYXVsdFRhYik7XG4gICAgICAgICAgICAgIGlmKHJlY3Vyc2lvbkRlcHRoIDwgc2V0dGluZ3MubWF4RGVwdGgpIHtcbiAgICAgICAgICAgICAgICByZWN1cnNpb25EZXB0aCsrO1xuICAgICAgICAgICAgICAgIHJldHVybiBtb2R1bGUuZ2V0LmRlZmF1bHRQYXRoKGRlZmF1bHRUYWIpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5yZWN1cnNpb24pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnTm8gZGVmYXVsdCB0YWJzIGZvdW5kIGZvcicsIHRhYlBhdGgsICR0YWJzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJlY3Vyc2lvbkRlcHRoID0gMDtcbiAgICAgICAgICAgIHJldHVybiB0YWJQYXRoO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgbmF2RWxlbWVudDogZnVuY3Rpb24odGFiUGF0aCkge1xuICAgICAgICAgICAgdGFiUGF0aCA9IHRhYlBhdGggfHwgYWN0aXZlVGFiUGF0aDtcbiAgICAgICAgICAgIHJldHVybiAkYWxsTW9kdWxlcy5maWx0ZXIoJ1tkYXRhLScgKyBtZXRhZGF0YS50YWIgKyAnPVwiJyArIHRhYlBhdGggKyAnXCJdJyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB0YWJFbGVtZW50OiBmdW5jdGlvbih0YWJQYXRoKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgJGZ1bGxQYXRoVGFiLFxuICAgICAgICAgICAgICAkc2ltcGxlUGF0aFRhYixcbiAgICAgICAgICAgICAgdGFiUGF0aEFycmF5LFxuICAgICAgICAgICAgICBsYXN0VGFiXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICB0YWJQYXRoICAgICAgICA9IHRhYlBhdGggfHwgYWN0aXZlVGFiUGF0aDtcbiAgICAgICAgICAgIHRhYlBhdGhBcnJheSAgID0gbW9kdWxlLnV0aWxpdGllcy5wYXRoVG9BcnJheSh0YWJQYXRoKTtcbiAgICAgICAgICAgIGxhc3RUYWIgICAgICAgID0gbW9kdWxlLnV0aWxpdGllcy5sYXN0KHRhYlBhdGhBcnJheSk7XG4gICAgICAgICAgICAkZnVsbFBhdGhUYWIgICA9ICR0YWJzLmZpbHRlcignW2RhdGEtJyArIG1ldGFkYXRhLnRhYiArICc9XCInICsgdGFiUGF0aCArICdcIl0nKTtcbiAgICAgICAgICAgICRzaW1wbGVQYXRoVGFiID0gJHRhYnMuZmlsdGVyKCdbZGF0YS0nICsgbWV0YWRhdGEudGFiICsgJz1cIicgKyBsYXN0VGFiICsgJ1wiXScpO1xuICAgICAgICAgICAgcmV0dXJuICgkZnVsbFBhdGhUYWIubGVuZ3RoID4gMClcbiAgICAgICAgICAgICAgPyAkZnVsbFBhdGhUYWJcbiAgICAgICAgICAgICAgOiAkc2ltcGxlUGF0aFRhYlxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgdGFiOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBhY3RpdmVUYWJQYXRoO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICB1dGlsaXRpZXM6IHtcbiAgICAgICAgICBmaWx0ZXJBcnJheTogZnVuY3Rpb24oa2VlcEFycmF5LCByZW1vdmVBcnJheSkge1xuICAgICAgICAgICAgcmV0dXJuICQuZ3JlcChrZWVwQXJyYXksIGZ1bmN0aW9uKGtlZXBWYWx1ZSkge1xuICAgICAgICAgICAgICByZXR1cm4gKCAkLmluQXJyYXkoa2VlcFZhbHVlLCByZW1vdmVBcnJheSkgPT0gLTEpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBsYXN0OiBmdW5jdGlvbihhcnJheSkge1xuICAgICAgICAgICAgcmV0dXJuICQuaXNBcnJheShhcnJheSlcbiAgICAgICAgICAgICAgPyBhcnJheVsgYXJyYXkubGVuZ3RoIC0gMV1cbiAgICAgICAgICAgICAgOiBmYWxzZVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcGF0aFRvQXJyYXk6IGZ1bmN0aW9uKHBhdGhOYW1lKSB7XG4gICAgICAgICAgICBpZihwYXRoTmFtZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIHBhdGhOYW1lID0gYWN0aXZlVGFiUGF0aDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0eXBlb2YgcGF0aE5hbWUgPT0gJ3N0cmluZydcbiAgICAgICAgICAgICAgPyBwYXRoTmFtZS5zcGxpdCgnLycpXG4gICAgICAgICAgICAgIDogW3BhdGhOYW1lXVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYXJyYXlUb1BhdGg6IGZ1bmN0aW9uKHBhdGhBcnJheSkge1xuICAgICAgICAgICAgcmV0dXJuICQuaXNBcnJheShwYXRoQXJyYXkpXG4gICAgICAgICAgICAgID8gcGF0aEFycmF5LmpvaW4oJy8nKVxuICAgICAgICAgICAgICA6IGZhbHNlXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldHRpbmc6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdDaGFuZ2luZyBzZXR0aW5nJywgbmFtZSwgdmFsdWUpO1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBzZXR0aW5ncywgbmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgaWYoJC5pc1BsYWluT2JqZWN0KHNldHRpbmdzW25hbWVdKSkge1xuICAgICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBzZXR0aW5nc1tuYW1lXSwgdmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHNldHRpbmdzW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHNldHRpbmdzW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgaW50ZXJuYWw6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChuYW1lKSApIHtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIG1vZHVsZSwgbmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgbW9kdWxlW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZVtuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGRlYnVnOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50ICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UubG9nKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmluZm8sIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHZlcmJvc2U6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQgJiYgc2V0dGluZ3MudmVyYm9zZSAmJiBzZXR0aW5ncy5kZWJ1Zykge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmluZm8sIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZS5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQpIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvciA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5lcnJvciwgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHBlcmZvcm1hbmNlOiB7XG4gICAgICAgICAgbG9nOiBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgY3VycmVudFRpbWUsXG4gICAgICAgICAgICAgIGV4ZWN1dGlvblRpbWUsXG4gICAgICAgICAgICAgIHByZXZpb3VzVGltZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgY3VycmVudFRpbWUgICA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgICAgICAgICAgICBwcmV2aW91c1RpbWUgID0gdGltZSB8fCBjdXJyZW50VGltZTtcbiAgICAgICAgICAgICAgZXhlY3V0aW9uVGltZSA9IGN1cnJlbnRUaW1lIC0gcHJldmlvdXNUaW1lO1xuICAgICAgICAgICAgICB0aW1lICAgICAgICAgID0gY3VycmVudFRpbWU7XG4gICAgICAgICAgICAgIHBlcmZvcm1hbmNlLnB1c2goe1xuICAgICAgICAgICAgICAgICdOYW1lJyAgICAgICAgICAgOiBtZXNzYWdlWzBdLFxuICAgICAgICAgICAgICAgICdBcmd1bWVudHMnICAgICAgOiBbXS5zbGljZS5jYWxsKG1lc3NhZ2UsIDEpIHx8ICcnLFxuICAgICAgICAgICAgICAgICdFbGVtZW50JyAgICAgICAgOiBlbGVtZW50LFxuICAgICAgICAgICAgICAgICdFeGVjdXRpb24gVGltZScgOiBleGVjdXRpb25UaW1lXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lcik7XG4gICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UudGltZXIgPSBzZXRUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS5kaXNwbGF5LCA1MDApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZGlzcGxheTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgdGl0bGUgPSBzZXR0aW5ncy5uYW1lICsgJzonLFxuICAgICAgICAgICAgICB0b3RhbFRpbWUgPSAwXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICB0aW1lID0gZmFsc2U7XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgdG90YWxUaW1lICs9IGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ107XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHRpdGxlICs9ICcgJyArIHRvdGFsVGltZSArICdtcyc7XG4gICAgICAgICAgICBpZihtb2R1bGVTZWxlY3Rvcikge1xuICAgICAgICAgICAgICB0aXRsZSArPSAnIFxcJycgKyBtb2R1bGVTZWxlY3RvciArICdcXCcnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoIChjb25zb2xlLmdyb3VwICE9PSB1bmRlZmluZWQgfHwgY29uc29sZS50YWJsZSAhPT0gdW5kZWZpbmVkKSAmJiBwZXJmb3JtYW5jZS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBDb2xsYXBzZWQodGl0bGUpO1xuICAgICAgICAgICAgICBpZihjb25zb2xlLnRhYmxlKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS50YWJsZShwZXJmb3JtYW5jZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YVsnTmFtZSddICsgJzogJyArIGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ10rJ21zJyk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgY29uc29sZS5ncm91cEVuZCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcGVyZm9ybWFuY2UgPSBbXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludm9rZTogZnVuY3Rpb24ocXVlcnksIHBhc3NlZEFyZ3VtZW50cywgY29udGV4dCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgb2JqZWN0ID0gaW5zdGFuY2UsXG4gICAgICAgICAgICBtYXhEZXB0aCxcbiAgICAgICAgICAgIGZvdW5kLFxuICAgICAgICAgICAgcmVzcG9uc2VcbiAgICAgICAgICA7XG4gICAgICAgICAgcGFzc2VkQXJndW1lbnRzID0gcGFzc2VkQXJndW1lbnRzIHx8IHF1ZXJ5QXJndW1lbnRzO1xuICAgICAgICAgIGNvbnRleHQgICAgICAgICA9IGVsZW1lbnQgICAgICAgICB8fCBjb250ZXh0O1xuICAgICAgICAgIGlmKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyAmJiBvYmplY3QgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnkgICAgPSBxdWVyeS5zcGxpdCgvW1xcLiBdLyk7XG4gICAgICAgICAgICBtYXhEZXB0aCA9IHF1ZXJ5Lmxlbmd0aCAtIDE7XG4gICAgICAgICAgICAkLmVhY2gocXVlcnksIGZ1bmN0aW9uKGRlcHRoLCB2YWx1ZSkge1xuICAgICAgICAgICAgICB2YXIgY2FtZWxDYXNlVmFsdWUgPSAoZGVwdGggIT0gbWF4RGVwdGgpXG4gICAgICAgICAgICAgICAgPyB2YWx1ZSArIHF1ZXJ5W2RlcHRoICsgMV0uY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBxdWVyeVtkZXB0aCArIDFdLnNsaWNlKDEpXG4gICAgICAgICAgICAgICAgOiBxdWVyeVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gKSAmJiAoZGVwdGggIT0gbWF4RGVwdGgpICkge1xuICAgICAgICAgICAgICAgIG9iamVjdCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgICAgIGZvdW5kID0gb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbdmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFt2YWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFt2YWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5tZXRob2QsIHF1ZXJ5KTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoICQuaXNGdW5jdGlvbiggZm91bmQgKSApIHtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gZm91bmQuYXBwbHkoY29udGV4dCwgcGFzc2VkQXJndW1lbnRzKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihmb3VuZCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXNwb25zZSA9IGZvdW5kO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZigkLmlzQXJyYXkocmV0dXJuZWRWYWx1ZSkpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUucHVzaChyZXNwb25zZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYocmV0dXJuZWRWYWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlID0gW3JldHVybmVkVmFsdWUsIHJlc3BvbnNlXTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihyZXNwb25zZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlID0gcmVzcG9uc2U7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBmb3VuZDtcbiAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIGlmKG1ldGhvZEludm9rZWQpIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICAgIH1cbiAgICAgICAgbW9kdWxlLmludm9rZShxdWVyeSk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIGluc3RhbmNlLmludm9rZSgnZGVzdHJveScpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICB9XG4gICAgfSlcbiAgO1xuICByZXR1cm4gKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZClcbiAgICA/IHJldHVybmVkVmFsdWVcbiAgICA6IHRoaXNcbiAgO1xuXG59O1xuXG4vLyBzaG9ydGN1dCBmb3IgdGFiYmVkIGNvbnRlbnQgd2l0aCBubyBkZWZpbmVkIG5hdmlnYXRpb25cbiQudGFiID0gZnVuY3Rpb24oKSB7XG4gICQod2luZG93KS50YWIuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbn07XG5cbiQuZm4udGFiLnNldHRpbmdzID0ge1xuXG4gIG5hbWUgICAgICAgICAgICA6ICdUYWInLFxuICBuYW1lc3BhY2UgICAgICAgOiAndGFiJyxcblxuICBzaWxlbnQgICAgICAgICAgOiBmYWxzZSxcbiAgZGVidWcgICAgICAgICAgIDogZmFsc2UsXG4gIHZlcmJvc2UgICAgICAgICA6IGZhbHNlLFxuICBwZXJmb3JtYW5jZSAgICAgOiB0cnVlLFxuXG4gIGF1dG8gICAgICAgICAgICA6IGZhbHNlLCAgICAgIC8vIHVzZXMgcGpheCBzdHlsZSBlbmRwb2ludHMgZmV0Y2hpbmcgY29udGVudCBmcm9tIHNhbWUgdXJsIHdpdGggcmVtb3RlLWNvbnRlbnQgaGVhZGVyc1xuICBoaXN0b3J5ICAgICAgICAgOiBmYWxzZSwgICAgICAvLyB1c2UgYnJvd3NlciBoaXN0b3J5XG4gIGhpc3RvcnlUeXBlICAgICA6ICdoYXNoJywgICAgIC8vICMvIG9yIGh0bWw1IHN0YXRlXG4gIHBhdGggICAgICAgICAgICA6IGZhbHNlLCAgICAgIC8vIGJhc2UgcGF0aCBvZiB1cmxcblxuICBjb250ZXh0ICAgICAgICAgOiBmYWxzZSwgICAgICAvLyBzcGVjaWZ5IGEgY29udGV4dCB0aGF0IHRhYnMgbXVzdCBhcHBlYXIgaW5zaWRlXG4gIGNoaWxkcmVuT25seSAgICA6IGZhbHNlLCAgICAgIC8vIHVzZSBvbmx5IHRhYnMgdGhhdCBhcmUgY2hpbGRyZW4gb2YgY29udGV4dFxuICBtYXhEZXB0aCAgICAgICAgOiAyNSwgICAgICAgICAvLyBtYXggZGVwdGggYSB0YWIgY2FuIGJlIG5lc3RlZFxuXG4gIGRlYWN0aXZhdGUgICAgICA6ICdzaWJsaW5ncycsIC8vIHdoZXRoZXIgdGFicyBzaG91bGQgZGVhY3RpdmF0ZSBzaWJsaW5nIG1lbnUgZWxlbWVudHMgb3IgYWxsIGVsZW1lbnRzIGluaXRpYWxpemVkIHRvZ2V0aGVyXG5cbiAgYWx3YXlzUmVmcmVzaCAgIDogZmFsc2UsICAgICAgLy8gbG9hZCB0YWIgY29udGVudCBuZXcgZXZlcnkgdGFiIGNsaWNrXG4gIGNhY2hlICAgICAgICAgICA6IHRydWUsICAgICAgIC8vIGNhY2hlIHRoZSBjb250ZW50IHJlcXVlc3RzIHRvIHB1bGwgbG9jYWxseVxuICBjYWNoZVR5cGUgICAgICAgOiAncmVzcG9uc2UnLCAvLyBXaGV0aGVyIHRvIGNhY2hlIGV4YWN0IHJlc3BvbnNlLCBvciB0byBodG1sIGNhY2hlIGNvbnRlbnRzIGFmdGVyIHNjcmlwdHMgZXhlY3V0ZVxuICBpZ25vcmVGaXJzdExvYWQgOiBmYWxzZSwgICAgICAvLyBkb24ndCBsb2FkIHJlbW90ZSBjb250ZW50IG9uIGZpcnN0IGxvYWRcblxuICBhcGlTZXR0aW5ncyAgICAgOiBmYWxzZSwgICAgICAvLyBzZXR0aW5ncyBmb3IgYXBpIGNhbGxcbiAgZXZhbHVhdGVTY3JpcHRzIDogJ29uY2UnLCAgICAgLy8gd2hldGhlciBpbmxpbmUgc2NyaXB0cyBzaG91bGQgYmUgcGFyc2VkICh0cnVlL2ZhbHNlL29uY2UpLiBPbmNlIHdpbGwgbm90IHJlLWV2YWx1YXRlIG9uIGNhY2hlZCBjb250ZW50XG5cbiAgb25GaXJzdExvYWQgOiBmdW5jdGlvbih0YWJQYXRoLCBwYXJhbWV0ZXJBcnJheSwgaGlzdG9yeUV2ZW50KSB7fSwgLy8gY2FsbGVkIGZpcnN0IHRpbWUgbG9hZGVkXG4gIG9uTG9hZCAgICAgIDogZnVuY3Rpb24odGFiUGF0aCwgcGFyYW1ldGVyQXJyYXksIGhpc3RvcnlFdmVudCkge30sIC8vIGNhbGxlZCBvbiBldmVyeSBsb2FkXG4gIG9uVmlzaWJsZSAgIDogZnVuY3Rpb24odGFiUGF0aCwgcGFyYW1ldGVyQXJyYXksIGhpc3RvcnlFdmVudCkge30sIC8vIGNhbGxlZCBldmVyeSB0aW1lIHRhYiB2aXNpYmxlXG4gIG9uUmVxdWVzdCAgIDogZnVuY3Rpb24odGFiUGF0aCwgcGFyYW1ldGVyQXJyYXksIGhpc3RvcnlFdmVudCkge30sIC8vIGNhbGxlZCBldmVyIHRpbWUgYSB0YWIgYmVpbmdzIGxvYWRpbmcgcmVtb3RlIGNvbnRlbnRcblxuICB0ZW1wbGF0ZXMgOiB7XG4gICAgZGV0ZXJtaW5lVGl0bGU6IGZ1bmN0aW9uKHRhYkFycmF5KSB7fSAvLyByZXR1cm5zIHBhZ2UgdGl0bGUgZm9yIHBhdGhcbiAgfSxcblxuICBlcnJvcjoge1xuICAgIGFwaSAgICAgICAgOiAnWW91IGF0dGVtcHRlZCB0byBsb2FkIGNvbnRlbnQgd2l0aG91dCBBUEkgbW9kdWxlJyxcbiAgICBtZXRob2QgICAgIDogJ1RoZSBtZXRob2QgeW91IGNhbGxlZCBpcyBub3QgZGVmaW5lZCcsXG4gICAgbWlzc2luZ1RhYiA6ICdBY3RpdmF0ZWQgdGFiIGNhbm5vdCBiZSBmb3VuZC4gVGFicyBhcmUgY2FzZS1zZW5zaXRpdmUuJyxcbiAgICBub0NvbnRlbnQgIDogJ1RoZSB0YWIgeW91IHNwZWNpZmllZCBpcyBtaXNzaW5nIGEgY29udGVudCB1cmwuJyxcbiAgICBwYXRoICAgICAgIDogJ0hpc3RvcnkgZW5hYmxlZCwgYnV0IG5vIHBhdGggd2FzIHNwZWNpZmllZCcsXG4gICAgcmVjdXJzaW9uICA6ICdNYXggcmVjdXJzaXZlIGRlcHRoIHJlYWNoZWQnLFxuICAgIGxlZ2FjeUluaXQgOiAnb25UYWJJbml0IGhhcyBiZWVuIHJlbmFtZWQgdG8gb25GaXJzdExvYWQgaW4gMi4wLCBwbGVhc2UgYWRqdXN0IHlvdXIgY29kZS4nLFxuICAgIGxlZ2FjeUxvYWQgOiAnb25UYWJMb2FkIGhhcyBiZWVuIHJlbmFtZWQgdG8gb25Mb2FkIGluIDIuMC4gUGxlYXNlIGFkanVzdCB5b3VyIGNvZGUnLFxuICAgIHN0YXRlICAgICAgOiAnSGlzdG9yeSByZXF1aXJlcyBBc3VhbFxcJ3MgQWRkcmVzcyBsaWJyYXJ5IDxodHRwczovL2dpdGh1Yi5jb20vYXN1YWwvanF1ZXJ5LWFkZHJlc3M+J1xuICB9LFxuXG4gIG1ldGFkYXRhIDoge1xuICAgIHRhYiAgICA6ICd0YWInLFxuICAgIGxvYWRlZCA6ICdsb2FkZWQnLFxuICAgIHByb21pc2U6ICdwcm9taXNlJ1xuICB9LFxuXG4gIGNsYXNzTmFtZSAgIDoge1xuICAgIGxvYWRpbmcgOiAnbG9hZGluZycsXG4gICAgYWN0aXZlICA6ICdhY3RpdmUnXG4gIH0sXG5cbiAgc2VsZWN0b3IgICAgOiB7XG4gICAgdGFicyA6ICcudWkudGFiJyxcbiAgICB1aSAgIDogJy51aSdcbiAgfVxuXG59O1xuXG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7XG4iXX0=