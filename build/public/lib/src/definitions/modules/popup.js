/*!
 * # Semantic UI - Popup
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.popup = function (parameters) {
    var $allModules = $(this),
        $document = $(document),
        $window = $(window),
        $body = $('body'),
        moduleSelector = $allModules.selector || '',
        hasTouch = true,
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue;
    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.popup.settings, parameters) : $.extend({}, $.fn.popup.settings),
          selector = settings.selector,
          className = settings.className,
          error = settings.error,
          metadata = settings.metadata,
          namespace = settings.namespace,
          eventNamespace = '.' + settings.namespace,
          moduleNamespace = 'module-' + namespace,
          $module = $(this),
          $context = $(settings.context),
          $scrollContext = $(settings.scrollContext),
          $boundary = $(settings.boundary),
          $target = settings.target ? $(settings.target) : $module,
          $popup,
          $offsetParent,
          searchDepth = 0,
          triedPositions = false,
          openedWithTouch = false,
          element = this,
          instance = $module.data(moduleNamespace),
          documentObserver,
          elementNamespace,
          id,
          module;

      module = {

        // binds events
        initialize: function () {
          module.debug('Initializing', $module);
          module.createID();
          module.bind.events();
          if (!module.exists() && settings.preserve) {
            module.create();
          }
          module.observeChanges();
          module.instantiate();
        },

        instantiate: function () {
          module.verbose('Storing instance', module);
          instance = module;
          $module.data(moduleNamespace, instance);
        },

        observeChanges: function () {
          if ('MutationObserver' in window) {
            documentObserver = new MutationObserver(module.event.documentChanged);
            documentObserver.observe(document, {
              childList: true,
              subtree: true
            });
            module.debug('Setting up mutation observer', documentObserver);
          }
        },

        refresh: function () {
          if (settings.popup) {
            $popup = $(settings.popup).eq(0);
          } else {
            if (settings.inline) {
              $popup = $target.nextAll(selector.popup).eq(0);
              settings.popup = $popup;
            }
          }
          if (settings.popup) {
            $popup.addClass(className.loading);
            $offsetParent = module.get.offsetParent();
            $popup.removeClass(className.loading);
            if (settings.movePopup && module.has.popup() && module.get.offsetParent($popup)[0] !== $offsetParent[0]) {
              module.debug('Moving popup to the same offset parent as activating element');
              $popup.detach().appendTo($offsetParent);
            }
          } else {
            $offsetParent = settings.inline ? module.get.offsetParent($target) : module.has.popup() ? module.get.offsetParent($popup) : $body;
          }
          if ($offsetParent.is('html') && $offsetParent[0] !== $body[0]) {
            module.debug('Setting page as offset parent');
            $offsetParent = $body;
          }
          if (module.get.variation()) {
            module.set.variation();
          }
        },

        reposition: function () {
          module.refresh();
          module.set.position();
        },

        destroy: function () {
          module.debug('Destroying previous module');
          if (documentObserver) {
            documentObserver.disconnect();
          }
          // remove element only if was created dynamically
          if ($popup && !settings.preserve) {
            module.removePopup();
          }
          // clear all timeouts
          clearTimeout(module.hideTimer);
          clearTimeout(module.showTimer);
          // remove events
          module.unbind.close();
          module.unbind.events();
          $module.removeData(moduleNamespace);
        },

        event: {
          start: function (event) {
            var delay = $.isPlainObject(settings.delay) ? settings.delay.show : settings.delay;
            clearTimeout(module.hideTimer);
            if (!openedWithTouch) {
              module.showTimer = setTimeout(module.show, delay);
            }
          },
          end: function () {
            var delay = $.isPlainObject(settings.delay) ? settings.delay.hide : settings.delay;
            clearTimeout(module.showTimer);
            module.hideTimer = setTimeout(module.hide, delay);
          },
          touchstart: function (event) {
            openedWithTouch = true;
            module.show();
          },
          resize: function () {
            if (module.is.visible()) {
              module.set.position();
            }
          },
          documentChanged: function (mutations) {
            [].forEach.call(mutations, function (mutation) {
              if (mutation.removedNodes) {
                [].forEach.call(mutation.removedNodes, function (node) {
                  if (node == element || $(node).find(element).length > 0) {
                    module.debug('Element removed from DOM, tearing down events');
                    module.destroy();
                  }
                });
              }
            });
          },
          hideGracefully: function (event) {
            var $target = $(event.target),
                isInDOM = $.contains(document.documentElement, event.target),
                inPopup = $target.closest(selector.popup).length > 0;
            // don't close on clicks inside popup
            if (event && !inPopup && isInDOM) {
              module.debug('Click occurred outside popup hiding popup');
              module.hide();
            } else {
              module.debug('Click was inside popup, keeping popup open');
            }
          }
        },

        // generates popup html from metadata
        create: function () {
          var html = module.get.html(),
              title = module.get.title(),
              content = module.get.content();

          if (html || content || title) {
            module.debug('Creating pop-up html');
            if (!html) {
              html = settings.templates.popup({
                title: title,
                content: content
              });
            }
            $popup = $('<div/>').addClass(className.popup).data(metadata.activator, $module).html(html);
            if (settings.inline) {
              module.verbose('Inserting popup element inline', $popup);
              $popup.insertAfter($module);
            } else {
              module.verbose('Appending popup element to body', $popup);
              $popup.appendTo($context);
            }
            module.refresh();
            module.set.variation();

            if (settings.hoverable) {
              module.bind.popup();
            }
            settings.onCreate.call($popup, element);
          } else if ($target.next(selector.popup).length !== 0) {
            module.verbose('Pre-existing popup found');
            settings.inline = true;
            settings.popup = $target.next(selector.popup).data(metadata.activator, $module);
            module.refresh();
            if (settings.hoverable) {
              module.bind.popup();
            }
          } else if (settings.popup) {
            $(settings.popup).data(metadata.activator, $module);
            module.verbose('Used popup specified in settings');
            module.refresh();
            if (settings.hoverable) {
              module.bind.popup();
            }
          } else {
            module.debug('No content specified skipping display', element);
          }
        },

        createID: function () {
          id = (Math.random().toString(16) + '000000000').substr(2, 8);
          elementNamespace = '.' + id;
          module.verbose('Creating unique id for element', id);
        },

        // determines popup state
        toggle: function () {
          module.debug('Toggling pop-up');
          if (module.is.hidden()) {
            module.debug('Popup is hidden, showing pop-up');
            module.unbind.close();
            module.show();
          } else {
            module.debug('Popup is visible, hiding pop-up');
            module.hide();
          }
        },

        show: function (callback) {
          callback = callback || function () {};
          module.debug('Showing pop-up', settings.transition);
          if (module.is.hidden() && !(module.is.active() && module.is.dropdown())) {
            if (!module.exists()) {
              module.create();
            }
            if (settings.onShow.call($popup, element) === false) {
              module.debug('onShow callback returned false, cancelling popup animation');
              return;
            } else if (!settings.preserve && !settings.popup) {
              module.refresh();
            }
            if ($popup && module.set.position()) {
              module.save.conditions();
              if (settings.exclusive) {
                module.hideAll();
              }
              module.animate.show(callback);
            }
          }
        },

        hide: function (callback) {
          callback = callback || function () {};
          if (module.is.visible() || module.is.animating()) {
            if (settings.onHide.call($popup, element) === false) {
              module.debug('onHide callback returned false, cancelling popup animation');
              return;
            }
            module.remove.visible();
            module.unbind.close();
            module.restore.conditions();
            module.animate.hide(callback);
          }
        },

        hideAll: function () {
          $(selector.popup).filter('.' + className.visible).each(function () {
            $(this).data(metadata.activator).popup('hide');
          });
        },
        exists: function () {
          if (!$popup) {
            return false;
          }
          if (settings.inline || settings.popup) {
            return module.has.popup();
          } else {
            return $popup.closest($context).length >= 1 ? true : false;
          }
        },

        removePopup: function () {
          if (module.has.popup() && !settings.popup) {
            module.debug('Removing popup', $popup);
            $popup.remove();
            $popup = undefined;
            settings.onRemove.call($popup, element);
          }
        },

        save: {
          conditions: function () {
            module.cache = {
              title: $module.attr('title')
            };
            if (module.cache.title) {
              $module.removeAttr('title');
            }
            module.verbose('Saving original attributes', module.cache.title);
          }
        },
        restore: {
          conditions: function () {
            if (module.cache && module.cache.title) {
              $module.attr('title', module.cache.title);
              module.verbose('Restoring original attributes', module.cache.title);
            }
            return true;
          }
        },
        supports: {
          svg: function () {
            return typeof SVGGraphicsElement === undefined;
          }
        },
        animate: {
          show: function (callback) {
            callback = $.isFunction(callback) ? callback : function () {};
            if (settings.transition && $.fn.transition !== undefined && $module.transition('is supported')) {
              module.set.visible();
              $popup.transition({
                animation: settings.transition + ' in',
                queue: false,
                debug: settings.debug,
                verbose: settings.verbose,
                duration: settings.duration,
                onComplete: function () {
                  module.bind.close();
                  callback.call($popup, element);
                  settings.onVisible.call($popup, element);
                }
              });
            } else {
              module.error(error.noTransition);
            }
          },
          hide: function (callback) {
            callback = $.isFunction(callback) ? callback : function () {};
            module.debug('Hiding pop-up');
            if (settings.onHide.call($popup, element) === false) {
              module.debug('onHide callback returned false, cancelling popup animation');
              return;
            }
            if (settings.transition && $.fn.transition !== undefined && $module.transition('is supported')) {
              $popup.transition({
                animation: settings.transition + ' out',
                queue: false,
                duration: settings.duration,
                debug: settings.debug,
                verbose: settings.verbose,
                onComplete: function () {
                  module.reset();
                  callback.call($popup, element);
                  settings.onHidden.call($popup, element);
                }
              });
            } else {
              module.error(error.noTransition);
            }
          }
        },

        change: {
          content: function (html) {
            $popup.html(html);
          }
        },

        get: {
          html: function () {
            $module.removeData(metadata.html);
            return $module.data(metadata.html) || settings.html;
          },
          title: function () {
            $module.removeData(metadata.title);
            return $module.data(metadata.title) || settings.title;
          },
          content: function () {
            $module.removeData(metadata.content);
            return $module.data(metadata.content) || $module.attr('title') || settings.content;
          },
          variation: function () {
            $module.removeData(metadata.variation);
            return $module.data(metadata.variation) || settings.variation;
          },
          popup: function () {
            return $popup;
          },
          popupOffset: function () {
            return $popup.offset();
          },
          calculations: function () {
            var targetElement = $target[0],
                isWindow = $boundary[0] == window,
                targetPosition = settings.inline || settings.popup && settings.movePopup ? $target.position() : $target.offset(),
                screenPosition = isWindow ? { top: 0, left: 0 } : $boundary.offset(),
                calculations = {},
                scroll = isWindow ? { top: $window.scrollTop(), left: $window.scrollLeft() } : { top: 0, left: 0 },
                screen;
            calculations = {
              // element which is launching popup
              target: {
                element: $target[0],
                width: $target.outerWidth(),
                height: $target.outerHeight(),
                top: targetPosition.top,
                left: targetPosition.left,
                margin: {}
              },
              // popup itself
              popup: {
                width: $popup.outerWidth(),
                height: $popup.outerHeight()
              },
              // offset container (or 3d context)
              parent: {
                width: $offsetParent.outerWidth(),
                height: $offsetParent.outerHeight()
              },
              // screen boundaries
              screen: {
                top: screenPosition.top,
                left: screenPosition.left,
                scroll: {
                  top: scroll.top,
                  left: scroll.left
                },
                width: $boundary.width(),
                height: $boundary.height()
              }
            };

            // add in container calcs if fluid
            if (settings.setFluidWidth && module.is.fluid()) {
              calculations.container = {
                width: $popup.parent().outerWidth()
              };
              calculations.popup.width = calculations.container.width;
            }

            // add in margins if inline
            calculations.target.margin.top = settings.inline ? parseInt(window.getComputedStyle(targetElement).getPropertyValue('margin-top'), 10) : 0;
            calculations.target.margin.left = settings.inline ? module.is.rtl() ? parseInt(window.getComputedStyle(targetElement).getPropertyValue('margin-right'), 10) : parseInt(window.getComputedStyle(targetElement).getPropertyValue('margin-left'), 10) : 0;
            // calculate screen boundaries
            screen = calculations.screen;
            calculations.boundary = {
              top: screen.top + screen.scroll.top,
              bottom: screen.top + screen.scroll.top + screen.height,
              left: screen.left + screen.scroll.left,
              right: screen.left + screen.scroll.left + screen.width
            };
            return calculations;
          },
          id: function () {
            return id;
          },
          startEvent: function () {
            if (settings.on == 'hover') {
              return 'mouseenter';
            } else if (settings.on == 'focus') {
              return 'focus';
            }
            return false;
          },
          scrollEvent: function () {
            return 'scroll';
          },
          endEvent: function () {
            if (settings.on == 'hover') {
              return 'mouseleave';
            } else if (settings.on == 'focus') {
              return 'blur';
            }
            return false;
          },
          distanceFromBoundary: function (offset, calculations) {
            var distanceFromBoundary = {},
                popup,
                boundary;
            calculations = calculations || module.get.calculations();

            // shorthand
            popup = calculations.popup;
            boundary = calculations.boundary;

            if (offset) {
              distanceFromBoundary = {
                top: offset.top - boundary.top,
                left: offset.left - boundary.left,
                right: boundary.right - (offset.left + popup.width),
                bottom: boundary.bottom - (offset.top + popup.height)
              };
              module.verbose('Distance from boundaries determined', offset, distanceFromBoundary);
            }
            return distanceFromBoundary;
          },
          offsetParent: function ($target) {
            var element = $target !== undefined ? $target[0] : $module[0],
                parentNode = element.parentNode,
                $node = $(parentNode);
            if (parentNode) {
              var is2D = $node.css('transform') === 'none',
                  isStatic = $node.css('position') === 'static',
                  isHTML = $node.is('html');
              while (parentNode && !isHTML && isStatic && is2D) {
                parentNode = parentNode.parentNode;
                $node = $(parentNode);
                is2D = $node.css('transform') === 'none';
                isStatic = $node.css('position') === 'static';
                isHTML = $node.is('html');
              }
            }
            return $node && $node.length > 0 ? $node : $();
          },
          positions: function () {
            return {
              'top left': false,
              'top center': false,
              'top right': false,
              'bottom left': false,
              'bottom center': false,
              'bottom right': false,
              'left center': false,
              'right center': false
            };
          },
          nextPosition: function (position) {
            var positions = position.split(' '),
                verticalPosition = positions[0],
                horizontalPosition = positions[1],
                opposite = {
              top: 'bottom',
              bottom: 'top',
              left: 'right',
              right: 'left'
            },
                adjacent = {
              left: 'center',
              center: 'right',
              right: 'left'
            },
                backup = {
              'top left': 'top center',
              'top center': 'top right',
              'top right': 'right center',
              'right center': 'bottom right',
              'bottom right': 'bottom center',
              'bottom center': 'bottom left',
              'bottom left': 'left center',
              'left center': 'top left'
            },
                adjacentsAvailable = verticalPosition == 'top' || verticalPosition == 'bottom',
                oppositeTried = false,
                adjacentTried = false,
                nextPosition = false;
            if (!triedPositions) {
              module.verbose('All available positions available');
              triedPositions = module.get.positions();
            }

            module.debug('Recording last position tried', position);
            triedPositions[position] = true;

            if (settings.prefer === 'opposite') {
              nextPosition = [opposite[verticalPosition], horizontalPosition];
              nextPosition = nextPosition.join(' ');
              oppositeTried = triedPositions[nextPosition] === true;
              module.debug('Trying opposite strategy', nextPosition);
            }
            if (settings.prefer === 'adjacent' && adjacentsAvailable) {
              nextPosition = [verticalPosition, adjacent[horizontalPosition]];
              nextPosition = nextPosition.join(' ');
              adjacentTried = triedPositions[nextPosition] === true;
              module.debug('Trying adjacent strategy', nextPosition);
            }
            if (adjacentTried || oppositeTried) {
              module.debug('Using backup position', nextPosition);
              nextPosition = backup[position];
            }
            return nextPosition;
          }
        },

        set: {
          position: function (position, calculations) {

            // exit conditions
            if ($target.length === 0 || $popup.length === 0) {
              module.error(error.notFound);
              return;
            }
            var offset, distanceAway, target, popup, parent, positioning, popupOffset, distanceFromBoundary;

            calculations = calculations || module.get.calculations();
            position = position || $module.data(metadata.position) || settings.position;

            offset = $module.data(metadata.offset) || settings.offset;
            distanceAway = settings.distanceAway;

            // shorthand
            target = calculations.target;
            popup = calculations.popup;
            parent = calculations.parent;

            if (target.width === 0 && target.height === 0 && !module.is.svg(target.element)) {
              module.debug('Popup target is hidden, no action taken');
              return false;
            }

            if (settings.inline) {
              module.debug('Adding margin to calculation', target.margin);
              if (position == 'left center' || position == 'right center') {
                offset += target.margin.top;
                distanceAway += -target.margin.left;
              } else if (position == 'top left' || position == 'top center' || position == 'top right') {
                offset += target.margin.left;
                distanceAway -= target.margin.top;
              } else {
                offset += target.margin.left;
                distanceAway += target.margin.top;
              }
            }

            module.debug('Determining popup position from calculations', position, calculations);

            if (module.is.rtl()) {
              position = position.replace(/left|right/g, function (match) {
                return match == 'left' ? 'right' : 'left';
              });
              module.debug('RTL: Popup position updated', position);
            }

            // if last attempt use specified last resort position
            if (searchDepth == settings.maxSearchDepth && typeof settings.lastResort === 'string') {
              position = settings.lastResort;
            }

            switch (position) {
              case 'top left':
                positioning = {
                  top: 'auto',
                  bottom: parent.height - target.top + distanceAway,
                  left: target.left + offset,
                  right: 'auto'
                };
                break;
              case 'top center':
                positioning = {
                  bottom: parent.height - target.top + distanceAway,
                  left: target.left + target.width / 2 - popup.width / 2 + offset,
                  top: 'auto',
                  right: 'auto'
                };
                break;
              case 'top right':
                positioning = {
                  bottom: parent.height - target.top + distanceAway,
                  right: parent.width - target.left - target.width - offset,
                  top: 'auto',
                  left: 'auto'
                };
                break;
              case 'left center':
                positioning = {
                  top: target.top + target.height / 2 - popup.height / 2 + offset,
                  right: parent.width - target.left + distanceAway,
                  left: 'auto',
                  bottom: 'auto'
                };
                break;
              case 'right center':
                positioning = {
                  top: target.top + target.height / 2 - popup.height / 2 + offset,
                  left: target.left + target.width + distanceAway,
                  bottom: 'auto',
                  right: 'auto'
                };
                break;
              case 'bottom left':
                positioning = {
                  top: target.top + target.height + distanceAway,
                  left: target.left + offset,
                  bottom: 'auto',
                  right: 'auto'
                };
                break;
              case 'bottom center':
                positioning = {
                  top: target.top + target.height + distanceAway,
                  left: target.left + target.width / 2 - popup.width / 2 + offset,
                  bottom: 'auto',
                  right: 'auto'
                };
                break;
              case 'bottom right':
                positioning = {
                  top: target.top + target.height + distanceAway,
                  right: parent.width - target.left - target.width - offset,
                  left: 'auto',
                  bottom: 'auto'
                };
                break;
            }
            if (positioning === undefined) {
              module.error(error.invalidPosition, position);
            }

            module.debug('Calculated popup positioning values', positioning);

            // tentatively place on stage
            $popup.css(positioning).removeClass(className.position).addClass(position).addClass(className.loading);

            popupOffset = module.get.popupOffset();

            // see if any boundaries are surpassed with this tentative position
            distanceFromBoundary = module.get.distanceFromBoundary(popupOffset, calculations);

            if (module.is.offstage(distanceFromBoundary, position)) {
              module.debug('Position is outside viewport', position);
              if (searchDepth < settings.maxSearchDepth) {
                searchDepth++;
                position = module.get.nextPosition(position);
                module.debug('Trying new position', position);
                return $popup ? module.set.position(position, calculations) : false;
              } else {
                if (settings.lastResort) {
                  module.debug('No position found, showing with last position');
                } else {
                  module.debug('Popup could not find a position to display', $popup);
                  module.error(error.cannotPlace, element);
                  module.remove.attempts();
                  module.remove.loading();
                  module.reset();
                  settings.onUnplaceable.call($popup, element);
                  return false;
                }
              }
            }
            module.debug('Position is on stage', position);
            module.remove.attempts();
            module.remove.loading();
            if (settings.setFluidWidth && module.is.fluid()) {
              module.set.fluidWidth(calculations);
            }
            return true;
          },

          fluidWidth: function (calculations) {
            calculations = calculations || module.get.calculations();
            module.debug('Automatically setting element width to parent width', calculations.parent.width);
            $popup.css('width', calculations.container.width);
          },

          variation: function (variation) {
            variation = variation || module.get.variation();
            if (variation && module.has.popup()) {
              module.verbose('Adding variation to popup', variation);
              $popup.addClass(variation);
            }
          },

          visible: function () {
            $module.addClass(className.visible);
          }
        },

        remove: {
          loading: function () {
            $popup.removeClass(className.loading);
          },
          variation: function (variation) {
            variation = variation || module.get.variation();
            if (variation) {
              module.verbose('Removing variation', variation);
              $popup.removeClass(variation);
            }
          },
          visible: function () {
            $module.removeClass(className.visible);
          },
          attempts: function () {
            module.verbose('Resetting all searched positions');
            searchDepth = 0;
            triedPositions = false;
          }
        },

        bind: {
          events: function () {
            module.debug('Binding popup events to module');
            if (settings.on == 'click') {
              $module.on('click' + eventNamespace, module.toggle);
            }
            if (settings.on == 'hover' && hasTouch) {
              $module.on('touchstart' + eventNamespace, module.event.touchstart);
            }
            if (module.get.startEvent()) {
              $module.on(module.get.startEvent() + eventNamespace, module.event.start).on(module.get.endEvent() + eventNamespace, module.event.end);
            }
            if (settings.target) {
              module.debug('Target set to element', $target);
            }
            $window.on('resize' + elementNamespace, module.event.resize);
          },
          popup: function () {
            module.verbose('Allowing hover events on popup to prevent closing');
            if ($popup && module.has.popup()) {
              $popup.on('mouseenter' + eventNamespace, module.event.start).on('mouseleave' + eventNamespace, module.event.end);
            }
          },
          close: function () {
            if (settings.hideOnScroll === true || settings.hideOnScroll == 'auto' && settings.on != 'click') {
              $scrollContext.one(module.get.scrollEvent() + elementNamespace, module.event.hideGracefully);
            }
            if (settings.on == 'hover' && openedWithTouch) {
              module.verbose('Binding popup close event to document');
              $document.on('touchstart' + elementNamespace, function (event) {
                module.verbose('Touched away from popup');
                module.event.hideGracefully.call(element, event);
              });
            }
            if (settings.on == 'click' && settings.closable) {
              module.verbose('Binding popup close event to document');
              $document.on('click' + elementNamespace, function (event) {
                module.verbose('Clicked away from popup');
                module.event.hideGracefully.call(element, event);
              });
            }
          }
        },

        unbind: {
          events: function () {
            $window.off(elementNamespace);
            $module.off(eventNamespace);
          },
          close: function () {
            $document.off(elementNamespace);
            $scrollContext.off(elementNamespace);
          }
        },

        has: {
          popup: function () {
            return $popup && $popup.length > 0;
          }
        },

        is: {
          offstage: function (distanceFromBoundary, position) {
            var offstage = [];
            // return boundaries that have been surpassed
            $.each(distanceFromBoundary, function (direction, distance) {
              if (distance < -settings.jitter) {
                module.debug('Position exceeds allowable distance from edge', direction, distance, position);
                offstage.push(direction);
              }
            });
            if (offstage.length > 0) {
              return true;
            } else {
              return false;
            }
          },
          svg: function (element) {
            return module.supports.svg() && element instanceof SVGGraphicsElement;
          },
          active: function () {
            return $module.hasClass(className.active);
          },
          animating: function () {
            return $popup !== undefined && $popup.hasClass(className.animating);
          },
          fluid: function () {
            return $popup !== undefined && $popup.hasClass(className.fluid);
          },
          visible: function () {
            return $popup !== undefined && $popup.hasClass(className.visible);
          },
          dropdown: function () {
            return $module.hasClass(className.dropdown);
          },
          hidden: function () {
            return !module.is.visible();
          },
          rtl: function () {
            return $module.css('direction') == 'rtl';
          }
        },

        reset: function () {
          module.remove.visible();
          if (settings.preserve) {
            if ($.fn.transition !== undefined) {
              $popup.transition('remove transition');
            }
          } else {
            module.removePopup();
          }
        },

        setting: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            settings[name] = value;
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };

      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.popup.settings = {

    name: 'Popup',

    // module settings
    silent: false,
    debug: false,
    verbose: false,
    performance: true,
    namespace: 'popup',

    // whether it should use dom mutation observers
    observeChanges: true,

    // callback only when element added to dom
    onCreate: function () {},

    // callback before element removed from dom
    onRemove: function () {},

    // callback before show animation
    onShow: function () {},

    // callback after show animation
    onVisible: function () {},

    // callback before hide animation
    onHide: function () {},

    // callback when popup cannot be positioned in visible screen
    onUnplaceable: function () {},

    // callback after hide animation
    onHidden: function () {},

    // when to show popup
    on: 'hover',

    // element to use to determine if popup is out of boundary
    boundary: window,

    // whether to add touchstart events when using hover
    addTouchEvents: true,

    // default position relative to element
    position: 'top left',

    // name of variation to use
    variation: '',

    // whether popup should be moved to context
    movePopup: true,

    // element which popup should be relative to
    target: false,

    // jq selector or element that should be used as popup
    popup: false,

    // popup should remain inline next to activator
    inline: false,

    // popup should be removed from page on hide
    preserve: false,

    // popup should not close when being hovered on
    hoverable: false,

    // explicitly set content
    content: false,

    // explicitly set html
    html: false,

    // explicitly set title
    title: false,

    // whether automatically close on clickaway when on click
    closable: true,

    // automatically hide on scroll
    hideOnScroll: 'auto',

    // hide other popups on show
    exclusive: false,

    // context to attach popups
    context: 'body',

    // context for binding scroll events
    scrollContext: window,

    // position to prefer when calculating new position
    prefer: 'opposite',

    // specify position to appear even if it doesn't fit
    lastResort: false,

    // delay used to prevent accidental refiring of animations due to user error
    delay: {
      show: 50,
      hide: 70
    },

    // whether fluid variation should assign width explicitly
    setFluidWidth: true,

    // transition settings
    duration: 200,
    transition: 'scale',

    // distance away from activating element in px
    distanceAway: 0,

    // number of pixels an element is allowed to be "offstage" for a position to be chosen (allows for rounding)
    jitter: 2,

    // offset on aligning axis from calculated position
    offset: 0,

    // maximum times to look for a position before failing (9 positions total)
    maxSearchDepth: 15,

    error: {
      invalidPosition: 'The position you specified is not a valid position',
      cannotPlace: 'Popup does not fit within the boundaries of the viewport',
      method: 'The method you called is not defined.',
      noTransition: 'This module requires ui transitions <https://github.com/Semantic-Org/UI-Transition>',
      notFound: 'The target or popup you specified does not exist on the page'
    },

    metadata: {
      activator: 'activator',
      content: 'content',
      html: 'html',
      offset: 'offset',
      position: 'position',
      title: 'title',
      variation: 'variation'
    },

    className: {
      active: 'active',
      animating: 'animating',
      dropdown: 'dropdown',
      fluid: 'fluid',
      loading: 'loading',
      popup: 'ui popup',
      position: 'top left center bottom right',
      visible: 'visible'
    },

    selector: {
      popup: '.ui.popup'
    },

    templates: {
      escape: function (string) {
        var badChars = /[&<>"'`]/g,
            shouldEscape = /[&<>"'`]/,
            escape = {
          "&": "&amp;",
          "<": "&lt;",
          ">": "&gt;",
          '"': "&quot;",
          "'": "&#x27;",
          "`": "&#x60;"
        },
            escapedChar = function (chr) {
          return escape[chr];
        };
        if (shouldEscape.test(string)) {
          return string.replace(badChars, escapedChar);
        }
        return string;
      },
      popup: function (text) {
        var html = '',
            escape = $.fn.popup.settings.templates.escape;
        if (typeof text !== undefined) {
          if (typeof text.title !== undefined && text.title) {
            text.title = escape(text.title);
            html += '<div class="header">' + text.title + '</div>';
          }
          if (typeof text.content !== undefined && text.content) {
            text.content = escape(text.content);
            html += '<div class="content">' + text.content + '</div>';
          }
        }
        return html;
      }
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL3BvcHVwLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFVQSxDQUFDLENBQUMsVUFBVSxDQUFWLEVBQWEsTUFBYixFQUFxQixRQUFyQixFQUErQixTQUEvQixFQUEwQzs7QUFFNUM7O0FBRUEsV0FBVSxPQUFPLE1BQVAsSUFBaUIsV0FBakIsSUFBZ0MsT0FBTyxJQUFQLElBQWUsSUFBaEQsR0FDTCxNQURLLEdBRUosT0FBTyxJQUFQLElBQWUsV0FBZixJQUE4QixLQUFLLElBQUwsSUFBYSxJQUE1QyxHQUNFLElBREYsR0FFRSxTQUFTLGFBQVQsR0FKTjs7QUFPQSxJQUFFLEVBQUYsQ0FBSyxLQUFMLEdBQWEsVUFBUyxVQUFULEVBQXFCO0FBQ2hDLFFBQ0UsY0FBaUIsRUFBRSxJQUFGLENBRG5CO0FBQUEsUUFFRSxZQUFpQixFQUFFLFFBQUYsQ0FGbkI7QUFBQSxRQUdFLFVBQWlCLEVBQUUsTUFBRixDQUhuQjtBQUFBLFFBSUUsUUFBaUIsRUFBRSxNQUFGLENBSm5CO0FBQUEsUUFNRSxpQkFBaUIsWUFBWSxRQUFaLElBQXdCLEVBTjNDO0FBQUEsUUFRRSxXQUFrQixJQVJwQjtBQUFBLFFBU0UsT0FBaUIsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQVRuQjtBQUFBLFFBVUUsY0FBaUIsRUFWbkI7QUFBQSxRQVlFLFFBQWlCLFVBQVUsQ0FBVixDQVpuQjtBQUFBLFFBYUUsZ0JBQWtCLE9BQU8sS0FBUCxJQUFnQixRQWJwQztBQUFBLFFBY0UsaUJBQWlCLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLENBQXpCLENBZG5CO0FBQUEsUUFnQkUsYUFoQkY7QUFrQkEsZ0JBQ0csSUFESCxDQUNRLFlBQVc7QUFDZixVQUNFLFdBQW9CLEVBQUUsYUFBRixDQUFnQixVQUFoQixDQUFGLEdBQ2QsRUFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUIsRUFBRSxFQUFGLENBQUssS0FBTCxDQUFXLFFBQTlCLEVBQXdDLFVBQXhDLENBRGMsR0FFZCxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsRUFBRSxFQUFGLENBQUssS0FBTCxDQUFXLFFBQXhCLENBSE47QUFBQSxVQUtFLFdBQXFCLFNBQVMsUUFMaEM7QUFBQSxVQU1FLFlBQXFCLFNBQVMsU0FOaEM7QUFBQSxVQU9FLFFBQXFCLFNBQVMsS0FQaEM7QUFBQSxVQVFFLFdBQXFCLFNBQVMsUUFSaEM7QUFBQSxVQVNFLFlBQXFCLFNBQVMsU0FUaEM7QUFBQSxVQVdFLGlCQUFxQixNQUFNLFNBQVMsU0FYdEM7QUFBQSxVQVlFLGtCQUFxQixZQUFZLFNBWm5DO0FBQUEsVUFjRSxVQUFxQixFQUFFLElBQUYsQ0FkdkI7QUFBQSxVQWVFLFdBQXFCLEVBQUUsU0FBUyxPQUFYLENBZnZCO0FBQUEsVUFnQkUsaUJBQXFCLEVBQUUsU0FBUyxhQUFYLENBaEJ2QjtBQUFBLFVBaUJFLFlBQXFCLEVBQUUsU0FBUyxRQUFYLENBakJ2QjtBQUFBLFVBa0JFLFVBQXNCLFNBQVMsTUFBVixHQUNqQixFQUFFLFNBQVMsTUFBWCxDQURpQixHQUVqQixPQXBCTjtBQUFBLFVBc0JFLE1BdEJGO0FBQUEsVUF1QkUsYUF2QkY7QUFBQSxVQXlCRSxjQUFxQixDQXpCdkI7QUFBQSxVQTBCRSxpQkFBcUIsS0ExQnZCO0FBQUEsVUEyQkUsa0JBQXFCLEtBM0J2QjtBQUFBLFVBNkJFLFVBQXFCLElBN0J2QjtBQUFBLFVBOEJFLFdBQXFCLFFBQVEsSUFBUixDQUFhLGVBQWIsQ0E5QnZCO0FBQUEsVUFnQ0UsZ0JBaENGO0FBQUEsVUFpQ0UsZ0JBakNGO0FBQUEsVUFrQ0UsRUFsQ0Y7QUFBQSxVQW1DRSxNQW5DRjs7QUFzQ0EsZUFBUzs7O0FBR1Asb0JBQVksWUFBVztBQUNyQixpQkFBTyxLQUFQLENBQWEsY0FBYixFQUE2QixPQUE3QjtBQUNBLGlCQUFPLFFBQVA7QUFDQSxpQkFBTyxJQUFQLENBQVksTUFBWjtBQUNBLGNBQUcsQ0FBQyxPQUFPLE1BQVAsRUFBRCxJQUFvQixTQUFTLFFBQWhDLEVBQTBDO0FBQ3hDLG1CQUFPLE1BQVA7QUFDRDtBQUNELGlCQUFPLGNBQVA7QUFDQSxpQkFBTyxXQUFQO0FBQ0QsU0FaTTs7QUFjUCxxQkFBYSxZQUFXO0FBQ3RCLGlCQUFPLE9BQVAsQ0FBZSxrQkFBZixFQUFtQyxNQUFuQztBQUNBLHFCQUFXLE1BQVg7QUFDQSxrQkFDRyxJQURILENBQ1EsZUFEUixFQUN5QixRQUR6QjtBQUdELFNBcEJNOztBQXNCUCx3QkFBZ0IsWUFBVztBQUN6QixjQUFHLHNCQUFzQixNQUF6QixFQUFpQztBQUMvQiwrQkFBbUIsSUFBSSxnQkFBSixDQUFxQixPQUFPLEtBQVAsQ0FBYSxlQUFsQyxDQUFuQjtBQUNBLDZCQUFpQixPQUFqQixDQUF5QixRQUF6QixFQUFtQztBQUNqQyx5QkFBWSxJQURxQjtBQUVqQyx1QkFBWTtBQUZxQixhQUFuQztBQUlBLG1CQUFPLEtBQVAsQ0FBYSw4QkFBYixFQUE2QyxnQkFBN0M7QUFDRDtBQUNGLFNBL0JNOztBQWlDUCxpQkFBUyxZQUFXO0FBQ2xCLGNBQUcsU0FBUyxLQUFaLEVBQW1CO0FBQ2pCLHFCQUFTLEVBQUUsU0FBUyxLQUFYLEVBQWtCLEVBQWxCLENBQXFCLENBQXJCLENBQVQ7QUFDRCxXQUZELE1BR0s7QUFDSCxnQkFBRyxTQUFTLE1BQVosRUFBb0I7QUFDbEIsdUJBQVMsUUFBUSxPQUFSLENBQWdCLFNBQVMsS0FBekIsRUFBZ0MsRUFBaEMsQ0FBbUMsQ0FBbkMsQ0FBVDtBQUNBLHVCQUFTLEtBQVQsR0FBaUIsTUFBakI7QUFDRDtBQUNGO0FBQ0QsY0FBRyxTQUFTLEtBQVosRUFBbUI7QUFDakIsbUJBQU8sUUFBUCxDQUFnQixVQUFVLE9BQTFCO0FBQ0EsNEJBQWdCLE9BQU8sR0FBUCxDQUFXLFlBQVgsRUFBaEI7QUFDQSxtQkFBTyxXQUFQLENBQW1CLFVBQVUsT0FBN0I7QUFDQSxnQkFBRyxTQUFTLFNBQVQsSUFBc0IsT0FBTyxHQUFQLENBQVcsS0FBWCxFQUF0QixJQUE0QyxPQUFPLEdBQVAsQ0FBVyxZQUFYLENBQXdCLE1BQXhCLEVBQWdDLENBQWhDLE1BQXVDLGNBQWMsQ0FBZCxDQUF0RixFQUF3RztBQUN0RyxxQkFBTyxLQUFQLENBQWEsOERBQWI7QUFDQSxxQkFDRyxNQURILEdBRUcsUUFGSCxDQUVZLGFBRlo7QUFJRDtBQUNGLFdBWEQsTUFZSztBQUNILDRCQUFpQixTQUFTLE1BQVYsR0FDWixPQUFPLEdBQVAsQ0FBVyxZQUFYLENBQXdCLE9BQXhCLENBRFksR0FFWixPQUFPLEdBQVAsQ0FBVyxLQUFYLEtBQ0UsT0FBTyxHQUFQLENBQVcsWUFBWCxDQUF3QixNQUF4QixDQURGLEdBRUUsS0FKTjtBQU1EO0FBQ0QsY0FBSSxjQUFjLEVBQWQsQ0FBaUIsTUFBakIsS0FBNEIsY0FBYyxDQUFkLE1BQXFCLE1BQU0sQ0FBTixDQUFyRCxFQUFnRTtBQUM5RCxtQkFBTyxLQUFQLENBQWEsK0JBQWI7QUFDQSw0QkFBZ0IsS0FBaEI7QUFDRDtBQUNELGNBQUksT0FBTyxHQUFQLENBQVcsU0FBWCxFQUFKLEVBQTZCO0FBQzNCLG1CQUFPLEdBQVAsQ0FBVyxTQUFYO0FBQ0Q7QUFDRixTQXRFTTs7QUF3RVAsb0JBQVksWUFBVztBQUNyQixpQkFBTyxPQUFQO0FBQ0EsaUJBQU8sR0FBUCxDQUFXLFFBQVg7QUFDRCxTQTNFTTs7QUE2RVAsaUJBQVMsWUFBVztBQUNsQixpQkFBTyxLQUFQLENBQWEsNEJBQWI7QUFDQSxjQUFHLGdCQUFILEVBQXFCO0FBQ25CLDZCQUFpQixVQUFqQjtBQUNEOztBQUVELGNBQUcsVUFBVSxDQUFDLFNBQVMsUUFBdkIsRUFBaUM7QUFDL0IsbUJBQU8sV0FBUDtBQUNEOztBQUVELHVCQUFhLE9BQU8sU0FBcEI7QUFDQSx1QkFBYSxPQUFPLFNBQXBCOztBQUVBLGlCQUFPLE1BQVAsQ0FBYyxLQUFkO0FBQ0EsaUJBQU8sTUFBUCxDQUFjLE1BQWQ7QUFDQSxrQkFDRyxVQURILENBQ2MsZUFEZDtBQUdELFNBL0ZNOztBQWlHUCxlQUFPO0FBQ0wsaUJBQVEsVUFBUyxLQUFULEVBQWdCO0FBQ3RCLGdCQUNFLFFBQVMsRUFBRSxhQUFGLENBQWdCLFNBQVMsS0FBekIsQ0FBRCxHQUNKLFNBQVMsS0FBVCxDQUFlLElBRFgsR0FFSixTQUFTLEtBSGY7QUFLQSx5QkFBYSxPQUFPLFNBQXBCO0FBQ0EsZ0JBQUcsQ0FBQyxlQUFKLEVBQXFCO0FBQ25CLHFCQUFPLFNBQVAsR0FBbUIsV0FBVyxPQUFPLElBQWxCLEVBQXdCLEtBQXhCLENBQW5CO0FBQ0Q7QUFDRixXQVhJO0FBWUwsZUFBTSxZQUFXO0FBQ2YsZ0JBQ0UsUUFBUyxFQUFFLGFBQUYsQ0FBZ0IsU0FBUyxLQUF6QixDQUFELEdBQ0osU0FBUyxLQUFULENBQWUsSUFEWCxHQUVKLFNBQVMsS0FIZjtBQUtBLHlCQUFhLE9BQU8sU0FBcEI7QUFDQSxtQkFBTyxTQUFQLEdBQW1CLFdBQVcsT0FBTyxJQUFsQixFQUF3QixLQUF4QixDQUFuQjtBQUNELFdBcEJJO0FBcUJMLHNCQUFZLFVBQVMsS0FBVCxFQUFnQjtBQUMxQiw4QkFBa0IsSUFBbEI7QUFDQSxtQkFBTyxJQUFQO0FBQ0QsV0F4Qkk7QUF5Qkwsa0JBQVEsWUFBVztBQUNqQixnQkFBSSxPQUFPLEVBQVAsQ0FBVSxPQUFWLEVBQUosRUFBMEI7QUFDeEIscUJBQU8sR0FBUCxDQUFXLFFBQVg7QUFDRDtBQUNGLFdBN0JJO0FBOEJMLDJCQUFpQixVQUFTLFNBQVQsRUFBb0I7QUFDbkMsZUFBRyxPQUFILENBQVcsSUFBWCxDQUFnQixTQUFoQixFQUEyQixVQUFTLFFBQVQsRUFBbUI7QUFDNUMsa0JBQUcsU0FBUyxZQUFaLEVBQTBCO0FBQ3hCLG1CQUFHLE9BQUgsQ0FBVyxJQUFYLENBQWdCLFNBQVMsWUFBekIsRUFBdUMsVUFBUyxJQUFULEVBQWU7QUFDcEQsc0JBQUcsUUFBUSxPQUFSLElBQW1CLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxPQUFiLEVBQXNCLE1BQXRCLEdBQStCLENBQXJELEVBQXdEO0FBQ3RELDJCQUFPLEtBQVAsQ0FBYSwrQ0FBYjtBQUNBLDJCQUFPLE9BQVA7QUFDRDtBQUNGLGlCQUxEO0FBTUQ7QUFDRixhQVREO0FBVUQsV0F6Q0k7QUEwQ0wsMEJBQWdCLFVBQVMsS0FBVCxFQUFnQjtBQUM5QixnQkFDRSxVQUFVLEVBQUUsTUFBTSxNQUFSLENBRFo7QUFBQSxnQkFFRSxVQUFVLEVBQUUsUUFBRixDQUFXLFNBQVMsZUFBcEIsRUFBcUMsTUFBTSxNQUEzQyxDQUZaO0FBQUEsZ0JBR0UsVUFBVyxRQUFRLE9BQVIsQ0FBZ0IsU0FBUyxLQUF6QixFQUFnQyxNQUFoQyxHQUF5QyxDQUh0RDs7QUFNQSxnQkFBRyxTQUFTLENBQUMsT0FBVixJQUFxQixPQUF4QixFQUFpQztBQUMvQixxQkFBTyxLQUFQLENBQWEsMkNBQWI7QUFDQSxxQkFBTyxJQUFQO0FBQ0QsYUFIRCxNQUlLO0FBQ0gscUJBQU8sS0FBUCxDQUFhLDRDQUFiO0FBQ0Q7QUFDRjtBQXhESSxTQWpHQTs7O0FBNkpQLGdCQUFRLFlBQVc7QUFDakIsY0FDRSxPQUFZLE9BQU8sR0FBUCxDQUFXLElBQVgsRUFEZDtBQUFBLGNBRUUsUUFBWSxPQUFPLEdBQVAsQ0FBVyxLQUFYLEVBRmQ7QUFBQSxjQUdFLFVBQVksT0FBTyxHQUFQLENBQVcsT0FBWCxFQUhkOztBQU1BLGNBQUcsUUFBUSxPQUFSLElBQW1CLEtBQXRCLEVBQTZCO0FBQzNCLG1CQUFPLEtBQVAsQ0FBYSxzQkFBYjtBQUNBLGdCQUFHLENBQUMsSUFBSixFQUFVO0FBQ1IscUJBQU8sU0FBUyxTQUFULENBQW1CLEtBQW5CLENBQXlCO0FBQzlCLHVCQUFVLEtBRG9CO0FBRTlCLHlCQUFVO0FBRm9CLGVBQXpCLENBQVA7QUFJRDtBQUNELHFCQUFTLEVBQUUsUUFBRixFQUNOLFFBRE0sQ0FDRyxVQUFVLEtBRGIsRUFFTixJQUZNLENBRUQsU0FBUyxTQUZSLEVBRW1CLE9BRm5CLEVBR04sSUFITSxDQUdELElBSEMsQ0FBVDtBQUtBLGdCQUFHLFNBQVMsTUFBWixFQUFvQjtBQUNsQixxQkFBTyxPQUFQLENBQWUsZ0NBQWYsRUFBaUQsTUFBakQ7QUFDQSxxQkFDRyxXQURILENBQ2UsT0FEZjtBQUdELGFBTEQsTUFNSztBQUNILHFCQUFPLE9BQVAsQ0FBZSxpQ0FBZixFQUFrRCxNQUFsRDtBQUNBLHFCQUNHLFFBREgsQ0FDYSxRQURiO0FBR0Q7QUFDRCxtQkFBTyxPQUFQO0FBQ0EsbUJBQU8sR0FBUCxDQUFXLFNBQVg7O0FBRUEsZ0JBQUcsU0FBUyxTQUFaLEVBQXVCO0FBQ3JCLHFCQUFPLElBQVAsQ0FBWSxLQUFaO0FBQ0Q7QUFDRCxxQkFBUyxRQUFULENBQWtCLElBQWxCLENBQXVCLE1BQXZCLEVBQStCLE9BQS9CO0FBQ0QsV0FoQ0QsTUFpQ0ssSUFBRyxRQUFRLElBQVIsQ0FBYSxTQUFTLEtBQXRCLEVBQTZCLE1BQTdCLEtBQXdDLENBQTNDLEVBQThDO0FBQ2pELG1CQUFPLE9BQVAsQ0FBZSwwQkFBZjtBQUNBLHFCQUFTLE1BQVQsR0FBa0IsSUFBbEI7QUFDQSxxQkFBUyxLQUFULEdBQWtCLFFBQVEsSUFBUixDQUFhLFNBQVMsS0FBdEIsRUFBNkIsSUFBN0IsQ0FBa0MsU0FBUyxTQUEzQyxFQUFzRCxPQUF0RCxDQUFsQjtBQUNBLG1CQUFPLE9BQVA7QUFDQSxnQkFBRyxTQUFTLFNBQVosRUFBdUI7QUFDckIscUJBQU8sSUFBUCxDQUFZLEtBQVo7QUFDRDtBQUNGLFdBUkksTUFTQSxJQUFHLFNBQVMsS0FBWixFQUFtQjtBQUN0QixjQUFFLFNBQVMsS0FBWCxFQUFrQixJQUFsQixDQUF1QixTQUFTLFNBQWhDLEVBQTJDLE9BQTNDO0FBQ0EsbUJBQU8sT0FBUCxDQUFlLGtDQUFmO0FBQ0EsbUJBQU8sT0FBUDtBQUNBLGdCQUFHLFNBQVMsU0FBWixFQUF1QjtBQUNyQixxQkFBTyxJQUFQLENBQVksS0FBWjtBQUNEO0FBQ0YsV0FQSSxNQVFBO0FBQ0gsbUJBQU8sS0FBUCxDQUFhLHVDQUFiLEVBQXNELE9BQXREO0FBQ0Q7QUFDRixTQXpOTTs7QUEyTlAsa0JBQVUsWUFBVztBQUNuQixlQUFLLENBQUMsS0FBSyxNQUFMLEdBQWMsUUFBZCxDQUF1QixFQUF2QixJQUE2QixXQUE5QixFQUEyQyxNQUEzQyxDQUFrRCxDQUFsRCxFQUFxRCxDQUFyRCxDQUFMO0FBQ0EsNkJBQW1CLE1BQU0sRUFBekI7QUFDQSxpQkFBTyxPQUFQLENBQWUsZ0NBQWYsRUFBaUQsRUFBakQ7QUFDRCxTQS9OTTs7O0FBa09QLGdCQUFRLFlBQVc7QUFDakIsaUJBQU8sS0FBUCxDQUFhLGlCQUFiO0FBQ0EsY0FBSSxPQUFPLEVBQVAsQ0FBVSxNQUFWLEVBQUosRUFBeUI7QUFDdkIsbUJBQU8sS0FBUCxDQUFhLGlDQUFiO0FBQ0EsbUJBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDQSxtQkFBTyxJQUFQO0FBQ0QsV0FKRCxNQUtLO0FBQ0gsbUJBQU8sS0FBUCxDQUFhLGlDQUFiO0FBQ0EsbUJBQU8sSUFBUDtBQUNEO0FBQ0YsU0E3T007O0FBK09QLGNBQU0sVUFBUyxRQUFULEVBQW1CO0FBQ3ZCLHFCQUFXLFlBQVksWUFBVSxDQUFFLENBQW5DO0FBQ0EsaUJBQU8sS0FBUCxDQUFhLGdCQUFiLEVBQStCLFNBQVMsVUFBeEM7QUFDQSxjQUFHLE9BQU8sRUFBUCxDQUFVLE1BQVYsTUFBc0IsRUFBRyxPQUFPLEVBQVAsQ0FBVSxNQUFWLE1BQXNCLE9BQU8sRUFBUCxDQUFVLFFBQVYsRUFBekIsQ0FBekIsRUFBMEU7QUFDeEUsZ0JBQUksQ0FBQyxPQUFPLE1BQVAsRUFBTCxFQUF1QjtBQUNyQixxQkFBTyxNQUFQO0FBQ0Q7QUFDRCxnQkFBRyxTQUFTLE1BQVQsQ0FBZ0IsSUFBaEIsQ0FBcUIsTUFBckIsRUFBNkIsT0FBN0IsTUFBMEMsS0FBN0MsRUFBb0Q7QUFDbEQscUJBQU8sS0FBUCxDQUFhLDREQUFiO0FBQ0E7QUFDRCxhQUhELE1BSUssSUFBRyxDQUFDLFNBQVMsUUFBVixJQUFzQixDQUFDLFNBQVMsS0FBbkMsRUFBMEM7QUFDN0MscUJBQU8sT0FBUDtBQUNEO0FBQ0QsZ0JBQUksVUFBVSxPQUFPLEdBQVAsQ0FBVyxRQUFYLEVBQWQsRUFBc0M7QUFDcEMscUJBQU8sSUFBUCxDQUFZLFVBQVo7QUFDQSxrQkFBRyxTQUFTLFNBQVosRUFBdUI7QUFDckIsdUJBQU8sT0FBUDtBQUNEO0FBQ0QscUJBQU8sT0FBUCxDQUFlLElBQWYsQ0FBb0IsUUFBcEI7QUFDRDtBQUNGO0FBQ0YsU0FyUU07O0FBd1FQLGNBQU0sVUFBUyxRQUFULEVBQW1CO0FBQ3ZCLHFCQUFXLFlBQVksWUFBVSxDQUFFLENBQW5DO0FBQ0EsY0FBSSxPQUFPLEVBQVAsQ0FBVSxPQUFWLE1BQXVCLE9BQU8sRUFBUCxDQUFVLFNBQVYsRUFBM0IsRUFBbUQ7QUFDakQsZ0JBQUcsU0FBUyxNQUFULENBQWdCLElBQWhCLENBQXFCLE1BQXJCLEVBQTZCLE9BQTdCLE1BQTBDLEtBQTdDLEVBQW9EO0FBQ2xELHFCQUFPLEtBQVAsQ0FBYSw0REFBYjtBQUNBO0FBQ0Q7QUFDRCxtQkFBTyxNQUFQLENBQWMsT0FBZDtBQUNBLG1CQUFPLE1BQVAsQ0FBYyxLQUFkO0FBQ0EsbUJBQU8sT0FBUCxDQUFlLFVBQWY7QUFDQSxtQkFBTyxPQUFQLENBQWUsSUFBZixDQUFvQixRQUFwQjtBQUNEO0FBQ0YsU0FwUk07O0FBc1JQLGlCQUFTLFlBQVc7QUFDbEIsWUFBRSxTQUFTLEtBQVgsRUFDRyxNQURILENBQ1UsTUFBTSxVQUFVLE9BRDFCLEVBRUcsSUFGSCxDQUVRLFlBQVc7QUFDZixjQUFFLElBQUYsRUFDRyxJQURILENBQ1EsU0FBUyxTQURqQixFQUVLLEtBRkwsQ0FFVyxNQUZYO0FBSUQsV0FQSDtBQVNELFNBaFNNO0FBaVNQLGdCQUFRLFlBQVc7QUFDakIsY0FBRyxDQUFDLE1BQUosRUFBWTtBQUNWLG1CQUFPLEtBQVA7QUFDRDtBQUNELGNBQUcsU0FBUyxNQUFULElBQW1CLFNBQVMsS0FBL0IsRUFBc0M7QUFDcEMsbUJBQVMsT0FBTyxHQUFQLENBQVcsS0FBWCxFQUFUO0FBQ0QsV0FGRCxNQUdLO0FBQ0gsbUJBQVMsT0FBTyxPQUFQLENBQWUsUUFBZixFQUF5QixNQUF6QixJQUFtQyxDQUFyQyxHQUNILElBREcsR0FFSCxLQUZKO0FBSUQ7QUFDRixTQTlTTTs7QUFnVFAscUJBQWEsWUFBVztBQUN0QixjQUFJLE9BQU8sR0FBUCxDQUFXLEtBQVgsTUFBc0IsQ0FBQyxTQUFTLEtBQXBDLEVBQTJDO0FBQ3pDLG1CQUFPLEtBQVAsQ0FBYSxnQkFBYixFQUErQixNQUEvQjtBQUNBLG1CQUFPLE1BQVA7QUFDQSxxQkFBUyxTQUFUO0FBQ0EscUJBQVMsUUFBVCxDQUFrQixJQUFsQixDQUF1QixNQUF2QixFQUErQixPQUEvQjtBQUNEO0FBQ0YsU0F2VE07O0FBeVRQLGNBQU07QUFDSixzQkFBWSxZQUFXO0FBQ3JCLG1CQUFPLEtBQVAsR0FBZTtBQUNiLHFCQUFPLFFBQVEsSUFBUixDQUFhLE9BQWI7QUFETSxhQUFmO0FBR0EsZ0JBQUksT0FBTyxLQUFQLENBQWEsS0FBakIsRUFBd0I7QUFDdEIsc0JBQVEsVUFBUixDQUFtQixPQUFuQjtBQUNEO0FBQ0QsbUJBQU8sT0FBUCxDQUFlLDRCQUFmLEVBQTZDLE9BQU8sS0FBUCxDQUFhLEtBQTFEO0FBQ0Q7QUFURyxTQXpUQztBQW9VUCxpQkFBUztBQUNQLHNCQUFZLFlBQVc7QUFDckIsZ0JBQUcsT0FBTyxLQUFQLElBQWdCLE9BQU8sS0FBUCxDQUFhLEtBQWhDLEVBQXVDO0FBQ3JDLHNCQUFRLElBQVIsQ0FBYSxPQUFiLEVBQXNCLE9BQU8sS0FBUCxDQUFhLEtBQW5DO0FBQ0EscUJBQU8sT0FBUCxDQUFlLCtCQUFmLEVBQWdELE9BQU8sS0FBUCxDQUFhLEtBQTdEO0FBQ0Q7QUFDRCxtQkFBTyxJQUFQO0FBQ0Q7QUFQTSxTQXBVRjtBQTZVUCxrQkFBVTtBQUNSLGVBQUssWUFBVztBQUNkLG1CQUFRLE9BQU8sa0JBQVAsS0FBOEIsU0FBdEM7QUFDRDtBQUhPLFNBN1VIO0FBa1ZQLGlCQUFTO0FBQ1AsZ0JBQU0sVUFBUyxRQUFULEVBQW1CO0FBQ3ZCLHVCQUFXLEVBQUUsVUFBRixDQUFhLFFBQWIsSUFBeUIsUUFBekIsR0FBb0MsWUFBVSxDQUFFLENBQTNEO0FBQ0EsZ0JBQUcsU0FBUyxVQUFULElBQXVCLEVBQUUsRUFBRixDQUFLLFVBQUwsS0FBb0IsU0FBM0MsSUFBd0QsUUFBUSxVQUFSLENBQW1CLGNBQW5CLENBQTNELEVBQStGO0FBQzdGLHFCQUFPLEdBQVAsQ0FBVyxPQUFYO0FBQ0EscUJBQ0csVUFESCxDQUNjO0FBQ1YsMkJBQWEsU0FBUyxVQUFULEdBQXNCLEtBRHpCO0FBRVYsdUJBQWEsS0FGSDtBQUdWLHVCQUFhLFNBQVMsS0FIWjtBQUlWLHlCQUFhLFNBQVMsT0FKWjtBQUtWLDBCQUFhLFNBQVMsUUFMWjtBQU1WLDRCQUFhLFlBQVc7QUFDdEIseUJBQU8sSUFBUCxDQUFZLEtBQVo7QUFDQSwyQkFBUyxJQUFULENBQWMsTUFBZCxFQUFzQixPQUF0QjtBQUNBLDJCQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsTUFBeEIsRUFBZ0MsT0FBaEM7QUFDRDtBQVZTLGVBRGQ7QUFjRCxhQWhCRCxNQWlCSztBQUNILHFCQUFPLEtBQVAsQ0FBYSxNQUFNLFlBQW5CO0FBQ0Q7QUFDRixXQXZCTTtBQXdCUCxnQkFBTSxVQUFTLFFBQVQsRUFBbUI7QUFDdkIsdUJBQVcsRUFBRSxVQUFGLENBQWEsUUFBYixJQUF5QixRQUF6QixHQUFvQyxZQUFVLENBQUUsQ0FBM0Q7QUFDQSxtQkFBTyxLQUFQLENBQWEsZUFBYjtBQUNBLGdCQUFHLFNBQVMsTUFBVCxDQUFnQixJQUFoQixDQUFxQixNQUFyQixFQUE2QixPQUE3QixNQUEwQyxLQUE3QyxFQUFvRDtBQUNsRCxxQkFBTyxLQUFQLENBQWEsNERBQWI7QUFDQTtBQUNEO0FBQ0QsZ0JBQUcsU0FBUyxVQUFULElBQXVCLEVBQUUsRUFBRixDQUFLLFVBQUwsS0FBb0IsU0FBM0MsSUFBd0QsUUFBUSxVQUFSLENBQW1CLGNBQW5CLENBQTNELEVBQStGO0FBQzdGLHFCQUNHLFVBREgsQ0FDYztBQUNWLDJCQUFhLFNBQVMsVUFBVCxHQUFzQixNQUR6QjtBQUVWLHVCQUFhLEtBRkg7QUFHViwwQkFBYSxTQUFTLFFBSFo7QUFJVix1QkFBYSxTQUFTLEtBSlo7QUFLVix5QkFBYSxTQUFTLE9BTFo7QUFNViw0QkFBYSxZQUFXO0FBQ3RCLHlCQUFPLEtBQVA7QUFDQSwyQkFBUyxJQUFULENBQWMsTUFBZCxFQUFzQixPQUF0QjtBQUNBLDJCQUFTLFFBQVQsQ0FBa0IsSUFBbEIsQ0FBdUIsTUFBdkIsRUFBK0IsT0FBL0I7QUFDRDtBQVZTLGVBRGQ7QUFjRCxhQWZELE1BZ0JLO0FBQ0gscUJBQU8sS0FBUCxDQUFhLE1BQU0sWUFBbkI7QUFDRDtBQUNGO0FBbERNLFNBbFZGOztBQXVZUCxnQkFBUTtBQUNOLG1CQUFTLFVBQVMsSUFBVCxFQUFlO0FBQ3RCLG1CQUFPLElBQVAsQ0FBWSxJQUFaO0FBQ0Q7QUFISyxTQXZZRDs7QUE2WVAsYUFBSztBQUNILGdCQUFNLFlBQVc7QUFDZixvQkFBUSxVQUFSLENBQW1CLFNBQVMsSUFBNUI7QUFDQSxtQkFBTyxRQUFRLElBQVIsQ0FBYSxTQUFTLElBQXRCLEtBQStCLFNBQVMsSUFBL0M7QUFDRCxXQUpFO0FBS0gsaUJBQU8sWUFBVztBQUNoQixvQkFBUSxVQUFSLENBQW1CLFNBQVMsS0FBNUI7QUFDQSxtQkFBTyxRQUFRLElBQVIsQ0FBYSxTQUFTLEtBQXRCLEtBQWdDLFNBQVMsS0FBaEQ7QUFDRCxXQVJFO0FBU0gsbUJBQVMsWUFBVztBQUNsQixvQkFBUSxVQUFSLENBQW1CLFNBQVMsT0FBNUI7QUFDQSxtQkFBTyxRQUFRLElBQVIsQ0FBYSxTQUFTLE9BQXRCLEtBQWtDLFFBQVEsSUFBUixDQUFhLE9BQWIsQ0FBbEMsSUFBMkQsU0FBUyxPQUEzRTtBQUNELFdBWkU7QUFhSCxxQkFBVyxZQUFXO0FBQ3BCLG9CQUFRLFVBQVIsQ0FBbUIsU0FBUyxTQUE1QjtBQUNBLG1CQUFPLFFBQVEsSUFBUixDQUFhLFNBQVMsU0FBdEIsS0FBb0MsU0FBUyxTQUFwRDtBQUNELFdBaEJFO0FBaUJILGlCQUFPLFlBQVc7QUFDaEIsbUJBQU8sTUFBUDtBQUNELFdBbkJFO0FBb0JILHVCQUFhLFlBQVc7QUFDdEIsbUJBQU8sT0FBTyxNQUFQLEVBQVA7QUFDRCxXQXRCRTtBQXVCSCx3QkFBYyxZQUFXO0FBQ3ZCLGdCQUNFLGdCQUFtQixRQUFRLENBQVIsQ0FEckI7QUFBQSxnQkFFRSxXQUFvQixVQUFVLENBQVYsS0FBZ0IsTUFGdEM7QUFBQSxnQkFHRSxpQkFBb0IsU0FBUyxNQUFULElBQW9CLFNBQVMsS0FBVCxJQUFrQixTQUFTLFNBQWhELEdBQ2YsUUFBUSxRQUFSLEVBRGUsR0FFZixRQUFRLE1BQVIsRUFMTjtBQUFBLGdCQU1FLGlCQUFrQixRQUFELEdBQ2IsRUFBRSxLQUFLLENBQVAsRUFBVSxNQUFNLENBQWhCLEVBRGEsR0FFYixVQUFVLE1BQVYsRUFSTjtBQUFBLGdCQVNFLGVBQWlCLEVBVG5CO0FBQUEsZ0JBVUUsU0FBVSxRQUFELEdBQ0wsRUFBRSxLQUFLLFFBQVEsU0FBUixFQUFQLEVBQTRCLE1BQU0sUUFBUSxVQUFSLEVBQWxDLEVBREssR0FFTCxFQUFFLEtBQUssQ0FBUCxFQUFVLE1BQU0sQ0FBaEIsRUFaTjtBQUFBLGdCQWFFLE1BYkY7QUFlQSwyQkFBZTs7QUFFYixzQkFBUztBQUNQLHlCQUFVLFFBQVEsQ0FBUixDQURIO0FBRVAsdUJBQVUsUUFBUSxVQUFSLEVBRkg7QUFHUCx3QkFBVSxRQUFRLFdBQVIsRUFISDtBQUlQLHFCQUFVLGVBQWUsR0FKbEI7QUFLUCxzQkFBVSxlQUFlLElBTGxCO0FBTVAsd0JBQVU7QUFOSCxlQUZJOztBQVdiLHFCQUFRO0FBQ04sdUJBQVMsT0FBTyxVQUFQLEVBREg7QUFFTix3QkFBUyxPQUFPLFdBQVA7QUFGSCxlQVhLOztBQWdCYixzQkFBUztBQUNQLHVCQUFTLGNBQWMsVUFBZCxFQURGO0FBRVAsd0JBQVMsY0FBYyxXQUFkO0FBRkYsZUFoQkk7O0FBcUJiLHNCQUFTO0FBQ1AscUJBQU8sZUFBZSxHQURmO0FBRVAsc0JBQU8sZUFBZSxJQUZmO0FBR1Asd0JBQVE7QUFDTix1QkFBTyxPQUFPLEdBRFI7QUFFTix3QkFBTyxPQUFPO0FBRlIsaUJBSEQ7QUFPUCx1QkFBUyxVQUFVLEtBQVYsRUFQRjtBQVFQLHdCQUFTLFVBQVUsTUFBVjtBQVJGO0FBckJJLGFBQWY7OztBQWtDQSxnQkFBSSxTQUFTLGFBQVQsSUFBMEIsT0FBTyxFQUFQLENBQVUsS0FBVixFQUE5QixFQUFrRDtBQUNoRCwyQkFBYSxTQUFiLEdBQXlCO0FBQ3ZCLHVCQUFPLE9BQU8sTUFBUCxHQUFnQixVQUFoQjtBQURnQixlQUF6QjtBQUdBLDJCQUFhLEtBQWIsQ0FBbUIsS0FBbkIsR0FBMkIsYUFBYSxTQUFiLENBQXVCLEtBQWxEO0FBQ0Q7OztBQUdELHlCQUFhLE1BQWIsQ0FBb0IsTUFBcEIsQ0FBMkIsR0FBM0IsR0FBa0MsU0FBUyxNQUFWLEdBQzdCLFNBQVUsT0FBTyxnQkFBUCxDQUF3QixhQUF4QixFQUF1QyxnQkFBdkMsQ0FBd0QsWUFBeEQsQ0FBVixFQUFpRixFQUFqRixDQUQ2QixHQUU3QixDQUZKO0FBSUEseUJBQWEsTUFBYixDQUFvQixNQUFwQixDQUEyQixJQUEzQixHQUFtQyxTQUFTLE1BQVYsR0FDOUIsT0FBTyxFQUFQLENBQVUsR0FBVixLQUNFLFNBQVUsT0FBTyxnQkFBUCxDQUF3QixhQUF4QixFQUF1QyxnQkFBdkMsQ0FBd0QsY0FBeEQsQ0FBVixFQUFtRixFQUFuRixDQURGLEdBRUUsU0FBVSxPQUFPLGdCQUFQLENBQXdCLGFBQXhCLEVBQXVDLGdCQUF2QyxDQUF3RCxhQUF4RCxDQUFWLEVBQWtGLEVBQWxGLENBSDRCLEdBSTlCLENBSko7O0FBT0EscUJBQVMsYUFBYSxNQUF0QjtBQUNBLHlCQUFhLFFBQWIsR0FBd0I7QUFDdEIsbUJBQVMsT0FBTyxHQUFQLEdBQWEsT0FBTyxNQUFQLENBQWMsR0FEZDtBQUV0QixzQkFBUyxPQUFPLEdBQVAsR0FBYSxPQUFPLE1BQVAsQ0FBYyxHQUEzQixHQUFpQyxPQUFPLE1BRjNCO0FBR3RCLG9CQUFTLE9BQU8sSUFBUCxHQUFjLE9BQU8sTUFBUCxDQUFjLElBSGY7QUFJdEIscUJBQVMsT0FBTyxJQUFQLEdBQWMsT0FBTyxNQUFQLENBQWMsSUFBNUIsR0FBbUMsT0FBTztBQUo3QixhQUF4QjtBQU1BLG1CQUFPLFlBQVA7QUFDRCxXQXBHRTtBQXFHSCxjQUFJLFlBQVc7QUFDYixtQkFBTyxFQUFQO0FBQ0QsV0F2R0U7QUF3R0gsc0JBQVksWUFBVztBQUNyQixnQkFBRyxTQUFTLEVBQVQsSUFBZSxPQUFsQixFQUEyQjtBQUN6QixxQkFBTyxZQUFQO0FBQ0QsYUFGRCxNQUdLLElBQUcsU0FBUyxFQUFULElBQWUsT0FBbEIsRUFBMkI7QUFDOUIscUJBQU8sT0FBUDtBQUNEO0FBQ0QsbUJBQU8sS0FBUDtBQUNELFdBaEhFO0FBaUhILHVCQUFhLFlBQVc7QUFDdEIsbUJBQU8sUUFBUDtBQUNELFdBbkhFO0FBb0hILG9CQUFVLFlBQVc7QUFDbkIsZ0JBQUcsU0FBUyxFQUFULElBQWUsT0FBbEIsRUFBMkI7QUFDekIscUJBQU8sWUFBUDtBQUNELGFBRkQsTUFHSyxJQUFHLFNBQVMsRUFBVCxJQUFlLE9BQWxCLEVBQTJCO0FBQzlCLHFCQUFPLE1BQVA7QUFDRDtBQUNELG1CQUFPLEtBQVA7QUFDRCxXQTVIRTtBQTZISCxnQ0FBc0IsVUFBUyxNQUFULEVBQWlCLFlBQWpCLEVBQStCO0FBQ25ELGdCQUNFLHVCQUF1QixFQUR6QjtBQUFBLGdCQUVFLEtBRkY7QUFBQSxnQkFHRSxRQUhGO0FBS0EsMkJBQWUsZ0JBQWdCLE9BQU8sR0FBUCxDQUFXLFlBQVgsRUFBL0I7OztBQUdBLG9CQUFlLGFBQWEsS0FBNUI7QUFDQSx1QkFBZSxhQUFhLFFBQTVCOztBQUVBLGdCQUFHLE1BQUgsRUFBVztBQUNULHFDQUF1QjtBQUNyQixxQkFBVSxPQUFPLEdBQVAsR0FBYSxTQUFTLEdBRFg7QUFFckIsc0JBQVUsT0FBTyxJQUFQLEdBQWMsU0FBUyxJQUZaO0FBR3JCLHVCQUFVLFNBQVMsS0FBVCxJQUFrQixPQUFPLElBQVAsR0FBYyxNQUFNLEtBQXRDLENBSFc7QUFJckIsd0JBQVUsU0FBUyxNQUFULElBQW1CLE9BQU8sR0FBUCxHQUFhLE1BQU0sTUFBdEM7QUFKVyxlQUF2QjtBQU1BLHFCQUFPLE9BQVAsQ0FBZSxxQ0FBZixFQUFzRCxNQUF0RCxFQUE4RCxvQkFBOUQ7QUFDRDtBQUNELG1CQUFPLG9CQUFQO0FBQ0QsV0FuSkU7QUFvSkgsd0JBQWMsVUFBUyxPQUFULEVBQWtCO0FBQzlCLGdCQUNFLFVBQVcsWUFBWSxTQUFiLEdBQ04sUUFBUSxDQUFSLENBRE0sR0FFTixRQUFRLENBQVIsQ0FITjtBQUFBLGdCQUlFLGFBQWEsUUFBUSxVQUp2QjtBQUFBLGdCQUtFLFFBQVcsRUFBRSxVQUFGLENBTGI7QUFPQSxnQkFBRyxVQUFILEVBQWU7QUFDYixrQkFDRSxPQUFZLE1BQU0sR0FBTixDQUFVLFdBQVYsTUFBMkIsTUFEekM7QUFBQSxrQkFFRSxXQUFZLE1BQU0sR0FBTixDQUFVLFVBQVYsTUFBMEIsUUFGeEM7QUFBQSxrQkFHRSxTQUFXLE1BQU0sRUFBTixDQUFTLE1BQVQsQ0FIYjtBQUtBLHFCQUFNLGNBQWMsQ0FBQyxNQUFmLElBQXlCLFFBQXpCLElBQXFDLElBQTNDLEVBQWlEO0FBQy9DLDZCQUFhLFdBQVcsVUFBeEI7QUFDQSx3QkFBVyxFQUFFLFVBQUYsQ0FBWDtBQUNBLHVCQUFZLE1BQU0sR0FBTixDQUFVLFdBQVYsTUFBMkIsTUFBdkM7QUFDQSwyQkFBWSxNQUFNLEdBQU4sQ0FBVSxVQUFWLE1BQTBCLFFBQXRDO0FBQ0EseUJBQVcsTUFBTSxFQUFOLENBQVMsTUFBVCxDQUFYO0FBQ0Q7QUFDRjtBQUNELG1CQUFRLFNBQVMsTUFBTSxNQUFOLEdBQWUsQ0FBekIsR0FDSCxLQURHLEdBRUgsR0FGSjtBQUlELFdBOUtFO0FBK0tILHFCQUFXLFlBQVc7QUFDcEIsbUJBQU87QUFDTCwwQkFBa0IsS0FEYjtBQUVMLDRCQUFrQixLQUZiO0FBR0wsMkJBQWtCLEtBSGI7QUFJTCw2QkFBa0IsS0FKYjtBQUtMLCtCQUFrQixLQUxiO0FBTUwsOEJBQWtCLEtBTmI7QUFPTCw2QkFBa0IsS0FQYjtBQVFMLDhCQUFrQjtBQVJiLGFBQVA7QUFVRCxXQTFMRTtBQTJMSCx3QkFBYyxVQUFTLFFBQVQsRUFBbUI7QUFDL0IsZ0JBQ0UsWUFBcUIsU0FBUyxLQUFULENBQWUsR0FBZixDQUR2QjtBQUFBLGdCQUVFLG1CQUFxQixVQUFVLENBQVYsQ0FGdkI7QUFBQSxnQkFHRSxxQkFBcUIsVUFBVSxDQUFWLENBSHZCO0FBQUEsZ0JBSUUsV0FBVztBQUNULG1CQUFTLFFBREE7QUFFVCxzQkFBUyxLQUZBO0FBR1Qsb0JBQVMsT0FIQTtBQUlULHFCQUFTO0FBSkEsYUFKYjtBQUFBLGdCQVVFLFdBQVc7QUFDVCxvQkFBUyxRQURBO0FBRVQsc0JBQVMsT0FGQTtBQUdULHFCQUFTO0FBSEEsYUFWYjtBQUFBLGdCQWVFLFNBQVM7QUFDUCwwQkFBa0IsWUFEWDtBQUVQLDRCQUFrQixXQUZYO0FBR1AsMkJBQWtCLGNBSFg7QUFJUCw4QkFBa0IsY0FKWDtBQUtQLDhCQUFrQixlQUxYO0FBTVAsK0JBQWtCLGFBTlg7QUFPUCw2QkFBa0IsYUFQWDtBQVFQLDZCQUFrQjtBQVJYLGFBZlg7QUFBQSxnQkF5QkUscUJBQXNCLG9CQUFvQixLQUFwQixJQUE2QixvQkFBb0IsUUF6QnpFO0FBQUEsZ0JBMEJFLGdCQUFnQixLQTFCbEI7QUFBQSxnQkEyQkUsZ0JBQWdCLEtBM0JsQjtBQUFBLGdCQTRCRSxlQUFnQixLQTVCbEI7QUE4QkEsZ0JBQUcsQ0FBQyxjQUFKLEVBQW9CO0FBQ2xCLHFCQUFPLE9BQVAsQ0FBZSxtQ0FBZjtBQUNBLCtCQUFpQixPQUFPLEdBQVAsQ0FBVyxTQUFYLEVBQWpCO0FBQ0Q7O0FBRUQsbUJBQU8sS0FBUCxDQUFhLCtCQUFiLEVBQThDLFFBQTlDO0FBQ0EsMkJBQWUsUUFBZixJQUEyQixJQUEzQjs7QUFFQSxnQkFBRyxTQUFTLE1BQVQsS0FBb0IsVUFBdkIsRUFBbUM7QUFDakMsNkJBQWdCLENBQUMsU0FBUyxnQkFBVCxDQUFELEVBQTZCLGtCQUE3QixDQUFoQjtBQUNBLDZCQUFnQixhQUFhLElBQWIsQ0FBa0IsR0FBbEIsQ0FBaEI7QUFDQSw4QkFBaUIsZUFBZSxZQUFmLE1BQWlDLElBQWxEO0FBQ0EscUJBQU8sS0FBUCxDQUFhLDBCQUFiLEVBQXlDLFlBQXpDO0FBQ0Q7QUFDRCxnQkFBSSxTQUFTLE1BQVQsS0FBb0IsVUFBckIsSUFBb0Msa0JBQXZDLEVBQTREO0FBQzFELDZCQUFnQixDQUFDLGdCQUFELEVBQW1CLFNBQVMsa0JBQVQsQ0FBbkIsQ0FBaEI7QUFDQSw2QkFBZ0IsYUFBYSxJQUFiLENBQWtCLEdBQWxCLENBQWhCO0FBQ0EsOEJBQWlCLGVBQWUsWUFBZixNQUFpQyxJQUFsRDtBQUNBLHFCQUFPLEtBQVAsQ0FBYSwwQkFBYixFQUF5QyxZQUF6QztBQUNEO0FBQ0QsZ0JBQUcsaUJBQWlCLGFBQXBCLEVBQW1DO0FBQ2pDLHFCQUFPLEtBQVAsQ0FBYSx1QkFBYixFQUFzQyxZQUF0QztBQUNBLDZCQUFlLE9BQU8sUUFBUCxDQUFmO0FBQ0Q7QUFDRCxtQkFBTyxZQUFQO0FBQ0Q7QUFuUEUsU0E3WUU7O0FBbW9CUCxhQUFLO0FBQ0gsb0JBQVUsVUFBUyxRQUFULEVBQW1CLFlBQW5CLEVBQWlDOzs7QUFHekMsZ0JBQUcsUUFBUSxNQUFSLEtBQW1CLENBQW5CLElBQXdCLE9BQU8sTUFBUCxLQUFrQixDQUE3QyxFQUFnRDtBQUM5QyxxQkFBTyxLQUFQLENBQWEsTUFBTSxRQUFuQjtBQUNBO0FBQ0Q7QUFDRCxnQkFDRSxNQURGLEVBRUUsWUFGRixFQUdFLE1BSEYsRUFJRSxLQUpGLEVBS0UsTUFMRixFQU1FLFdBTkYsRUFPRSxXQVBGLEVBUUUsb0JBUkY7O0FBV0EsMkJBQWUsZ0JBQWdCLE9BQU8sR0FBUCxDQUFXLFlBQVgsRUFBL0I7QUFDQSx1QkFBZSxZQUFnQixRQUFRLElBQVIsQ0FBYSxTQUFTLFFBQXRCLENBQWhCLElBQW1ELFNBQVMsUUFBM0U7O0FBRUEscUJBQWUsUUFBUSxJQUFSLENBQWEsU0FBUyxNQUF0QixLQUFpQyxTQUFTLE1BQXpEO0FBQ0EsMkJBQWUsU0FBUyxZQUF4Qjs7O0FBR0EscUJBQVMsYUFBYSxNQUF0QjtBQUNBLG9CQUFTLGFBQWEsS0FBdEI7QUFDQSxxQkFBUyxhQUFhLE1BQXRCOztBQUVBLGdCQUFHLE9BQU8sS0FBUCxLQUFpQixDQUFqQixJQUFzQixPQUFPLE1BQVAsS0FBa0IsQ0FBeEMsSUFBNkMsQ0FBQyxPQUFPLEVBQVAsQ0FBVSxHQUFWLENBQWMsT0FBTyxPQUFyQixDQUFqRCxFQUFnRjtBQUM5RSxxQkFBTyxLQUFQLENBQWEseUNBQWI7QUFDQSxxQkFBTyxLQUFQO0FBQ0Q7O0FBRUQsZ0JBQUcsU0FBUyxNQUFaLEVBQW9CO0FBQ2xCLHFCQUFPLEtBQVAsQ0FBYSw4QkFBYixFQUE2QyxPQUFPLE1BQXBEO0FBQ0Esa0JBQUcsWUFBWSxhQUFaLElBQTZCLFlBQVksY0FBNUMsRUFBNEQ7QUFDMUQsMEJBQWlCLE9BQU8sTUFBUCxDQUFjLEdBQS9CO0FBQ0EsZ0NBQWdCLENBQUMsT0FBTyxNQUFQLENBQWMsSUFBL0I7QUFDRCxlQUhELE1BSUssSUFBSSxZQUFZLFVBQVosSUFBMEIsWUFBWSxZQUF0QyxJQUFzRCxZQUFZLFdBQXRFLEVBQW1GO0FBQ3RGLDBCQUFnQixPQUFPLE1BQVAsQ0FBYyxJQUE5QjtBQUNBLGdDQUFnQixPQUFPLE1BQVAsQ0FBYyxHQUE5QjtBQUNELGVBSEksTUFJQTtBQUNILDBCQUFnQixPQUFPLE1BQVAsQ0FBYyxJQUE5QjtBQUNBLGdDQUFnQixPQUFPLE1BQVAsQ0FBYyxHQUE5QjtBQUNEO0FBQ0Y7O0FBRUQsbUJBQU8sS0FBUCxDQUFhLDhDQUFiLEVBQTZELFFBQTdELEVBQXVFLFlBQXZFOztBQUVBLGdCQUFJLE9BQU8sRUFBUCxDQUFVLEdBQVYsRUFBSixFQUFxQjtBQUNuQix5QkFBVyxTQUFTLE9BQVQsQ0FBaUIsYUFBakIsRUFBZ0MsVUFBVSxLQUFWLEVBQWlCO0FBQzFELHVCQUFRLFNBQVMsTUFBVixHQUNILE9BREcsR0FFSCxNQUZKO0FBSUQsZUFMVSxDQUFYO0FBTUEscUJBQU8sS0FBUCxDQUFhLDZCQUFiLEVBQTRDLFFBQTVDO0FBQ0Q7OztBQUdELGdCQUFHLGVBQWUsU0FBUyxjQUF4QixJQUEwQyxPQUFPLFNBQVMsVUFBaEIsS0FBK0IsUUFBNUUsRUFBc0Y7QUFDcEYseUJBQVcsU0FBUyxVQUFwQjtBQUNEOztBQUVELG9CQUFRLFFBQVI7QUFDRSxtQkFBSyxVQUFMO0FBQ0UsOEJBQWM7QUFDWix1QkFBUyxNQURHO0FBRVosMEJBQVMsT0FBTyxNQUFQLEdBQWdCLE9BQU8sR0FBdkIsR0FBNkIsWUFGMUI7QUFHWix3QkFBUyxPQUFPLElBQVAsR0FBYyxNQUhYO0FBSVoseUJBQVM7QUFKRyxpQkFBZDtBQU1GO0FBQ0EsbUJBQUssWUFBTDtBQUNFLDhCQUFjO0FBQ1osMEJBQVMsT0FBTyxNQUFQLEdBQWdCLE9BQU8sR0FBdkIsR0FBNkIsWUFEMUI7QUFFWix3QkFBUyxPQUFPLElBQVAsR0FBZSxPQUFPLEtBQVAsR0FBZSxDQUE5QixHQUFvQyxNQUFNLEtBQU4sR0FBYyxDQUFsRCxHQUF1RCxNQUZwRDtBQUdaLHVCQUFTLE1BSEc7QUFJWix5QkFBUztBQUpHLGlCQUFkO0FBTUY7QUFDQSxtQkFBSyxXQUFMO0FBQ0UsOEJBQWM7QUFDWiwwQkFBVSxPQUFPLE1BQVAsR0FBZ0IsT0FBTyxHQUF2QixHQUE2QixZQUQzQjtBQUVaLHlCQUFVLE9BQU8sS0FBUCxHQUFlLE9BQU8sSUFBdEIsR0FBNkIsT0FBTyxLQUFwQyxHQUE0QyxNQUYxQztBQUdaLHVCQUFTLE1BSEc7QUFJWix3QkFBUztBQUpHLGlCQUFkO0FBTUY7QUFDQSxtQkFBSyxhQUFMO0FBQ0UsOEJBQWM7QUFDWix1QkFBUyxPQUFPLEdBQVAsR0FBYyxPQUFPLE1BQVAsR0FBZ0IsQ0FBOUIsR0FBb0MsTUFBTSxNQUFOLEdBQWUsQ0FBbkQsR0FBd0QsTUFEckQ7QUFFWix5QkFBUyxPQUFPLEtBQVAsR0FBZSxPQUFPLElBQXRCLEdBQTZCLFlBRjFCO0FBR1osd0JBQVMsTUFIRztBQUlaLDBCQUFTO0FBSkcsaUJBQWQ7QUFNRjtBQUNBLG1CQUFLLGNBQUw7QUFDRSw4QkFBYztBQUNaLHVCQUFTLE9BQU8sR0FBUCxHQUFjLE9BQU8sTUFBUCxHQUFnQixDQUE5QixHQUFvQyxNQUFNLE1BQU4sR0FBZSxDQUFuRCxHQUF3RCxNQURyRDtBQUVaLHdCQUFTLE9BQU8sSUFBUCxHQUFjLE9BQU8sS0FBckIsR0FBNkIsWUFGMUI7QUFHWiwwQkFBUyxNQUhHO0FBSVoseUJBQVM7QUFKRyxpQkFBZDtBQU1GO0FBQ0EsbUJBQUssYUFBTDtBQUNFLDhCQUFjO0FBQ1osdUJBQVMsT0FBTyxHQUFQLEdBQWEsT0FBTyxNQUFwQixHQUE2QixZQUQxQjtBQUVaLHdCQUFTLE9BQU8sSUFBUCxHQUFjLE1BRlg7QUFHWiwwQkFBUyxNQUhHO0FBSVoseUJBQVM7QUFKRyxpQkFBZDtBQU1GO0FBQ0EsbUJBQUssZUFBTDtBQUNFLDhCQUFjO0FBQ1osdUJBQVMsT0FBTyxHQUFQLEdBQWEsT0FBTyxNQUFwQixHQUE2QixZQUQxQjtBQUVaLHdCQUFTLE9BQU8sSUFBUCxHQUFlLE9BQU8sS0FBUCxHQUFlLENBQTlCLEdBQW9DLE1BQU0sS0FBTixHQUFjLENBQWxELEdBQXVELE1BRnBEO0FBR1osMEJBQVMsTUFIRztBQUlaLHlCQUFTO0FBSkcsaUJBQWQ7QUFNRjtBQUNBLG1CQUFLLGNBQUw7QUFDRSw4QkFBYztBQUNaLHVCQUFTLE9BQU8sR0FBUCxHQUFhLE9BQU8sTUFBcEIsR0FBNkIsWUFEMUI7QUFFWix5QkFBUyxPQUFPLEtBQVAsR0FBZSxPQUFPLElBQXRCLEdBQThCLE9BQU8sS0FBckMsR0FBNkMsTUFGMUM7QUFHWix3QkFBUyxNQUhHO0FBSVosMEJBQVM7QUFKRyxpQkFBZDtBQU1GO0FBaEVGO0FBa0VBLGdCQUFHLGdCQUFnQixTQUFuQixFQUE4QjtBQUM1QixxQkFBTyxLQUFQLENBQWEsTUFBTSxlQUFuQixFQUFvQyxRQUFwQztBQUNEOztBQUVELG1CQUFPLEtBQVAsQ0FBYSxxQ0FBYixFQUFvRCxXQUFwRDs7O0FBR0EsbUJBQ0csR0FESCxDQUNPLFdBRFAsRUFFRyxXQUZILENBRWUsVUFBVSxRQUZ6QixFQUdHLFFBSEgsQ0FHWSxRQUhaLEVBSUcsUUFKSCxDQUlZLFVBQVUsT0FKdEI7O0FBT0EsMEJBQWMsT0FBTyxHQUFQLENBQVcsV0FBWCxFQUFkOzs7QUFHQSxtQ0FBdUIsT0FBTyxHQUFQLENBQVcsb0JBQVgsQ0FBZ0MsV0FBaEMsRUFBNkMsWUFBN0MsQ0FBdkI7O0FBRUEsZ0JBQUksT0FBTyxFQUFQLENBQVUsUUFBVixDQUFtQixvQkFBbkIsRUFBeUMsUUFBekMsQ0FBSixFQUF5RDtBQUN2RCxxQkFBTyxLQUFQLENBQWEsOEJBQWIsRUFBNkMsUUFBN0M7QUFDQSxrQkFBRyxjQUFjLFNBQVMsY0FBMUIsRUFBMEM7QUFDeEM7QUFDQSwyQkFBVyxPQUFPLEdBQVAsQ0FBVyxZQUFYLENBQXdCLFFBQXhCLENBQVg7QUFDQSx1QkFBTyxLQUFQLENBQWEscUJBQWIsRUFBb0MsUUFBcEM7QUFDQSx1QkFBUSxNQUFELEdBQ0gsT0FBTyxHQUFQLENBQVcsUUFBWCxDQUFvQixRQUFwQixFQUE4QixZQUE5QixDQURHLEdBRUgsS0FGSjtBQUlELGVBUkQsTUFTSztBQUNILG9CQUFHLFNBQVMsVUFBWixFQUF3QjtBQUN0Qix5QkFBTyxLQUFQLENBQWEsK0NBQWI7QUFDRCxpQkFGRCxNQUdLO0FBQ0gseUJBQU8sS0FBUCxDQUFhLDRDQUFiLEVBQTJELE1BQTNEO0FBQ0EseUJBQU8sS0FBUCxDQUFhLE1BQU0sV0FBbkIsRUFBZ0MsT0FBaEM7QUFDQSx5QkFBTyxNQUFQLENBQWMsUUFBZDtBQUNBLHlCQUFPLE1BQVAsQ0FBYyxPQUFkO0FBQ0EseUJBQU8sS0FBUDtBQUNBLDJCQUFTLGFBQVQsQ0FBdUIsSUFBdkIsQ0FBNEIsTUFBNUIsRUFBb0MsT0FBcEM7QUFDQSx5QkFBTyxLQUFQO0FBQ0Q7QUFDRjtBQUNGO0FBQ0QsbUJBQU8sS0FBUCxDQUFhLHNCQUFiLEVBQXFDLFFBQXJDO0FBQ0EsbUJBQU8sTUFBUCxDQUFjLFFBQWQ7QUFDQSxtQkFBTyxNQUFQLENBQWMsT0FBZDtBQUNBLGdCQUFJLFNBQVMsYUFBVCxJQUEwQixPQUFPLEVBQVAsQ0FBVSxLQUFWLEVBQTlCLEVBQWtEO0FBQ2hELHFCQUFPLEdBQVAsQ0FBVyxVQUFYLENBQXNCLFlBQXRCO0FBQ0Q7QUFDRCxtQkFBTyxJQUFQO0FBQ0QsV0ExTEU7O0FBNExILHNCQUFZLFVBQVMsWUFBVCxFQUF1QjtBQUNqQywyQkFBZSxnQkFBZ0IsT0FBTyxHQUFQLENBQVcsWUFBWCxFQUEvQjtBQUNBLG1CQUFPLEtBQVAsQ0FBYSxxREFBYixFQUFvRSxhQUFhLE1BQWIsQ0FBb0IsS0FBeEY7QUFDQSxtQkFBTyxHQUFQLENBQVcsT0FBWCxFQUFvQixhQUFhLFNBQWIsQ0FBdUIsS0FBM0M7QUFDRCxXQWhNRTs7QUFrTUgscUJBQVcsVUFBUyxTQUFULEVBQW9CO0FBQzdCLHdCQUFZLGFBQWEsT0FBTyxHQUFQLENBQVcsU0FBWCxFQUF6QjtBQUNBLGdCQUFHLGFBQWEsT0FBTyxHQUFQLENBQVcsS0FBWCxFQUFoQixFQUFxQztBQUNuQyxxQkFBTyxPQUFQLENBQWUsMkJBQWYsRUFBNEMsU0FBNUM7QUFDQSxxQkFBTyxRQUFQLENBQWdCLFNBQWhCO0FBQ0Q7QUFDRixXQXhNRTs7QUEwTUgsbUJBQVMsWUFBVztBQUNsQixvQkFBUSxRQUFSLENBQWlCLFVBQVUsT0FBM0I7QUFDRDtBQTVNRSxTQW5vQkU7O0FBazFCUCxnQkFBUTtBQUNOLG1CQUFTLFlBQVc7QUFDbEIsbUJBQU8sV0FBUCxDQUFtQixVQUFVLE9BQTdCO0FBQ0QsV0FISztBQUlOLHFCQUFXLFVBQVMsU0FBVCxFQUFvQjtBQUM3Qix3QkFBWSxhQUFhLE9BQU8sR0FBUCxDQUFXLFNBQVgsRUFBekI7QUFDQSxnQkFBRyxTQUFILEVBQWM7QUFDWixxQkFBTyxPQUFQLENBQWUsb0JBQWYsRUFBcUMsU0FBckM7QUFDQSxxQkFBTyxXQUFQLENBQW1CLFNBQW5CO0FBQ0Q7QUFDRixXQVZLO0FBV04sbUJBQVMsWUFBVztBQUNsQixvQkFBUSxXQUFSLENBQW9CLFVBQVUsT0FBOUI7QUFDRCxXQWJLO0FBY04sb0JBQVUsWUFBVztBQUNuQixtQkFBTyxPQUFQLENBQWUsa0NBQWY7QUFDQSwwQkFBaUIsQ0FBakI7QUFDQSw2QkFBaUIsS0FBakI7QUFDRDtBQWxCSyxTQWwxQkQ7O0FBdTJCUCxjQUFNO0FBQ0osa0JBQVEsWUFBVztBQUNqQixtQkFBTyxLQUFQLENBQWEsZ0NBQWI7QUFDQSxnQkFBRyxTQUFTLEVBQVQsSUFBZSxPQUFsQixFQUEyQjtBQUN6QixzQkFDRyxFQURILENBQ00sVUFBVSxjQURoQixFQUNnQyxPQUFPLE1BRHZDO0FBR0Q7QUFDRCxnQkFBRyxTQUFTLEVBQVQsSUFBZSxPQUFmLElBQTBCLFFBQTdCLEVBQXVDO0FBQ3JDLHNCQUNHLEVBREgsQ0FDTSxlQUFlLGNBRHJCLEVBQ3FDLE9BQU8sS0FBUCxDQUFhLFVBRGxEO0FBR0Q7QUFDRCxnQkFBSSxPQUFPLEdBQVAsQ0FBVyxVQUFYLEVBQUosRUFBOEI7QUFDNUIsc0JBQ0csRUFESCxDQUNNLE9BQU8sR0FBUCxDQUFXLFVBQVgsS0FBMEIsY0FEaEMsRUFDZ0QsT0FBTyxLQUFQLENBQWEsS0FEN0QsRUFFRyxFQUZILENBRU0sT0FBTyxHQUFQLENBQVcsUUFBWCxLQUF3QixjQUY5QixFQUU4QyxPQUFPLEtBQVAsQ0FBYSxHQUYzRDtBQUlEO0FBQ0QsZ0JBQUcsU0FBUyxNQUFaLEVBQW9CO0FBQ2xCLHFCQUFPLEtBQVAsQ0FBYSx1QkFBYixFQUFzQyxPQUF0QztBQUNEO0FBQ0Qsb0JBQVEsRUFBUixDQUFXLFdBQVcsZ0JBQXRCLEVBQXdDLE9BQU8sS0FBUCxDQUFhLE1BQXJEO0FBQ0QsV0F2Qkc7QUF3QkosaUJBQU8sWUFBVztBQUNoQixtQkFBTyxPQUFQLENBQWUsbURBQWY7QUFDQSxnQkFBSSxVQUFVLE9BQU8sR0FBUCxDQUFXLEtBQVgsRUFBZCxFQUFtQztBQUNqQyxxQkFDRyxFQURILENBQ00sZUFBZSxjQURyQixFQUNxQyxPQUFPLEtBQVAsQ0FBYSxLQURsRCxFQUVHLEVBRkgsQ0FFTSxlQUFlLGNBRnJCLEVBRXFDLE9BQU8sS0FBUCxDQUFhLEdBRmxEO0FBSUQ7QUFDRixXQWhDRztBQWlDSixpQkFBTyxZQUFXO0FBQ2hCLGdCQUFHLFNBQVMsWUFBVCxLQUEwQixJQUExQixJQUFtQyxTQUFTLFlBQVQsSUFBeUIsTUFBekIsSUFBbUMsU0FBUyxFQUFULElBQWUsT0FBeEYsRUFBa0c7QUFDaEcsNkJBQ0csR0FESCxDQUNPLE9BQU8sR0FBUCxDQUFXLFdBQVgsS0FBMkIsZ0JBRGxDLEVBQ29ELE9BQU8sS0FBUCxDQUFhLGNBRGpFO0FBR0Q7QUFDRCxnQkFBRyxTQUFTLEVBQVQsSUFBZSxPQUFmLElBQTBCLGVBQTdCLEVBQThDO0FBQzVDLHFCQUFPLE9BQVAsQ0FBZSx1Q0FBZjtBQUNBLHdCQUNHLEVBREgsQ0FDTSxlQUFlLGdCQURyQixFQUN1QyxVQUFTLEtBQVQsRUFBZ0I7QUFDbkQsdUJBQU8sT0FBUCxDQUFlLHlCQUFmO0FBQ0EsdUJBQU8sS0FBUCxDQUFhLGNBQWIsQ0FBNEIsSUFBNUIsQ0FBaUMsT0FBakMsRUFBMEMsS0FBMUM7QUFDRCxlQUpIO0FBTUQ7QUFDRCxnQkFBRyxTQUFTLEVBQVQsSUFBZSxPQUFmLElBQTBCLFNBQVMsUUFBdEMsRUFBZ0Q7QUFDOUMscUJBQU8sT0FBUCxDQUFlLHVDQUFmO0FBQ0Esd0JBQ0csRUFESCxDQUNNLFVBQVUsZ0JBRGhCLEVBQ2tDLFVBQVMsS0FBVCxFQUFnQjtBQUM5Qyx1QkFBTyxPQUFQLENBQWUseUJBQWY7QUFDQSx1QkFBTyxLQUFQLENBQWEsY0FBYixDQUE0QixJQUE1QixDQUFpQyxPQUFqQyxFQUEwQyxLQUExQztBQUNELGVBSkg7QUFNRDtBQUNGO0FBekRHLFNBdjJCQzs7QUFtNkJQLGdCQUFRO0FBQ04sa0JBQVEsWUFBVztBQUNqQixvQkFDRyxHQURILENBQ08sZ0JBRFA7QUFHQSxvQkFDRyxHQURILENBQ08sY0FEUDtBQUdELFdBUks7QUFTTixpQkFBTyxZQUFXO0FBQ2hCLHNCQUNHLEdBREgsQ0FDTyxnQkFEUDtBQUdBLDJCQUNHLEdBREgsQ0FDTyxnQkFEUDtBQUdEO0FBaEJLLFNBbjZCRDs7QUFzN0JQLGFBQUs7QUFDSCxpQkFBTyxZQUFXO0FBQ2hCLG1CQUFRLFVBQVUsT0FBTyxNQUFQLEdBQWdCLENBQWxDO0FBQ0Q7QUFIRSxTQXQ3QkU7O0FBNDdCUCxZQUFJO0FBQ0Ysb0JBQVUsVUFBUyxvQkFBVCxFQUErQixRQUEvQixFQUF5QztBQUNqRCxnQkFDRSxXQUFXLEVBRGI7O0FBSUEsY0FBRSxJQUFGLENBQU8sb0JBQVAsRUFBNkIsVUFBUyxTQUFULEVBQW9CLFFBQXBCLEVBQThCO0FBQ3pELGtCQUFHLFdBQVcsQ0FBQyxTQUFTLE1BQXhCLEVBQWdDO0FBQzlCLHVCQUFPLEtBQVAsQ0FBYSwrQ0FBYixFQUE4RCxTQUE5RCxFQUF5RSxRQUF6RSxFQUFtRixRQUFuRjtBQUNBLHlCQUFTLElBQVQsQ0FBYyxTQUFkO0FBQ0Q7QUFDRixhQUxEO0FBTUEsZ0JBQUcsU0FBUyxNQUFULEdBQWtCLENBQXJCLEVBQXdCO0FBQ3RCLHFCQUFPLElBQVA7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxLQUFQO0FBQ0Q7QUFDRixXQWxCQztBQW1CRixlQUFLLFVBQVMsT0FBVCxFQUFrQjtBQUNyQixtQkFBTyxPQUFPLFFBQVAsQ0FBZ0IsR0FBaEIsTUFBMEIsbUJBQW1CLGtCQUFwRDtBQUNELFdBckJDO0FBc0JGLGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sUUFBUSxRQUFSLENBQWlCLFVBQVUsTUFBM0IsQ0FBUDtBQUNELFdBeEJDO0FBeUJGLHFCQUFXLFlBQVc7QUFDcEIsbUJBQVEsV0FBVyxTQUFYLElBQXdCLE9BQU8sUUFBUCxDQUFnQixVQUFVLFNBQTFCLENBQWhDO0FBQ0QsV0EzQkM7QUE0QkYsaUJBQU8sWUFBVztBQUNoQixtQkFBUSxXQUFXLFNBQVgsSUFBd0IsT0FBTyxRQUFQLENBQWdCLFVBQVUsS0FBMUIsQ0FBaEM7QUFDRCxXQTlCQztBQStCRixtQkFBUyxZQUFXO0FBQ2xCLG1CQUFRLFdBQVcsU0FBWCxJQUF3QixPQUFPLFFBQVAsQ0FBZ0IsVUFBVSxPQUExQixDQUFoQztBQUNELFdBakNDO0FBa0NGLG9CQUFVLFlBQVc7QUFDbkIsbUJBQU8sUUFBUSxRQUFSLENBQWlCLFVBQVUsUUFBM0IsQ0FBUDtBQUNELFdBcENDO0FBcUNGLGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sQ0FBQyxPQUFPLEVBQVAsQ0FBVSxPQUFWLEVBQVI7QUFDRCxXQXZDQztBQXdDRixlQUFLLFlBQVk7QUFDZixtQkFBTyxRQUFRLEdBQVIsQ0FBWSxXQUFaLEtBQTRCLEtBQW5DO0FBQ0Q7QUExQ0MsU0E1N0JHOztBQXkrQlAsZUFBTyxZQUFXO0FBQ2hCLGlCQUFPLE1BQVAsQ0FBYyxPQUFkO0FBQ0EsY0FBRyxTQUFTLFFBQVosRUFBc0I7QUFDcEIsZ0JBQUcsRUFBRSxFQUFGLENBQUssVUFBTCxLQUFvQixTQUF2QixFQUFrQztBQUNoQyxxQkFDRyxVQURILENBQ2MsbUJBRGQ7QUFHRDtBQUNGLFdBTkQsTUFPSztBQUNILG1CQUFPLFdBQVA7QUFDRDtBQUNGLFNBci9CTTs7QUF1L0JQLGlCQUFTLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDN0IsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsUUFBZixFQUF5QixJQUF6QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixxQkFBUyxJQUFULElBQWlCLEtBQWpCO0FBQ0QsV0FGSSxNQUdBO0FBQ0gsbUJBQU8sU0FBUyxJQUFULENBQVA7QUFDRDtBQUNGLFNBamdDTTtBQWtnQ1Asa0JBQVUsVUFBUyxJQUFULEVBQWUsS0FBZixFQUFzQjtBQUM5QixjQUFJLEVBQUUsYUFBRixDQUFnQixJQUFoQixDQUFKLEVBQTRCO0FBQzFCLGNBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxNQUFmLEVBQXVCLElBQXZCO0FBQ0QsV0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLG1CQUFPLElBQVAsSUFBZSxLQUFmO0FBQ0QsV0FGSSxNQUdBO0FBQ0gsbUJBQU8sT0FBTyxJQUFQLENBQVA7QUFDRDtBQUNGLFNBNWdDTTtBQTZnQ1AsZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQVYsSUFBb0IsU0FBUyxLQUFoQyxFQUF1QztBQUNyQyxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLEtBQVAsR0FBZSxTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxJQUFyQyxFQUEyQyxPQUEzQyxFQUFvRCxTQUFTLElBQVQsR0FBZ0IsR0FBcEUsQ0FBZjtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLE9BQW5CLEVBQTRCLFNBQTVCO0FBQ0Q7QUFDRjtBQUNGLFNBdmhDTTtBQXdoQ1AsaUJBQVMsWUFBVztBQUNsQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsT0FBN0IsSUFBd0MsU0FBUyxLQUFwRCxFQUEyRDtBQUN6RCxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLE9BQVAsR0FBaUIsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWpCO0FBQ0EscUJBQU8sT0FBUCxDQUFlLEtBQWYsQ0FBcUIsT0FBckIsRUFBOEIsU0FBOUI7QUFDRDtBQUNGO0FBQ0YsU0FsaUNNO0FBbWlDUCxlQUFPLFlBQVc7QUFDaEIsY0FBRyxDQUFDLFNBQVMsTUFBYixFQUFxQjtBQUNuQixtQkFBTyxLQUFQLEdBQWUsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsS0FBckMsRUFBNEMsT0FBNUMsRUFBcUQsU0FBUyxJQUFULEdBQWdCLEdBQXJFLENBQWY7QUFDQSxtQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNEO0FBQ0YsU0F4aUNNO0FBeWlDUCxxQkFBYTtBQUNYLGVBQUssVUFBUyxPQUFULEVBQWtCO0FBQ3JCLGdCQUNFLFdBREYsRUFFRSxhQUZGLEVBR0UsWUFIRjtBQUtBLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2Qiw0QkFBZ0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUFoQjtBQUNBLDZCQUFnQixRQUFRLFdBQXhCO0FBQ0EsOEJBQWdCLGNBQWMsWUFBOUI7QUFDQSxxQkFBZ0IsV0FBaEI7QUFDQSwwQkFBWSxJQUFaLENBQWlCO0FBQ2Ysd0JBQW1CLFFBQVEsQ0FBUixDQURKO0FBRWYsNkJBQW1CLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxPQUFkLEVBQXVCLENBQXZCLEtBQTZCLEVBRmpDO0FBR2YsMkJBQW1CLE9BSEo7QUFJZixrQ0FBbUI7QUFKSixlQUFqQjtBQU1EO0FBQ0QseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsbUJBQU8sV0FBUCxDQUFtQixLQUFuQixHQUEyQixXQUFXLE9BQU8sV0FBUCxDQUFtQixPQUE5QixFQUF1QyxHQUF2QyxDQUEzQjtBQUNELFdBckJVO0FBc0JYLG1CQUFTLFlBQVc7QUFDbEIsZ0JBQ0UsUUFBUSxTQUFTLElBQVQsR0FBZ0IsR0FEMUI7QUFBQSxnQkFFRSxZQUFZLENBRmQ7QUFJQSxtQkFBTyxLQUFQO0FBQ0EseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsY0FBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMkJBQWEsS0FBSyxnQkFBTCxDQUFiO0FBQ0QsYUFGRDtBQUdBLHFCQUFTLE1BQU0sU0FBTixHQUFrQixJQUEzQjtBQUNBLGdCQUFHLGNBQUgsRUFBbUI7QUFDakIsdUJBQVMsUUFBUSxjQUFSLEdBQXlCLElBQWxDO0FBQ0Q7QUFDRCxnQkFBSSxDQUFDLFFBQVEsS0FBUixLQUFrQixTQUFsQixJQUErQixRQUFRLEtBQVIsS0FBa0IsU0FBbEQsS0FBZ0UsWUFBWSxNQUFaLEdBQXFCLENBQXpGLEVBQTRGO0FBQzFGLHNCQUFRLGNBQVIsQ0FBdUIsS0FBdkI7QUFDQSxrQkFBRyxRQUFRLEtBQVgsRUFBa0I7QUFDaEIsd0JBQVEsS0FBUixDQUFjLFdBQWQ7QUFDRCxlQUZELE1BR0s7QUFDSCxrQkFBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMEJBQVEsR0FBUixDQUFZLEtBQUssTUFBTCxJQUFlLElBQWYsR0FBc0IsS0FBSyxnQkFBTCxDQUF0QixHQUE2QyxJQUF6RDtBQUNELGlCQUZEO0FBR0Q7QUFDRCxzQkFBUSxRQUFSO0FBQ0Q7QUFDRCwwQkFBYyxFQUFkO0FBQ0Q7QUFqRFUsU0F6aUNOO0FBNGxDUCxnQkFBUSxVQUFTLEtBQVQsRUFBZ0IsZUFBaEIsRUFBaUMsT0FBakMsRUFBMEM7QUFDaEQsY0FDRSxTQUFTLFFBRFg7QUFBQSxjQUVFLFFBRkY7QUFBQSxjQUdFLEtBSEY7QUFBQSxjQUlFLFFBSkY7QUFNQSw0QkFBa0IsbUJBQW1CLGNBQXJDO0FBQ0Esb0JBQWtCLFdBQW1CLE9BQXJDO0FBQ0EsY0FBRyxPQUFPLEtBQVAsSUFBZ0IsUUFBaEIsSUFBNEIsV0FBVyxTQUExQyxFQUFxRDtBQUNuRCxvQkFBVyxNQUFNLEtBQU4sQ0FBWSxPQUFaLENBQVg7QUFDQSx1QkFBVyxNQUFNLE1BQU4sR0FBZSxDQUExQjtBQUNBLGNBQUUsSUFBRixDQUFPLEtBQVAsRUFBYyxVQUFTLEtBQVQsRUFBZ0IsS0FBaEIsRUFBdUI7QUFDbkMsa0JBQUksaUJBQWtCLFNBQVMsUUFBVixHQUNqQixRQUFRLE1BQU0sUUFBUSxDQUFkLEVBQWlCLE1BQWpCLENBQXdCLENBQXhCLEVBQTJCLFdBQTNCLEVBQVIsR0FBbUQsTUFBTSxRQUFRLENBQWQsRUFBaUIsS0FBakIsQ0FBdUIsQ0FBdkIsQ0FEbEMsR0FFakIsS0FGSjtBQUlBLGtCQUFJLEVBQUUsYUFBRixDQUFpQixPQUFPLGNBQVAsQ0FBakIsS0FBOEMsU0FBUyxRQUEzRCxFQUF1RTtBQUNyRSx5QkFBUyxPQUFPLGNBQVAsQ0FBVDtBQUNELGVBRkQsTUFHSyxJQUFJLE9BQU8sY0FBUCxNQUEyQixTQUEvQixFQUEyQztBQUM5Qyx3QkFBUSxPQUFPLGNBQVAsQ0FBUjtBQUNBLHVCQUFPLEtBQVA7QUFDRCxlQUhJLE1BSUEsSUFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxLQUFQLENBQWpCLEtBQXFDLFNBQVMsUUFBbEQsRUFBOEQ7QUFDakUseUJBQVMsT0FBTyxLQUFQLENBQVQ7QUFDRCxlQUZJLE1BR0EsSUFBSSxPQUFPLEtBQVAsTUFBa0IsU0FBdEIsRUFBa0M7QUFDckMsd0JBQVEsT0FBTyxLQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBO0FBQ0gsdUJBQU8sS0FBUDtBQUNEO0FBQ0YsYUF0QkQ7QUF1QkQ7QUFDRCxjQUFLLEVBQUUsVUFBRixDQUFjLEtBQWQsQ0FBTCxFQUE2QjtBQUMzQix1QkFBVyxNQUFNLEtBQU4sQ0FBWSxPQUFaLEVBQXFCLGVBQXJCLENBQVg7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsdUJBQVcsS0FBWDtBQUNEO0FBQ0QsY0FBRyxFQUFFLE9BQUYsQ0FBVSxhQUFWLENBQUgsRUFBNkI7QUFDM0IsMEJBQWMsSUFBZCxDQUFtQixRQUFuQjtBQUNELFdBRkQsTUFHSyxJQUFHLGtCQUFrQixTQUFyQixFQUFnQztBQUNuQyw0QkFBZ0IsQ0FBQyxhQUFELEVBQWdCLFFBQWhCLENBQWhCO0FBQ0QsV0FGSSxNQUdBLElBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUM5Qiw0QkFBZ0IsUUFBaEI7QUFDRDtBQUNELGlCQUFPLEtBQVA7QUFDRDtBQWhwQ00sT0FBVDs7QUFtcENBLFVBQUcsYUFBSCxFQUFrQjtBQUNoQixZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsaUJBQU8sVUFBUDtBQUNEO0FBQ0QsZUFBTyxNQUFQLENBQWMsS0FBZDtBQUNELE9BTEQsTUFNSztBQUNILFlBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUN6QixtQkFBUyxNQUFULENBQWdCLFNBQWhCO0FBQ0Q7QUFDRCxlQUFPLFVBQVA7QUFDRDtBQUNGLEtBdnNDSDs7QUEwc0NBLFdBQVEsa0JBQWtCLFNBQW5CLEdBQ0gsYUFERyxHQUVILElBRko7QUFJRCxHQWp1Q0Q7O0FBbXVDQSxJQUFFLEVBQUYsQ0FBSyxLQUFMLENBQVcsUUFBWCxHQUFzQjs7QUFFcEIsVUFBaUIsT0FGRzs7O0FBS3BCLFlBQWlCLEtBTEc7QUFNcEIsV0FBaUIsS0FORztBQU9wQixhQUFpQixLQVBHO0FBUXBCLGlCQUFpQixJQVJHO0FBU3BCLGVBQWlCLE9BVEc7OztBQVlwQixvQkFBaUIsSUFaRzs7O0FBZXBCLGNBQWlCLFlBQVUsQ0FBRSxDQWZUOzs7QUFrQnBCLGNBQWlCLFlBQVUsQ0FBRSxDQWxCVDs7O0FBcUJwQixZQUFpQixZQUFVLENBQUUsQ0FyQlQ7OztBQXdCcEIsZUFBaUIsWUFBVSxDQUFFLENBeEJUOzs7QUEyQnBCLFlBQWlCLFlBQVUsQ0FBRSxDQTNCVDs7O0FBOEJwQixtQkFBaUIsWUFBVSxDQUFFLENBOUJUOzs7QUFpQ3BCLGNBQWlCLFlBQVUsQ0FBRSxDQWpDVDs7O0FBb0NwQixRQUFpQixPQXBDRzs7O0FBdUNwQixjQUFpQixNQXZDRzs7O0FBMENwQixvQkFBaUIsSUExQ0c7OztBQTZDcEIsY0FBaUIsVUE3Q0c7OztBQWdEcEIsZUFBaUIsRUFoREc7OztBQW1EcEIsZUFBaUIsSUFuREc7OztBQXNEcEIsWUFBaUIsS0F0REc7OztBQXlEcEIsV0FBaUIsS0F6REc7OztBQTREcEIsWUFBaUIsS0E1REc7OztBQStEcEIsY0FBaUIsS0EvREc7OztBQWtFcEIsZUFBaUIsS0FsRUc7OztBQXFFcEIsYUFBaUIsS0FyRUc7OztBQXdFcEIsVUFBaUIsS0F4RUc7OztBQTJFcEIsV0FBaUIsS0EzRUc7OztBQThFcEIsY0FBaUIsSUE5RUc7OztBQWlGcEIsa0JBQWlCLE1BakZHOzs7QUFvRnBCLGVBQWlCLEtBcEZHOzs7QUF1RnBCLGFBQWlCLE1BdkZHOzs7QUEwRnBCLG1CQUFpQixNQTFGRzs7O0FBNkZwQixZQUFpQixVQTdGRzs7O0FBZ0dwQixnQkFBaUIsS0FoR0c7OztBQW1HcEIsV0FBZTtBQUNiLFlBQU8sRUFETTtBQUViLFlBQU87QUFGTSxLQW5HSzs7O0FBeUdwQixtQkFBaUIsSUF6R0c7OztBQTRHcEIsY0FBaUIsR0E1R0c7QUE2R3BCLGdCQUFpQixPQTdHRzs7O0FBZ0hwQixrQkFBaUIsQ0FoSEc7OztBQW1IcEIsWUFBaUIsQ0FuSEc7OztBQXNIcEIsWUFBaUIsQ0F0SEc7OztBQXlIcEIsb0JBQWlCLEVBekhHOztBQTJIcEIsV0FBTztBQUNMLHVCQUFrQixvREFEYjtBQUVMLG1CQUFrQiwwREFGYjtBQUdMLGNBQWtCLHVDQUhiO0FBSUwsb0JBQWtCLHFGQUpiO0FBS0wsZ0JBQWtCO0FBTGIsS0EzSGE7O0FBbUlwQixjQUFVO0FBQ1IsaUJBQVksV0FESjtBQUVSLGVBQVksU0FGSjtBQUdSLFlBQVksTUFISjtBQUlSLGNBQVksUUFKSjtBQUtSLGdCQUFZLFVBTEo7QUFNUixhQUFZLE9BTko7QUFPUixpQkFBWTtBQVBKLEtBbklVOztBQTZJcEIsZUFBYztBQUNaLGNBQVksUUFEQTtBQUVaLGlCQUFZLFdBRkE7QUFHWixnQkFBWSxVQUhBO0FBSVosYUFBWSxPQUpBO0FBS1osZUFBWSxTQUxBO0FBTVosYUFBWSxVQU5BO0FBT1osZ0JBQVksOEJBUEE7QUFRWixlQUFZO0FBUkEsS0E3SU07O0FBd0pwQixjQUFjO0FBQ1osYUFBVztBQURDLEtBeEpNOztBQTRKcEIsZUFBVztBQUNULGNBQVEsVUFBUyxNQUFULEVBQWlCO0FBQ3ZCLFlBQ0UsV0FBZSxXQURqQjtBQUFBLFlBRUUsZUFBZSxVQUZqQjtBQUFBLFlBR0UsU0FBZTtBQUNiLGVBQUssT0FEUTtBQUViLGVBQUssTUFGUTtBQUdiLGVBQUssTUFIUTtBQUliLGVBQUssUUFKUTtBQUtiLGVBQUssUUFMUTtBQU1iLGVBQUs7QUFOUSxTQUhqQjtBQUFBLFlBV0UsY0FBZSxVQUFTLEdBQVQsRUFBYztBQUMzQixpQkFBTyxPQUFPLEdBQVAsQ0FBUDtBQUNELFNBYkg7QUFlQSxZQUFHLGFBQWEsSUFBYixDQUFrQixNQUFsQixDQUFILEVBQThCO0FBQzVCLGlCQUFPLE9BQU8sT0FBUCxDQUFlLFFBQWYsRUFBeUIsV0FBekIsQ0FBUDtBQUNEO0FBQ0QsZUFBTyxNQUFQO0FBQ0QsT0FyQlE7QUFzQlQsYUFBTyxVQUFTLElBQVQsRUFBZTtBQUNwQixZQUNFLE9BQVMsRUFEWDtBQUFBLFlBRUUsU0FBUyxFQUFFLEVBQUYsQ0FBSyxLQUFMLENBQVcsUUFBWCxDQUFvQixTQUFwQixDQUE4QixNQUZ6QztBQUlBLFlBQUcsT0FBTyxJQUFQLEtBQWdCLFNBQW5CLEVBQThCO0FBQzVCLGNBQUcsT0FBTyxLQUFLLEtBQVosS0FBc0IsU0FBdEIsSUFBbUMsS0FBSyxLQUEzQyxFQUFrRDtBQUNoRCxpQkFBSyxLQUFMLEdBQWEsT0FBTyxLQUFLLEtBQVosQ0FBYjtBQUNBLG9CQUFRLHlCQUF5QixLQUFLLEtBQTlCLEdBQXNDLFFBQTlDO0FBQ0Q7QUFDRCxjQUFHLE9BQU8sS0FBSyxPQUFaLEtBQXdCLFNBQXhCLElBQXFDLEtBQUssT0FBN0MsRUFBc0Q7QUFDcEQsaUJBQUssT0FBTCxHQUFlLE9BQU8sS0FBSyxPQUFaLENBQWY7QUFDQSxvQkFBUSwwQkFBMEIsS0FBSyxPQUEvQixHQUF5QyxRQUFqRDtBQUNEO0FBQ0Y7QUFDRCxlQUFPLElBQVA7QUFDRDtBQXRDUTs7QUE1SlMsR0FBdEI7QUF3TUMsQ0F0N0NBLEVBczdDRyxNQXQ3Q0gsRUFzN0NXLE1BdDdDWCxFQXM3Q21CLFFBdDdDbkIiLCJmaWxlIjoicG9wdXAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcbiAqICMgU2VtYW50aWMgVUkgLSBQb3B1cFxuICogaHR0cDovL2dpdGh1Yi5jb20vc2VtYW50aWMtb3JnL3NlbWFudGljLXVpL1xuICpcbiAqXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2VcbiAqIGh0dHA6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9NSVRcbiAqXG4gKi9cblxuOyhmdW5jdGlvbiAoJCwgd2luZG93LCBkb2N1bWVudCwgdW5kZWZpbmVkKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG53aW5kb3cgPSAodHlwZW9mIHdpbmRvdyAhPSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuTWF0aCA9PSBNYXRoKVxuICA/IHdpbmRvd1xuICA6ICh0eXBlb2Ygc2VsZiAhPSAndW5kZWZpbmVkJyAmJiBzZWxmLk1hdGggPT0gTWF0aClcbiAgICA/IHNlbGZcbiAgICA6IEZ1bmN0aW9uKCdyZXR1cm4gdGhpcycpKClcbjtcblxuJC5mbi5wb3B1cCA9IGZ1bmN0aW9uKHBhcmFtZXRlcnMpIHtcbiAgdmFyXG4gICAgJGFsbE1vZHVsZXMgICAgPSAkKHRoaXMpLFxuICAgICRkb2N1bWVudCAgICAgID0gJChkb2N1bWVudCksXG4gICAgJHdpbmRvdyAgICAgICAgPSAkKHdpbmRvdyksXG4gICAgJGJvZHkgICAgICAgICAgPSAkKCdib2R5JyksXG5cbiAgICBtb2R1bGVTZWxlY3RvciA9ICRhbGxNb2R1bGVzLnNlbGVjdG9yIHx8ICcnLFxuXG4gICAgaGFzVG91Y2ggICAgICAgPSAodHJ1ZSksXG4gICAgdGltZSAgICAgICAgICAgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKSxcbiAgICBwZXJmb3JtYW5jZSAgICA9IFtdLFxuXG4gICAgcXVlcnkgICAgICAgICAgPSBhcmd1bWVudHNbMF0sXG4gICAgbWV0aG9kSW52b2tlZCAgPSAodHlwZW9mIHF1ZXJ5ID09ICdzdHJpbmcnKSxcbiAgICBxdWVyeUFyZ3VtZW50cyA9IFtdLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSxcblxuICAgIHJldHVybmVkVmFsdWVcbiAgO1xuICAkYWxsTW9kdWxlc1xuICAgIC5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgdmFyXG4gICAgICAgIHNldHRpbmdzICAgICAgICA9ICggJC5pc1BsYWluT2JqZWN0KHBhcmFtZXRlcnMpIClcbiAgICAgICAgICA/ICQuZXh0ZW5kKHRydWUsIHt9LCAkLmZuLnBvcHVwLnNldHRpbmdzLCBwYXJhbWV0ZXJzKVxuICAgICAgICAgIDogJC5leHRlbmQoe30sICQuZm4ucG9wdXAuc2V0dGluZ3MpLFxuXG4gICAgICAgIHNlbGVjdG9yICAgICAgICAgICA9IHNldHRpbmdzLnNlbGVjdG9yLFxuICAgICAgICBjbGFzc05hbWUgICAgICAgICAgPSBzZXR0aW5ncy5jbGFzc05hbWUsXG4gICAgICAgIGVycm9yICAgICAgICAgICAgICA9IHNldHRpbmdzLmVycm9yLFxuICAgICAgICBtZXRhZGF0YSAgICAgICAgICAgPSBzZXR0aW5ncy5tZXRhZGF0YSxcbiAgICAgICAgbmFtZXNwYWNlICAgICAgICAgID0gc2V0dGluZ3MubmFtZXNwYWNlLFxuXG4gICAgICAgIGV2ZW50TmFtZXNwYWNlICAgICA9ICcuJyArIHNldHRpbmdzLm5hbWVzcGFjZSxcbiAgICAgICAgbW9kdWxlTmFtZXNwYWNlICAgID0gJ21vZHVsZS0nICsgbmFtZXNwYWNlLFxuXG4gICAgICAgICRtb2R1bGUgICAgICAgICAgICA9ICQodGhpcyksXG4gICAgICAgICRjb250ZXh0ICAgICAgICAgICA9ICQoc2V0dGluZ3MuY29udGV4dCksXG4gICAgICAgICRzY3JvbGxDb250ZXh0ICAgICA9ICQoc2V0dGluZ3Muc2Nyb2xsQ29udGV4dCksXG4gICAgICAgICRib3VuZGFyeSAgICAgICAgICA9ICQoc2V0dGluZ3MuYm91bmRhcnkpLFxuICAgICAgICAkdGFyZ2V0ICAgICAgICAgICAgPSAoc2V0dGluZ3MudGFyZ2V0KVxuICAgICAgICAgID8gJChzZXR0aW5ncy50YXJnZXQpXG4gICAgICAgICAgOiAkbW9kdWxlLFxuXG4gICAgICAgICRwb3B1cCxcbiAgICAgICAgJG9mZnNldFBhcmVudCxcblxuICAgICAgICBzZWFyY2hEZXB0aCAgICAgICAgPSAwLFxuICAgICAgICB0cmllZFBvc2l0aW9ucyAgICAgPSBmYWxzZSxcbiAgICAgICAgb3BlbmVkV2l0aFRvdWNoICAgID0gZmFsc2UsXG5cbiAgICAgICAgZWxlbWVudCAgICAgICAgICAgID0gdGhpcyxcbiAgICAgICAgaW5zdGFuY2UgICAgICAgICAgID0gJG1vZHVsZS5kYXRhKG1vZHVsZU5hbWVzcGFjZSksXG5cbiAgICAgICAgZG9jdW1lbnRPYnNlcnZlcixcbiAgICAgICAgZWxlbWVudE5hbWVzcGFjZSxcbiAgICAgICAgaWQsXG4gICAgICAgIG1vZHVsZVxuICAgICAgO1xuXG4gICAgICBtb2R1bGUgPSB7XG5cbiAgICAgICAgLy8gYmluZHMgZXZlbnRzXG4gICAgICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW5pdGlhbGl6aW5nJywgJG1vZHVsZSk7XG4gICAgICAgICAgbW9kdWxlLmNyZWF0ZUlEKCk7XG4gICAgICAgICAgbW9kdWxlLmJpbmQuZXZlbnRzKCk7XG4gICAgICAgICAgaWYoIW1vZHVsZS5leGlzdHMoKSAmJiBzZXR0aW5ncy5wcmVzZXJ2ZSkge1xuICAgICAgICAgICAgbW9kdWxlLmNyZWF0ZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBtb2R1bGUub2JzZXJ2ZUNoYW5nZXMoKTtcbiAgICAgICAgICBtb2R1bGUuaW5zdGFudGlhdGUoKTtcbiAgICAgICAgfSxcblxuICAgICAgICBpbnN0YW50aWF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1N0b3JpbmcgaW5zdGFuY2UnLCBtb2R1bGUpO1xuICAgICAgICAgIGluc3RhbmNlID0gbW9kdWxlO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5kYXRhKG1vZHVsZU5hbWVzcGFjZSwgaW5zdGFuY2UpXG4gICAgICAgICAgO1xuICAgICAgICB9LFxuXG4gICAgICAgIG9ic2VydmVDaGFuZ2VzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZignTXV0YXRpb25PYnNlcnZlcicgaW4gd2luZG93KSB7XG4gICAgICAgICAgICBkb2N1bWVudE9ic2VydmVyID0gbmV3IE11dGF0aW9uT2JzZXJ2ZXIobW9kdWxlLmV2ZW50LmRvY3VtZW50Q2hhbmdlZCk7XG4gICAgICAgICAgICBkb2N1bWVudE9ic2VydmVyLm9ic2VydmUoZG9jdW1lbnQsIHtcbiAgICAgICAgICAgICAgY2hpbGRMaXN0IDogdHJ1ZSxcbiAgICAgICAgICAgICAgc3VidHJlZSAgIDogdHJ1ZVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NldHRpbmcgdXAgbXV0YXRpb24gb2JzZXJ2ZXInLCBkb2N1bWVudE9ic2VydmVyKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVmcmVzaDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoc2V0dGluZ3MucG9wdXApIHtcbiAgICAgICAgICAgICRwb3B1cCA9ICQoc2V0dGluZ3MucG9wdXApLmVxKDApO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmlubGluZSkge1xuICAgICAgICAgICAgICAkcG9wdXAgPSAkdGFyZ2V0Lm5leHRBbGwoc2VsZWN0b3IucG9wdXApLmVxKDApO1xuICAgICAgICAgICAgICBzZXR0aW5ncy5wb3B1cCA9ICRwb3B1cDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoc2V0dGluZ3MucG9wdXApIHtcbiAgICAgICAgICAgICRwb3B1cC5hZGRDbGFzcyhjbGFzc05hbWUubG9hZGluZyk7XG4gICAgICAgICAgICAkb2Zmc2V0UGFyZW50ID0gbW9kdWxlLmdldC5vZmZzZXRQYXJlbnQoKTtcbiAgICAgICAgICAgICRwb3B1cC5yZW1vdmVDbGFzcyhjbGFzc05hbWUubG9hZGluZyk7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5tb3ZlUG9wdXAgJiYgbW9kdWxlLmhhcy5wb3B1cCgpICYmIG1vZHVsZS5nZXQub2Zmc2V0UGFyZW50KCRwb3B1cClbMF0gIT09ICRvZmZzZXRQYXJlbnRbMF0pIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdNb3ZpbmcgcG9wdXAgdG8gdGhlIHNhbWUgb2Zmc2V0IHBhcmVudCBhcyBhY3RpdmF0aW5nIGVsZW1lbnQnKTtcbiAgICAgICAgICAgICAgJHBvcHVwXG4gICAgICAgICAgICAgICAgLmRldGFjaCgpXG4gICAgICAgICAgICAgICAgLmFwcGVuZFRvKCRvZmZzZXRQYXJlbnQpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAkb2Zmc2V0UGFyZW50ID0gKHNldHRpbmdzLmlubGluZSlcbiAgICAgICAgICAgICAgPyBtb2R1bGUuZ2V0Lm9mZnNldFBhcmVudCgkdGFyZ2V0KVxuICAgICAgICAgICAgICA6IG1vZHVsZS5oYXMucG9wdXAoKVxuICAgICAgICAgICAgICAgID8gbW9kdWxlLmdldC5vZmZzZXRQYXJlbnQoJHBvcHVwKVxuICAgICAgICAgICAgICAgIDogJGJvZHlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoICRvZmZzZXRQYXJlbnQuaXMoJ2h0bWwnKSAmJiAkb2Zmc2V0UGFyZW50WzBdICE9PSAkYm9keVswXSApIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2V0dGluZyBwYWdlIGFzIG9mZnNldCBwYXJlbnQnKTtcbiAgICAgICAgICAgICRvZmZzZXRQYXJlbnQgPSAkYm9keTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoIG1vZHVsZS5nZXQudmFyaWF0aW9uKCkgKSB7XG4gICAgICAgICAgICBtb2R1bGUuc2V0LnZhcmlhdGlvbigpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICByZXBvc2l0aW9uOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUucmVmcmVzaCgpO1xuICAgICAgICAgIG1vZHVsZS5zZXQucG9zaXRpb24oKTtcbiAgICAgICAgfSxcblxuICAgICAgICBkZXN0cm95OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0Rlc3Ryb3lpbmcgcHJldmlvdXMgbW9kdWxlJyk7XG4gICAgICAgICAgaWYoZG9jdW1lbnRPYnNlcnZlcikge1xuICAgICAgICAgICAgZG9jdW1lbnRPYnNlcnZlci5kaXNjb25uZWN0KCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIC8vIHJlbW92ZSBlbGVtZW50IG9ubHkgaWYgd2FzIGNyZWF0ZWQgZHluYW1pY2FsbHlcbiAgICAgICAgICBpZigkcG9wdXAgJiYgIXNldHRpbmdzLnByZXNlcnZlKSB7XG4gICAgICAgICAgICBtb2R1bGUucmVtb3ZlUG9wdXAoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLy8gY2xlYXIgYWxsIHRpbWVvdXRzXG4gICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5oaWRlVGltZXIpO1xuICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUuc2hvd1RpbWVyKTtcbiAgICAgICAgICAvLyByZW1vdmUgZXZlbnRzXG4gICAgICAgICAgbW9kdWxlLnVuYmluZC5jbG9zZSgpO1xuICAgICAgICAgIG1vZHVsZS51bmJpbmQuZXZlbnRzKCk7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLnJlbW92ZURhdGEobW9kdWxlTmFtZXNwYWNlKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBldmVudDoge1xuICAgICAgICAgIHN0YXJ0OiAgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBkZWxheSA9ICgkLmlzUGxhaW5PYmplY3Qoc2V0dGluZ3MuZGVsYXkpKVxuICAgICAgICAgICAgICAgID8gc2V0dGluZ3MuZGVsYXkuc2hvd1xuICAgICAgICAgICAgICAgIDogc2V0dGluZ3MuZGVsYXlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUuaGlkZVRpbWVyKTtcbiAgICAgICAgICAgIGlmKCFvcGVuZWRXaXRoVG91Y2gpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnNob3dUaW1lciA9IHNldFRpbWVvdXQobW9kdWxlLnNob3csIGRlbGF5KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGVuZDogIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGRlbGF5ID0gKCQuaXNQbGFpbk9iamVjdChzZXR0aW5ncy5kZWxheSkpXG4gICAgICAgICAgICAgICAgPyBzZXR0aW5ncy5kZWxheS5oaWRlXG4gICAgICAgICAgICAgICAgOiBzZXR0aW5ncy5kZWxheVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5zaG93VGltZXIpO1xuICAgICAgICAgICAgbW9kdWxlLmhpZGVUaW1lciA9IHNldFRpbWVvdXQobW9kdWxlLmhpZGUsIGRlbGF5KTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHRvdWNoc3RhcnQ6IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICBvcGVuZWRXaXRoVG91Y2ggPSB0cnVlO1xuICAgICAgICAgICAgbW9kdWxlLnNob3coKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHJlc2l6ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZiggbW9kdWxlLmlzLnZpc2libGUoKSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5wb3NpdGlvbigpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgZG9jdW1lbnRDaGFuZ2VkOiBmdW5jdGlvbihtdXRhdGlvbnMpIHtcbiAgICAgICAgICAgIFtdLmZvckVhY2guY2FsbChtdXRhdGlvbnMsIGZ1bmN0aW9uKG11dGF0aW9uKSB7XG4gICAgICAgICAgICAgIGlmKG11dGF0aW9uLnJlbW92ZWROb2Rlcykge1xuICAgICAgICAgICAgICAgIFtdLmZvckVhY2guY2FsbChtdXRhdGlvbi5yZW1vdmVkTm9kZXMsIGZ1bmN0aW9uKG5vZGUpIHtcbiAgICAgICAgICAgICAgICAgIGlmKG5vZGUgPT0gZWxlbWVudCB8fCAkKG5vZGUpLmZpbmQoZWxlbWVudCkubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0VsZW1lbnQgcmVtb3ZlZCBmcm9tIERPTSwgdGVhcmluZyBkb3duIGV2ZW50cycpO1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUuZGVzdHJveSgpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGhpZGVHcmFjZWZ1bGx5OiBmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICR0YXJnZXQgPSAkKGV2ZW50LnRhcmdldCksXG4gICAgICAgICAgICAgIGlzSW5ET00gPSAkLmNvbnRhaW5zKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCwgZXZlbnQudGFyZ2V0KSxcbiAgICAgICAgICAgICAgaW5Qb3B1cCA9ICgkdGFyZ2V0LmNsb3Nlc3Qoc2VsZWN0b3IucG9wdXApLmxlbmd0aCA+IDApXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICAvLyBkb24ndCBjbG9zZSBvbiBjbGlja3MgaW5zaWRlIHBvcHVwXG4gICAgICAgICAgICBpZihldmVudCAmJiAhaW5Qb3B1cCAmJiBpc0luRE9NKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ2xpY2sgb2NjdXJyZWQgb3V0c2lkZSBwb3B1cCBoaWRpbmcgcG9wdXAnKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmhpZGUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NsaWNrIHdhcyBpbnNpZGUgcG9wdXAsIGtlZXBpbmcgcG9wdXAgb3BlbicpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICAvLyBnZW5lcmF0ZXMgcG9wdXAgaHRtbCBmcm9tIG1ldGFkYXRhXG4gICAgICAgIGNyZWF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICBodG1sICAgICAgPSBtb2R1bGUuZ2V0Lmh0bWwoKSxcbiAgICAgICAgICAgIHRpdGxlICAgICA9IG1vZHVsZS5nZXQudGl0bGUoKSxcbiAgICAgICAgICAgIGNvbnRlbnQgICA9IG1vZHVsZS5nZXQuY29udGVudCgpXG4gICAgICAgICAgO1xuXG4gICAgICAgICAgaWYoaHRtbCB8fCBjb250ZW50IHx8IHRpdGxlKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NyZWF0aW5nIHBvcC11cCBodG1sJyk7XG4gICAgICAgICAgICBpZighaHRtbCkge1xuICAgICAgICAgICAgICBodG1sID0gc2V0dGluZ3MudGVtcGxhdGVzLnBvcHVwKHtcbiAgICAgICAgICAgICAgICB0aXRsZSAgIDogdGl0bGUsXG4gICAgICAgICAgICAgICAgY29udGVudCA6IGNvbnRlbnRcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAkcG9wdXAgPSAkKCc8ZGl2Lz4nKVxuICAgICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLnBvcHVwKVxuICAgICAgICAgICAgICAuZGF0YShtZXRhZGF0YS5hY3RpdmF0b3IsICRtb2R1bGUpXG4gICAgICAgICAgICAgIC5odG1sKGh0bWwpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5pbmxpbmUpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0luc2VydGluZyBwb3B1cCBlbGVtZW50IGlubGluZScsICRwb3B1cCk7XG4gICAgICAgICAgICAgICRwb3B1cFxuICAgICAgICAgICAgICAgIC5pbnNlcnRBZnRlcigkbW9kdWxlKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0FwcGVuZGluZyBwb3B1cCBlbGVtZW50IHRvIGJvZHknLCAkcG9wdXApO1xuICAgICAgICAgICAgICAkcG9wdXBcbiAgICAgICAgICAgICAgICAuYXBwZW5kVG8oICRjb250ZXh0IClcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLnJlZnJlc2goKTtcbiAgICAgICAgICAgIG1vZHVsZS5zZXQudmFyaWF0aW9uKCk7XG5cbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmhvdmVyYWJsZSkge1xuICAgICAgICAgICAgICBtb2R1bGUuYmluZC5wb3B1cCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc2V0dGluZ3Mub25DcmVhdGUuY2FsbCgkcG9wdXAsIGVsZW1lbnQpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKCR0YXJnZXQubmV4dChzZWxlY3Rvci5wb3B1cCkubGVuZ3RoICE9PSAwKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUHJlLWV4aXN0aW5nIHBvcHVwIGZvdW5kJyk7XG4gICAgICAgICAgICBzZXR0aW5ncy5pbmxpbmUgPSB0cnVlO1xuICAgICAgICAgICAgc2V0dGluZ3MucG9wdXAgID0gJHRhcmdldC5uZXh0KHNlbGVjdG9yLnBvcHVwKS5kYXRhKG1ldGFkYXRhLmFjdGl2YXRvciwgJG1vZHVsZSk7XG4gICAgICAgICAgICBtb2R1bGUucmVmcmVzaCgpO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MuaG92ZXJhYmxlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5iaW5kLnBvcHVwKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYoc2V0dGluZ3MucG9wdXApIHtcbiAgICAgICAgICAgICQoc2V0dGluZ3MucG9wdXApLmRhdGEobWV0YWRhdGEuYWN0aXZhdG9yLCAkbW9kdWxlKTtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdVc2VkIHBvcHVwIHNwZWNpZmllZCBpbiBzZXR0aW5ncycpO1xuICAgICAgICAgICAgbW9kdWxlLnJlZnJlc2goKTtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmhvdmVyYWJsZSkge1xuICAgICAgICAgICAgICBtb2R1bGUuYmluZC5wb3B1cCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnTm8gY29udGVudCBzcGVjaWZpZWQgc2tpcHBpbmcgZGlzcGxheScsIGVsZW1lbnQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBjcmVhdGVJRDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWQgPSAoTWF0aC5yYW5kb20oKS50b1N0cmluZygxNikgKyAnMDAwMDAwMDAwJykuc3Vic3RyKDIsIDgpO1xuICAgICAgICAgIGVsZW1lbnROYW1lc3BhY2UgPSAnLicgKyBpZDtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQ3JlYXRpbmcgdW5pcXVlIGlkIGZvciBlbGVtZW50JywgaWQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8vIGRldGVybWluZXMgcG9wdXAgc3RhdGVcbiAgICAgICAgdG9nZ2xlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ1RvZ2dsaW5nIHBvcC11cCcpO1xuICAgICAgICAgIGlmKCBtb2R1bGUuaXMuaGlkZGVuKCkgKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1BvcHVwIGlzIGhpZGRlbiwgc2hvd2luZyBwb3AtdXAnKTtcbiAgICAgICAgICAgIG1vZHVsZS51bmJpbmQuY2xvc2UoKTtcbiAgICAgICAgICAgIG1vZHVsZS5zaG93KCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdQb3B1cCBpcyB2aXNpYmxlLCBoaWRpbmcgcG9wLXVwJyk7XG4gICAgICAgICAgICBtb2R1bGUuaGlkZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzaG93OiBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICAgIGNhbGxiYWNrID0gY2FsbGJhY2sgfHwgZnVuY3Rpb24oKXt9O1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2hvd2luZyBwb3AtdXAnLCBzZXR0aW5ncy50cmFuc2l0aW9uKTtcbiAgICAgICAgICBpZihtb2R1bGUuaXMuaGlkZGVuKCkgJiYgISggbW9kdWxlLmlzLmFjdGl2ZSgpICYmIG1vZHVsZS5pcy5kcm9wZG93bigpKSApIHtcbiAgICAgICAgICAgIGlmKCAhbW9kdWxlLmV4aXN0cygpICkge1xuICAgICAgICAgICAgICBtb2R1bGUuY3JlYXRlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5vblNob3cuY2FsbCgkcG9wdXAsIGVsZW1lbnQpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ29uU2hvdyBjYWxsYmFjayByZXR1cm5lZCBmYWxzZSwgY2FuY2VsbGluZyBwb3B1cCBhbmltYXRpb24nKTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZighc2V0dGluZ3MucHJlc2VydmUgJiYgIXNldHRpbmdzLnBvcHVwKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5yZWZyZXNoKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiggJHBvcHVwICYmIG1vZHVsZS5zZXQucG9zaXRpb24oKSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnNhdmUuY29uZGl0aW9ucygpO1xuICAgICAgICAgICAgICBpZihzZXR0aW5ncy5leGNsdXNpdmUpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuaGlkZUFsbCgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIG1vZHVsZS5hbmltYXRlLnNob3coY2FsbGJhY2spO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuXG4gICAgICAgIGhpZGU6IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG4gICAgICAgICAgY2FsbGJhY2sgPSBjYWxsYmFjayB8fCBmdW5jdGlvbigpe307XG4gICAgICAgICAgaWYoIG1vZHVsZS5pcy52aXNpYmxlKCkgfHwgbW9kdWxlLmlzLmFuaW1hdGluZygpICkge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3Mub25IaWRlLmNhbGwoJHBvcHVwLCBlbGVtZW50KSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdvbkhpZGUgY2FsbGJhY2sgcmV0dXJuZWQgZmFsc2UsIGNhbmNlbGxpbmcgcG9wdXAgYW5pbWF0aW9uJyk7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUudmlzaWJsZSgpO1xuICAgICAgICAgICAgbW9kdWxlLnVuYmluZC5jbG9zZSgpO1xuICAgICAgICAgICAgbW9kdWxlLnJlc3RvcmUuY29uZGl0aW9ucygpO1xuICAgICAgICAgICAgbW9kdWxlLmFuaW1hdGUuaGlkZShjYWxsYmFjayk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGhpZGVBbGw6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICQoc2VsZWN0b3IucG9wdXApXG4gICAgICAgICAgICAuZmlsdGVyKCcuJyArIGNsYXNzTmFtZS52aXNpYmxlKVxuICAgICAgICAgICAgLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICQodGhpcylcbiAgICAgICAgICAgICAgICAuZGF0YShtZXRhZGF0YS5hY3RpdmF0b3IpXG4gICAgICAgICAgICAgICAgICAucG9wdXAoJ2hpZGUnKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcbiAgICAgICAgZXhpc3RzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighJHBvcHVwKSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKHNldHRpbmdzLmlubGluZSB8fCBzZXR0aW5ncy5wb3B1cCkge1xuICAgICAgICAgICAgcmV0dXJuICggbW9kdWxlLmhhcy5wb3B1cCgpICk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuICggJHBvcHVwLmNsb3Nlc3QoJGNvbnRleHQpLmxlbmd0aCA+PSAxIClcbiAgICAgICAgICAgICAgPyB0cnVlXG4gICAgICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVtb3ZlUG9wdXA6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCBtb2R1bGUuaGFzLnBvcHVwKCkgJiYgIXNldHRpbmdzLnBvcHVwKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1JlbW92aW5nIHBvcHVwJywgJHBvcHVwKTtcbiAgICAgICAgICAgICRwb3B1cC5yZW1vdmUoKTtcbiAgICAgICAgICAgICRwb3B1cCA9IHVuZGVmaW5lZDtcbiAgICAgICAgICAgIHNldHRpbmdzLm9uUmVtb3ZlLmNhbGwoJHBvcHVwLCBlbGVtZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2F2ZToge1xuICAgICAgICAgIGNvbmRpdGlvbnM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLmNhY2hlID0ge1xuICAgICAgICAgICAgICB0aXRsZTogJG1vZHVsZS5hdHRyKCd0aXRsZScpXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgaWYgKG1vZHVsZS5jYWNoZS50aXRsZSkge1xuICAgICAgICAgICAgICAkbW9kdWxlLnJlbW92ZUF0dHIoJ3RpdGxlJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU2F2aW5nIG9yaWdpbmFsIGF0dHJpYnV0ZXMnLCBtb2R1bGUuY2FjaGUudGl0bGUpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgcmVzdG9yZToge1xuICAgICAgICAgIGNvbmRpdGlvbnM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmNhY2hlICYmIG1vZHVsZS5jYWNoZS50aXRsZSkge1xuICAgICAgICAgICAgICAkbW9kdWxlLmF0dHIoJ3RpdGxlJywgbW9kdWxlLmNhY2hlLnRpdGxlKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1Jlc3RvcmluZyBvcmlnaW5hbCBhdHRyaWJ1dGVzJywgbW9kdWxlLmNhY2hlLnRpdGxlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgc3VwcG9ydHM6IHtcbiAgICAgICAgICBzdmc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICh0eXBlb2YgU1ZHR3JhcGhpY3NFbGVtZW50ID09PSB1bmRlZmluZWQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgYW5pbWF0ZToge1xuICAgICAgICAgIHNob3c6IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG4gICAgICAgICAgICBjYWxsYmFjayA9ICQuaXNGdW5jdGlvbihjYWxsYmFjaykgPyBjYWxsYmFjayA6IGZ1bmN0aW9uKCl7fTtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnRyYW5zaXRpb24gJiYgJC5mbi50cmFuc2l0aW9uICE9PSB1bmRlZmluZWQgJiYgJG1vZHVsZS50cmFuc2l0aW9uKCdpcyBzdXBwb3J0ZWQnKSkge1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0LnZpc2libGUoKTtcbiAgICAgICAgICAgICAgJHBvcHVwXG4gICAgICAgICAgICAgICAgLnRyYW5zaXRpb24oe1xuICAgICAgICAgICAgICAgICAgYW5pbWF0aW9uICA6IHNldHRpbmdzLnRyYW5zaXRpb24gKyAnIGluJyxcbiAgICAgICAgICAgICAgICAgIHF1ZXVlICAgICAgOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgIGRlYnVnICAgICAgOiBzZXR0aW5ncy5kZWJ1ZyxcbiAgICAgICAgICAgICAgICAgIHZlcmJvc2UgICAgOiBzZXR0aW5ncy52ZXJib3NlLFxuICAgICAgICAgICAgICAgICAgZHVyYXRpb24gICA6IHNldHRpbmdzLmR1cmF0aW9uLFxuICAgICAgICAgICAgICAgICAgb25Db21wbGV0ZSA6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUuYmluZC5jbG9zZSgpO1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjay5jYWxsKCRwb3B1cCwgZWxlbWVudCk7XG4gICAgICAgICAgICAgICAgICAgIHNldHRpbmdzLm9uVmlzaWJsZS5jYWxsKCRwb3B1cCwgZWxlbWVudCk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5ub1RyYW5zaXRpb24pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgaGlkZTogZnVuY3Rpb24oY2FsbGJhY2spIHtcbiAgICAgICAgICAgIGNhbGxiYWNrID0gJC5pc0Z1bmN0aW9uKGNhbGxiYWNrKSA/IGNhbGxiYWNrIDogZnVuY3Rpb24oKXt9O1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdIaWRpbmcgcG9wLXVwJyk7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5vbkhpZGUuY2FsbCgkcG9wdXAsIGVsZW1lbnQpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ29uSGlkZSBjYWxsYmFjayByZXR1cm5lZCBmYWxzZSwgY2FuY2VsbGluZyBwb3B1cCBhbmltYXRpb24nKTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoc2V0dGluZ3MudHJhbnNpdGlvbiAmJiAkLmZuLnRyYW5zaXRpb24gIT09IHVuZGVmaW5lZCAmJiAkbW9kdWxlLnRyYW5zaXRpb24oJ2lzIHN1cHBvcnRlZCcpKSB7XG4gICAgICAgICAgICAgICRwb3B1cFxuICAgICAgICAgICAgICAgIC50cmFuc2l0aW9uKHtcbiAgICAgICAgICAgICAgICAgIGFuaW1hdGlvbiAgOiBzZXR0aW5ncy50cmFuc2l0aW9uICsgJyBvdXQnLFxuICAgICAgICAgICAgICAgICAgcXVldWUgICAgICA6IGZhbHNlLFxuICAgICAgICAgICAgICAgICAgZHVyYXRpb24gICA6IHNldHRpbmdzLmR1cmF0aW9uLFxuICAgICAgICAgICAgICAgICAgZGVidWcgICAgICA6IHNldHRpbmdzLmRlYnVnLFxuICAgICAgICAgICAgICAgICAgdmVyYm9zZSAgICA6IHNldHRpbmdzLnZlcmJvc2UsXG4gICAgICAgICAgICAgICAgICBvbkNvbXBsZXRlIDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vZHVsZS5yZXNldCgpO1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjay5jYWxsKCRwb3B1cCwgZWxlbWVudCk7XG4gICAgICAgICAgICAgICAgICAgIHNldHRpbmdzLm9uSGlkZGVuLmNhbGwoJHBvcHVwLCBlbGVtZW50KTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm5vVHJhbnNpdGlvbik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGNoYW5nZToge1xuICAgICAgICAgIGNvbnRlbnQ6IGZ1bmN0aW9uKGh0bWwpIHtcbiAgICAgICAgICAgICRwb3B1cC5odG1sKGh0bWwpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBnZXQ6IHtcbiAgICAgICAgICBodG1sOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRtb2R1bGUucmVtb3ZlRGF0YShtZXRhZGF0YS5odG1sKTtcbiAgICAgICAgICAgIHJldHVybiAkbW9kdWxlLmRhdGEobWV0YWRhdGEuaHRtbCkgfHwgc2V0dGluZ3MuaHRtbDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHRpdGxlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRtb2R1bGUucmVtb3ZlRGF0YShtZXRhZGF0YS50aXRsZSk7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5kYXRhKG1ldGFkYXRhLnRpdGxlKSB8fCBzZXR0aW5ncy50aXRsZTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGNvbnRlbnQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZS5yZW1vdmVEYXRhKG1ldGFkYXRhLmNvbnRlbnQpO1xuICAgICAgICAgICAgcmV0dXJuICRtb2R1bGUuZGF0YShtZXRhZGF0YS5jb250ZW50KSB8fCAkbW9kdWxlLmF0dHIoJ3RpdGxlJykgfHwgc2V0dGluZ3MuY29udGVudDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHZhcmlhdGlvbjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkbW9kdWxlLnJlbW92ZURhdGEobWV0YWRhdGEudmFyaWF0aW9uKTtcbiAgICAgICAgICAgIHJldHVybiAkbW9kdWxlLmRhdGEobWV0YWRhdGEudmFyaWF0aW9uKSB8fCBzZXR0aW5ncy52YXJpYXRpb247XG4gICAgICAgICAgfSxcbiAgICAgICAgICBwb3B1cDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJHBvcHVwO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcG9wdXBPZmZzZXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRwb3B1cC5vZmZzZXQoKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGNhbGN1bGF0aW9uczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgdGFyZ2V0RWxlbWVudCAgICA9ICR0YXJnZXRbMF0sXG4gICAgICAgICAgICAgIGlzV2luZG93ICAgICAgICAgPSAoJGJvdW5kYXJ5WzBdID09IHdpbmRvdyksXG4gICAgICAgICAgICAgIHRhcmdldFBvc2l0aW9uICAgPSAoc2V0dGluZ3MuaW5saW5lIHx8IChzZXR0aW5ncy5wb3B1cCAmJiBzZXR0aW5ncy5tb3ZlUG9wdXApKVxuICAgICAgICAgICAgICAgID8gJHRhcmdldC5wb3NpdGlvbigpXG4gICAgICAgICAgICAgICAgOiAkdGFyZ2V0Lm9mZnNldCgpLFxuICAgICAgICAgICAgICBzY3JlZW5Qb3NpdGlvbiA9IChpc1dpbmRvdylcbiAgICAgICAgICAgICAgICA/IHsgdG9wOiAwLCBsZWZ0OiAwIH1cbiAgICAgICAgICAgICAgICA6ICRib3VuZGFyeS5vZmZzZXQoKSxcbiAgICAgICAgICAgICAgY2FsY3VsYXRpb25zICAgPSB7fSxcbiAgICAgICAgICAgICAgc2Nyb2xsID0gKGlzV2luZG93KVxuICAgICAgICAgICAgICAgID8geyB0b3A6ICR3aW5kb3cuc2Nyb2xsVG9wKCksIGxlZnQ6ICR3aW5kb3cuc2Nyb2xsTGVmdCgpIH1cbiAgICAgICAgICAgICAgICA6IHsgdG9wOiAwLCBsZWZ0OiAwfSxcbiAgICAgICAgICAgICAgc2NyZWVuXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBjYWxjdWxhdGlvbnMgPSB7XG4gICAgICAgICAgICAgIC8vIGVsZW1lbnQgd2hpY2ggaXMgbGF1bmNoaW5nIHBvcHVwXG4gICAgICAgICAgICAgIHRhcmdldCA6IHtcbiAgICAgICAgICAgICAgICBlbGVtZW50IDogJHRhcmdldFswXSxcbiAgICAgICAgICAgICAgICB3aWR0aCAgIDogJHRhcmdldC5vdXRlcldpZHRoKCksXG4gICAgICAgICAgICAgICAgaGVpZ2h0ICA6ICR0YXJnZXQub3V0ZXJIZWlnaHQoKSxcbiAgICAgICAgICAgICAgICB0b3AgICAgIDogdGFyZ2V0UG9zaXRpb24udG9wLFxuICAgICAgICAgICAgICAgIGxlZnQgICAgOiB0YXJnZXRQb3NpdGlvbi5sZWZ0LFxuICAgICAgICAgICAgICAgIG1hcmdpbiAgOiB7fVxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAvLyBwb3B1cCBpdHNlbGZcbiAgICAgICAgICAgICAgcG9wdXAgOiB7XG4gICAgICAgICAgICAgICAgd2lkdGggIDogJHBvcHVwLm91dGVyV2lkdGgoKSxcbiAgICAgICAgICAgICAgICBoZWlnaHQgOiAkcG9wdXAub3V0ZXJIZWlnaHQoKVxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAvLyBvZmZzZXQgY29udGFpbmVyIChvciAzZCBjb250ZXh0KVxuICAgICAgICAgICAgICBwYXJlbnQgOiB7XG4gICAgICAgICAgICAgICAgd2lkdGggIDogJG9mZnNldFBhcmVudC5vdXRlcldpZHRoKCksXG4gICAgICAgICAgICAgICAgaGVpZ2h0IDogJG9mZnNldFBhcmVudC5vdXRlckhlaWdodCgpXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIC8vIHNjcmVlbiBib3VuZGFyaWVzXG4gICAgICAgICAgICAgIHNjcmVlbiA6IHtcbiAgICAgICAgICAgICAgICB0b3AgIDogc2NyZWVuUG9zaXRpb24udG9wLFxuICAgICAgICAgICAgICAgIGxlZnQgOiBzY3JlZW5Qb3NpdGlvbi5sZWZ0LFxuICAgICAgICAgICAgICAgIHNjcm9sbDoge1xuICAgICAgICAgICAgICAgICAgdG9wICA6IHNjcm9sbC50b3AsXG4gICAgICAgICAgICAgICAgICBsZWZ0IDogc2Nyb2xsLmxlZnRcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHdpZHRoICA6ICRib3VuZGFyeS53aWR0aCgpLFxuICAgICAgICAgICAgICAgIGhlaWdodCA6ICRib3VuZGFyeS5oZWlnaHQoKVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAvLyBhZGQgaW4gY29udGFpbmVyIGNhbGNzIGlmIGZsdWlkXG4gICAgICAgICAgICBpZiggc2V0dGluZ3Muc2V0Rmx1aWRXaWR0aCAmJiBtb2R1bGUuaXMuZmx1aWQoKSApIHtcbiAgICAgICAgICAgICAgY2FsY3VsYXRpb25zLmNvbnRhaW5lciA9IHtcbiAgICAgICAgICAgICAgICB3aWR0aDogJHBvcHVwLnBhcmVudCgpLm91dGVyV2lkdGgoKVxuICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICBjYWxjdWxhdGlvbnMucG9wdXAud2lkdGggPSBjYWxjdWxhdGlvbnMuY29udGFpbmVyLndpZHRoO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBhZGQgaW4gbWFyZ2lucyBpZiBpbmxpbmVcbiAgICAgICAgICAgIGNhbGN1bGF0aW9ucy50YXJnZXQubWFyZ2luLnRvcCA9IChzZXR0aW5ncy5pbmxpbmUpXG4gICAgICAgICAgICAgID8gcGFyc2VJbnQoIHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKHRhcmdldEVsZW1lbnQpLmdldFByb3BlcnR5VmFsdWUoJ21hcmdpbi10b3AnKSwgMTApXG4gICAgICAgICAgICAgIDogMFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgY2FsY3VsYXRpb25zLnRhcmdldC5tYXJnaW4ubGVmdCA9IChzZXR0aW5ncy5pbmxpbmUpXG4gICAgICAgICAgICAgID8gbW9kdWxlLmlzLnJ0bCgpXG4gICAgICAgICAgICAgICAgPyBwYXJzZUludCggd2luZG93LmdldENvbXB1dGVkU3R5bGUodGFyZ2V0RWxlbWVudCkuZ2V0UHJvcGVydHlWYWx1ZSgnbWFyZ2luLXJpZ2h0JyksIDEwKVxuICAgICAgICAgICAgICAgIDogcGFyc2VJbnQoIHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKHRhcmdldEVsZW1lbnQpLmdldFByb3BlcnR5VmFsdWUoJ21hcmdpbi1sZWZ0JyksIDEwKVxuICAgICAgICAgICAgICA6IDBcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIC8vIGNhbGN1bGF0ZSBzY3JlZW4gYm91bmRhcmllc1xuICAgICAgICAgICAgc2NyZWVuID0gY2FsY3VsYXRpb25zLnNjcmVlbjtcbiAgICAgICAgICAgIGNhbGN1bGF0aW9ucy5ib3VuZGFyeSA9IHtcbiAgICAgICAgICAgICAgdG9wICAgIDogc2NyZWVuLnRvcCArIHNjcmVlbi5zY3JvbGwudG9wLFxuICAgICAgICAgICAgICBib3R0b20gOiBzY3JlZW4udG9wICsgc2NyZWVuLnNjcm9sbC50b3AgKyBzY3JlZW4uaGVpZ2h0LFxuICAgICAgICAgICAgICBsZWZ0ICAgOiBzY3JlZW4ubGVmdCArIHNjcmVlbi5zY3JvbGwubGVmdCxcbiAgICAgICAgICAgICAgcmlnaHQgIDogc2NyZWVuLmxlZnQgKyBzY3JlZW4uc2Nyb2xsLmxlZnQgKyBzY3JlZW4ud2lkdGhcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICByZXR1cm4gY2FsY3VsYXRpb25zO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgaWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIGlkO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc3RhcnRFdmVudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5vbiA9PSAnaG92ZXInKSB7XG4gICAgICAgICAgICAgIHJldHVybiAnbW91c2VlbnRlcic7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKHNldHRpbmdzLm9uID09ICdmb2N1cycpIHtcbiAgICAgICAgICAgICAgcmV0dXJuICdmb2N1cyc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzY3JvbGxFdmVudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJ3Njcm9sbCc7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBlbmRFdmVudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5vbiA9PSAnaG92ZXInKSB7XG4gICAgICAgICAgICAgIHJldHVybiAnbW91c2VsZWF2ZSc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKHNldHRpbmdzLm9uID09ICdmb2N1cycpIHtcbiAgICAgICAgICAgICAgcmV0dXJuICdibHVyJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc3RhbmNlRnJvbUJvdW5kYXJ5OiBmdW5jdGlvbihvZmZzZXQsIGNhbGN1bGF0aW9ucykge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGRpc3RhbmNlRnJvbUJvdW5kYXJ5ID0ge30sXG4gICAgICAgICAgICAgIHBvcHVwLFxuICAgICAgICAgICAgICBib3VuZGFyeVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgY2FsY3VsYXRpb25zID0gY2FsY3VsYXRpb25zIHx8IG1vZHVsZS5nZXQuY2FsY3VsYXRpb25zKCk7XG5cbiAgICAgICAgICAgIC8vIHNob3J0aGFuZFxuICAgICAgICAgICAgcG9wdXAgICAgICAgID0gY2FsY3VsYXRpb25zLnBvcHVwO1xuICAgICAgICAgICAgYm91bmRhcnkgICAgID0gY2FsY3VsYXRpb25zLmJvdW5kYXJ5O1xuXG4gICAgICAgICAgICBpZihvZmZzZXQpIHtcbiAgICAgICAgICAgICAgZGlzdGFuY2VGcm9tQm91bmRhcnkgPSB7XG4gICAgICAgICAgICAgICAgdG9wICAgIDogKG9mZnNldC50b3AgLSBib3VuZGFyeS50b3ApLFxuICAgICAgICAgICAgICAgIGxlZnQgICA6IChvZmZzZXQubGVmdCAtIGJvdW5kYXJ5LmxlZnQpLFxuICAgICAgICAgICAgICAgIHJpZ2h0ICA6IChib3VuZGFyeS5yaWdodCAtIChvZmZzZXQubGVmdCArIHBvcHVwLndpZHRoKSApLFxuICAgICAgICAgICAgICAgIGJvdHRvbSA6IChib3VuZGFyeS5ib3R0b20gLSAob2Zmc2V0LnRvcCArIHBvcHVwLmhlaWdodCkgKVxuICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnRGlzdGFuY2UgZnJvbSBib3VuZGFyaWVzIGRldGVybWluZWQnLCBvZmZzZXQsIGRpc3RhbmNlRnJvbUJvdW5kYXJ5KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBkaXN0YW5jZUZyb21Cb3VuZGFyeTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIG9mZnNldFBhcmVudDogZnVuY3Rpb24oJHRhcmdldCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGVsZW1lbnQgPSAoJHRhcmdldCAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICAgID8gJHRhcmdldFswXVxuICAgICAgICAgICAgICAgIDogJG1vZHVsZVswXSxcbiAgICAgICAgICAgICAgcGFyZW50Tm9kZSA9IGVsZW1lbnQucGFyZW50Tm9kZSxcbiAgICAgICAgICAgICAgJG5vZGUgICAgPSAkKHBhcmVudE5vZGUpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihwYXJlbnROb2RlKSB7XG4gICAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAgIGlzMkQgICAgID0gKCRub2RlLmNzcygndHJhbnNmb3JtJykgPT09ICdub25lJyksXG4gICAgICAgICAgICAgICAgaXNTdGF0aWMgPSAoJG5vZGUuY3NzKCdwb3NpdGlvbicpID09PSAnc3RhdGljJyksXG4gICAgICAgICAgICAgICAgaXNIVE1MICAgPSAkbm9kZS5pcygnaHRtbCcpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgd2hpbGUocGFyZW50Tm9kZSAmJiAhaXNIVE1MICYmIGlzU3RhdGljICYmIGlzMkQpIHtcbiAgICAgICAgICAgICAgICBwYXJlbnROb2RlID0gcGFyZW50Tm9kZS5wYXJlbnROb2RlO1xuICAgICAgICAgICAgICAgICRub2RlICAgID0gJChwYXJlbnROb2RlKTtcbiAgICAgICAgICAgICAgICBpczJEICAgICA9ICgkbm9kZS5jc3MoJ3RyYW5zZm9ybScpID09PSAnbm9uZScpO1xuICAgICAgICAgICAgICAgIGlzU3RhdGljID0gKCRub2RlLmNzcygncG9zaXRpb24nKSA9PT0gJ3N0YXRpYycpO1xuICAgICAgICAgICAgICAgIGlzSFRNTCAgID0gJG5vZGUuaXMoJ2h0bWwnKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuICgkbm9kZSAmJiAkbm9kZS5sZW5ndGggPiAwKVxuICAgICAgICAgICAgICA/ICRub2RlXG4gICAgICAgICAgICAgIDogJCgpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBwb3NpdGlvbnM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgJ3RvcCBsZWZ0JyAgICAgIDogZmFsc2UsXG4gICAgICAgICAgICAgICd0b3AgY2VudGVyJyAgICA6IGZhbHNlLFxuICAgICAgICAgICAgICAndG9wIHJpZ2h0JyAgICAgOiBmYWxzZSxcbiAgICAgICAgICAgICAgJ2JvdHRvbSBsZWZ0JyAgIDogZmFsc2UsXG4gICAgICAgICAgICAgICdib3R0b20gY2VudGVyJyA6IGZhbHNlLFxuICAgICAgICAgICAgICAnYm90dG9tIHJpZ2h0JyAgOiBmYWxzZSxcbiAgICAgICAgICAgICAgJ2xlZnQgY2VudGVyJyAgIDogZmFsc2UsXG4gICAgICAgICAgICAgICdyaWdodCBjZW50ZXInICA6IGZhbHNlXG4gICAgICAgICAgICB9O1xuICAgICAgICAgIH0sXG4gICAgICAgICAgbmV4dFBvc2l0aW9uOiBmdW5jdGlvbihwb3NpdGlvbikge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHBvc2l0aW9ucyAgICAgICAgICA9IHBvc2l0aW9uLnNwbGl0KCcgJyksXG4gICAgICAgICAgICAgIHZlcnRpY2FsUG9zaXRpb24gICA9IHBvc2l0aW9uc1swXSxcbiAgICAgICAgICAgICAgaG9yaXpvbnRhbFBvc2l0aW9uID0gcG9zaXRpb25zWzFdLFxuICAgICAgICAgICAgICBvcHBvc2l0ZSA9IHtcbiAgICAgICAgICAgICAgICB0b3AgICAgOiAnYm90dG9tJyxcbiAgICAgICAgICAgICAgICBib3R0b20gOiAndG9wJyxcbiAgICAgICAgICAgICAgICBsZWZ0ICAgOiAncmlnaHQnLFxuICAgICAgICAgICAgICAgIHJpZ2h0ICA6ICdsZWZ0J1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBhZGphY2VudCA9IHtcbiAgICAgICAgICAgICAgICBsZWZ0ICAgOiAnY2VudGVyJyxcbiAgICAgICAgICAgICAgICBjZW50ZXIgOiAncmlnaHQnLFxuICAgICAgICAgICAgICAgIHJpZ2h0ICA6ICdsZWZ0J1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBiYWNrdXAgPSB7XG4gICAgICAgICAgICAgICAgJ3RvcCBsZWZ0JyAgICAgIDogJ3RvcCBjZW50ZXInLFxuICAgICAgICAgICAgICAgICd0b3AgY2VudGVyJyAgICA6ICd0b3AgcmlnaHQnLFxuICAgICAgICAgICAgICAgICd0b3AgcmlnaHQnICAgICA6ICdyaWdodCBjZW50ZXInLFxuICAgICAgICAgICAgICAgICdyaWdodCBjZW50ZXInICA6ICdib3R0b20gcmlnaHQnLFxuICAgICAgICAgICAgICAgICdib3R0b20gcmlnaHQnICA6ICdib3R0b20gY2VudGVyJyxcbiAgICAgICAgICAgICAgICAnYm90dG9tIGNlbnRlcicgOiAnYm90dG9tIGxlZnQnLFxuICAgICAgICAgICAgICAgICdib3R0b20gbGVmdCcgICA6ICdsZWZ0IGNlbnRlcicsXG4gICAgICAgICAgICAgICAgJ2xlZnQgY2VudGVyJyAgIDogJ3RvcCBsZWZ0J1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBhZGphY2VudHNBdmFpbGFibGUgPSAodmVydGljYWxQb3NpdGlvbiA9PSAndG9wJyB8fCB2ZXJ0aWNhbFBvc2l0aW9uID09ICdib3R0b20nKSxcbiAgICAgICAgICAgICAgb3Bwb3NpdGVUcmllZCA9IGZhbHNlLFxuICAgICAgICAgICAgICBhZGphY2VudFRyaWVkID0gZmFsc2UsXG4gICAgICAgICAgICAgIG5leHRQb3NpdGlvbiAgPSBmYWxzZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoIXRyaWVkUG9zaXRpb25zKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdBbGwgYXZhaWxhYmxlIHBvc2l0aW9ucyBhdmFpbGFibGUnKTtcbiAgICAgICAgICAgICAgdHJpZWRQb3NpdGlvbnMgPSBtb2R1bGUuZ2V0LnBvc2l0aW9ucygpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1JlY29yZGluZyBsYXN0IHBvc2l0aW9uIHRyaWVkJywgcG9zaXRpb24pO1xuICAgICAgICAgICAgdHJpZWRQb3NpdGlvbnNbcG9zaXRpb25dID0gdHJ1ZTtcblxuICAgICAgICAgICAgaWYoc2V0dGluZ3MucHJlZmVyID09PSAnb3Bwb3NpdGUnKSB7XG4gICAgICAgICAgICAgIG5leHRQb3NpdGlvbiAgPSBbb3Bwb3NpdGVbdmVydGljYWxQb3NpdGlvbl0sIGhvcml6b250YWxQb3NpdGlvbl07XG4gICAgICAgICAgICAgIG5leHRQb3NpdGlvbiAgPSBuZXh0UG9zaXRpb24uam9pbignICcpO1xuICAgICAgICAgICAgICBvcHBvc2l0ZVRyaWVkID0gKHRyaWVkUG9zaXRpb25zW25leHRQb3NpdGlvbl0gPT09IHRydWUpO1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1RyeWluZyBvcHBvc2l0ZSBzdHJhdGVneScsIG5leHRQb3NpdGlvbik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZigoc2V0dGluZ3MucHJlZmVyID09PSAnYWRqYWNlbnQnKSAmJiBhZGphY2VudHNBdmFpbGFibGUgKSB7XG4gICAgICAgICAgICAgIG5leHRQb3NpdGlvbiAgPSBbdmVydGljYWxQb3NpdGlvbiwgYWRqYWNlbnRbaG9yaXpvbnRhbFBvc2l0aW9uXV07XG4gICAgICAgICAgICAgIG5leHRQb3NpdGlvbiAgPSBuZXh0UG9zaXRpb24uam9pbignICcpO1xuICAgICAgICAgICAgICBhZGphY2VudFRyaWVkID0gKHRyaWVkUG9zaXRpb25zW25leHRQb3NpdGlvbl0gPT09IHRydWUpO1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1RyeWluZyBhZGphY2VudCBzdHJhdGVneScsIG5leHRQb3NpdGlvbik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihhZGphY2VudFRyaWVkIHx8IG9wcG9zaXRlVHJpZWQpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdVc2luZyBiYWNrdXAgcG9zaXRpb24nLCBuZXh0UG9zaXRpb24pO1xuICAgICAgICAgICAgICBuZXh0UG9zaXRpb24gPSBiYWNrdXBbcG9zaXRpb25dO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG5leHRQb3NpdGlvbjtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0OiB7XG4gICAgICAgICAgcG9zaXRpb246IGZ1bmN0aW9uKHBvc2l0aW9uLCBjYWxjdWxhdGlvbnMpIHtcblxuICAgICAgICAgICAgLy8gZXhpdCBjb25kaXRpb25zXG4gICAgICAgICAgICBpZigkdGFyZ2V0Lmxlbmd0aCA9PT0gMCB8fCAkcG9wdXAubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5ub3RGb3VuZCk7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBvZmZzZXQsXG4gICAgICAgICAgICAgIGRpc3RhbmNlQXdheSxcbiAgICAgICAgICAgICAgdGFyZ2V0LFxuICAgICAgICAgICAgICBwb3B1cCxcbiAgICAgICAgICAgICAgcGFyZW50LFxuICAgICAgICAgICAgICBwb3NpdGlvbmluZyxcbiAgICAgICAgICAgICAgcG9wdXBPZmZzZXQsXG4gICAgICAgICAgICAgIGRpc3RhbmNlRnJvbUJvdW5kYXJ5XG4gICAgICAgICAgICA7XG5cbiAgICAgICAgICAgIGNhbGN1bGF0aW9ucyA9IGNhbGN1bGF0aW9ucyB8fCBtb2R1bGUuZ2V0LmNhbGN1bGF0aW9ucygpO1xuICAgICAgICAgICAgcG9zaXRpb24gICAgID0gcG9zaXRpb24gICAgIHx8ICRtb2R1bGUuZGF0YShtZXRhZGF0YS5wb3NpdGlvbikgfHwgc2V0dGluZ3MucG9zaXRpb247XG5cbiAgICAgICAgICAgIG9mZnNldCAgICAgICA9ICRtb2R1bGUuZGF0YShtZXRhZGF0YS5vZmZzZXQpIHx8IHNldHRpbmdzLm9mZnNldDtcbiAgICAgICAgICAgIGRpc3RhbmNlQXdheSA9IHNldHRpbmdzLmRpc3RhbmNlQXdheTtcblxuICAgICAgICAgICAgLy8gc2hvcnRoYW5kXG4gICAgICAgICAgICB0YXJnZXQgPSBjYWxjdWxhdGlvbnMudGFyZ2V0O1xuICAgICAgICAgICAgcG9wdXAgID0gY2FsY3VsYXRpb25zLnBvcHVwO1xuICAgICAgICAgICAgcGFyZW50ID0gY2FsY3VsYXRpb25zLnBhcmVudDtcblxuICAgICAgICAgICAgaWYodGFyZ2V0LndpZHRoID09PSAwICYmIHRhcmdldC5oZWlnaHQgPT09IDAgJiYgIW1vZHVsZS5pcy5zdmcodGFyZ2V0LmVsZW1lbnQpKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnUG9wdXAgdGFyZ2V0IGlzIGhpZGRlbiwgbm8gYWN0aW9uIHRha2VuJyk7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYoc2V0dGluZ3MuaW5saW5lKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWRkaW5nIG1hcmdpbiB0byBjYWxjdWxhdGlvbicsIHRhcmdldC5tYXJnaW4pO1xuICAgICAgICAgICAgICBpZihwb3NpdGlvbiA9PSAnbGVmdCBjZW50ZXInIHx8IHBvc2l0aW9uID09ICdyaWdodCBjZW50ZXInKSB7XG4gICAgICAgICAgICAgICAgb2Zmc2V0ICAgICAgICs9ICB0YXJnZXQubWFyZ2luLnRvcDtcbiAgICAgICAgICAgICAgICBkaXN0YW5jZUF3YXkgKz0gLXRhcmdldC5tYXJnaW4ubGVmdDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmIChwb3NpdGlvbiA9PSAndG9wIGxlZnQnIHx8IHBvc2l0aW9uID09ICd0b3AgY2VudGVyJyB8fCBwb3NpdGlvbiA9PSAndG9wIHJpZ2h0Jykge1xuICAgICAgICAgICAgICAgIG9mZnNldCAgICAgICArPSB0YXJnZXQubWFyZ2luLmxlZnQ7XG4gICAgICAgICAgICAgICAgZGlzdGFuY2VBd2F5IC09IHRhcmdldC5tYXJnaW4udG9wO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIG9mZnNldCAgICAgICArPSB0YXJnZXQubWFyZ2luLmxlZnQ7XG4gICAgICAgICAgICAgICAgZGlzdGFuY2VBd2F5ICs9IHRhcmdldC5tYXJnaW4udG9wO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRGV0ZXJtaW5pbmcgcG9wdXAgcG9zaXRpb24gZnJvbSBjYWxjdWxhdGlvbnMnLCBwb3NpdGlvbiwgY2FsY3VsYXRpb25zKTtcblxuICAgICAgICAgICAgaWYgKG1vZHVsZS5pcy5ydGwoKSkge1xuICAgICAgICAgICAgICBwb3NpdGlvbiA9IHBvc2l0aW9uLnJlcGxhY2UoL2xlZnR8cmlnaHQvZywgZnVuY3Rpb24gKG1hdGNoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIChtYXRjaCA9PSAnbGVmdCcpXG4gICAgICAgICAgICAgICAgICA/ICdyaWdodCdcbiAgICAgICAgICAgICAgICAgIDogJ2xlZnQnXG4gICAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdSVEw6IFBvcHVwIHBvc2l0aW9uIHVwZGF0ZWQnLCBwb3NpdGlvbik7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIGlmIGxhc3QgYXR0ZW1wdCB1c2Ugc3BlY2lmaWVkIGxhc3QgcmVzb3J0IHBvc2l0aW9uXG4gICAgICAgICAgICBpZihzZWFyY2hEZXB0aCA9PSBzZXR0aW5ncy5tYXhTZWFyY2hEZXB0aCAmJiB0eXBlb2Ygc2V0dGluZ3MubGFzdFJlc29ydCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgcG9zaXRpb24gPSBzZXR0aW5ncy5sYXN0UmVzb3J0O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBzd2l0Y2ggKHBvc2l0aW9uKSB7XG4gICAgICAgICAgICAgIGNhc2UgJ3RvcCBsZWZ0JzpcbiAgICAgICAgICAgICAgICBwb3NpdGlvbmluZyA9IHtcbiAgICAgICAgICAgICAgICAgIHRvcCAgICA6ICdhdXRvJyxcbiAgICAgICAgICAgICAgICAgIGJvdHRvbSA6IHBhcmVudC5oZWlnaHQgLSB0YXJnZXQudG9wICsgZGlzdGFuY2VBd2F5LFxuICAgICAgICAgICAgICAgICAgbGVmdCAgIDogdGFyZ2V0LmxlZnQgKyBvZmZzZXQsXG4gICAgICAgICAgICAgICAgICByaWdodCAgOiAnYXV0bydcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgY2FzZSAndG9wIGNlbnRlcic6XG4gICAgICAgICAgICAgICAgcG9zaXRpb25pbmcgPSB7XG4gICAgICAgICAgICAgICAgICBib3R0b20gOiBwYXJlbnQuaGVpZ2h0IC0gdGFyZ2V0LnRvcCArIGRpc3RhbmNlQXdheSxcbiAgICAgICAgICAgICAgICAgIGxlZnQgICA6IHRhcmdldC5sZWZ0ICsgKHRhcmdldC53aWR0aCAvIDIpIC0gKHBvcHVwLndpZHRoIC8gMikgKyBvZmZzZXQsXG4gICAgICAgICAgICAgICAgICB0b3AgICAgOiAnYXV0bycsXG4gICAgICAgICAgICAgICAgICByaWdodCAgOiAnYXV0bydcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgY2FzZSAndG9wIHJpZ2h0JzpcbiAgICAgICAgICAgICAgICBwb3NpdGlvbmluZyA9IHtcbiAgICAgICAgICAgICAgICAgIGJvdHRvbSA6ICBwYXJlbnQuaGVpZ2h0IC0gdGFyZ2V0LnRvcCArIGRpc3RhbmNlQXdheSxcbiAgICAgICAgICAgICAgICAgIHJpZ2h0ICA6ICBwYXJlbnQud2lkdGggLSB0YXJnZXQubGVmdCAtIHRhcmdldC53aWR0aCAtIG9mZnNldCxcbiAgICAgICAgICAgICAgICAgIHRvcCAgICA6ICdhdXRvJyxcbiAgICAgICAgICAgICAgICAgIGxlZnQgICA6ICdhdXRvJ1xuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICBjYXNlICdsZWZ0IGNlbnRlcic6XG4gICAgICAgICAgICAgICAgcG9zaXRpb25pbmcgPSB7XG4gICAgICAgICAgICAgICAgICB0b3AgICAgOiB0YXJnZXQudG9wICsgKHRhcmdldC5oZWlnaHQgLyAyKSAtIChwb3B1cC5oZWlnaHQgLyAyKSArIG9mZnNldCxcbiAgICAgICAgICAgICAgICAgIHJpZ2h0ICA6IHBhcmVudC53aWR0aCAtIHRhcmdldC5sZWZ0ICsgZGlzdGFuY2VBd2F5LFxuICAgICAgICAgICAgICAgICAgbGVmdCAgIDogJ2F1dG8nLFxuICAgICAgICAgICAgICAgICAgYm90dG9tIDogJ2F1dG8nXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgIGNhc2UgJ3JpZ2h0IGNlbnRlcic6XG4gICAgICAgICAgICAgICAgcG9zaXRpb25pbmcgPSB7XG4gICAgICAgICAgICAgICAgICB0b3AgICAgOiB0YXJnZXQudG9wICsgKHRhcmdldC5oZWlnaHQgLyAyKSAtIChwb3B1cC5oZWlnaHQgLyAyKSArIG9mZnNldCxcbiAgICAgICAgICAgICAgICAgIGxlZnQgICA6IHRhcmdldC5sZWZ0ICsgdGFyZ2V0LndpZHRoICsgZGlzdGFuY2VBd2F5LFxuICAgICAgICAgICAgICAgICAgYm90dG9tIDogJ2F1dG8nLFxuICAgICAgICAgICAgICAgICAgcmlnaHQgIDogJ2F1dG8nXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgIGNhc2UgJ2JvdHRvbSBsZWZ0JzpcbiAgICAgICAgICAgICAgICBwb3NpdGlvbmluZyA9IHtcbiAgICAgICAgICAgICAgICAgIHRvcCAgICA6IHRhcmdldC50b3AgKyB0YXJnZXQuaGVpZ2h0ICsgZGlzdGFuY2VBd2F5LFxuICAgICAgICAgICAgICAgICAgbGVmdCAgIDogdGFyZ2V0LmxlZnQgKyBvZmZzZXQsXG4gICAgICAgICAgICAgICAgICBib3R0b20gOiAnYXV0bycsXG4gICAgICAgICAgICAgICAgICByaWdodCAgOiAnYXV0bydcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgY2FzZSAnYm90dG9tIGNlbnRlcic6XG4gICAgICAgICAgICAgICAgcG9zaXRpb25pbmcgPSB7XG4gICAgICAgICAgICAgICAgICB0b3AgICAgOiB0YXJnZXQudG9wICsgdGFyZ2V0LmhlaWdodCArIGRpc3RhbmNlQXdheSxcbiAgICAgICAgICAgICAgICAgIGxlZnQgICA6IHRhcmdldC5sZWZ0ICsgKHRhcmdldC53aWR0aCAvIDIpIC0gKHBvcHVwLndpZHRoIC8gMikgKyBvZmZzZXQsXG4gICAgICAgICAgICAgICAgICBib3R0b20gOiAnYXV0bycsXG4gICAgICAgICAgICAgICAgICByaWdodCAgOiAnYXV0bydcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgY2FzZSAnYm90dG9tIHJpZ2h0JzpcbiAgICAgICAgICAgICAgICBwb3NpdGlvbmluZyA9IHtcbiAgICAgICAgICAgICAgICAgIHRvcCAgICA6IHRhcmdldC50b3AgKyB0YXJnZXQuaGVpZ2h0ICsgZGlzdGFuY2VBd2F5LFxuICAgICAgICAgICAgICAgICAgcmlnaHQgIDogcGFyZW50LndpZHRoIC0gdGFyZ2V0LmxlZnQgIC0gdGFyZ2V0LndpZHRoIC0gb2Zmc2V0LFxuICAgICAgICAgICAgICAgICAgbGVmdCAgIDogJ2F1dG8nLFxuICAgICAgICAgICAgICAgICAgYm90dG9tIDogJ2F1dG8nXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihwb3NpdGlvbmluZyA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5pbnZhbGlkUG9zaXRpb24sIHBvc2l0aW9uKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDYWxjdWxhdGVkIHBvcHVwIHBvc2l0aW9uaW5nIHZhbHVlcycsIHBvc2l0aW9uaW5nKTtcblxuICAgICAgICAgICAgLy8gdGVudGF0aXZlbHkgcGxhY2Ugb24gc3RhZ2VcbiAgICAgICAgICAgICRwb3B1cFxuICAgICAgICAgICAgICAuY3NzKHBvc2l0aW9uaW5nKVxuICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLnBvc2l0aW9uKVxuICAgICAgICAgICAgICAuYWRkQ2xhc3MocG9zaXRpb24pXG4gICAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUubG9hZGluZylcbiAgICAgICAgICAgIDtcblxuICAgICAgICAgICAgcG9wdXBPZmZzZXQgPSBtb2R1bGUuZ2V0LnBvcHVwT2Zmc2V0KCk7XG5cbiAgICAgICAgICAgIC8vIHNlZSBpZiBhbnkgYm91bmRhcmllcyBhcmUgc3VycGFzc2VkIHdpdGggdGhpcyB0ZW50YXRpdmUgcG9zaXRpb25cbiAgICAgICAgICAgIGRpc3RhbmNlRnJvbUJvdW5kYXJ5ID0gbW9kdWxlLmdldC5kaXN0YW5jZUZyb21Cb3VuZGFyeShwb3B1cE9mZnNldCwgY2FsY3VsYXRpb25zKTtcblxuICAgICAgICAgICAgaWYoIG1vZHVsZS5pcy5vZmZzdGFnZShkaXN0YW5jZUZyb21Cb3VuZGFyeSwgcG9zaXRpb24pICkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Bvc2l0aW9uIGlzIG91dHNpZGUgdmlld3BvcnQnLCBwb3NpdGlvbik7XG4gICAgICAgICAgICAgIGlmKHNlYXJjaERlcHRoIDwgc2V0dGluZ3MubWF4U2VhcmNoRGVwdGgpIHtcbiAgICAgICAgICAgICAgICBzZWFyY2hEZXB0aCsrO1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uID0gbW9kdWxlLmdldC5uZXh0UG9zaXRpb24ocG9zaXRpb24pO1xuICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVHJ5aW5nIG5ldyBwb3NpdGlvbicsIHBvc2l0aW9uKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gKCRwb3B1cClcbiAgICAgICAgICAgICAgICAgID8gbW9kdWxlLnNldC5wb3NpdGlvbihwb3NpdGlvbiwgY2FsY3VsYXRpb25zKVxuICAgICAgICAgICAgICAgICAgOiBmYWxzZVxuICAgICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBpZihzZXR0aW5ncy5sYXN0UmVzb3J0KSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ05vIHBvc2l0aW9uIGZvdW5kLCBzaG93aW5nIHdpdGggbGFzdCBwb3NpdGlvbicpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnUG9wdXAgY291bGQgbm90IGZpbmQgYSBwb3NpdGlvbiB0byBkaXNwbGF5JywgJHBvcHVwKTtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5jYW5ub3RQbGFjZSwgZWxlbWVudCk7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUucmVtb3ZlLmF0dGVtcHRzKCk7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUucmVtb3ZlLmxvYWRpbmcoKTtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5yZXNldCgpO1xuICAgICAgICAgICAgICAgICAgc2V0dGluZ3Mub25VbnBsYWNlYWJsZS5jYWxsKCRwb3B1cCwgZWxlbWVudCk7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Bvc2l0aW9uIGlzIG9uIHN0YWdlJywgcG9zaXRpb24pO1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5hdHRlbXB0cygpO1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5sb2FkaW5nKCk7XG4gICAgICAgICAgICBpZiggc2V0dGluZ3Muc2V0Rmx1aWRXaWR0aCAmJiBtb2R1bGUuaXMuZmx1aWQoKSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5mbHVpZFdpZHRoKGNhbGN1bGF0aW9ucyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICB9LFxuXG4gICAgICAgICAgZmx1aWRXaWR0aDogZnVuY3Rpb24oY2FsY3VsYXRpb25zKSB7XG4gICAgICAgICAgICBjYWxjdWxhdGlvbnMgPSBjYWxjdWxhdGlvbnMgfHwgbW9kdWxlLmdldC5jYWxjdWxhdGlvbnMoKTtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQXV0b21hdGljYWxseSBzZXR0aW5nIGVsZW1lbnQgd2lkdGggdG8gcGFyZW50IHdpZHRoJywgY2FsY3VsYXRpb25zLnBhcmVudC53aWR0aCk7XG4gICAgICAgICAgICAkcG9wdXAuY3NzKCd3aWR0aCcsIGNhbGN1bGF0aW9ucy5jb250YWluZXIud2lkdGgpO1xuICAgICAgICAgIH0sXG5cbiAgICAgICAgICB2YXJpYXRpb246IGZ1bmN0aW9uKHZhcmlhdGlvbikge1xuICAgICAgICAgICAgdmFyaWF0aW9uID0gdmFyaWF0aW9uIHx8IG1vZHVsZS5nZXQudmFyaWF0aW9uKCk7XG4gICAgICAgICAgICBpZih2YXJpYXRpb24gJiYgbW9kdWxlLmhhcy5wb3B1cCgpICkge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQWRkaW5nIHZhcmlhdGlvbiB0byBwb3B1cCcsIHZhcmlhdGlvbik7XG4gICAgICAgICAgICAgICRwb3B1cC5hZGRDbGFzcyh2YXJpYXRpb24pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG5cbiAgICAgICAgICB2aXNpYmxlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRtb2R1bGUuYWRkQ2xhc3MoY2xhc3NOYW1lLnZpc2libGUpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICByZW1vdmU6IHtcbiAgICAgICAgICBsb2FkaW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRwb3B1cC5yZW1vdmVDbGFzcyhjbGFzc05hbWUubG9hZGluZyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB2YXJpYXRpb246IGZ1bmN0aW9uKHZhcmlhdGlvbikge1xuICAgICAgICAgICAgdmFyaWF0aW9uID0gdmFyaWF0aW9uIHx8IG1vZHVsZS5nZXQudmFyaWF0aW9uKCk7XG4gICAgICAgICAgICBpZih2YXJpYXRpb24pIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1JlbW92aW5nIHZhcmlhdGlvbicsIHZhcmlhdGlvbik7XG4gICAgICAgICAgICAgICRwb3B1cC5yZW1vdmVDbGFzcyh2YXJpYXRpb24pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgdmlzaWJsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkbW9kdWxlLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS52aXNpYmxlKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGF0dGVtcHRzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZXNldHRpbmcgYWxsIHNlYXJjaGVkIHBvc2l0aW9ucycpO1xuICAgICAgICAgICAgc2VhcmNoRGVwdGggICAgPSAwO1xuICAgICAgICAgICAgdHJpZWRQb3NpdGlvbnMgPSBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgYmluZDoge1xuICAgICAgICAgIGV2ZW50czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0JpbmRpbmcgcG9wdXAgZXZlbnRzIHRvIG1vZHVsZScpO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3Mub24gPT0gJ2NsaWNrJykge1xuICAgICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgICAgLm9uKCdjbGljaycgKyBldmVudE5hbWVzcGFjZSwgbW9kdWxlLnRvZ2dsZSlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoc2V0dGluZ3Mub24gPT0gJ2hvdmVyJyAmJiBoYXNUb3VjaCkge1xuICAgICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgICAgLm9uKCd0b3VjaHN0YXJ0JyArIGV2ZW50TmFtZXNwYWNlLCBtb2R1bGUuZXZlbnQudG91Y2hzdGFydClcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoIG1vZHVsZS5nZXQuc3RhcnRFdmVudCgpICkge1xuICAgICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgICAgLm9uKG1vZHVsZS5nZXQuc3RhcnRFdmVudCgpICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS5ldmVudC5zdGFydClcbiAgICAgICAgICAgICAgICAub24obW9kdWxlLmdldC5lbmRFdmVudCgpICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS5ldmVudC5lbmQpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnRhcmdldCkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1RhcmdldCBzZXQgdG8gZWxlbWVudCcsICR0YXJnZXQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgJHdpbmRvdy5vbigncmVzaXplJyArIGVsZW1lbnROYW1lc3BhY2UsIG1vZHVsZS5ldmVudC5yZXNpemUpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcG9wdXA6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0FsbG93aW5nIGhvdmVyIGV2ZW50cyBvbiBwb3B1cCB0byBwcmV2ZW50IGNsb3NpbmcnKTtcbiAgICAgICAgICAgIGlmKCAkcG9wdXAgJiYgbW9kdWxlLmhhcy5wb3B1cCgpICkge1xuICAgICAgICAgICAgICAkcG9wdXBcbiAgICAgICAgICAgICAgICAub24oJ21vdXNlZW50ZXInICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS5ldmVudC5zdGFydClcbiAgICAgICAgICAgICAgICAub24oJ21vdXNlbGVhdmUnICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS5ldmVudC5lbmQpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGNsb3NlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmhpZGVPblNjcm9sbCA9PT0gdHJ1ZSB8fCAoc2V0dGluZ3MuaGlkZU9uU2Nyb2xsID09ICdhdXRvJyAmJiBzZXR0aW5ncy5vbiAhPSAnY2xpY2snKSkge1xuICAgICAgICAgICAgICAkc2Nyb2xsQ29udGV4dFxuICAgICAgICAgICAgICAgIC5vbmUobW9kdWxlLmdldC5zY3JvbGxFdmVudCgpICsgZWxlbWVudE5hbWVzcGFjZSwgbW9kdWxlLmV2ZW50LmhpZGVHcmFjZWZ1bGx5KVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5vbiA9PSAnaG92ZXInICYmIG9wZW5lZFdpdGhUb3VjaCkge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQmluZGluZyBwb3B1cCBjbG9zZSBldmVudCB0byBkb2N1bWVudCcpO1xuICAgICAgICAgICAgICAkZG9jdW1lbnRcbiAgICAgICAgICAgICAgICAub24oJ3RvdWNoc3RhcnQnICsgZWxlbWVudE5hbWVzcGFjZSwgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdUb3VjaGVkIGF3YXkgZnJvbSBwb3B1cCcpO1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmV2ZW50LmhpZGVHcmFjZWZ1bGx5LmNhbGwoZWxlbWVudCwgZXZlbnQpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKHNldHRpbmdzLm9uID09ICdjbGljaycgJiYgc2V0dGluZ3MuY2xvc2FibGUpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0JpbmRpbmcgcG9wdXAgY2xvc2UgZXZlbnQgdG8gZG9jdW1lbnQnKTtcbiAgICAgICAgICAgICAgJGRvY3VtZW50XG4gICAgICAgICAgICAgICAgLm9uKCdjbGljaycgKyBlbGVtZW50TmFtZXNwYWNlLCBmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0NsaWNrZWQgYXdheSBmcm9tIHBvcHVwJyk7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuZXZlbnQuaGlkZUdyYWNlZnVsbHkuY2FsbChlbGVtZW50LCBldmVudCk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICB1bmJpbmQ6IHtcbiAgICAgICAgICBldmVudHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJHdpbmRvd1xuICAgICAgICAgICAgICAub2ZmKGVsZW1lbnROYW1lc3BhY2UpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5vZmYoZXZlbnROYW1lc3BhY2UpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBjbG9zZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkZG9jdW1lbnRcbiAgICAgICAgICAgICAgLm9mZihlbGVtZW50TmFtZXNwYWNlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJHNjcm9sbENvbnRleHRcbiAgICAgICAgICAgICAgLm9mZihlbGVtZW50TmFtZXNwYWNlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgIH0sXG5cbiAgICAgICAgaGFzOiB7XG4gICAgICAgICAgcG9wdXA6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICgkcG9wdXAgJiYgJHBvcHVwLmxlbmd0aCA+IDApO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBpczoge1xuICAgICAgICAgIG9mZnN0YWdlOiBmdW5jdGlvbihkaXN0YW5jZUZyb21Cb3VuZGFyeSwgcG9zaXRpb24pIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBvZmZzdGFnZSA9IFtdXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICAvLyByZXR1cm4gYm91bmRhcmllcyB0aGF0IGhhdmUgYmVlbiBzdXJwYXNzZWRcbiAgICAgICAgICAgICQuZWFjaChkaXN0YW5jZUZyb21Cb3VuZGFyeSwgZnVuY3Rpb24oZGlyZWN0aW9uLCBkaXN0YW5jZSkge1xuICAgICAgICAgICAgICBpZihkaXN0YW5jZSA8IC1zZXR0aW5ncy5qaXR0ZXIpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Bvc2l0aW9uIGV4Y2VlZHMgYWxsb3dhYmxlIGRpc3RhbmNlIGZyb20gZWRnZScsIGRpcmVjdGlvbiwgZGlzdGFuY2UsIHBvc2l0aW9uKTtcbiAgICAgICAgICAgICAgICBvZmZzdGFnZS5wdXNoKGRpcmVjdGlvbik7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaWYob2Zmc3RhZ2UubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzdmc6IGZ1bmN0aW9uKGVsZW1lbnQpIHtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGUuc3VwcG9ydHMuc3ZnKCkgJiYgKGVsZW1lbnQgaW5zdGFuY2VvZiBTVkdHcmFwaGljc0VsZW1lbnQpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYWN0aXZlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkbW9kdWxlLmhhc0NsYXNzKGNsYXNzTmFtZS5hY3RpdmUpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYW5pbWF0aW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAoJHBvcHVwICE9PSB1bmRlZmluZWQgJiYgJHBvcHVwLmhhc0NsYXNzKGNsYXNzTmFtZS5hbmltYXRpbmcpICk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBmbHVpZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gKCRwb3B1cCAhPT0gdW5kZWZpbmVkICYmICRwb3B1cC5oYXNDbGFzcyhjbGFzc05hbWUuZmx1aWQpKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHZpc2libGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICgkcG9wdXAgIT09IHVuZGVmaW5lZCAmJiAkcG9wdXAuaGFzQ2xhc3MoY2xhc3NOYW1lLnZpc2libGUpKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRyb3Bkb3duOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkbW9kdWxlLmhhc0NsYXNzKGNsYXNzTmFtZS5kcm9wZG93bik7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBoaWRkZW46IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICFtb2R1bGUuaXMudmlzaWJsZSgpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcnRsOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5jc3MoJ2RpcmVjdGlvbicpID09ICdydGwnO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICByZXNldDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnJlbW92ZS52aXNpYmxlKCk7XG4gICAgICAgICAgaWYoc2V0dGluZ3MucHJlc2VydmUpIHtcbiAgICAgICAgICAgIGlmKCQuZm4udHJhbnNpdGlvbiAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICRwb3B1cFxuICAgICAgICAgICAgICAgIC50cmFuc2l0aW9uKCdyZW1vdmUgdHJhbnNpdGlvbicpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBtb2R1bGUucmVtb3ZlUG9wdXAoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0dGluZzogZnVuY3Rpb24obmFtZSwgdmFsdWUpIHtcbiAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgc2V0dGluZ3MsIG5hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHNldHRpbmdzW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHNldHRpbmdzW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgaW50ZXJuYWw6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChuYW1lKSApIHtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIG1vZHVsZSwgbmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgbW9kdWxlW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZVtuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGRlYnVnOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50ICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UubG9nKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmluZm8sIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHZlcmJvc2U6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQgJiYgc2V0dGluZ3MudmVyYm9zZSAmJiBzZXR0aW5ncy5kZWJ1Zykge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmluZm8sIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZS5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQpIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvciA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5lcnJvciwgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHBlcmZvcm1hbmNlOiB7XG4gICAgICAgICAgbG9nOiBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgY3VycmVudFRpbWUsXG4gICAgICAgICAgICAgIGV4ZWN1dGlvblRpbWUsXG4gICAgICAgICAgICAgIHByZXZpb3VzVGltZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgY3VycmVudFRpbWUgICA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgICAgICAgICAgICBwcmV2aW91c1RpbWUgID0gdGltZSB8fCBjdXJyZW50VGltZTtcbiAgICAgICAgICAgICAgZXhlY3V0aW9uVGltZSA9IGN1cnJlbnRUaW1lIC0gcHJldmlvdXNUaW1lO1xuICAgICAgICAgICAgICB0aW1lICAgICAgICAgID0gY3VycmVudFRpbWU7XG4gICAgICAgICAgICAgIHBlcmZvcm1hbmNlLnB1c2goe1xuICAgICAgICAgICAgICAgICdOYW1lJyAgICAgICAgICAgOiBtZXNzYWdlWzBdLFxuICAgICAgICAgICAgICAgICdBcmd1bWVudHMnICAgICAgOiBbXS5zbGljZS5jYWxsKG1lc3NhZ2UsIDEpIHx8ICcnLFxuICAgICAgICAgICAgICAgICdFbGVtZW50JyAgICAgICAgOiBlbGVtZW50LFxuICAgICAgICAgICAgICAgICdFeGVjdXRpb24gVGltZScgOiBleGVjdXRpb25UaW1lXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lcik7XG4gICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UudGltZXIgPSBzZXRUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS5kaXNwbGF5LCA1MDApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZGlzcGxheTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgdGl0bGUgPSBzZXR0aW5ncy5uYW1lICsgJzonLFxuICAgICAgICAgICAgICB0b3RhbFRpbWUgPSAwXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICB0aW1lID0gZmFsc2U7XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgdG90YWxUaW1lICs9IGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ107XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHRpdGxlICs9ICcgJyArIHRvdGFsVGltZSArICdtcyc7XG4gICAgICAgICAgICBpZihtb2R1bGVTZWxlY3Rvcikge1xuICAgICAgICAgICAgICB0aXRsZSArPSAnIFxcJycgKyBtb2R1bGVTZWxlY3RvciArICdcXCcnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoIChjb25zb2xlLmdyb3VwICE9PSB1bmRlZmluZWQgfHwgY29uc29sZS50YWJsZSAhPT0gdW5kZWZpbmVkKSAmJiBwZXJmb3JtYW5jZS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBDb2xsYXBzZWQodGl0bGUpO1xuICAgICAgICAgICAgICBpZihjb25zb2xlLnRhYmxlKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS50YWJsZShwZXJmb3JtYW5jZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YVsnTmFtZSddICsgJzogJyArIGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ10rJ21zJyk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgY29uc29sZS5ncm91cEVuZCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcGVyZm9ybWFuY2UgPSBbXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludm9rZTogZnVuY3Rpb24ocXVlcnksIHBhc3NlZEFyZ3VtZW50cywgY29udGV4dCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgb2JqZWN0ID0gaW5zdGFuY2UsXG4gICAgICAgICAgICBtYXhEZXB0aCxcbiAgICAgICAgICAgIGZvdW5kLFxuICAgICAgICAgICAgcmVzcG9uc2VcbiAgICAgICAgICA7XG4gICAgICAgICAgcGFzc2VkQXJndW1lbnRzID0gcGFzc2VkQXJndW1lbnRzIHx8IHF1ZXJ5QXJndW1lbnRzO1xuICAgICAgICAgIGNvbnRleHQgICAgICAgICA9IGVsZW1lbnQgICAgICAgICB8fCBjb250ZXh0O1xuICAgICAgICAgIGlmKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyAmJiBvYmplY3QgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnkgICAgPSBxdWVyeS5zcGxpdCgvW1xcLiBdLyk7XG4gICAgICAgICAgICBtYXhEZXB0aCA9IHF1ZXJ5Lmxlbmd0aCAtIDE7XG4gICAgICAgICAgICAkLmVhY2gocXVlcnksIGZ1bmN0aW9uKGRlcHRoLCB2YWx1ZSkge1xuICAgICAgICAgICAgICB2YXIgY2FtZWxDYXNlVmFsdWUgPSAoZGVwdGggIT0gbWF4RGVwdGgpXG4gICAgICAgICAgICAgICAgPyB2YWx1ZSArIHF1ZXJ5W2RlcHRoICsgMV0uY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBxdWVyeVtkZXB0aCArIDFdLnNsaWNlKDEpXG4gICAgICAgICAgICAgICAgOiBxdWVyeVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gKSAmJiAoZGVwdGggIT0gbWF4RGVwdGgpICkge1xuICAgICAgICAgICAgICAgIG9iamVjdCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgICAgIGZvdW5kID0gb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbdmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFt2YWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFt2YWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmICggJC5pc0Z1bmN0aW9uKCBmb3VuZCApICkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZC5hcHBseShjb250ZXh0LCBwYXNzZWRBcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKGZvdW5kICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gZm91bmQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKCQuaXNBcnJheShyZXR1cm5lZFZhbHVlKSkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZS5wdXNoKHJlc3BvbnNlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSBbcmV0dXJuZWRWYWx1ZSwgcmVzcG9uc2VdO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJlc3BvbnNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSByZXNwb25zZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGZvdW5kO1xuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICBpZihtZXRob2RJbnZva2VkKSB7XG4gICAgICAgIGlmKGluc3RhbmNlID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbnZva2UocXVlcnkpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGlmKGluc3RhbmNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBpbnN0YW5jZS5pbnZva2UoJ2Rlc3Ryb3knKTtcbiAgICAgICAgfVxuICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgfVxuICAgIH0pXG4gIDtcblxuICByZXR1cm4gKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZClcbiAgICA/IHJldHVybmVkVmFsdWVcbiAgICA6IHRoaXNcbiAgO1xufTtcblxuJC5mbi5wb3B1cC5zZXR0aW5ncyA9IHtcblxuICBuYW1lICAgICAgICAgICA6ICdQb3B1cCcsXG5cbiAgLy8gbW9kdWxlIHNldHRpbmdzXG4gIHNpbGVudCAgICAgICAgIDogZmFsc2UsXG4gIGRlYnVnICAgICAgICAgIDogZmFsc2UsXG4gIHZlcmJvc2UgICAgICAgIDogZmFsc2UsXG4gIHBlcmZvcm1hbmNlICAgIDogdHJ1ZSxcbiAgbmFtZXNwYWNlICAgICAgOiAncG9wdXAnLFxuXG4gIC8vIHdoZXRoZXIgaXQgc2hvdWxkIHVzZSBkb20gbXV0YXRpb24gb2JzZXJ2ZXJzXG4gIG9ic2VydmVDaGFuZ2VzIDogdHJ1ZSxcblxuICAvLyBjYWxsYmFjayBvbmx5IHdoZW4gZWxlbWVudCBhZGRlZCB0byBkb21cbiAgb25DcmVhdGUgICAgICAgOiBmdW5jdGlvbigpe30sXG5cbiAgLy8gY2FsbGJhY2sgYmVmb3JlIGVsZW1lbnQgcmVtb3ZlZCBmcm9tIGRvbVxuICBvblJlbW92ZSAgICAgICA6IGZ1bmN0aW9uKCl7fSxcblxuICAvLyBjYWxsYmFjayBiZWZvcmUgc2hvdyBhbmltYXRpb25cbiAgb25TaG93ICAgICAgICAgOiBmdW5jdGlvbigpe30sXG5cbiAgLy8gY2FsbGJhY2sgYWZ0ZXIgc2hvdyBhbmltYXRpb25cbiAgb25WaXNpYmxlICAgICAgOiBmdW5jdGlvbigpe30sXG5cbiAgLy8gY2FsbGJhY2sgYmVmb3JlIGhpZGUgYW5pbWF0aW9uXG4gIG9uSGlkZSAgICAgICAgIDogZnVuY3Rpb24oKXt9LFxuXG4gIC8vIGNhbGxiYWNrIHdoZW4gcG9wdXAgY2Fubm90IGJlIHBvc2l0aW9uZWQgaW4gdmlzaWJsZSBzY3JlZW5cbiAgb25VbnBsYWNlYWJsZSAgOiBmdW5jdGlvbigpe30sXG5cbiAgLy8gY2FsbGJhY2sgYWZ0ZXIgaGlkZSBhbmltYXRpb25cbiAgb25IaWRkZW4gICAgICAgOiBmdW5jdGlvbigpe30sXG5cbiAgLy8gd2hlbiB0byBzaG93IHBvcHVwXG4gIG9uICAgICAgICAgICAgIDogJ2hvdmVyJyxcblxuICAvLyBlbGVtZW50IHRvIHVzZSB0byBkZXRlcm1pbmUgaWYgcG9wdXAgaXMgb3V0IG9mIGJvdW5kYXJ5XG4gIGJvdW5kYXJ5ICAgICAgIDogd2luZG93LFxuXG4gIC8vIHdoZXRoZXIgdG8gYWRkIHRvdWNoc3RhcnQgZXZlbnRzIHdoZW4gdXNpbmcgaG92ZXJcbiAgYWRkVG91Y2hFdmVudHMgOiB0cnVlLFxuXG4gIC8vIGRlZmF1bHQgcG9zaXRpb24gcmVsYXRpdmUgdG8gZWxlbWVudFxuICBwb3NpdGlvbiAgICAgICA6ICd0b3AgbGVmdCcsXG5cbiAgLy8gbmFtZSBvZiB2YXJpYXRpb24gdG8gdXNlXG4gIHZhcmlhdGlvbiAgICAgIDogJycsXG5cbiAgLy8gd2hldGhlciBwb3B1cCBzaG91bGQgYmUgbW92ZWQgdG8gY29udGV4dFxuICBtb3ZlUG9wdXAgICAgICA6IHRydWUsXG5cbiAgLy8gZWxlbWVudCB3aGljaCBwb3B1cCBzaG91bGQgYmUgcmVsYXRpdmUgdG9cbiAgdGFyZ2V0ICAgICAgICAgOiBmYWxzZSxcblxuICAvLyBqcSBzZWxlY3RvciBvciBlbGVtZW50IHRoYXQgc2hvdWxkIGJlIHVzZWQgYXMgcG9wdXBcbiAgcG9wdXAgICAgICAgICAgOiBmYWxzZSxcblxuICAvLyBwb3B1cCBzaG91bGQgcmVtYWluIGlubGluZSBuZXh0IHRvIGFjdGl2YXRvclxuICBpbmxpbmUgICAgICAgICA6IGZhbHNlLFxuXG4gIC8vIHBvcHVwIHNob3VsZCBiZSByZW1vdmVkIGZyb20gcGFnZSBvbiBoaWRlXG4gIHByZXNlcnZlICAgICAgIDogZmFsc2UsXG5cbiAgLy8gcG9wdXAgc2hvdWxkIG5vdCBjbG9zZSB3aGVuIGJlaW5nIGhvdmVyZWQgb25cbiAgaG92ZXJhYmxlICAgICAgOiBmYWxzZSxcblxuICAvLyBleHBsaWNpdGx5IHNldCBjb250ZW50XG4gIGNvbnRlbnQgICAgICAgIDogZmFsc2UsXG5cbiAgLy8gZXhwbGljaXRseSBzZXQgaHRtbFxuICBodG1sICAgICAgICAgICA6IGZhbHNlLFxuXG4gIC8vIGV4cGxpY2l0bHkgc2V0IHRpdGxlXG4gIHRpdGxlICAgICAgICAgIDogZmFsc2UsXG5cbiAgLy8gd2hldGhlciBhdXRvbWF0aWNhbGx5IGNsb3NlIG9uIGNsaWNrYXdheSB3aGVuIG9uIGNsaWNrXG4gIGNsb3NhYmxlICAgICAgIDogdHJ1ZSxcblxuICAvLyBhdXRvbWF0aWNhbGx5IGhpZGUgb24gc2Nyb2xsXG4gIGhpZGVPblNjcm9sbCAgIDogJ2F1dG8nLFxuXG4gIC8vIGhpZGUgb3RoZXIgcG9wdXBzIG9uIHNob3dcbiAgZXhjbHVzaXZlICAgICAgOiBmYWxzZSxcblxuICAvLyBjb250ZXh0IHRvIGF0dGFjaCBwb3B1cHNcbiAgY29udGV4dCAgICAgICAgOiAnYm9keScsXG5cbiAgLy8gY29udGV4dCBmb3IgYmluZGluZyBzY3JvbGwgZXZlbnRzXG4gIHNjcm9sbENvbnRleHQgIDogd2luZG93LFxuXG4gIC8vIHBvc2l0aW9uIHRvIHByZWZlciB3aGVuIGNhbGN1bGF0aW5nIG5ldyBwb3NpdGlvblxuICBwcmVmZXIgICAgICAgICA6ICdvcHBvc2l0ZScsXG5cbiAgLy8gc3BlY2lmeSBwb3NpdGlvbiB0byBhcHBlYXIgZXZlbiBpZiBpdCBkb2Vzbid0IGZpdFxuICBsYXN0UmVzb3J0ICAgICA6IGZhbHNlLFxuXG4gIC8vIGRlbGF5IHVzZWQgdG8gcHJldmVudCBhY2NpZGVudGFsIHJlZmlyaW5nIG9mIGFuaW1hdGlvbnMgZHVlIHRvIHVzZXIgZXJyb3JcbiAgZGVsYXkgICAgICAgIDoge1xuICAgIHNob3cgOiA1MCxcbiAgICBoaWRlIDogNzBcbiAgfSxcblxuICAvLyB3aGV0aGVyIGZsdWlkIHZhcmlhdGlvbiBzaG91bGQgYXNzaWduIHdpZHRoIGV4cGxpY2l0bHlcbiAgc2V0Rmx1aWRXaWR0aCAgOiB0cnVlLFxuXG4gIC8vIHRyYW5zaXRpb24gc2V0dGluZ3NcbiAgZHVyYXRpb24gICAgICAgOiAyMDAsXG4gIHRyYW5zaXRpb24gICAgIDogJ3NjYWxlJyxcblxuICAvLyBkaXN0YW5jZSBhd2F5IGZyb20gYWN0aXZhdGluZyBlbGVtZW50IGluIHB4XG4gIGRpc3RhbmNlQXdheSAgIDogMCxcblxuICAvLyBudW1iZXIgb2YgcGl4ZWxzIGFuIGVsZW1lbnQgaXMgYWxsb3dlZCB0byBiZSBcIm9mZnN0YWdlXCIgZm9yIGEgcG9zaXRpb24gdG8gYmUgY2hvc2VuIChhbGxvd3MgZm9yIHJvdW5kaW5nKVxuICBqaXR0ZXIgICAgICAgICA6IDIsXG5cbiAgLy8gb2Zmc2V0IG9uIGFsaWduaW5nIGF4aXMgZnJvbSBjYWxjdWxhdGVkIHBvc2l0aW9uXG4gIG9mZnNldCAgICAgICAgIDogMCxcblxuICAvLyBtYXhpbXVtIHRpbWVzIHRvIGxvb2sgZm9yIGEgcG9zaXRpb24gYmVmb3JlIGZhaWxpbmcgKDkgcG9zaXRpb25zIHRvdGFsKVxuICBtYXhTZWFyY2hEZXB0aCA6IDE1LFxuXG4gIGVycm9yOiB7XG4gICAgaW52YWxpZFBvc2l0aW9uIDogJ1RoZSBwb3NpdGlvbiB5b3Ugc3BlY2lmaWVkIGlzIG5vdCBhIHZhbGlkIHBvc2l0aW9uJyxcbiAgICBjYW5ub3RQbGFjZSAgICAgOiAnUG9wdXAgZG9lcyBub3QgZml0IHdpdGhpbiB0aGUgYm91bmRhcmllcyBvZiB0aGUgdmlld3BvcnQnLFxuICAgIG1ldGhvZCAgICAgICAgICA6ICdUaGUgbWV0aG9kIHlvdSBjYWxsZWQgaXMgbm90IGRlZmluZWQuJyxcbiAgICBub1RyYW5zaXRpb24gICAgOiAnVGhpcyBtb2R1bGUgcmVxdWlyZXMgdWkgdHJhbnNpdGlvbnMgPGh0dHBzOi8vZ2l0aHViLmNvbS9TZW1hbnRpYy1PcmcvVUktVHJhbnNpdGlvbj4nLFxuICAgIG5vdEZvdW5kICAgICAgICA6ICdUaGUgdGFyZ2V0IG9yIHBvcHVwIHlvdSBzcGVjaWZpZWQgZG9lcyBub3QgZXhpc3Qgb24gdGhlIHBhZ2UnXG4gIH0sXG5cbiAgbWV0YWRhdGE6IHtcbiAgICBhY3RpdmF0b3IgOiAnYWN0aXZhdG9yJyxcbiAgICBjb250ZW50ICAgOiAnY29udGVudCcsXG4gICAgaHRtbCAgICAgIDogJ2h0bWwnLFxuICAgIG9mZnNldCAgICA6ICdvZmZzZXQnLFxuICAgIHBvc2l0aW9uICA6ICdwb3NpdGlvbicsXG4gICAgdGl0bGUgICAgIDogJ3RpdGxlJyxcbiAgICB2YXJpYXRpb24gOiAndmFyaWF0aW9uJ1xuICB9LFxuXG4gIGNsYXNzTmFtZSAgIDoge1xuICAgIGFjdGl2ZSAgICA6ICdhY3RpdmUnLFxuICAgIGFuaW1hdGluZyA6ICdhbmltYXRpbmcnLFxuICAgIGRyb3Bkb3duICA6ICdkcm9wZG93bicsXG4gICAgZmx1aWQgICAgIDogJ2ZsdWlkJyxcbiAgICBsb2FkaW5nICAgOiAnbG9hZGluZycsXG4gICAgcG9wdXAgICAgIDogJ3VpIHBvcHVwJyxcbiAgICBwb3NpdGlvbiAgOiAndG9wIGxlZnQgY2VudGVyIGJvdHRvbSByaWdodCcsXG4gICAgdmlzaWJsZSAgIDogJ3Zpc2libGUnXG4gIH0sXG5cbiAgc2VsZWN0b3IgICAgOiB7XG4gICAgcG9wdXAgICAgOiAnLnVpLnBvcHVwJ1xuICB9LFxuXG4gIHRlbXBsYXRlczoge1xuICAgIGVzY2FwZTogZnVuY3Rpb24oc3RyaW5nKSB7XG4gICAgICB2YXJcbiAgICAgICAgYmFkQ2hhcnMgICAgID0gL1smPD5cIidgXS9nLFxuICAgICAgICBzaG91bGRFc2NhcGUgPSAvWyY8PlwiJ2BdLyxcbiAgICAgICAgZXNjYXBlICAgICAgID0ge1xuICAgICAgICAgIFwiJlwiOiBcIiZhbXA7XCIsXG4gICAgICAgICAgXCI8XCI6IFwiJmx0O1wiLFxuICAgICAgICAgIFwiPlwiOiBcIiZndDtcIixcbiAgICAgICAgICAnXCInOiBcIiZxdW90O1wiLFxuICAgICAgICAgIFwiJ1wiOiBcIiYjeDI3O1wiLFxuICAgICAgICAgIFwiYFwiOiBcIiYjeDYwO1wiXG4gICAgICAgIH0sXG4gICAgICAgIGVzY2FwZWRDaGFyICA9IGZ1bmN0aW9uKGNocikge1xuICAgICAgICAgIHJldHVybiBlc2NhcGVbY2hyXTtcbiAgICAgICAgfVxuICAgICAgO1xuICAgICAgaWYoc2hvdWxkRXNjYXBlLnRlc3Qoc3RyaW5nKSkge1xuICAgICAgICByZXR1cm4gc3RyaW5nLnJlcGxhY2UoYmFkQ2hhcnMsIGVzY2FwZWRDaGFyKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBzdHJpbmc7XG4gICAgfSxcbiAgICBwb3B1cDogZnVuY3Rpb24odGV4dCkge1xuICAgICAgdmFyXG4gICAgICAgIGh0bWwgICA9ICcnLFxuICAgICAgICBlc2NhcGUgPSAkLmZuLnBvcHVwLnNldHRpbmdzLnRlbXBsYXRlcy5lc2NhcGVcbiAgICAgIDtcbiAgICAgIGlmKHR5cGVvZiB0ZXh0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgaWYodHlwZW9mIHRleHQudGl0bGUgIT09IHVuZGVmaW5lZCAmJiB0ZXh0LnRpdGxlKSB7XG4gICAgICAgICAgdGV4dC50aXRsZSA9IGVzY2FwZSh0ZXh0LnRpdGxlKTtcbiAgICAgICAgICBodG1sICs9ICc8ZGl2IGNsYXNzPVwiaGVhZGVyXCI+JyArIHRleHQudGl0bGUgKyAnPC9kaXY+JztcbiAgICAgICAgfVxuICAgICAgICBpZih0eXBlb2YgdGV4dC5jb250ZW50ICE9PSB1bmRlZmluZWQgJiYgdGV4dC5jb250ZW50KSB7XG4gICAgICAgICAgdGV4dC5jb250ZW50ID0gZXNjYXBlKHRleHQuY29udGVudCk7XG4gICAgICAgICAgaHRtbCArPSAnPGRpdiBjbGFzcz1cImNvbnRlbnRcIj4nICsgdGV4dC5jb250ZW50ICsgJzwvZGl2Pic7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBodG1sO1xuICAgIH1cbiAgfVxuXG59O1xuXG5cbn0pKCBqUXVlcnksIHdpbmRvdywgZG9jdW1lbnQgKTtcbiJdfQ==