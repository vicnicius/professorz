/*!
 * # Semantic UI - Progress
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  var global = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.progress = function (parameters) {
    var $allModules = $(this),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue;

    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.progress.settings, parameters) : $.extend({}, $.fn.progress.settings),
          className = settings.className,
          metadata = settings.metadata,
          namespace = settings.namespace,
          selector = settings.selector,
          error = settings.error,
          eventNamespace = '.' + namespace,
          moduleNamespace = 'module-' + namespace,
          $module = $(this),
          $bar = $(this).find(selector.bar),
          $progress = $(this).find(selector.progress),
          $label = $(this).find(selector.label),
          element = this,
          instance = $module.data(moduleNamespace),
          animating = false,
          transitionEnd,
          module;

      module = {

        initialize: function () {
          module.debug('Initializing progress bar', settings);

          module.set.duration();
          module.set.transitionEvent();

          module.read.metadata();
          module.read.settings();

          module.instantiate();
        },

        instantiate: function () {
          module.verbose('Storing instance of progress', module);
          instance = module;
          $module.data(moduleNamespace, module);
        },
        destroy: function () {
          module.verbose('Destroying previous progress for', $module);
          clearInterval(instance.interval);
          module.remove.state();
          $module.removeData(moduleNamespace);
          instance = undefined;
        },

        reset: function () {
          module.remove.nextValue();
          module.update.progress(0);
        },

        complete: function () {
          if (module.percent === undefined || module.percent < 100) {
            module.remove.progressPoll();
            module.set.percent(100);
          }
        },

        read: {
          metadata: function () {
            var data = {
              percent: $module.data(metadata.percent),
              total: $module.data(metadata.total),
              value: $module.data(metadata.value)
            };
            if (data.percent) {
              module.debug('Current percent value set from metadata', data.percent);
              module.set.percent(data.percent);
            }
            if (data.total) {
              module.debug('Total value set from metadata', data.total);
              module.set.total(data.total);
            }
            if (data.value) {
              module.debug('Current value set from metadata', data.value);
              module.set.value(data.value);
              module.set.progress(data.value);
            }
          },
          settings: function () {
            if (settings.total !== false) {
              module.debug('Current total set in settings', settings.total);
              module.set.total(settings.total);
            }
            if (settings.value !== false) {
              module.debug('Current value set in settings', settings.value);
              module.set.value(settings.value);
              module.set.progress(module.value);
            }
            if (settings.percent !== false) {
              module.debug('Current percent set in settings', settings.percent);
              module.set.percent(settings.percent);
            }
          }
        },

        increment: function (incrementValue) {
          var maxValue, startValue, newValue;
          if (module.has.total()) {
            startValue = module.get.value();
            incrementValue = incrementValue || 1;
            newValue = startValue + incrementValue;
          } else {
            startValue = module.get.percent();
            incrementValue = incrementValue || module.get.randomValue();

            newValue = startValue + incrementValue;
            maxValue = 100;
            module.debug('Incrementing percentage by', startValue, newValue);
          }
          newValue = module.get.normalizedValue(newValue);
          module.set.progress(newValue);
        },
        decrement: function (decrementValue) {
          var total = module.get.total(),
              startValue,
              newValue;
          if (total) {
            startValue = module.get.value();
            decrementValue = decrementValue || 1;
            newValue = startValue - decrementValue;
            module.debug('Decrementing value by', decrementValue, startValue);
          } else {
            startValue = module.get.percent();
            decrementValue = decrementValue || module.get.randomValue();
            newValue = startValue - decrementValue;
            module.debug('Decrementing percentage by', decrementValue, startValue);
          }
          newValue = module.get.normalizedValue(newValue);
          module.set.progress(newValue);
        },

        has: {
          progressPoll: function () {
            return module.progressPoll;
          },
          total: function () {
            return module.get.total() !== false;
          }
        },

        get: {
          text: function (templateText) {
            var value = module.value || 0,
                total = module.total || 0,
                percent = animating ? module.get.displayPercent() : module.percent || 0,
                left = module.total > 0 ? total - value : 100 - percent;
            templateText = templateText || '';
            templateText = templateText.replace('{value}', value).replace('{total}', total).replace('{left}', left).replace('{percent}', percent);
            module.verbose('Adding variables to progress bar text', templateText);
            return templateText;
          },

          normalizedValue: function (value) {
            if (value < 0) {
              module.debug('Value cannot decrement below 0');
              return 0;
            }
            if (module.has.total()) {
              if (value > module.total) {
                module.debug('Value cannot increment above total', module.total);
                return module.total;
              }
            } else if (value > 100) {
              module.debug('Value cannot increment above 100 percent');
              return 100;
            }
            return value;
          },

          updateInterval: function () {
            if (settings.updateInterval == 'auto') {
              return settings.duration;
            }
            return settings.updateInterval;
          },

          randomValue: function () {
            module.debug('Generating random increment percentage');
            return Math.floor(Math.random() * settings.random.max + settings.random.min);
          },

          numericValue: function (value) {
            return typeof value === 'string' ? value.replace(/[^\d.]/g, '') !== '' ? +value.replace(/[^\d.]/g, '') : false : value;
          },

          transitionEnd: function () {
            var element = document.createElement('element'),
                transitions = {
              'transition': 'transitionend',
              'OTransition': 'oTransitionEnd',
              'MozTransition': 'transitionend',
              'WebkitTransition': 'webkitTransitionEnd'
            },
                transition;
            for (transition in transitions) {
              if (element.style[transition] !== undefined) {
                return transitions[transition];
              }
            }
          },

          // gets current displayed percentage (if animating values this is the intermediary value)
          displayPercent: function () {
            var barWidth = $bar.width(),
                totalWidth = $module.width(),
                minDisplay = parseInt($bar.css('min-width'), 10),
                displayPercent = barWidth > minDisplay ? barWidth / totalWidth * 100 : module.percent;
            return settings.precision > 0 ? Math.round(displayPercent * (10 * settings.precision)) / (10 * settings.precision) : Math.round(displayPercent);
          },

          percent: function () {
            return module.percent || 0;
          },
          value: function () {
            return module.nextValue || module.value || 0;
          },
          total: function () {
            return module.total || false;
          }
        },

        create: {
          progressPoll: function () {
            module.progressPoll = setTimeout(function () {
              module.update.toNextValue();
              module.remove.progressPoll();
            }, module.get.updateInterval());
          }
        },

        is: {
          complete: function () {
            return module.is.success() || module.is.warning() || module.is.error();
          },
          success: function () {
            return $module.hasClass(className.success);
          },
          warning: function () {
            return $module.hasClass(className.warning);
          },
          error: function () {
            return $module.hasClass(className.error);
          },
          active: function () {
            return $module.hasClass(className.active);
          },
          visible: function () {
            return $module.is(':visible');
          }
        },

        remove: {
          progressPoll: function () {
            module.verbose('Removing progress poll timer');
            if (module.progressPoll) {
              clearTimeout(module.progressPoll);
              delete module.progressPoll;
            }
          },
          nextValue: function () {
            module.verbose('Removing progress value stored for next update');
            delete module.nextValue;
          },
          state: function () {
            module.verbose('Removing stored state');
            delete module.total;
            delete module.percent;
            delete module.value;
          },
          active: function () {
            module.verbose('Removing active state');
            $module.removeClass(className.active);
          },
          success: function () {
            module.verbose('Removing success state');
            $module.removeClass(className.success);
          },
          warning: function () {
            module.verbose('Removing warning state');
            $module.removeClass(className.warning);
          },
          error: function () {
            module.verbose('Removing error state');
            $module.removeClass(className.error);
          }
        },

        set: {
          barWidth: function (value) {
            if (value > 100) {
              module.error(error.tooHigh, value);
            } else if (value < 0) {
              module.error(error.tooLow, value);
            } else {
              $bar.css('width', value + '%');
              $module.attr('data-percent', parseInt(value, 10));
            }
          },
          duration: function (duration) {
            duration = duration || settings.duration;
            duration = typeof duration == 'number' ? duration + 'ms' : duration;
            module.verbose('Setting progress bar transition duration', duration);
            $bar.css({
              'transition-duration': duration
            });
          },
          percent: function (percent) {
            percent = typeof percent == 'string' ? +percent.replace('%', '') : percent;
            // round display percentage
            percent = settings.precision > 0 ? Math.round(percent * (10 * settings.precision)) / (10 * settings.precision) : Math.round(percent);
            module.percent = percent;
            if (!module.has.total()) {
              module.value = settings.precision > 0 ? Math.round(percent / 100 * module.total * (10 * settings.precision)) / (10 * settings.precision) : Math.round(percent / 100 * module.total * 10) / 10;
              if (settings.limitValues) {
                module.value = module.value > 100 ? 100 : module.value < 0 ? 0 : module.value;
              }
            }
            module.set.barWidth(percent);
            module.set.labelInterval();
            module.set.labels();
            settings.onChange.call(element, percent, module.value, module.total);
          },
          labelInterval: function () {
            var animationCallback = function () {
              module.verbose('Bar finished animating, removing continuous label updates');
              clearInterval(module.interval);
              animating = false;
              module.set.labels();
            };
            clearInterval(module.interval);
            $bar.one(transitionEnd + eventNamespace, animationCallback);
            animating = true;
            module.interval = setInterval(function () {
              var isInDOM = $.contains(document.documentElement, element);
              if (!isInDOM) {
                clearInterval(module.interval);
                animating = false;
              }
              module.set.labels();
            }, settings.framerate);
          },
          labels: function () {
            module.verbose('Setting both bar progress and outer label text');
            module.set.barLabel();
            module.set.state();
          },
          label: function (text) {
            text = text || '';
            if (text) {
              text = module.get.text(text);
              module.verbose('Setting label to text', text);
              $label.text(text);
            }
          },
          state: function (percent) {
            percent = percent !== undefined ? percent : module.percent;
            if (percent === 100) {
              if (settings.autoSuccess && !(module.is.warning() || module.is.error() || module.is.success())) {
                module.set.success();
                module.debug('Automatically triggering success at 100%');
              } else {
                module.verbose('Reached 100% removing active state');
                module.remove.active();
                module.remove.progressPoll();
              }
            } else if (percent > 0) {
              module.verbose('Adjusting active progress bar label', percent);
              module.set.active();
            } else {
              module.remove.active();
              module.set.label(settings.text.active);
            }
          },
          barLabel: function (text) {
            if (text !== undefined) {
              $progress.text(module.get.text(text));
            } else if (settings.label == 'ratio' && module.total) {
              module.verbose('Adding ratio to bar label');
              $progress.text(module.get.text(settings.text.ratio));
            } else if (settings.label == 'percent') {
              module.verbose('Adding percentage to bar label');
              $progress.text(module.get.text(settings.text.percent));
            }
          },
          active: function (text) {
            text = text || settings.text.active;
            module.debug('Setting active state');
            if (settings.showActivity && !module.is.active()) {
              $module.addClass(className.active);
            }
            module.remove.warning();
            module.remove.error();
            module.remove.success();
            text = settings.onLabelUpdate('active', text, module.value, module.total);
            if (text) {
              module.set.label(text);
            }
            $bar.one(transitionEnd + eventNamespace, function () {
              settings.onActive.call(element, module.value, module.total);
            });
          },
          success: function (text) {
            text = text || settings.text.success || settings.text.active;
            module.debug('Setting success state');
            $module.addClass(className.success);
            module.remove.active();
            module.remove.warning();
            module.remove.error();
            module.complete();
            if (settings.text.success) {
              text = settings.onLabelUpdate('success', text, module.value, module.total);
              module.set.label(text);
            } else {
              text = settings.onLabelUpdate('active', text, module.value, module.total);
              module.set.label(text);
            }
            $bar.one(transitionEnd + eventNamespace, function () {
              settings.onSuccess.call(element, module.total);
            });
          },
          warning: function (text) {
            text = text || settings.text.warning;
            module.debug('Setting warning state');
            $module.addClass(className.warning);
            module.remove.active();
            module.remove.success();
            module.remove.error();
            module.complete();
            text = settings.onLabelUpdate('warning', text, module.value, module.total);
            if (text) {
              module.set.label(text);
            }
            $bar.one(transitionEnd + eventNamespace, function () {
              settings.onWarning.call(element, module.value, module.total);
            });
          },
          error: function (text) {
            text = text || settings.text.error;
            module.debug('Setting error state');
            $module.addClass(className.error);
            module.remove.active();
            module.remove.success();
            module.remove.warning();
            module.complete();
            text = settings.onLabelUpdate('error', text, module.value, module.total);
            if (text) {
              module.set.label(text);
            }
            $bar.one(transitionEnd + eventNamespace, function () {
              settings.onError.call(element, module.value, module.total);
            });
          },
          transitionEvent: function () {
            transitionEnd = module.get.transitionEnd();
          },
          total: function (totalValue) {
            module.total = totalValue;
          },
          value: function (value) {
            module.value = value;
          },
          progress: function (value) {
            if (!module.has.progressPoll()) {
              module.debug('First update in progress update interval, immediately updating', value);
              module.update.progress(value);
              module.create.progressPoll();
            } else {
              module.debug('Updated within interval, setting next update to use new value', value);
              module.set.nextValue(value);
            }
          },
          nextValue: function (value) {
            module.nextValue = value;
          }
        },

        update: {
          toNextValue: function () {
            var nextValue = module.nextValue;
            if (nextValue) {
              module.debug('Update interval complete using last updated value', nextValue);
              module.update.progress(nextValue);
              module.remove.nextValue();
            }
          },
          progress: function (value) {
            var percentComplete;
            value = module.get.numericValue(value);
            if (value === false) {
              module.error(error.nonNumeric, value);
            }
            value = module.get.normalizedValue(value);
            if (module.has.total()) {
              module.set.value(value);
              percentComplete = value / module.total * 100;
              module.debug('Calculating percent complete from total', percentComplete);
              module.set.percent(percentComplete);
            } else {
              percentComplete = value;
              module.debug('Setting value to exact percentage value', percentComplete);
              module.set.percent(percentComplete);
            }
          }
        },

        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                module.error(error.method, query);
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };

      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.progress.settings = {

    name: 'Progress',
    namespace: 'progress',

    silent: false,
    debug: false,
    verbose: false,
    performance: true,

    random: {
      min: 2,
      max: 5
    },

    duration: 300,

    updateInterval: 'auto',

    autoSuccess: true,
    showActivity: true,
    limitValues: true,

    label: 'percent',
    precision: 0,
    framerate: 1000 / 30, /// 30 fps

    percent: false,
    total: false,
    value: false,

    onLabelUpdate: function (state, text, value, total) {
      return text;
    },
    onChange: function (percent, value, total) {},
    onSuccess: function (total) {},
    onActive: function (value, total) {},
    onError: function (value, total) {},
    onWarning: function (value, total) {},

    error: {
      method: 'The method you called is not defined.',
      nonNumeric: 'Progress value is non numeric',
      tooHigh: 'Value specified is above 100%',
      tooLow: 'Value specified is below 0%'
    },

    regExp: {
      variable: /\{\$*[A-z0-9]+\}/g
    },

    metadata: {
      percent: 'percent',
      total: 'total',
      value: 'value'
    },

    selector: {
      bar: '> .bar',
      label: '> .label',
      progress: '.bar > .progress'
    },

    text: {
      active: false,
      error: false,
      success: false,
      warning: false,
      percent: '{percent}%',
      ratio: '{value} of {total}'
    },

    className: {
      active: 'active',
      error: 'error',
      success: 'success',
      warning: 'warning'
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL3Byb2dyZXNzLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFVQSxDQUFDLENBQUMsVUFBVSxDQUFWLEVBQWEsTUFBYixFQUFxQixRQUFyQixFQUErQixTQUEvQixFQUEwQzs7QUFFNUM7O0FBRUEsV0FBVSxPQUFPLE1BQVAsSUFBaUIsV0FBakIsSUFBZ0MsT0FBTyxJQUFQLElBQWUsSUFBaEQsR0FDTCxNQURLLEdBRUosT0FBTyxJQUFQLElBQWUsV0FBZixJQUE4QixLQUFLLElBQUwsSUFBYSxJQUE1QyxHQUNFLElBREYsR0FFRSxTQUFTLGFBQVQsR0FKTjs7QUFPQSxNQUNFLFNBQVUsT0FBTyxNQUFQLElBQWlCLFdBQWpCLElBQWdDLE9BQU8sSUFBUCxJQUFlLElBQWhELEdBQ0wsTUFESyxHQUVKLE9BQU8sSUFBUCxJQUFlLFdBQWYsSUFBOEIsS0FBSyxJQUFMLElBQWEsSUFBNUMsR0FDRSxJQURGLEdBRUUsU0FBUyxhQUFULEdBTFI7O0FBUUEsSUFBRSxFQUFGLENBQUssUUFBTCxHQUFnQixVQUFTLFVBQVQsRUFBcUI7QUFDbkMsUUFDRSxjQUFpQixFQUFFLElBQUYsQ0FEbkI7QUFBQSxRQUdFLGlCQUFpQixZQUFZLFFBQVosSUFBd0IsRUFIM0M7QUFBQSxRQUtFLE9BQWlCLElBQUksSUFBSixHQUFXLE9BQVgsRUFMbkI7QUFBQSxRQU1FLGNBQWlCLEVBTm5CO0FBQUEsUUFRRSxRQUFpQixVQUFVLENBQVYsQ0FSbkI7QUFBQSxRQVNFLGdCQUFrQixPQUFPLEtBQVAsSUFBZ0IsUUFUcEM7QUFBQSxRQVVFLGlCQUFpQixHQUFHLEtBQUgsQ0FBUyxJQUFULENBQWMsU0FBZCxFQUF5QixDQUF6QixDQVZuQjtBQUFBLFFBWUUsYUFaRjs7QUFlQSxnQkFDRyxJQURILENBQ1EsWUFBVztBQUNmLFVBQ0UsV0FBc0IsRUFBRSxhQUFGLENBQWdCLFVBQWhCLENBQUYsR0FDaEIsRUFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUIsRUFBRSxFQUFGLENBQUssUUFBTCxDQUFjLFFBQWpDLEVBQTJDLFVBQTNDLENBRGdCLEdBRWhCLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxFQUFFLEVBQUYsQ0FBSyxRQUFMLENBQWMsUUFBM0IsQ0FITjtBQUFBLFVBS0UsWUFBa0IsU0FBUyxTQUw3QjtBQUFBLFVBTUUsV0FBa0IsU0FBUyxRQU43QjtBQUFBLFVBT0UsWUFBa0IsU0FBUyxTQVA3QjtBQUFBLFVBUUUsV0FBa0IsU0FBUyxRQVI3QjtBQUFBLFVBU0UsUUFBa0IsU0FBUyxLQVQ3QjtBQUFBLFVBV0UsaUJBQWtCLE1BQU0sU0FYMUI7QUFBQSxVQVlFLGtCQUFrQixZQUFZLFNBWmhDO0FBQUEsVUFjRSxVQUFrQixFQUFFLElBQUYsQ0FkcEI7QUFBQSxVQWVFLE9BQWtCLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxTQUFTLEdBQXRCLENBZnBCO0FBQUEsVUFnQkUsWUFBa0IsRUFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLFNBQVMsUUFBdEIsQ0FoQnBCO0FBQUEsVUFpQkUsU0FBa0IsRUFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLFNBQVMsS0FBdEIsQ0FqQnBCO0FBQUEsVUFtQkUsVUFBa0IsSUFuQnBCO0FBQUEsVUFvQkUsV0FBa0IsUUFBUSxJQUFSLENBQWEsZUFBYixDQXBCcEI7QUFBQSxVQXNCRSxZQUFZLEtBdEJkO0FBQUEsVUF1QkUsYUF2QkY7QUFBQSxVQXdCRSxNQXhCRjs7QUEyQkEsZUFBUzs7QUFFUCxvQkFBWSxZQUFXO0FBQ3JCLGlCQUFPLEtBQVAsQ0FBYSwyQkFBYixFQUEwQyxRQUExQzs7QUFFQSxpQkFBTyxHQUFQLENBQVcsUUFBWDtBQUNBLGlCQUFPLEdBQVAsQ0FBVyxlQUFYOztBQUVBLGlCQUFPLElBQVAsQ0FBWSxRQUFaO0FBQ0EsaUJBQU8sSUFBUCxDQUFZLFFBQVo7O0FBRUEsaUJBQU8sV0FBUDtBQUNELFNBWk07O0FBY1AscUJBQWEsWUFBVztBQUN0QixpQkFBTyxPQUFQLENBQWUsOEJBQWYsRUFBK0MsTUFBL0M7QUFDQSxxQkFBVyxNQUFYO0FBQ0Esa0JBQ0csSUFESCxDQUNRLGVBRFIsRUFDeUIsTUFEekI7QUFHRCxTQXBCTTtBQXFCUCxpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLE9BQVAsQ0FBZSxrQ0FBZixFQUFtRCxPQUFuRDtBQUNBLHdCQUFjLFNBQVMsUUFBdkI7QUFDQSxpQkFBTyxNQUFQLENBQWMsS0FBZDtBQUNBLGtCQUFRLFVBQVIsQ0FBbUIsZUFBbkI7QUFDQSxxQkFBVyxTQUFYO0FBQ0QsU0EzQk07O0FBNkJQLGVBQU8sWUFBVztBQUNoQixpQkFBTyxNQUFQLENBQWMsU0FBZDtBQUNBLGlCQUFPLE1BQVAsQ0FBYyxRQUFkLENBQXVCLENBQXZCO0FBQ0QsU0FoQ007O0FBa0NQLGtCQUFVLFlBQVc7QUFDbkIsY0FBRyxPQUFPLE9BQVAsS0FBbUIsU0FBbkIsSUFBZ0MsT0FBTyxPQUFQLEdBQWlCLEdBQXBELEVBQXlEO0FBQ3ZELG1CQUFPLE1BQVAsQ0FBYyxZQUFkO0FBQ0EsbUJBQU8sR0FBUCxDQUFXLE9BQVgsQ0FBbUIsR0FBbkI7QUFDRDtBQUNGLFNBdkNNOztBQXlDUCxjQUFNO0FBQ0osb0JBQVUsWUFBVztBQUNuQixnQkFDRSxPQUFPO0FBQ0wsdUJBQVUsUUFBUSxJQUFSLENBQWEsU0FBUyxPQUF0QixDQURMO0FBRUwscUJBQVUsUUFBUSxJQUFSLENBQWEsU0FBUyxLQUF0QixDQUZMO0FBR0wscUJBQVUsUUFBUSxJQUFSLENBQWEsU0FBUyxLQUF0QjtBQUhMLGFBRFQ7QUFPQSxnQkFBRyxLQUFLLE9BQVIsRUFBaUI7QUFDZixxQkFBTyxLQUFQLENBQWEseUNBQWIsRUFBd0QsS0FBSyxPQUE3RDtBQUNBLHFCQUFPLEdBQVAsQ0FBVyxPQUFYLENBQW1CLEtBQUssT0FBeEI7QUFDRDtBQUNELGdCQUFHLEtBQUssS0FBUixFQUFlO0FBQ2IscUJBQU8sS0FBUCxDQUFhLCtCQUFiLEVBQThDLEtBQUssS0FBbkQ7QUFDQSxxQkFBTyxHQUFQLENBQVcsS0FBWCxDQUFpQixLQUFLLEtBQXRCO0FBQ0Q7QUFDRCxnQkFBRyxLQUFLLEtBQVIsRUFBZTtBQUNiLHFCQUFPLEtBQVAsQ0FBYSxpQ0FBYixFQUFnRCxLQUFLLEtBQXJEO0FBQ0EscUJBQU8sR0FBUCxDQUFXLEtBQVgsQ0FBaUIsS0FBSyxLQUF0QjtBQUNBLHFCQUFPLEdBQVAsQ0FBVyxRQUFYLENBQW9CLEtBQUssS0FBekI7QUFDRDtBQUNGLFdBdEJHO0FBdUJKLG9CQUFVLFlBQVc7QUFDbkIsZ0JBQUcsU0FBUyxLQUFULEtBQW1CLEtBQXRCLEVBQTZCO0FBQzNCLHFCQUFPLEtBQVAsQ0FBYSwrQkFBYixFQUE4QyxTQUFTLEtBQXZEO0FBQ0EscUJBQU8sR0FBUCxDQUFXLEtBQVgsQ0FBaUIsU0FBUyxLQUExQjtBQUNEO0FBQ0QsZ0JBQUcsU0FBUyxLQUFULEtBQW1CLEtBQXRCLEVBQTZCO0FBQzNCLHFCQUFPLEtBQVAsQ0FBYSwrQkFBYixFQUE4QyxTQUFTLEtBQXZEO0FBQ0EscUJBQU8sR0FBUCxDQUFXLEtBQVgsQ0FBaUIsU0FBUyxLQUExQjtBQUNBLHFCQUFPLEdBQVAsQ0FBVyxRQUFYLENBQW9CLE9BQU8sS0FBM0I7QUFDRDtBQUNELGdCQUFHLFNBQVMsT0FBVCxLQUFxQixLQUF4QixFQUErQjtBQUM3QixxQkFBTyxLQUFQLENBQWEsaUNBQWIsRUFBZ0QsU0FBUyxPQUF6RDtBQUNBLHFCQUFPLEdBQVAsQ0FBVyxPQUFYLENBQW1CLFNBQVMsT0FBNUI7QUFDRDtBQUNGO0FBckNHLFNBekNDOztBQWlGUCxtQkFBVyxVQUFTLGNBQVQsRUFBeUI7QUFDbEMsY0FDRSxRQURGLEVBRUUsVUFGRixFQUdFLFFBSEY7QUFLQSxjQUFJLE9BQU8sR0FBUCxDQUFXLEtBQVgsRUFBSixFQUF5QjtBQUN2Qix5QkFBaUIsT0FBTyxHQUFQLENBQVcsS0FBWCxFQUFqQjtBQUNBLDZCQUFpQixrQkFBa0IsQ0FBbkM7QUFDQSx1QkFBaUIsYUFBYSxjQUE5QjtBQUNELFdBSkQsTUFLSztBQUNILHlCQUFpQixPQUFPLEdBQVAsQ0FBVyxPQUFYLEVBQWpCO0FBQ0EsNkJBQWlCLGtCQUFrQixPQUFPLEdBQVAsQ0FBVyxXQUFYLEVBQW5DOztBQUVBLHVCQUFpQixhQUFhLGNBQTlCO0FBQ0EsdUJBQWlCLEdBQWpCO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLDRCQUFiLEVBQTJDLFVBQTNDLEVBQXVELFFBQXZEO0FBQ0Q7QUFDRCxxQkFBVyxPQUFPLEdBQVAsQ0FBVyxlQUFYLENBQTJCLFFBQTNCLENBQVg7QUFDQSxpQkFBTyxHQUFQLENBQVcsUUFBWCxDQUFvQixRQUFwQjtBQUNELFNBdEdNO0FBdUdQLG1CQUFXLFVBQVMsY0FBVCxFQUF5QjtBQUNsQyxjQUNFLFFBQVksT0FBTyxHQUFQLENBQVcsS0FBWCxFQURkO0FBQUEsY0FFRSxVQUZGO0FBQUEsY0FHRSxRQUhGO0FBS0EsY0FBRyxLQUFILEVBQVU7QUFDUix5QkFBa0IsT0FBTyxHQUFQLENBQVcsS0FBWCxFQUFsQjtBQUNBLDZCQUFrQixrQkFBa0IsQ0FBcEM7QUFDQSx1QkFBa0IsYUFBYSxjQUEvQjtBQUNBLG1CQUFPLEtBQVAsQ0FBYSx1QkFBYixFQUFzQyxjQUF0QyxFQUFzRCxVQUF0RDtBQUNELFdBTEQsTUFNSztBQUNILHlCQUFrQixPQUFPLEdBQVAsQ0FBVyxPQUFYLEVBQWxCO0FBQ0EsNkJBQWtCLGtCQUFrQixPQUFPLEdBQVAsQ0FBVyxXQUFYLEVBQXBDO0FBQ0EsdUJBQWtCLGFBQWEsY0FBL0I7QUFDQSxtQkFBTyxLQUFQLENBQWEsNEJBQWIsRUFBMkMsY0FBM0MsRUFBMkQsVUFBM0Q7QUFDRDtBQUNELHFCQUFXLE9BQU8sR0FBUCxDQUFXLGVBQVgsQ0FBMkIsUUFBM0IsQ0FBWDtBQUNBLGlCQUFPLEdBQVAsQ0FBVyxRQUFYLENBQW9CLFFBQXBCO0FBQ0QsU0EzSE07O0FBNkhQLGFBQUs7QUFDSCx3QkFBYyxZQUFXO0FBQ3ZCLG1CQUFPLE9BQU8sWUFBZDtBQUNELFdBSEU7QUFJSCxpQkFBTyxZQUFXO0FBQ2hCLG1CQUFRLE9BQU8sR0FBUCxDQUFXLEtBQVgsT0FBdUIsS0FBL0I7QUFDRDtBQU5FLFNBN0hFOztBQXNJUCxhQUFLO0FBQ0gsZ0JBQU0sVUFBUyxZQUFULEVBQXVCO0FBQzNCLGdCQUNFLFFBQVUsT0FBTyxLQUFQLElBQStCLENBRDNDO0FBQUEsZ0JBRUUsUUFBVSxPQUFPLEtBQVAsSUFBK0IsQ0FGM0M7QUFBQSxnQkFHRSxVQUFXLFNBQUQsR0FDTixPQUFPLEdBQVAsQ0FBVyxjQUFYLEVBRE0sR0FFTixPQUFPLE9BQVAsSUFBa0IsQ0FMeEI7QUFBQSxnQkFNRSxPQUFRLE9BQU8sS0FBUCxHQUFlLENBQWhCLEdBQ0YsUUFBUSxLQUROLEdBRUYsTUFBTSxPQVJiO0FBVUEsMkJBQWUsZ0JBQWdCLEVBQS9CO0FBQ0EsMkJBQWUsYUFDWixPQURZLENBQ0osU0FESSxFQUNPLEtBRFAsRUFFWixPQUZZLENBRUosU0FGSSxFQUVPLEtBRlAsRUFHWixPQUhZLENBR0osUUFISSxFQUdNLElBSE4sRUFJWixPQUpZLENBSUosV0FKSSxFQUlTLE9BSlQsQ0FBZjtBQU1BLG1CQUFPLE9BQVAsQ0FBZSx1Q0FBZixFQUF3RCxZQUF4RDtBQUNBLG1CQUFPLFlBQVA7QUFDRCxXQXJCRTs7QUF1QkgsMkJBQWlCLFVBQVMsS0FBVCxFQUFnQjtBQUMvQixnQkFBRyxRQUFRLENBQVgsRUFBYztBQUNaLHFCQUFPLEtBQVAsQ0FBYSxnQ0FBYjtBQUNBLHFCQUFPLENBQVA7QUFDRDtBQUNELGdCQUFHLE9BQU8sR0FBUCxDQUFXLEtBQVgsRUFBSCxFQUF1QjtBQUNyQixrQkFBRyxRQUFRLE9BQU8sS0FBbEIsRUFBeUI7QUFDdkIsdUJBQU8sS0FBUCxDQUFhLG9DQUFiLEVBQW1ELE9BQU8sS0FBMUQ7QUFDQSx1QkFBTyxPQUFPLEtBQWQ7QUFDRDtBQUNGLGFBTEQsTUFNSyxJQUFHLFFBQVEsR0FBWCxFQUFpQjtBQUNwQixxQkFBTyxLQUFQLENBQWEsMENBQWI7QUFDQSxxQkFBTyxHQUFQO0FBQ0Q7QUFDRCxtQkFBTyxLQUFQO0FBQ0QsV0F2Q0U7O0FBeUNILDBCQUFnQixZQUFXO0FBQ3pCLGdCQUFHLFNBQVMsY0FBVCxJQUEyQixNQUE5QixFQUFzQztBQUNwQyxxQkFBTyxTQUFTLFFBQWhCO0FBQ0Q7QUFDRCxtQkFBTyxTQUFTLGNBQWhCO0FBQ0QsV0E5Q0U7O0FBZ0RILHVCQUFhLFlBQVc7QUFDdEIsbUJBQU8sS0FBUCxDQUFhLHdDQUFiO0FBQ0EsbUJBQU8sS0FBSyxLQUFMLENBQVksS0FBSyxNQUFMLEtBQWdCLFNBQVMsTUFBVCxDQUFnQixHQUFqQyxHQUF3QyxTQUFTLE1BQVQsQ0FBZ0IsR0FBbkUsQ0FBUDtBQUNELFdBbkRFOztBQXFESCx3QkFBYyxVQUFTLEtBQVQsRUFBZ0I7QUFDNUIsbUJBQVEsT0FBTyxLQUFQLEtBQWlCLFFBQWxCLEdBQ0YsTUFBTSxPQUFOLENBQWMsU0FBZCxFQUF5QixFQUF6QixNQUFpQyxFQUFsQyxHQUNFLENBQUUsTUFBTSxPQUFOLENBQWMsU0FBZCxFQUF5QixFQUF6QixDQURKLEdBRUUsS0FIQyxHQUlILEtBSko7QUFNRCxXQTVERTs7QUE4REgseUJBQWUsWUFBVztBQUN4QixnQkFDRSxVQUFjLFNBQVMsYUFBVCxDQUF1QixTQUF2QixDQURoQjtBQUFBLGdCQUVFLGNBQWM7QUFDWiw0QkFBb0IsZUFEUjtBQUVaLDZCQUFvQixnQkFGUjtBQUdaLCtCQUFvQixlQUhSO0FBSVosa0NBQW9CO0FBSlIsYUFGaEI7QUFBQSxnQkFRRSxVQVJGO0FBVUEsaUJBQUksVUFBSixJQUFrQixXQUFsQixFQUE4QjtBQUM1QixrQkFBSSxRQUFRLEtBQVIsQ0FBYyxVQUFkLE1BQThCLFNBQWxDLEVBQTZDO0FBQzNDLHVCQUFPLFlBQVksVUFBWixDQUFQO0FBQ0Q7QUFDRjtBQUNGLFdBOUVFOzs7QUFpRkgsMEJBQWdCLFlBQVc7QUFDekIsZ0JBQ0UsV0FBaUIsS0FBSyxLQUFMLEVBRG5CO0FBQUEsZ0JBRUUsYUFBaUIsUUFBUSxLQUFSLEVBRm5CO0FBQUEsZ0JBR0UsYUFBaUIsU0FBUyxLQUFLLEdBQUwsQ0FBUyxXQUFULENBQVQsRUFBZ0MsRUFBaEMsQ0FIbkI7QUFBQSxnQkFJRSxpQkFBa0IsV0FBVyxVQUFaLEdBQ1osV0FBVyxVQUFYLEdBQXdCLEdBRFosR0FFYixPQUFPLE9BTmI7QUFRQSxtQkFBUSxTQUFTLFNBQVQsR0FBcUIsQ0FBdEIsR0FDSCxLQUFLLEtBQUwsQ0FBVyxrQkFBa0IsS0FBSyxTQUFTLFNBQWhDLENBQVgsS0FBMEQsS0FBSyxTQUFTLFNBQXhFLENBREcsR0FFSCxLQUFLLEtBQUwsQ0FBVyxjQUFYLENBRko7QUFJRCxXQTlGRTs7QUFnR0gsbUJBQVMsWUFBVztBQUNsQixtQkFBTyxPQUFPLE9BQVAsSUFBa0IsQ0FBekI7QUFDRCxXQWxHRTtBQW1HSCxpQkFBTyxZQUFXO0FBQ2hCLG1CQUFPLE9BQU8sU0FBUCxJQUFvQixPQUFPLEtBQTNCLElBQW9DLENBQTNDO0FBQ0QsV0FyR0U7QUFzR0gsaUJBQU8sWUFBVztBQUNoQixtQkFBTyxPQUFPLEtBQVAsSUFBZ0IsS0FBdkI7QUFDRDtBQXhHRSxTQXRJRTs7QUFpUFAsZ0JBQVE7QUFDTix3QkFBYyxZQUFXO0FBQ3ZCLG1CQUFPLFlBQVAsR0FBc0IsV0FBVyxZQUFXO0FBQzFDLHFCQUFPLE1BQVAsQ0FBYyxXQUFkO0FBQ0EscUJBQU8sTUFBUCxDQUFjLFlBQWQ7QUFDRCxhQUhxQixFQUduQixPQUFPLEdBQVAsQ0FBVyxjQUFYLEVBSG1CLENBQXRCO0FBSUQ7QUFOSyxTQWpQRDs7QUEwUFAsWUFBSTtBQUNGLG9CQUFVLFlBQVc7QUFDbkIsbUJBQU8sT0FBTyxFQUFQLENBQVUsT0FBVixNQUF1QixPQUFPLEVBQVAsQ0FBVSxPQUFWLEVBQXZCLElBQThDLE9BQU8sRUFBUCxDQUFVLEtBQVYsRUFBckQ7QUFDRCxXQUhDO0FBSUYsbUJBQVMsWUFBVztBQUNsQixtQkFBTyxRQUFRLFFBQVIsQ0FBaUIsVUFBVSxPQUEzQixDQUFQO0FBQ0QsV0FOQztBQU9GLG1CQUFTLFlBQVc7QUFDbEIsbUJBQU8sUUFBUSxRQUFSLENBQWlCLFVBQVUsT0FBM0IsQ0FBUDtBQUNELFdBVEM7QUFVRixpQkFBTyxZQUFXO0FBQ2hCLG1CQUFPLFFBQVEsUUFBUixDQUFpQixVQUFVLEtBQTNCLENBQVA7QUFDRCxXQVpDO0FBYUYsa0JBQVEsWUFBVztBQUNqQixtQkFBTyxRQUFRLFFBQVIsQ0FBaUIsVUFBVSxNQUEzQixDQUFQO0FBQ0QsV0FmQztBQWdCRixtQkFBUyxZQUFXO0FBQ2xCLG1CQUFPLFFBQVEsRUFBUixDQUFXLFVBQVgsQ0FBUDtBQUNEO0FBbEJDLFNBMVBHOztBQStRUCxnQkFBUTtBQUNOLHdCQUFjLFlBQVc7QUFDdkIsbUJBQU8sT0FBUCxDQUFlLDhCQUFmO0FBQ0EsZ0JBQUcsT0FBTyxZQUFWLEVBQXdCO0FBQ3RCLDJCQUFhLE9BQU8sWUFBcEI7QUFDQSxxQkFBTyxPQUFPLFlBQWQ7QUFDRDtBQUNGLFdBUEs7QUFRTixxQkFBVyxZQUFXO0FBQ3BCLG1CQUFPLE9BQVAsQ0FBZSxnREFBZjtBQUNBLG1CQUFPLE9BQU8sU0FBZDtBQUNELFdBWEs7QUFZTixpQkFBTyxZQUFXO0FBQ2hCLG1CQUFPLE9BQVAsQ0FBZSx1QkFBZjtBQUNBLG1CQUFPLE9BQU8sS0FBZDtBQUNBLG1CQUFPLE9BQU8sT0FBZDtBQUNBLG1CQUFPLE9BQU8sS0FBZDtBQUNELFdBakJLO0FBa0JOLGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sT0FBUCxDQUFlLHVCQUFmO0FBQ0Esb0JBQVEsV0FBUixDQUFvQixVQUFVLE1BQTlCO0FBQ0QsV0FyQks7QUFzQk4sbUJBQVMsWUFBVztBQUNsQixtQkFBTyxPQUFQLENBQWUsd0JBQWY7QUFDQSxvQkFBUSxXQUFSLENBQW9CLFVBQVUsT0FBOUI7QUFDRCxXQXpCSztBQTBCTixtQkFBUyxZQUFXO0FBQ2xCLG1CQUFPLE9BQVAsQ0FBZSx3QkFBZjtBQUNBLG9CQUFRLFdBQVIsQ0FBb0IsVUFBVSxPQUE5QjtBQUNELFdBN0JLO0FBOEJOLGlCQUFPLFlBQVc7QUFDaEIsbUJBQU8sT0FBUCxDQUFlLHNCQUFmO0FBQ0Esb0JBQVEsV0FBUixDQUFvQixVQUFVLEtBQTlCO0FBQ0Q7QUFqQ0ssU0EvUUQ7O0FBbVRQLGFBQUs7QUFDSCxvQkFBVSxVQUFTLEtBQVQsRUFBZ0I7QUFDeEIsZ0JBQUcsUUFBUSxHQUFYLEVBQWdCO0FBQ2QscUJBQU8sS0FBUCxDQUFhLE1BQU0sT0FBbkIsRUFBNEIsS0FBNUI7QUFDRCxhQUZELE1BR0ssSUFBSSxRQUFRLENBQVosRUFBZTtBQUNsQixxQkFBTyxLQUFQLENBQWEsTUFBTSxNQUFuQixFQUEyQixLQUEzQjtBQUNELGFBRkksTUFHQTtBQUNILG1CQUNHLEdBREgsQ0FDTyxPQURQLEVBQ2dCLFFBQVEsR0FEeEI7QUFHQSxzQkFDRyxJQURILENBQ1EsY0FEUixFQUN3QixTQUFTLEtBQVQsRUFBZ0IsRUFBaEIsQ0FEeEI7QUFHRDtBQUNGLFdBaEJFO0FBaUJILG9CQUFVLFVBQVMsUUFBVCxFQUFtQjtBQUMzQix1QkFBVyxZQUFZLFNBQVMsUUFBaEM7QUFDQSx1QkFBWSxPQUFPLFFBQVAsSUFBbUIsUUFBcEIsR0FDUCxXQUFXLElBREosR0FFUCxRQUZKO0FBSUEsbUJBQU8sT0FBUCxDQUFlLDBDQUFmLEVBQTJELFFBQTNEO0FBQ0EsaUJBQ0csR0FESCxDQUNPO0FBQ0gscUNBQXdCO0FBRHJCLGFBRFA7QUFLRCxXQTdCRTtBQThCSCxtQkFBUyxVQUFTLE9BQVQsRUFBa0I7QUFDekIsc0JBQVcsT0FBTyxPQUFQLElBQWtCLFFBQW5CLEdBQ04sQ0FBRSxRQUFRLE9BQVIsQ0FBZ0IsR0FBaEIsRUFBcUIsRUFBckIsQ0FESSxHQUVOLE9BRko7O0FBS0Esc0JBQVcsU0FBUyxTQUFULEdBQXFCLENBQXRCLEdBQ04sS0FBSyxLQUFMLENBQVcsV0FBVyxLQUFLLFNBQVMsU0FBekIsQ0FBWCxLQUFtRCxLQUFLLFNBQVMsU0FBakUsQ0FETSxHQUVOLEtBQUssS0FBTCxDQUFXLE9BQVgsQ0FGSjtBQUlBLG1CQUFPLE9BQVAsR0FBaUIsT0FBakI7QUFDQSxnQkFBSSxDQUFDLE9BQU8sR0FBUCxDQUFXLEtBQVgsRUFBTCxFQUEwQjtBQUN4QixxQkFBTyxLQUFQLEdBQWdCLFNBQVMsU0FBVCxHQUFxQixDQUF0QixHQUNYLEtBQUssS0FBTCxDQUFhLFVBQVUsR0FBWCxHQUFrQixPQUFPLEtBQXpCLElBQWtDLEtBQUssU0FBUyxTQUFoRCxDQUFaLEtBQTJFLEtBQUssU0FBUyxTQUF6RixDQURXLEdBRVgsS0FBSyxLQUFMLENBQWEsVUFBVSxHQUFYLEdBQWtCLE9BQU8sS0FBekIsR0FBaUMsRUFBN0MsSUFBbUQsRUFGdkQ7QUFJQSxrQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIsdUJBQU8sS0FBUCxHQUFnQixPQUFPLEtBQVAsR0FBZSxHQUFoQixHQUNYLEdBRFcsR0FFVixPQUFPLEtBQVAsR0FBZSxDQUFoQixHQUNFLENBREYsR0FFRSxPQUFPLEtBSmI7QUFNRDtBQUNGO0FBQ0QsbUJBQU8sR0FBUCxDQUFXLFFBQVgsQ0FBb0IsT0FBcEI7QUFDQSxtQkFBTyxHQUFQLENBQVcsYUFBWDtBQUNBLG1CQUFPLEdBQVAsQ0FBVyxNQUFYO0FBQ0EscUJBQVMsUUFBVCxDQUFrQixJQUFsQixDQUF1QixPQUF2QixFQUFnQyxPQUFoQyxFQUF5QyxPQUFPLEtBQWhELEVBQXVELE9BQU8sS0FBOUQ7QUFDRCxXQTNERTtBQTRESCx5QkFBZSxZQUFXO0FBQ3hCLGdCQUNFLG9CQUFvQixZQUFXO0FBQzdCLHFCQUFPLE9BQVAsQ0FBZSwyREFBZjtBQUNBLDRCQUFjLE9BQU8sUUFBckI7QUFDQSwwQkFBWSxLQUFaO0FBQ0EscUJBQU8sR0FBUCxDQUFXLE1BQVg7QUFDRCxhQU5IO0FBUUEsMEJBQWMsT0FBTyxRQUFyQjtBQUNBLGlCQUFLLEdBQUwsQ0FBUyxnQkFBZ0IsY0FBekIsRUFBeUMsaUJBQXpDO0FBQ0Esd0JBQVksSUFBWjtBQUNBLG1CQUFPLFFBQVAsR0FBa0IsWUFBWSxZQUFXO0FBQ3ZDLGtCQUNFLFVBQVUsRUFBRSxRQUFGLENBQVcsU0FBUyxlQUFwQixFQUFxQyxPQUFyQyxDQURaO0FBR0Esa0JBQUcsQ0FBQyxPQUFKLEVBQWE7QUFDWCw4QkFBYyxPQUFPLFFBQXJCO0FBQ0EsNEJBQVksS0FBWjtBQUNEO0FBQ0QscUJBQU8sR0FBUCxDQUFXLE1BQVg7QUFDRCxhQVRpQixFQVNmLFNBQVMsU0FUTSxDQUFsQjtBQVVELFdBbEZFO0FBbUZILGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sT0FBUCxDQUFlLGdEQUFmO0FBQ0EsbUJBQU8sR0FBUCxDQUFXLFFBQVg7QUFDQSxtQkFBTyxHQUFQLENBQVcsS0FBWDtBQUNELFdBdkZFO0FBd0ZILGlCQUFPLFVBQVMsSUFBVCxFQUFlO0FBQ3BCLG1CQUFPLFFBQVEsRUFBZjtBQUNBLGdCQUFHLElBQUgsRUFBUztBQUNQLHFCQUFPLE9BQU8sR0FBUCxDQUFXLElBQVgsQ0FBZ0IsSUFBaEIsQ0FBUDtBQUNBLHFCQUFPLE9BQVAsQ0FBZSx1QkFBZixFQUF3QyxJQUF4QztBQUNBLHFCQUFPLElBQVAsQ0FBWSxJQUFaO0FBQ0Q7QUFDRixXQS9GRTtBQWdHSCxpQkFBTyxVQUFTLE9BQVQsRUFBa0I7QUFDdkIsc0JBQVcsWUFBWSxTQUFiLEdBQ04sT0FETSxHQUVOLE9BQU8sT0FGWDtBQUlBLGdCQUFHLFlBQVksR0FBZixFQUFvQjtBQUNsQixrQkFBRyxTQUFTLFdBQVQsSUFBd0IsRUFBRSxPQUFPLEVBQVAsQ0FBVSxPQUFWLE1BQXVCLE9BQU8sRUFBUCxDQUFVLEtBQVYsRUFBdkIsSUFBNEMsT0FBTyxFQUFQLENBQVUsT0FBVixFQUE5QyxDQUEzQixFQUErRjtBQUM3Rix1QkFBTyxHQUFQLENBQVcsT0FBWDtBQUNBLHVCQUFPLEtBQVAsQ0FBYSwwQ0FBYjtBQUNELGVBSEQsTUFJSztBQUNILHVCQUFPLE9BQVAsQ0FBZSxvQ0FBZjtBQUNBLHVCQUFPLE1BQVAsQ0FBYyxNQUFkO0FBQ0EsdUJBQU8sTUFBUCxDQUFjLFlBQWQ7QUFDRDtBQUNGLGFBVkQsTUFXSyxJQUFHLFVBQVUsQ0FBYixFQUFnQjtBQUNuQixxQkFBTyxPQUFQLENBQWUscUNBQWYsRUFBc0QsT0FBdEQ7QUFDQSxxQkFBTyxHQUFQLENBQVcsTUFBWDtBQUNELGFBSEksTUFJQTtBQUNILHFCQUFPLE1BQVAsQ0FBYyxNQUFkO0FBQ0EscUJBQU8sR0FBUCxDQUFXLEtBQVgsQ0FBaUIsU0FBUyxJQUFULENBQWMsTUFBL0I7QUFDRDtBQUNGLFdBeEhFO0FBeUhILG9CQUFVLFVBQVMsSUFBVCxFQUFlO0FBQ3ZCLGdCQUFHLFNBQVMsU0FBWixFQUF1QjtBQUNyQix3QkFBVSxJQUFWLENBQWdCLE9BQU8sR0FBUCxDQUFXLElBQVgsQ0FBZ0IsSUFBaEIsQ0FBaEI7QUFDRCxhQUZELE1BR0ssSUFBRyxTQUFTLEtBQVQsSUFBa0IsT0FBbEIsSUFBNkIsT0FBTyxLQUF2QyxFQUE4QztBQUNqRCxxQkFBTyxPQUFQLENBQWUsMkJBQWY7QUFDQSx3QkFBVSxJQUFWLENBQWdCLE9BQU8sR0FBUCxDQUFXLElBQVgsQ0FBZ0IsU0FBUyxJQUFULENBQWMsS0FBOUIsQ0FBaEI7QUFDRCxhQUhJLE1BSUEsSUFBRyxTQUFTLEtBQVQsSUFBa0IsU0FBckIsRUFBZ0M7QUFDbkMscUJBQU8sT0FBUCxDQUFlLGdDQUFmO0FBQ0Esd0JBQVUsSUFBVixDQUFnQixPQUFPLEdBQVAsQ0FBVyxJQUFYLENBQWdCLFNBQVMsSUFBVCxDQUFjLE9BQTlCLENBQWhCO0FBQ0Q7QUFDRixXQXJJRTtBQXNJSCxrQkFBUSxVQUFTLElBQVQsRUFBZTtBQUNyQixtQkFBTyxRQUFRLFNBQVMsSUFBVCxDQUFjLE1BQTdCO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLHNCQUFiO0FBQ0EsZ0JBQUcsU0FBUyxZQUFULElBQXlCLENBQUMsT0FBTyxFQUFQLENBQVUsTUFBVixFQUE3QixFQUFrRDtBQUNoRCxzQkFBUSxRQUFSLENBQWlCLFVBQVUsTUFBM0I7QUFDRDtBQUNELG1CQUFPLE1BQVAsQ0FBYyxPQUFkO0FBQ0EsbUJBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDQSxtQkFBTyxNQUFQLENBQWMsT0FBZDtBQUNBLG1CQUFPLFNBQVMsYUFBVCxDQUF1QixRQUF2QixFQUFpQyxJQUFqQyxFQUF1QyxPQUFPLEtBQTlDLEVBQXFELE9BQU8sS0FBNUQsQ0FBUDtBQUNBLGdCQUFHLElBQUgsRUFBUztBQUNQLHFCQUFPLEdBQVAsQ0FBVyxLQUFYLENBQWlCLElBQWpCO0FBQ0Q7QUFDRCxpQkFBSyxHQUFMLENBQVMsZ0JBQWdCLGNBQXpCLEVBQXlDLFlBQVc7QUFDbEQsdUJBQVMsUUFBVCxDQUFrQixJQUFsQixDQUF1QixPQUF2QixFQUFnQyxPQUFPLEtBQXZDLEVBQThDLE9BQU8sS0FBckQ7QUFDRCxhQUZEO0FBR0QsV0F0SkU7QUF1SkgsbUJBQVUsVUFBUyxJQUFULEVBQWU7QUFDdkIsbUJBQU8sUUFBUSxTQUFTLElBQVQsQ0FBYyxPQUF0QixJQUFpQyxTQUFTLElBQVQsQ0FBYyxNQUF0RDtBQUNBLG1CQUFPLEtBQVAsQ0FBYSx1QkFBYjtBQUNBLG9CQUFRLFFBQVIsQ0FBaUIsVUFBVSxPQUEzQjtBQUNBLG1CQUFPLE1BQVAsQ0FBYyxNQUFkO0FBQ0EsbUJBQU8sTUFBUCxDQUFjLE9BQWQ7QUFDQSxtQkFBTyxNQUFQLENBQWMsS0FBZDtBQUNBLG1CQUFPLFFBQVA7QUFDQSxnQkFBRyxTQUFTLElBQVQsQ0FBYyxPQUFqQixFQUEwQjtBQUN4QixxQkFBTyxTQUFTLGFBQVQsQ0FBdUIsU0FBdkIsRUFBa0MsSUFBbEMsRUFBd0MsT0FBTyxLQUEvQyxFQUFzRCxPQUFPLEtBQTdELENBQVA7QUFDQSxxQkFBTyxHQUFQLENBQVcsS0FBWCxDQUFpQixJQUFqQjtBQUNELGFBSEQsTUFJSztBQUNILHFCQUFPLFNBQVMsYUFBVCxDQUF1QixRQUF2QixFQUFpQyxJQUFqQyxFQUF1QyxPQUFPLEtBQTlDLEVBQXFELE9BQU8sS0FBNUQsQ0FBUDtBQUNBLHFCQUFPLEdBQVAsQ0FBVyxLQUFYLENBQWlCLElBQWpCO0FBQ0Q7QUFDRCxpQkFBSyxHQUFMLENBQVMsZ0JBQWdCLGNBQXpCLEVBQXlDLFlBQVc7QUFDbEQsdUJBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixPQUF4QixFQUFpQyxPQUFPLEtBQXhDO0FBQ0QsYUFGRDtBQUdELFdBMUtFO0FBMktILG1CQUFVLFVBQVMsSUFBVCxFQUFlO0FBQ3ZCLG1CQUFPLFFBQVEsU0FBUyxJQUFULENBQWMsT0FBN0I7QUFDQSxtQkFBTyxLQUFQLENBQWEsdUJBQWI7QUFDQSxvQkFBUSxRQUFSLENBQWlCLFVBQVUsT0FBM0I7QUFDQSxtQkFBTyxNQUFQLENBQWMsTUFBZDtBQUNBLG1CQUFPLE1BQVAsQ0FBYyxPQUFkO0FBQ0EsbUJBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDQSxtQkFBTyxRQUFQO0FBQ0EsbUJBQU8sU0FBUyxhQUFULENBQXVCLFNBQXZCLEVBQWtDLElBQWxDLEVBQXdDLE9BQU8sS0FBL0MsRUFBc0QsT0FBTyxLQUE3RCxDQUFQO0FBQ0EsZ0JBQUcsSUFBSCxFQUFTO0FBQ1AscUJBQU8sR0FBUCxDQUFXLEtBQVgsQ0FBaUIsSUFBakI7QUFDRDtBQUNELGlCQUFLLEdBQUwsQ0FBUyxnQkFBZ0IsY0FBekIsRUFBeUMsWUFBVztBQUNsRCx1QkFBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLE9BQXhCLEVBQWlDLE9BQU8sS0FBeEMsRUFBK0MsT0FBTyxLQUF0RDtBQUNELGFBRkQ7QUFHRCxXQTFMRTtBQTJMSCxpQkFBUSxVQUFTLElBQVQsRUFBZTtBQUNyQixtQkFBTyxRQUFRLFNBQVMsSUFBVCxDQUFjLEtBQTdCO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLHFCQUFiO0FBQ0Esb0JBQVEsUUFBUixDQUFpQixVQUFVLEtBQTNCO0FBQ0EsbUJBQU8sTUFBUCxDQUFjLE1BQWQ7QUFDQSxtQkFBTyxNQUFQLENBQWMsT0FBZDtBQUNBLG1CQUFPLE1BQVAsQ0FBYyxPQUFkO0FBQ0EsbUJBQU8sUUFBUDtBQUNBLG1CQUFPLFNBQVMsYUFBVCxDQUF1QixPQUF2QixFQUFnQyxJQUFoQyxFQUFzQyxPQUFPLEtBQTdDLEVBQW9ELE9BQU8sS0FBM0QsQ0FBUDtBQUNBLGdCQUFHLElBQUgsRUFBUztBQUNQLHFCQUFPLEdBQVAsQ0FBVyxLQUFYLENBQWlCLElBQWpCO0FBQ0Q7QUFDRCxpQkFBSyxHQUFMLENBQVMsZ0JBQWdCLGNBQXpCLEVBQXlDLFlBQVc7QUFDbEQsdUJBQVMsT0FBVCxDQUFpQixJQUFqQixDQUFzQixPQUF0QixFQUErQixPQUFPLEtBQXRDLEVBQTZDLE9BQU8sS0FBcEQ7QUFDRCxhQUZEO0FBR0QsV0ExTUU7QUEyTUgsMkJBQWlCLFlBQVc7QUFDMUIsNEJBQWdCLE9BQU8sR0FBUCxDQUFXLGFBQVgsRUFBaEI7QUFDRCxXQTdNRTtBQThNSCxpQkFBTyxVQUFTLFVBQVQsRUFBcUI7QUFDMUIsbUJBQU8sS0FBUCxHQUFlLFVBQWY7QUFDRCxXQWhORTtBQWlOSCxpQkFBTyxVQUFTLEtBQVQsRUFBZ0I7QUFDckIsbUJBQU8sS0FBUCxHQUFlLEtBQWY7QUFDRCxXQW5ORTtBQW9OSCxvQkFBVSxVQUFTLEtBQVQsRUFBZ0I7QUFDeEIsZ0JBQUcsQ0FBQyxPQUFPLEdBQVAsQ0FBVyxZQUFYLEVBQUosRUFBK0I7QUFDN0IscUJBQU8sS0FBUCxDQUFhLGdFQUFiLEVBQStFLEtBQS9FO0FBQ0EscUJBQU8sTUFBUCxDQUFjLFFBQWQsQ0FBdUIsS0FBdkI7QUFDQSxxQkFBTyxNQUFQLENBQWMsWUFBZDtBQUNELGFBSkQsTUFLSztBQUNILHFCQUFPLEtBQVAsQ0FBYSwrREFBYixFQUE4RSxLQUE5RTtBQUNBLHFCQUFPLEdBQVAsQ0FBVyxTQUFYLENBQXFCLEtBQXJCO0FBQ0Q7QUFDRixXQTlORTtBQStOSCxxQkFBVyxVQUFTLEtBQVQsRUFBZ0I7QUFDekIsbUJBQU8sU0FBUCxHQUFtQixLQUFuQjtBQUNEO0FBak9FLFNBblRFOztBQXVoQlAsZ0JBQVE7QUFDTix1QkFBYSxZQUFXO0FBQ3RCLGdCQUNFLFlBQVksT0FBTyxTQURyQjtBQUdBLGdCQUFHLFNBQUgsRUFBYztBQUNaLHFCQUFPLEtBQVAsQ0FBYSxtREFBYixFQUFrRSxTQUFsRTtBQUNBLHFCQUFPLE1BQVAsQ0FBYyxRQUFkLENBQXVCLFNBQXZCO0FBQ0EscUJBQU8sTUFBUCxDQUFjLFNBQWQ7QUFDRDtBQUNGLFdBVks7QUFXTixvQkFBVSxVQUFTLEtBQVQsRUFBZ0I7QUFDeEIsZ0JBQ0UsZUFERjtBQUdBLG9CQUFRLE9BQU8sR0FBUCxDQUFXLFlBQVgsQ0FBd0IsS0FBeEIsQ0FBUjtBQUNBLGdCQUFHLFVBQVUsS0FBYixFQUFvQjtBQUNsQixxQkFBTyxLQUFQLENBQWEsTUFBTSxVQUFuQixFQUErQixLQUEvQjtBQUNEO0FBQ0Qsb0JBQVEsT0FBTyxHQUFQLENBQVcsZUFBWCxDQUEyQixLQUEzQixDQUFSO0FBQ0EsZ0JBQUksT0FBTyxHQUFQLENBQVcsS0FBWCxFQUFKLEVBQXlCO0FBQ3ZCLHFCQUFPLEdBQVAsQ0FBVyxLQUFYLENBQWlCLEtBQWpCO0FBQ0EsZ0NBQW1CLFFBQVEsT0FBTyxLQUFoQixHQUF5QixHQUEzQztBQUNBLHFCQUFPLEtBQVAsQ0FBYSx5Q0FBYixFQUF3RCxlQUF4RDtBQUNBLHFCQUFPLEdBQVAsQ0FBVyxPQUFYLENBQW9CLGVBQXBCO0FBQ0QsYUFMRCxNQU1LO0FBQ0gsZ0NBQWtCLEtBQWxCO0FBQ0EscUJBQU8sS0FBUCxDQUFhLHlDQUFiLEVBQXdELGVBQXhEO0FBQ0EscUJBQU8sR0FBUCxDQUFXLE9BQVgsQ0FBb0IsZUFBcEI7QUFDRDtBQUNGO0FBL0JLLFNBdmhCRDs7QUF5akJQLGlCQUFTLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDN0IsaUJBQU8sS0FBUCxDQUFhLGtCQUFiLEVBQWlDLElBQWpDLEVBQXVDLEtBQXZDO0FBQ0EsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsUUFBZixFQUF5QixJQUF6QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixnQkFBRyxFQUFFLGFBQUYsQ0FBZ0IsU0FBUyxJQUFULENBQWhCLENBQUgsRUFBb0M7QUFDbEMsZ0JBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxTQUFTLElBQVQsQ0FBZixFQUErQixLQUEvQjtBQUNELGFBRkQsTUFHSztBQUNILHVCQUFTLElBQVQsSUFBaUIsS0FBakI7QUFDRDtBQUNGLFdBUEksTUFRQTtBQUNILG1CQUFPLFNBQVMsSUFBVCxDQUFQO0FBQ0Q7QUFDRixTQXprQk07QUEwa0JQLGtCQUFVLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDOUIsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsTUFBZixFQUF1QixJQUF2QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixtQkFBTyxJQUFQLElBQWUsS0FBZjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLE9BQU8sSUFBUCxDQUFQO0FBQ0Q7QUFDRixTQXBsQk07QUFxbEJQLGVBQU8sWUFBVztBQUNoQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsS0FBaEMsRUFBdUM7QUFDckMsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxLQUFQLEdBQWUsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWY7QUFDQSxxQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNEO0FBQ0Y7QUFDRixTQS9sQk07QUFnbUJQLGlCQUFTLFlBQVc7QUFDbEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLE9BQTdCLElBQXdDLFNBQVMsS0FBcEQsRUFBMkQ7QUFDekQsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxPQUFQLEdBQWlCLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFqQjtBQUNBLHFCQUFPLE9BQVAsQ0FBZSxLQUFmLENBQXFCLE9BQXJCLEVBQThCLFNBQTlCO0FBQ0Q7QUFDRjtBQUNGLFNBMW1CTTtBQTJtQlAsZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQWIsRUFBcUI7QUFDbkIsbUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLEtBQXJDLEVBQTRDLE9BQTVDLEVBQXFELFNBQVMsSUFBVCxHQUFnQixHQUFyRSxDQUFmO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGLFNBaG5CTTtBQWluQlAscUJBQWE7QUFDWCxlQUFLLFVBQVMsT0FBVCxFQUFrQjtBQUNyQixnQkFDRSxXQURGLEVBRUUsYUFGRixFQUdFLFlBSEY7QUFLQSxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIsNEJBQWdCLElBQUksSUFBSixHQUFXLE9BQVgsRUFBaEI7QUFDQSw2QkFBZ0IsUUFBUSxXQUF4QjtBQUNBLDhCQUFnQixjQUFjLFlBQTlCO0FBQ0EscUJBQWdCLFdBQWhCO0FBQ0EsMEJBQVksSUFBWixDQUFpQjtBQUNmLHdCQUFtQixRQUFRLENBQVIsQ0FESjtBQUVmLDZCQUFtQixHQUFHLEtBQUgsQ0FBUyxJQUFULENBQWMsT0FBZCxFQUF1QixDQUF2QixLQUE2QixFQUZqQztBQUdmLDJCQUFtQixPQUhKO0FBSWYsa0NBQW1CO0FBSkosZUFBakI7QUFNRDtBQUNELHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLG1CQUFPLFdBQVAsQ0FBbUIsS0FBbkIsR0FBMkIsV0FBVyxPQUFPLFdBQVAsQ0FBbUIsT0FBOUIsRUFBdUMsR0FBdkMsQ0FBM0I7QUFDRCxXQXJCVTtBQXNCWCxtQkFBUyxZQUFXO0FBQ2xCLGdCQUNFLFFBQVEsU0FBUyxJQUFULEdBQWdCLEdBRDFCO0FBQUEsZ0JBRUUsWUFBWSxDQUZkO0FBSUEsbUJBQU8sS0FBUDtBQUNBLHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLGNBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDJCQUFhLEtBQUssZ0JBQUwsQ0FBYjtBQUNELGFBRkQ7QUFHQSxxQkFBUyxNQUFNLFNBQU4sR0FBa0IsSUFBM0I7QUFDQSxnQkFBRyxjQUFILEVBQW1CO0FBQ2pCLHVCQUFTLFFBQVEsY0FBUixHQUF5QixJQUFsQztBQUNEO0FBQ0QsZ0JBQUksQ0FBQyxRQUFRLEtBQVIsS0FBa0IsU0FBbEIsSUFBK0IsUUFBUSxLQUFSLEtBQWtCLFNBQWxELEtBQWdFLFlBQVksTUFBWixHQUFxQixDQUF6RixFQUE0RjtBQUMxRixzQkFBUSxjQUFSLENBQXVCLEtBQXZCO0FBQ0Esa0JBQUcsUUFBUSxLQUFYLEVBQWtCO0FBQ2hCLHdCQUFRLEtBQVIsQ0FBYyxXQUFkO0FBQ0QsZUFGRCxNQUdLO0FBQ0gsa0JBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDBCQUFRLEdBQVIsQ0FBWSxLQUFLLE1BQUwsSUFBZSxJQUFmLEdBQXNCLEtBQUssZ0JBQUwsQ0FBdEIsR0FBNkMsSUFBekQ7QUFDRCxpQkFGRDtBQUdEO0FBQ0Qsc0JBQVEsUUFBUjtBQUNEO0FBQ0QsMEJBQWMsRUFBZDtBQUNEO0FBakRVLFNBam5CTjtBQW9xQlAsZ0JBQVEsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDLE9BQWpDLEVBQTBDO0FBQ2hELGNBQ0UsU0FBUyxRQURYO0FBQUEsY0FFRSxRQUZGO0FBQUEsY0FHRSxLQUhGO0FBQUEsY0FJRSxRQUpGO0FBTUEsNEJBQWtCLG1CQUFtQixjQUFyQztBQUNBLG9CQUFrQixXQUFtQixPQUFyQztBQUNBLGNBQUcsT0FBTyxLQUFQLElBQWdCLFFBQWhCLElBQTRCLFdBQVcsU0FBMUMsRUFBcUQ7QUFDbkQsb0JBQVcsTUFBTSxLQUFOLENBQVksT0FBWixDQUFYO0FBQ0EsdUJBQVcsTUFBTSxNQUFOLEdBQWUsQ0FBMUI7QUFDQSxjQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ25DLGtCQUFJLGlCQUFrQixTQUFTLFFBQVYsR0FDakIsUUFBUSxNQUFNLFFBQVEsQ0FBZCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixFQUEyQixXQUEzQixFQUFSLEdBQW1ELE1BQU0sUUFBUSxDQUFkLEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBRGxDLEdBRWpCLEtBRko7QUFJQSxrQkFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxjQUFQLENBQWpCLEtBQThDLFNBQVMsUUFBM0QsRUFBdUU7QUFDckUseUJBQVMsT0FBTyxjQUFQLENBQVQ7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLGNBQVAsTUFBMkIsU0FBL0IsRUFBMkM7QUFDOUMsd0JBQVEsT0FBTyxjQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBLElBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sS0FBUCxDQUFqQixLQUFxQyxTQUFTLFFBQWxELEVBQThEO0FBQ2pFLHlCQUFTLE9BQU8sS0FBUCxDQUFUO0FBQ0QsZUFGSSxNQUdBLElBQUksT0FBTyxLQUFQLE1BQWtCLFNBQXRCLEVBQWtDO0FBQ3JDLHdCQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQTtBQUNILHVCQUFPLEtBQVAsQ0FBYSxNQUFNLE1BQW5CLEVBQTJCLEtBQTNCO0FBQ0EsdUJBQU8sS0FBUDtBQUNEO0FBQ0YsYUF2QkQ7QUF3QkQ7QUFDRCxjQUFLLEVBQUUsVUFBRixDQUFjLEtBQWQsQ0FBTCxFQUE2QjtBQUMzQix1QkFBVyxNQUFNLEtBQU4sQ0FBWSxPQUFaLEVBQXFCLGVBQXJCLENBQVg7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsdUJBQVcsS0FBWDtBQUNEO0FBQ0QsY0FBRyxFQUFFLE9BQUYsQ0FBVSxhQUFWLENBQUgsRUFBNkI7QUFDM0IsMEJBQWMsSUFBZCxDQUFtQixRQUFuQjtBQUNELFdBRkQsTUFHSyxJQUFHLGtCQUFrQixTQUFyQixFQUFnQztBQUNuQyw0QkFBZ0IsQ0FBQyxhQUFELEVBQWdCLFFBQWhCLENBQWhCO0FBQ0QsV0FGSSxNQUdBLElBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUM5Qiw0QkFBZ0IsUUFBaEI7QUFDRDtBQUNELGlCQUFPLEtBQVA7QUFDRDtBQXp0Qk0sT0FBVDs7QUE0dEJBLFVBQUcsYUFBSCxFQUFrQjtBQUNoQixZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsaUJBQU8sVUFBUDtBQUNEO0FBQ0QsZUFBTyxNQUFQLENBQWMsS0FBZDtBQUNELE9BTEQsTUFNSztBQUNILFlBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUN6QixtQkFBUyxNQUFULENBQWdCLFNBQWhCO0FBQ0Q7QUFDRCxlQUFPLFVBQVA7QUFDRDtBQUNGLEtBcndCSDs7QUF3d0JBLFdBQVEsa0JBQWtCLFNBQW5CLEdBQ0gsYUFERyxHQUVILElBRko7QUFJRCxHQTV4QkQ7O0FBOHhCQSxJQUFFLEVBQUYsQ0FBSyxRQUFMLENBQWMsUUFBZCxHQUF5Qjs7QUFFdkIsVUFBZSxVQUZRO0FBR3ZCLGVBQWUsVUFIUTs7QUFLdkIsWUFBZSxLQUxRO0FBTXZCLFdBQWUsS0FOUTtBQU92QixhQUFlLEtBUFE7QUFRdkIsaUJBQWUsSUFSUTs7QUFVdkIsWUFBZTtBQUNiLFdBQU0sQ0FETztBQUViLFdBQU07QUFGTyxLQVZROztBQWV2QixjQUFpQixHQWZNOztBQWlCdkIsb0JBQWlCLE1BakJNOztBQW1CdkIsaUJBQWlCLElBbkJNO0FBb0J2QixrQkFBaUIsSUFwQk07QUFxQnZCLGlCQUFpQixJQXJCTTs7QUF1QnZCLFdBQWlCLFNBdkJNO0FBd0J2QixlQUFpQixDQXhCTTtBQXlCdkIsZUFBa0IsT0FBTyxFQXpCRixFOztBQTJCdkIsYUFBaUIsS0EzQk07QUE0QnZCLFdBQWlCLEtBNUJNO0FBNkJ2QixXQUFpQixLQTdCTTs7QUErQnZCLG1CQUFnQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0IsS0FBdEIsRUFBNkIsS0FBN0IsRUFBbUM7QUFDakQsYUFBTyxJQUFQO0FBQ0QsS0FqQ3NCO0FBa0N2QixjQUFnQixVQUFTLE9BQVQsRUFBa0IsS0FBbEIsRUFBeUIsS0FBekIsRUFBK0IsQ0FBRSxDQWxDMUI7QUFtQ3ZCLGVBQWdCLFVBQVMsS0FBVCxFQUFlLENBQUUsQ0FuQ1Y7QUFvQ3ZCLGNBQWdCLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUFzQixDQUFFLENBcENqQjtBQXFDdkIsYUFBZ0IsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXNCLENBQUUsQ0FyQ2pCO0FBc0N2QixlQUFnQixVQUFTLEtBQVQsRUFBZ0IsS0FBaEIsRUFBc0IsQ0FBRSxDQXRDakI7O0FBd0N2QixXQUFXO0FBQ1QsY0FBYSx1Q0FESjtBQUVULGtCQUFhLCtCQUZKO0FBR1QsZUFBYSwrQkFISjtBQUlULGNBQWE7QUFKSixLQXhDWTs7QUErQ3ZCLFlBQVE7QUFDTixnQkFBVTtBQURKLEtBL0NlOztBQW1EdkIsY0FBVTtBQUNSLGVBQVUsU0FERjtBQUVSLGFBQVUsT0FGRjtBQUdSLGFBQVU7QUFIRixLQW5EYTs7QUF5RHZCLGNBQVc7QUFDVCxXQUFXLFFBREY7QUFFVCxhQUFXLFVBRkY7QUFHVCxnQkFBVztBQUhGLEtBekRZOztBQStEdkIsVUFBTztBQUNMLGNBQVUsS0FETDtBQUVMLGFBQVUsS0FGTDtBQUdMLGVBQVUsS0FITDtBQUlMLGVBQVUsS0FKTDtBQUtMLGVBQVUsWUFMTDtBQU1MLGFBQVU7QUFOTCxLQS9EZ0I7O0FBd0V2QixlQUFZO0FBQ1YsY0FBVSxRQURBO0FBRVYsYUFBVSxPQUZBO0FBR1YsZUFBVSxTQUhBO0FBSVYsZUFBVTtBQUpBOztBQXhFVyxHQUF6QjtBQWtGQyxDQW40QkEsRUFtNEJHLE1BbjRCSCxFQW00QlcsTUFuNEJYLEVBbTRCbUIsUUFuNEJuQiIsImZpbGUiOiJwcm9ncmVzcy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxuICogIyBTZW1hbnRpYyBVSSAtIFByb2dyZXNzXG4gKiBodHRwOi8vZ2l0aHViLmNvbS9zZW1hbnRpYy1vcmcvc2VtYW50aWMtdWkvXG4gKlxuICpcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxuICogaHR0cDovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVFxuICpcbiAqL1xuXG47KGZ1bmN0aW9uICgkLCB3aW5kb3csIGRvY3VtZW50LCB1bmRlZmluZWQpIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cbndpbmRvdyA9ICh0eXBlb2Ygd2luZG93ICE9ICd1bmRlZmluZWQnICYmIHdpbmRvdy5NYXRoID09IE1hdGgpXG4gID8gd2luZG93XG4gIDogKHR5cGVvZiBzZWxmICE9ICd1bmRlZmluZWQnICYmIHNlbGYuTWF0aCA9PSBNYXRoKVxuICAgID8gc2VsZlxuICAgIDogRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKVxuO1xuXG52YXJcbiAgZ2xvYmFsID0gKHR5cGVvZiB3aW5kb3cgIT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93Lk1hdGggPT0gTWF0aClcbiAgICA/IHdpbmRvd1xuICAgIDogKHR5cGVvZiBzZWxmICE9ICd1bmRlZmluZWQnICYmIHNlbGYuTWF0aCA9PSBNYXRoKVxuICAgICAgPyBzZWxmXG4gICAgICA6IEZ1bmN0aW9uKCdyZXR1cm4gdGhpcycpKClcbjtcblxuJC5mbi5wcm9ncmVzcyA9IGZ1bmN0aW9uKHBhcmFtZXRlcnMpIHtcbiAgdmFyXG4gICAgJGFsbE1vZHVsZXMgICAgPSAkKHRoaXMpLFxuXG4gICAgbW9kdWxlU2VsZWN0b3IgPSAkYWxsTW9kdWxlcy5zZWxlY3RvciB8fCAnJyxcblxuICAgIHRpbWUgICAgICAgICAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCksXG4gICAgcGVyZm9ybWFuY2UgICAgPSBbXSxcblxuICAgIHF1ZXJ5ICAgICAgICAgID0gYXJndW1lbnRzWzBdLFxuICAgIG1ldGhvZEludm9rZWQgID0gKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyksXG4gICAgcXVlcnlBcmd1bWVudHMgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSksXG5cbiAgICByZXR1cm5lZFZhbHVlXG4gIDtcblxuICAkYWxsTW9kdWxlc1xuICAgIC5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgdmFyXG4gICAgICAgIHNldHRpbmdzICAgICAgICAgID0gKCAkLmlzUGxhaW5PYmplY3QocGFyYW1ldGVycykgKVxuICAgICAgICAgID8gJC5leHRlbmQodHJ1ZSwge30sICQuZm4ucHJvZ3Jlc3Muc2V0dGluZ3MsIHBhcmFtZXRlcnMpXG4gICAgICAgICAgOiAkLmV4dGVuZCh7fSwgJC5mbi5wcm9ncmVzcy5zZXR0aW5ncyksXG5cbiAgICAgICAgY2xhc3NOYW1lICAgICAgID0gc2V0dGluZ3MuY2xhc3NOYW1lLFxuICAgICAgICBtZXRhZGF0YSAgICAgICAgPSBzZXR0aW5ncy5tZXRhZGF0YSxcbiAgICAgICAgbmFtZXNwYWNlICAgICAgID0gc2V0dGluZ3MubmFtZXNwYWNlLFxuICAgICAgICBzZWxlY3RvciAgICAgICAgPSBzZXR0aW5ncy5zZWxlY3RvcixcbiAgICAgICAgZXJyb3IgICAgICAgICAgID0gc2V0dGluZ3MuZXJyb3IsXG5cbiAgICAgICAgZXZlbnROYW1lc3BhY2UgID0gJy4nICsgbmFtZXNwYWNlLFxuICAgICAgICBtb2R1bGVOYW1lc3BhY2UgPSAnbW9kdWxlLScgKyBuYW1lc3BhY2UsXG5cbiAgICAgICAgJG1vZHVsZSAgICAgICAgID0gJCh0aGlzKSxcbiAgICAgICAgJGJhciAgICAgICAgICAgID0gJCh0aGlzKS5maW5kKHNlbGVjdG9yLmJhciksXG4gICAgICAgICRwcm9ncmVzcyAgICAgICA9ICQodGhpcykuZmluZChzZWxlY3Rvci5wcm9ncmVzcyksXG4gICAgICAgICRsYWJlbCAgICAgICAgICA9ICQodGhpcykuZmluZChzZWxlY3Rvci5sYWJlbCksXG5cbiAgICAgICAgZWxlbWVudCAgICAgICAgID0gdGhpcyxcbiAgICAgICAgaW5zdGFuY2UgICAgICAgID0gJG1vZHVsZS5kYXRhKG1vZHVsZU5hbWVzcGFjZSksXG5cbiAgICAgICAgYW5pbWF0aW5nID0gZmFsc2UsXG4gICAgICAgIHRyYW5zaXRpb25FbmQsXG4gICAgICAgIG1vZHVsZVxuICAgICAgO1xuXG4gICAgICBtb2R1bGUgPSB7XG5cbiAgICAgICAgaW5pdGlhbGl6ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdJbml0aWFsaXppbmcgcHJvZ3Jlc3MgYmFyJywgc2V0dGluZ3MpO1xuXG4gICAgICAgICAgbW9kdWxlLnNldC5kdXJhdGlvbigpO1xuICAgICAgICAgIG1vZHVsZS5zZXQudHJhbnNpdGlvbkV2ZW50KCk7XG5cbiAgICAgICAgICBtb2R1bGUucmVhZC5tZXRhZGF0YSgpO1xuICAgICAgICAgIG1vZHVsZS5yZWFkLnNldHRpbmdzKCk7XG5cbiAgICAgICAgICBtb2R1bGUuaW5zdGFudGlhdGUoKTtcbiAgICAgICAgfSxcblxuICAgICAgICBpbnN0YW50aWF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1N0b3JpbmcgaW5zdGFuY2Ugb2YgcHJvZ3Jlc3MnLCBtb2R1bGUpO1xuICAgICAgICAgIGluc3RhbmNlID0gbW9kdWxlO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5kYXRhKG1vZHVsZU5hbWVzcGFjZSwgbW9kdWxlKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcbiAgICAgICAgZGVzdHJveTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0Rlc3Ryb3lpbmcgcHJldmlvdXMgcHJvZ3Jlc3MgZm9yJywgJG1vZHVsZSk7XG4gICAgICAgICAgY2xlYXJJbnRlcnZhbChpbnN0YW5jZS5pbnRlcnZhbCk7XG4gICAgICAgICAgbW9kdWxlLnJlbW92ZS5zdGF0ZSgpO1xuICAgICAgICAgICRtb2R1bGUucmVtb3ZlRGF0YShtb2R1bGVOYW1lc3BhY2UpO1xuICAgICAgICAgIGluc3RhbmNlID0gdW5kZWZpbmVkO1xuICAgICAgICB9LFxuXG4gICAgICAgIHJlc2V0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUucmVtb3ZlLm5leHRWYWx1ZSgpO1xuICAgICAgICAgIG1vZHVsZS51cGRhdGUucHJvZ3Jlc3MoMCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgY29tcGxldGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKG1vZHVsZS5wZXJjZW50ID09PSB1bmRlZmluZWQgfHwgbW9kdWxlLnBlcmNlbnQgPCAxMDApIHtcbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUucHJvZ3Jlc3NQb2xsKCk7XG4gICAgICAgICAgICBtb2R1bGUuc2V0LnBlcmNlbnQoMTAwKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVhZDoge1xuICAgICAgICAgIG1ldGFkYXRhOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBkYXRhID0ge1xuICAgICAgICAgICAgICAgIHBlcmNlbnQgOiAkbW9kdWxlLmRhdGEobWV0YWRhdGEucGVyY2VudCksXG4gICAgICAgICAgICAgICAgdG90YWwgICA6ICRtb2R1bGUuZGF0YShtZXRhZGF0YS50b3RhbCksXG4gICAgICAgICAgICAgICAgdmFsdWUgICA6ICRtb2R1bGUuZGF0YShtZXRhZGF0YS52YWx1ZSlcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoZGF0YS5wZXJjZW50KSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ3VycmVudCBwZXJjZW50IHZhbHVlIHNldCBmcm9tIG1ldGFkYXRhJywgZGF0YS5wZXJjZW50KTtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5wZXJjZW50KGRhdGEucGVyY2VudCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihkYXRhLnRvdGFsKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVG90YWwgdmFsdWUgc2V0IGZyb20gbWV0YWRhdGEnLCBkYXRhLnRvdGFsKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC50b3RhbChkYXRhLnRvdGFsKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKGRhdGEudmFsdWUpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDdXJyZW50IHZhbHVlIHNldCBmcm9tIG1ldGFkYXRhJywgZGF0YS52YWx1ZSk7XG4gICAgICAgICAgICAgIG1vZHVsZS5zZXQudmFsdWUoZGF0YS52YWx1ZSk7XG4gICAgICAgICAgICAgIG1vZHVsZS5zZXQucHJvZ3Jlc3MoZGF0YS52YWx1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzZXR0aW5nczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy50b3RhbCAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDdXJyZW50IHRvdGFsIHNldCBpbiBzZXR0aW5ncycsIHNldHRpbmdzLnRvdGFsKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC50b3RhbChzZXR0aW5ncy50b3RhbCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihzZXR0aW5ncy52YWx1ZSAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDdXJyZW50IHZhbHVlIHNldCBpbiBzZXR0aW5ncycsIHNldHRpbmdzLnZhbHVlKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC52YWx1ZShzZXR0aW5ncy52YWx1ZSk7XG4gICAgICAgICAgICAgIG1vZHVsZS5zZXQucHJvZ3Jlc3MobW9kdWxlLnZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmNlbnQgIT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ3VycmVudCBwZXJjZW50IHNldCBpbiBzZXR0aW5ncycsIHNldHRpbmdzLnBlcmNlbnQpO1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0LnBlcmNlbnQoc2V0dGluZ3MucGVyY2VudCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGluY3JlbWVudDogZnVuY3Rpb24oaW5jcmVtZW50VmFsdWUpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIG1heFZhbHVlLFxuICAgICAgICAgICAgc3RhcnRWYWx1ZSxcbiAgICAgICAgICAgIG5ld1ZhbHVlXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKCBtb2R1bGUuaGFzLnRvdGFsKCkgKSB7XG4gICAgICAgICAgICBzdGFydFZhbHVlICAgICA9IG1vZHVsZS5nZXQudmFsdWUoKTtcbiAgICAgICAgICAgIGluY3JlbWVudFZhbHVlID0gaW5jcmVtZW50VmFsdWUgfHwgMTtcbiAgICAgICAgICAgIG5ld1ZhbHVlICAgICAgID0gc3RhcnRWYWx1ZSArIGluY3JlbWVudFZhbHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHN0YXJ0VmFsdWUgICAgID0gbW9kdWxlLmdldC5wZXJjZW50KCk7XG4gICAgICAgICAgICBpbmNyZW1lbnRWYWx1ZSA9IGluY3JlbWVudFZhbHVlIHx8IG1vZHVsZS5nZXQucmFuZG9tVmFsdWUoKTtcblxuICAgICAgICAgICAgbmV3VmFsdWUgICAgICAgPSBzdGFydFZhbHVlICsgaW5jcmVtZW50VmFsdWU7XG4gICAgICAgICAgICBtYXhWYWx1ZSAgICAgICA9IDEwMDtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW5jcmVtZW50aW5nIHBlcmNlbnRhZ2UgYnknLCBzdGFydFZhbHVlLCBuZXdWYWx1ZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIG5ld1ZhbHVlID0gbW9kdWxlLmdldC5ub3JtYWxpemVkVmFsdWUobmV3VmFsdWUpO1xuICAgICAgICAgIG1vZHVsZS5zZXQucHJvZ3Jlc3MobmV3VmFsdWUpO1xuICAgICAgICB9LFxuICAgICAgICBkZWNyZW1lbnQ6IGZ1bmN0aW9uKGRlY3JlbWVudFZhbHVlKSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICB0b3RhbCAgICAgPSBtb2R1bGUuZ2V0LnRvdGFsKCksXG4gICAgICAgICAgICBzdGFydFZhbHVlLFxuICAgICAgICAgICAgbmV3VmFsdWVcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYodG90YWwpIHtcbiAgICAgICAgICAgIHN0YXJ0VmFsdWUgICAgID0gIG1vZHVsZS5nZXQudmFsdWUoKTtcbiAgICAgICAgICAgIGRlY3JlbWVudFZhbHVlID0gIGRlY3JlbWVudFZhbHVlIHx8IDE7XG4gICAgICAgICAgICBuZXdWYWx1ZSAgICAgICA9ICBzdGFydFZhbHVlIC0gZGVjcmVtZW50VmFsdWU7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0RlY3JlbWVudGluZyB2YWx1ZSBieScsIGRlY3JlbWVudFZhbHVlLCBzdGFydFZhbHVlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBzdGFydFZhbHVlICAgICA9ICBtb2R1bGUuZ2V0LnBlcmNlbnQoKTtcbiAgICAgICAgICAgIGRlY3JlbWVudFZhbHVlID0gIGRlY3JlbWVudFZhbHVlIHx8IG1vZHVsZS5nZXQucmFuZG9tVmFsdWUoKTtcbiAgICAgICAgICAgIG5ld1ZhbHVlICAgICAgID0gIHN0YXJ0VmFsdWUgLSBkZWNyZW1lbnRWYWx1ZTtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRGVjcmVtZW50aW5nIHBlcmNlbnRhZ2UgYnknLCBkZWNyZW1lbnRWYWx1ZSwgc3RhcnRWYWx1ZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIG5ld1ZhbHVlID0gbW9kdWxlLmdldC5ub3JtYWxpemVkVmFsdWUobmV3VmFsdWUpO1xuICAgICAgICAgIG1vZHVsZS5zZXQucHJvZ3Jlc3MobmV3VmFsdWUpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGhhczoge1xuICAgICAgICAgIHByb2dyZXNzUG9sbDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlLnByb2dyZXNzUG9sbDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHRvdGFsOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAobW9kdWxlLmdldC50b3RhbCgpICE9PSBmYWxzZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGdldDoge1xuICAgICAgICAgIHRleHQ6IGZ1bmN0aW9uKHRlbXBsYXRlVGV4dCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHZhbHVlICAgPSBtb2R1bGUudmFsdWUgICAgICAgICAgICAgICAgfHwgMCxcbiAgICAgICAgICAgICAgdG90YWwgICA9IG1vZHVsZS50b3RhbCAgICAgICAgICAgICAgICB8fCAwLFxuICAgICAgICAgICAgICBwZXJjZW50ID0gKGFuaW1hdGluZylcbiAgICAgICAgICAgICAgICA/IG1vZHVsZS5nZXQuZGlzcGxheVBlcmNlbnQoKVxuICAgICAgICAgICAgICAgIDogbW9kdWxlLnBlcmNlbnQgfHwgMCxcbiAgICAgICAgICAgICAgbGVmdCA9IChtb2R1bGUudG90YWwgPiAwKVxuICAgICAgICAgICAgICAgID8gKHRvdGFsIC0gdmFsdWUpXG4gICAgICAgICAgICAgICAgOiAoMTAwIC0gcGVyY2VudClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHRlbXBsYXRlVGV4dCA9IHRlbXBsYXRlVGV4dCB8fCAnJztcbiAgICAgICAgICAgIHRlbXBsYXRlVGV4dCA9IHRlbXBsYXRlVGV4dFxuICAgICAgICAgICAgICAucmVwbGFjZSgne3ZhbHVlfScsIHZhbHVlKVxuICAgICAgICAgICAgICAucmVwbGFjZSgne3RvdGFsfScsIHRvdGFsKVxuICAgICAgICAgICAgICAucmVwbGFjZSgne2xlZnR9JywgbGVmdClcbiAgICAgICAgICAgICAgLnJlcGxhY2UoJ3twZXJjZW50fScsIHBlcmNlbnQpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQWRkaW5nIHZhcmlhYmxlcyB0byBwcm9ncmVzcyBiYXIgdGV4dCcsIHRlbXBsYXRlVGV4dCk7XG4gICAgICAgICAgICByZXR1cm4gdGVtcGxhdGVUZXh0O1xuICAgICAgICAgIH0sXG5cbiAgICAgICAgICBub3JtYWxpemVkVmFsdWU6IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICAgICAgICBpZih2YWx1ZSA8IDApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdWYWx1ZSBjYW5ub3QgZGVjcmVtZW50IGJlbG93IDAnKTtcbiAgICAgICAgICAgICAgcmV0dXJuIDA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihtb2R1bGUuaGFzLnRvdGFsKCkpIHtcbiAgICAgICAgICAgICAgaWYodmFsdWUgPiBtb2R1bGUudG90YWwpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1ZhbHVlIGNhbm5vdCBpbmNyZW1lbnQgYWJvdmUgdG90YWwnLCBtb2R1bGUudG90YWwpO1xuICAgICAgICAgICAgICAgIHJldHVybiBtb2R1bGUudG90YWw7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYodmFsdWUgPiAxMDAgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVmFsdWUgY2Fubm90IGluY3JlbWVudCBhYm92ZSAxMDAgcGVyY2VudCcpO1xuICAgICAgICAgICAgICByZXR1cm4gMTAwO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHZhbHVlO1xuICAgICAgICAgIH0sXG5cbiAgICAgICAgICB1cGRhdGVJbnRlcnZhbDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy51cGRhdGVJbnRlcnZhbCA9PSAnYXV0bycpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHNldHRpbmdzLmR1cmF0aW9uO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHNldHRpbmdzLnVwZGF0ZUludGVydmFsO1xuICAgICAgICAgIH0sXG5cbiAgICAgICAgICByYW5kb21WYWx1ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0dlbmVyYXRpbmcgcmFuZG9tIGluY3JlbWVudCBwZXJjZW50YWdlJyk7XG4gICAgICAgICAgICByZXR1cm4gTWF0aC5mbG9vcigoTWF0aC5yYW5kb20oKSAqIHNldHRpbmdzLnJhbmRvbS5tYXgpICsgc2V0dGluZ3MucmFuZG9tLm1pbik7XG4gICAgICAgICAgfSxcblxuICAgICAgICAgIG51bWVyaWNWYWx1ZTogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgICAgIHJldHVybiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJylcbiAgICAgICAgICAgICAgPyAodmFsdWUucmVwbGFjZSgvW15cXGQuXS9nLCAnJykgIT09ICcnKVxuICAgICAgICAgICAgICAgID8gKyh2YWx1ZS5yZXBsYWNlKC9bXlxcZC5dL2csICcnKSlcbiAgICAgICAgICAgICAgICA6IGZhbHNlXG4gICAgICAgICAgICAgIDogdmFsdWVcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuXG4gICAgICAgICAgdHJhbnNpdGlvbkVuZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgZWxlbWVudCAgICAgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdlbGVtZW50JyksXG4gICAgICAgICAgICAgIHRyYW5zaXRpb25zID0ge1xuICAgICAgICAgICAgICAgICd0cmFuc2l0aW9uJyAgICAgICA6J3RyYW5zaXRpb25lbmQnLFxuICAgICAgICAgICAgICAgICdPVHJhbnNpdGlvbicgICAgICA6J29UcmFuc2l0aW9uRW5kJyxcbiAgICAgICAgICAgICAgICAnTW96VHJhbnNpdGlvbicgICAgOid0cmFuc2l0aW9uZW5kJyxcbiAgICAgICAgICAgICAgICAnV2Via2l0VHJhbnNpdGlvbicgOid3ZWJraXRUcmFuc2l0aW9uRW5kJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB0cmFuc2l0aW9uXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBmb3IodHJhbnNpdGlvbiBpbiB0cmFuc2l0aW9ucyl7XG4gICAgICAgICAgICAgIGlmKCBlbGVtZW50LnN0eWxlW3RyYW5zaXRpb25dICE9PSB1bmRlZmluZWQgKXtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJhbnNpdGlvbnNbdHJhbnNpdGlvbl07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuXG4gICAgICAgICAgLy8gZ2V0cyBjdXJyZW50IGRpc3BsYXllZCBwZXJjZW50YWdlIChpZiBhbmltYXRpbmcgdmFsdWVzIHRoaXMgaXMgdGhlIGludGVybWVkaWFyeSB2YWx1ZSlcbiAgICAgICAgICBkaXNwbGF5UGVyY2VudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgYmFyV2lkdGggICAgICAgPSAkYmFyLndpZHRoKCksXG4gICAgICAgICAgICAgIHRvdGFsV2lkdGggICAgID0gJG1vZHVsZS53aWR0aCgpLFxuICAgICAgICAgICAgICBtaW5EaXNwbGF5ICAgICA9IHBhcnNlSW50KCRiYXIuY3NzKCdtaW4td2lkdGgnKSwgMTApLFxuICAgICAgICAgICAgICBkaXNwbGF5UGVyY2VudCA9IChiYXJXaWR0aCA+IG1pbkRpc3BsYXkpXG4gICAgICAgICAgICAgICAgPyAoYmFyV2lkdGggLyB0b3RhbFdpZHRoICogMTAwKVxuICAgICAgICAgICAgICAgIDogbW9kdWxlLnBlcmNlbnRcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHJldHVybiAoc2V0dGluZ3MucHJlY2lzaW9uID4gMClcbiAgICAgICAgICAgICAgPyBNYXRoLnJvdW5kKGRpc3BsYXlQZXJjZW50ICogKDEwICogc2V0dGluZ3MucHJlY2lzaW9uKSkgLyAoMTAgKiBzZXR0aW5ncy5wcmVjaXNpb24pXG4gICAgICAgICAgICAgIDogTWF0aC5yb3VuZChkaXNwbGF5UGVyY2VudClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuXG4gICAgICAgICAgcGVyY2VudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlLnBlcmNlbnQgfHwgMDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGUubmV4dFZhbHVlIHx8IG1vZHVsZS52YWx1ZSB8fCAwO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgdG90YWw6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZS50b3RhbCB8fCBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgY3JlYXRlOiB7XG4gICAgICAgICAgcHJvZ3Jlc3NQb2xsOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS5wcm9ncmVzc1BvbGwgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICBtb2R1bGUudXBkYXRlLnRvTmV4dFZhbHVlKCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUucHJvZ3Jlc3NQb2xsKCk7XG4gICAgICAgICAgICB9LCBtb2R1bGUuZ2V0LnVwZGF0ZUludGVydmFsKCkpO1xuICAgICAgICAgIH0sXG4gICAgICAgIH0sXG5cbiAgICAgICAgaXM6IHtcbiAgICAgICAgICBjb21wbGV0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlLmlzLnN1Y2Nlc3MoKSB8fCBtb2R1bGUuaXMud2FybmluZygpIHx8IG1vZHVsZS5pcy5lcnJvcigpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5oYXNDbGFzcyhjbGFzc05hbWUuc3VjY2Vzcyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB3YXJuaW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkbW9kdWxlLmhhc0NsYXNzKGNsYXNzTmFtZS53YXJuaW5nKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGVycm9yOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkbW9kdWxlLmhhc0NsYXNzKGNsYXNzTmFtZS5lcnJvcik7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBhY3RpdmU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRtb2R1bGUuaGFzQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB2aXNpYmxlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkbW9kdWxlLmlzKCc6dmlzaWJsZScpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICByZW1vdmU6IHtcbiAgICAgICAgICBwcm9ncmVzc1BvbGw6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1JlbW92aW5nIHByb2dyZXNzIHBvbGwgdGltZXInKTtcbiAgICAgICAgICAgIGlmKG1vZHVsZS5wcm9ncmVzc1BvbGwpIHtcbiAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5wcm9ncmVzc1BvbGwpO1xuICAgICAgICAgICAgICBkZWxldGUgbW9kdWxlLnByb2dyZXNzUG9sbDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIG5leHRWYWx1ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVtb3ZpbmcgcHJvZ3Jlc3MgdmFsdWUgc3RvcmVkIGZvciBuZXh0IHVwZGF0ZScpO1xuICAgICAgICAgICAgZGVsZXRlIG1vZHVsZS5uZXh0VmFsdWU7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzdGF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVtb3Zpbmcgc3RvcmVkIHN0YXRlJyk7XG4gICAgICAgICAgICBkZWxldGUgbW9kdWxlLnRvdGFsO1xuICAgICAgICAgICAgZGVsZXRlIG1vZHVsZS5wZXJjZW50O1xuICAgICAgICAgICAgZGVsZXRlIG1vZHVsZS52YWx1ZTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFjdGl2ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVtb3ZpbmcgYWN0aXZlIHN0YXRlJyk7XG4gICAgICAgICAgICAkbW9kdWxlLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5hY3RpdmUpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVtb3Zpbmcgc3VjY2VzcyBzdGF0ZScpO1xuICAgICAgICAgICAgJG1vZHVsZS5yZW1vdmVDbGFzcyhjbGFzc05hbWUuc3VjY2Vzcyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB3YXJuaW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZW1vdmluZyB3YXJuaW5nIHN0YXRlJyk7XG4gICAgICAgICAgICAkbW9kdWxlLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS53YXJuaW5nKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGVycm9yOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZW1vdmluZyBlcnJvciBzdGF0ZScpO1xuICAgICAgICAgICAgJG1vZHVsZS5yZW1vdmVDbGFzcyhjbGFzc05hbWUuZXJyb3IpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZXQ6IHtcbiAgICAgICAgICBiYXJXaWR0aDogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgICAgIGlmKHZhbHVlID4gMTAwKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci50b29IaWdoLCB2YWx1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmICh2YWx1ZSA8IDApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLnRvb0xvdywgdmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICRiYXJcbiAgICAgICAgICAgICAgICAuY3NzKCd3aWR0aCcsIHZhbHVlICsgJyUnKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgICAuYXR0cignZGF0YS1wZXJjZW50JywgcGFyc2VJbnQodmFsdWUsIDEwKSlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgZHVyYXRpb246IGZ1bmN0aW9uKGR1cmF0aW9uKSB7XG4gICAgICAgICAgICBkdXJhdGlvbiA9IGR1cmF0aW9uIHx8IHNldHRpbmdzLmR1cmF0aW9uO1xuICAgICAgICAgICAgZHVyYXRpb24gPSAodHlwZW9mIGR1cmF0aW9uID09ICdudW1iZXInKVxuICAgICAgICAgICAgICA/IGR1cmF0aW9uICsgJ21zJ1xuICAgICAgICAgICAgICA6IGR1cmF0aW9uXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU2V0dGluZyBwcm9ncmVzcyBiYXIgdHJhbnNpdGlvbiBkdXJhdGlvbicsIGR1cmF0aW9uKTtcbiAgICAgICAgICAgICRiYXJcbiAgICAgICAgICAgICAgLmNzcyh7XG4gICAgICAgICAgICAgICAgJ3RyYW5zaXRpb24tZHVyYXRpb24nOiAgZHVyYXRpb25cbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHBlcmNlbnQ6IGZ1bmN0aW9uKHBlcmNlbnQpIHtcbiAgICAgICAgICAgIHBlcmNlbnQgPSAodHlwZW9mIHBlcmNlbnQgPT0gJ3N0cmluZycpXG4gICAgICAgICAgICAgID8gKyhwZXJjZW50LnJlcGxhY2UoJyUnLCAnJykpXG4gICAgICAgICAgICAgIDogcGVyY2VudFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgLy8gcm91bmQgZGlzcGxheSBwZXJjZW50YWdlXG4gICAgICAgICAgICBwZXJjZW50ID0gKHNldHRpbmdzLnByZWNpc2lvbiA+IDApXG4gICAgICAgICAgICAgID8gTWF0aC5yb3VuZChwZXJjZW50ICogKDEwICogc2V0dGluZ3MucHJlY2lzaW9uKSkgLyAoMTAgKiBzZXR0aW5ncy5wcmVjaXNpb24pXG4gICAgICAgICAgICAgIDogTWF0aC5yb3VuZChwZXJjZW50KVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnBlcmNlbnQgPSBwZXJjZW50O1xuICAgICAgICAgICAgaWYoICFtb2R1bGUuaGFzLnRvdGFsKCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52YWx1ZSA9IChzZXR0aW5ncy5wcmVjaXNpb24gPiAwKVxuICAgICAgICAgICAgICAgID8gTWF0aC5yb3VuZCggKHBlcmNlbnQgLyAxMDApICogbW9kdWxlLnRvdGFsICogKDEwICogc2V0dGluZ3MucHJlY2lzaW9uKSkgLyAoMTAgKiBzZXR0aW5ncy5wcmVjaXNpb24pXG4gICAgICAgICAgICAgICAgOiBNYXRoLnJvdW5kKCAocGVyY2VudCAvIDEwMCkgKiBtb2R1bGUudG90YWwgKiAxMCkgLyAxMFxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKHNldHRpbmdzLmxpbWl0VmFsdWVzKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnZhbHVlID0gKG1vZHVsZS52YWx1ZSA+IDEwMClcbiAgICAgICAgICAgICAgICAgID8gMTAwXG4gICAgICAgICAgICAgICAgICA6IChtb2R1bGUudmFsdWUgPCAwKVxuICAgICAgICAgICAgICAgICAgICA/IDBcbiAgICAgICAgICAgICAgICAgICAgOiBtb2R1bGUudmFsdWVcbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS5zZXQuYmFyV2lkdGgocGVyY2VudCk7XG4gICAgICAgICAgICBtb2R1bGUuc2V0LmxhYmVsSW50ZXJ2YWwoKTtcbiAgICAgICAgICAgIG1vZHVsZS5zZXQubGFiZWxzKCk7XG4gICAgICAgICAgICBzZXR0aW5ncy5vbkNoYW5nZS5jYWxsKGVsZW1lbnQsIHBlcmNlbnQsIG1vZHVsZS52YWx1ZSwgbW9kdWxlLnRvdGFsKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGxhYmVsSW50ZXJ2YWw6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGFuaW1hdGlvbkNhbGxiYWNrID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0JhciBmaW5pc2hlZCBhbmltYXRpbmcsIHJlbW92aW5nIGNvbnRpbnVvdXMgbGFiZWwgdXBkYXRlcycpO1xuICAgICAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwobW9kdWxlLmludGVydmFsKTtcbiAgICAgICAgICAgICAgICBhbmltYXRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICBtb2R1bGUuc2V0LmxhYmVscygpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBjbGVhckludGVydmFsKG1vZHVsZS5pbnRlcnZhbCk7XG4gICAgICAgICAgICAkYmFyLm9uZSh0cmFuc2l0aW9uRW5kICsgZXZlbnROYW1lc3BhY2UsIGFuaW1hdGlvbkNhbGxiYWNrKTtcbiAgICAgICAgICAgIGFuaW1hdGluZyA9IHRydWU7XG4gICAgICAgICAgICBtb2R1bGUuaW50ZXJ2YWwgPSBzZXRJbnRlcnZhbChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICAgaXNJbkRPTSA9ICQuY29udGFpbnMoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LCBlbGVtZW50KVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKCFpc0luRE9NKSB7XG4gICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChtb2R1bGUuaW50ZXJ2YWwpO1xuICAgICAgICAgICAgICAgIGFuaW1hdGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIG1vZHVsZS5zZXQubGFiZWxzKCk7XG4gICAgICAgICAgICB9LCBzZXR0aW5ncy5mcmFtZXJhdGUpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgbGFiZWxzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIGJvdGggYmFyIHByb2dyZXNzIGFuZCBvdXRlciBsYWJlbCB0ZXh0Jyk7XG4gICAgICAgICAgICBtb2R1bGUuc2V0LmJhckxhYmVsKCk7XG4gICAgICAgICAgICBtb2R1bGUuc2V0LnN0YXRlKCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBsYWJlbDogZnVuY3Rpb24odGV4dCkge1xuICAgICAgICAgICAgdGV4dCA9IHRleHQgfHwgJyc7XG4gICAgICAgICAgICBpZih0ZXh0KSB7XG4gICAgICAgICAgICAgIHRleHQgPSBtb2R1bGUuZ2V0LnRleHQodGV4dCk7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIGxhYmVsIHRvIHRleHQnLCB0ZXh0KTtcbiAgICAgICAgICAgICAgJGxhYmVsLnRleHQodGV4dCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzdGF0ZTogZnVuY3Rpb24ocGVyY2VudCkge1xuICAgICAgICAgICAgcGVyY2VudCA9IChwZXJjZW50ICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgID8gcGVyY2VudFxuICAgICAgICAgICAgICA6IG1vZHVsZS5wZXJjZW50XG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihwZXJjZW50ID09PSAxMDApIHtcbiAgICAgICAgICAgICAgaWYoc2V0dGluZ3MuYXV0b1N1Y2Nlc3MgJiYgIShtb2R1bGUuaXMud2FybmluZygpIHx8IG1vZHVsZS5pcy5lcnJvcigpIHx8IG1vZHVsZS5pcy5zdWNjZXNzKCkpKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnNldC5zdWNjZXNzKCk7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdBdXRvbWF0aWNhbGx5IHRyaWdnZXJpbmcgc3VjY2VzcyBhdCAxMDAlJyk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1JlYWNoZWQgMTAwJSByZW1vdmluZyBhY3RpdmUgc3RhdGUnKTtcbiAgICAgICAgICAgICAgICBtb2R1bGUucmVtb3ZlLmFjdGl2ZSgpO1xuICAgICAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUucHJvZ3Jlc3NQb2xsKCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYocGVyY2VudCA+IDApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0FkanVzdGluZyBhY3RpdmUgcHJvZ3Jlc3MgYmFyIGxhYmVsJywgcGVyY2VudCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5zZXQuYWN0aXZlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5hY3RpdmUoKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5sYWJlbChzZXR0aW5ncy50ZXh0LmFjdGl2ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBiYXJMYWJlbDogZnVuY3Rpb24odGV4dCkge1xuICAgICAgICAgICAgaWYodGV4dCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICRwcm9ncmVzcy50ZXh0KCBtb2R1bGUuZ2V0LnRleHQodGV4dCkgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoc2V0dGluZ3MubGFiZWwgPT0gJ3JhdGlvJyAmJiBtb2R1bGUudG90YWwpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0FkZGluZyByYXRpbyB0byBiYXIgbGFiZWwnKTtcbiAgICAgICAgICAgICAgJHByb2dyZXNzLnRleHQoIG1vZHVsZS5nZXQudGV4dChzZXR0aW5ncy50ZXh0LnJhdGlvKSApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZihzZXR0aW5ncy5sYWJlbCA9PSAncGVyY2VudCcpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0FkZGluZyBwZXJjZW50YWdlIHRvIGJhciBsYWJlbCcpO1xuICAgICAgICAgICAgICAkcHJvZ3Jlc3MudGV4dCggbW9kdWxlLmdldC50ZXh0KHNldHRpbmdzLnRleHQucGVyY2VudCkgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGFjdGl2ZTogZnVuY3Rpb24odGV4dCkge1xuICAgICAgICAgICAgdGV4dCA9IHRleHQgfHwgc2V0dGluZ3MudGV4dC5hY3RpdmU7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NldHRpbmcgYWN0aXZlIHN0YXRlJyk7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5zaG93QWN0aXZpdHkgJiYgIW1vZHVsZS5pcy5hY3RpdmUoKSApIHtcbiAgICAgICAgICAgICAgJG1vZHVsZS5hZGRDbGFzcyhjbGFzc05hbWUuYWN0aXZlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUud2FybmluZygpO1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5lcnJvcigpO1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5zdWNjZXNzKCk7XG4gICAgICAgICAgICB0ZXh0ID0gc2V0dGluZ3Mub25MYWJlbFVwZGF0ZSgnYWN0aXZlJywgdGV4dCwgbW9kdWxlLnZhbHVlLCBtb2R1bGUudG90YWwpO1xuICAgICAgICAgICAgaWYodGV4dCkge1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0LmxhYmVsKHRleHQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgJGJhci5vbmUodHJhbnNpdGlvbkVuZCArIGV2ZW50TmFtZXNwYWNlLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgc2V0dGluZ3Mub25BY3RpdmUuY2FsbChlbGVtZW50LCBtb2R1bGUudmFsdWUsIG1vZHVsZS50b3RhbCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHN1Y2Nlc3MgOiBmdW5jdGlvbih0ZXh0KSB7XG4gICAgICAgICAgICB0ZXh0ID0gdGV4dCB8fCBzZXR0aW5ncy50ZXh0LnN1Y2Nlc3MgfHwgc2V0dGluZ3MudGV4dC5hY3RpdmU7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NldHRpbmcgc3VjY2VzcyBzdGF0ZScpO1xuICAgICAgICAgICAgJG1vZHVsZS5hZGRDbGFzcyhjbGFzc05hbWUuc3VjY2Vzcyk7XG4gICAgICAgICAgICBtb2R1bGUucmVtb3ZlLmFjdGl2ZSgpO1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS53YXJuaW5nKCk7XG4gICAgICAgICAgICBtb2R1bGUucmVtb3ZlLmVycm9yKCk7XG4gICAgICAgICAgICBtb2R1bGUuY29tcGxldGUoKTtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnRleHQuc3VjY2Vzcykge1xuICAgICAgICAgICAgICB0ZXh0ID0gc2V0dGluZ3Mub25MYWJlbFVwZGF0ZSgnc3VjY2VzcycsIHRleHQsIG1vZHVsZS52YWx1ZSwgbW9kdWxlLnRvdGFsKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5sYWJlbCh0ZXh0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICB0ZXh0ID0gc2V0dGluZ3Mub25MYWJlbFVwZGF0ZSgnYWN0aXZlJywgdGV4dCwgbW9kdWxlLnZhbHVlLCBtb2R1bGUudG90YWwpO1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0LmxhYmVsKHRleHQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgJGJhci5vbmUodHJhbnNpdGlvbkVuZCArIGV2ZW50TmFtZXNwYWNlLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgc2V0dGluZ3Mub25TdWNjZXNzLmNhbGwoZWxlbWVudCwgbW9kdWxlLnRvdGFsKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgd2FybmluZyA6IGZ1bmN0aW9uKHRleHQpIHtcbiAgICAgICAgICAgIHRleHQgPSB0ZXh0IHx8IHNldHRpbmdzLnRleHQud2FybmluZztcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2V0dGluZyB3YXJuaW5nIHN0YXRlJyk7XG4gICAgICAgICAgICAkbW9kdWxlLmFkZENsYXNzKGNsYXNzTmFtZS53YXJuaW5nKTtcbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUuYWN0aXZlKCk7XG4gICAgICAgICAgICBtb2R1bGUucmVtb3ZlLnN1Y2Nlc3MoKTtcbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUuZXJyb3IoKTtcbiAgICAgICAgICAgIG1vZHVsZS5jb21wbGV0ZSgpO1xuICAgICAgICAgICAgdGV4dCA9IHNldHRpbmdzLm9uTGFiZWxVcGRhdGUoJ3dhcm5pbmcnLCB0ZXh0LCBtb2R1bGUudmFsdWUsIG1vZHVsZS50b3RhbCk7XG4gICAgICAgICAgICBpZih0ZXh0KSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5zZXQubGFiZWwodGV4dCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAkYmFyLm9uZSh0cmFuc2l0aW9uRW5kICsgZXZlbnROYW1lc3BhY2UsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICBzZXR0aW5ncy5vbldhcm5pbmcuY2FsbChlbGVtZW50LCBtb2R1bGUudmFsdWUsIG1vZHVsZS50b3RhbCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGVycm9yIDogZnVuY3Rpb24odGV4dCkge1xuICAgICAgICAgICAgdGV4dCA9IHRleHQgfHwgc2V0dGluZ3MudGV4dC5lcnJvcjtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2V0dGluZyBlcnJvciBzdGF0ZScpO1xuICAgICAgICAgICAgJG1vZHVsZS5hZGRDbGFzcyhjbGFzc05hbWUuZXJyb3IpO1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5hY3RpdmUoKTtcbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUuc3VjY2VzcygpO1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS53YXJuaW5nKCk7XG4gICAgICAgICAgICBtb2R1bGUuY29tcGxldGUoKTtcbiAgICAgICAgICAgIHRleHQgPSBzZXR0aW5ncy5vbkxhYmVsVXBkYXRlKCdlcnJvcicsIHRleHQsIG1vZHVsZS52YWx1ZSwgbW9kdWxlLnRvdGFsKTtcbiAgICAgICAgICAgIGlmKHRleHQpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5sYWJlbCh0ZXh0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICRiYXIub25lKHRyYW5zaXRpb25FbmQgKyBldmVudE5hbWVzcGFjZSwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIHNldHRpbmdzLm9uRXJyb3IuY2FsbChlbGVtZW50LCBtb2R1bGUudmFsdWUsIG1vZHVsZS50b3RhbCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHRyYW5zaXRpb25FdmVudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB0cmFuc2l0aW9uRW5kID0gbW9kdWxlLmdldC50cmFuc2l0aW9uRW5kKCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB0b3RhbDogZnVuY3Rpb24odG90YWxWYWx1ZSkge1xuICAgICAgICAgICAgbW9kdWxlLnRvdGFsID0gdG90YWxWYWx1ZTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgICAgICAgbW9kdWxlLnZhbHVlID0gdmFsdWU7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBwcm9ncmVzczogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgICAgIGlmKCFtb2R1bGUuaGFzLnByb2dyZXNzUG9sbCgpKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRmlyc3QgdXBkYXRlIGluIHByb2dyZXNzIHVwZGF0ZSBpbnRlcnZhbCwgaW1tZWRpYXRlbHkgdXBkYXRpbmcnLCB2YWx1ZSk7XG4gICAgICAgICAgICAgIG1vZHVsZS51cGRhdGUucHJvZ3Jlc3ModmFsdWUpO1xuICAgICAgICAgICAgICBtb2R1bGUuY3JlYXRlLnByb2dyZXNzUG9sbCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVXBkYXRlZCB3aXRoaW4gaW50ZXJ2YWwsIHNldHRpbmcgbmV4dCB1cGRhdGUgdG8gdXNlIG5ldyB2YWx1ZScsIHZhbHVlKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5uZXh0VmFsdWUodmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgbmV4dFZhbHVlOiBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgICAgICAgbW9kdWxlLm5leHRWYWx1ZSA9IHZhbHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICB1cGRhdGU6IHtcbiAgICAgICAgICB0b05leHRWYWx1ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgbmV4dFZhbHVlID0gbW9kdWxlLm5leHRWYWx1ZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYobmV4dFZhbHVlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVXBkYXRlIGludGVydmFsIGNvbXBsZXRlIHVzaW5nIGxhc3QgdXBkYXRlZCB2YWx1ZScsIG5leHRWYWx1ZSk7XG4gICAgICAgICAgICAgIG1vZHVsZS51cGRhdGUucHJvZ3Jlc3MobmV4dFZhbHVlKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5uZXh0VmFsdWUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHByb2dyZXNzOiBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHBlcmNlbnRDb21wbGV0ZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgdmFsdWUgPSBtb2R1bGUuZ2V0Lm51bWVyaWNWYWx1ZSh2YWx1ZSk7XG4gICAgICAgICAgICBpZih2YWx1ZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm5vbk51bWVyaWMsIHZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHZhbHVlID0gbW9kdWxlLmdldC5ub3JtYWxpemVkVmFsdWUodmFsdWUpO1xuICAgICAgICAgICAgaWYoIG1vZHVsZS5oYXMudG90YWwoKSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC52YWx1ZSh2YWx1ZSk7XG4gICAgICAgICAgICAgIHBlcmNlbnRDb21wbGV0ZSA9ICh2YWx1ZSAvIG1vZHVsZS50b3RhbCkgKiAxMDA7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ2FsY3VsYXRpbmcgcGVyY2VudCBjb21wbGV0ZSBmcm9tIHRvdGFsJywgcGVyY2VudENvbXBsZXRlKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5wZXJjZW50KCBwZXJjZW50Q29tcGxldGUgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBwZXJjZW50Q29tcGxldGUgPSB2YWx1ZTtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTZXR0aW5nIHZhbHVlIHRvIGV4YWN0IHBlcmNlbnRhZ2UgdmFsdWUnLCBwZXJjZW50Q29tcGxldGUpO1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0LnBlcmNlbnQoIHBlcmNlbnRDb21wbGV0ZSApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZXR0aW5nOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ2hhbmdpbmcgc2V0dGluZycsIG5hbWUsIHZhbHVlKTtcbiAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgc2V0dGluZ3MsIG5hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGlmKCQuaXNQbGFpbk9iamVjdChzZXR0aW5nc1tuYW1lXSkpIHtcbiAgICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgc2V0dGluZ3NbbmFtZV0sIHZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBzZXR0aW5nc1tuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBzZXR0aW5nc1tuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludGVybmFsOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBtb2R1bGUsIG5hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIG1vZHVsZVtuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGVbbmFtZV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBkZWJ1ZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy5kZWJ1Zykge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZyA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB2ZXJib3NlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50ICYmIHNldHRpbmdzLnZlcmJvc2UgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGVycm9yOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50KSB7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuZXJyb3IsIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBwZXJmb3JtYW5jZToge1xuICAgICAgICAgIGxvZzogZnVuY3Rpb24obWVzc2FnZSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lLFxuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lLFxuICAgICAgICAgICAgICBwcmV2aW91c1RpbWVcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lICAgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lICA9IHRpbWUgfHwgY3VycmVudFRpbWU7XG4gICAgICAgICAgICAgIGV4ZWN1dGlvblRpbWUgPSBjdXJyZW50VGltZSAtIHByZXZpb3VzVGltZTtcbiAgICAgICAgICAgICAgdGltZSAgICAgICAgICA9IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBwZXJmb3JtYW5jZS5wdXNoKHtcbiAgICAgICAgICAgICAgICAnTmFtZScgICAgICAgICAgIDogbWVzc2FnZVswXSxcbiAgICAgICAgICAgICAgICAnQXJndW1lbnRzJyAgICAgIDogW10uc2xpY2UuY2FsbChtZXNzYWdlLCAxKSB8fCAnJyxcbiAgICAgICAgICAgICAgICAnRWxlbWVudCcgICAgICAgIDogZWxlbWVudCxcbiAgICAgICAgICAgICAgICAnRXhlY3V0aW9uIFRpbWUnIDogZXhlY3V0aW9uVGltZVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyID0gc2V0VGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UuZGlzcGxheSwgNTAwKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc3BsYXk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHRpdGxlID0gc2V0dGluZ3MubmFtZSArICc6JyxcbiAgICAgICAgICAgICAgdG90YWxUaW1lID0gMFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgdGltZSA9IGZhbHNlO1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lcik7XG4gICAgICAgICAgICAkLmVhY2gocGVyZm9ybWFuY2UsIGZ1bmN0aW9uKGluZGV4LCBkYXRhKSB7XG4gICAgICAgICAgICAgIHRvdGFsVGltZSArPSBkYXRhWydFeGVjdXRpb24gVGltZSddO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB0aXRsZSArPSAnICcgKyB0b3RhbFRpbWUgKyAnbXMnO1xuICAgICAgICAgICAgaWYobW9kdWxlU2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgdGl0bGUgKz0gJyBcXCcnICsgbW9kdWxlU2VsZWN0b3IgKyAnXFwnJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCAoY29uc29sZS5ncm91cCAhPT0gdW5kZWZpbmVkIHx8IGNvbnNvbGUudGFibGUgIT09IHVuZGVmaW5lZCkgJiYgcGVyZm9ybWFuY2UubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKHRpdGxlKTtcbiAgICAgICAgICAgICAgaWYoY29uc29sZS50YWJsZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUudGFibGUocGVyZm9ybWFuY2UpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGFbJ05hbWUnXSArICc6ICcgKyBkYXRhWydFeGVjdXRpb24gVGltZSddKydtcycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHBlcmZvcm1hbmNlID0gW107XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnZva2U6IGZ1bmN0aW9uKHF1ZXJ5LCBwYXNzZWRBcmd1bWVudHMsIGNvbnRleHQpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIG9iamVjdCA9IGluc3RhbmNlLFxuICAgICAgICAgICAgbWF4RGVwdGgsXG4gICAgICAgICAgICBmb3VuZCxcbiAgICAgICAgICAgIHJlc3BvbnNlXG4gICAgICAgICAgO1xuICAgICAgICAgIHBhc3NlZEFyZ3VtZW50cyA9IHBhc3NlZEFyZ3VtZW50cyB8fCBxdWVyeUFyZ3VtZW50cztcbiAgICAgICAgICBjb250ZXh0ICAgICAgICAgPSBlbGVtZW50ICAgICAgICAgfHwgY29udGV4dDtcbiAgICAgICAgICBpZih0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycgJiYgb2JqZWN0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5ICAgID0gcXVlcnkuc3BsaXQoL1tcXC4gXS8pO1xuICAgICAgICAgICAgbWF4RGVwdGggPSBxdWVyeS5sZW5ndGggLSAxO1xuICAgICAgICAgICAgJC5lYWNoKHF1ZXJ5LCBmdW5jdGlvbihkZXB0aCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgdmFyIGNhbWVsQ2FzZVZhbHVlID0gKGRlcHRoICE9IG1heERlcHRoKVxuICAgICAgICAgICAgICAgID8gdmFsdWUgKyBxdWVyeVtkZXB0aCArIDFdLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcXVlcnlbZGVwdGggKyAxXS5zbGljZSgxKVxuICAgICAgICAgICAgICAgIDogcXVlcnlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbY2FtZWxDYXNlVmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoICQuaXNQbGFpbk9iamVjdCggb2JqZWN0W3ZhbHVlXSApICYmIChkZXB0aCAhPSBtYXhEZXB0aCkgKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBvYmplY3RbdmFsdWVdICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3IubWV0aG9kLCBxdWVyeSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCAkLmlzRnVuY3Rpb24oIGZvdW5kICkgKSB7XG4gICAgICAgICAgICByZXNwb25zZSA9IGZvdW5kLmFwcGx5KGNvbnRleHQsIHBhc3NlZEFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYoZm91bmQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZDtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoJC5pc0FycmF5KHJldHVybmVkVmFsdWUpKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlLnB1c2gocmVzcG9uc2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IFtyZXR1cm5lZFZhbHVlLCByZXNwb25zZV07XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYocmVzcG9uc2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IHJlc3BvbnNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gZm91bmQ7XG4gICAgICAgIH1cbiAgICAgIH07XG5cbiAgICAgIGlmKG1ldGhvZEludm9rZWQpIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICAgIH1cbiAgICAgICAgbW9kdWxlLmludm9rZShxdWVyeSk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIGluc3RhbmNlLmludm9rZSgnZGVzdHJveScpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICB9XG4gICAgfSlcbiAgO1xuXG4gIHJldHVybiAocmV0dXJuZWRWYWx1ZSAhPT0gdW5kZWZpbmVkKVxuICAgID8gcmV0dXJuZWRWYWx1ZVxuICAgIDogdGhpc1xuICA7XG59O1xuXG4kLmZuLnByb2dyZXNzLnNldHRpbmdzID0ge1xuXG4gIG5hbWUgICAgICAgICA6ICdQcm9ncmVzcycsXG4gIG5hbWVzcGFjZSAgICA6ICdwcm9ncmVzcycsXG5cbiAgc2lsZW50ICAgICAgIDogZmFsc2UsXG4gIGRlYnVnICAgICAgICA6IGZhbHNlLFxuICB2ZXJib3NlICAgICAgOiBmYWxzZSxcbiAgcGVyZm9ybWFuY2UgIDogdHJ1ZSxcblxuICByYW5kb20gICAgICAgOiB7XG4gICAgbWluIDogMixcbiAgICBtYXggOiA1XG4gIH0sXG5cbiAgZHVyYXRpb24gICAgICAgOiAzMDAsXG5cbiAgdXBkYXRlSW50ZXJ2YWwgOiAnYXV0bycsXG5cbiAgYXV0b1N1Y2Nlc3MgICAgOiB0cnVlLFxuICBzaG93QWN0aXZpdHkgICA6IHRydWUsXG4gIGxpbWl0VmFsdWVzICAgIDogdHJ1ZSxcblxuICBsYWJlbCAgICAgICAgICA6ICdwZXJjZW50JyxcbiAgcHJlY2lzaW9uICAgICAgOiAwLFxuICBmcmFtZXJhdGUgICAgICA6ICgxMDAwIC8gMzApLCAvLy8gMzAgZnBzXG5cbiAgcGVyY2VudCAgICAgICAgOiBmYWxzZSxcbiAgdG90YWwgICAgICAgICAgOiBmYWxzZSxcbiAgdmFsdWUgICAgICAgICAgOiBmYWxzZSxcblxuICBvbkxhYmVsVXBkYXRlIDogZnVuY3Rpb24oc3RhdGUsIHRleHQsIHZhbHVlLCB0b3RhbCl7XG4gICAgcmV0dXJuIHRleHQ7XG4gIH0sXG4gIG9uQ2hhbmdlICAgICAgOiBmdW5jdGlvbihwZXJjZW50LCB2YWx1ZSwgdG90YWwpe30sXG4gIG9uU3VjY2VzcyAgICAgOiBmdW5jdGlvbih0b3RhbCl7fSxcbiAgb25BY3RpdmUgICAgICA6IGZ1bmN0aW9uKHZhbHVlLCB0b3RhbCl7fSxcbiAgb25FcnJvciAgICAgICA6IGZ1bmN0aW9uKHZhbHVlLCB0b3RhbCl7fSxcbiAgb25XYXJuaW5nICAgICA6IGZ1bmN0aW9uKHZhbHVlLCB0b3RhbCl7fSxcblxuICBlcnJvciAgICA6IHtcbiAgICBtZXRob2QgICAgIDogJ1RoZSBtZXRob2QgeW91IGNhbGxlZCBpcyBub3QgZGVmaW5lZC4nLFxuICAgIG5vbk51bWVyaWMgOiAnUHJvZ3Jlc3MgdmFsdWUgaXMgbm9uIG51bWVyaWMnLFxuICAgIHRvb0hpZ2ggICAgOiAnVmFsdWUgc3BlY2lmaWVkIGlzIGFib3ZlIDEwMCUnLFxuICAgIHRvb0xvdyAgICAgOiAnVmFsdWUgc3BlY2lmaWVkIGlzIGJlbG93IDAlJ1xuICB9LFxuXG4gIHJlZ0V4cDoge1xuICAgIHZhcmlhYmxlOiAvXFx7XFwkKltBLXowLTldK1xcfS9nXG4gIH0sXG5cbiAgbWV0YWRhdGE6IHtcbiAgICBwZXJjZW50IDogJ3BlcmNlbnQnLFxuICAgIHRvdGFsICAgOiAndG90YWwnLFxuICAgIHZhbHVlICAgOiAndmFsdWUnXG4gIH0sXG5cbiAgc2VsZWN0b3IgOiB7XG4gICAgYmFyICAgICAgOiAnPiAuYmFyJyxcbiAgICBsYWJlbCAgICA6ICc+IC5sYWJlbCcsXG4gICAgcHJvZ3Jlc3MgOiAnLmJhciA+IC5wcm9ncmVzcydcbiAgfSxcblxuICB0ZXh0IDoge1xuICAgIGFjdGl2ZSAgOiBmYWxzZSxcbiAgICBlcnJvciAgIDogZmFsc2UsXG4gICAgc3VjY2VzcyA6IGZhbHNlLFxuICAgIHdhcm5pbmcgOiBmYWxzZSxcbiAgICBwZXJjZW50IDogJ3twZXJjZW50fSUnLFxuICAgIHJhdGlvICAgOiAne3ZhbHVlfSBvZiB7dG90YWx9J1xuICB9LFxuXG4gIGNsYXNzTmFtZSA6IHtcbiAgICBhY3RpdmUgIDogJ2FjdGl2ZScsXG4gICAgZXJyb3IgICA6ICdlcnJvcicsXG4gICAgc3VjY2VzcyA6ICdzdWNjZXNzJyxcbiAgICB3YXJuaW5nIDogJ3dhcm5pbmcnXG4gIH1cblxufTtcblxuXG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7XG4iXX0=