/*!
 * # Semantic UI - Rating
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.rating = function (parameters) {
    var $allModules = $(this),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue;
    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.rating.settings, parameters) : $.extend({}, $.fn.rating.settings),
          namespace = settings.namespace,
          className = settings.className,
          metadata = settings.metadata,
          selector = settings.selector,
          error = settings.error,
          eventNamespace = '.' + namespace,
          moduleNamespace = 'module-' + namespace,
          element = this,
          instance = $(this).data(moduleNamespace),
          $module = $(this),
          $icon = $module.find(selector.icon),
          initialLoad,
          module;

      module = {

        initialize: function () {
          module.verbose('Initializing rating module', settings);

          if ($icon.length === 0) {
            module.setup.layout();
          }

          if (settings.interactive) {
            module.enable();
          } else {
            module.disable();
          }
          module.set.initialLoad();
          module.set.rating(module.get.initialRating());
          module.remove.initialLoad();
          module.instantiate();
        },

        instantiate: function () {
          module.verbose('Instantiating module', settings);
          instance = module;
          $module.data(moduleNamespace, module);
        },

        destroy: function () {
          module.verbose('Destroying previous instance', instance);
          module.remove.events();
          $module.removeData(moduleNamespace);
        },

        refresh: function () {
          $icon = $module.find(selector.icon);
        },

        setup: {
          layout: function () {
            var maxRating = module.get.maxRating(),
                html = $.fn.rating.settings.templates.icon(maxRating);
            module.debug('Generating icon html dynamically');
            $module.html(html);
            module.refresh();
          }
        },

        event: {
          mouseenter: function () {
            var $activeIcon = $(this);
            $activeIcon.nextAll().removeClass(className.selected);
            $module.addClass(className.selected);
            $activeIcon.addClass(className.selected).prevAll().addClass(className.selected);
          },
          mouseleave: function () {
            $module.removeClass(className.selected);
            $icon.removeClass(className.selected);
          },
          click: function () {
            var $activeIcon = $(this),
                currentRating = module.get.rating(),
                rating = $icon.index($activeIcon) + 1,
                canClear = settings.clearable == 'auto' ? $icon.length === 1 : settings.clearable;
            if (canClear && currentRating == rating) {
              module.clearRating();
            } else {
              module.set.rating(rating);
            }
          }
        },

        clearRating: function () {
          module.debug('Clearing current rating');
          module.set.rating(0);
        },

        bind: {
          events: function () {
            module.verbose('Binding events');
            $module.on('mouseenter' + eventNamespace, selector.icon, module.event.mouseenter).on('mouseleave' + eventNamespace, selector.icon, module.event.mouseleave).on('click' + eventNamespace, selector.icon, module.event.click);
          }
        },

        remove: {
          events: function () {
            module.verbose('Removing events');
            $module.off(eventNamespace);
          },
          initialLoad: function () {
            initialLoad = false;
          }
        },

        enable: function () {
          module.debug('Setting rating to interactive mode');
          module.bind.events();
          $module.removeClass(className.disabled);
        },

        disable: function () {
          module.debug('Setting rating to read-only mode');
          module.remove.events();
          $module.addClass(className.disabled);
        },

        is: {
          initialLoad: function () {
            return initialLoad;
          }
        },

        get: {
          initialRating: function () {
            if ($module.data(metadata.rating) !== undefined) {
              $module.removeData(metadata.rating);
              return $module.data(metadata.rating);
            }
            return settings.initialRating;
          },
          maxRating: function () {
            if ($module.data(metadata.maxRating) !== undefined) {
              $module.removeData(metadata.maxRating);
              return $module.data(metadata.maxRating);
            }
            return settings.maxRating;
          },
          rating: function () {
            var currentRating = $icon.filter('.' + className.active).length;
            module.verbose('Current rating retrieved', currentRating);
            return currentRating;
          }
        },

        set: {
          rating: function (rating) {
            var ratingIndex = rating - 1 >= 0 ? rating - 1 : 0,
                $activeIcon = $icon.eq(ratingIndex);
            $module.removeClass(className.selected);
            $icon.removeClass(className.selected).removeClass(className.active);
            if (rating > 0) {
              module.verbose('Setting current rating to', rating);
              $activeIcon.prevAll().addBack().addClass(className.active);
            }
            if (!module.is.initialLoad()) {
              settings.onRate.call(element, rating);
            }
          },
          initialLoad: function () {
            initialLoad = true;
          }
        },

        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ($allModules.length > 1) {
              title += ' ' + '(' + $allModules.length + ')';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };
      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.rating.settings = {

    name: 'Rating',
    namespace: 'rating',

    slent: false,
    debug: false,
    verbose: false,
    performance: true,

    initialRating: 0,
    interactive: true,
    maxRating: 4,
    clearable: 'auto',

    fireOnInit: false,

    onRate: function (rating) {},

    error: {
      method: 'The method you called is not defined',
      noMaximum: 'No maximum rating specified. Cannot generate HTML automatically'
    },

    metadata: {
      rating: 'rating',
      maxRating: 'maxRating'
    },

    className: {
      active: 'active',
      disabled: 'disabled',
      selected: 'selected',
      loading: 'loading'
    },

    selector: {
      icon: '.icon'
    },

    templates: {
      icon: function (maxRating) {
        var icon = 1,
            html = '';
        while (icon <= maxRating) {
          html += '<i class="icon"></i>';
          icon++;
        }
        return html;
      }
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL3JhdGluZy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBVUEsQ0FBQyxDQUFDLFVBQVUsQ0FBVixFQUFhLE1BQWIsRUFBcUIsUUFBckIsRUFBK0IsU0FBL0IsRUFBMEM7O0FBRTVDOztBQUVBLFdBQVUsT0FBTyxNQUFQLElBQWlCLFdBQWpCLElBQWdDLE9BQU8sSUFBUCxJQUFlLElBQWhELEdBQ0wsTUFESyxHQUVKLE9BQU8sSUFBUCxJQUFlLFdBQWYsSUFBOEIsS0FBSyxJQUFMLElBQWEsSUFBNUMsR0FDRSxJQURGLEdBRUUsU0FBUyxhQUFULEdBSk47O0FBT0EsSUFBRSxFQUFGLENBQUssTUFBTCxHQUFjLFVBQVMsVUFBVCxFQUFxQjtBQUNqQyxRQUNFLGNBQWtCLEVBQUUsSUFBRixDQURwQjtBQUFBLFFBRUUsaUJBQWtCLFlBQVksUUFBWixJQUF3QixFQUY1QztBQUFBLFFBSUUsT0FBa0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUpwQjtBQUFBLFFBS0UsY0FBa0IsRUFMcEI7QUFBQSxRQU9FLFFBQWtCLFVBQVUsQ0FBVixDQVBwQjtBQUFBLFFBUUUsZ0JBQW1CLE9BQU8sS0FBUCxJQUFnQixRQVJyQztBQUFBLFFBU0UsaUJBQWtCLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLENBQXpCLENBVHBCO0FBQUEsUUFVRSxhQVZGO0FBWUEsZ0JBQ0csSUFESCxDQUNRLFlBQVc7QUFDZixVQUNFLFdBQW9CLEVBQUUsYUFBRixDQUFnQixVQUFoQixDQUFGLEdBQ2QsRUFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUIsRUFBRSxFQUFGLENBQUssTUFBTCxDQUFZLFFBQS9CLEVBQXlDLFVBQXpDLENBRGMsR0FFZCxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsRUFBRSxFQUFGLENBQUssTUFBTCxDQUFZLFFBQXpCLENBSE47QUFBQSxVQUtFLFlBQWtCLFNBQVMsU0FMN0I7QUFBQSxVQU1FLFlBQWtCLFNBQVMsU0FON0I7QUFBQSxVQU9FLFdBQWtCLFNBQVMsUUFQN0I7QUFBQSxVQVFFLFdBQWtCLFNBQVMsUUFSN0I7QUFBQSxVQVNFLFFBQWtCLFNBQVMsS0FUN0I7QUFBQSxVQVdFLGlCQUFrQixNQUFNLFNBWDFCO0FBQUEsVUFZRSxrQkFBa0IsWUFBWSxTQVpoQztBQUFBLFVBY0UsVUFBa0IsSUFkcEI7QUFBQSxVQWVFLFdBQWtCLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxlQUFiLENBZnBCO0FBQUEsVUFpQkUsVUFBa0IsRUFBRSxJQUFGLENBakJwQjtBQUFBLFVBa0JFLFFBQWtCLFFBQVEsSUFBUixDQUFhLFNBQVMsSUFBdEIsQ0FsQnBCO0FBQUEsVUFvQkUsV0FwQkY7QUFBQSxVQXFCRSxNQXJCRjs7QUF3QkEsZUFBUzs7QUFFUCxvQkFBWSxZQUFXO0FBQ3JCLGlCQUFPLE9BQVAsQ0FBZSw0QkFBZixFQUE2QyxRQUE3Qzs7QUFFQSxjQUFHLE1BQU0sTUFBTixLQUFpQixDQUFwQixFQUF1QjtBQUNyQixtQkFBTyxLQUFQLENBQWEsTUFBYjtBQUNEOztBQUVELGNBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLG1CQUFPLE1BQVA7QUFDRCxXQUZELE1BR0s7QUFDSCxtQkFBTyxPQUFQO0FBQ0Q7QUFDRCxpQkFBTyxHQUFQLENBQVcsV0FBWDtBQUNBLGlCQUFPLEdBQVAsQ0FBVyxNQUFYLENBQW1CLE9BQU8sR0FBUCxDQUFXLGFBQVgsRUFBbkI7QUFDQSxpQkFBTyxNQUFQLENBQWMsV0FBZDtBQUNBLGlCQUFPLFdBQVA7QUFDRCxTQW5CTTs7QUFxQlAscUJBQWEsWUFBVztBQUN0QixpQkFBTyxPQUFQLENBQWUsc0JBQWYsRUFBdUMsUUFBdkM7QUFDQSxxQkFBVyxNQUFYO0FBQ0Esa0JBQ0csSUFESCxDQUNRLGVBRFIsRUFDeUIsTUFEekI7QUFHRCxTQTNCTTs7QUE2QlAsaUJBQVMsWUFBVztBQUNsQixpQkFBTyxPQUFQLENBQWUsOEJBQWYsRUFBK0MsUUFBL0M7QUFDQSxpQkFBTyxNQUFQLENBQWMsTUFBZDtBQUNBLGtCQUNHLFVBREgsQ0FDYyxlQURkO0FBR0QsU0FuQ007O0FBcUNQLGlCQUFTLFlBQVc7QUFDbEIsa0JBQVUsUUFBUSxJQUFSLENBQWEsU0FBUyxJQUF0QixDQUFWO0FBQ0QsU0F2Q007O0FBeUNQLGVBQU87QUFDTCxrQkFBUSxZQUFXO0FBQ2pCLGdCQUNFLFlBQVksT0FBTyxHQUFQLENBQVcsU0FBWCxFQURkO0FBQUEsZ0JBRUUsT0FBWSxFQUFFLEVBQUYsQ0FBSyxNQUFMLENBQVksUUFBWixDQUFxQixTQUFyQixDQUErQixJQUEvQixDQUFvQyxTQUFwQyxDQUZkO0FBSUEsbUJBQU8sS0FBUCxDQUFhLGtDQUFiO0FBQ0Esb0JBQ0csSUFESCxDQUNRLElBRFI7QUFHQSxtQkFBTyxPQUFQO0FBQ0Q7QUFYSSxTQXpDQTs7QUF1RFAsZUFBTztBQUNMLHNCQUFZLFlBQVc7QUFDckIsZ0JBQ0UsY0FBYyxFQUFFLElBQUYsQ0FEaEI7QUFHQSx3QkFDRyxPQURILEdBRUssV0FGTCxDQUVpQixVQUFVLFFBRjNCO0FBSUEsb0JBQ0csUUFESCxDQUNZLFVBQVUsUUFEdEI7QUFHQSx3QkFDRyxRQURILENBQ1ksVUFBVSxRQUR0QixFQUVLLE9BRkwsR0FHSyxRQUhMLENBR2MsVUFBVSxRQUh4QjtBQUtELFdBakJJO0FBa0JMLHNCQUFZLFlBQVc7QUFDckIsb0JBQ0csV0FESCxDQUNlLFVBQVUsUUFEekI7QUFHQSxrQkFDRyxXQURILENBQ2UsVUFBVSxRQUR6QjtBQUdELFdBekJJO0FBMEJMLGlCQUFPLFlBQVc7QUFDaEIsZ0JBQ0UsY0FBZ0IsRUFBRSxJQUFGLENBRGxCO0FBQUEsZ0JBRUUsZ0JBQWdCLE9BQU8sR0FBUCxDQUFXLE1BQVgsRUFGbEI7QUFBQSxnQkFHRSxTQUFnQixNQUFNLEtBQU4sQ0FBWSxXQUFaLElBQTJCLENBSDdDO0FBQUEsZ0JBSUUsV0FBaUIsU0FBUyxTQUFULElBQXNCLE1BQXZCLEdBQ1osTUFBTSxNQUFOLEtBQWlCLENBREwsR0FFYixTQUFTLFNBTmQ7QUFRQSxnQkFBRyxZQUFZLGlCQUFpQixNQUFoQyxFQUF3QztBQUN0QyxxQkFBTyxXQUFQO0FBQ0QsYUFGRCxNQUdLO0FBQ0gscUJBQU8sR0FBUCxDQUFXLE1BQVgsQ0FBbUIsTUFBbkI7QUFDRDtBQUNGO0FBekNJLFNBdkRBOztBQW1HUCxxQkFBYSxZQUFXO0FBQ3RCLGlCQUFPLEtBQVAsQ0FBYSx5QkFBYjtBQUNBLGlCQUFPLEdBQVAsQ0FBVyxNQUFYLENBQWtCLENBQWxCO0FBQ0QsU0F0R007O0FBd0dQLGNBQU07QUFDSixrQkFBUSxZQUFXO0FBQ2pCLG1CQUFPLE9BQVAsQ0FBZSxnQkFBZjtBQUNBLG9CQUNHLEVBREgsQ0FDTSxlQUFlLGNBRHJCLEVBQ3FDLFNBQVMsSUFEOUMsRUFDb0QsT0FBTyxLQUFQLENBQWEsVUFEakUsRUFFRyxFQUZILENBRU0sZUFBZSxjQUZyQixFQUVxQyxTQUFTLElBRjlDLEVBRW9ELE9BQU8sS0FBUCxDQUFhLFVBRmpFLEVBR0csRUFISCxDQUdNLFVBQWUsY0FIckIsRUFHcUMsU0FBUyxJQUg5QyxFQUdvRCxPQUFPLEtBQVAsQ0FBYSxLQUhqRTtBQUtEO0FBUkcsU0F4R0M7O0FBbUhQLGdCQUFRO0FBQ04sa0JBQVEsWUFBVztBQUNqQixtQkFBTyxPQUFQLENBQWUsaUJBQWY7QUFDQSxvQkFDRyxHQURILENBQ08sY0FEUDtBQUdELFdBTks7QUFPTix1QkFBYSxZQUFXO0FBQ3RCLDBCQUFjLEtBQWQ7QUFDRDtBQVRLLFNBbkhEOztBQStIUCxnQkFBUSxZQUFXO0FBQ2pCLGlCQUFPLEtBQVAsQ0FBYSxvQ0FBYjtBQUNBLGlCQUFPLElBQVAsQ0FBWSxNQUFaO0FBQ0Esa0JBQ0csV0FESCxDQUNlLFVBQVUsUUFEekI7QUFHRCxTQXJJTTs7QUF1SVAsaUJBQVMsWUFBVztBQUNsQixpQkFBTyxLQUFQLENBQWEsa0NBQWI7QUFDQSxpQkFBTyxNQUFQLENBQWMsTUFBZDtBQUNBLGtCQUNHLFFBREgsQ0FDWSxVQUFVLFFBRHRCO0FBR0QsU0E3SU07O0FBK0lQLFlBQUk7QUFDRix1QkFBYSxZQUFXO0FBQ3RCLG1CQUFPLFdBQVA7QUFDRDtBQUhDLFNBL0lHOztBQXFKUCxhQUFLO0FBQ0gseUJBQWUsWUFBVztBQUN4QixnQkFBRyxRQUFRLElBQVIsQ0FBYSxTQUFTLE1BQXRCLE1BQWtDLFNBQXJDLEVBQWdEO0FBQzlDLHNCQUFRLFVBQVIsQ0FBbUIsU0FBUyxNQUE1QjtBQUNBLHFCQUFPLFFBQVEsSUFBUixDQUFhLFNBQVMsTUFBdEIsQ0FBUDtBQUNEO0FBQ0QsbUJBQU8sU0FBUyxhQUFoQjtBQUNELFdBUEU7QUFRSCxxQkFBVyxZQUFXO0FBQ3BCLGdCQUFHLFFBQVEsSUFBUixDQUFhLFNBQVMsU0FBdEIsTUFBcUMsU0FBeEMsRUFBbUQ7QUFDakQsc0JBQVEsVUFBUixDQUFtQixTQUFTLFNBQTVCO0FBQ0EscUJBQU8sUUFBUSxJQUFSLENBQWEsU0FBUyxTQUF0QixDQUFQO0FBQ0Q7QUFDRCxtQkFBTyxTQUFTLFNBQWhCO0FBQ0QsV0FkRTtBQWVILGtCQUFRLFlBQVc7QUFDakIsZ0JBQ0UsZ0JBQWdCLE1BQU0sTUFBTixDQUFhLE1BQU0sVUFBVSxNQUE3QixFQUFxQyxNQUR2RDtBQUdBLG1CQUFPLE9BQVAsQ0FBZSwwQkFBZixFQUEyQyxhQUEzQztBQUNBLG1CQUFPLGFBQVA7QUFDRDtBQXJCRSxTQXJKRTs7QUE2S1AsYUFBSztBQUNILGtCQUFRLFVBQVMsTUFBVCxFQUFpQjtBQUN2QixnQkFDRSxjQUFlLFNBQVMsQ0FBVCxJQUFjLENBQWYsR0FDVCxTQUFTLENBREEsR0FFVixDQUhOO0FBQUEsZ0JBSUUsY0FBYyxNQUFNLEVBQU4sQ0FBUyxXQUFULENBSmhCO0FBTUEsb0JBQ0csV0FESCxDQUNlLFVBQVUsUUFEekI7QUFHQSxrQkFDRyxXQURILENBQ2UsVUFBVSxRQUR6QixFQUVHLFdBRkgsQ0FFZSxVQUFVLE1BRnpCO0FBSUEsZ0JBQUcsU0FBUyxDQUFaLEVBQWU7QUFDYixxQkFBTyxPQUFQLENBQWUsMkJBQWYsRUFBNEMsTUFBNUM7QUFDQSwwQkFDRyxPQURILEdBRUcsT0FGSCxHQUdLLFFBSEwsQ0FHYyxVQUFVLE1BSHhCO0FBS0Q7QUFDRCxnQkFBRyxDQUFDLE9BQU8sRUFBUCxDQUFVLFdBQVYsRUFBSixFQUE2QjtBQUMzQix1QkFBUyxNQUFULENBQWdCLElBQWhCLENBQXFCLE9BQXJCLEVBQThCLE1BQTlCO0FBQ0Q7QUFDRixXQTFCRTtBQTJCSCx1QkFBYSxZQUFXO0FBQ3RCLDBCQUFjLElBQWQ7QUFDRDtBQTdCRSxTQTdLRTs7QUE2TVAsaUJBQVMsVUFBUyxJQUFULEVBQWUsS0FBZixFQUFzQjtBQUM3QixpQkFBTyxLQUFQLENBQWEsa0JBQWIsRUFBaUMsSUFBakMsRUFBdUMsS0FBdkM7QUFDQSxjQUFJLEVBQUUsYUFBRixDQUFnQixJQUFoQixDQUFKLEVBQTRCO0FBQzFCLGNBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxRQUFmLEVBQXlCLElBQXpCO0FBQ0QsV0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLGdCQUFHLEVBQUUsYUFBRixDQUFnQixTQUFTLElBQVQsQ0FBaEIsQ0FBSCxFQUFvQztBQUNsQyxnQkFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLFNBQVMsSUFBVCxDQUFmLEVBQStCLEtBQS9CO0FBQ0QsYUFGRCxNQUdLO0FBQ0gsdUJBQVMsSUFBVCxJQUFpQixLQUFqQjtBQUNEO0FBQ0YsV0FQSSxNQVFBO0FBQ0gsbUJBQU8sU0FBUyxJQUFULENBQVA7QUFDRDtBQUNGLFNBN05NO0FBOE5QLGtCQUFVLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDOUIsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsTUFBZixFQUF1QixJQUF2QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixtQkFBTyxJQUFQLElBQWUsS0FBZjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLE9BQU8sSUFBUCxDQUFQO0FBQ0Q7QUFDRixTQXhPTTtBQXlPUCxlQUFPLFlBQVc7QUFDaEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLEtBQWhDLEVBQXVDO0FBQ3JDLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2QixxQkFBTyxXQUFQLENBQW1CLEdBQW5CLENBQXVCLFNBQXZCO0FBQ0QsYUFGRCxNQUdLO0FBQ0gscUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFmO0FBQ0EscUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGO0FBQ0YsU0FuUE07QUFvUFAsaUJBQVMsWUFBVztBQUNsQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsT0FBN0IsSUFBd0MsU0FBUyxLQUFwRCxFQUEyRDtBQUN6RCxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLE9BQVAsR0FBaUIsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWpCO0FBQ0EscUJBQU8sT0FBUCxDQUFlLEtBQWYsQ0FBcUIsT0FBckIsRUFBOEIsU0FBOUI7QUFDRDtBQUNGO0FBQ0YsU0E5UE07QUErUFAsZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQWIsRUFBcUI7QUFDbkIsbUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLEtBQXJDLEVBQTRDLE9BQTVDLEVBQXFELFNBQVMsSUFBVCxHQUFnQixHQUFyRSxDQUFmO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGLFNBcFFNO0FBcVFQLHFCQUFhO0FBQ1gsZUFBSyxVQUFTLE9BQVQsRUFBa0I7QUFDckIsZ0JBQ0UsV0FERixFQUVFLGFBRkYsRUFHRSxZQUhGO0FBS0EsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLDRCQUFnQixJQUFJLElBQUosR0FBVyxPQUFYLEVBQWhCO0FBQ0EsNkJBQWdCLFFBQVEsV0FBeEI7QUFDQSw4QkFBZ0IsY0FBYyxZQUE5QjtBQUNBLHFCQUFnQixXQUFoQjtBQUNBLDBCQUFZLElBQVosQ0FBaUI7QUFDZix3QkFBbUIsUUFBUSxDQUFSLENBREo7QUFFZiw2QkFBbUIsR0FBRyxLQUFILENBQVMsSUFBVCxDQUFjLE9BQWQsRUFBdUIsQ0FBdkIsS0FBNkIsRUFGakM7QUFHZiwyQkFBbUIsT0FISjtBQUlmLGtDQUFtQjtBQUpKLGVBQWpCO0FBTUQ7QUFDRCx5QkFBYSxPQUFPLFdBQVAsQ0FBbUIsS0FBaEM7QUFDQSxtQkFBTyxXQUFQLENBQW1CLEtBQW5CLEdBQTJCLFdBQVcsT0FBTyxXQUFQLENBQW1CLE9BQTlCLEVBQXVDLEdBQXZDLENBQTNCO0FBQ0QsV0FyQlU7QUFzQlgsbUJBQVMsWUFBVztBQUNsQixnQkFDRSxRQUFRLFNBQVMsSUFBVCxHQUFnQixHQUQxQjtBQUFBLGdCQUVFLFlBQVksQ0FGZDtBQUlBLG1CQUFPLEtBQVA7QUFDQSx5QkFBYSxPQUFPLFdBQVAsQ0FBbUIsS0FBaEM7QUFDQSxjQUFFLElBQUYsQ0FBTyxXQUFQLEVBQW9CLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjtBQUN4QywyQkFBYSxLQUFLLGdCQUFMLENBQWI7QUFDRCxhQUZEO0FBR0EscUJBQVMsTUFBTSxTQUFOLEdBQWtCLElBQTNCO0FBQ0EsZ0JBQUcsY0FBSCxFQUFtQjtBQUNqQix1QkFBUyxRQUFRLGNBQVIsR0FBeUIsSUFBbEM7QUFDRDtBQUNELGdCQUFHLFlBQVksTUFBWixHQUFxQixDQUF4QixFQUEyQjtBQUN6Qix1QkFBUyxNQUFNLEdBQU4sR0FBWSxZQUFZLE1BQXhCLEdBQWlDLEdBQTFDO0FBQ0Q7QUFDRCxnQkFBSSxDQUFDLFFBQVEsS0FBUixLQUFrQixTQUFsQixJQUErQixRQUFRLEtBQVIsS0FBa0IsU0FBbEQsS0FBZ0UsWUFBWSxNQUFaLEdBQXFCLENBQXpGLEVBQTRGO0FBQzFGLHNCQUFRLGNBQVIsQ0FBdUIsS0FBdkI7QUFDQSxrQkFBRyxRQUFRLEtBQVgsRUFBa0I7QUFDaEIsd0JBQVEsS0FBUixDQUFjLFdBQWQ7QUFDRCxlQUZELE1BR0s7QUFDSCxrQkFBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMEJBQVEsR0FBUixDQUFZLEtBQUssTUFBTCxJQUFlLElBQWYsR0FBc0IsS0FBSyxnQkFBTCxDQUF0QixHQUE2QyxJQUF6RDtBQUNELGlCQUZEO0FBR0Q7QUFDRCxzQkFBUSxRQUFSO0FBQ0Q7QUFDRCwwQkFBYyxFQUFkO0FBQ0Q7QUFwRFUsU0FyUU47QUEyVFAsZ0JBQVEsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDLE9BQWpDLEVBQTBDO0FBQ2hELGNBQ0UsU0FBUyxRQURYO0FBQUEsY0FFRSxRQUZGO0FBQUEsY0FHRSxLQUhGO0FBQUEsY0FJRSxRQUpGO0FBTUEsNEJBQWtCLG1CQUFtQixjQUFyQztBQUNBLG9CQUFrQixXQUFtQixPQUFyQztBQUNBLGNBQUcsT0FBTyxLQUFQLElBQWdCLFFBQWhCLElBQTRCLFdBQVcsU0FBMUMsRUFBcUQ7QUFDbkQsb0JBQVcsTUFBTSxLQUFOLENBQVksT0FBWixDQUFYO0FBQ0EsdUJBQVcsTUFBTSxNQUFOLEdBQWUsQ0FBMUI7QUFDQSxjQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ25DLGtCQUFJLGlCQUFrQixTQUFTLFFBQVYsR0FDakIsUUFBUSxNQUFNLFFBQVEsQ0FBZCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixFQUEyQixXQUEzQixFQUFSLEdBQW1ELE1BQU0sUUFBUSxDQUFkLEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBRGxDLEdBRWpCLEtBRko7QUFJQSxrQkFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxjQUFQLENBQWpCLEtBQThDLFNBQVMsUUFBM0QsRUFBdUU7QUFDckUseUJBQVMsT0FBTyxjQUFQLENBQVQ7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLGNBQVAsTUFBMkIsU0FBL0IsRUFBMkM7QUFDOUMsd0JBQVEsT0FBTyxjQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBLElBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sS0FBUCxDQUFqQixLQUFxQyxTQUFTLFFBQWxELEVBQThEO0FBQ2pFLHlCQUFTLE9BQU8sS0FBUCxDQUFUO0FBQ0QsZUFGSSxNQUdBLElBQUksT0FBTyxLQUFQLE1BQWtCLFNBQXRCLEVBQWtDO0FBQ3JDLHdCQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQTtBQUNILHVCQUFPLEtBQVA7QUFDRDtBQUNGLGFBdEJEO0FBdUJEO0FBQ0QsY0FBSyxFQUFFLFVBQUYsQ0FBYyxLQUFkLENBQUwsRUFBNkI7QUFDM0IsdUJBQVcsTUFBTSxLQUFOLENBQVksT0FBWixFQUFxQixlQUFyQixDQUFYO0FBQ0QsV0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLHVCQUFXLEtBQVg7QUFDRDtBQUNELGNBQUcsRUFBRSxPQUFGLENBQVUsYUFBVixDQUFILEVBQTZCO0FBQzNCLDBCQUFjLElBQWQsQ0FBbUIsUUFBbkI7QUFDRCxXQUZELE1BR0ssSUFBRyxrQkFBa0IsU0FBckIsRUFBZ0M7QUFDbkMsNEJBQWdCLENBQUMsYUFBRCxFQUFnQixRQUFoQixDQUFoQjtBQUNELFdBRkksTUFHQSxJQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDOUIsNEJBQWdCLFFBQWhCO0FBQ0Q7QUFDRCxpQkFBTyxLQUFQO0FBQ0Q7QUEvV00sT0FBVDtBQWlYQSxVQUFHLGFBQUgsRUFBa0I7QUFDaEIsWUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLGlCQUFPLFVBQVA7QUFDRDtBQUNELGVBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDRCxPQUxELE1BTUs7QUFDSCxZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsbUJBQVMsTUFBVCxDQUFnQixTQUFoQjtBQUNEO0FBQ0QsZUFBTyxVQUFQO0FBQ0Q7QUFDRixLQXZaSDs7QUEwWkEsV0FBUSxrQkFBa0IsU0FBbkIsR0FDSCxhQURHLEdBRUgsSUFGSjtBQUlELEdBM2FEOztBQTZhQSxJQUFFLEVBQUYsQ0FBSyxNQUFMLENBQVksUUFBWixHQUF1Qjs7QUFFckIsVUFBZ0IsUUFGSztBQUdyQixlQUFnQixRQUhLOztBQUtyQixXQUFnQixLQUxLO0FBTXJCLFdBQWdCLEtBTks7QUFPckIsYUFBZ0IsS0FQSztBQVFyQixpQkFBZ0IsSUFSSzs7QUFVckIsbUJBQWdCLENBVks7QUFXckIsaUJBQWdCLElBWEs7QUFZckIsZUFBZ0IsQ0FaSztBQWFyQixlQUFnQixNQWJLOztBQWVyQixnQkFBZ0IsS0FmSzs7QUFpQnJCLFlBQWdCLFVBQVMsTUFBVCxFQUFnQixDQUFFLENBakJiOztBQW1CckIsV0FBZ0I7QUFDZCxjQUFZLHNDQURFO0FBRWQsaUJBQVk7QUFGRSxLQW5CSzs7QUF5QnJCLGNBQVU7QUFDUixjQUFZLFFBREo7QUFFUixpQkFBWTtBQUZKLEtBekJXOztBQThCckIsZUFBWTtBQUNWLGNBQVcsUUFERDtBQUVWLGdCQUFXLFVBRkQ7QUFHVixnQkFBVyxVQUhEO0FBSVYsZUFBVztBQUpELEtBOUJTOztBQXFDckIsY0FBWTtBQUNWLFlBQU87QUFERyxLQXJDUzs7QUF5Q3JCLGVBQVc7QUFDVCxZQUFNLFVBQVMsU0FBVCxFQUFvQjtBQUN4QixZQUNFLE9BQU8sQ0FEVDtBQUFBLFlBRUUsT0FBTyxFQUZUO0FBSUEsZUFBTSxRQUFRLFNBQWQsRUFBeUI7QUFDdkIsa0JBQVEsc0JBQVI7QUFDQTtBQUNEO0FBQ0QsZUFBTyxJQUFQO0FBQ0Q7QUFYUTs7QUF6Q1UsR0FBdkI7QUF5REMsQ0FqZkEsRUFpZkcsTUFqZkgsRUFpZlcsTUFqZlgsRUFpZm1CLFFBamZuQiIsImZpbGUiOiJyYXRpbmcuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcbiAqICMgU2VtYW50aWMgVUkgLSBSYXRpbmdcbiAqIGh0dHA6Ly9naXRodWIuY29tL3NlbWFudGljLW9yZy9zZW1hbnRpYy11aS9cbiAqXG4gKlxuICogUmVsZWFzZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlXG4gKiBodHRwOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvTUlUXG4gKlxuICovXG5cbjsoZnVuY3Rpb24gKCQsIHdpbmRvdywgZG9jdW1lbnQsIHVuZGVmaW5lZCkge1xuXG5cInVzZSBzdHJpY3RcIjtcblxud2luZG93ID0gKHR5cGVvZiB3aW5kb3cgIT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93Lk1hdGggPT0gTWF0aClcbiAgPyB3aW5kb3dcbiAgOiAodHlwZW9mIHNlbGYgIT0gJ3VuZGVmaW5lZCcgJiYgc2VsZi5NYXRoID09IE1hdGgpXG4gICAgPyBzZWxmXG4gICAgOiBGdW5jdGlvbigncmV0dXJuIHRoaXMnKSgpXG47XG5cbiQuZm4ucmF0aW5nID0gZnVuY3Rpb24ocGFyYW1ldGVycykge1xuICB2YXJcbiAgICAkYWxsTW9kdWxlcyAgICAgPSAkKHRoaXMpLFxuICAgIG1vZHVsZVNlbGVjdG9yICA9ICRhbGxNb2R1bGVzLnNlbGVjdG9yIHx8ICcnLFxuXG4gICAgdGltZSAgICAgICAgICAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCksXG4gICAgcGVyZm9ybWFuY2UgICAgID0gW10sXG5cbiAgICBxdWVyeSAgICAgICAgICAgPSBhcmd1bWVudHNbMF0sXG4gICAgbWV0aG9kSW52b2tlZCAgID0gKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyksXG4gICAgcXVlcnlBcmd1bWVudHMgID0gW10uc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpLFxuICAgIHJldHVybmVkVmFsdWVcbiAgO1xuICAkYWxsTW9kdWxlc1xuICAgIC5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgdmFyXG4gICAgICAgIHNldHRpbmdzICAgICAgICA9ICggJC5pc1BsYWluT2JqZWN0KHBhcmFtZXRlcnMpIClcbiAgICAgICAgICA/ICQuZXh0ZW5kKHRydWUsIHt9LCAkLmZuLnJhdGluZy5zZXR0aW5ncywgcGFyYW1ldGVycylcbiAgICAgICAgICA6ICQuZXh0ZW5kKHt9LCAkLmZuLnJhdGluZy5zZXR0aW5ncyksXG5cbiAgICAgICAgbmFtZXNwYWNlICAgICAgID0gc2V0dGluZ3MubmFtZXNwYWNlLFxuICAgICAgICBjbGFzc05hbWUgICAgICAgPSBzZXR0aW5ncy5jbGFzc05hbWUsXG4gICAgICAgIG1ldGFkYXRhICAgICAgICA9IHNldHRpbmdzLm1ldGFkYXRhLFxuICAgICAgICBzZWxlY3RvciAgICAgICAgPSBzZXR0aW5ncy5zZWxlY3RvcixcbiAgICAgICAgZXJyb3IgICAgICAgICAgID0gc2V0dGluZ3MuZXJyb3IsXG5cbiAgICAgICAgZXZlbnROYW1lc3BhY2UgID0gJy4nICsgbmFtZXNwYWNlLFxuICAgICAgICBtb2R1bGVOYW1lc3BhY2UgPSAnbW9kdWxlLScgKyBuYW1lc3BhY2UsXG5cbiAgICAgICAgZWxlbWVudCAgICAgICAgID0gdGhpcyxcbiAgICAgICAgaW5zdGFuY2UgICAgICAgID0gJCh0aGlzKS5kYXRhKG1vZHVsZU5hbWVzcGFjZSksXG5cbiAgICAgICAgJG1vZHVsZSAgICAgICAgID0gJCh0aGlzKSxcbiAgICAgICAgJGljb24gICAgICAgICAgID0gJG1vZHVsZS5maW5kKHNlbGVjdG9yLmljb24pLFxuXG4gICAgICAgIGluaXRpYWxMb2FkLFxuICAgICAgICBtb2R1bGVcbiAgICAgIDtcblxuICAgICAgbW9kdWxlID0ge1xuXG4gICAgICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdJbml0aWFsaXppbmcgcmF0aW5nIG1vZHVsZScsIHNldHRpbmdzKTtcblxuICAgICAgICAgIGlmKCRpY29uLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgbW9kdWxlLnNldHVwLmxheW91dCgpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmKHNldHRpbmdzLmludGVyYWN0aXZlKSB7XG4gICAgICAgICAgICBtb2R1bGUuZW5hYmxlKCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgbW9kdWxlLmRpc2FibGUoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgbW9kdWxlLnNldC5pbml0aWFsTG9hZCgpO1xuICAgICAgICAgIG1vZHVsZS5zZXQucmF0aW5nKCBtb2R1bGUuZ2V0LmluaXRpYWxSYXRpbmcoKSApO1xuICAgICAgICAgIG1vZHVsZS5yZW1vdmUuaW5pdGlhbExvYWQoKTtcbiAgICAgICAgICBtb2R1bGUuaW5zdGFudGlhdGUoKTtcbiAgICAgICAgfSxcblxuICAgICAgICBpbnN0YW50aWF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0luc3RhbnRpYXRpbmcgbW9kdWxlJywgc2V0dGluZ3MpO1xuICAgICAgICAgIGluc3RhbmNlID0gbW9kdWxlO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5kYXRhKG1vZHVsZU5hbWVzcGFjZSwgbW9kdWxlKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBkZXN0cm95OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnRGVzdHJveWluZyBwcmV2aW91cyBpbnN0YW5jZScsIGluc3RhbmNlKTtcbiAgICAgICAgICBtb2R1bGUucmVtb3ZlLmV2ZW50cygpO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5yZW1vdmVEYXRhKG1vZHVsZU5hbWVzcGFjZSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVmcmVzaDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgJGljb24gICA9ICRtb2R1bGUuZmluZChzZWxlY3Rvci5pY29uKTtcbiAgICAgICAgfSxcblxuICAgICAgICBzZXR1cDoge1xuICAgICAgICAgIGxheW91dDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgbWF4UmF0aW5nID0gbW9kdWxlLmdldC5tYXhSYXRpbmcoKSxcbiAgICAgICAgICAgICAgaHRtbCAgICAgID0gJC5mbi5yYXRpbmcuc2V0dGluZ3MudGVtcGxhdGVzLmljb24obWF4UmF0aW5nKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdHZW5lcmF0aW5nIGljb24gaHRtbCBkeW5hbWljYWxseScpO1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAuaHRtbChodG1sKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnJlZnJlc2goKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZXZlbnQ6IHtcbiAgICAgICAgICBtb3VzZWVudGVyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAkYWN0aXZlSWNvbiA9ICQodGhpcylcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgICRhY3RpdmVJY29uXG4gICAgICAgICAgICAgIC5uZXh0QWxsKClcbiAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLnNlbGVjdGVkKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLnNlbGVjdGVkKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJGFjdGl2ZUljb25cbiAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5zZWxlY3RlZClcbiAgICAgICAgICAgICAgICAucHJldkFsbCgpXG4gICAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5zZWxlY3RlZClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIG1vdXNlbGVhdmU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLnNlbGVjdGVkKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJGljb25cbiAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5zZWxlY3RlZClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAkYWN0aXZlSWNvbiAgID0gJCh0aGlzKSxcbiAgICAgICAgICAgICAgY3VycmVudFJhdGluZyA9IG1vZHVsZS5nZXQucmF0aW5nKCksXG4gICAgICAgICAgICAgIHJhdGluZyAgICAgICAgPSAkaWNvbi5pbmRleCgkYWN0aXZlSWNvbikgKyAxLFxuICAgICAgICAgICAgICBjYW5DbGVhciAgICAgID0gKHNldHRpbmdzLmNsZWFyYWJsZSA9PSAnYXV0bycpXG4gICAgICAgICAgICAgICA/ICgkaWNvbi5sZW5ndGggPT09IDEpXG4gICAgICAgICAgICAgICA6IHNldHRpbmdzLmNsZWFyYWJsZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoY2FuQ2xlYXIgJiYgY3VycmVudFJhdGluZyA9PSByYXRpbmcpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmNsZWFyUmF0aW5nKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5yYXRpbmcoIHJhdGluZyApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBjbGVhclJhdGluZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdDbGVhcmluZyBjdXJyZW50IHJhdGluZycpO1xuICAgICAgICAgIG1vZHVsZS5zZXQucmF0aW5nKDApO1xuICAgICAgICB9LFxuXG4gICAgICAgIGJpbmQ6IHtcbiAgICAgICAgICBldmVudHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0JpbmRpbmcgZXZlbnRzJyk7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5vbignbW91c2VlbnRlcicgKyBldmVudE5hbWVzcGFjZSwgc2VsZWN0b3IuaWNvbiwgbW9kdWxlLmV2ZW50Lm1vdXNlZW50ZXIpXG4gICAgICAgICAgICAgIC5vbignbW91c2VsZWF2ZScgKyBldmVudE5hbWVzcGFjZSwgc2VsZWN0b3IuaWNvbiwgbW9kdWxlLmV2ZW50Lm1vdXNlbGVhdmUpXG4gICAgICAgICAgICAgIC5vbignY2xpY2snICAgICAgKyBldmVudE5hbWVzcGFjZSwgc2VsZWN0b3IuaWNvbiwgbW9kdWxlLmV2ZW50LmNsaWNrKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICByZW1vdmU6IHtcbiAgICAgICAgICBldmVudHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1JlbW92aW5nIGV2ZW50cycpO1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAub2ZmKGV2ZW50TmFtZXNwYWNlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgaW5pdGlhbExvYWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaW5pdGlhbExvYWQgPSBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZW5hYmxlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NldHRpbmcgcmF0aW5nIHRvIGludGVyYWN0aXZlIG1vZGUnKTtcbiAgICAgICAgICBtb2R1bGUuYmluZC5ldmVudHMoKTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmRpc2FibGVkKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBkaXNhYmxlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NldHRpbmcgcmF0aW5nIHRvIHJlYWQtb25seSBtb2RlJyk7XG4gICAgICAgICAgbW9kdWxlLnJlbW92ZS5ldmVudHMoKTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLmRpc2FibGVkKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBpczoge1xuICAgICAgICAgIGluaXRpYWxMb2FkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBpbml0aWFsTG9hZDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0OiB7XG4gICAgICAgICAgaW5pdGlhbFJhdGluZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZigkbW9kdWxlLmRhdGEobWV0YWRhdGEucmF0aW5nKSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICRtb2R1bGUucmVtb3ZlRGF0YShtZXRhZGF0YS5yYXRpbmcpO1xuICAgICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5kYXRhKG1ldGFkYXRhLnJhdGluZyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gc2V0dGluZ3MuaW5pdGlhbFJhdGluZztcbiAgICAgICAgICB9LFxuICAgICAgICAgIG1heFJhdGluZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZigkbW9kdWxlLmRhdGEobWV0YWRhdGEubWF4UmF0aW5nKSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICRtb2R1bGUucmVtb3ZlRGF0YShtZXRhZGF0YS5tYXhSYXRpbmcpO1xuICAgICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5kYXRhKG1ldGFkYXRhLm1heFJhdGluZyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gc2V0dGluZ3MubWF4UmF0aW5nO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcmF0aW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBjdXJyZW50UmF0aW5nID0gJGljb24uZmlsdGVyKCcuJyArIGNsYXNzTmFtZS5hY3RpdmUpLmxlbmd0aFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0N1cnJlbnQgcmF0aW5nIHJldHJpZXZlZCcsIGN1cnJlbnRSYXRpbmcpO1xuICAgICAgICAgICAgcmV0dXJuIGN1cnJlbnRSYXRpbmc7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldDoge1xuICAgICAgICAgIHJhdGluZzogZnVuY3Rpb24ocmF0aW5nKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgcmF0aW5nSW5kZXggPSAocmF0aW5nIC0gMSA+PSAwKVxuICAgICAgICAgICAgICAgID8gKHJhdGluZyAtIDEpXG4gICAgICAgICAgICAgICAgOiAwLFxuICAgICAgICAgICAgICAkYWN0aXZlSWNvbiA9ICRpY29uLmVxKHJhdGluZ0luZGV4KVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLnNlbGVjdGVkKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJGljb25cbiAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5zZWxlY3RlZClcbiAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5hY3RpdmUpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihyYXRpbmcgPiAwKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIGN1cnJlbnQgcmF0aW5nIHRvJywgcmF0aW5nKTtcbiAgICAgICAgICAgICAgJGFjdGl2ZUljb25cbiAgICAgICAgICAgICAgICAucHJldkFsbCgpXG4gICAgICAgICAgICAgICAgLmFkZEJhY2soKVxuICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5hY3RpdmUpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCFtb2R1bGUuaXMuaW5pdGlhbExvYWQoKSkge1xuICAgICAgICAgICAgICBzZXR0aW5ncy5vblJhdGUuY2FsbChlbGVtZW50LCByYXRpbmcpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgaW5pdGlhbExvYWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaW5pdGlhbExvYWQgPSB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZXR0aW5nOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ2hhbmdpbmcgc2V0dGluZycsIG5hbWUsIHZhbHVlKTtcbiAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgc2V0dGluZ3MsIG5hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGlmKCQuaXNQbGFpbk9iamVjdChzZXR0aW5nc1tuYW1lXSkpIHtcbiAgICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgc2V0dGluZ3NbbmFtZV0sIHZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBzZXR0aW5nc1tuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBzZXR0aW5nc1tuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludGVybmFsOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBtb2R1bGUsIG5hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIG1vZHVsZVtuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGVbbmFtZV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBkZWJ1ZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy5kZWJ1Zykge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZyA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB2ZXJib3NlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50ICYmIHNldHRpbmdzLnZlcmJvc2UgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGVycm9yOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50KSB7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuZXJyb3IsIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBwZXJmb3JtYW5jZToge1xuICAgICAgICAgIGxvZzogZnVuY3Rpb24obWVzc2FnZSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lLFxuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lLFxuICAgICAgICAgICAgICBwcmV2aW91c1RpbWVcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lICAgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lICA9IHRpbWUgfHwgY3VycmVudFRpbWU7XG4gICAgICAgICAgICAgIGV4ZWN1dGlvblRpbWUgPSBjdXJyZW50VGltZSAtIHByZXZpb3VzVGltZTtcbiAgICAgICAgICAgICAgdGltZSAgICAgICAgICA9IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBwZXJmb3JtYW5jZS5wdXNoKHtcbiAgICAgICAgICAgICAgICAnTmFtZScgICAgICAgICAgIDogbWVzc2FnZVswXSxcbiAgICAgICAgICAgICAgICAnQXJndW1lbnRzJyAgICAgIDogW10uc2xpY2UuY2FsbChtZXNzYWdlLCAxKSB8fCAnJyxcbiAgICAgICAgICAgICAgICAnRWxlbWVudCcgICAgICAgIDogZWxlbWVudCxcbiAgICAgICAgICAgICAgICAnRXhlY3V0aW9uIFRpbWUnIDogZXhlY3V0aW9uVGltZVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyID0gc2V0VGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UuZGlzcGxheSwgNTAwKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc3BsYXk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHRpdGxlID0gc2V0dGluZ3MubmFtZSArICc6JyxcbiAgICAgICAgICAgICAgdG90YWxUaW1lID0gMFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgdGltZSA9IGZhbHNlO1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lcik7XG4gICAgICAgICAgICAkLmVhY2gocGVyZm9ybWFuY2UsIGZ1bmN0aW9uKGluZGV4LCBkYXRhKSB7XG4gICAgICAgICAgICAgIHRvdGFsVGltZSArPSBkYXRhWydFeGVjdXRpb24gVGltZSddO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB0aXRsZSArPSAnICcgKyB0b3RhbFRpbWUgKyAnbXMnO1xuICAgICAgICAgICAgaWYobW9kdWxlU2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgdGl0bGUgKz0gJyBcXCcnICsgbW9kdWxlU2VsZWN0b3IgKyAnXFwnJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCRhbGxNb2R1bGVzLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgICAgICAgdGl0bGUgKz0gJyAnICsgJygnICsgJGFsbE1vZHVsZXMubGVuZ3RoICsgJyknO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoIChjb25zb2xlLmdyb3VwICE9PSB1bmRlZmluZWQgfHwgY29uc29sZS50YWJsZSAhPT0gdW5kZWZpbmVkKSAmJiBwZXJmb3JtYW5jZS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBDb2xsYXBzZWQodGl0bGUpO1xuICAgICAgICAgICAgICBpZihjb25zb2xlLnRhYmxlKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS50YWJsZShwZXJmb3JtYW5jZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YVsnTmFtZSddICsgJzogJyArIGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ10rJ21zJyk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgY29uc29sZS5ncm91cEVuZCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcGVyZm9ybWFuY2UgPSBbXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludm9rZTogZnVuY3Rpb24ocXVlcnksIHBhc3NlZEFyZ3VtZW50cywgY29udGV4dCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgb2JqZWN0ID0gaW5zdGFuY2UsXG4gICAgICAgICAgICBtYXhEZXB0aCxcbiAgICAgICAgICAgIGZvdW5kLFxuICAgICAgICAgICAgcmVzcG9uc2VcbiAgICAgICAgICA7XG4gICAgICAgICAgcGFzc2VkQXJndW1lbnRzID0gcGFzc2VkQXJndW1lbnRzIHx8IHF1ZXJ5QXJndW1lbnRzO1xuICAgICAgICAgIGNvbnRleHQgICAgICAgICA9IGVsZW1lbnQgICAgICAgICB8fCBjb250ZXh0O1xuICAgICAgICAgIGlmKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyAmJiBvYmplY3QgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcXVlcnkgICAgPSBxdWVyeS5zcGxpdCgvW1xcLiBdLyk7XG4gICAgICAgICAgICBtYXhEZXB0aCA9IHF1ZXJ5Lmxlbmd0aCAtIDE7XG4gICAgICAgICAgICAkLmVhY2gocXVlcnksIGZ1bmN0aW9uKGRlcHRoLCB2YWx1ZSkge1xuICAgICAgICAgICAgICB2YXIgY2FtZWxDYXNlVmFsdWUgPSAoZGVwdGggIT0gbWF4RGVwdGgpXG4gICAgICAgICAgICAgICAgPyB2YWx1ZSArIHF1ZXJ5W2RlcHRoICsgMV0uY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBxdWVyeVtkZXB0aCArIDFdLnNsaWNlKDEpXG4gICAgICAgICAgICAgICAgOiBxdWVyeVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gKSAmJiAoZGVwdGggIT0gbWF4RGVwdGgpICkge1xuICAgICAgICAgICAgICAgIG9iamVjdCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgICAgIGZvdW5kID0gb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbdmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFt2YWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFt2YWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmICggJC5pc0Z1bmN0aW9uKCBmb3VuZCApICkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZC5hcHBseShjb250ZXh0LCBwYXNzZWRBcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKGZvdW5kICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gZm91bmQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKCQuaXNBcnJheShyZXR1cm5lZFZhbHVlKSkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZS5wdXNoKHJlc3BvbnNlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSBbcmV0dXJuZWRWYWx1ZSwgcmVzcG9uc2VdO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJlc3BvbnNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSByZXNwb25zZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGZvdW5kO1xuICAgICAgICB9XG4gICAgICB9O1xuICAgICAgaWYobWV0aG9kSW52b2tlZCkge1xuICAgICAgICBpZihpbnN0YW5jZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgbW9kdWxlLmluaXRpYWxpemUoKTtcbiAgICAgICAgfVxuICAgICAgICBtb2R1bGUuaW52b2tlKHF1ZXJ5KTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBpZihpbnN0YW5jZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgaW5zdGFuY2UuaW52b2tlKCdkZXN0cm95Jyk7XG4gICAgICAgIH1cbiAgICAgICAgbW9kdWxlLmluaXRpYWxpemUoKTtcbiAgICAgIH1cbiAgICB9KVxuICA7XG5cbiAgcmV0dXJuIChyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpXG4gICAgPyByZXR1cm5lZFZhbHVlXG4gICAgOiB0aGlzXG4gIDtcbn07XG5cbiQuZm4ucmF0aW5nLnNldHRpbmdzID0ge1xuXG4gIG5hbWUgICAgICAgICAgOiAnUmF0aW5nJyxcbiAgbmFtZXNwYWNlICAgICA6ICdyYXRpbmcnLFxuXG4gIHNsZW50ICAgICAgICAgOiBmYWxzZSxcbiAgZGVidWcgICAgICAgICA6IGZhbHNlLFxuICB2ZXJib3NlICAgICAgIDogZmFsc2UsXG4gIHBlcmZvcm1hbmNlICAgOiB0cnVlLFxuXG4gIGluaXRpYWxSYXRpbmcgOiAwLFxuICBpbnRlcmFjdGl2ZSAgIDogdHJ1ZSxcbiAgbWF4UmF0aW5nICAgICA6IDQsXG4gIGNsZWFyYWJsZSAgICAgOiAnYXV0bycsXG5cbiAgZmlyZU9uSW5pdCAgICA6IGZhbHNlLFxuXG4gIG9uUmF0ZSAgICAgICAgOiBmdW5jdGlvbihyYXRpbmcpe30sXG5cbiAgZXJyb3IgICAgICAgICA6IHtcbiAgICBtZXRob2QgICAgOiAnVGhlIG1ldGhvZCB5b3UgY2FsbGVkIGlzIG5vdCBkZWZpbmVkJyxcbiAgICBub01heGltdW0gOiAnTm8gbWF4aW11bSByYXRpbmcgc3BlY2lmaWVkLiBDYW5ub3QgZ2VuZXJhdGUgSFRNTCBhdXRvbWF0aWNhbGx5J1xuICB9LFxuXG5cbiAgbWV0YWRhdGE6IHtcbiAgICByYXRpbmcgICAgOiAncmF0aW5nJyxcbiAgICBtYXhSYXRpbmcgOiAnbWF4UmF0aW5nJ1xuICB9LFxuXG4gIGNsYXNzTmFtZSA6IHtcbiAgICBhY3RpdmUgICA6ICdhY3RpdmUnLFxuICAgIGRpc2FibGVkIDogJ2Rpc2FibGVkJyxcbiAgICBzZWxlY3RlZCA6ICdzZWxlY3RlZCcsXG4gICAgbG9hZGluZyAgOiAnbG9hZGluZydcbiAgfSxcblxuICBzZWxlY3RvciAgOiB7XG4gICAgaWNvbiA6ICcuaWNvbidcbiAgfSxcblxuICB0ZW1wbGF0ZXM6IHtcbiAgICBpY29uOiBmdW5jdGlvbihtYXhSYXRpbmcpIHtcbiAgICAgIHZhclxuICAgICAgICBpY29uID0gMSxcbiAgICAgICAgaHRtbCA9ICcnXG4gICAgICA7XG4gICAgICB3aGlsZShpY29uIDw9IG1heFJhdGluZykge1xuICAgICAgICBodG1sICs9ICc8aSBjbGFzcz1cImljb25cIj48L2k+JztcbiAgICAgICAgaWNvbisrO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGh0bWw7XG4gICAgfVxuICB9XG5cbn07XG5cbn0pKCBqUXVlcnksIHdpbmRvdywgZG9jdW1lbnQgKTtcbiJdfQ==