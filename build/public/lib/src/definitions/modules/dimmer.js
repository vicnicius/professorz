/*!
 * # Semantic UI - Dimmer
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.dimmer = function (parameters) {
    var $allModules = $(this),
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue;

    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.dimmer.settings, parameters) : $.extend({}, $.fn.dimmer.settings),
          selector = settings.selector,
          namespace = settings.namespace,
          className = settings.className,
          error = settings.error,
          eventNamespace = '.' + namespace,
          moduleNamespace = 'module-' + namespace,
          moduleSelector = $allModules.selector || '',
          clickEvent = 'ontouchstart' in document.documentElement ? 'touchstart' : 'click',
          $module = $(this),
          $dimmer,
          $dimmable,
          element = this,
          instance = $module.data(moduleNamespace),
          module;

      module = {

        preinitialize: function () {
          if (module.is.dimmer()) {

            $dimmable = $module.parent();
            $dimmer = $module;
          } else {
            $dimmable = $module;
            if (module.has.dimmer()) {
              if (settings.dimmerName) {
                $dimmer = $dimmable.find(selector.dimmer).filter('.' + settings.dimmerName);
              } else {
                $dimmer = $dimmable.find(selector.dimmer);
              }
            } else {
              $dimmer = module.create();
            }
            module.set.variation();
          }
        },

        initialize: function () {
          module.debug('Initializing dimmer', settings);

          module.bind.events();
          module.set.dimmable();
          module.instantiate();
        },

        instantiate: function () {
          module.verbose('Storing instance of module', module);
          instance = module;
          $module.data(moduleNamespace, instance);
        },

        destroy: function () {
          module.verbose('Destroying previous module', $dimmer);
          module.unbind.events();
          module.remove.variation();
          $dimmable.off(eventNamespace);
        },

        bind: {
          events: function () {
            if (settings.on == 'hover') {
              $dimmable.on('mouseenter' + eventNamespace, module.show).on('mouseleave' + eventNamespace, module.hide);
            } else if (settings.on == 'click') {
              $dimmable.on(clickEvent + eventNamespace, module.toggle);
            }
            if (module.is.page()) {
              module.debug('Setting as a page dimmer', $dimmable);
              module.set.pageDimmer();
            }

            if (module.is.closable()) {
              module.verbose('Adding dimmer close event', $dimmer);
              $dimmable.on(clickEvent + eventNamespace, selector.dimmer, module.event.click);
            }
          }
        },

        unbind: {
          events: function () {
            $module.removeData(moduleNamespace);
            $dimmable.off(eventNamespace);
          }
        },

        event: {
          click: function (event) {
            module.verbose('Determining if event occured on dimmer', event);
            if ($dimmer.find(event.target).length === 0 || $(event.target).is(selector.content)) {
              module.hide();
              event.stopImmediatePropagation();
            }
          }
        },

        addContent: function (element) {
          var $content = $(element);
          module.debug('Add content to dimmer', $content);
          if ($content.parent()[0] !== $dimmer[0]) {
            $content.detach().appendTo($dimmer);
          }
        },

        create: function () {
          var $element = $(settings.template.dimmer());
          if (settings.dimmerName) {
            module.debug('Creating named dimmer', settings.dimmerName);
            $element.addClass(settings.dimmerName);
          }
          $element.appendTo($dimmable);
          return $element;
        },

        show: function (callback) {
          callback = $.isFunction(callback) ? callback : function () {};
          module.debug('Showing dimmer', $dimmer, settings);
          if ((!module.is.dimmed() || module.is.animating()) && module.is.enabled()) {
            module.animate.show(callback);
            settings.onShow.call(element);
            settings.onChange.call(element);
          } else {
            module.debug('Dimmer is already shown or disabled');
          }
        },

        hide: function (callback) {
          callback = $.isFunction(callback) ? callback : function () {};
          if (module.is.dimmed() || module.is.animating()) {
            module.debug('Hiding dimmer', $dimmer);
            module.animate.hide(callback);
            settings.onHide.call(element);
            settings.onChange.call(element);
          } else {
            module.debug('Dimmer is not visible');
          }
        },

        toggle: function () {
          module.verbose('Toggling dimmer visibility', $dimmer);
          if (!module.is.dimmed()) {
            module.show();
          } else {
            module.hide();
          }
        },

        animate: {
          show: function (callback) {
            callback = $.isFunction(callback) ? callback : function () {};
            if (settings.useCSS && $.fn.transition !== undefined && $dimmer.transition('is supported')) {
              if (settings.opacity !== 'auto') {
                module.set.opacity();
              }
              $dimmer.transition({
                animation: settings.transition + ' in',
                queue: false,
                duration: module.get.duration(),
                useFailSafe: true,
                onStart: function () {
                  module.set.dimmed();
                },
                onComplete: function () {
                  module.set.active();
                  callback();
                }
              });
            } else {
              module.verbose('Showing dimmer animation with javascript');
              module.set.dimmed();
              if (settings.opacity == 'auto') {
                settings.opacity = 0.8;
              }
              $dimmer.stop().css({
                opacity: 0,
                width: '100%',
                height: '100%'
              }).fadeTo(module.get.duration(), settings.opacity, function () {
                $dimmer.removeAttr('style');
                module.set.active();
                callback();
              });
            }
          },
          hide: function (callback) {
            callback = $.isFunction(callback) ? callback : function () {};
            if (settings.useCSS && $.fn.transition !== undefined && $dimmer.transition('is supported')) {
              module.verbose('Hiding dimmer with css');
              $dimmer.transition({
                animation: settings.transition + ' out',
                queue: false,
                duration: module.get.duration(),
                useFailSafe: true,
                onStart: function () {
                  module.remove.dimmed();
                },
                onComplete: function () {
                  module.remove.active();
                  callback();
                }
              });
            } else {
              module.verbose('Hiding dimmer with javascript');
              module.remove.dimmed();
              $dimmer.stop().fadeOut(module.get.duration(), function () {
                module.remove.active();
                $dimmer.removeAttr('style');
                callback();
              });
            }
          }
        },

        get: {
          dimmer: function () {
            return $dimmer;
          },
          duration: function () {
            if (typeof settings.duration == 'object') {
              if (module.is.active()) {
                return settings.duration.hide;
              } else {
                return settings.duration.show;
              }
            }
            return settings.duration;
          }
        },

        has: {
          dimmer: function () {
            if (settings.dimmerName) {
              return $module.find(selector.dimmer).filter('.' + settings.dimmerName).length > 0;
            } else {
              return $module.find(selector.dimmer).length > 0;
            }
          }
        },

        is: {
          active: function () {
            return $dimmer.hasClass(className.active);
          },
          animating: function () {
            return $dimmer.is(':animated') || $dimmer.hasClass(className.animating);
          },
          closable: function () {
            if (settings.closable == 'auto') {
              if (settings.on == 'hover') {
                return false;
              }
              return true;
            }
            return settings.closable;
          },
          dimmer: function () {
            return $module.hasClass(className.dimmer);
          },
          dimmable: function () {
            return $module.hasClass(className.dimmable);
          },
          dimmed: function () {
            return $dimmable.hasClass(className.dimmed);
          },
          disabled: function () {
            return $dimmable.hasClass(className.disabled);
          },
          enabled: function () {
            return !module.is.disabled();
          },
          page: function () {
            return $dimmable.is('body');
          },
          pageDimmer: function () {
            return $dimmer.hasClass(className.pageDimmer);
          }
        },

        can: {
          show: function () {
            return !$dimmer.hasClass(className.disabled);
          }
        },

        set: {
          opacity: function (opacity) {
            var color = $dimmer.css('background-color'),
                colorArray = color.split(','),
                isRGB = colorArray && colorArray.length == 3,
                isRGBA = colorArray && colorArray.length == 4;
            opacity = settings.opacity === 0 ? 0 : settings.opacity || opacity;
            if (isRGB || isRGBA) {
              colorArray[3] = opacity + ')';
              color = colorArray.join(',');
            } else {
              color = 'rgba(0, 0, 0, ' + opacity + ')';
            }
            module.debug('Setting opacity to', opacity);
            $dimmer.css('background-color', color);
          },
          active: function () {
            $dimmer.addClass(className.active);
          },
          dimmable: function () {
            $dimmable.addClass(className.dimmable);
          },
          dimmed: function () {
            $dimmable.addClass(className.dimmed);
          },
          pageDimmer: function () {
            $dimmer.addClass(className.pageDimmer);
          },
          disabled: function () {
            $dimmer.addClass(className.disabled);
          },
          variation: function (variation) {
            variation = variation || settings.variation;
            if (variation) {
              $dimmer.addClass(variation);
            }
          }
        },

        remove: {
          active: function () {
            $dimmer.removeClass(className.active);
          },
          dimmed: function () {
            $dimmable.removeClass(className.dimmed);
          },
          disabled: function () {
            $dimmer.removeClass(className.disabled);
          },
          variation: function (variation) {
            variation = variation || settings.variation;
            if (variation) {
              $dimmer.removeClass(variation);
            }
          }
        },

        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ($allModules.length > 1) {
              title += ' ' + '(' + $allModules.length + ')';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                module.error(error.method, query);
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };

      module.preinitialize();

      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.dimmer.settings = {

    name: 'Dimmer',
    namespace: 'dimmer',

    silent: false,
    debug: false,
    verbose: false,
    performance: true,

    // name to distinguish between multiple dimmers in context
    dimmerName: false,

    // whether to add a variation type
    variation: false,

    // whether to bind close events
    closable: 'auto',

    // whether to use css animations
    useCSS: true,

    // css animation to use
    transition: 'fade',

    // event to bind to
    on: false,

    // overriding opacity value
    opacity: 'auto',

    // transition durations
    duration: {
      show: 500,
      hide: 500
    },

    onChange: function () {},
    onShow: function () {},
    onHide: function () {},

    error: {
      method: 'The method you called is not defined.'
    },

    className: {
      active: 'active',
      animating: 'animating',
      dimmable: 'dimmable',
      dimmed: 'dimmed',
      dimmer: 'dimmer',
      disabled: 'disabled',
      hide: 'hide',
      pageDimmer: 'page',
      show: 'show'
    },

    selector: {
      dimmer: '> .ui.dimmer',
      content: '.ui.dimmer > .content, .ui.dimmer > .content > .center'
    },

    template: {
      dimmer: function () {
        return $('<div />').attr('class', 'ui dimmer');
      }
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL2RpbW1lci5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBVUEsQ0FBQyxDQUFDLFVBQVUsQ0FBVixFQUFhLE1BQWIsRUFBcUIsUUFBckIsRUFBK0IsU0FBL0IsRUFBMEM7O0FBRTVDOztBQUVBLFdBQVUsT0FBTyxNQUFQLElBQWlCLFdBQWpCLElBQWdDLE9BQU8sSUFBUCxJQUFlLElBQWhELEdBQ0wsTUFESyxHQUVKLE9BQU8sSUFBUCxJQUFlLFdBQWYsSUFBOEIsS0FBSyxJQUFMLElBQWEsSUFBNUMsR0FDRSxJQURGLEdBRUUsU0FBUyxhQUFULEdBSk47O0FBT0EsSUFBRSxFQUFGLENBQUssTUFBTCxHQUFjLFVBQVMsVUFBVCxFQUFxQjtBQUNqQyxRQUNFLGNBQWtCLEVBQUUsSUFBRixDQURwQjtBQUFBLFFBR0UsT0FBa0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUhwQjtBQUFBLFFBSUUsY0FBa0IsRUFKcEI7QUFBQSxRQU1FLFFBQWtCLFVBQVUsQ0FBVixDQU5wQjtBQUFBLFFBT0UsZ0JBQW1CLE9BQU8sS0FBUCxJQUFnQixRQVByQztBQUFBLFFBUUUsaUJBQWtCLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLENBQXpCLENBUnBCO0FBQUEsUUFVRSxhQVZGOztBQWFBLGdCQUNHLElBREgsQ0FDUSxZQUFXO0FBQ2YsVUFDRSxXQUFvQixFQUFFLGFBQUYsQ0FBZ0IsVUFBaEIsQ0FBRixHQUNkLEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLEVBQUUsRUFBRixDQUFLLE1BQUwsQ0FBWSxRQUEvQixFQUF5QyxVQUF6QyxDQURjLEdBRWQsRUFBRSxNQUFGLENBQVMsRUFBVCxFQUFhLEVBQUUsRUFBRixDQUFLLE1BQUwsQ0FBWSxRQUF6QixDQUhOO0FBQUEsVUFLRSxXQUFrQixTQUFTLFFBTDdCO0FBQUEsVUFNRSxZQUFrQixTQUFTLFNBTjdCO0FBQUEsVUFPRSxZQUFrQixTQUFTLFNBUDdCO0FBQUEsVUFRRSxRQUFrQixTQUFTLEtBUjdCO0FBQUEsVUFVRSxpQkFBa0IsTUFBTSxTQVYxQjtBQUFBLFVBV0Usa0JBQWtCLFlBQVksU0FYaEM7QUFBQSxVQVlFLGlCQUFrQixZQUFZLFFBQVosSUFBd0IsRUFaNUM7QUFBQSxVQWNFLGFBQW1CLGtCQUFrQixTQUFTLGVBQTVCLEdBQ2QsWUFEYyxHQUVkLE9BaEJOO0FBQUEsVUFrQkUsVUFBVSxFQUFFLElBQUYsQ0FsQlo7QUFBQSxVQW1CRSxPQW5CRjtBQUFBLFVBb0JFLFNBcEJGO0FBQUEsVUFzQkUsVUFBWSxJQXRCZDtBQUFBLFVBdUJFLFdBQVksUUFBUSxJQUFSLENBQWEsZUFBYixDQXZCZDtBQUFBLFVBd0JFLE1BeEJGOztBQTJCQSxlQUFTOztBQUVQLHVCQUFlLFlBQVc7QUFDeEIsY0FBSSxPQUFPLEVBQVAsQ0FBVSxNQUFWLEVBQUosRUFBeUI7O0FBRXZCLHdCQUFZLFFBQVEsTUFBUixFQUFaO0FBQ0Esc0JBQVksT0FBWjtBQUNELFdBSkQsTUFLSztBQUNILHdCQUFZLE9BQVo7QUFDQSxnQkFBSSxPQUFPLEdBQVAsQ0FBVyxNQUFYLEVBQUosRUFBMEI7QUFDeEIsa0JBQUcsU0FBUyxVQUFaLEVBQXdCO0FBQ3RCLDBCQUFVLFVBQVUsSUFBVixDQUFlLFNBQVMsTUFBeEIsRUFBZ0MsTUFBaEMsQ0FBdUMsTUFBTSxTQUFTLFVBQXRELENBQVY7QUFDRCxlQUZELE1BR0s7QUFDSCwwQkFBVSxVQUFVLElBQVYsQ0FBZSxTQUFTLE1BQXhCLENBQVY7QUFDRDtBQUNGLGFBUEQsTUFRSztBQUNILHdCQUFVLE9BQU8sTUFBUCxFQUFWO0FBQ0Q7QUFDRCxtQkFBTyxHQUFQLENBQVcsU0FBWDtBQUNEO0FBQ0YsU0F2Qk07O0FBeUJQLG9CQUFZLFlBQVc7QUFDckIsaUJBQU8sS0FBUCxDQUFhLHFCQUFiLEVBQW9DLFFBQXBDOztBQUVBLGlCQUFPLElBQVAsQ0FBWSxNQUFaO0FBQ0EsaUJBQU8sR0FBUCxDQUFXLFFBQVg7QUFDQSxpQkFBTyxXQUFQO0FBQ0QsU0EvQk07O0FBaUNQLHFCQUFhLFlBQVc7QUFDdEIsaUJBQU8sT0FBUCxDQUFlLDRCQUFmLEVBQTZDLE1BQTdDO0FBQ0EscUJBQVcsTUFBWDtBQUNBLGtCQUNHLElBREgsQ0FDUSxlQURSLEVBQ3lCLFFBRHpCO0FBR0QsU0F2Q007O0FBeUNQLGlCQUFTLFlBQVc7QUFDbEIsaUJBQU8sT0FBUCxDQUFlLDRCQUFmLEVBQTZDLE9BQTdDO0FBQ0EsaUJBQU8sTUFBUCxDQUFjLE1BQWQ7QUFDQSxpQkFBTyxNQUFQLENBQWMsU0FBZDtBQUNBLG9CQUNHLEdBREgsQ0FDTyxjQURQO0FBR0QsU0FoRE07O0FBa0RQLGNBQU07QUFDSixrQkFBUSxZQUFXO0FBQ2pCLGdCQUFHLFNBQVMsRUFBVCxJQUFlLE9BQWxCLEVBQTJCO0FBQ3pCLHdCQUNHLEVBREgsQ0FDTSxlQUFlLGNBRHJCLEVBQ3FDLE9BQU8sSUFENUMsRUFFRyxFQUZILENBRU0sZUFBZSxjQUZyQixFQUVxQyxPQUFPLElBRjVDO0FBSUQsYUFMRCxNQU1LLElBQUcsU0FBUyxFQUFULElBQWUsT0FBbEIsRUFBMkI7QUFDOUIsd0JBQ0csRUFESCxDQUNNLGFBQWEsY0FEbkIsRUFDbUMsT0FBTyxNQUQxQztBQUdEO0FBQ0QsZ0JBQUksT0FBTyxFQUFQLENBQVUsSUFBVixFQUFKLEVBQXVCO0FBQ3JCLHFCQUFPLEtBQVAsQ0FBYSwwQkFBYixFQUF5QyxTQUF6QztBQUNBLHFCQUFPLEdBQVAsQ0FBVyxVQUFYO0FBQ0Q7O0FBRUQsZ0JBQUksT0FBTyxFQUFQLENBQVUsUUFBVixFQUFKLEVBQTJCO0FBQ3pCLHFCQUFPLE9BQVAsQ0FBZSwyQkFBZixFQUE0QyxPQUE1QztBQUNBLHdCQUNHLEVBREgsQ0FDTSxhQUFhLGNBRG5CLEVBQ21DLFNBQVMsTUFENUMsRUFDb0QsT0FBTyxLQUFQLENBQWEsS0FEakU7QUFHRDtBQUNGO0FBeEJHLFNBbERDOztBQTZFUCxnQkFBUTtBQUNOLGtCQUFRLFlBQVc7QUFDakIsb0JBQ0csVUFESCxDQUNjLGVBRGQ7QUFHQSxzQkFDRyxHQURILENBQ08sY0FEUDtBQUdEO0FBUkssU0E3RUQ7O0FBd0ZQLGVBQU87QUFDTCxpQkFBTyxVQUFTLEtBQVQsRUFBZ0I7QUFDckIsbUJBQU8sT0FBUCxDQUFlLHdDQUFmLEVBQXlELEtBQXpEO0FBQ0EsZ0JBQUksUUFBUSxJQUFSLENBQWEsTUFBTSxNQUFuQixFQUEyQixNQUEzQixLQUFzQyxDQUF0QyxJQUEyQyxFQUFFLE1BQU0sTUFBUixFQUFnQixFQUFoQixDQUFtQixTQUFTLE9BQTVCLENBQS9DLEVBQXNGO0FBQ3BGLHFCQUFPLElBQVA7QUFDQSxvQkFBTSx3QkFBTjtBQUNEO0FBQ0Y7QUFQSSxTQXhGQTs7QUFrR1Asb0JBQVksVUFBUyxPQUFULEVBQWtCO0FBQzVCLGNBQ0UsV0FBVyxFQUFFLE9BQUYsQ0FEYjtBQUdBLGlCQUFPLEtBQVAsQ0FBYSx1QkFBYixFQUFzQyxRQUF0QztBQUNBLGNBQUcsU0FBUyxNQUFULEdBQWtCLENBQWxCLE1BQXlCLFFBQVEsQ0FBUixDQUE1QixFQUF3QztBQUN0QyxxQkFBUyxNQUFULEdBQWtCLFFBQWxCLENBQTJCLE9BQTNCO0FBQ0Q7QUFDRixTQTFHTTs7QUE0R1AsZ0JBQVEsWUFBVztBQUNqQixjQUNFLFdBQVcsRUFBRyxTQUFTLFFBQVQsQ0FBa0IsTUFBbEIsRUFBSCxDQURiO0FBR0EsY0FBRyxTQUFTLFVBQVosRUFBd0I7QUFDdEIsbUJBQU8sS0FBUCxDQUFhLHVCQUFiLEVBQXNDLFNBQVMsVUFBL0M7QUFDQSxxQkFBUyxRQUFULENBQWtCLFNBQVMsVUFBM0I7QUFDRDtBQUNELG1CQUNHLFFBREgsQ0FDWSxTQURaO0FBR0EsaUJBQU8sUUFBUDtBQUNELFNBeEhNOztBQTBIUCxjQUFNLFVBQVMsUUFBVCxFQUFtQjtBQUN2QixxQkFBVyxFQUFFLFVBQUYsQ0FBYSxRQUFiLElBQ1AsUUFETyxHQUVQLFlBQVUsQ0FBRSxDQUZoQjtBQUlBLGlCQUFPLEtBQVAsQ0FBYSxnQkFBYixFQUErQixPQUEvQixFQUF3QyxRQUF4QztBQUNBLGNBQUksQ0FBQyxDQUFDLE9BQU8sRUFBUCxDQUFVLE1BQVYsRUFBRCxJQUF1QixPQUFPLEVBQVAsQ0FBVSxTQUFWLEVBQXhCLEtBQWtELE9BQU8sRUFBUCxDQUFVLE9BQVYsRUFBdEQsRUFBNEU7QUFDMUUsbUJBQU8sT0FBUCxDQUFlLElBQWYsQ0FBb0IsUUFBcEI7QUFDQSxxQkFBUyxNQUFULENBQWdCLElBQWhCLENBQXFCLE9BQXJCO0FBQ0EscUJBQVMsUUFBVCxDQUFrQixJQUFsQixDQUF1QixPQUF2QjtBQUNELFdBSkQsTUFLSztBQUNILG1CQUFPLEtBQVAsQ0FBYSxxQ0FBYjtBQUNEO0FBQ0YsU0F4SU07O0FBMElQLGNBQU0sVUFBUyxRQUFULEVBQW1CO0FBQ3ZCLHFCQUFXLEVBQUUsVUFBRixDQUFhLFFBQWIsSUFDUCxRQURPLEdBRVAsWUFBVSxDQUFFLENBRmhCO0FBSUEsY0FBSSxPQUFPLEVBQVAsQ0FBVSxNQUFWLE1BQXNCLE9BQU8sRUFBUCxDQUFVLFNBQVYsRUFBMUIsRUFBa0Q7QUFDaEQsbUJBQU8sS0FBUCxDQUFhLGVBQWIsRUFBOEIsT0FBOUI7QUFDQSxtQkFBTyxPQUFQLENBQWUsSUFBZixDQUFvQixRQUFwQjtBQUNBLHFCQUFTLE1BQVQsQ0FBZ0IsSUFBaEIsQ0FBcUIsT0FBckI7QUFDQSxxQkFBUyxRQUFULENBQWtCLElBQWxCLENBQXVCLE9BQXZCO0FBQ0QsV0FMRCxNQU1LO0FBQ0gsbUJBQU8sS0FBUCxDQUFhLHVCQUFiO0FBQ0Q7QUFDRixTQXhKTTs7QUEwSlAsZ0JBQVEsWUFBVztBQUNqQixpQkFBTyxPQUFQLENBQWUsNEJBQWYsRUFBNkMsT0FBN0M7QUFDQSxjQUFJLENBQUMsT0FBTyxFQUFQLENBQVUsTUFBVixFQUFMLEVBQTBCO0FBQ3hCLG1CQUFPLElBQVA7QUFDRCxXQUZELE1BR0s7QUFDSCxtQkFBTyxJQUFQO0FBQ0Q7QUFDRixTQWxLTTs7QUFvS1AsaUJBQVM7QUFDUCxnQkFBTSxVQUFTLFFBQVQsRUFBbUI7QUFDdkIsdUJBQVcsRUFBRSxVQUFGLENBQWEsUUFBYixJQUNQLFFBRE8sR0FFUCxZQUFVLENBQUUsQ0FGaEI7QUFJQSxnQkFBRyxTQUFTLE1BQVQsSUFBbUIsRUFBRSxFQUFGLENBQUssVUFBTCxLQUFvQixTQUF2QyxJQUFvRCxRQUFRLFVBQVIsQ0FBbUIsY0FBbkIsQ0FBdkQsRUFBMkY7QUFDekYsa0JBQUcsU0FBUyxPQUFULEtBQXFCLE1BQXhCLEVBQWdDO0FBQzlCLHVCQUFPLEdBQVAsQ0FBVyxPQUFYO0FBQ0Q7QUFDRCxzQkFDRyxVQURILENBQ2M7QUFDViwyQkFBYyxTQUFTLFVBQVQsR0FBc0IsS0FEMUI7QUFFVix1QkFBYyxLQUZKO0FBR1YsMEJBQWMsT0FBTyxHQUFQLENBQVcsUUFBWCxFQUhKO0FBSVYsNkJBQWMsSUFKSjtBQUtWLHlCQUFjLFlBQVc7QUFDdkIseUJBQU8sR0FBUCxDQUFXLE1BQVg7QUFDRCxpQkFQUztBQVFWLDRCQUFjLFlBQVc7QUFDdkIseUJBQU8sR0FBUCxDQUFXLE1BQVg7QUFDQTtBQUNEO0FBWFMsZUFEZDtBQWVELGFBbkJELE1Bb0JLO0FBQ0gscUJBQU8sT0FBUCxDQUFlLDBDQUFmO0FBQ0EscUJBQU8sR0FBUCxDQUFXLE1BQVg7QUFDQSxrQkFBRyxTQUFTLE9BQVQsSUFBb0IsTUFBdkIsRUFBK0I7QUFDN0IseUJBQVMsT0FBVCxHQUFtQixHQUFuQjtBQUNEO0FBQ0Qsc0JBQ0csSUFESCxHQUVHLEdBRkgsQ0FFTztBQUNILHlCQUFVLENBRFA7QUFFSCx1QkFBVSxNQUZQO0FBR0gsd0JBQVU7QUFIUCxlQUZQLEVBT0csTUFQSCxDQU9VLE9BQU8sR0FBUCxDQUFXLFFBQVgsRUFQVixFQU9pQyxTQUFTLE9BUDFDLEVBT21ELFlBQVc7QUFDMUQsd0JBQVEsVUFBUixDQUFtQixPQUFuQjtBQUNBLHVCQUFPLEdBQVAsQ0FBVyxNQUFYO0FBQ0E7QUFDRCxlQVhIO0FBYUQ7QUFDRixXQTlDTTtBQStDUCxnQkFBTSxVQUFTLFFBQVQsRUFBbUI7QUFDdkIsdUJBQVcsRUFBRSxVQUFGLENBQWEsUUFBYixJQUNQLFFBRE8sR0FFUCxZQUFVLENBQUUsQ0FGaEI7QUFJQSxnQkFBRyxTQUFTLE1BQVQsSUFBbUIsRUFBRSxFQUFGLENBQUssVUFBTCxLQUFvQixTQUF2QyxJQUFvRCxRQUFRLFVBQVIsQ0FBbUIsY0FBbkIsQ0FBdkQsRUFBMkY7QUFDekYscUJBQU8sT0FBUCxDQUFlLHdCQUFmO0FBQ0Esc0JBQ0csVUFESCxDQUNjO0FBQ1YsMkJBQWMsU0FBUyxVQUFULEdBQXNCLE1BRDFCO0FBRVYsdUJBQWMsS0FGSjtBQUdWLDBCQUFjLE9BQU8sR0FBUCxDQUFXLFFBQVgsRUFISjtBQUlWLDZCQUFjLElBSko7QUFLVix5QkFBYyxZQUFXO0FBQ3ZCLHlCQUFPLE1BQVAsQ0FBYyxNQUFkO0FBQ0QsaUJBUFM7QUFRViw0QkFBYyxZQUFXO0FBQ3ZCLHlCQUFPLE1BQVAsQ0FBYyxNQUFkO0FBQ0E7QUFDRDtBQVhTLGVBRGQ7QUFlRCxhQWpCRCxNQWtCSztBQUNILHFCQUFPLE9BQVAsQ0FBZSwrQkFBZjtBQUNBLHFCQUFPLE1BQVAsQ0FBYyxNQUFkO0FBQ0Esc0JBQ0csSUFESCxHQUVHLE9BRkgsQ0FFVyxPQUFPLEdBQVAsQ0FBVyxRQUFYLEVBRlgsRUFFa0MsWUFBVztBQUN6Qyx1QkFBTyxNQUFQLENBQWMsTUFBZDtBQUNBLHdCQUFRLFVBQVIsQ0FBbUIsT0FBbkI7QUFDQTtBQUNELGVBTkg7QUFRRDtBQUNGO0FBbEZNLFNBcEtGOztBQXlQUCxhQUFLO0FBQ0gsa0JBQVEsWUFBVztBQUNqQixtQkFBTyxPQUFQO0FBQ0QsV0FIRTtBQUlILG9CQUFVLFlBQVc7QUFDbkIsZ0JBQUcsT0FBTyxTQUFTLFFBQWhCLElBQTRCLFFBQS9CLEVBQXlDO0FBQ3ZDLGtCQUFJLE9BQU8sRUFBUCxDQUFVLE1BQVYsRUFBSixFQUF5QjtBQUN2Qix1QkFBTyxTQUFTLFFBQVQsQ0FBa0IsSUFBekI7QUFDRCxlQUZELE1BR0s7QUFDSCx1QkFBTyxTQUFTLFFBQVQsQ0FBa0IsSUFBekI7QUFDRDtBQUNGO0FBQ0QsbUJBQU8sU0FBUyxRQUFoQjtBQUNEO0FBZEUsU0F6UEU7O0FBMFFQLGFBQUs7QUFDSCxrQkFBUSxZQUFXO0FBQ2pCLGdCQUFHLFNBQVMsVUFBWixFQUF3QjtBQUN0QixxQkFBUSxRQUFRLElBQVIsQ0FBYSxTQUFTLE1BQXRCLEVBQThCLE1BQTlCLENBQXFDLE1BQU0sU0FBUyxVQUFwRCxFQUFnRSxNQUFoRSxHQUF5RSxDQUFqRjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFTLFFBQVEsSUFBUixDQUFhLFNBQVMsTUFBdEIsRUFBOEIsTUFBOUIsR0FBdUMsQ0FBaEQ7QUFDRDtBQUNGO0FBUkUsU0ExUUU7O0FBcVJQLFlBQUk7QUFDRixrQkFBUSxZQUFXO0FBQ2pCLG1CQUFPLFFBQVEsUUFBUixDQUFpQixVQUFVLE1BQTNCLENBQVA7QUFDRCxXQUhDO0FBSUYscUJBQVcsWUFBVztBQUNwQixtQkFBUyxRQUFRLEVBQVIsQ0FBVyxXQUFYLEtBQTJCLFFBQVEsUUFBUixDQUFpQixVQUFVLFNBQTNCLENBQXBDO0FBQ0QsV0FOQztBQU9GLG9CQUFVLFlBQVc7QUFDbkIsZ0JBQUcsU0FBUyxRQUFULElBQXFCLE1BQXhCLEVBQWdDO0FBQzlCLGtCQUFHLFNBQVMsRUFBVCxJQUFlLE9BQWxCLEVBQTJCO0FBQ3pCLHVCQUFPLEtBQVA7QUFDRDtBQUNELHFCQUFPLElBQVA7QUFDRDtBQUNELG1CQUFPLFNBQVMsUUFBaEI7QUFDRCxXQWZDO0FBZ0JGLGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sUUFBUSxRQUFSLENBQWlCLFVBQVUsTUFBM0IsQ0FBUDtBQUNELFdBbEJDO0FBbUJGLG9CQUFVLFlBQVc7QUFDbkIsbUJBQU8sUUFBUSxRQUFSLENBQWlCLFVBQVUsUUFBM0IsQ0FBUDtBQUNELFdBckJDO0FBc0JGLGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sVUFBVSxRQUFWLENBQW1CLFVBQVUsTUFBN0IsQ0FBUDtBQUNELFdBeEJDO0FBeUJGLG9CQUFVLFlBQVc7QUFDbkIsbUJBQU8sVUFBVSxRQUFWLENBQW1CLFVBQVUsUUFBN0IsQ0FBUDtBQUNELFdBM0JDO0FBNEJGLG1CQUFTLFlBQVc7QUFDbEIsbUJBQU8sQ0FBQyxPQUFPLEVBQVAsQ0FBVSxRQUFWLEVBQVI7QUFDRCxXQTlCQztBQStCRixnQkFBTSxZQUFZO0FBQ2hCLG1CQUFPLFVBQVUsRUFBVixDQUFhLE1BQWIsQ0FBUDtBQUNELFdBakNDO0FBa0NGLHNCQUFZLFlBQVc7QUFDckIsbUJBQU8sUUFBUSxRQUFSLENBQWlCLFVBQVUsVUFBM0IsQ0FBUDtBQUNEO0FBcENDLFNBclJHOztBQTRUUCxhQUFLO0FBQ0gsZ0JBQU0sWUFBVztBQUNmLG1CQUFPLENBQUMsUUFBUSxRQUFSLENBQWlCLFVBQVUsUUFBM0IsQ0FBUjtBQUNEO0FBSEUsU0E1VEU7O0FBa1VQLGFBQUs7QUFDSCxtQkFBUyxVQUFTLE9BQVQsRUFBa0I7QUFDekIsZ0JBQ0UsUUFBYSxRQUFRLEdBQVIsQ0FBWSxrQkFBWixDQURmO0FBQUEsZ0JBRUUsYUFBYSxNQUFNLEtBQU4sQ0FBWSxHQUFaLENBRmY7QUFBQSxnQkFHRSxRQUFjLGNBQWMsV0FBVyxNQUFYLElBQXFCLENBSG5EO0FBQUEsZ0JBSUUsU0FBYyxjQUFjLFdBQVcsTUFBWCxJQUFxQixDQUpuRDtBQU1BLHNCQUFhLFNBQVMsT0FBVCxLQUFxQixDQUFyQixHQUF5QixDQUF6QixHQUE2QixTQUFTLE9BQVQsSUFBb0IsT0FBOUQ7QUFDQSxnQkFBRyxTQUFTLE1BQVosRUFBb0I7QUFDbEIseUJBQVcsQ0FBWCxJQUFnQixVQUFVLEdBQTFCO0FBQ0Esc0JBQWdCLFdBQVcsSUFBWCxDQUFnQixHQUFoQixDQUFoQjtBQUNELGFBSEQsTUFJSztBQUNILHNCQUFRLG1CQUFtQixPQUFuQixHQUE2QixHQUFyQztBQUNEO0FBQ0QsbUJBQU8sS0FBUCxDQUFhLG9CQUFiLEVBQW1DLE9BQW5DO0FBQ0Esb0JBQVEsR0FBUixDQUFZLGtCQUFaLEVBQWdDLEtBQWhDO0FBQ0QsV0FsQkU7QUFtQkgsa0JBQVEsWUFBVztBQUNqQixvQkFBUSxRQUFSLENBQWlCLFVBQVUsTUFBM0I7QUFDRCxXQXJCRTtBQXNCSCxvQkFBVSxZQUFXO0FBQ25CLHNCQUFVLFFBQVYsQ0FBbUIsVUFBVSxRQUE3QjtBQUNELFdBeEJFO0FBeUJILGtCQUFRLFlBQVc7QUFDakIsc0JBQVUsUUFBVixDQUFtQixVQUFVLE1BQTdCO0FBQ0QsV0EzQkU7QUE0Qkgsc0JBQVksWUFBVztBQUNyQixvQkFBUSxRQUFSLENBQWlCLFVBQVUsVUFBM0I7QUFDRCxXQTlCRTtBQStCSCxvQkFBVSxZQUFXO0FBQ25CLG9CQUFRLFFBQVIsQ0FBaUIsVUFBVSxRQUEzQjtBQUNELFdBakNFO0FBa0NILHFCQUFXLFVBQVMsU0FBVCxFQUFvQjtBQUM3Qix3QkFBWSxhQUFhLFNBQVMsU0FBbEM7QUFDQSxnQkFBRyxTQUFILEVBQWM7QUFDWixzQkFBUSxRQUFSLENBQWlCLFNBQWpCO0FBQ0Q7QUFDRjtBQXZDRSxTQWxVRTs7QUE0V1AsZ0JBQVE7QUFDTixrQkFBUSxZQUFXO0FBQ2pCLG9CQUNHLFdBREgsQ0FDZSxVQUFVLE1BRHpCO0FBR0QsV0FMSztBQU1OLGtCQUFRLFlBQVc7QUFDakIsc0JBQVUsV0FBVixDQUFzQixVQUFVLE1BQWhDO0FBQ0QsV0FSSztBQVNOLG9CQUFVLFlBQVc7QUFDbkIsb0JBQVEsV0FBUixDQUFvQixVQUFVLFFBQTlCO0FBQ0QsV0FYSztBQVlOLHFCQUFXLFVBQVMsU0FBVCxFQUFvQjtBQUM3Qix3QkFBWSxhQUFhLFNBQVMsU0FBbEM7QUFDQSxnQkFBRyxTQUFILEVBQWM7QUFDWixzQkFBUSxXQUFSLENBQW9CLFNBQXBCO0FBQ0Q7QUFDRjtBQWpCSyxTQTVXRDs7QUFnWVAsaUJBQVMsVUFBUyxJQUFULEVBQWUsS0FBZixFQUFzQjtBQUM3QixpQkFBTyxLQUFQLENBQWEsa0JBQWIsRUFBaUMsSUFBakMsRUFBdUMsS0FBdkM7QUFDQSxjQUFJLEVBQUUsYUFBRixDQUFnQixJQUFoQixDQUFKLEVBQTRCO0FBQzFCLGNBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxRQUFmLEVBQXlCLElBQXpCO0FBQ0QsV0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLGdCQUFHLEVBQUUsYUFBRixDQUFnQixTQUFTLElBQVQsQ0FBaEIsQ0FBSCxFQUFvQztBQUNsQyxnQkFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLFNBQVMsSUFBVCxDQUFmLEVBQStCLEtBQS9CO0FBQ0QsYUFGRCxNQUdLO0FBQ0gsdUJBQVMsSUFBVCxJQUFpQixLQUFqQjtBQUNEO0FBQ0YsV0FQSSxNQVFBO0FBQ0gsbUJBQU8sU0FBUyxJQUFULENBQVA7QUFDRDtBQUNGLFNBaFpNO0FBaVpQLGtCQUFVLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDOUIsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsTUFBZixFQUF1QixJQUF2QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixtQkFBTyxJQUFQLElBQWUsS0FBZjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLE9BQU8sSUFBUCxDQUFQO0FBQ0Q7QUFDRixTQTNaTTtBQTRaUCxlQUFPLFlBQVc7QUFDaEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLEtBQWhDLEVBQXVDO0FBQ3JDLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2QixxQkFBTyxXQUFQLENBQW1CLEdBQW5CLENBQXVCLFNBQXZCO0FBQ0QsYUFGRCxNQUdLO0FBQ0gscUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFmO0FBQ0EscUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGO0FBQ0YsU0F0YU07QUF1YVAsaUJBQVMsWUFBVztBQUNsQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsT0FBN0IsSUFBd0MsU0FBUyxLQUFwRCxFQUEyRDtBQUN6RCxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLE9BQVAsR0FBaUIsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWpCO0FBQ0EscUJBQU8sT0FBUCxDQUFlLEtBQWYsQ0FBcUIsT0FBckIsRUFBOEIsU0FBOUI7QUFDRDtBQUNGO0FBQ0YsU0FqYk07QUFrYlAsZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQWIsRUFBcUI7QUFDbkIsbUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLEtBQXJDLEVBQTRDLE9BQTVDLEVBQXFELFNBQVMsSUFBVCxHQUFnQixHQUFyRSxDQUFmO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGLFNBdmJNO0FBd2JQLHFCQUFhO0FBQ1gsZUFBSyxVQUFTLE9BQVQsRUFBa0I7QUFDckIsZ0JBQ0UsV0FERixFQUVFLGFBRkYsRUFHRSxZQUhGO0FBS0EsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLDRCQUFnQixJQUFJLElBQUosR0FBVyxPQUFYLEVBQWhCO0FBQ0EsNkJBQWdCLFFBQVEsV0FBeEI7QUFDQSw4QkFBZ0IsY0FBYyxZQUE5QjtBQUNBLHFCQUFnQixXQUFoQjtBQUNBLDBCQUFZLElBQVosQ0FBaUI7QUFDZix3QkFBbUIsUUFBUSxDQUFSLENBREo7QUFFZiw2QkFBbUIsR0FBRyxLQUFILENBQVMsSUFBVCxDQUFjLE9BQWQsRUFBdUIsQ0FBdkIsS0FBNkIsRUFGakM7QUFHZiwyQkFBbUIsT0FISjtBQUlmLGtDQUFtQjtBQUpKLGVBQWpCO0FBTUQ7QUFDRCx5QkFBYSxPQUFPLFdBQVAsQ0FBbUIsS0FBaEM7QUFDQSxtQkFBTyxXQUFQLENBQW1CLEtBQW5CLEdBQTJCLFdBQVcsT0FBTyxXQUFQLENBQW1CLE9BQTlCLEVBQXVDLEdBQXZDLENBQTNCO0FBQ0QsV0FyQlU7QUFzQlgsbUJBQVMsWUFBVztBQUNsQixnQkFDRSxRQUFRLFNBQVMsSUFBVCxHQUFnQixHQUQxQjtBQUFBLGdCQUVFLFlBQVksQ0FGZDtBQUlBLG1CQUFPLEtBQVA7QUFDQSx5QkFBYSxPQUFPLFdBQVAsQ0FBbUIsS0FBaEM7QUFDQSxjQUFFLElBQUYsQ0FBTyxXQUFQLEVBQW9CLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjtBQUN4QywyQkFBYSxLQUFLLGdCQUFMLENBQWI7QUFDRCxhQUZEO0FBR0EscUJBQVMsTUFBTSxTQUFOLEdBQWtCLElBQTNCO0FBQ0EsZ0JBQUcsY0FBSCxFQUFtQjtBQUNqQix1QkFBUyxRQUFRLGNBQVIsR0FBeUIsSUFBbEM7QUFDRDtBQUNELGdCQUFHLFlBQVksTUFBWixHQUFxQixDQUF4QixFQUEyQjtBQUN6Qix1QkFBUyxNQUFNLEdBQU4sR0FBWSxZQUFZLE1BQXhCLEdBQWlDLEdBQTFDO0FBQ0Q7QUFDRCxnQkFBSSxDQUFDLFFBQVEsS0FBUixLQUFrQixTQUFsQixJQUErQixRQUFRLEtBQVIsS0FBa0IsU0FBbEQsS0FBZ0UsWUFBWSxNQUFaLEdBQXFCLENBQXpGLEVBQTRGO0FBQzFGLHNCQUFRLGNBQVIsQ0FBdUIsS0FBdkI7QUFDQSxrQkFBRyxRQUFRLEtBQVgsRUFBa0I7QUFDaEIsd0JBQVEsS0FBUixDQUFjLFdBQWQ7QUFDRCxlQUZELE1BR0s7QUFDSCxrQkFBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMEJBQVEsR0FBUixDQUFZLEtBQUssTUFBTCxJQUFlLElBQWYsR0FBc0IsS0FBSyxnQkFBTCxDQUF0QixHQUE2QyxJQUF6RDtBQUNELGlCQUZEO0FBR0Q7QUFDRCxzQkFBUSxRQUFSO0FBQ0Q7QUFDRCwwQkFBYyxFQUFkO0FBQ0Q7QUFwRFUsU0F4Yk47QUE4ZVAsZ0JBQVEsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDLE9BQWpDLEVBQTBDO0FBQ2hELGNBQ0UsU0FBUyxRQURYO0FBQUEsY0FFRSxRQUZGO0FBQUEsY0FHRSxLQUhGO0FBQUEsY0FJRSxRQUpGO0FBTUEsNEJBQWtCLG1CQUFtQixjQUFyQztBQUNBLG9CQUFrQixXQUFtQixPQUFyQztBQUNBLGNBQUcsT0FBTyxLQUFQLElBQWdCLFFBQWhCLElBQTRCLFdBQVcsU0FBMUMsRUFBcUQ7QUFDbkQsb0JBQVcsTUFBTSxLQUFOLENBQVksT0FBWixDQUFYO0FBQ0EsdUJBQVcsTUFBTSxNQUFOLEdBQWUsQ0FBMUI7QUFDQSxjQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ25DLGtCQUFJLGlCQUFrQixTQUFTLFFBQVYsR0FDakIsUUFBUSxNQUFNLFFBQVEsQ0FBZCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixFQUEyQixXQUEzQixFQUFSLEdBQW1ELE1BQU0sUUFBUSxDQUFkLEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBRGxDLEdBRWpCLEtBRko7QUFJQSxrQkFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxjQUFQLENBQWpCLEtBQThDLFNBQVMsUUFBM0QsRUFBdUU7QUFDckUseUJBQVMsT0FBTyxjQUFQLENBQVQ7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLGNBQVAsTUFBMkIsU0FBL0IsRUFBMkM7QUFDOUMsd0JBQVEsT0FBTyxjQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBLElBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sS0FBUCxDQUFqQixLQUFxQyxTQUFTLFFBQWxELEVBQThEO0FBQ2pFLHlCQUFTLE9BQU8sS0FBUCxDQUFUO0FBQ0QsZUFGSSxNQUdBLElBQUksT0FBTyxLQUFQLE1BQWtCLFNBQXRCLEVBQWtDO0FBQ3JDLHdCQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQTtBQUNILHVCQUFPLEtBQVAsQ0FBYSxNQUFNLE1BQW5CLEVBQTJCLEtBQTNCO0FBQ0EsdUJBQU8sS0FBUDtBQUNEO0FBQ0YsYUF2QkQ7QUF3QkQ7QUFDRCxjQUFLLEVBQUUsVUFBRixDQUFjLEtBQWQsQ0FBTCxFQUE2QjtBQUMzQix1QkFBVyxNQUFNLEtBQU4sQ0FBWSxPQUFaLEVBQXFCLGVBQXJCLENBQVg7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsdUJBQVcsS0FBWDtBQUNEO0FBQ0QsY0FBRyxFQUFFLE9BQUYsQ0FBVSxhQUFWLENBQUgsRUFBNkI7QUFDM0IsMEJBQWMsSUFBZCxDQUFtQixRQUFuQjtBQUNELFdBRkQsTUFHSyxJQUFHLGtCQUFrQixTQUFyQixFQUFnQztBQUNuQyw0QkFBZ0IsQ0FBQyxhQUFELEVBQWdCLFFBQWhCLENBQWhCO0FBQ0QsV0FGSSxNQUdBLElBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUM5Qiw0QkFBZ0IsUUFBaEI7QUFDRDtBQUNELGlCQUFPLEtBQVA7QUFDRDtBQW5pQk0sT0FBVDs7QUFzaUJBLGFBQU8sYUFBUDs7QUFFQSxVQUFHLGFBQUgsRUFBa0I7QUFDaEIsWUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLGlCQUFPLFVBQVA7QUFDRDtBQUNELGVBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDRCxPQUxELE1BTUs7QUFDSCxZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsbUJBQVMsTUFBVCxDQUFnQixTQUFoQjtBQUNEO0FBQ0QsZUFBTyxVQUFQO0FBQ0Q7QUFDRixLQWpsQkg7O0FBb2xCQSxXQUFRLGtCQUFrQixTQUFuQixHQUNILGFBREcsR0FFSCxJQUZKO0FBSUQsR0F0bUJEOztBQXdtQkEsSUFBRSxFQUFGLENBQUssTUFBTCxDQUFZLFFBQVosR0FBdUI7O0FBRXJCLFVBQWMsUUFGTztBQUdyQixlQUFjLFFBSE87O0FBS3JCLFlBQWMsS0FMTztBQU1yQixXQUFjLEtBTk87QUFPckIsYUFBYyxLQVBPO0FBUXJCLGlCQUFjLElBUk87OztBQVdyQixnQkFBYyxLQVhPOzs7QUFjckIsZUFBYyxLQWRPOzs7QUFpQnJCLGNBQWMsTUFqQk87OztBQW9CckIsWUFBYyxJQXBCTzs7O0FBdUJyQixnQkFBYyxNQXZCTzs7O0FBMEJyQixRQUFjLEtBMUJPOzs7QUE2QnJCLGFBQWMsTUE3Qk87OztBQWdDckIsY0FBYztBQUNaLFlBQU8sR0FESztBQUVaLFlBQU87QUFGSyxLQWhDTzs7QUFxQ3JCLGNBQWMsWUFBVSxDQUFFLENBckNMO0FBc0NyQixZQUFjLFlBQVUsQ0FBRSxDQXRDTDtBQXVDckIsWUFBYyxZQUFVLENBQUUsQ0F2Q0w7O0FBeUNyQixXQUFVO0FBQ1IsY0FBVztBQURILEtBekNXOztBQTZDckIsZUFBWTtBQUNWLGNBQWEsUUFESDtBQUVWLGlCQUFhLFdBRkg7QUFHVixnQkFBYSxVQUhIO0FBSVYsY0FBYSxRQUpIO0FBS1YsY0FBYSxRQUxIO0FBTVYsZ0JBQWEsVUFOSDtBQU9WLFlBQWEsTUFQSDtBQVFWLGtCQUFhLE1BUkg7QUFTVixZQUFhO0FBVEgsS0E3Q1M7O0FBeURyQixjQUFVO0FBQ1IsY0FBVyxjQURIO0FBRVIsZUFBVztBQUZILEtBekRXOztBQThEckIsY0FBVTtBQUNSLGNBQVEsWUFBVztBQUNsQixlQUFPLEVBQUUsU0FBRixFQUFhLElBQWIsQ0FBa0IsT0FBbEIsRUFBMkIsV0FBM0IsQ0FBUDtBQUNBO0FBSE87O0FBOURXLEdBQXZCO0FBc0VDLENBenJCQSxFQXlyQkcsTUF6ckJILEVBeXJCVyxNQXpyQlgsRUF5ckJtQixRQXpyQm5CIiwiZmlsZSI6ImRpbW1lci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxuICogIyBTZW1hbnRpYyBVSSAtIERpbW1lclxuICogaHR0cDovL2dpdGh1Yi5jb20vc2VtYW50aWMtb3JnL3NlbWFudGljLXVpL1xuICpcbiAqXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2VcbiAqIGh0dHA6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9NSVRcbiAqXG4gKi9cblxuOyhmdW5jdGlvbiAoJCwgd2luZG93LCBkb2N1bWVudCwgdW5kZWZpbmVkKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG53aW5kb3cgPSAodHlwZW9mIHdpbmRvdyAhPSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuTWF0aCA9PSBNYXRoKVxuICA/IHdpbmRvd1xuICA6ICh0eXBlb2Ygc2VsZiAhPSAndW5kZWZpbmVkJyAmJiBzZWxmLk1hdGggPT0gTWF0aClcbiAgICA/IHNlbGZcbiAgICA6IEZ1bmN0aW9uKCdyZXR1cm4gdGhpcycpKClcbjtcblxuJC5mbi5kaW1tZXIgPSBmdW5jdGlvbihwYXJhbWV0ZXJzKSB7XG4gIHZhclxuICAgICRhbGxNb2R1bGVzICAgICA9ICQodGhpcyksXG5cbiAgICB0aW1lICAgICAgICAgICAgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKSxcbiAgICBwZXJmb3JtYW5jZSAgICAgPSBbXSxcblxuICAgIHF1ZXJ5ICAgICAgICAgICA9IGFyZ3VtZW50c1swXSxcbiAgICBtZXRob2RJbnZva2VkICAgPSAodHlwZW9mIHF1ZXJ5ID09ICdzdHJpbmcnKSxcbiAgICBxdWVyeUFyZ3VtZW50cyAgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSksXG5cbiAgICByZXR1cm5lZFZhbHVlXG4gIDtcblxuICAkYWxsTW9kdWxlc1xuICAgIC5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgdmFyXG4gICAgICAgIHNldHRpbmdzICAgICAgICA9ICggJC5pc1BsYWluT2JqZWN0KHBhcmFtZXRlcnMpIClcbiAgICAgICAgICA/ICQuZXh0ZW5kKHRydWUsIHt9LCAkLmZuLmRpbW1lci5zZXR0aW5ncywgcGFyYW1ldGVycylcbiAgICAgICAgICA6ICQuZXh0ZW5kKHt9LCAkLmZuLmRpbW1lci5zZXR0aW5ncyksXG5cbiAgICAgICAgc2VsZWN0b3IgICAgICAgID0gc2V0dGluZ3Muc2VsZWN0b3IsXG4gICAgICAgIG5hbWVzcGFjZSAgICAgICA9IHNldHRpbmdzLm5hbWVzcGFjZSxcbiAgICAgICAgY2xhc3NOYW1lICAgICAgID0gc2V0dGluZ3MuY2xhc3NOYW1lLFxuICAgICAgICBlcnJvciAgICAgICAgICAgPSBzZXR0aW5ncy5lcnJvcixcblxuICAgICAgICBldmVudE5hbWVzcGFjZSAgPSAnLicgKyBuYW1lc3BhY2UsXG4gICAgICAgIG1vZHVsZU5hbWVzcGFjZSA9ICdtb2R1bGUtJyArIG5hbWVzcGFjZSxcbiAgICAgICAgbW9kdWxlU2VsZWN0b3IgID0gJGFsbE1vZHVsZXMuc2VsZWN0b3IgfHwgJycsXG5cbiAgICAgICAgY2xpY2tFdmVudCAgICAgID0gKCdvbnRvdWNoc3RhcnQnIGluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudClcbiAgICAgICAgICA/ICd0b3VjaHN0YXJ0J1xuICAgICAgICAgIDogJ2NsaWNrJyxcblxuICAgICAgICAkbW9kdWxlID0gJCh0aGlzKSxcbiAgICAgICAgJGRpbW1lcixcbiAgICAgICAgJGRpbW1hYmxlLFxuXG4gICAgICAgIGVsZW1lbnQgICA9IHRoaXMsXG4gICAgICAgIGluc3RhbmNlICA9ICRtb2R1bGUuZGF0YShtb2R1bGVOYW1lc3BhY2UpLFxuICAgICAgICBtb2R1bGVcbiAgICAgIDtcblxuICAgICAgbW9kdWxlID0ge1xuXG4gICAgICAgIHByZWluaXRpYWxpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCBtb2R1bGUuaXMuZGltbWVyKCkgKSB7XG5cbiAgICAgICAgICAgICRkaW1tYWJsZSA9ICRtb2R1bGUucGFyZW50KCk7XG4gICAgICAgICAgICAkZGltbWVyICAgPSAkbW9kdWxlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICRkaW1tYWJsZSA9ICRtb2R1bGU7XG4gICAgICAgICAgICBpZiggbW9kdWxlLmhhcy5kaW1tZXIoKSApIHtcbiAgICAgICAgICAgICAgaWYoc2V0dGluZ3MuZGltbWVyTmFtZSkge1xuICAgICAgICAgICAgICAgICRkaW1tZXIgPSAkZGltbWFibGUuZmluZChzZWxlY3Rvci5kaW1tZXIpLmZpbHRlcignLicgKyBzZXR0aW5ncy5kaW1tZXJOYW1lKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAkZGltbWVyID0gJGRpbW1hYmxlLmZpbmQoc2VsZWN0b3IuZGltbWVyKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICRkaW1tZXIgPSBtb2R1bGUuY3JlYXRlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUuc2V0LnZhcmlhdGlvbigpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBpbml0aWFsaXplOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0luaXRpYWxpemluZyBkaW1tZXInLCBzZXR0aW5ncyk7XG5cbiAgICAgICAgICBtb2R1bGUuYmluZC5ldmVudHMoKTtcbiAgICAgICAgICBtb2R1bGUuc2V0LmRpbW1hYmxlKCk7XG4gICAgICAgICAgbW9kdWxlLmluc3RhbnRpYXRlKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5zdGFudGlhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTdG9yaW5nIGluc3RhbmNlIG9mIG1vZHVsZScsIG1vZHVsZSk7XG4gICAgICAgICAgaW5zdGFuY2UgPSBtb2R1bGU7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLmRhdGEobW9kdWxlTmFtZXNwYWNlLCBpbnN0YW5jZSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVzdHJveTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0Rlc3Ryb3lpbmcgcHJldmlvdXMgbW9kdWxlJywgJGRpbW1lcik7XG4gICAgICAgICAgbW9kdWxlLnVuYmluZC5ldmVudHMoKTtcbiAgICAgICAgICBtb2R1bGUucmVtb3ZlLnZhcmlhdGlvbigpO1xuICAgICAgICAgICRkaW1tYWJsZVxuICAgICAgICAgICAgLm9mZihldmVudE5hbWVzcGFjZSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgYmluZDoge1xuICAgICAgICAgIGV2ZW50czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5vbiA9PSAnaG92ZXInKSB7XG4gICAgICAgICAgICAgICRkaW1tYWJsZVxuICAgICAgICAgICAgICAgIC5vbignbW91c2VlbnRlcicgKyBldmVudE5hbWVzcGFjZSwgbW9kdWxlLnNob3cpXG4gICAgICAgICAgICAgICAgLm9uKCdtb3VzZWxlYXZlJyArIGV2ZW50TmFtZXNwYWNlLCBtb2R1bGUuaGlkZSlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZihzZXR0aW5ncy5vbiA9PSAnY2xpY2snKSB7XG4gICAgICAgICAgICAgICRkaW1tYWJsZVxuICAgICAgICAgICAgICAgIC5vbihjbGlja0V2ZW50ICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS50b2dnbGUpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCBtb2R1bGUuaXMucGFnZSgpICkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NldHRpbmcgYXMgYSBwYWdlIGRpbW1lcicsICRkaW1tYWJsZSk7XG4gICAgICAgICAgICAgIG1vZHVsZS5zZXQucGFnZURpbW1lcigpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiggbW9kdWxlLmlzLmNsb3NhYmxlKCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdBZGRpbmcgZGltbWVyIGNsb3NlIGV2ZW50JywgJGRpbW1lcik7XG4gICAgICAgICAgICAgICRkaW1tYWJsZVxuICAgICAgICAgICAgICAgIC5vbihjbGlja0V2ZW50ICsgZXZlbnROYW1lc3BhY2UsIHNlbGVjdG9yLmRpbW1lciwgbW9kdWxlLmV2ZW50LmNsaWNrKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHVuYmluZDoge1xuICAgICAgICAgIGV2ZW50czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5yZW1vdmVEYXRhKG1vZHVsZU5hbWVzcGFjZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgICRkaW1tYWJsZVxuICAgICAgICAgICAgICAub2ZmKGV2ZW50TmFtZXNwYWNlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBldmVudDoge1xuICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0RldGVybWluaW5nIGlmIGV2ZW50IG9jY3VyZWQgb24gZGltbWVyJywgZXZlbnQpO1xuICAgICAgICAgICAgaWYoICRkaW1tZXIuZmluZChldmVudC50YXJnZXQpLmxlbmd0aCA9PT0gMCB8fCAkKGV2ZW50LnRhcmdldCkuaXMoc2VsZWN0b3IuY29udGVudCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5oaWRlKCk7XG4gICAgICAgICAgICAgIGV2ZW50LnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBhZGRDb250ZW50OiBmdW5jdGlvbihlbGVtZW50KSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICAkY29udGVudCA9ICQoZWxlbWVudClcbiAgICAgICAgICA7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdBZGQgY29udGVudCB0byBkaW1tZXInLCAkY29udGVudCk7XG4gICAgICAgICAgaWYoJGNvbnRlbnQucGFyZW50KClbMF0gIT09ICRkaW1tZXJbMF0pIHtcbiAgICAgICAgICAgICRjb250ZW50LmRldGFjaCgpLmFwcGVuZFRvKCRkaW1tZXIpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBjcmVhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgJGVsZW1lbnQgPSAkKCBzZXR0aW5ncy50ZW1wbGF0ZS5kaW1tZXIoKSApXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKHNldHRpbmdzLmRpbW1lck5hbWUpIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ3JlYXRpbmcgbmFtZWQgZGltbWVyJywgc2V0dGluZ3MuZGltbWVyTmFtZSk7XG4gICAgICAgICAgICAkZWxlbWVudC5hZGRDbGFzcyhzZXR0aW5ncy5kaW1tZXJOYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgJGVsZW1lbnRcbiAgICAgICAgICAgIC5hcHBlbmRUbygkZGltbWFibGUpXG4gICAgICAgICAgO1xuICAgICAgICAgIHJldHVybiAkZWxlbWVudDtcbiAgICAgICAgfSxcblxuICAgICAgICBzaG93OiBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICAgIGNhbGxiYWNrID0gJC5pc0Z1bmN0aW9uKGNhbGxiYWNrKVxuICAgICAgICAgICAgPyBjYWxsYmFja1xuICAgICAgICAgICAgOiBmdW5jdGlvbigpe31cbiAgICAgICAgICA7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdTaG93aW5nIGRpbW1lcicsICRkaW1tZXIsIHNldHRpbmdzKTtcbiAgICAgICAgICBpZiggKCFtb2R1bGUuaXMuZGltbWVkKCkgfHwgbW9kdWxlLmlzLmFuaW1hdGluZygpKSAmJiBtb2R1bGUuaXMuZW5hYmxlZCgpICkge1xuICAgICAgICAgICAgbW9kdWxlLmFuaW1hdGUuc2hvdyhjYWxsYmFjayk7XG4gICAgICAgICAgICBzZXR0aW5ncy5vblNob3cuY2FsbChlbGVtZW50KTtcbiAgICAgICAgICAgIHNldHRpbmdzLm9uQ2hhbmdlLmNhbGwoZWxlbWVudCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdEaW1tZXIgaXMgYWxyZWFkeSBzaG93biBvciBkaXNhYmxlZCcpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBoaWRlOiBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICAgIGNhbGxiYWNrID0gJC5pc0Z1bmN0aW9uKGNhbGxiYWNrKVxuICAgICAgICAgICAgPyBjYWxsYmFja1xuICAgICAgICAgICAgOiBmdW5jdGlvbigpe31cbiAgICAgICAgICA7XG4gICAgICAgICAgaWYoIG1vZHVsZS5pcy5kaW1tZWQoKSB8fCBtb2R1bGUuaXMuYW5pbWF0aW5nKCkgKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0hpZGluZyBkaW1tZXInLCAkZGltbWVyKTtcbiAgICAgICAgICAgIG1vZHVsZS5hbmltYXRlLmhpZGUoY2FsbGJhY2spO1xuICAgICAgICAgICAgc2V0dGluZ3Mub25IaWRlLmNhbGwoZWxlbWVudCk7XG4gICAgICAgICAgICBzZXR0aW5ncy5vbkNoYW5nZS5jYWxsKGVsZW1lbnQpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRGltbWVyIGlzIG5vdCB2aXNpYmxlJyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHRvZ2dsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1RvZ2dsaW5nIGRpbW1lciB2aXNpYmlsaXR5JywgJGRpbW1lcik7XG4gICAgICAgICAgaWYoICFtb2R1bGUuaXMuZGltbWVkKCkgKSB7XG4gICAgICAgICAgICBtb2R1bGUuc2hvdygpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIG1vZHVsZS5oaWRlKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGFuaW1hdGU6IHtcbiAgICAgICAgICBzaG93OiBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICAgICAgY2FsbGJhY2sgPSAkLmlzRnVuY3Rpb24oY2FsbGJhY2spXG4gICAgICAgICAgICAgID8gY2FsbGJhY2tcbiAgICAgICAgICAgICAgOiBmdW5jdGlvbigpe31cbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnVzZUNTUyAmJiAkLmZuLnRyYW5zaXRpb24gIT09IHVuZGVmaW5lZCAmJiAkZGltbWVyLnRyYW5zaXRpb24oJ2lzIHN1cHBvcnRlZCcpKSB7XG4gICAgICAgICAgICAgIGlmKHNldHRpbmdzLm9wYWNpdHkgIT09ICdhdXRvJykge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5zZXQub3BhY2l0eSgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICRkaW1tZXJcbiAgICAgICAgICAgICAgICAudHJhbnNpdGlvbih7XG4gICAgICAgICAgICAgICAgICBhbmltYXRpb24gICA6IHNldHRpbmdzLnRyYW5zaXRpb24gKyAnIGluJyxcbiAgICAgICAgICAgICAgICAgIHF1ZXVlICAgICAgIDogZmFsc2UsXG4gICAgICAgICAgICAgICAgICBkdXJhdGlvbiAgICA6IG1vZHVsZS5nZXQuZHVyYXRpb24oKSxcbiAgICAgICAgICAgICAgICAgIHVzZUZhaWxTYWZlIDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgIG9uU3RhcnQgICAgIDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vZHVsZS5zZXQuZGltbWVkKCk7XG4gICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgb25Db21wbGV0ZSAgOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgbW9kdWxlLnNldC5hY3RpdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1Nob3dpbmcgZGltbWVyIGFuaW1hdGlvbiB3aXRoIGphdmFzY3JpcHQnKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5kaW1tZWQoKTtcbiAgICAgICAgICAgICAgaWYoc2V0dGluZ3Mub3BhY2l0eSA9PSAnYXV0bycpIHtcbiAgICAgICAgICAgICAgICBzZXR0aW5ncy5vcGFjaXR5ID0gMC44O1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICRkaW1tZXJcbiAgICAgICAgICAgICAgICAuc3RvcCgpXG4gICAgICAgICAgICAgICAgLmNzcyh7XG4gICAgICAgICAgICAgICAgICBvcGFjaXR5IDogMCxcbiAgICAgICAgICAgICAgICAgIHdpZHRoICAgOiAnMTAwJScsXG4gICAgICAgICAgICAgICAgICBoZWlnaHQgIDogJzEwMCUnXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuZmFkZVRvKG1vZHVsZS5nZXQuZHVyYXRpb24oKSwgc2V0dGluZ3Mub3BhY2l0eSwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAkZGltbWVyLnJlbW92ZUF0dHIoJ3N0eWxlJyk7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuc2V0LmFjdGl2ZSgpO1xuICAgICAgICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBoaWRlOiBmdW5jdGlvbihjYWxsYmFjaykge1xuICAgICAgICAgICAgY2FsbGJhY2sgPSAkLmlzRnVuY3Rpb24oY2FsbGJhY2spXG4gICAgICAgICAgICAgID8gY2FsbGJhY2tcbiAgICAgICAgICAgICAgOiBmdW5jdGlvbigpe31cbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnVzZUNTUyAmJiAkLmZuLnRyYW5zaXRpb24gIT09IHVuZGVmaW5lZCAmJiAkZGltbWVyLnRyYW5zaXRpb24oJ2lzIHN1cHBvcnRlZCcpKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdIaWRpbmcgZGltbWVyIHdpdGggY3NzJyk7XG4gICAgICAgICAgICAgICRkaW1tZXJcbiAgICAgICAgICAgICAgICAudHJhbnNpdGlvbih7XG4gICAgICAgICAgICAgICAgICBhbmltYXRpb24gICA6IHNldHRpbmdzLnRyYW5zaXRpb24gKyAnIG91dCcsXG4gICAgICAgICAgICAgICAgICBxdWV1ZSAgICAgICA6IGZhbHNlLFxuICAgICAgICAgICAgICAgICAgZHVyYXRpb24gICAgOiBtb2R1bGUuZ2V0LmR1cmF0aW9uKCksXG4gICAgICAgICAgICAgICAgICB1c2VGYWlsU2FmZSA6IHRydWUsXG4gICAgICAgICAgICAgICAgICBvblN0YXJ0ICAgICA6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUucmVtb3ZlLmRpbW1lZCgpO1xuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIG9uQ29tcGxldGUgIDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUuYWN0aXZlKCk7XG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdIaWRpbmcgZGltbWVyIHdpdGggamF2YXNjcmlwdCcpO1xuICAgICAgICAgICAgICBtb2R1bGUucmVtb3ZlLmRpbW1lZCgpO1xuICAgICAgICAgICAgICAkZGltbWVyXG4gICAgICAgICAgICAgICAgLnN0b3AoKVxuICAgICAgICAgICAgICAgIC5mYWRlT3V0KG1vZHVsZS5nZXQuZHVyYXRpb24oKSwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUucmVtb3ZlLmFjdGl2ZSgpO1xuICAgICAgICAgICAgICAgICAgJGRpbW1lci5yZW1vdmVBdHRyKCdzdHlsZScpO1xuICAgICAgICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGdldDoge1xuICAgICAgICAgIGRpbW1lcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJGRpbW1lcjtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGR1cmF0aW9uOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKHR5cGVvZiBzZXR0aW5ncy5kdXJhdGlvbiA9PSAnb2JqZWN0Jykge1xuICAgICAgICAgICAgICBpZiggbW9kdWxlLmlzLmFjdGl2ZSgpICkge1xuICAgICAgICAgICAgICAgIHJldHVybiBzZXR0aW5ncy5kdXJhdGlvbi5oaWRlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiBzZXR0aW5ncy5kdXJhdGlvbi5zaG93O1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gc2V0dGluZ3MuZHVyYXRpb247XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGhhczoge1xuICAgICAgICAgIGRpbW1lcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5kaW1tZXJOYW1lKSB7XG4gICAgICAgICAgICAgIHJldHVybiAoJG1vZHVsZS5maW5kKHNlbGVjdG9yLmRpbW1lcikuZmlsdGVyKCcuJyArIHNldHRpbmdzLmRpbW1lck5hbWUpLmxlbmd0aCA+IDApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHJldHVybiAoICRtb2R1bGUuZmluZChzZWxlY3Rvci5kaW1tZXIpLmxlbmd0aCA+IDAgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgaXM6IHtcbiAgICAgICAgICBhY3RpdmU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRkaW1tZXIuaGFzQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBhbmltYXRpbmc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICggJGRpbW1lci5pcygnOmFuaW1hdGVkJykgfHwgJGRpbW1lci5oYXNDbGFzcyhjbGFzc05hbWUuYW5pbWF0aW5nKSApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgY2xvc2FibGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MuY2xvc2FibGUgPT0gJ2F1dG8nKSB7XG4gICAgICAgICAgICAgIGlmKHNldHRpbmdzLm9uID09ICdob3ZlcicpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gc2V0dGluZ3MuY2xvc2FibGU7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaW1tZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRtb2R1bGUuaGFzQ2xhc3MoY2xhc3NOYW1lLmRpbW1lcik7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaW1tYWJsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5oYXNDbGFzcyhjbGFzc05hbWUuZGltbWFibGUpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZGltbWVkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkZGltbWFibGUuaGFzQ2xhc3MoY2xhc3NOYW1lLmRpbW1lZCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXNhYmxlZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJGRpbW1hYmxlLmhhc0NsYXNzKGNsYXNzTmFtZS5kaXNhYmxlZCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBlbmFibGVkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAhbW9kdWxlLmlzLmRpc2FibGVkKCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBwYWdlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gJGRpbW1hYmxlLmlzKCdib2R5Jyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBwYWdlRGltbWVyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkZGltbWVyLmhhc0NsYXNzKGNsYXNzTmFtZS5wYWdlRGltbWVyKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2FuOiB7XG4gICAgICAgICAgc2hvdzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gISRkaW1tZXIuaGFzQ2xhc3MoY2xhc3NOYW1lLmRpc2FibGVkKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0OiB7XG4gICAgICAgICAgb3BhY2l0eTogZnVuY3Rpb24ob3BhY2l0eSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGNvbG9yICAgICAgPSAkZGltbWVyLmNzcygnYmFja2dyb3VuZC1jb2xvcicpLFxuICAgICAgICAgICAgICBjb2xvckFycmF5ID0gY29sb3Iuc3BsaXQoJywnKSxcbiAgICAgICAgICAgICAgaXNSR0IgICAgICA9IChjb2xvckFycmF5ICYmIGNvbG9yQXJyYXkubGVuZ3RoID09IDMpLFxuICAgICAgICAgICAgICBpc1JHQkEgICAgID0gKGNvbG9yQXJyYXkgJiYgY29sb3JBcnJheS5sZW5ndGggPT0gNClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIG9wYWNpdHkgICAgPSBzZXR0aW5ncy5vcGFjaXR5ID09PSAwID8gMCA6IHNldHRpbmdzLm9wYWNpdHkgfHwgb3BhY2l0eTtcbiAgICAgICAgICAgIGlmKGlzUkdCIHx8IGlzUkdCQSkge1xuICAgICAgICAgICAgICBjb2xvckFycmF5WzNdID0gb3BhY2l0eSArICcpJztcbiAgICAgICAgICAgICAgY29sb3IgICAgICAgICA9IGNvbG9yQXJyYXkuam9pbignLCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIGNvbG9yID0gJ3JnYmEoMCwgMCwgMCwgJyArIG9wYWNpdHkgKyAnKSc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NldHRpbmcgb3BhY2l0eSB0bycsIG9wYWNpdHkpO1xuICAgICAgICAgICAgJGRpbW1lci5jc3MoJ2JhY2tncm91bmQtY29sb3InLCBjb2xvcik7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBhY3RpdmU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJGRpbW1lci5hZGRDbGFzcyhjbGFzc05hbWUuYWN0aXZlKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpbW1hYmxlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRkaW1tYWJsZS5hZGRDbGFzcyhjbGFzc05hbWUuZGltbWFibGUpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZGltbWVkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRkaW1tYWJsZS5hZGRDbGFzcyhjbGFzc05hbWUuZGltbWVkKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHBhZ2VEaW1tZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJGRpbW1lci5hZGRDbGFzcyhjbGFzc05hbWUucGFnZURpbW1lcik7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXNhYmxlZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkZGltbWVyLmFkZENsYXNzKGNsYXNzTmFtZS5kaXNhYmxlZCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB2YXJpYXRpb246IGZ1bmN0aW9uKHZhcmlhdGlvbikge1xuICAgICAgICAgICAgdmFyaWF0aW9uID0gdmFyaWF0aW9uIHx8IHNldHRpbmdzLnZhcmlhdGlvbjtcbiAgICAgICAgICAgIGlmKHZhcmlhdGlvbikge1xuICAgICAgICAgICAgICAkZGltbWVyLmFkZENsYXNzKHZhcmlhdGlvbik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlbW92ZToge1xuICAgICAgICAgIGFjdGl2ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkZGltbWVyXG4gICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuYWN0aXZlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZGltbWVkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRkaW1tYWJsZS5yZW1vdmVDbGFzcyhjbGFzc05hbWUuZGltbWVkKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc2FibGVkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRkaW1tZXIucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmRpc2FibGVkKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHZhcmlhdGlvbjogZnVuY3Rpb24odmFyaWF0aW9uKSB7XG4gICAgICAgICAgICB2YXJpYXRpb24gPSB2YXJpYXRpb24gfHwgc2V0dGluZ3MudmFyaWF0aW9uO1xuICAgICAgICAgICAgaWYodmFyaWF0aW9uKSB7XG4gICAgICAgICAgICAgICRkaW1tZXIucmVtb3ZlQ2xhc3ModmFyaWF0aW9uKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0dGluZzogZnVuY3Rpb24obmFtZSwgdmFsdWUpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NoYW5naW5nIHNldHRpbmcnLCBuYW1lLCB2YWx1ZSk7XG4gICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChuYW1lKSApIHtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIHNldHRpbmdzLCBuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBpZigkLmlzUGxhaW5PYmplY3Qoc2V0dGluZ3NbbmFtZV0pKSB7XG4gICAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIHNldHRpbmdzW25hbWVdLCB2YWx1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgc2V0dGluZ3NbbmFtZV0gPSB2YWx1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gc2V0dGluZ3NbbmFtZV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnRlcm5hbDogZnVuY3Rpb24obmFtZSwgdmFsdWUpIHtcbiAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgbW9kdWxlLCBuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBtb2R1bGVbbmFtZV0gPSB2YWx1ZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZGVidWc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1Zy5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdmVyYm9zZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy52ZXJib3NlICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UubG9nKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBlcnJvcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCkge1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmVycm9yLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvci5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgcGVyZm9ybWFuY2U6IHtcbiAgICAgICAgICBsb2c6IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBjdXJyZW50VGltZSxcbiAgICAgICAgICAgICAgZXhlY3V0aW9uVGltZSxcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBjdXJyZW50VGltZSAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG4gICAgICAgICAgICAgIHByZXZpb3VzVGltZSAgPSB0aW1lIHx8IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lID0gY3VycmVudFRpbWUgLSBwcmV2aW91c1RpbWU7XG4gICAgICAgICAgICAgIHRpbWUgICAgICAgICAgPSBjdXJyZW50VGltZTtcbiAgICAgICAgICAgICAgcGVyZm9ybWFuY2UucHVzaCh7XG4gICAgICAgICAgICAgICAgJ05hbWUnICAgICAgICAgICA6IG1lc3NhZ2VbMF0sXG4gICAgICAgICAgICAgICAgJ0FyZ3VtZW50cycgICAgICA6IFtdLnNsaWNlLmNhbGwobWVzc2FnZSwgMSkgfHwgJycsXG4gICAgICAgICAgICAgICAgJ0VsZW1lbnQnICAgICAgICA6IGVsZW1lbnQsXG4gICAgICAgICAgICAgICAgJ0V4ZWN1dGlvbiBUaW1lJyA6IGV4ZWN1dGlvblRpbWVcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lciA9IHNldFRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLmRpc3BsYXksIDUwMCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXNwbGF5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICB0aXRsZSA9IHNldHRpbmdzLm5hbWUgKyAnOicsXG4gICAgICAgICAgICAgIHRvdGFsVGltZSA9IDBcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHRpbWUgPSBmYWxzZTtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICB0b3RhbFRpbWUgKz0gZGF0YVsnRXhlY3V0aW9uIFRpbWUnXTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgdGl0bGUgKz0gJyAnICsgdG90YWxUaW1lICsgJ21zJztcbiAgICAgICAgICAgIGlmKG1vZHVsZVNlbGVjdG9yKSB7XG4gICAgICAgICAgICAgIHRpdGxlICs9ICcgXFwnJyArIG1vZHVsZVNlbGVjdG9yICsgJ1xcJyc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZigkYWxsTW9kdWxlcy5sZW5ndGggPiAxKSB7XG4gICAgICAgICAgICAgIHRpdGxlICs9ICcgJyArICcoJyArICRhbGxNb2R1bGVzLmxlbmd0aCArICcpJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCAoY29uc29sZS5ncm91cCAhPT0gdW5kZWZpbmVkIHx8IGNvbnNvbGUudGFibGUgIT09IHVuZGVmaW5lZCkgJiYgcGVyZm9ybWFuY2UubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKHRpdGxlKTtcbiAgICAgICAgICAgICAgaWYoY29uc29sZS50YWJsZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUudGFibGUocGVyZm9ybWFuY2UpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGFbJ05hbWUnXSArICc6ICcgKyBkYXRhWydFeGVjdXRpb24gVGltZSddKydtcycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHBlcmZvcm1hbmNlID0gW107XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnZva2U6IGZ1bmN0aW9uKHF1ZXJ5LCBwYXNzZWRBcmd1bWVudHMsIGNvbnRleHQpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIG9iamVjdCA9IGluc3RhbmNlLFxuICAgICAgICAgICAgbWF4RGVwdGgsXG4gICAgICAgICAgICBmb3VuZCxcbiAgICAgICAgICAgIHJlc3BvbnNlXG4gICAgICAgICAgO1xuICAgICAgICAgIHBhc3NlZEFyZ3VtZW50cyA9IHBhc3NlZEFyZ3VtZW50cyB8fCBxdWVyeUFyZ3VtZW50cztcbiAgICAgICAgICBjb250ZXh0ICAgICAgICAgPSBlbGVtZW50ICAgICAgICAgfHwgY29udGV4dDtcbiAgICAgICAgICBpZih0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycgJiYgb2JqZWN0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5ICAgID0gcXVlcnkuc3BsaXQoL1tcXC4gXS8pO1xuICAgICAgICAgICAgbWF4RGVwdGggPSBxdWVyeS5sZW5ndGggLSAxO1xuICAgICAgICAgICAgJC5lYWNoKHF1ZXJ5LCBmdW5jdGlvbihkZXB0aCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgdmFyIGNhbWVsQ2FzZVZhbHVlID0gKGRlcHRoICE9IG1heERlcHRoKVxuICAgICAgICAgICAgICAgID8gdmFsdWUgKyBxdWVyeVtkZXB0aCArIDFdLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcXVlcnlbZGVwdGggKyAxXS5zbGljZSgxKVxuICAgICAgICAgICAgICAgIDogcXVlcnlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbY2FtZWxDYXNlVmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoICQuaXNQbGFpbk9iamVjdCggb2JqZWN0W3ZhbHVlXSApICYmIChkZXB0aCAhPSBtYXhEZXB0aCkgKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBvYmplY3RbdmFsdWVdICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3IubWV0aG9kLCBxdWVyeSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCAkLmlzRnVuY3Rpb24oIGZvdW5kICkgKSB7XG4gICAgICAgICAgICByZXNwb25zZSA9IGZvdW5kLmFwcGx5KGNvbnRleHQsIHBhc3NlZEFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYoZm91bmQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZDtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoJC5pc0FycmF5KHJldHVybmVkVmFsdWUpKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlLnB1c2gocmVzcG9uc2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IFtyZXR1cm5lZFZhbHVlLCByZXNwb25zZV07XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYocmVzcG9uc2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IHJlc3BvbnNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gZm91bmQ7XG4gICAgICAgIH1cbiAgICAgIH07XG5cbiAgICAgIG1vZHVsZS5wcmVpbml0aWFsaXplKCk7XG5cbiAgICAgIGlmKG1ldGhvZEludm9rZWQpIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICAgIH1cbiAgICAgICAgbW9kdWxlLmludm9rZShxdWVyeSk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIGluc3RhbmNlLmludm9rZSgnZGVzdHJveScpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICB9XG4gICAgfSlcbiAgO1xuXG4gIHJldHVybiAocmV0dXJuZWRWYWx1ZSAhPT0gdW5kZWZpbmVkKVxuICAgID8gcmV0dXJuZWRWYWx1ZVxuICAgIDogdGhpc1xuICA7XG59O1xuXG4kLmZuLmRpbW1lci5zZXR0aW5ncyA9IHtcblxuICBuYW1lICAgICAgICA6ICdEaW1tZXInLFxuICBuYW1lc3BhY2UgICA6ICdkaW1tZXInLFxuXG4gIHNpbGVudCAgICAgIDogZmFsc2UsXG4gIGRlYnVnICAgICAgIDogZmFsc2UsXG4gIHZlcmJvc2UgICAgIDogZmFsc2UsXG4gIHBlcmZvcm1hbmNlIDogdHJ1ZSxcblxuICAvLyBuYW1lIHRvIGRpc3Rpbmd1aXNoIGJldHdlZW4gbXVsdGlwbGUgZGltbWVycyBpbiBjb250ZXh0XG4gIGRpbW1lck5hbWUgIDogZmFsc2UsXG5cbiAgLy8gd2hldGhlciB0byBhZGQgYSB2YXJpYXRpb24gdHlwZVxuICB2YXJpYXRpb24gICA6IGZhbHNlLFxuXG4gIC8vIHdoZXRoZXIgdG8gYmluZCBjbG9zZSBldmVudHNcbiAgY2xvc2FibGUgICAgOiAnYXV0bycsXG5cbiAgLy8gd2hldGhlciB0byB1c2UgY3NzIGFuaW1hdGlvbnNcbiAgdXNlQ1NTICAgICAgOiB0cnVlLFxuXG4gIC8vIGNzcyBhbmltYXRpb24gdG8gdXNlXG4gIHRyYW5zaXRpb24gIDogJ2ZhZGUnLFxuXG4gIC8vIGV2ZW50IHRvIGJpbmQgdG9cbiAgb24gICAgICAgICAgOiBmYWxzZSxcblxuICAvLyBvdmVycmlkaW5nIG9wYWNpdHkgdmFsdWVcbiAgb3BhY2l0eSAgICAgOiAnYXV0bycsXG5cbiAgLy8gdHJhbnNpdGlvbiBkdXJhdGlvbnNcbiAgZHVyYXRpb24gICAgOiB7XG4gICAgc2hvdyA6IDUwMCxcbiAgICBoaWRlIDogNTAwXG4gIH0sXG5cbiAgb25DaGFuZ2UgICAgOiBmdW5jdGlvbigpe30sXG4gIG9uU2hvdyAgICAgIDogZnVuY3Rpb24oKXt9LFxuICBvbkhpZGUgICAgICA6IGZ1bmN0aW9uKCl7fSxcblxuICBlcnJvciAgIDoge1xuICAgIG1ldGhvZCAgIDogJ1RoZSBtZXRob2QgeW91IGNhbGxlZCBpcyBub3QgZGVmaW5lZC4nXG4gIH0sXG5cbiAgY2xhc3NOYW1lIDoge1xuICAgIGFjdGl2ZSAgICAgOiAnYWN0aXZlJyxcbiAgICBhbmltYXRpbmcgIDogJ2FuaW1hdGluZycsXG4gICAgZGltbWFibGUgICA6ICdkaW1tYWJsZScsXG4gICAgZGltbWVkICAgICA6ICdkaW1tZWQnLFxuICAgIGRpbW1lciAgICAgOiAnZGltbWVyJyxcbiAgICBkaXNhYmxlZCAgIDogJ2Rpc2FibGVkJyxcbiAgICBoaWRlICAgICAgIDogJ2hpZGUnLFxuICAgIHBhZ2VEaW1tZXIgOiAncGFnZScsXG4gICAgc2hvdyAgICAgICA6ICdzaG93J1xuICB9LFxuXG4gIHNlbGVjdG9yOiB7XG4gICAgZGltbWVyICAgOiAnPiAudWkuZGltbWVyJyxcbiAgICBjb250ZW50ICA6ICcudWkuZGltbWVyID4gLmNvbnRlbnQsIC51aS5kaW1tZXIgPiAuY29udGVudCA+IC5jZW50ZXInXG4gIH0sXG5cbiAgdGVtcGxhdGU6IHtcbiAgICBkaW1tZXI6IGZ1bmN0aW9uKCkge1xuICAgICByZXR1cm4gJCgnPGRpdiAvPicpLmF0dHIoJ2NsYXNzJywgJ3VpIGRpbW1lcicpO1xuICAgIH1cbiAgfVxuXG59O1xuXG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7XG4iXX0=