/*!
 * # Semantic UI - Search
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.search = function (parameters) {
    var $allModules = $(this),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue;
    $(this).each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.search.settings, parameters) : $.extend({}, $.fn.search.settings),
          className = settings.className,
          metadata = settings.metadata,
          regExp = settings.regExp,
          fields = settings.fields,
          selector = settings.selector,
          error = settings.error,
          namespace = settings.namespace,
          eventNamespace = '.' + namespace,
          moduleNamespace = namespace + '-module',
          $module = $(this),
          $prompt = $module.find(selector.prompt),
          $searchButton = $module.find(selector.searchButton),
          $results = $module.find(selector.results),
          $result = $module.find(selector.result),
          $category = $module.find(selector.category),
          element = this,
          instance = $module.data(moduleNamespace),
          disabledBubbled = false,
          module;

      module = {

        initialize: function () {
          module.verbose('Initializing module');
          module.determine.searchFields();
          module.bind.events();
          module.set.type();
          module.create.results();
          module.instantiate();
        },
        instantiate: function () {
          module.verbose('Storing instance of module', module);
          instance = module;
          $module.data(moduleNamespace, module);
        },
        destroy: function () {
          module.verbose('Destroying instance');
          $module.off(eventNamespace).removeData(moduleNamespace);
        },

        refresh: function () {
          module.debug('Refreshing selector cache');
          $prompt = $module.find(selector.prompt);
          $searchButton = $module.find(selector.searchButton);
          $category = $module.find(selector.category);
          $results = $module.find(selector.results);
          $result = $module.find(selector.result);
        },

        refreshResults: function () {
          $results = $module.find(selector.results);
          $result = $module.find(selector.result);
        },

        bind: {
          events: function () {
            module.verbose('Binding events to search');
            if (settings.automatic) {
              $module.on(module.get.inputEvent() + eventNamespace, selector.prompt, module.event.input);
              $prompt.attr('autocomplete', 'off');
            }
            $module
            // prompt
            .on('focus' + eventNamespace, selector.prompt, module.event.focus).on('blur' + eventNamespace, selector.prompt, module.event.blur).on('keydown' + eventNamespace, selector.prompt, module.handleKeyboard)
            // search button
            .on('click' + eventNamespace, selector.searchButton, module.query)
            // results
            .on('mousedown' + eventNamespace, selector.results, module.event.result.mousedown).on('mouseup' + eventNamespace, selector.results, module.event.result.mouseup).on('click' + eventNamespace, selector.result, module.event.result.click);
          }
        },

        determine: {
          searchFields: function () {
            // this makes sure $.extend does not add specified search fields to default fields
            // this is the only setting which should not extend defaults
            if (parameters && parameters.searchFields !== undefined) {
              settings.searchFields = parameters.searchFields;
            }
          }
        },

        event: {
          input: function () {
            clearTimeout(module.timer);
            module.timer = setTimeout(module.query, settings.searchDelay);
          },
          focus: function () {
            module.set.focus();
            if (module.has.minimumCharacters()) {
              module.query();
              if (module.can.show()) {
                module.showResults();
              }
            }
          },
          blur: function (event) {
            var pageLostFocus = document.activeElement === this,
                callback = function () {
              module.cancel.query();
              module.remove.focus();
              module.timer = setTimeout(module.hideResults, settings.hideDelay);
            };
            if (pageLostFocus) {
              return;
            }
            if (module.resultsClicked) {
              module.debug('Determining if user action caused search to close');
              $module.one('click.close' + eventNamespace, selector.results, function (event) {
                if (module.is.inMessage(event) || disabledBubbled) {
                  $prompt.focus();
                  return;
                }
                disabledBubbled = false;
                if (!module.is.animating() && !module.is.hidden()) {
                  callback();
                }
              });
            } else {
              module.debug('Input blurred without user action, closing results');
              callback();
            }
          },
          result: {
            mousedown: function () {
              module.resultsClicked = true;
            },
            mouseup: function () {
              module.resultsClicked = false;
            },
            click: function (event) {
              module.debug('Search result selected');
              var $result = $(this),
                  $title = $result.find(selector.title).eq(0),
                  $link = $result.is('a[href]') ? $result : $result.find('a[href]').eq(0),
                  href = $link.attr('href') || false,
                  target = $link.attr('target') || false,
                  title = $title.html(),

              // title is used for result lookup
              value = $title.length > 0 ? $title.text() : false,
                  results = module.get.results(),
                  result = $result.data(metadata.result) || module.get.result(value, results),
                  returnedValue;
              if ($.isFunction(settings.onSelect)) {
                if (settings.onSelect.call(element, result, results) === false) {
                  module.debug('Custom onSelect callback cancelled default select action');
                  disabledBubbled = true;
                  return;
                }
              }
              module.hideResults();
              if (value) {
                module.set.value(value);
              }
              if (href) {
                module.verbose('Opening search link found in result', $link);
                if (target == '_blank' || event.ctrlKey) {
                  window.open(href);
                } else {
                  window.location.href = href;
                }
              }
            }
          }
        },
        handleKeyboard: function (event) {
          var
          // force selector refresh
          $result = $module.find(selector.result),
              $category = $module.find(selector.category),
              currentIndex = $result.index($result.filter('.' + className.active)),
              resultSize = $result.length,
              keyCode = event.which,
              keys = {
            backspace: 8,
            enter: 13,
            escape: 27,
            upArrow: 38,
            downArrow: 40
          },
              newIndex;
          // search shortcuts
          if (keyCode == keys.escape) {
            module.verbose('Escape key pressed, blurring search field');
            module.trigger.blur();
          }
          if (module.is.visible()) {
            if (keyCode == keys.enter) {
              module.verbose('Enter key pressed, selecting active result');
              if ($result.filter('.' + className.active).length > 0) {
                module.event.result.click.call($result.filter('.' + className.active), event);
                event.preventDefault();
                return false;
              }
            } else if (keyCode == keys.upArrow) {
              module.verbose('Up key pressed, changing active result');
              newIndex = currentIndex - 1 < 0 ? currentIndex : currentIndex - 1;
              $category.removeClass(className.active);
              $result.removeClass(className.active).eq(newIndex).addClass(className.active).closest($category).addClass(className.active);
              event.preventDefault();
            } else if (keyCode == keys.downArrow) {
              module.verbose('Down key pressed, changing active result');
              newIndex = currentIndex + 1 >= resultSize ? currentIndex : currentIndex + 1;
              $category.removeClass(className.active);
              $result.removeClass(className.active).eq(newIndex).addClass(className.active).closest($category).addClass(className.active);
              event.preventDefault();
            }
          } else {
            // query shortcuts
            if (keyCode == keys.enter) {
              module.verbose('Enter key pressed, executing query');
              module.query();
              module.set.buttonPressed();
              $prompt.one('keyup', module.remove.buttonFocus);
            }
          }
        },

        setup: {
          api: function (searchTerm) {
            var apiSettings = {
              debug: settings.debug,
              on: false,
              cache: true,
              action: 'search',
              urlData: {
                query: searchTerm
              },
              onSuccess: function (response) {
                module.parse.response.call(element, response, searchTerm);
              },
              onAbort: function (response) {},
              onFailure: function () {
                module.displayMessage(error.serverError);
              },
              onError: module.error
            },
                searchHTML;
            $.extend(true, apiSettings, settings.apiSettings);
            module.verbose('Setting up API request', apiSettings);
            $module.api(apiSettings);
          }
        },

        can: {
          useAPI: function () {
            return $.fn.api !== undefined;
          },
          show: function () {
            return module.is.focused() && !module.is.visible() && !module.is.empty();
          },
          transition: function () {
            return settings.transition && $.fn.transition !== undefined && $module.transition('is supported');
          }
        },

        is: {
          animating: function () {
            return $results.hasClass(className.animating);
          },
          hidden: function () {
            return $results.hasClass(className.hidden);
          },
          inMessage: function (event) {
            return event.target && $(event.target).closest(selector.message).length > 0;
          },
          empty: function () {
            return $results.html() === '';
          },
          visible: function () {
            return $results.filter(':visible').length > 0;
          },
          focused: function () {
            return $prompt.filter(':focus').length > 0;
          }
        },

        trigger: {
          blur: function () {
            var events = document.createEvent('HTMLEvents'),
                promptElement = $prompt[0];
            if (promptElement) {
              module.verbose('Triggering native blur event');
              events.initEvent('blur', false, false);
              promptElement.dispatchEvent(events);
            }
          }
        },

        get: {
          inputEvent: function () {
            var prompt = $prompt[0],
                inputEvent = prompt !== undefined && prompt.oninput !== undefined ? 'input' : prompt !== undefined && prompt.onpropertychange !== undefined ? 'propertychange' : 'keyup';
            return inputEvent;
          },
          value: function () {
            return $prompt.val();
          },
          results: function () {
            var results = $module.data(metadata.results);
            return results;
          },
          result: function (value, results) {
            var lookupFields = ['title', 'id'],
                result = false;
            value = value !== undefined ? value : module.get.value();
            results = results !== undefined ? results : module.get.results();
            if (settings.type === 'category') {
              module.debug('Finding result that matches', value);
              $.each(results, function (index, category) {
                if ($.isArray(category.results)) {
                  result = module.search.object(value, category.results, lookupFields)[0];
                  // don't continue searching if a result is found
                  if (result) {
                    return false;
                  }
                }
              });
            } else {
              module.debug('Finding result in results object', value);
              result = module.search.object(value, results, lookupFields)[0];
            }
            return result || false;
          }
        },

        select: {
          firstResult: function () {
            module.verbose('Selecting first result');
            $result.first().addClass(className.active);
          }
        },

        set: {
          focus: function () {
            $module.addClass(className.focus);
          },
          loading: function () {
            $module.addClass(className.loading);
          },
          value: function (value) {
            module.verbose('Setting search input value', value);
            $prompt.val(value);
          },
          type: function (type) {
            type = type || settings.type;
            if (settings.type == 'category') {
              $module.addClass(settings.type);
            }
          },
          buttonPressed: function () {
            $searchButton.addClass(className.pressed);
          }
        },

        remove: {
          loading: function () {
            $module.removeClass(className.loading);
          },
          focus: function () {
            $module.removeClass(className.focus);
          },
          buttonPressed: function () {
            $searchButton.removeClass(className.pressed);
          }
        },

        query: function () {
          var searchTerm = module.get.value(),
              cache = module.read.cache(searchTerm);
          if (module.has.minimumCharacters()) {
            if (cache) {
              module.debug('Reading result from cache', searchTerm);
              module.save.results(cache.results);
              module.addResults(cache.html);
              module.inject.id(cache.results);
            } else {
              module.debug('Querying for', searchTerm);
              if ($.isPlainObject(settings.source) || $.isArray(settings.source)) {
                module.search.local(searchTerm);
              } else if (module.can.useAPI()) {
                module.search.remote(searchTerm);
              } else {
                module.error(error.source);
              }
            }
            settings.onSearchQuery.call(element, searchTerm);
          } else {
            module.hideResults();
          }
        },

        search: {
          local: function (searchTerm) {
            var results = module.search.object(searchTerm, settings.content),
                searchHTML;
            module.set.loading();
            module.save.results(results);
            module.debug('Returned local search results', results);

            searchHTML = module.generateResults({
              results: results
            });
            module.remove.loading();
            module.addResults(searchHTML);
            module.inject.id(results);
            module.write.cache(searchTerm, {
              html: searchHTML,
              results: results
            });
          },
          remote: function (searchTerm) {
            if ($module.api('is loading')) {
              $module.api('abort');
            }
            module.setup.api(searchTerm);
            $module.api('query');
          },
          object: function (searchTerm, source, searchFields) {
            var results = [],
                fuzzyResults = [],
                searchExp = searchTerm.toString().replace(regExp.escape, '\\$&'),
                matchRegExp = new RegExp(regExp.beginsWith + searchExp, 'i'),


            // avoid duplicates when pushing results
            addResult = function (array, result) {
              var notResult = $.inArray(result, results) == -1,
                  notFuzzyResult = $.inArray(result, fuzzyResults) == -1;
              if (notResult && notFuzzyResult) {
                array.push(result);
              }
            };
            source = source || settings.source;
            searchFields = searchFields !== undefined ? searchFields : settings.searchFields;

            // search fields should be array to loop correctly
            if (!$.isArray(searchFields)) {
              searchFields = [searchFields];
            }

            // exit conditions if no source
            if (source === undefined || source === false) {
              module.error(error.source);
              return [];
            }

            // iterate through search fields looking for matches
            $.each(searchFields, function (index, field) {
              $.each(source, function (label, content) {
                var fieldExists = typeof content[field] == 'string';
                if (fieldExists) {
                  if (content[field].search(matchRegExp) !== -1) {
                    // content starts with value (first in results)
                    addResult(results, content);
                  } else if (settings.searchFullText && module.fuzzySearch(searchTerm, content[field])) {
                    // content fuzzy matches (last in results)
                    addResult(fuzzyResults, content);
                  }
                }
              });
            });
            return $.merge(results, fuzzyResults);
          }
        },

        fuzzySearch: function (query, term) {
          var termLength = term.length,
              queryLength = query.length;
          if (typeof query !== 'string') {
            return false;
          }
          query = query.toLowerCase();
          term = term.toLowerCase();
          if (queryLength > termLength) {
            return false;
          }
          if (queryLength === termLength) {
            return query === term;
          }
          search: for (var characterIndex = 0, nextCharacterIndex = 0; characterIndex < queryLength; characterIndex++) {
            var queryCharacter = query.charCodeAt(characterIndex);
            while (nextCharacterIndex < termLength) {
              if (term.charCodeAt(nextCharacterIndex++) === queryCharacter) {
                continue search;
              }
            }
            return false;
          }
          return true;
        },

        parse: {
          response: function (response, searchTerm) {
            var searchHTML = module.generateResults(response);
            module.verbose('Parsing server response', response);
            if (response !== undefined) {
              if (searchTerm !== undefined && response[fields.results] !== undefined) {
                module.addResults(searchHTML);
                module.inject.id(response[fields.results]);
                module.write.cache(searchTerm, {
                  html: searchHTML,
                  results: response[fields.results]
                });
                module.save.results(response[fields.results]);
              }
            }
          }
        },

        cancel: {
          query: function () {
            if (module.can.useAPI()) {
              $module.api('abort');
            }
          }
        },

        has: {
          minimumCharacters: function () {
            var searchTerm = module.get.value(),
                numCharacters = searchTerm.length;
            return numCharacters >= settings.minCharacters;
          }
        },

        clear: {
          cache: function (value) {
            var cache = $module.data(metadata.cache);
            if (!value) {
              module.debug('Clearing cache', value);
              $module.removeData(metadata.cache);
            } else if (value && cache && cache[value]) {
              module.debug('Removing value from cache', value);
              delete cache[value];
              $module.data(metadata.cache, cache);
            }
          }
        },

        read: {
          cache: function (name) {
            var cache = $module.data(metadata.cache);
            if (settings.cache) {
              module.verbose('Checking cache for generated html for query', name);
              return typeof cache == 'object' && cache[name] !== undefined ? cache[name] : false;
            }
            return false;
          }
        },

        create: {
          id: function (resultIndex, categoryIndex) {
            var resultID = resultIndex + 1,
                // not zero indexed
            categoryID = categoryIndex + 1,
                firstCharCode,
                letterID,
                id;
            if (categoryIndex !== undefined) {
              // start char code for "A"
              letterID = String.fromCharCode(97 + categoryIndex);
              id = letterID + resultID;
              module.verbose('Creating category result id', id);
            } else {
              id = resultID;
              module.verbose('Creating result id', id);
            }
            return id;
          },
          results: function () {
            if ($results.length === 0) {
              $results = $('<div />').addClass(className.results).appendTo($module);
            }
          }
        },

        inject: {
          result: function (result, resultIndex, categoryIndex) {
            module.verbose('Injecting result into results');
            var $selectedResult = categoryIndex !== undefined ? $results.children().eq(categoryIndex).children(selector.result).eq(resultIndex) : $results.children(selector.result).eq(resultIndex);
            module.verbose('Injecting results metadata', $selectedResult);
            $selectedResult.data(metadata.result, result);
          },
          id: function (results) {
            module.debug('Injecting unique ids into results');
            var
            // since results may be object, we must use counters
            categoryIndex = 0,
                resultIndex = 0;
            if (settings.type === 'category') {
              // iterate through each category result
              $.each(results, function (index, category) {
                resultIndex = 0;
                $.each(category.results, function (index, value) {
                  var result = category.results[index];
                  if (result.id === undefined) {
                    result.id = module.create.id(resultIndex, categoryIndex);
                  }
                  module.inject.result(result, resultIndex, categoryIndex);
                  resultIndex++;
                });
                categoryIndex++;
              });
            } else {
              // top level
              $.each(results, function (index, value) {
                var result = results[index];
                if (result.id === undefined) {
                  result.id = module.create.id(resultIndex);
                }
                module.inject.result(result, resultIndex);
                resultIndex++;
              });
            }
            return results;
          }
        },

        save: {
          results: function (results) {
            module.verbose('Saving current search results to metadata', results);
            $module.data(metadata.results, results);
          }
        },

        write: {
          cache: function (name, value) {
            var cache = $module.data(metadata.cache) !== undefined ? $module.data(metadata.cache) : {};
            if (settings.cache) {
              module.verbose('Writing generated html to cache', name, value);
              cache[name] = value;
              $module.data(metadata.cache, cache);
            }
          }
        },

        addResults: function (html) {
          if ($.isFunction(settings.onResultsAdd)) {
            if (settings.onResultsAdd.call($results, html) === false) {
              module.debug('onResultsAdd callback cancelled default action');
              return false;
            }
          }
          if (html) {
            $results.html(html);
            module.refreshResults();
            if (settings.selectFirstResult) {
              module.select.firstResult();
            }
            module.showResults();
          } else {
            module.hideResults();
          }
        },

        showResults: function () {
          if (!module.is.visible()) {
            if (module.can.transition()) {
              module.debug('Showing results with css animations');
              $results.transition({
                animation: settings.transition + ' in',
                debug: settings.debug,
                verbose: settings.verbose,
                duration: settings.duration,
                queue: true
              });
            } else {
              module.debug('Showing results with javascript');
              $results.stop().fadeIn(settings.duration, settings.easing);
            }
            settings.onResultsOpen.call($results);
          }
        },
        hideResults: function () {
          if (module.is.visible()) {
            if (module.can.transition()) {
              module.debug('Hiding results with css animations');
              $results.transition({
                animation: settings.transition + ' out',
                debug: settings.debug,
                verbose: settings.verbose,
                duration: settings.duration,
                queue: true
              });
            } else {
              module.debug('Hiding results with javascript');
              $results.stop().fadeOut(settings.duration, settings.easing);
            }
            settings.onResultsClose.call($results);
          }
        },

        generateResults: function (response) {
          module.debug('Generating html from response', response);
          var template = settings.templates[settings.type],
              isProperObject = $.isPlainObject(response[fields.results]) && !$.isEmptyObject(response[fields.results]),
              isProperArray = $.isArray(response[fields.results]) && response[fields.results].length > 0,
              html = '';
          if (isProperObject || isProperArray) {
            if (settings.maxResults > 0) {
              if (isProperObject) {
                if (settings.type == 'standard') {
                  module.error(error.maxResults);
                }
              } else {
                response[fields.results] = response[fields.results].slice(0, settings.maxResults);
              }
            }
            if ($.isFunction(template)) {
              html = template(response, fields);
            } else {
              module.error(error.noTemplate, false);
            }
          } else if (settings.showNoResults) {
            html = module.displayMessage(error.noResults, 'empty');
          }
          settings.onResults.call(element, response);
          return html;
        },

        displayMessage: function (text, type) {
          type = type || 'standard';
          module.debug('Displaying message', text, type);
          module.addResults(settings.templates.message(text, type));
          return settings.templates.message(text, type);
        },

        setting: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            settings[name] = value;
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ($allModules.length > 1) {
              title += ' ' + '(' + $allModules.length + ')';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };
      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.search.settings = {

    name: 'Search',
    namespace: 'search',

    silent: false,
    debug: false,
    verbose: false,
    performance: true,

    // template to use (specified in settings.templates)
    type: 'standard',

    // minimum characters required to search
    minCharacters: 1,

    // whether to select first result after searching automatically
    selectFirstResult: false,

    // API config
    apiSettings: false,

    // object to search
    source: false,

    // fields to search
    searchFields: ['title', 'description'],

    // field to display in standard results template
    displayField: '',

    // whether to include fuzzy results in local search
    searchFullText: true,

    // whether to add events to prompt automatically
    automatic: true,

    // delay before hiding menu after blur
    hideDelay: 0,

    // delay before searching
    searchDelay: 200,

    // maximum results returned from local
    maxResults: 7,

    // whether to store lookups in local cache
    cache: true,

    // whether no results errors should be shown
    showNoResults: true,

    // transition settings
    transition: 'scale',
    duration: 200,
    easing: 'easeOutExpo',

    // callbacks
    onSelect: false,
    onResultsAdd: false,

    onSearchQuery: function (query) {},
    onResults: function (response) {},

    onResultsOpen: function () {},
    onResultsClose: function () {},

    className: {
      animating: 'animating',
      active: 'active',
      empty: 'empty',
      focus: 'focus',
      hidden: 'hidden',
      loading: 'loading',
      results: 'results',
      pressed: 'down'
    },

    error: {
      source: 'Cannot search. No source used, and Semantic API module was not included',
      noResults: 'Your search returned no results',
      logging: 'Error in debug logging, exiting.',
      noEndpoint: 'No search endpoint was specified',
      noTemplate: 'A valid template name was not specified.',
      serverError: 'There was an issue querying the server.',
      maxResults: 'Results must be an array to use maxResults setting',
      method: 'The method you called is not defined.'
    },

    metadata: {
      cache: 'cache',
      results: 'results',
      result: 'result'
    },

    regExp: {
      escape: /[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g,
      beginsWith: '(?:\s|^)'
    },

    // maps api response attributes to internal representation
    fields: {
      categories: 'results', // array of categories (category view)
      categoryName: 'name', // name of category (category view)
      categoryResults: 'results', // array of results (category view)
      description: 'description', // result description
      image: 'image', // result image
      price: 'price', // result price
      results: 'results', // array of results (standard)
      title: 'title', // result title
      url: 'url', // result url
      action: 'action', // "view more" object name
      actionText: 'text', // "view more" text
      actionURL: 'url' // "view more" url
    },

    selector: {
      prompt: '.prompt',
      searchButton: '.search.button',
      results: '.results',
      message: '.results > .message',
      category: '.category',
      result: '.result',
      title: '.title, .name'
    },

    templates: {
      escape: function (string) {
        var badChars = /[&<>"'`]/g,
            shouldEscape = /[&<>"'`]/,
            escape = {
          "&": "&amp;",
          "<": "&lt;",
          ">": "&gt;",
          '"': "&quot;",
          "'": "&#x27;",
          "`": "&#x60;"
        },
            escapedChar = function (chr) {
          return escape[chr];
        };
        if (shouldEscape.test(string)) {
          return string.replace(badChars, escapedChar);
        }
        return string;
      },
      message: function (message, type) {
        var html = '';
        if (message !== undefined && type !== undefined) {
          html += '' + '<div class="message ' + type + '">';
          // message type
          if (type == 'empty') {
            html += '' + '<div class="header">No Results</div class="header">' + '<div class="description">' + message + '</div class="description">';
          } else {
            html += ' <div class="description">' + message + '</div>';
          }
          html += '</div>';
        }
        return html;
      },
      category: function (response, fields) {
        var html = '',
            escape = $.fn.search.settings.templates.escape;
        if (response[fields.categoryResults] !== undefined) {

          // each category
          $.each(response[fields.categoryResults], function (index, category) {
            if (category[fields.results] !== undefined && category.results.length > 0) {

              html += '<div class="category">';

              if (category[fields.categoryName] !== undefined) {
                html += '<div class="name">' + category[fields.categoryName] + '</div>';
              }

              // each item inside category
              $.each(category.results, function (index, result) {
                if (result[fields.url]) {
                  html += '<a class="result" href="' + result[fields.url] + '">';
                } else {
                  html += '<a class="result">';
                }
                if (result[fields.image] !== undefined) {
                  html += '' + '<div class="image">' + ' <img src="' + result[fields.image] + '">' + '</div>';
                }
                html += '<div class="content">';
                if (result[fields.price] !== undefined) {
                  html += '<div class="price">' + result[fields.price] + '</div>';
                }
                if (result[fields.title] !== undefined) {
                  html += '<div class="title">' + result[fields.title] + '</div>';
                }
                if (result[fields.description] !== undefined) {
                  html += '<div class="description">' + result[fields.description] + '</div>';
                }
                html += '' + '</div>';
                html += '</a>';
              });
              html += '' + '</div>';
            }
          });
          if (response[fields.action]) {
            html += '' + '<a href="' + response[fields.action][fields.actionURL] + '" class="action">' + response[fields.action][fields.actionText] + '</a>';
          }
          return html;
        }
        return false;
      },
      standard: function (response, fields) {
        var html = '';
        if (response[fields.results] !== undefined) {

          // each result
          $.each(response[fields.results], function (index, result) {
            if (result[fields.url]) {
              html += '<a class="result" href="' + result[fields.url] + '">';
            } else {
              html += '<a class="result">';
            }
            if (result[fields.image] !== undefined) {
              html += '' + '<div class="image">' + ' <img src="' + result[fields.image] + '">' + '</div>';
            }
            html += '<div class="content">';
            if (result[fields.price] !== undefined) {
              html += '<div class="price">' + result[fields.price] + '</div>';
            }
            if (result[fields.title] !== undefined) {
              html += '<div class="title">' + result[fields.title] + '</div>';
            }
            if (result[fields.description] !== undefined) {
              html += '<div class="description">' + result[fields.description] + '</div>';
            }
            html += '' + '</div>';
            html += '</a>';
          });

          if (response[fields.action]) {
            html += '' + '<a href="' + response[fields.action][fields.actionURL] + '" class="action">' + response[fields.action][fields.actionText] + '</a>';
          }
          return html;
        }
        return false;
      }
    }
  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL3NlYXJjaC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBVUEsQ0FBQyxDQUFDLFVBQVUsQ0FBVixFQUFhLE1BQWIsRUFBcUIsUUFBckIsRUFBK0IsU0FBL0IsRUFBMEM7O0FBRTVDOztBQUVBLFdBQVUsT0FBTyxNQUFQLElBQWlCLFdBQWpCLElBQWdDLE9BQU8sSUFBUCxJQUFlLElBQWhELEdBQ0wsTUFESyxHQUVKLE9BQU8sSUFBUCxJQUFlLFdBQWYsSUFBOEIsS0FBSyxJQUFMLElBQWEsSUFBNUMsR0FDRSxJQURGLEdBRUUsU0FBUyxhQUFULEdBSk47O0FBT0EsSUFBRSxFQUFGLENBQUssTUFBTCxHQUFjLFVBQVMsVUFBVCxFQUFxQjtBQUNqQyxRQUNFLGNBQWtCLEVBQUUsSUFBRixDQURwQjtBQUFBLFFBRUUsaUJBQWtCLFlBQVksUUFBWixJQUF3QixFQUY1QztBQUFBLFFBSUUsT0FBa0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUpwQjtBQUFBLFFBS0UsY0FBa0IsRUFMcEI7QUFBQSxRQU9FLFFBQWtCLFVBQVUsQ0FBVixDQVBwQjtBQUFBLFFBUUUsZ0JBQW1CLE9BQU8sS0FBUCxJQUFnQixRQVJyQztBQUFBLFFBU0UsaUJBQWtCLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLENBQXpCLENBVHBCO0FBQUEsUUFVRSxhQVZGO0FBWUEsTUFBRSxJQUFGLEVBQ0csSUFESCxDQUNRLFlBQVc7QUFDZixVQUNFLFdBQXNCLEVBQUUsYUFBRixDQUFnQixVQUFoQixDQUFGLEdBQ2hCLEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLEVBQUUsRUFBRixDQUFLLE1BQUwsQ0FBWSxRQUEvQixFQUF5QyxVQUF6QyxDQURnQixHQUVoQixFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsRUFBRSxFQUFGLENBQUssTUFBTCxDQUFZLFFBQXpCLENBSE47QUFBQSxVQUtFLFlBQWtCLFNBQVMsU0FMN0I7QUFBQSxVQU1FLFdBQWtCLFNBQVMsUUFON0I7QUFBQSxVQU9FLFNBQWtCLFNBQVMsTUFQN0I7QUFBQSxVQVFFLFNBQWtCLFNBQVMsTUFSN0I7QUFBQSxVQVNFLFdBQWtCLFNBQVMsUUFUN0I7QUFBQSxVQVVFLFFBQWtCLFNBQVMsS0FWN0I7QUFBQSxVQVdFLFlBQWtCLFNBQVMsU0FYN0I7QUFBQSxVQWFFLGlCQUFrQixNQUFNLFNBYjFCO0FBQUEsVUFjRSxrQkFBa0IsWUFBWSxTQWRoQztBQUFBLFVBZ0JFLFVBQWtCLEVBQUUsSUFBRixDQWhCcEI7QUFBQSxVQWlCRSxVQUFrQixRQUFRLElBQVIsQ0FBYSxTQUFTLE1BQXRCLENBakJwQjtBQUFBLFVBa0JFLGdCQUFrQixRQUFRLElBQVIsQ0FBYSxTQUFTLFlBQXRCLENBbEJwQjtBQUFBLFVBbUJFLFdBQWtCLFFBQVEsSUFBUixDQUFhLFNBQVMsT0FBdEIsQ0FuQnBCO0FBQUEsVUFvQkUsVUFBa0IsUUFBUSxJQUFSLENBQWEsU0FBUyxNQUF0QixDQXBCcEI7QUFBQSxVQXFCRSxZQUFrQixRQUFRLElBQVIsQ0FBYSxTQUFTLFFBQXRCLENBckJwQjtBQUFBLFVBdUJFLFVBQWtCLElBdkJwQjtBQUFBLFVBd0JFLFdBQWtCLFFBQVEsSUFBUixDQUFhLGVBQWIsQ0F4QnBCO0FBQUEsVUEwQkUsa0JBQWtCLEtBMUJwQjtBQUFBLFVBNEJFLE1BNUJGOztBQStCQSxlQUFTOztBQUVQLG9CQUFZLFlBQVc7QUFDckIsaUJBQU8sT0FBUCxDQUFlLHFCQUFmO0FBQ0EsaUJBQU8sU0FBUCxDQUFpQixZQUFqQjtBQUNBLGlCQUFPLElBQVAsQ0FBWSxNQUFaO0FBQ0EsaUJBQU8sR0FBUCxDQUFXLElBQVg7QUFDQSxpQkFBTyxNQUFQLENBQWMsT0FBZDtBQUNBLGlCQUFPLFdBQVA7QUFDRCxTQVRNO0FBVVAscUJBQWEsWUFBVztBQUN0QixpQkFBTyxPQUFQLENBQWUsNEJBQWYsRUFBNkMsTUFBN0M7QUFDQSxxQkFBVyxNQUFYO0FBQ0Esa0JBQ0csSUFESCxDQUNRLGVBRFIsRUFDeUIsTUFEekI7QUFHRCxTQWhCTTtBQWlCUCxpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLE9BQVAsQ0FBZSxxQkFBZjtBQUNBLGtCQUNHLEdBREgsQ0FDTyxjQURQLEVBRUcsVUFGSCxDQUVjLGVBRmQ7QUFJRCxTQXZCTTs7QUF5QlAsaUJBQVMsWUFBVztBQUNsQixpQkFBTyxLQUFQLENBQWEsMkJBQWI7QUFDQSxvQkFBa0IsUUFBUSxJQUFSLENBQWEsU0FBUyxNQUF0QixDQUFsQjtBQUNBLDBCQUFrQixRQUFRLElBQVIsQ0FBYSxTQUFTLFlBQXRCLENBQWxCO0FBQ0Esc0JBQWtCLFFBQVEsSUFBUixDQUFhLFNBQVMsUUFBdEIsQ0FBbEI7QUFDQSxxQkFBa0IsUUFBUSxJQUFSLENBQWEsU0FBUyxPQUF0QixDQUFsQjtBQUNBLG9CQUFrQixRQUFRLElBQVIsQ0FBYSxTQUFTLE1BQXRCLENBQWxCO0FBQ0QsU0FoQ007O0FBa0NQLHdCQUFnQixZQUFXO0FBQ3pCLHFCQUFXLFFBQVEsSUFBUixDQUFhLFNBQVMsT0FBdEIsQ0FBWDtBQUNBLG9CQUFXLFFBQVEsSUFBUixDQUFhLFNBQVMsTUFBdEIsQ0FBWDtBQUNELFNBckNNOztBQXVDUCxjQUFNO0FBQ0osa0JBQVEsWUFBVztBQUNqQixtQkFBTyxPQUFQLENBQWUsMEJBQWY7QUFDQSxnQkFBRyxTQUFTLFNBQVosRUFBdUI7QUFDckIsc0JBQ0csRUFESCxDQUNNLE9BQU8sR0FBUCxDQUFXLFVBQVgsS0FBMEIsY0FEaEMsRUFDZ0QsU0FBUyxNQUR6RCxFQUNpRSxPQUFPLEtBQVAsQ0FBYSxLQUQ5RTtBQUdBLHNCQUNHLElBREgsQ0FDUSxjQURSLEVBQ3dCLEtBRHhCO0FBR0Q7QUFDRDs7QUFBQSxhQUVHLEVBRkgsQ0FFTSxVQUFjLGNBRnBCLEVBRW9DLFNBQVMsTUFGN0MsRUFFcUQsT0FBTyxLQUFQLENBQWEsS0FGbEUsRUFHRyxFQUhILENBR00sU0FBYyxjQUhwQixFQUdvQyxTQUFTLE1BSDdDLEVBR3FELE9BQU8sS0FBUCxDQUFhLElBSGxFLEVBSUcsRUFKSCxDQUlNLFlBQWMsY0FKcEIsRUFJb0MsU0FBUyxNQUo3QyxFQUlxRCxPQUFPLGNBSjVEOztBQUFBLGFBTUcsRUFOSCxDQU1NLFVBQWMsY0FOcEIsRUFNb0MsU0FBUyxZQU43QyxFQU0yRCxPQUFPLEtBTmxFOztBQUFBLGFBUUcsRUFSSCxDQVFNLGNBQWMsY0FScEIsRUFRb0MsU0FBUyxPQVI3QyxFQVFzRCxPQUFPLEtBQVAsQ0FBYSxNQUFiLENBQW9CLFNBUjFFLEVBU0csRUFUSCxDQVNNLFlBQWMsY0FUcEIsRUFTb0MsU0FBUyxPQVQ3QyxFQVNzRCxPQUFPLEtBQVAsQ0FBYSxNQUFiLENBQW9CLE9BVDFFLEVBVUcsRUFWSCxDQVVNLFVBQWMsY0FWcEIsRUFVb0MsU0FBUyxNQVY3QyxFQVVzRCxPQUFPLEtBQVAsQ0FBYSxNQUFiLENBQW9CLEtBVjFFO0FBWUQ7QUF2QkcsU0F2Q0M7O0FBaUVQLG1CQUFXO0FBQ1Qsd0JBQWMsWUFBVzs7O0FBR3ZCLGdCQUFHLGNBQWMsV0FBVyxZQUFYLEtBQTRCLFNBQTdDLEVBQXdEO0FBQ3RELHVCQUFTLFlBQVQsR0FBd0IsV0FBVyxZQUFuQztBQUNEO0FBQ0Y7QUFQUSxTQWpFSjs7QUEyRVAsZUFBTztBQUNMLGlCQUFPLFlBQVc7QUFDaEIseUJBQWEsT0FBTyxLQUFwQjtBQUNBLG1CQUFPLEtBQVAsR0FBZSxXQUFXLE9BQU8sS0FBbEIsRUFBeUIsU0FBUyxXQUFsQyxDQUFmO0FBQ0QsV0FKSTtBQUtMLGlCQUFPLFlBQVc7QUFDaEIsbUJBQU8sR0FBUCxDQUFXLEtBQVg7QUFDQSxnQkFBSSxPQUFPLEdBQVAsQ0FBVyxpQkFBWCxFQUFKLEVBQXFDO0FBQ25DLHFCQUFPLEtBQVA7QUFDQSxrQkFBSSxPQUFPLEdBQVAsQ0FBVyxJQUFYLEVBQUosRUFBd0I7QUFDdEIsdUJBQU8sV0FBUDtBQUNEO0FBQ0Y7QUFDRixXQWJJO0FBY0wsZ0JBQU0sVUFBUyxLQUFULEVBQWdCO0FBQ3BCLGdCQUNFLGdCQUFpQixTQUFTLGFBQVQsS0FBMkIsSUFEOUM7QUFBQSxnQkFFRSxXQUFnQixZQUFXO0FBQ3pCLHFCQUFPLE1BQVAsQ0FBYyxLQUFkO0FBQ0EscUJBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDQSxxQkFBTyxLQUFQLEdBQWUsV0FBVyxPQUFPLFdBQWxCLEVBQStCLFNBQVMsU0FBeEMsQ0FBZjtBQUNELGFBTkg7QUFRQSxnQkFBRyxhQUFILEVBQWtCO0FBQ2hCO0FBQ0Q7QUFDRCxnQkFBRyxPQUFPLGNBQVYsRUFBMEI7QUFDeEIscUJBQU8sS0FBUCxDQUFhLG1EQUFiO0FBQ0Esc0JBQ0csR0FESCxDQUNPLGdCQUFnQixjQUR2QixFQUN1QyxTQUFTLE9BRGhELEVBQ3lELFVBQVMsS0FBVCxFQUFnQjtBQUNyRSxvQkFBRyxPQUFPLEVBQVAsQ0FBVSxTQUFWLENBQW9CLEtBQXBCLEtBQThCLGVBQWpDLEVBQWtEO0FBQ2hELDBCQUFRLEtBQVI7QUFDQTtBQUNEO0FBQ0Qsa0NBQWtCLEtBQWxCO0FBQ0Esb0JBQUksQ0FBQyxPQUFPLEVBQVAsQ0FBVSxTQUFWLEVBQUQsSUFBMEIsQ0FBQyxPQUFPLEVBQVAsQ0FBVSxNQUFWLEVBQS9CLEVBQW1EO0FBQ2pEO0FBQ0Q7QUFDRixlQVZIO0FBWUQsYUFkRCxNQWVLO0FBQ0gscUJBQU8sS0FBUCxDQUFhLG9EQUFiO0FBQ0E7QUFDRDtBQUNGLFdBN0NJO0FBOENMLGtCQUFRO0FBQ04sdUJBQVcsWUFBVztBQUNwQixxQkFBTyxjQUFQLEdBQXdCLElBQXhCO0FBQ0QsYUFISztBQUlOLHFCQUFTLFlBQVc7QUFDbEIscUJBQU8sY0FBUCxHQUF3QixLQUF4QjtBQUNELGFBTks7QUFPTixtQkFBTyxVQUFTLEtBQVQsRUFBZ0I7QUFDckIscUJBQU8sS0FBUCxDQUFhLHdCQUFiO0FBQ0Esa0JBQ0UsVUFBVSxFQUFFLElBQUYsQ0FEWjtBQUFBLGtCQUVFLFNBQVUsUUFBUSxJQUFSLENBQWEsU0FBUyxLQUF0QixFQUE2QixFQUE3QixDQUFnQyxDQUFoQyxDQUZaO0FBQUEsa0JBR0UsUUFBVSxRQUFRLEVBQVIsQ0FBVyxTQUFYLElBQ04sT0FETSxHQUVOLFFBQVEsSUFBUixDQUFhLFNBQWIsRUFBd0IsRUFBeEIsQ0FBMkIsQ0FBM0IsQ0FMTjtBQUFBLGtCQU1FLE9BQVUsTUFBTSxJQUFOLENBQVcsTUFBWCxLQUF3QixLQU5wQztBQUFBLGtCQU9FLFNBQVUsTUFBTSxJQUFOLENBQVcsUUFBWCxLQUF3QixLQVBwQztBQUFBLGtCQVFFLFFBQVUsT0FBTyxJQUFQLEVBUlo7QUFBQTs7QUFVRSxzQkFBVyxPQUFPLE1BQVAsR0FBZ0IsQ0FBakIsR0FDTixPQUFPLElBQVAsRUFETSxHQUVOLEtBWk47QUFBQSxrQkFhRSxVQUFVLE9BQU8sR0FBUCxDQUFXLE9BQVgsRUFiWjtBQUFBLGtCQWNFLFNBQVUsUUFBUSxJQUFSLENBQWEsU0FBUyxNQUF0QixLQUFpQyxPQUFPLEdBQVAsQ0FBVyxNQUFYLENBQWtCLEtBQWxCLEVBQXlCLE9BQXpCLENBZDdDO0FBQUEsa0JBZUUsYUFmRjtBQWlCQSxrQkFBSSxFQUFFLFVBQUYsQ0FBYSxTQUFTLFFBQXRCLENBQUosRUFBc0M7QUFDcEMsb0JBQUcsU0FBUyxRQUFULENBQWtCLElBQWxCLENBQXVCLE9BQXZCLEVBQWdDLE1BQWhDLEVBQXdDLE9BQXhDLE1BQXFELEtBQXhELEVBQStEO0FBQzdELHlCQUFPLEtBQVAsQ0FBYSwwREFBYjtBQUNBLG9DQUFrQixJQUFsQjtBQUNBO0FBQ0Q7QUFDRjtBQUNELHFCQUFPLFdBQVA7QUFDQSxrQkFBRyxLQUFILEVBQVU7QUFDUix1QkFBTyxHQUFQLENBQVcsS0FBWCxDQUFpQixLQUFqQjtBQUNEO0FBQ0Qsa0JBQUcsSUFBSCxFQUFTO0FBQ1AsdUJBQU8sT0FBUCxDQUFlLHFDQUFmLEVBQXNELEtBQXREO0FBQ0Esb0JBQUcsVUFBVSxRQUFWLElBQXNCLE1BQU0sT0FBL0IsRUFBd0M7QUFDdEMseUJBQU8sSUFBUCxDQUFZLElBQVo7QUFDRCxpQkFGRCxNQUdLO0FBQ0gseUJBQU8sUUFBUCxDQUFnQixJQUFoQixHQUF3QixJQUF4QjtBQUNEO0FBQ0Y7QUFDRjtBQTlDSztBQTlDSCxTQTNFQTtBQTBLUCx3QkFBZ0IsVUFBUyxLQUFULEVBQWdCO0FBQzlCOztBQUVFLG9CQUFlLFFBQVEsSUFBUixDQUFhLFNBQVMsTUFBdEIsQ0FGakI7QUFBQSxjQUdFLFlBQWUsUUFBUSxJQUFSLENBQWEsU0FBUyxRQUF0QixDQUhqQjtBQUFBLGNBSUUsZUFBZSxRQUFRLEtBQVIsQ0FBZSxRQUFRLE1BQVIsQ0FBZSxNQUFNLFVBQVUsTUFBL0IsQ0FBZixDQUpqQjtBQUFBLGNBS0UsYUFBZSxRQUFRLE1BTHpCO0FBQUEsY0FPRSxVQUFlLE1BQU0sS0FQdkI7QUFBQSxjQVFFLE9BQWU7QUFDYix1QkFBWSxDQURDO0FBRWIsbUJBQVksRUFGQztBQUdiLG9CQUFZLEVBSEM7QUFJYixxQkFBWSxFQUpDO0FBS2IsdUJBQVk7QUFMQyxXQVJqQjtBQUFBLGNBZUUsUUFmRjs7QUFrQkEsY0FBRyxXQUFXLEtBQUssTUFBbkIsRUFBMkI7QUFDekIsbUJBQU8sT0FBUCxDQUFlLDJDQUFmO0FBQ0EsbUJBQU8sT0FBUCxDQUFlLElBQWY7QUFDRDtBQUNELGNBQUksT0FBTyxFQUFQLENBQVUsT0FBVixFQUFKLEVBQTBCO0FBQ3hCLGdCQUFHLFdBQVcsS0FBSyxLQUFuQixFQUEwQjtBQUN4QixxQkFBTyxPQUFQLENBQWUsNENBQWY7QUFDQSxrQkFBSSxRQUFRLE1BQVIsQ0FBZSxNQUFNLFVBQVUsTUFBL0IsRUFBdUMsTUFBdkMsR0FBZ0QsQ0FBcEQsRUFBd0Q7QUFDdEQsdUJBQU8sS0FBUCxDQUFhLE1BQWIsQ0FBb0IsS0FBcEIsQ0FBMEIsSUFBMUIsQ0FBK0IsUUFBUSxNQUFSLENBQWUsTUFBTSxVQUFVLE1BQS9CLENBQS9CLEVBQXVFLEtBQXZFO0FBQ0Esc0JBQU0sY0FBTjtBQUNBLHVCQUFPLEtBQVA7QUFDRDtBQUNGLGFBUEQsTUFRSyxJQUFHLFdBQVcsS0FBSyxPQUFuQixFQUE0QjtBQUMvQixxQkFBTyxPQUFQLENBQWUsd0NBQWY7QUFDQSx5QkFBWSxlQUFlLENBQWYsR0FBbUIsQ0FBcEIsR0FDUCxZQURPLEdBRVAsZUFBZSxDQUZuQjtBQUlBLHdCQUNHLFdBREgsQ0FDZSxVQUFVLE1BRHpCO0FBR0Esc0JBQ0csV0FESCxDQUNlLFVBQVUsTUFEekIsRUFFRyxFQUZILENBRU0sUUFGTixFQUdLLFFBSEwsQ0FHYyxVQUFVLE1BSHhCLEVBSUssT0FKTCxDQUlhLFNBSmIsRUFLTyxRQUxQLENBS2dCLFVBQVUsTUFMMUI7QUFPQSxvQkFBTSxjQUFOO0FBQ0QsYUFqQkksTUFrQkEsSUFBRyxXQUFXLEtBQUssU0FBbkIsRUFBOEI7QUFDakMscUJBQU8sT0FBUCxDQUFlLDBDQUFmO0FBQ0EseUJBQVksZUFBZSxDQUFmLElBQW9CLFVBQXJCLEdBQ1AsWUFETyxHQUVQLGVBQWUsQ0FGbkI7QUFJQSx3QkFDRyxXQURILENBQ2UsVUFBVSxNQUR6QjtBQUdBLHNCQUNHLFdBREgsQ0FDZSxVQUFVLE1BRHpCLEVBRUcsRUFGSCxDQUVNLFFBRk4sRUFHSyxRQUhMLENBR2MsVUFBVSxNQUh4QixFQUlLLE9BSkwsQ0FJYSxTQUpiLEVBS08sUUFMUCxDQUtnQixVQUFVLE1BTDFCO0FBT0Esb0JBQU0sY0FBTjtBQUNEO0FBQ0YsV0E3Q0QsTUE4Q0s7O0FBRUgsZ0JBQUcsV0FBVyxLQUFLLEtBQW5CLEVBQTBCO0FBQ3hCLHFCQUFPLE9BQVAsQ0FBZSxvQ0FBZjtBQUNBLHFCQUFPLEtBQVA7QUFDQSxxQkFBTyxHQUFQLENBQVcsYUFBWDtBQUNBLHNCQUFRLEdBQVIsQ0FBWSxPQUFaLEVBQXFCLE9BQU8sTUFBUCxDQUFjLFdBQW5DO0FBQ0Q7QUFDRjtBQUNGLFNBeFBNOztBQTBQUCxlQUFPO0FBQ0wsZUFBSyxVQUFTLFVBQVQsRUFBcUI7QUFDeEIsZ0JBQ0UsY0FBYztBQUNaLHFCQUFvQixTQUFTLEtBRGpCO0FBRVosa0JBQW9CLEtBRlI7QUFHWixxQkFBb0IsSUFIUjtBQUlaLHNCQUFvQixRQUpSO0FBS1osdUJBQW9CO0FBQ2xCLHVCQUFRO0FBRFUsZUFMUjtBQVFaLHlCQUFvQixVQUFTLFFBQVQsRUFBbUI7QUFDckMsdUJBQU8sS0FBUCxDQUFhLFFBQWIsQ0FBc0IsSUFBdEIsQ0FBMkIsT0FBM0IsRUFBb0MsUUFBcEMsRUFBOEMsVUFBOUM7QUFDRCxlQVZXO0FBV1osdUJBQW9CLFVBQVMsUUFBVCxFQUFtQixDQUN0QyxDQVpXO0FBYVoseUJBQW9CLFlBQVc7QUFDN0IsdUJBQU8sY0FBUCxDQUFzQixNQUFNLFdBQTVCO0FBQ0QsZUFmVztBQWdCWix1QkFBb0IsT0FBTztBQWhCZixhQURoQjtBQUFBLGdCQW1CRSxVQW5CRjtBQXFCQSxjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsV0FBZixFQUE0QixTQUFTLFdBQXJDO0FBQ0EsbUJBQU8sT0FBUCxDQUFlLHdCQUFmLEVBQXlDLFdBQXpDO0FBQ0Esb0JBQVEsR0FBUixDQUFZLFdBQVo7QUFDRDtBQTFCSSxTQTFQQTs7QUF1UlAsYUFBSztBQUNILGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sRUFBRSxFQUFGLENBQUssR0FBTCxLQUFhLFNBQXBCO0FBQ0QsV0FIRTtBQUlILGdCQUFNLFlBQVc7QUFDZixtQkFBTyxPQUFPLEVBQVAsQ0FBVSxPQUFWLE1BQXVCLENBQUMsT0FBTyxFQUFQLENBQVUsT0FBVixFQUF4QixJQUErQyxDQUFDLE9BQU8sRUFBUCxDQUFVLEtBQVYsRUFBdkQ7QUFDRCxXQU5FO0FBT0gsc0JBQVksWUFBVztBQUNyQixtQkFBTyxTQUFTLFVBQVQsSUFBdUIsRUFBRSxFQUFGLENBQUssVUFBTCxLQUFvQixTQUEzQyxJQUF3RCxRQUFRLFVBQVIsQ0FBbUIsY0FBbkIsQ0FBL0Q7QUFDRDtBQVRFLFNBdlJFOztBQW1TUCxZQUFJO0FBQ0YscUJBQVcsWUFBVztBQUNwQixtQkFBTyxTQUFTLFFBQVQsQ0FBa0IsVUFBVSxTQUE1QixDQUFQO0FBQ0QsV0FIQztBQUlGLGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sU0FBUyxRQUFULENBQWtCLFVBQVUsTUFBNUIsQ0FBUDtBQUNELFdBTkM7QUFPRixxQkFBVyxVQUFTLEtBQVQsRUFBZ0I7QUFDekIsbUJBQVEsTUFBTSxNQUFOLElBQWdCLEVBQUUsTUFBTSxNQUFSLEVBQWdCLE9BQWhCLENBQXdCLFNBQVMsT0FBakMsRUFBMEMsTUFBMUMsR0FBbUQsQ0FBM0U7QUFDRCxXQVRDO0FBVUYsaUJBQU8sWUFBVztBQUNoQixtQkFBUSxTQUFTLElBQVQsT0FBb0IsRUFBNUI7QUFDRCxXQVpDO0FBYUYsbUJBQVMsWUFBVztBQUNsQixtQkFBUSxTQUFTLE1BQVQsQ0FBZ0IsVUFBaEIsRUFBNEIsTUFBNUIsR0FBcUMsQ0FBN0M7QUFDRCxXQWZDO0FBZ0JGLG1CQUFTLFlBQVc7QUFDbEIsbUJBQVEsUUFBUSxNQUFSLENBQWUsUUFBZixFQUF5QixNQUF6QixHQUFrQyxDQUExQztBQUNEO0FBbEJDLFNBblNHOztBQXdUUCxpQkFBUztBQUNQLGdCQUFNLFlBQVc7QUFDZixnQkFDRSxTQUFnQixTQUFTLFdBQVQsQ0FBcUIsWUFBckIsQ0FEbEI7QUFBQSxnQkFFRSxnQkFBZ0IsUUFBUSxDQUFSLENBRmxCO0FBSUEsZ0JBQUcsYUFBSCxFQUFrQjtBQUNoQixxQkFBTyxPQUFQLENBQWUsOEJBQWY7QUFDQSxxQkFBTyxTQUFQLENBQWlCLE1BQWpCLEVBQXlCLEtBQXpCLEVBQWdDLEtBQWhDO0FBQ0EsNEJBQWMsYUFBZCxDQUE0QixNQUE1QjtBQUNEO0FBQ0Y7QUFYTSxTQXhURjs7QUFzVVAsYUFBSztBQUNILHNCQUFZLFlBQVc7QUFDckIsZ0JBQ0UsU0FBUyxRQUFRLENBQVIsQ0FEWDtBQUFBLGdCQUVFLGFBQWdCLFdBQVcsU0FBWCxJQUF3QixPQUFPLE9BQVAsS0FBbUIsU0FBNUMsR0FDWCxPQURXLEdBRVYsV0FBVyxTQUFYLElBQXdCLE9BQU8sZ0JBQVAsS0FBNEIsU0FBckQsR0FDRSxnQkFERixHQUVFLE9BTlI7QUFRQSxtQkFBTyxVQUFQO0FBQ0QsV0FYRTtBQVlILGlCQUFPLFlBQVc7QUFDaEIsbUJBQU8sUUFBUSxHQUFSLEVBQVA7QUFDRCxXQWRFO0FBZUgsbUJBQVMsWUFBVztBQUNsQixnQkFDRSxVQUFVLFFBQVEsSUFBUixDQUFhLFNBQVMsT0FBdEIsQ0FEWjtBQUdBLG1CQUFPLE9BQVA7QUFDRCxXQXBCRTtBQXFCSCxrQkFBUSxVQUFTLEtBQVQsRUFBZ0IsT0FBaEIsRUFBeUI7QUFDL0IsZ0JBQ0UsZUFBZSxDQUFDLE9BQUQsRUFBVSxJQUFWLENBRGpCO0FBQUEsZ0JBRUUsU0FBZSxLQUZqQjtBQUlBLG9CQUFTLFVBQVUsU0FBWCxHQUNKLEtBREksR0FFSixPQUFPLEdBQVAsQ0FBVyxLQUFYLEVBRko7QUFJQSxzQkFBVyxZQUFZLFNBQWIsR0FDTixPQURNLEdBRU4sT0FBTyxHQUFQLENBQVcsT0FBWCxFQUZKO0FBSUEsZ0JBQUcsU0FBUyxJQUFULEtBQWtCLFVBQXJCLEVBQWlDO0FBQy9CLHFCQUFPLEtBQVAsQ0FBYSw2QkFBYixFQUE0QyxLQUE1QztBQUNBLGdCQUFFLElBQUYsQ0FBTyxPQUFQLEVBQWdCLFVBQVMsS0FBVCxFQUFnQixRQUFoQixFQUEwQjtBQUN4QyxvQkFBRyxFQUFFLE9BQUYsQ0FBVSxTQUFTLE9BQW5CLENBQUgsRUFBZ0M7QUFDOUIsMkJBQVMsT0FBTyxNQUFQLENBQWMsTUFBZCxDQUFxQixLQUFyQixFQUE0QixTQUFTLE9BQXJDLEVBQThDLFlBQTlDLEVBQTRELENBQTVELENBQVQ7O0FBRUEsc0JBQUcsTUFBSCxFQUFXO0FBQ1QsMkJBQU8sS0FBUDtBQUNEO0FBQ0Y7QUFDRixlQVJEO0FBU0QsYUFYRCxNQVlLO0FBQ0gscUJBQU8sS0FBUCxDQUFhLGtDQUFiLEVBQWlELEtBQWpEO0FBQ0EsdUJBQVMsT0FBTyxNQUFQLENBQWMsTUFBZCxDQUFxQixLQUFyQixFQUE0QixPQUE1QixFQUFxQyxZQUFyQyxFQUFtRCxDQUFuRCxDQUFUO0FBQ0Q7QUFDRCxtQkFBTyxVQUFVLEtBQWpCO0FBQ0Q7QUFuREUsU0F0VUU7O0FBNFhQLGdCQUFRO0FBQ04sdUJBQWEsWUFBVztBQUN0QixtQkFBTyxPQUFQLENBQWUsd0JBQWY7QUFDQSxvQkFBUSxLQUFSLEdBQWdCLFFBQWhCLENBQXlCLFVBQVUsTUFBbkM7QUFDRDtBQUpLLFNBNVhEOztBQW1ZUCxhQUFLO0FBQ0gsaUJBQU8sWUFBVztBQUNoQixvQkFBUSxRQUFSLENBQWlCLFVBQVUsS0FBM0I7QUFDRCxXQUhFO0FBSUgsbUJBQVMsWUFBVztBQUNsQixvQkFBUSxRQUFSLENBQWlCLFVBQVUsT0FBM0I7QUFDRCxXQU5FO0FBT0gsaUJBQU8sVUFBUyxLQUFULEVBQWdCO0FBQ3JCLG1CQUFPLE9BQVAsQ0FBZSw0QkFBZixFQUE2QyxLQUE3QztBQUNBLG9CQUNHLEdBREgsQ0FDTyxLQURQO0FBR0QsV0FaRTtBQWFILGdCQUFNLFVBQVMsSUFBVCxFQUFlO0FBQ25CLG1CQUFPLFFBQVEsU0FBUyxJQUF4QjtBQUNBLGdCQUFHLFNBQVMsSUFBVCxJQUFpQixVQUFwQixFQUFnQztBQUM5QixzQkFBUSxRQUFSLENBQWlCLFNBQVMsSUFBMUI7QUFDRDtBQUNGLFdBbEJFO0FBbUJILHlCQUFlLFlBQVc7QUFDeEIsMEJBQWMsUUFBZCxDQUF1QixVQUFVLE9BQWpDO0FBQ0Q7QUFyQkUsU0FuWUU7O0FBMlpQLGdCQUFRO0FBQ04sbUJBQVMsWUFBVztBQUNsQixvQkFBUSxXQUFSLENBQW9CLFVBQVUsT0FBOUI7QUFDRCxXQUhLO0FBSU4saUJBQU8sWUFBVztBQUNoQixvQkFBUSxXQUFSLENBQW9CLFVBQVUsS0FBOUI7QUFDRCxXQU5LO0FBT04seUJBQWUsWUFBVztBQUN4QiwwQkFBYyxXQUFkLENBQTBCLFVBQVUsT0FBcEM7QUFDRDtBQVRLLFNBM1pEOztBQXVhUCxlQUFPLFlBQVc7QUFDaEIsY0FDRSxhQUFhLE9BQU8sR0FBUCxDQUFXLEtBQVgsRUFEZjtBQUFBLGNBRUUsUUFBUSxPQUFPLElBQVAsQ0FBWSxLQUFaLENBQWtCLFVBQWxCLENBRlY7QUFJQSxjQUFJLE9BQU8sR0FBUCxDQUFXLGlCQUFYLEVBQUosRUFBc0M7QUFDcEMsZ0JBQUcsS0FBSCxFQUFVO0FBQ1IscUJBQU8sS0FBUCxDQUFhLDJCQUFiLEVBQTBDLFVBQTFDO0FBQ0EscUJBQU8sSUFBUCxDQUFZLE9BQVosQ0FBb0IsTUFBTSxPQUExQjtBQUNBLHFCQUFPLFVBQVAsQ0FBa0IsTUFBTSxJQUF4QjtBQUNBLHFCQUFPLE1BQVAsQ0FBYyxFQUFkLENBQWlCLE1BQU0sT0FBdkI7QUFDRCxhQUxELE1BTUs7QUFDSCxxQkFBTyxLQUFQLENBQWEsY0FBYixFQUE2QixVQUE3QjtBQUNBLGtCQUFHLEVBQUUsYUFBRixDQUFnQixTQUFTLE1BQXpCLEtBQW9DLEVBQUUsT0FBRixDQUFVLFNBQVMsTUFBbkIsQ0FBdkMsRUFBbUU7QUFDakUsdUJBQU8sTUFBUCxDQUFjLEtBQWQsQ0FBb0IsVUFBcEI7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLEdBQVAsQ0FBVyxNQUFYLEVBQUosRUFBMEI7QUFDN0IsdUJBQU8sTUFBUCxDQUFjLE1BQWQsQ0FBcUIsVUFBckI7QUFDRCxlQUZJLE1BR0E7QUFDSCx1QkFBTyxLQUFQLENBQWEsTUFBTSxNQUFuQjtBQUNEO0FBQ0Y7QUFDRCxxQkFBUyxhQUFULENBQXVCLElBQXZCLENBQTRCLE9BQTVCLEVBQXFDLFVBQXJDO0FBQ0QsV0FwQkQsTUFxQks7QUFDSCxtQkFBTyxXQUFQO0FBQ0Q7QUFDRixTQXBjTTs7QUFzY1AsZ0JBQVE7QUFDTixpQkFBTyxVQUFTLFVBQVQsRUFBcUI7QUFDMUIsZ0JBQ0UsVUFBVSxPQUFPLE1BQVAsQ0FBYyxNQUFkLENBQXFCLFVBQXJCLEVBQWlDLFNBQVMsT0FBMUMsQ0FEWjtBQUFBLGdCQUVFLFVBRkY7QUFJQSxtQkFBTyxHQUFQLENBQVcsT0FBWDtBQUNBLG1CQUFPLElBQVAsQ0FBWSxPQUFaLENBQW9CLE9BQXBCO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLCtCQUFiLEVBQThDLE9BQTlDOztBQUVBLHlCQUFhLE9BQU8sZUFBUCxDQUF1QjtBQUNsQyx1QkFBUztBQUR5QixhQUF2QixDQUFiO0FBR0EsbUJBQU8sTUFBUCxDQUFjLE9BQWQ7QUFDQSxtQkFBTyxVQUFQLENBQWtCLFVBQWxCO0FBQ0EsbUJBQU8sTUFBUCxDQUFjLEVBQWQsQ0FBaUIsT0FBakI7QUFDQSxtQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixVQUFuQixFQUErQjtBQUM3QixvQkFBVSxVQURtQjtBQUU3Qix1QkFBVTtBQUZtQixhQUEvQjtBQUlELFdBcEJLO0FBcUJOLGtCQUFRLFVBQVMsVUFBVCxFQUFxQjtBQUMzQixnQkFBRyxRQUFRLEdBQVIsQ0FBWSxZQUFaLENBQUgsRUFBOEI7QUFDNUIsc0JBQVEsR0FBUixDQUFZLE9BQVo7QUFDRDtBQUNELG1CQUFPLEtBQVAsQ0FBYSxHQUFiLENBQWlCLFVBQWpCO0FBQ0Esb0JBQ0csR0FESCxDQUNPLE9BRFA7QUFHRCxXQTdCSztBQThCTixrQkFBUSxVQUFTLFVBQVQsRUFBcUIsTUFBckIsRUFBNkIsWUFBN0IsRUFBMkM7QUFDakQsZ0JBQ0UsVUFBZSxFQURqQjtBQUFBLGdCQUVFLGVBQWUsRUFGakI7QUFBQSxnQkFHRSxZQUFlLFdBQVcsUUFBWCxHQUFzQixPQUF0QixDQUE4QixPQUFPLE1BQXJDLEVBQTZDLE1BQTdDLENBSGpCO0FBQUEsZ0JBSUUsY0FBZSxJQUFJLE1BQUosQ0FBVyxPQUFPLFVBQVAsR0FBb0IsU0FBL0IsRUFBMEMsR0FBMUMsQ0FKakI7QUFBQTs7O0FBT0Usd0JBQVksVUFBUyxLQUFULEVBQWdCLE1BQWhCLEVBQXdCO0FBQ2xDLGtCQUNFLFlBQWtCLEVBQUUsT0FBRixDQUFVLE1BQVYsRUFBa0IsT0FBbEIsS0FBOEIsQ0FBQyxDQURuRDtBQUFBLGtCQUVFLGlCQUFrQixFQUFFLE9BQUYsQ0FBVSxNQUFWLEVBQWtCLFlBQWxCLEtBQW1DLENBQUMsQ0FGeEQ7QUFJQSxrQkFBRyxhQUFhLGNBQWhCLEVBQWdDO0FBQzlCLHNCQUFNLElBQU4sQ0FBVyxNQUFYO0FBQ0Q7QUFDRixhQWZIO0FBaUJBLHFCQUFTLFVBQVUsU0FBUyxNQUE1QjtBQUNBLDJCQUFnQixpQkFBaUIsU0FBbEIsR0FDWCxZQURXLEdBRVgsU0FBUyxZQUZiOzs7QUFNQSxnQkFBRyxDQUFDLEVBQUUsT0FBRixDQUFVLFlBQVYsQ0FBSixFQUE2QjtBQUMzQiw2QkFBZSxDQUFDLFlBQUQsQ0FBZjtBQUNEOzs7QUFHRCxnQkFBRyxXQUFXLFNBQVgsSUFBd0IsV0FBVyxLQUF0QyxFQUE2QztBQUMzQyxxQkFBTyxLQUFQLENBQWEsTUFBTSxNQUFuQjtBQUNBLHFCQUFPLEVBQVA7QUFDRDs7O0FBR0QsY0FBRSxJQUFGLENBQU8sWUFBUCxFQUFxQixVQUFTLEtBQVQsRUFBZ0IsS0FBaEIsRUFBdUI7QUFDMUMsZ0JBQUUsSUFBRixDQUFPLE1BQVAsRUFBZSxVQUFTLEtBQVQsRUFBZ0IsT0FBaEIsRUFBeUI7QUFDdEMsb0JBQ0UsY0FBZSxPQUFPLFFBQVEsS0FBUixDQUFQLElBQXlCLFFBRDFDO0FBR0Esb0JBQUcsV0FBSCxFQUFnQjtBQUNkLHNCQUFJLFFBQVEsS0FBUixFQUFlLE1BQWYsQ0FBc0IsV0FBdEIsTUFBdUMsQ0FBQyxDQUE1QyxFQUErQzs7QUFFN0MsOEJBQVUsT0FBVixFQUFtQixPQUFuQjtBQUNELG1CQUhELE1BSUssSUFBRyxTQUFTLGNBQVQsSUFBMkIsT0FBTyxXQUFQLENBQW1CLFVBQW5CLEVBQStCLFFBQVEsS0FBUixDQUEvQixDQUE5QixFQUErRTs7QUFFbEYsOEJBQVUsWUFBVixFQUF3QixPQUF4QjtBQUNEO0FBQ0Y7QUFDRixlQWREO0FBZUQsYUFoQkQ7QUFpQkEsbUJBQU8sRUFBRSxLQUFGLENBQVEsT0FBUixFQUFpQixZQUFqQixDQUFQO0FBQ0Q7QUFwRkssU0F0Y0Q7O0FBNmhCUCxxQkFBYSxVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDakMsY0FDRSxhQUFjLEtBQUssTUFEckI7QUFBQSxjQUVFLGNBQWMsTUFBTSxNQUZ0QjtBQUlBLGNBQUcsT0FBTyxLQUFQLEtBQWlCLFFBQXBCLEVBQThCO0FBQzVCLG1CQUFPLEtBQVA7QUFDRDtBQUNELGtCQUFRLE1BQU0sV0FBTixFQUFSO0FBQ0EsaUJBQVEsS0FBSyxXQUFMLEVBQVI7QUFDQSxjQUFHLGNBQWMsVUFBakIsRUFBNkI7QUFDM0IsbUJBQU8sS0FBUDtBQUNEO0FBQ0QsY0FBRyxnQkFBZ0IsVUFBbkIsRUFBK0I7QUFDN0IsbUJBQVEsVUFBVSxJQUFsQjtBQUNEO0FBQ0Qsa0JBQVEsS0FBSyxJQUFJLGlCQUFpQixDQUFyQixFQUF3QixxQkFBcUIsQ0FBbEQsRUFBcUQsaUJBQWlCLFdBQXRFLEVBQW1GLGdCQUFuRixFQUFxRztBQUMzRyxnQkFDRSxpQkFBaUIsTUFBTSxVQUFOLENBQWlCLGNBQWpCLENBRG5CO0FBR0EsbUJBQU0scUJBQXFCLFVBQTNCLEVBQXVDO0FBQ3JDLGtCQUFHLEtBQUssVUFBTCxDQUFnQixvQkFBaEIsTUFBMEMsY0FBN0MsRUFBNkQ7QUFDM0QseUJBQVMsTUFBVDtBQUNEO0FBQ0Y7QUFDRCxtQkFBTyxLQUFQO0FBQ0Q7QUFDRCxpQkFBTyxJQUFQO0FBQ0QsU0F6akJNOztBQTJqQlAsZUFBTztBQUNMLG9CQUFVLFVBQVMsUUFBVCxFQUFtQixVQUFuQixFQUErQjtBQUN2QyxnQkFDRSxhQUFhLE9BQU8sZUFBUCxDQUF1QixRQUF2QixDQURmO0FBR0EsbUJBQU8sT0FBUCxDQUFlLHlCQUFmLEVBQTBDLFFBQTFDO0FBQ0EsZ0JBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUN6QixrQkFBRyxlQUFlLFNBQWYsSUFBNEIsU0FBUyxPQUFPLE9BQWhCLE1BQTZCLFNBQTVELEVBQXVFO0FBQ3JFLHVCQUFPLFVBQVAsQ0FBa0IsVUFBbEI7QUFDQSx1QkFBTyxNQUFQLENBQWMsRUFBZCxDQUFpQixTQUFTLE9BQU8sT0FBaEIsQ0FBakI7QUFDQSx1QkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixVQUFuQixFQUErQjtBQUM3Qix3QkFBVSxVQURtQjtBQUU3QiwyQkFBVSxTQUFTLE9BQU8sT0FBaEI7QUFGbUIsaUJBQS9CO0FBSUEsdUJBQU8sSUFBUCxDQUFZLE9BQVosQ0FBb0IsU0FBUyxPQUFPLE9BQWhCLENBQXBCO0FBQ0Q7QUFDRjtBQUNGO0FBakJJLFNBM2pCQTs7QUEra0JQLGdCQUFRO0FBQ04saUJBQU8sWUFBVztBQUNoQixnQkFBSSxPQUFPLEdBQVAsQ0FBVyxNQUFYLEVBQUosRUFBMEI7QUFDeEIsc0JBQVEsR0FBUixDQUFZLE9BQVo7QUFDRDtBQUNGO0FBTEssU0Eva0JEOztBQXVsQlAsYUFBSztBQUNILDZCQUFtQixZQUFXO0FBQzVCLGdCQUNFLGFBQWdCLE9BQU8sR0FBUCxDQUFXLEtBQVgsRUFEbEI7QUFBQSxnQkFFRSxnQkFBZ0IsV0FBVyxNQUY3QjtBQUlBLG1CQUFRLGlCQUFpQixTQUFTLGFBQWxDO0FBQ0Q7QUFQRSxTQXZsQkU7O0FBaW1CUCxlQUFPO0FBQ0wsaUJBQU8sVUFBUyxLQUFULEVBQWdCO0FBQ3JCLGdCQUNFLFFBQVEsUUFBUSxJQUFSLENBQWEsU0FBUyxLQUF0QixDQURWO0FBR0EsZ0JBQUcsQ0FBQyxLQUFKLEVBQVc7QUFDVCxxQkFBTyxLQUFQLENBQWEsZ0JBQWIsRUFBK0IsS0FBL0I7QUFDQSxzQkFBUSxVQUFSLENBQW1CLFNBQVMsS0FBNUI7QUFDRCxhQUhELE1BSUssSUFBRyxTQUFTLEtBQVQsSUFBa0IsTUFBTSxLQUFOLENBQXJCLEVBQW1DO0FBQ3RDLHFCQUFPLEtBQVAsQ0FBYSwyQkFBYixFQUEwQyxLQUExQztBQUNBLHFCQUFPLE1BQU0sS0FBTixDQUFQO0FBQ0Esc0JBQVEsSUFBUixDQUFhLFNBQVMsS0FBdEIsRUFBNkIsS0FBN0I7QUFDRDtBQUNGO0FBZEksU0FqbUJBOztBQWtuQlAsY0FBTTtBQUNKLGlCQUFPLFVBQVMsSUFBVCxFQUFlO0FBQ3BCLGdCQUNFLFFBQVEsUUFBUSxJQUFSLENBQWEsU0FBUyxLQUF0QixDQURWO0FBR0EsZ0JBQUcsU0FBUyxLQUFaLEVBQW1CO0FBQ2pCLHFCQUFPLE9BQVAsQ0FBZSw2Q0FBZixFQUE4RCxJQUE5RDtBQUNBLHFCQUFRLE9BQU8sS0FBUCxJQUFnQixRQUFqQixJQUErQixNQUFNLElBQU4sTUFBZ0IsU0FBL0MsR0FDSCxNQUFNLElBQU4sQ0FERyxHQUVILEtBRko7QUFJRDtBQUNELG1CQUFPLEtBQVA7QUFDRDtBQWJHLFNBbG5CQzs7QUFrb0JQLGdCQUFRO0FBQ04sY0FBSSxVQUFTLFdBQVQsRUFBc0IsYUFBdEIsRUFBcUM7QUFDdkMsZ0JBQ0UsV0FBaUIsY0FBYyxDQURqQztBQUFBLGdCO0FBRUUseUJBQWlCLGdCQUFnQixDQUZuQztBQUFBLGdCQUdFLGFBSEY7QUFBQSxnQkFJRSxRQUpGO0FBQUEsZ0JBS0UsRUFMRjtBQU9BLGdCQUFHLGtCQUFrQixTQUFyQixFQUFnQzs7QUFFOUIseUJBQVcsT0FBTyxZQUFQLENBQW9CLEtBQUssYUFBekIsQ0FBWDtBQUNBLG1CQUFjLFdBQVcsUUFBekI7QUFDQSxxQkFBTyxPQUFQLENBQWUsNkJBQWYsRUFBOEMsRUFBOUM7QUFDRCxhQUxELE1BTUs7QUFDSCxtQkFBSyxRQUFMO0FBQ0EscUJBQU8sT0FBUCxDQUFlLG9CQUFmLEVBQXFDLEVBQXJDO0FBQ0Q7QUFDRCxtQkFBTyxFQUFQO0FBQ0QsV0FwQks7QUFxQk4sbUJBQVMsWUFBVztBQUNsQixnQkFBRyxTQUFTLE1BQVQsS0FBb0IsQ0FBdkIsRUFBMEI7QUFDeEIseUJBQVcsRUFBRSxTQUFGLEVBQ1IsUUFEUSxDQUNDLFVBQVUsT0FEWCxFQUVSLFFBRlEsQ0FFQyxPQUZELENBQVg7QUFJRDtBQUNGO0FBNUJLLFNBbG9CRDs7QUFpcUJQLGdCQUFRO0FBQ04sa0JBQVEsVUFBUyxNQUFULEVBQWlCLFdBQWpCLEVBQThCLGFBQTlCLEVBQTZDO0FBQ25ELG1CQUFPLE9BQVAsQ0FBZSwrQkFBZjtBQUNBLGdCQUNFLGtCQUFtQixrQkFBa0IsU0FBbkIsR0FDZCxTQUNHLFFBREgsR0FDYyxFQURkLENBQ2lCLGFBRGpCLEVBRUssUUFGTCxDQUVjLFNBQVMsTUFGdkIsRUFFK0IsRUFGL0IsQ0FFa0MsV0FGbEMsQ0FEYyxHQUlkLFNBQ0csUUFESCxDQUNZLFNBQVMsTUFEckIsRUFDNkIsRUFEN0IsQ0FDZ0MsV0FEaEMsQ0FMTjtBQVFBLG1CQUFPLE9BQVAsQ0FBZSw0QkFBZixFQUE2QyxlQUE3QztBQUNBLDRCQUNHLElBREgsQ0FDUSxTQUFTLE1BRGpCLEVBQ3lCLE1BRHpCO0FBR0QsV0FmSztBQWdCTixjQUFJLFVBQVMsT0FBVCxFQUFrQjtBQUNwQixtQkFBTyxLQUFQLENBQWEsbUNBQWI7QUFDQTs7QUFFRSw0QkFBZ0IsQ0FGbEI7QUFBQSxnQkFHRSxjQUFnQixDQUhsQjtBQUtBLGdCQUFHLFNBQVMsSUFBVCxLQUFrQixVQUFyQixFQUFpQzs7QUFFL0IsZ0JBQUUsSUFBRixDQUFPLE9BQVAsRUFBZ0IsVUFBUyxLQUFULEVBQWdCLFFBQWhCLEVBQTBCO0FBQ3hDLDhCQUFjLENBQWQ7QUFDQSxrQkFBRSxJQUFGLENBQU8sU0FBUyxPQUFoQixFQUF5QixVQUFTLEtBQVQsRUFBZ0IsS0FBaEIsRUFBdUI7QUFDOUMsc0JBQ0UsU0FBUyxTQUFTLE9BQVQsQ0FBaUIsS0FBakIsQ0FEWDtBQUdBLHNCQUFHLE9BQU8sRUFBUCxLQUFjLFNBQWpCLEVBQTRCO0FBQzFCLDJCQUFPLEVBQVAsR0FBWSxPQUFPLE1BQVAsQ0FBYyxFQUFkLENBQWlCLFdBQWpCLEVBQThCLGFBQTlCLENBQVo7QUFDRDtBQUNELHlCQUFPLE1BQVAsQ0FBYyxNQUFkLENBQXFCLE1BQXJCLEVBQTZCLFdBQTdCLEVBQTBDLGFBQTFDO0FBQ0E7QUFDRCxpQkFURDtBQVVBO0FBQ0QsZUFiRDtBQWNELGFBaEJELE1BaUJLOztBQUVILGdCQUFFLElBQUYsQ0FBTyxPQUFQLEVBQWdCLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUF1QjtBQUNyQyxvQkFDRSxTQUFTLFFBQVEsS0FBUixDQURYO0FBR0Esb0JBQUcsT0FBTyxFQUFQLEtBQWMsU0FBakIsRUFBNEI7QUFDMUIseUJBQU8sRUFBUCxHQUFZLE9BQU8sTUFBUCxDQUFjLEVBQWQsQ0FBaUIsV0FBakIsQ0FBWjtBQUNEO0FBQ0QsdUJBQU8sTUFBUCxDQUFjLE1BQWQsQ0FBcUIsTUFBckIsRUFBNkIsV0FBN0I7QUFDQTtBQUNELGVBVEQ7QUFVRDtBQUNELG1CQUFPLE9BQVA7QUFDRDtBQXRESyxTQWpxQkQ7O0FBMHRCUCxjQUFNO0FBQ0osbUJBQVMsVUFBUyxPQUFULEVBQWtCO0FBQ3pCLG1CQUFPLE9BQVAsQ0FBZSwyQ0FBZixFQUE0RCxPQUE1RDtBQUNBLG9CQUFRLElBQVIsQ0FBYSxTQUFTLE9BQXRCLEVBQStCLE9BQS9CO0FBQ0Q7QUFKRyxTQTF0QkM7O0FBaXVCUCxlQUFPO0FBQ0wsaUJBQU8sVUFBUyxJQUFULEVBQWUsS0FBZixFQUFzQjtBQUMzQixnQkFDRSxRQUFTLFFBQVEsSUFBUixDQUFhLFNBQVMsS0FBdEIsTUFBaUMsU0FBbEMsR0FDSixRQUFRLElBQVIsQ0FBYSxTQUFTLEtBQXRCLENBREksR0FFSixFQUhOO0FBS0EsZ0JBQUcsU0FBUyxLQUFaLEVBQW1CO0FBQ2pCLHFCQUFPLE9BQVAsQ0FBZSxpQ0FBZixFQUFrRCxJQUFsRCxFQUF3RCxLQUF4RDtBQUNBLG9CQUFNLElBQU4sSUFBYyxLQUFkO0FBQ0Esc0JBQ0csSUFESCxDQUNRLFNBQVMsS0FEakIsRUFDd0IsS0FEeEI7QUFHRDtBQUNGO0FBZEksU0FqdUJBOztBQWt2QlAsb0JBQVksVUFBUyxJQUFULEVBQWU7QUFDekIsY0FBSSxFQUFFLFVBQUYsQ0FBYSxTQUFTLFlBQXRCLENBQUosRUFBMEM7QUFDeEMsZ0JBQUksU0FBUyxZQUFULENBQXNCLElBQXRCLENBQTJCLFFBQTNCLEVBQXFDLElBQXJDLE1BQStDLEtBQW5ELEVBQTJEO0FBQ3pELHFCQUFPLEtBQVAsQ0FBYSxnREFBYjtBQUNBLHFCQUFPLEtBQVA7QUFDRDtBQUNGO0FBQ0QsY0FBRyxJQUFILEVBQVM7QUFDUCxxQkFDRyxJQURILENBQ1EsSUFEUjtBQUdBLG1CQUFPLGNBQVA7QUFDQSxnQkFBRyxTQUFTLGlCQUFaLEVBQStCO0FBQzdCLHFCQUFPLE1BQVAsQ0FBYyxXQUFkO0FBQ0Q7QUFDRCxtQkFBTyxXQUFQO0FBQ0QsV0FURCxNQVVLO0FBQ0gsbUJBQU8sV0FBUDtBQUNEO0FBQ0YsU0F0d0JNOztBQXd3QlAscUJBQWEsWUFBVztBQUN0QixjQUFHLENBQUMsT0FBTyxFQUFQLENBQVUsT0FBVixFQUFKLEVBQXlCO0FBQ3ZCLGdCQUFJLE9BQU8sR0FBUCxDQUFXLFVBQVgsRUFBSixFQUE4QjtBQUM1QixxQkFBTyxLQUFQLENBQWEscUNBQWI7QUFDQSx1QkFDRyxVQURILENBQ2M7QUFDViwyQkFBYSxTQUFTLFVBQVQsR0FBc0IsS0FEekI7QUFFVix1QkFBYSxTQUFTLEtBRlo7QUFHVix5QkFBYSxTQUFTLE9BSFo7QUFJViwwQkFBYSxTQUFTLFFBSlo7QUFLVix1QkFBYTtBQUxILGVBRGQ7QUFTRCxhQVhELE1BWUs7QUFDSCxxQkFBTyxLQUFQLENBQWEsaUNBQWI7QUFDQSx1QkFDRyxJQURILEdBRUcsTUFGSCxDQUVVLFNBQVMsUUFGbkIsRUFFNkIsU0FBUyxNQUZ0QztBQUlEO0FBQ0QscUJBQVMsYUFBVCxDQUF1QixJQUF2QixDQUE0QixRQUE1QjtBQUNEO0FBQ0YsU0EveEJNO0FBZ3lCUCxxQkFBYSxZQUFXO0FBQ3RCLGNBQUksT0FBTyxFQUFQLENBQVUsT0FBVixFQUFKLEVBQTBCO0FBQ3hCLGdCQUFJLE9BQU8sR0FBUCxDQUFXLFVBQVgsRUFBSixFQUE4QjtBQUM1QixxQkFBTyxLQUFQLENBQWEsb0NBQWI7QUFDQSx1QkFDRyxVQURILENBQ2M7QUFDViwyQkFBYSxTQUFTLFVBQVQsR0FBc0IsTUFEekI7QUFFVix1QkFBYSxTQUFTLEtBRlo7QUFHVix5QkFBYSxTQUFTLE9BSFo7QUFJViwwQkFBYSxTQUFTLFFBSlo7QUFLVix1QkFBYTtBQUxILGVBRGQ7QUFTRCxhQVhELE1BWUs7QUFDSCxxQkFBTyxLQUFQLENBQWEsZ0NBQWI7QUFDQSx1QkFDRyxJQURILEdBRUcsT0FGSCxDQUVXLFNBQVMsUUFGcEIsRUFFOEIsU0FBUyxNQUZ2QztBQUlEO0FBQ0QscUJBQVMsY0FBVCxDQUF3QixJQUF4QixDQUE2QixRQUE3QjtBQUNEO0FBQ0YsU0F2ekJNOztBQXl6QlAseUJBQWlCLFVBQVMsUUFBVCxFQUFtQjtBQUNsQyxpQkFBTyxLQUFQLENBQWEsK0JBQWIsRUFBOEMsUUFBOUM7QUFDQSxjQUNFLFdBQWlCLFNBQVMsU0FBVCxDQUFtQixTQUFTLElBQTVCLENBRG5CO0FBQUEsY0FFRSxpQkFBa0IsRUFBRSxhQUFGLENBQWdCLFNBQVMsT0FBTyxPQUFoQixDQUFoQixLQUE2QyxDQUFDLEVBQUUsYUFBRixDQUFnQixTQUFTLE9BQU8sT0FBaEIsQ0FBaEIsQ0FGbEU7QUFBQSxjQUdFLGdCQUFrQixFQUFFLE9BQUYsQ0FBVSxTQUFTLE9BQU8sT0FBaEIsQ0FBVixLQUF1QyxTQUFTLE9BQU8sT0FBaEIsRUFBeUIsTUFBekIsR0FBa0MsQ0FIN0Y7QUFBQSxjQUlFLE9BQWlCLEVBSm5CO0FBTUEsY0FBRyxrQkFBa0IsYUFBckIsRUFBcUM7QUFDbkMsZ0JBQUcsU0FBUyxVQUFULEdBQXNCLENBQXpCLEVBQTRCO0FBQzFCLGtCQUFHLGNBQUgsRUFBbUI7QUFDakIsb0JBQUcsU0FBUyxJQUFULElBQWlCLFVBQXBCLEVBQWdDO0FBQzlCLHlCQUFPLEtBQVAsQ0FBYSxNQUFNLFVBQW5CO0FBQ0Q7QUFDRixlQUpELE1BS0s7QUFDSCx5QkFBUyxPQUFPLE9BQWhCLElBQTJCLFNBQVMsT0FBTyxPQUFoQixFQUF5QixLQUF6QixDQUErQixDQUEvQixFQUFrQyxTQUFTLFVBQTNDLENBQTNCO0FBQ0Q7QUFDRjtBQUNELGdCQUFHLEVBQUUsVUFBRixDQUFhLFFBQWIsQ0FBSCxFQUEyQjtBQUN6QixxQkFBTyxTQUFTLFFBQVQsRUFBbUIsTUFBbkIsQ0FBUDtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLEtBQVAsQ0FBYSxNQUFNLFVBQW5CLEVBQStCLEtBQS9CO0FBQ0Q7QUFDRixXQWpCRCxNQWtCSyxJQUFHLFNBQVMsYUFBWixFQUEyQjtBQUM5QixtQkFBTyxPQUFPLGNBQVAsQ0FBc0IsTUFBTSxTQUE1QixFQUF1QyxPQUF2QyxDQUFQO0FBQ0Q7QUFDRCxtQkFBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLE9BQXhCLEVBQWlDLFFBQWpDO0FBQ0EsaUJBQU8sSUFBUDtBQUNELFNBeDFCTTs7QUEwMUJQLHdCQUFnQixVQUFTLElBQVQsRUFBZSxJQUFmLEVBQXFCO0FBQ25DLGlCQUFPLFFBQVEsVUFBZjtBQUNBLGlCQUFPLEtBQVAsQ0FBYSxvQkFBYixFQUFtQyxJQUFuQyxFQUF5QyxJQUF6QztBQUNBLGlCQUFPLFVBQVAsQ0FBbUIsU0FBUyxTQUFULENBQW1CLE9BQW5CLENBQTJCLElBQTNCLEVBQWlDLElBQWpDLENBQW5CO0FBQ0EsaUJBQU8sU0FBUyxTQUFULENBQW1CLE9BQW5CLENBQTJCLElBQTNCLEVBQWlDLElBQWpDLENBQVA7QUFDRCxTQS8xQk07O0FBaTJCUCxpQkFBUyxVQUFTLElBQVQsRUFBZSxLQUFmLEVBQXNCO0FBQzdCLGNBQUksRUFBRSxhQUFGLENBQWdCLElBQWhCLENBQUosRUFBNEI7QUFDMUIsY0FBRSxNQUFGLENBQVMsSUFBVCxFQUFlLFFBQWYsRUFBeUIsSUFBekI7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IscUJBQVMsSUFBVCxJQUFpQixLQUFqQjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLFNBQVMsSUFBVCxDQUFQO0FBQ0Q7QUFDRixTQTMyQk07QUE0MkJQLGtCQUFVLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDOUIsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsTUFBZixFQUF1QixJQUF2QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixtQkFBTyxJQUFQLElBQWUsS0FBZjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLE9BQU8sSUFBUCxDQUFQO0FBQ0Q7QUFDRixTQXQzQk07QUF1M0JQLGVBQU8sWUFBVztBQUNoQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsS0FBaEMsRUFBdUM7QUFDckMsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxLQUFQLEdBQWUsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWY7QUFDQSxxQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNEO0FBQ0Y7QUFDRixTQWo0Qk07QUFrNEJQLGlCQUFTLFlBQVc7QUFDbEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLE9BQTdCLElBQXdDLFNBQVMsS0FBcEQsRUFBMkQ7QUFDekQsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxPQUFQLEdBQWlCLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFqQjtBQUNBLHFCQUFPLE9BQVAsQ0FBZSxLQUFmLENBQXFCLE9BQXJCLEVBQThCLFNBQTlCO0FBQ0Q7QUFDRjtBQUNGLFNBNTRCTTtBQTY0QlAsZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQWIsRUFBcUI7QUFDbkIsbUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLEtBQXJDLEVBQTRDLE9BQTVDLEVBQXFELFNBQVMsSUFBVCxHQUFnQixHQUFyRSxDQUFmO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGLFNBbDVCTTtBQW01QlAscUJBQWE7QUFDWCxlQUFLLFVBQVMsT0FBVCxFQUFrQjtBQUNyQixnQkFDRSxXQURGLEVBRUUsYUFGRixFQUdFLFlBSEY7QUFLQSxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIsNEJBQWdCLElBQUksSUFBSixHQUFXLE9BQVgsRUFBaEI7QUFDQSw2QkFBZ0IsUUFBUSxXQUF4QjtBQUNBLDhCQUFnQixjQUFjLFlBQTlCO0FBQ0EscUJBQWdCLFdBQWhCO0FBQ0EsMEJBQVksSUFBWixDQUFpQjtBQUNmLHdCQUFtQixRQUFRLENBQVIsQ0FESjtBQUVmLDZCQUFtQixHQUFHLEtBQUgsQ0FBUyxJQUFULENBQWMsT0FBZCxFQUF1QixDQUF2QixLQUE2QixFQUZqQztBQUdmLDJCQUFtQixPQUhKO0FBSWYsa0NBQW1CO0FBSkosZUFBakI7QUFNRDtBQUNELHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLG1CQUFPLFdBQVAsQ0FBbUIsS0FBbkIsR0FBMkIsV0FBVyxPQUFPLFdBQVAsQ0FBbUIsT0FBOUIsRUFBdUMsR0FBdkMsQ0FBM0I7QUFDRCxXQXJCVTtBQXNCWCxtQkFBUyxZQUFXO0FBQ2xCLGdCQUNFLFFBQVEsU0FBUyxJQUFULEdBQWdCLEdBRDFCO0FBQUEsZ0JBRUUsWUFBWSxDQUZkO0FBSUEsbUJBQU8sS0FBUDtBQUNBLHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLGNBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDJCQUFhLEtBQUssZ0JBQUwsQ0FBYjtBQUNELGFBRkQ7QUFHQSxxQkFBUyxNQUFNLFNBQU4sR0FBa0IsSUFBM0I7QUFDQSxnQkFBRyxjQUFILEVBQW1CO0FBQ2pCLHVCQUFTLFFBQVEsY0FBUixHQUF5QixJQUFsQztBQUNEO0FBQ0QsZ0JBQUcsWUFBWSxNQUFaLEdBQXFCLENBQXhCLEVBQTJCO0FBQ3pCLHVCQUFTLE1BQU0sR0FBTixHQUFZLFlBQVksTUFBeEIsR0FBaUMsR0FBMUM7QUFDRDtBQUNELGdCQUFJLENBQUMsUUFBUSxLQUFSLEtBQWtCLFNBQWxCLElBQStCLFFBQVEsS0FBUixLQUFrQixTQUFsRCxLQUFnRSxZQUFZLE1BQVosR0FBcUIsQ0FBekYsRUFBNEY7QUFDMUYsc0JBQVEsY0FBUixDQUF1QixLQUF2QjtBQUNBLGtCQUFHLFFBQVEsS0FBWCxFQUFrQjtBQUNoQix3QkFBUSxLQUFSLENBQWMsV0FBZDtBQUNELGVBRkQsTUFHSztBQUNILGtCQUFFLElBQUYsQ0FBTyxXQUFQLEVBQW9CLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjtBQUN4QywwQkFBUSxHQUFSLENBQVksS0FBSyxNQUFMLElBQWUsSUFBZixHQUFzQixLQUFLLGdCQUFMLENBQXRCLEdBQTZDLElBQXpEO0FBQ0QsaUJBRkQ7QUFHRDtBQUNELHNCQUFRLFFBQVI7QUFDRDtBQUNELDBCQUFjLEVBQWQ7QUFDRDtBQXBEVSxTQW41Qk47QUF5OEJQLGdCQUFRLFVBQVMsS0FBVCxFQUFnQixlQUFoQixFQUFpQyxPQUFqQyxFQUEwQztBQUNoRCxjQUNFLFNBQVMsUUFEWDtBQUFBLGNBRUUsUUFGRjtBQUFBLGNBR0UsS0FIRjtBQUFBLGNBSUUsUUFKRjtBQU1BLDRCQUFrQixtQkFBbUIsY0FBckM7QUFDQSxvQkFBa0IsV0FBbUIsT0FBckM7QUFDQSxjQUFHLE9BQU8sS0FBUCxJQUFnQixRQUFoQixJQUE0QixXQUFXLFNBQTFDLEVBQXFEO0FBQ25ELG9CQUFXLE1BQU0sS0FBTixDQUFZLE9BQVosQ0FBWDtBQUNBLHVCQUFXLE1BQU0sTUFBTixHQUFlLENBQTFCO0FBQ0EsY0FBRSxJQUFGLENBQU8sS0FBUCxFQUFjLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUF1QjtBQUNuQyxrQkFBSSxpQkFBa0IsU0FBUyxRQUFWLEdBQ2pCLFFBQVEsTUFBTSxRQUFRLENBQWQsRUFBaUIsTUFBakIsQ0FBd0IsQ0FBeEIsRUFBMkIsV0FBM0IsRUFBUixHQUFtRCxNQUFNLFFBQVEsQ0FBZCxFQUFpQixLQUFqQixDQUF1QixDQUF2QixDQURsQyxHQUVqQixLQUZKO0FBSUEsa0JBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sY0FBUCxDQUFqQixLQUE4QyxTQUFTLFFBQTNELEVBQXVFO0FBQ3JFLHlCQUFTLE9BQU8sY0FBUCxDQUFUO0FBQ0QsZUFGRCxNQUdLLElBQUksT0FBTyxjQUFQLE1BQTJCLFNBQS9CLEVBQTJDO0FBQzlDLHdCQUFRLE9BQU8sY0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQSxJQUFJLEVBQUUsYUFBRixDQUFpQixPQUFPLEtBQVAsQ0FBakIsS0FBcUMsU0FBUyxRQUFsRCxFQUE4RDtBQUNqRSx5QkFBUyxPQUFPLEtBQVAsQ0FBVDtBQUNELGVBRkksTUFHQSxJQUFJLE9BQU8sS0FBUCxNQUFrQixTQUF0QixFQUFrQztBQUNyQyx3QkFBUSxPQUFPLEtBQVAsQ0FBUjtBQUNBLHVCQUFPLEtBQVA7QUFDRCxlQUhJLE1BSUE7QUFDSCx1QkFBTyxLQUFQO0FBQ0Q7QUFDRixhQXRCRDtBQXVCRDtBQUNELGNBQUksRUFBRSxVQUFGLENBQWMsS0FBZCxDQUFKLEVBQTRCO0FBQzFCLHVCQUFXLE1BQU0sS0FBTixDQUFZLE9BQVosRUFBcUIsZUFBckIsQ0FBWDtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQix1QkFBVyxLQUFYO0FBQ0Q7QUFDRCxjQUFHLEVBQUUsT0FBRixDQUFVLGFBQVYsQ0FBSCxFQUE2QjtBQUMzQiwwQkFBYyxJQUFkLENBQW1CLFFBQW5CO0FBQ0QsV0FGRCxNQUdLLElBQUcsa0JBQWtCLFNBQXJCLEVBQWdDO0FBQ25DLDRCQUFnQixDQUFDLGFBQUQsRUFBZ0IsUUFBaEIsQ0FBaEI7QUFDRCxXQUZJLE1BR0EsSUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQzlCLDRCQUFnQixRQUFoQjtBQUNEO0FBQ0QsaUJBQU8sS0FBUDtBQUNEO0FBNy9CTSxPQUFUO0FBKy9CQSxVQUFHLGFBQUgsRUFBa0I7QUFDaEIsWUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLGlCQUFPLFVBQVA7QUFDRDtBQUNELGVBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDRCxPQUxELE1BTUs7QUFDSCxZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsbUJBQVMsTUFBVCxDQUFnQixTQUFoQjtBQUNEO0FBQ0QsZUFBTyxVQUFQO0FBQ0Q7QUFFRixLQTdpQ0g7O0FBZ2pDQSxXQUFRLGtCQUFrQixTQUFuQixHQUNILGFBREcsR0FFSCxJQUZKO0FBSUQsR0Fqa0NEOztBQW1rQ0EsSUFBRSxFQUFGLENBQUssTUFBTCxDQUFZLFFBQVosR0FBdUI7O0FBRXJCLFVBQW9CLFFBRkM7QUFHckIsZUFBb0IsUUFIQzs7QUFLckIsWUFBb0IsS0FMQztBQU1yQixXQUFvQixLQU5DO0FBT3JCLGFBQW9CLEtBUEM7QUFRckIsaUJBQW9CLElBUkM7OztBQVdyQixVQUFvQixVQVhDOzs7QUFjckIsbUJBQW9CLENBZEM7OztBQWlCckIsdUJBQW9CLEtBakJDOzs7QUFvQnJCLGlCQUFvQixLQXBCQzs7O0FBdUJyQixZQUFvQixLQXZCQzs7O0FBMEJyQixrQkFBaUIsQ0FDZixPQURlLEVBRWYsYUFGZSxDQTFCSTs7O0FBZ0NyQixrQkFBaUIsRUFoQ0k7OztBQW1DckIsb0JBQWlCLElBbkNJOzs7QUFzQ3JCLGVBQWlCLElBdENJOzs7QUF5Q3JCLGVBQWlCLENBekNJOzs7QUE0Q3JCLGlCQUFpQixHQTVDSTs7O0FBK0NyQixnQkFBaUIsQ0EvQ0k7OztBQWtEckIsV0FBaUIsSUFsREk7OztBQXFEckIsbUJBQWlCLElBckRJOzs7QUF3RHJCLGdCQUFpQixPQXhESTtBQXlEckIsY0FBaUIsR0F6REk7QUEwRHJCLFlBQWlCLGFBMURJOzs7QUE2RHJCLGNBQWlCLEtBN0RJO0FBOERyQixrQkFBaUIsS0E5REk7O0FBZ0VyQixtQkFBaUIsVUFBUyxLQUFULEVBQWUsQ0FBRSxDQWhFYjtBQWlFckIsZUFBaUIsVUFBUyxRQUFULEVBQWtCLENBQUUsQ0FqRWhCOztBQW1FckIsbUJBQWlCLFlBQVUsQ0FBRSxDQW5FUjtBQW9FckIsb0JBQWlCLFlBQVUsQ0FBRSxDQXBFUjs7QUFzRXJCLGVBQVc7QUFDVCxpQkFBWSxXQURIO0FBRVQsY0FBWSxRQUZIO0FBR1QsYUFBWSxPQUhIO0FBSVQsYUFBWSxPQUpIO0FBS1QsY0FBWSxRQUxIO0FBTVQsZUFBWSxTQU5IO0FBT1QsZUFBWSxTQVBIO0FBUVQsZUFBWTtBQVJILEtBdEVVOztBQWlGckIsV0FBUTtBQUNOLGNBQWMseUVBRFI7QUFFTixpQkFBYyxpQ0FGUjtBQUdOLGVBQWMsa0NBSFI7QUFJTixrQkFBYyxrQ0FKUjtBQUtOLGtCQUFjLDBDQUxSO0FBTU4sbUJBQWMseUNBTlI7QUFPTixrQkFBYyxvREFQUjtBQVFOLGNBQWM7QUFSUixLQWpGYTs7QUE0RnJCLGNBQVU7QUFDUixhQUFVLE9BREY7QUFFUixlQUFVLFNBRkY7QUFHUixjQUFVO0FBSEYsS0E1Rlc7O0FBa0dyQixZQUFRO0FBQ04sY0FBYSxxQ0FEUDtBQUVOLGtCQUFhO0FBRlAsS0FsR2E7OztBQXdHckIsWUFBUTtBQUNOLGtCQUFrQixTQURaLEU7QUFFTixvQkFBa0IsTUFGWixFO0FBR04sdUJBQWtCLFNBSFosRTtBQUlOLG1CQUFrQixhQUpaLEU7QUFLTixhQUFrQixPQUxaLEU7QUFNTixhQUFrQixPQU5aLEU7QUFPTixlQUFrQixTQVBaLEU7QUFRTixhQUFrQixPQVJaLEU7QUFTTixXQUFrQixLQVRaLEU7QUFVTixjQUFrQixRQVZaLEU7QUFXTixrQkFBa0IsTUFYWixFO0FBWU4saUJBQWtCLEs7QUFaWixLQXhHYTs7QUF1SHJCLGNBQVc7QUFDVCxjQUFlLFNBRE47QUFFVCxvQkFBZSxnQkFGTjtBQUdULGVBQWUsVUFITjtBQUlULGVBQWUscUJBSk47QUFLVCxnQkFBZSxXQUxOO0FBTVQsY0FBZSxTQU5OO0FBT1QsYUFBZTtBQVBOLEtBdkhVOztBQWlJckIsZUFBVztBQUNULGNBQVEsVUFBUyxNQUFULEVBQWlCO0FBQ3ZCLFlBQ0UsV0FBZSxXQURqQjtBQUFBLFlBRUUsZUFBZSxVQUZqQjtBQUFBLFlBR0UsU0FBZTtBQUNiLGVBQUssT0FEUTtBQUViLGVBQUssTUFGUTtBQUdiLGVBQUssTUFIUTtBQUliLGVBQUssUUFKUTtBQUtiLGVBQUssUUFMUTtBQU1iLGVBQUs7QUFOUSxTQUhqQjtBQUFBLFlBV0UsY0FBZSxVQUFTLEdBQVQsRUFBYztBQUMzQixpQkFBTyxPQUFPLEdBQVAsQ0FBUDtBQUNELFNBYkg7QUFlQSxZQUFHLGFBQWEsSUFBYixDQUFrQixNQUFsQixDQUFILEVBQThCO0FBQzVCLGlCQUFPLE9BQU8sT0FBUCxDQUFlLFFBQWYsRUFBeUIsV0FBekIsQ0FBUDtBQUNEO0FBQ0QsZUFBTyxNQUFQO0FBQ0QsT0FyQlE7QUFzQlQsZUFBUyxVQUFTLE9BQVQsRUFBa0IsSUFBbEIsRUFBd0I7QUFDL0IsWUFDRSxPQUFPLEVBRFQ7QUFHQSxZQUFHLFlBQVksU0FBWixJQUF5QixTQUFTLFNBQXJDLEVBQWdEO0FBQzlDLGtCQUFTLEtBQ0wsc0JBREssR0FDb0IsSUFEcEIsR0FDMkIsSUFEcEM7O0FBSUEsY0FBRyxRQUFRLE9BQVgsRUFBb0I7QUFDbEIsb0JBQVEsS0FDSixxREFESSxHQUVKLDJCQUZJLEdBRTBCLE9BRjFCLEdBRW9DLDRCQUY1QztBQUlELFdBTEQsTUFNSztBQUNILG9CQUFRLCtCQUErQixPQUEvQixHQUF5QyxRQUFqRDtBQUNEO0FBQ0Qsa0JBQVEsUUFBUjtBQUNEO0FBQ0QsZUFBTyxJQUFQO0FBQ0QsT0EzQ1E7QUE0Q1QsZ0JBQVUsVUFBUyxRQUFULEVBQW1CLE1BQW5CLEVBQTJCO0FBQ25DLFlBQ0UsT0FBTyxFQURUO0FBQUEsWUFFRSxTQUFTLEVBQUUsRUFBRixDQUFLLE1BQUwsQ0FBWSxRQUFaLENBQXFCLFNBQXJCLENBQStCLE1BRjFDO0FBSUEsWUFBRyxTQUFTLE9BQU8sZUFBaEIsTUFBcUMsU0FBeEMsRUFBbUQ7OztBQUdqRCxZQUFFLElBQUYsQ0FBTyxTQUFTLE9BQU8sZUFBaEIsQ0FBUCxFQUF5QyxVQUFTLEtBQVQsRUFBZ0IsUUFBaEIsRUFBMEI7QUFDakUsZ0JBQUcsU0FBUyxPQUFPLE9BQWhCLE1BQTZCLFNBQTdCLElBQTBDLFNBQVMsT0FBVCxDQUFpQixNQUFqQixHQUEwQixDQUF2RSxFQUEwRTs7QUFFeEUsc0JBQVMsd0JBQVQ7O0FBRUEsa0JBQUcsU0FBUyxPQUFPLFlBQWhCLE1BQWtDLFNBQXJDLEVBQWdEO0FBQzlDLHdCQUFRLHVCQUF1QixTQUFTLE9BQU8sWUFBaEIsQ0FBdkIsR0FBdUQsUUFBL0Q7QUFDRDs7O0FBR0QsZ0JBQUUsSUFBRixDQUFPLFNBQVMsT0FBaEIsRUFBeUIsVUFBUyxLQUFULEVBQWdCLE1BQWhCLEVBQXdCO0FBQy9DLG9CQUFHLE9BQU8sT0FBTyxHQUFkLENBQUgsRUFBdUI7QUFDckIsMEJBQVMsNkJBQTZCLE9BQU8sT0FBTyxHQUFkLENBQTdCLEdBQWtELElBQTNEO0FBQ0QsaUJBRkQsTUFHSztBQUNILDBCQUFTLG9CQUFUO0FBQ0Q7QUFDRCxvQkFBRyxPQUFPLE9BQU8sS0FBZCxNQUF5QixTQUE1QixFQUF1QztBQUNyQywwQkFBUSxLQUNKLHFCQURJLEdBRUosYUFGSSxHQUVZLE9BQU8sT0FBTyxLQUFkLENBRlosR0FFbUMsSUFGbkMsR0FHSixRQUhKO0FBS0Q7QUFDRCx3QkFBUSx1QkFBUjtBQUNBLG9CQUFHLE9BQU8sT0FBTyxLQUFkLE1BQXlCLFNBQTVCLEVBQXVDO0FBQ3JDLDBCQUFRLHdCQUF3QixPQUFPLE9BQU8sS0FBZCxDQUF4QixHQUErQyxRQUF2RDtBQUNEO0FBQ0Qsb0JBQUcsT0FBTyxPQUFPLEtBQWQsTUFBeUIsU0FBNUIsRUFBdUM7QUFDckMsMEJBQVEsd0JBQXdCLE9BQU8sT0FBTyxLQUFkLENBQXhCLEdBQStDLFFBQXZEO0FBQ0Q7QUFDRCxvQkFBRyxPQUFPLE9BQU8sV0FBZCxNQUErQixTQUFsQyxFQUE2QztBQUMzQywwQkFBUSw4QkFBOEIsT0FBTyxPQUFPLFdBQWQsQ0FBOUIsR0FBMkQsUUFBbkU7QUFDRDtBQUNELHdCQUFTLEtBQ0wsUUFESjtBQUdBLHdCQUFRLE1BQVI7QUFDRCxlQTVCRDtBQTZCQSxzQkFBUyxLQUNMLFFBREo7QUFHRDtBQUNGLFdBM0NEO0FBNENBLGNBQUcsU0FBUyxPQUFPLE1BQWhCLENBQUgsRUFBNEI7QUFDMUIsb0JBQVEsS0FDTixXQURNLEdBQ1EsU0FBUyxPQUFPLE1BQWhCLEVBQXdCLE9BQU8sU0FBL0IsQ0FEUixHQUNvRCxtQkFEcEQsR0FFSixTQUFTLE9BQU8sTUFBaEIsRUFBd0IsT0FBTyxVQUEvQixDQUZJLEdBR04sTUFIRjtBQUlEO0FBQ0QsaUJBQU8sSUFBUDtBQUNEO0FBQ0QsZUFBTyxLQUFQO0FBQ0QsT0F6R1E7QUEwR1QsZ0JBQVUsVUFBUyxRQUFULEVBQW1CLE1BQW5CLEVBQTJCO0FBQ25DLFlBQ0UsT0FBTyxFQURUO0FBR0EsWUFBRyxTQUFTLE9BQU8sT0FBaEIsTUFBNkIsU0FBaEMsRUFBMkM7OztBQUd6QyxZQUFFLElBQUYsQ0FBTyxTQUFTLE9BQU8sT0FBaEIsQ0FBUCxFQUFpQyxVQUFTLEtBQVQsRUFBZ0IsTUFBaEIsRUFBd0I7QUFDdkQsZ0JBQUcsT0FBTyxPQUFPLEdBQWQsQ0FBSCxFQUF1QjtBQUNyQixzQkFBUyw2QkFBNkIsT0FBTyxPQUFPLEdBQWQsQ0FBN0IsR0FBa0QsSUFBM0Q7QUFDRCxhQUZELE1BR0s7QUFDSCxzQkFBUyxvQkFBVDtBQUNEO0FBQ0QsZ0JBQUcsT0FBTyxPQUFPLEtBQWQsTUFBeUIsU0FBNUIsRUFBdUM7QUFDckMsc0JBQVEsS0FDSixxQkFESSxHQUVKLGFBRkksR0FFWSxPQUFPLE9BQU8sS0FBZCxDQUZaLEdBRW1DLElBRm5DLEdBR0osUUFISjtBQUtEO0FBQ0Qsb0JBQVEsdUJBQVI7QUFDQSxnQkFBRyxPQUFPLE9BQU8sS0FBZCxNQUF5QixTQUE1QixFQUF1QztBQUNyQyxzQkFBUSx3QkFBd0IsT0FBTyxPQUFPLEtBQWQsQ0FBeEIsR0FBK0MsUUFBdkQ7QUFDRDtBQUNELGdCQUFHLE9BQU8sT0FBTyxLQUFkLE1BQXlCLFNBQTVCLEVBQXVDO0FBQ3JDLHNCQUFRLHdCQUF3QixPQUFPLE9BQU8sS0FBZCxDQUF4QixHQUErQyxRQUF2RDtBQUNEO0FBQ0QsZ0JBQUcsT0FBTyxPQUFPLFdBQWQsTUFBK0IsU0FBbEMsRUFBNkM7QUFDM0Msc0JBQVEsOEJBQThCLE9BQU8sT0FBTyxXQUFkLENBQTlCLEdBQTJELFFBQW5FO0FBQ0Q7QUFDRCxvQkFBUyxLQUNMLFFBREo7QUFHQSxvQkFBUSxNQUFSO0FBQ0QsV0E1QkQ7O0FBOEJBLGNBQUcsU0FBUyxPQUFPLE1BQWhCLENBQUgsRUFBNEI7QUFDMUIsb0JBQVEsS0FDTixXQURNLEdBQ1EsU0FBUyxPQUFPLE1BQWhCLEVBQXdCLE9BQU8sU0FBL0IsQ0FEUixHQUNvRCxtQkFEcEQsR0FFSixTQUFTLE9BQU8sTUFBaEIsRUFBd0IsT0FBTyxVQUEvQixDQUZJLEdBR04sTUFIRjtBQUlEO0FBQ0QsaUJBQU8sSUFBUDtBQUNEO0FBQ0QsZUFBTyxLQUFQO0FBQ0Q7QUF4SlE7QUFqSVUsR0FBdkI7QUE2UkMsQ0EzMkNBLEVBMjJDRyxNQTMyQ0gsRUEyMkNXLE1BMzJDWCxFQTIyQ21CLFFBMzJDbkIiLCJmaWxlIjoic2VhcmNoLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXG4gKiAjIFNlbWFudGljIFVJIC0gU2VhcmNoXG4gKiBodHRwOi8vZ2l0aHViLmNvbS9zZW1hbnRpYy1vcmcvc2VtYW50aWMtdWkvXG4gKlxuICpcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxuICogaHR0cDovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVFxuICpcbiAqL1xuXG47KGZ1bmN0aW9uICgkLCB3aW5kb3csIGRvY3VtZW50LCB1bmRlZmluZWQpIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cbndpbmRvdyA9ICh0eXBlb2Ygd2luZG93ICE9ICd1bmRlZmluZWQnICYmIHdpbmRvdy5NYXRoID09IE1hdGgpXG4gID8gd2luZG93XG4gIDogKHR5cGVvZiBzZWxmICE9ICd1bmRlZmluZWQnICYmIHNlbGYuTWF0aCA9PSBNYXRoKVxuICAgID8gc2VsZlxuICAgIDogRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKVxuO1xuXG4kLmZuLnNlYXJjaCA9IGZ1bmN0aW9uKHBhcmFtZXRlcnMpIHtcbiAgdmFyXG4gICAgJGFsbE1vZHVsZXMgICAgID0gJCh0aGlzKSxcbiAgICBtb2R1bGVTZWxlY3RvciAgPSAkYWxsTW9kdWxlcy5zZWxlY3RvciB8fCAnJyxcblxuICAgIHRpbWUgICAgICAgICAgICA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpLFxuICAgIHBlcmZvcm1hbmNlICAgICA9IFtdLFxuXG4gICAgcXVlcnkgICAgICAgICAgID0gYXJndW1lbnRzWzBdLFxuICAgIG1ldGhvZEludm9rZWQgICA9ICh0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycpLFxuICAgIHF1ZXJ5QXJndW1lbnRzICA9IFtdLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSxcbiAgICByZXR1cm5lZFZhbHVlXG4gIDtcbiAgJCh0aGlzKVxuICAgIC5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgdmFyXG4gICAgICAgIHNldHRpbmdzICAgICAgICAgID0gKCAkLmlzUGxhaW5PYmplY3QocGFyYW1ldGVycykgKVxuICAgICAgICAgID8gJC5leHRlbmQodHJ1ZSwge30sICQuZm4uc2VhcmNoLnNldHRpbmdzLCBwYXJhbWV0ZXJzKVxuICAgICAgICAgIDogJC5leHRlbmQoe30sICQuZm4uc2VhcmNoLnNldHRpbmdzKSxcblxuICAgICAgICBjbGFzc05hbWUgICAgICAgPSBzZXR0aW5ncy5jbGFzc05hbWUsXG4gICAgICAgIG1ldGFkYXRhICAgICAgICA9IHNldHRpbmdzLm1ldGFkYXRhLFxuICAgICAgICByZWdFeHAgICAgICAgICAgPSBzZXR0aW5ncy5yZWdFeHAsXG4gICAgICAgIGZpZWxkcyAgICAgICAgICA9IHNldHRpbmdzLmZpZWxkcyxcbiAgICAgICAgc2VsZWN0b3IgICAgICAgID0gc2V0dGluZ3Muc2VsZWN0b3IsXG4gICAgICAgIGVycm9yICAgICAgICAgICA9IHNldHRpbmdzLmVycm9yLFxuICAgICAgICBuYW1lc3BhY2UgICAgICAgPSBzZXR0aW5ncy5uYW1lc3BhY2UsXG5cbiAgICAgICAgZXZlbnROYW1lc3BhY2UgID0gJy4nICsgbmFtZXNwYWNlLFxuICAgICAgICBtb2R1bGVOYW1lc3BhY2UgPSBuYW1lc3BhY2UgKyAnLW1vZHVsZScsXG5cbiAgICAgICAgJG1vZHVsZSAgICAgICAgID0gJCh0aGlzKSxcbiAgICAgICAgJHByb21wdCAgICAgICAgID0gJG1vZHVsZS5maW5kKHNlbGVjdG9yLnByb21wdCksXG4gICAgICAgICRzZWFyY2hCdXR0b24gICA9ICRtb2R1bGUuZmluZChzZWxlY3Rvci5zZWFyY2hCdXR0b24pLFxuICAgICAgICAkcmVzdWx0cyAgICAgICAgPSAkbW9kdWxlLmZpbmQoc2VsZWN0b3IucmVzdWx0cyksXG4gICAgICAgICRyZXN1bHQgICAgICAgICA9ICRtb2R1bGUuZmluZChzZWxlY3Rvci5yZXN1bHQpLFxuICAgICAgICAkY2F0ZWdvcnkgICAgICAgPSAkbW9kdWxlLmZpbmQoc2VsZWN0b3IuY2F0ZWdvcnkpLFxuXG4gICAgICAgIGVsZW1lbnQgICAgICAgICA9IHRoaXMsXG4gICAgICAgIGluc3RhbmNlICAgICAgICA9ICRtb2R1bGUuZGF0YShtb2R1bGVOYW1lc3BhY2UpLFxuXG4gICAgICAgIGRpc2FibGVkQnViYmxlZCA9IGZhbHNlLFxuXG4gICAgICAgIG1vZHVsZVxuICAgICAgO1xuXG4gICAgICBtb2R1bGUgPSB7XG5cbiAgICAgICAgaW5pdGlhbGl6ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0luaXRpYWxpemluZyBtb2R1bGUnKTtcbiAgICAgICAgICBtb2R1bGUuZGV0ZXJtaW5lLnNlYXJjaEZpZWxkcygpO1xuICAgICAgICAgIG1vZHVsZS5iaW5kLmV2ZW50cygpO1xuICAgICAgICAgIG1vZHVsZS5zZXQudHlwZSgpO1xuICAgICAgICAgIG1vZHVsZS5jcmVhdGUucmVzdWx0cygpO1xuICAgICAgICAgIG1vZHVsZS5pbnN0YW50aWF0ZSgpO1xuICAgICAgICB9LFxuICAgICAgICBpbnN0YW50aWF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1N0b3JpbmcgaW5zdGFuY2Ugb2YgbW9kdWxlJywgbW9kdWxlKTtcbiAgICAgICAgICBpbnN0YW5jZSA9IG1vZHVsZTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAuZGF0YShtb2R1bGVOYW1lc3BhY2UsIG1vZHVsZSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG4gICAgICAgIGRlc3Ryb3k6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdEZXN0cm95aW5nIGluc3RhbmNlJyk7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLm9mZihldmVudE5hbWVzcGFjZSlcbiAgICAgICAgICAgIC5yZW1vdmVEYXRhKG1vZHVsZU5hbWVzcGFjZSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVmcmVzaDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZWZyZXNoaW5nIHNlbGVjdG9yIGNhY2hlJyk7XG4gICAgICAgICAgJHByb21wdCAgICAgICAgID0gJG1vZHVsZS5maW5kKHNlbGVjdG9yLnByb21wdCk7XG4gICAgICAgICAgJHNlYXJjaEJ1dHRvbiAgID0gJG1vZHVsZS5maW5kKHNlbGVjdG9yLnNlYXJjaEJ1dHRvbik7XG4gICAgICAgICAgJGNhdGVnb3J5ICAgICAgID0gJG1vZHVsZS5maW5kKHNlbGVjdG9yLmNhdGVnb3J5KTtcbiAgICAgICAgICAkcmVzdWx0cyAgICAgICAgPSAkbW9kdWxlLmZpbmQoc2VsZWN0b3IucmVzdWx0cyk7XG4gICAgICAgICAgJHJlc3VsdCAgICAgICAgID0gJG1vZHVsZS5maW5kKHNlbGVjdG9yLnJlc3VsdCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVmcmVzaFJlc3VsdHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICRyZXN1bHRzID0gJG1vZHVsZS5maW5kKHNlbGVjdG9yLnJlc3VsdHMpO1xuICAgICAgICAgICRyZXN1bHQgID0gJG1vZHVsZS5maW5kKHNlbGVjdG9yLnJlc3VsdCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgYmluZDoge1xuICAgICAgICAgIGV2ZW50czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQmluZGluZyBldmVudHMgdG8gc2VhcmNoJyk7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5hdXRvbWF0aWMpIHtcbiAgICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAgIC5vbihtb2R1bGUuZ2V0LmlucHV0RXZlbnQoKSArIGV2ZW50TmFtZXNwYWNlLCBzZWxlY3Rvci5wcm9tcHQsIG1vZHVsZS5ldmVudC5pbnB1dClcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICAkcHJvbXB0XG4gICAgICAgICAgICAgICAgLmF0dHIoJ2F1dG9jb21wbGV0ZScsICdvZmYnKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC8vIHByb21wdFxuICAgICAgICAgICAgICAub24oJ2ZvY3VzJyAgICAgKyBldmVudE5hbWVzcGFjZSwgc2VsZWN0b3IucHJvbXB0LCBtb2R1bGUuZXZlbnQuZm9jdXMpXG4gICAgICAgICAgICAgIC5vbignYmx1cicgICAgICArIGV2ZW50TmFtZXNwYWNlLCBzZWxlY3Rvci5wcm9tcHQsIG1vZHVsZS5ldmVudC5ibHVyKVxuICAgICAgICAgICAgICAub24oJ2tleWRvd24nICAgKyBldmVudE5hbWVzcGFjZSwgc2VsZWN0b3IucHJvbXB0LCBtb2R1bGUuaGFuZGxlS2V5Ym9hcmQpXG4gICAgICAgICAgICAgIC8vIHNlYXJjaCBidXR0b25cbiAgICAgICAgICAgICAgLm9uKCdjbGljaycgICAgICsgZXZlbnROYW1lc3BhY2UsIHNlbGVjdG9yLnNlYXJjaEJ1dHRvbiwgbW9kdWxlLnF1ZXJ5KVxuICAgICAgICAgICAgICAvLyByZXN1bHRzXG4gICAgICAgICAgICAgIC5vbignbW91c2Vkb3duJyArIGV2ZW50TmFtZXNwYWNlLCBzZWxlY3Rvci5yZXN1bHRzLCBtb2R1bGUuZXZlbnQucmVzdWx0Lm1vdXNlZG93bilcbiAgICAgICAgICAgICAgLm9uKCdtb3VzZXVwJyAgICsgZXZlbnROYW1lc3BhY2UsIHNlbGVjdG9yLnJlc3VsdHMsIG1vZHVsZS5ldmVudC5yZXN1bHQubW91c2V1cClcbiAgICAgICAgICAgICAgLm9uKCdjbGljaycgICAgICsgZXZlbnROYW1lc3BhY2UsIHNlbGVjdG9yLnJlc3VsdCwgIG1vZHVsZS5ldmVudC5yZXN1bHQuY2xpY2spXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGRldGVybWluZToge1xuICAgICAgICAgIHNlYXJjaEZpZWxkczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAvLyB0aGlzIG1ha2VzIHN1cmUgJC5leHRlbmQgZG9lcyBub3QgYWRkIHNwZWNpZmllZCBzZWFyY2ggZmllbGRzIHRvIGRlZmF1bHQgZmllbGRzXG4gICAgICAgICAgICAvLyB0aGlzIGlzIHRoZSBvbmx5IHNldHRpbmcgd2hpY2ggc2hvdWxkIG5vdCBleHRlbmQgZGVmYXVsdHNcbiAgICAgICAgICAgIGlmKHBhcmFtZXRlcnMgJiYgcGFyYW1ldGVycy5zZWFyY2hGaWVsZHMgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICBzZXR0aW5ncy5zZWFyY2hGaWVsZHMgPSBwYXJhbWV0ZXJzLnNlYXJjaEZpZWxkcztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZXZlbnQ6IHtcbiAgICAgICAgICBpbnB1dDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnRpbWVyKTtcbiAgICAgICAgICAgIG1vZHVsZS50aW1lciA9IHNldFRpbWVvdXQobW9kdWxlLnF1ZXJ5LCBzZXR0aW5ncy5zZWFyY2hEZWxheSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBmb2N1czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuc2V0LmZvY3VzKCk7XG4gICAgICAgICAgICBpZiggbW9kdWxlLmhhcy5taW5pbXVtQ2hhcmFjdGVycygpICkge1xuICAgICAgICAgICAgICBtb2R1bGUucXVlcnkoKTtcbiAgICAgICAgICAgICAgaWYoIG1vZHVsZS5jYW4uc2hvdygpICkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5zaG93UmVzdWx0cygpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBibHVyOiBmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHBhZ2VMb3N0Rm9jdXMgPSAoZG9jdW1lbnQuYWN0aXZlRWxlbWVudCA9PT0gdGhpcyksXG4gICAgICAgICAgICAgIGNhbGxiYWNrICAgICAgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuY2FuY2VsLnF1ZXJ5KCk7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5mb2N1cygpO1xuICAgICAgICAgICAgICAgIG1vZHVsZS50aW1lciA9IHNldFRpbWVvdXQobW9kdWxlLmhpZGVSZXN1bHRzLCBzZXR0aW5ncy5oaWRlRGVsYXkpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihwYWdlTG9zdEZvY3VzKSB7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKG1vZHVsZS5yZXN1bHRzQ2xpY2tlZCkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0RldGVybWluaW5nIGlmIHVzZXIgYWN0aW9uIGNhdXNlZCBzZWFyY2ggdG8gY2xvc2UnKTtcbiAgICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAgIC5vbmUoJ2NsaWNrLmNsb3NlJyArIGV2ZW50TmFtZXNwYWNlLCBzZWxlY3Rvci5yZXN1bHRzLCBmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgICAgICAgaWYobW9kdWxlLmlzLmluTWVzc2FnZShldmVudCkgfHwgZGlzYWJsZWRCdWJibGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICRwcm9tcHQuZm9jdXMoKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgZGlzYWJsZWRCdWJibGVkID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICBpZiggIW1vZHVsZS5pcy5hbmltYXRpbmcoKSAmJiAhbW9kdWxlLmlzLmhpZGRlbigpKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW5wdXQgYmx1cnJlZCB3aXRob3V0IHVzZXIgYWN0aW9uLCBjbG9zaW5nIHJlc3VsdHMnKTtcbiAgICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHJlc3VsdDoge1xuICAgICAgICAgICAgbW91c2Vkb3duOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnJlc3VsdHNDbGlja2VkID0gdHJ1ZTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBtb3VzZXVwOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnJlc3VsdHNDbGlja2VkID0gZmFsc2U7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2VhcmNoIHJlc3VsdCBzZWxlY3RlZCcpO1xuICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICAkcmVzdWx0ID0gJCh0aGlzKSxcbiAgICAgICAgICAgICAgICAkdGl0bGUgID0gJHJlc3VsdC5maW5kKHNlbGVjdG9yLnRpdGxlKS5lcSgwKSxcbiAgICAgICAgICAgICAgICAkbGluayAgID0gJHJlc3VsdC5pcygnYVtocmVmXScpXG4gICAgICAgICAgICAgICAgICA/ICRyZXN1bHRcbiAgICAgICAgICAgICAgICAgIDogJHJlc3VsdC5maW5kKCdhW2hyZWZdJykuZXEoMCksXG4gICAgICAgICAgICAgICAgaHJlZiAgICA9ICRsaW5rLmF0dHIoJ2hyZWYnKSAgIHx8IGZhbHNlLFxuICAgICAgICAgICAgICAgIHRhcmdldCAgPSAkbGluay5hdHRyKCd0YXJnZXQnKSB8fCBmYWxzZSxcbiAgICAgICAgICAgICAgICB0aXRsZSAgID0gJHRpdGxlLmh0bWwoKSxcbiAgICAgICAgICAgICAgICAvLyB0aXRsZSBpcyB1c2VkIGZvciByZXN1bHQgbG9va3VwXG4gICAgICAgICAgICAgICAgdmFsdWUgICA9ICgkdGl0bGUubGVuZ3RoID4gMClcbiAgICAgICAgICAgICAgICAgID8gJHRpdGxlLnRleHQoKVxuICAgICAgICAgICAgICAgICAgOiBmYWxzZSxcbiAgICAgICAgICAgICAgICByZXN1bHRzID0gbW9kdWxlLmdldC5yZXN1bHRzKCksXG4gICAgICAgICAgICAgICAgcmVzdWx0ICA9ICRyZXN1bHQuZGF0YShtZXRhZGF0YS5yZXN1bHQpIHx8IG1vZHVsZS5nZXQucmVzdWx0KHZhbHVlLCByZXN1bHRzKSxcbiAgICAgICAgICAgICAgICByZXR1cm5lZFZhbHVlXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgaWYoICQuaXNGdW5jdGlvbihzZXR0aW5ncy5vblNlbGVjdCkgKSB7XG4gICAgICAgICAgICAgICAgaWYoc2V0dGluZ3Mub25TZWxlY3QuY2FsbChlbGVtZW50LCByZXN1bHQsIHJlc3VsdHMpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDdXN0b20gb25TZWxlY3QgY2FsbGJhY2sgY2FuY2VsbGVkIGRlZmF1bHQgc2VsZWN0IGFjdGlvbicpO1xuICAgICAgICAgICAgICAgICAgZGlzYWJsZWRCdWJibGVkID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgbW9kdWxlLmhpZGVSZXN1bHRzKCk7XG4gICAgICAgICAgICAgIGlmKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnNldC52YWx1ZSh2YWx1ZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYoaHJlZikge1xuICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdPcGVuaW5nIHNlYXJjaCBsaW5rIGZvdW5kIGluIHJlc3VsdCcsICRsaW5rKTtcbiAgICAgICAgICAgICAgICBpZih0YXJnZXQgPT0gJ19ibGFuaycgfHwgZXZlbnQuY3RybEtleSkge1xuICAgICAgICAgICAgICAgICAgd2luZG93Lm9wZW4oaHJlZik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSAoaHJlZik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBoYW5kbGVLZXlib2FyZDogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIC8vIGZvcmNlIHNlbGVjdG9yIHJlZnJlc2hcbiAgICAgICAgICAgICRyZXN1bHQgICAgICA9ICRtb2R1bGUuZmluZChzZWxlY3Rvci5yZXN1bHQpLFxuICAgICAgICAgICAgJGNhdGVnb3J5ICAgID0gJG1vZHVsZS5maW5kKHNlbGVjdG9yLmNhdGVnb3J5KSxcbiAgICAgICAgICAgIGN1cnJlbnRJbmRleCA9ICRyZXN1bHQuaW5kZXgoICRyZXN1bHQuZmlsdGVyKCcuJyArIGNsYXNzTmFtZS5hY3RpdmUpICksXG4gICAgICAgICAgICByZXN1bHRTaXplICAgPSAkcmVzdWx0Lmxlbmd0aCxcblxuICAgICAgICAgICAga2V5Q29kZSAgICAgID0gZXZlbnQud2hpY2gsXG4gICAgICAgICAgICBrZXlzICAgICAgICAgPSB7XG4gICAgICAgICAgICAgIGJhY2tzcGFjZSA6IDgsXG4gICAgICAgICAgICAgIGVudGVyICAgICA6IDEzLFxuICAgICAgICAgICAgICBlc2NhcGUgICAgOiAyNyxcbiAgICAgICAgICAgICAgdXBBcnJvdyAgIDogMzgsXG4gICAgICAgICAgICAgIGRvd25BcnJvdyA6IDQwXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgbmV3SW5kZXhcbiAgICAgICAgICA7XG4gICAgICAgICAgLy8gc2VhcmNoIHNob3J0Y3V0c1xuICAgICAgICAgIGlmKGtleUNvZGUgPT0ga2V5cy5lc2NhcGUpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdFc2NhcGUga2V5IHByZXNzZWQsIGJsdXJyaW5nIHNlYXJjaCBmaWVsZCcpO1xuICAgICAgICAgICAgbW9kdWxlLnRyaWdnZXIuYmx1cigpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiggbW9kdWxlLmlzLnZpc2libGUoKSApIHtcbiAgICAgICAgICAgIGlmKGtleUNvZGUgPT0ga2V5cy5lbnRlcikge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnRW50ZXIga2V5IHByZXNzZWQsIHNlbGVjdGluZyBhY3RpdmUgcmVzdWx0Jyk7XG4gICAgICAgICAgICAgIGlmKCAkcmVzdWx0LmZpbHRlcignLicgKyBjbGFzc05hbWUuYWN0aXZlKS5sZW5ndGggPiAwICkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5ldmVudC5yZXN1bHQuY2xpY2suY2FsbCgkcmVzdWx0LmZpbHRlcignLicgKyBjbGFzc05hbWUuYWN0aXZlKSwgZXZlbnQpO1xuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKGtleUNvZGUgPT0ga2V5cy51cEFycm93KSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdVcCBrZXkgcHJlc3NlZCwgY2hhbmdpbmcgYWN0aXZlIHJlc3VsdCcpO1xuICAgICAgICAgICAgICBuZXdJbmRleCA9IChjdXJyZW50SW5kZXggLSAxIDwgMClcbiAgICAgICAgICAgICAgICA/IGN1cnJlbnRJbmRleFxuICAgICAgICAgICAgICAgIDogY3VycmVudEluZGV4IC0gMVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgICRjYXRlZ29yeVxuICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuYWN0aXZlKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgICRyZXN1bHRcbiAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSlcbiAgICAgICAgICAgICAgICAuZXEobmV3SW5kZXgpXG4gICAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSlcbiAgICAgICAgICAgICAgICAgIC5jbG9zZXN0KCRjYXRlZ29yeSlcbiAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5hY3RpdmUpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoa2V5Q29kZSA9PSBrZXlzLmRvd25BcnJvdykge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnRG93biBrZXkgcHJlc3NlZCwgY2hhbmdpbmcgYWN0aXZlIHJlc3VsdCcpO1xuICAgICAgICAgICAgICBuZXdJbmRleCA9IChjdXJyZW50SW5kZXggKyAxID49IHJlc3VsdFNpemUpXG4gICAgICAgICAgICAgICAgPyBjdXJyZW50SW5kZXhcbiAgICAgICAgICAgICAgICA6IGN1cnJlbnRJbmRleCArIDFcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICAkY2F0ZWdvcnlcbiAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICAkcmVzdWx0XG4gICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5hY3RpdmUpXG4gICAgICAgICAgICAgICAgLmVxKG5ld0luZGV4KVxuICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5hY3RpdmUpXG4gICAgICAgICAgICAgICAgICAuY2xvc2VzdCgkY2F0ZWdvcnkpXG4gICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUuYWN0aXZlKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgLy8gcXVlcnkgc2hvcnRjdXRzXG4gICAgICAgICAgICBpZihrZXlDb2RlID09IGtleXMuZW50ZXIpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0VudGVyIGtleSBwcmVzc2VkLCBleGVjdXRpbmcgcXVlcnknKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnF1ZXJ5KCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5zZXQuYnV0dG9uUHJlc3NlZCgpO1xuICAgICAgICAgICAgICAkcHJvbXB0Lm9uZSgna2V5dXAnLCBtb2R1bGUucmVtb3ZlLmJ1dHRvbkZvY3VzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0dXA6IHtcbiAgICAgICAgICBhcGk6IGZ1bmN0aW9uKHNlYXJjaFRlcm0pIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBhcGlTZXR0aW5ncyA9IHtcbiAgICAgICAgICAgICAgICBkZWJ1ZyAgICAgICAgICAgICA6IHNldHRpbmdzLmRlYnVnLFxuICAgICAgICAgICAgICAgIG9uICAgICAgICAgICAgICAgIDogZmFsc2UsXG4gICAgICAgICAgICAgICAgY2FjaGUgICAgICAgICAgICAgOiB0cnVlLFxuICAgICAgICAgICAgICAgIGFjdGlvbiAgICAgICAgICAgIDogJ3NlYXJjaCcsXG4gICAgICAgICAgICAgICAgdXJsRGF0YSAgICAgICAgICAgOiB7XG4gICAgICAgICAgICAgICAgICBxdWVyeSA6IHNlYXJjaFRlcm1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9uU3VjY2VzcyAgICAgICAgIDogZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5wYXJzZS5yZXNwb25zZS5jYWxsKGVsZW1lbnQsIHJlc3BvbnNlLCBzZWFyY2hUZXJtKTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9uQWJvcnQgICAgICAgICAgIDogZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9uRmFpbHVyZSAgICAgICAgIDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuZGlzcGxheU1lc3NhZ2UoZXJyb3Iuc2VydmVyRXJyb3IpO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgb25FcnJvciAgICAgICAgICAgOiBtb2R1bGUuZXJyb3JcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgc2VhcmNoSFRNTFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgYXBpU2V0dGluZ3MsIHNldHRpbmdzLmFwaVNldHRpbmdzKTtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIHVwIEFQSSByZXF1ZXN0JywgYXBpU2V0dGluZ3MpO1xuICAgICAgICAgICAgJG1vZHVsZS5hcGkoYXBpU2V0dGluZ3MpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBjYW46IHtcbiAgICAgICAgICB1c2VBUEk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICQuZm4uYXBpICE9PSB1bmRlZmluZWQ7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzaG93OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGUuaXMuZm9jdXNlZCgpICYmICFtb2R1bGUuaXMudmlzaWJsZSgpICYmICFtb2R1bGUuaXMuZW1wdHkoKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHRyYW5zaXRpb246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIHNldHRpbmdzLnRyYW5zaXRpb24gJiYgJC5mbi50cmFuc2l0aW9uICE9PSB1bmRlZmluZWQgJiYgJG1vZHVsZS50cmFuc2l0aW9uKCdpcyBzdXBwb3J0ZWQnKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgaXM6IHtcbiAgICAgICAgICBhbmltYXRpbmc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRyZXN1bHRzLmhhc0NsYXNzKGNsYXNzTmFtZS5hbmltYXRpbmcpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgaGlkZGVuOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkcmVzdWx0cy5oYXNDbGFzcyhjbGFzc05hbWUuaGlkZGVuKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGluTWVzc2FnZTogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgIHJldHVybiAoZXZlbnQudGFyZ2V0ICYmICQoZXZlbnQudGFyZ2V0KS5jbG9zZXN0KHNlbGVjdG9yLm1lc3NhZ2UpLmxlbmd0aCA+IDApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZW1wdHk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICgkcmVzdWx0cy5odG1sKCkgPT09ICcnKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHZpc2libGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICgkcmVzdWx0cy5maWx0ZXIoJzp2aXNpYmxlJykubGVuZ3RoID4gMCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBmb2N1c2VkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAoJHByb21wdC5maWx0ZXIoJzpmb2N1cycpLmxlbmd0aCA+IDApO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICB0cmlnZ2VyOiB7XG4gICAgICAgICAgYmx1cjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgZXZlbnRzICAgICAgICA9IGRvY3VtZW50LmNyZWF0ZUV2ZW50KCdIVE1MRXZlbnRzJyksXG4gICAgICAgICAgICAgIHByb21wdEVsZW1lbnQgPSAkcHJvbXB0WzBdXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihwcm9tcHRFbGVtZW50KSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdUcmlnZ2VyaW5nIG5hdGl2ZSBibHVyIGV2ZW50Jyk7XG4gICAgICAgICAgICAgIGV2ZW50cy5pbml0RXZlbnQoJ2JsdXInLCBmYWxzZSwgZmFsc2UpO1xuICAgICAgICAgICAgICBwcm9tcHRFbGVtZW50LmRpc3BhdGNoRXZlbnQoZXZlbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0OiB7XG4gICAgICAgICAgaW5wdXRFdmVudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgcHJvbXB0ID0gJHByb21wdFswXSxcbiAgICAgICAgICAgICAgaW5wdXRFdmVudCAgID0gKHByb21wdCAhPT0gdW5kZWZpbmVkICYmIHByb21wdC5vbmlucHV0ICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgICAgPyAnaW5wdXQnXG4gICAgICAgICAgICAgICAgOiAocHJvbXB0ICE9PSB1bmRlZmluZWQgJiYgcHJvbXB0Lm9ucHJvcGVydHljaGFuZ2UgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICAgID8gJ3Byb3BlcnR5Y2hhbmdlJ1xuICAgICAgICAgICAgICAgICAgOiAna2V5dXAnXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICByZXR1cm4gaW5wdXRFdmVudDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkcHJvbXB0LnZhbCgpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcmVzdWx0czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgcmVzdWx0cyA9ICRtb2R1bGUuZGF0YShtZXRhZGF0YS5yZXN1bHRzKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdHM7XG4gICAgICAgICAgfSxcbiAgICAgICAgICByZXN1bHQ6IGZ1bmN0aW9uKHZhbHVlLCByZXN1bHRzKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgbG9va3VwRmllbGRzID0gWyd0aXRsZScsICdpZCddLFxuICAgICAgICAgICAgICByZXN1bHQgICAgICAgPSBmYWxzZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgdmFsdWUgPSAodmFsdWUgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgPyB2YWx1ZVxuICAgICAgICAgICAgICA6IG1vZHVsZS5nZXQudmFsdWUoKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgcmVzdWx0cyA9IChyZXN1bHRzICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgID8gcmVzdWx0c1xuICAgICAgICAgICAgICA6IG1vZHVsZS5nZXQucmVzdWx0cygpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy50eXBlID09PSAnY2F0ZWdvcnknKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRmluZGluZyByZXN1bHQgdGhhdCBtYXRjaGVzJywgdmFsdWUpO1xuICAgICAgICAgICAgICAkLmVhY2gocmVzdWx0cywgZnVuY3Rpb24oaW5kZXgsIGNhdGVnb3J5KSB7XG4gICAgICAgICAgICAgICAgaWYoJC5pc0FycmF5KGNhdGVnb3J5LnJlc3VsdHMpKSB7XG4gICAgICAgICAgICAgICAgICByZXN1bHQgPSBtb2R1bGUuc2VhcmNoLm9iamVjdCh2YWx1ZSwgY2F0ZWdvcnkucmVzdWx0cywgbG9va3VwRmllbGRzKVswXTtcbiAgICAgICAgICAgICAgICAgIC8vIGRvbid0IGNvbnRpbnVlIHNlYXJjaGluZyBpZiBhIHJlc3VsdCBpcyBmb3VuZFxuICAgICAgICAgICAgICAgICAgaWYocmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRmluZGluZyByZXN1bHQgaW4gcmVzdWx0cyBvYmplY3QnLCB2YWx1ZSk7XG4gICAgICAgICAgICAgIHJlc3VsdCA9IG1vZHVsZS5zZWFyY2gub2JqZWN0KHZhbHVlLCByZXN1bHRzLCBsb29rdXBGaWVsZHMpWzBdO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdCB8fCBmYWxzZTtcbiAgICAgICAgICB9LFxuICAgICAgICB9LFxuXG4gICAgICAgIHNlbGVjdDoge1xuICAgICAgICAgIGZpcnN0UmVzdWx0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZWxlY3RpbmcgZmlyc3QgcmVzdWx0Jyk7XG4gICAgICAgICAgICAkcmVzdWx0LmZpcnN0KCkuYWRkQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldDoge1xuICAgICAgICAgIGZvY3VzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRtb2R1bGUuYWRkQ2xhc3MoY2xhc3NOYW1lLmZvY3VzKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGxvYWRpbmc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZS5hZGRDbGFzcyhjbGFzc05hbWUubG9hZGluZyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB2YWx1ZTogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIHNlYXJjaCBpbnB1dCB2YWx1ZScsIHZhbHVlKTtcbiAgICAgICAgICAgICRwcm9tcHRcbiAgICAgICAgICAgICAgLnZhbCh2YWx1ZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHR5cGU6IGZ1bmN0aW9uKHR5cGUpIHtcbiAgICAgICAgICAgIHR5cGUgPSB0eXBlIHx8IHNldHRpbmdzLnR5cGU7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy50eXBlID09ICdjYXRlZ29yeScpIHtcbiAgICAgICAgICAgICAgJG1vZHVsZS5hZGRDbGFzcyhzZXR0aW5ncy50eXBlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGJ1dHRvblByZXNzZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJHNlYXJjaEJ1dHRvbi5hZGRDbGFzcyhjbGFzc05hbWUucHJlc3NlZCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlbW92ZToge1xuICAgICAgICAgIGxvYWRpbmc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZS5yZW1vdmVDbGFzcyhjbGFzc05hbWUubG9hZGluZyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBmb2N1czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkbW9kdWxlLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5mb2N1cyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBidXR0b25QcmVzc2VkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRzZWFyY2hCdXR0b24ucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLnByZXNzZWQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBxdWVyeTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICBzZWFyY2hUZXJtID0gbW9kdWxlLmdldC52YWx1ZSgpLFxuICAgICAgICAgICAgY2FjaGUgPSBtb2R1bGUucmVhZC5jYWNoZShzZWFyY2hUZXJtKVxuICAgICAgICAgIDtcbiAgICAgICAgICBpZiggbW9kdWxlLmhhcy5taW5pbXVtQ2hhcmFjdGVycygpICkgIHtcbiAgICAgICAgICAgIGlmKGNhY2hlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnUmVhZGluZyByZXN1bHQgZnJvbSBjYWNoZScsIHNlYXJjaFRlcm0pO1xuICAgICAgICAgICAgICBtb2R1bGUuc2F2ZS5yZXN1bHRzKGNhY2hlLnJlc3VsdHMpO1xuICAgICAgICAgICAgICBtb2R1bGUuYWRkUmVzdWx0cyhjYWNoZS5odG1sKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmluamVjdC5pZChjYWNoZS5yZXN1bHRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1F1ZXJ5aW5nIGZvcicsIHNlYXJjaFRlcm0pO1xuICAgICAgICAgICAgICBpZigkLmlzUGxhaW5PYmplY3Qoc2V0dGluZ3Muc291cmNlKSB8fCAkLmlzQXJyYXkoc2V0dGluZ3Muc291cmNlKSkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5zZWFyY2gubG9jYWwoc2VhcmNoVGVybSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggbW9kdWxlLmNhbi51c2VBUEkoKSApIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuc2VhcmNoLnJlbW90ZShzZWFyY2hUZXJtKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3Iuc291cmNlKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc2V0dGluZ3Mub25TZWFyY2hRdWVyeS5jYWxsKGVsZW1lbnQsIHNlYXJjaFRlcm0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIG1vZHVsZS5oaWRlUmVzdWx0cygpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZWFyY2g6IHtcbiAgICAgICAgICBsb2NhbDogZnVuY3Rpb24oc2VhcmNoVGVybSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHJlc3VsdHMgPSBtb2R1bGUuc2VhcmNoLm9iamVjdChzZWFyY2hUZXJtLCBzZXR0aW5ncy5jb250ZW50KSxcbiAgICAgICAgICAgICAgc2VhcmNoSFRNTFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnNldC5sb2FkaW5nKCk7XG4gICAgICAgICAgICBtb2R1bGUuc2F2ZS5yZXN1bHRzKHJlc3VsdHMpO1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZXR1cm5lZCBsb2NhbCBzZWFyY2ggcmVzdWx0cycsIHJlc3VsdHMpO1xuXG4gICAgICAgICAgICBzZWFyY2hIVE1MID0gbW9kdWxlLmdlbmVyYXRlUmVzdWx0cyh7XG4gICAgICAgICAgICAgIHJlc3VsdHM6IHJlc3VsdHNcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5sb2FkaW5nKCk7XG4gICAgICAgICAgICBtb2R1bGUuYWRkUmVzdWx0cyhzZWFyY2hIVE1MKTtcbiAgICAgICAgICAgIG1vZHVsZS5pbmplY3QuaWQocmVzdWx0cyk7XG4gICAgICAgICAgICBtb2R1bGUud3JpdGUuY2FjaGUoc2VhcmNoVGVybSwge1xuICAgICAgICAgICAgICBodG1sICAgIDogc2VhcmNoSFRNTCxcbiAgICAgICAgICAgICAgcmVzdWx0cyA6IHJlc3VsdHNcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcmVtb3RlOiBmdW5jdGlvbihzZWFyY2hUZXJtKSB7XG4gICAgICAgICAgICBpZigkbW9kdWxlLmFwaSgnaXMgbG9hZGluZycpKSB7XG4gICAgICAgICAgICAgICRtb2R1bGUuYXBpKCdhYm9ydCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLnNldHVwLmFwaShzZWFyY2hUZXJtKTtcbiAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgLmFwaSgncXVlcnknKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgb2JqZWN0OiBmdW5jdGlvbihzZWFyY2hUZXJtLCBzb3VyY2UsIHNlYXJjaEZpZWxkcykge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHJlc3VsdHMgICAgICA9IFtdLFxuICAgICAgICAgICAgICBmdXp6eVJlc3VsdHMgPSBbXSxcbiAgICAgICAgICAgICAgc2VhcmNoRXhwICAgID0gc2VhcmNoVGVybS50b1N0cmluZygpLnJlcGxhY2UocmVnRXhwLmVzY2FwZSwgJ1xcXFwkJicpLFxuICAgICAgICAgICAgICBtYXRjaFJlZ0V4cCAgPSBuZXcgUmVnRXhwKHJlZ0V4cC5iZWdpbnNXaXRoICsgc2VhcmNoRXhwLCAnaScpLFxuXG4gICAgICAgICAgICAgIC8vIGF2b2lkIGR1cGxpY2F0ZXMgd2hlbiBwdXNoaW5nIHJlc3VsdHNcbiAgICAgICAgICAgICAgYWRkUmVzdWx0ID0gZnVuY3Rpb24oYXJyYXksIHJlc3VsdCkge1xuICAgICAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAgICAgbm90UmVzdWx0ICAgICAgPSAoJC5pbkFycmF5KHJlc3VsdCwgcmVzdWx0cykgPT0gLTEpLFxuICAgICAgICAgICAgICAgICAgbm90RnV6enlSZXN1bHQgPSAoJC5pbkFycmF5KHJlc3VsdCwgZnV6enlSZXN1bHRzKSA9PSAtMSlcbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgICAgaWYobm90UmVzdWx0ICYmIG5vdEZ1enp5UmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgICBhcnJheS5wdXNoKHJlc3VsdCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBzb3VyY2UgPSBzb3VyY2UgfHwgc2V0dGluZ3Muc291cmNlO1xuICAgICAgICAgICAgc2VhcmNoRmllbGRzID0gKHNlYXJjaEZpZWxkcyAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICA/IHNlYXJjaEZpZWxkc1xuICAgICAgICAgICAgICA6IHNldHRpbmdzLnNlYXJjaEZpZWxkc1xuICAgICAgICAgICAgO1xuXG4gICAgICAgICAgICAvLyBzZWFyY2ggZmllbGRzIHNob3VsZCBiZSBhcnJheSB0byBsb29wIGNvcnJlY3RseVxuICAgICAgICAgICAgaWYoISQuaXNBcnJheShzZWFyY2hGaWVsZHMpKSB7XG4gICAgICAgICAgICAgIHNlYXJjaEZpZWxkcyA9IFtzZWFyY2hGaWVsZHNdO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBleGl0IGNvbmRpdGlvbnMgaWYgbm8gc291cmNlXG4gICAgICAgICAgICBpZihzb3VyY2UgPT09IHVuZGVmaW5lZCB8fCBzb3VyY2UgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5zb3VyY2UpO1xuICAgICAgICAgICAgICByZXR1cm4gW107XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIGl0ZXJhdGUgdGhyb3VnaCBzZWFyY2ggZmllbGRzIGxvb2tpbmcgZm9yIG1hdGNoZXNcbiAgICAgICAgICAgICQuZWFjaChzZWFyY2hGaWVsZHMsIGZ1bmN0aW9uKGluZGV4LCBmaWVsZCkge1xuICAgICAgICAgICAgICAkLmVhY2goc291cmNlLCBmdW5jdGlvbihsYWJlbCwgY29udGVudCkge1xuICAgICAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAgICAgZmllbGRFeGlzdHMgPSAodHlwZW9mIGNvbnRlbnRbZmllbGRdID09ICdzdHJpbmcnKVxuICAgICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgICBpZihmaWVsZEV4aXN0cykge1xuICAgICAgICAgICAgICAgICAgaWYoIGNvbnRlbnRbZmllbGRdLnNlYXJjaChtYXRjaFJlZ0V4cCkgIT09IC0xKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGNvbnRlbnQgc3RhcnRzIHdpdGggdmFsdWUgKGZpcnN0IGluIHJlc3VsdHMpXG4gICAgICAgICAgICAgICAgICAgIGFkZFJlc3VsdChyZXN1bHRzLCBjb250ZW50KTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGVsc2UgaWYoc2V0dGluZ3Muc2VhcmNoRnVsbFRleHQgJiYgbW9kdWxlLmZ1enp5U2VhcmNoKHNlYXJjaFRlcm0sIGNvbnRlbnRbZmllbGRdKSApIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gY29udGVudCBmdXp6eSBtYXRjaGVzIChsYXN0IGluIHJlc3VsdHMpXG4gICAgICAgICAgICAgICAgICAgIGFkZFJlc3VsdChmdXp6eVJlc3VsdHMsIGNvbnRlbnQpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJldHVybiAkLm1lcmdlKHJlc3VsdHMsIGZ1enp5UmVzdWx0cyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGZ1enp5U2VhcmNoOiBmdW5jdGlvbihxdWVyeSwgdGVybSkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgdGVybUxlbmd0aCAgPSB0ZXJtLmxlbmd0aCxcbiAgICAgICAgICAgIHF1ZXJ5TGVuZ3RoID0gcXVlcnkubGVuZ3RoXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKHR5cGVvZiBxdWVyeSAhPT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcXVlcnkgPSBxdWVyeS50b0xvd2VyQ2FzZSgpO1xuICAgICAgICAgIHRlcm0gID0gdGVybS50b0xvd2VyQ2FzZSgpO1xuICAgICAgICAgIGlmKHF1ZXJ5TGVuZ3RoID4gdGVybUxlbmd0aCkge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZihxdWVyeUxlbmd0aCA9PT0gdGVybUxlbmd0aCkge1xuICAgICAgICAgICAgcmV0dXJuIChxdWVyeSA9PT0gdGVybSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHNlYXJjaDogZm9yICh2YXIgY2hhcmFjdGVySW5kZXggPSAwLCBuZXh0Q2hhcmFjdGVySW5kZXggPSAwOyBjaGFyYWN0ZXJJbmRleCA8IHF1ZXJ5TGVuZ3RoOyBjaGFyYWN0ZXJJbmRleCsrKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgcXVlcnlDaGFyYWN0ZXIgPSBxdWVyeS5jaGFyQ29kZUF0KGNoYXJhY3RlckluZGV4KVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgd2hpbGUobmV4dENoYXJhY3RlckluZGV4IDwgdGVybUxlbmd0aCkge1xuICAgICAgICAgICAgICBpZih0ZXJtLmNoYXJDb2RlQXQobmV4dENoYXJhY3RlckluZGV4KyspID09PSBxdWVyeUNoYXJhY3Rlcikge1xuICAgICAgICAgICAgICAgIGNvbnRpbnVlIHNlYXJjaDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfSxcblxuICAgICAgICBwYXJzZToge1xuICAgICAgICAgIHJlc3BvbnNlOiBmdW5jdGlvbihyZXNwb25zZSwgc2VhcmNoVGVybSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHNlYXJjaEhUTUwgPSBtb2R1bGUuZ2VuZXJhdGVSZXN1bHRzKHJlc3BvbnNlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1BhcnNpbmcgc2VydmVyIHJlc3BvbnNlJywgcmVzcG9uc2UpO1xuICAgICAgICAgICAgaWYocmVzcG9uc2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICBpZihzZWFyY2hUZXJtICE9PSB1bmRlZmluZWQgJiYgcmVzcG9uc2VbZmllbGRzLnJlc3VsdHNdICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuYWRkUmVzdWx0cyhzZWFyY2hIVE1MKTtcbiAgICAgICAgICAgICAgICBtb2R1bGUuaW5qZWN0LmlkKHJlc3BvbnNlW2ZpZWxkcy5yZXN1bHRzXSk7XG4gICAgICAgICAgICAgICAgbW9kdWxlLndyaXRlLmNhY2hlKHNlYXJjaFRlcm0sIHtcbiAgICAgICAgICAgICAgICAgIGh0bWwgICAgOiBzZWFyY2hIVE1MLFxuICAgICAgICAgICAgICAgICAgcmVzdWx0cyA6IHJlc3BvbnNlW2ZpZWxkcy5yZXN1bHRzXVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIG1vZHVsZS5zYXZlLnJlc3VsdHMocmVzcG9uc2VbZmllbGRzLnJlc3VsdHNdKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBjYW5jZWw6IHtcbiAgICAgICAgICBxdWVyeTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZiggbW9kdWxlLmNhbi51c2VBUEkoKSApIHtcbiAgICAgICAgICAgICAgJG1vZHVsZS5hcGkoJ2Fib3J0Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGhhczoge1xuICAgICAgICAgIG1pbmltdW1DaGFyYWN0ZXJzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBzZWFyY2hUZXJtICAgID0gbW9kdWxlLmdldC52YWx1ZSgpLFxuICAgICAgICAgICAgICBudW1DaGFyYWN0ZXJzID0gc2VhcmNoVGVybS5sZW5ndGhcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHJldHVybiAobnVtQ2hhcmFjdGVycyA+PSBzZXR0aW5ncy5taW5DaGFyYWN0ZXJzKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2xlYXI6IHtcbiAgICAgICAgICBjYWNoZTogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBjYWNoZSA9ICRtb2R1bGUuZGF0YShtZXRhZGF0YS5jYWNoZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKCF2YWx1ZSkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NsZWFyaW5nIGNhY2hlJywgdmFsdWUpO1xuICAgICAgICAgICAgICAkbW9kdWxlLnJlbW92ZURhdGEobWV0YWRhdGEuY2FjaGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZih2YWx1ZSAmJiBjYWNoZSAmJiBjYWNoZVt2YWx1ZV0pIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZW1vdmluZyB2YWx1ZSBmcm9tIGNhY2hlJywgdmFsdWUpO1xuICAgICAgICAgICAgICBkZWxldGUgY2FjaGVbdmFsdWVdO1xuICAgICAgICAgICAgICAkbW9kdWxlLmRhdGEobWV0YWRhdGEuY2FjaGUsIGNhY2hlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVhZDoge1xuICAgICAgICAgIGNhY2hlOiBmdW5jdGlvbihuYW1lKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgY2FjaGUgPSAkbW9kdWxlLmRhdGEobWV0YWRhdGEuY2FjaGUpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5jYWNoZSkge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQ2hlY2tpbmcgY2FjaGUgZm9yIGdlbmVyYXRlZCBodG1sIGZvciBxdWVyeScsIG5hbWUpO1xuICAgICAgICAgICAgICByZXR1cm4gKHR5cGVvZiBjYWNoZSA9PSAnb2JqZWN0JykgJiYgKGNhY2hlW25hbWVdICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgICAgPyBjYWNoZVtuYW1lXVxuICAgICAgICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBjcmVhdGU6IHtcbiAgICAgICAgICBpZDogZnVuY3Rpb24ocmVzdWx0SW5kZXgsIGNhdGVnb3J5SW5kZXgpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICByZXN1bHRJRCAgICAgID0gKHJlc3VsdEluZGV4ICsgMSksIC8vIG5vdCB6ZXJvIGluZGV4ZWRcbiAgICAgICAgICAgICAgY2F0ZWdvcnlJRCAgICA9IChjYXRlZ29yeUluZGV4ICsgMSksXG4gICAgICAgICAgICAgIGZpcnN0Q2hhckNvZGUsXG4gICAgICAgICAgICAgIGxldHRlcklELFxuICAgICAgICAgICAgICBpZFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoY2F0ZWdvcnlJbmRleCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIC8vIHN0YXJ0IGNoYXIgY29kZSBmb3IgXCJBXCJcbiAgICAgICAgICAgICAgbGV0dGVySUQgPSBTdHJpbmcuZnJvbUNoYXJDb2RlKDk3ICsgY2F0ZWdvcnlJbmRleCk7XG4gICAgICAgICAgICAgIGlkICAgICAgICAgID0gbGV0dGVySUQgKyByZXN1bHRJRDtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0NyZWF0aW5nIGNhdGVnb3J5IHJlc3VsdCBpZCcsIGlkKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBpZCA9IHJlc3VsdElEO1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQ3JlYXRpbmcgcmVzdWx0IGlkJywgaWQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGlkO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcmVzdWx0czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZigkcmVzdWx0cy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgICAgJHJlc3VsdHMgPSAkKCc8ZGl2IC8+JylcbiAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLnJlc3VsdHMpXG4gICAgICAgICAgICAgICAgLmFwcGVuZFRvKCRtb2R1bGUpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5qZWN0OiB7XG4gICAgICAgICAgcmVzdWx0OiBmdW5jdGlvbihyZXN1bHQsIHJlc3VsdEluZGV4LCBjYXRlZ29yeUluZGV4KSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnSW5qZWN0aW5nIHJlc3VsdCBpbnRvIHJlc3VsdHMnKTtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAkc2VsZWN0ZWRSZXN1bHQgPSAoY2F0ZWdvcnlJbmRleCAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICAgID8gJHJlc3VsdHNcbiAgICAgICAgICAgICAgICAgICAgLmNoaWxkcmVuKCkuZXEoY2F0ZWdvcnlJbmRleClcbiAgICAgICAgICAgICAgICAgICAgICAuY2hpbGRyZW4oc2VsZWN0b3IucmVzdWx0KS5lcShyZXN1bHRJbmRleClcbiAgICAgICAgICAgICAgICA6ICRyZXN1bHRzXG4gICAgICAgICAgICAgICAgICAgIC5jaGlsZHJlbihzZWxlY3Rvci5yZXN1bHQpLmVxKHJlc3VsdEluZGV4KVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0luamVjdGluZyByZXN1bHRzIG1ldGFkYXRhJywgJHNlbGVjdGVkUmVzdWx0KTtcbiAgICAgICAgICAgICRzZWxlY3RlZFJlc3VsdFxuICAgICAgICAgICAgICAuZGF0YShtZXRhZGF0YS5yZXN1bHQsIHJlc3VsdClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGlkOiBmdW5jdGlvbihyZXN1bHRzKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0luamVjdGluZyB1bmlxdWUgaWRzIGludG8gcmVzdWx0cycpO1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIC8vIHNpbmNlIHJlc3VsdHMgbWF5IGJlIG9iamVjdCwgd2UgbXVzdCB1c2UgY291bnRlcnNcbiAgICAgICAgICAgICAgY2F0ZWdvcnlJbmRleCA9IDAsXG4gICAgICAgICAgICAgIHJlc3VsdEluZGV4ICAgPSAwXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy50eXBlID09PSAnY2F0ZWdvcnknKSB7XG4gICAgICAgICAgICAgIC8vIGl0ZXJhdGUgdGhyb3VnaCBlYWNoIGNhdGVnb3J5IHJlc3VsdFxuICAgICAgICAgICAgICAkLmVhY2gocmVzdWx0cywgZnVuY3Rpb24oaW5kZXgsIGNhdGVnb3J5KSB7XG4gICAgICAgICAgICAgICAgcmVzdWx0SW5kZXggPSAwO1xuICAgICAgICAgICAgICAgICQuZWFjaChjYXRlZ29yeS5yZXN1bHRzLCBmdW5jdGlvbihpbmRleCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBjYXRlZ29yeS5yZXN1bHRzW2luZGV4XVxuICAgICAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICAgICAgaWYocmVzdWx0LmlkID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmlkID0gbW9kdWxlLmNyZWF0ZS5pZChyZXN1bHRJbmRleCwgY2F0ZWdvcnlJbmRleCk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuaW5qZWN0LnJlc3VsdChyZXN1bHQsIHJlc3VsdEluZGV4LCBjYXRlZ29yeUluZGV4KTtcbiAgICAgICAgICAgICAgICAgIHJlc3VsdEluZGV4Kys7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgY2F0ZWdvcnlJbmRleCsrO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAvLyB0b3AgbGV2ZWxcbiAgICAgICAgICAgICAgJC5lYWNoKHJlc3VsdHMsIGZ1bmN0aW9uKGluZGV4LCB2YWx1ZSkge1xuICAgICAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gcmVzdWx0c1tpbmRleF1cbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgICAgaWYocmVzdWx0LmlkID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgIHJlc3VsdC5pZCA9IG1vZHVsZS5jcmVhdGUuaWQocmVzdWx0SW5kZXgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBtb2R1bGUuaW5qZWN0LnJlc3VsdChyZXN1bHQsIHJlc3VsdEluZGV4KTtcbiAgICAgICAgICAgICAgICByZXN1bHRJbmRleCsrO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiByZXN1bHRzO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzYXZlOiB7XG4gICAgICAgICAgcmVzdWx0czogZnVuY3Rpb24ocmVzdWx0cykge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1NhdmluZyBjdXJyZW50IHNlYXJjaCByZXN1bHRzIHRvIG1ldGFkYXRhJywgcmVzdWx0cyk7XG4gICAgICAgICAgICAkbW9kdWxlLmRhdGEobWV0YWRhdGEucmVzdWx0cywgcmVzdWx0cyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHdyaXRlOiB7XG4gICAgICAgICAgY2FjaGU6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgY2FjaGUgPSAoJG1vZHVsZS5kYXRhKG1ldGFkYXRhLmNhY2hlKSAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICAgID8gJG1vZHVsZS5kYXRhKG1ldGFkYXRhLmNhY2hlKVxuICAgICAgICAgICAgICAgIDoge31cbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmNhY2hlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdXcml0aW5nIGdlbmVyYXRlZCBodG1sIHRvIGNhY2hlJywgbmFtZSwgdmFsdWUpO1xuICAgICAgICAgICAgICBjYWNoZVtuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgICAgLmRhdGEobWV0YWRhdGEuY2FjaGUsIGNhY2hlKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGFkZFJlc3VsdHM6IGZ1bmN0aW9uKGh0bWwpIHtcbiAgICAgICAgICBpZiggJC5pc0Z1bmN0aW9uKHNldHRpbmdzLm9uUmVzdWx0c0FkZCkgKSB7XG4gICAgICAgICAgICBpZiggc2V0dGluZ3Mub25SZXN1bHRzQWRkLmNhbGwoJHJlc3VsdHMsIGh0bWwpID09PSBmYWxzZSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdvblJlc3VsdHNBZGQgY2FsbGJhY2sgY2FuY2VsbGVkIGRlZmF1bHQgYWN0aW9uJyk7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoaHRtbCkge1xuICAgICAgICAgICAgJHJlc3VsdHNcbiAgICAgICAgICAgICAgLmh0bWwoaHRtbClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIG1vZHVsZS5yZWZyZXNoUmVzdWx0cygpO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3Muc2VsZWN0Rmlyc3RSZXN1bHQpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnNlbGVjdC5maXJzdFJlc3VsdCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLnNob3dSZXN1bHRzKCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgbW9kdWxlLmhpZGVSZXN1bHRzKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNob3dSZXN1bHRzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighbW9kdWxlLmlzLnZpc2libGUoKSkge1xuICAgICAgICAgICAgaWYoIG1vZHVsZS5jYW4udHJhbnNpdGlvbigpICkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Nob3dpbmcgcmVzdWx0cyB3aXRoIGNzcyBhbmltYXRpb25zJyk7XG4gICAgICAgICAgICAgICRyZXN1bHRzXG4gICAgICAgICAgICAgICAgLnRyYW5zaXRpb24oe1xuICAgICAgICAgICAgICAgICAgYW5pbWF0aW9uICA6IHNldHRpbmdzLnRyYW5zaXRpb24gKyAnIGluJyxcbiAgICAgICAgICAgICAgICAgIGRlYnVnICAgICAgOiBzZXR0aW5ncy5kZWJ1ZyxcbiAgICAgICAgICAgICAgICAgIHZlcmJvc2UgICAgOiBzZXR0aW5ncy52ZXJib3NlLFxuICAgICAgICAgICAgICAgICAgZHVyYXRpb24gICA6IHNldHRpbmdzLmR1cmF0aW9uLFxuICAgICAgICAgICAgICAgICAgcXVldWUgICAgICA6IHRydWVcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTaG93aW5nIHJlc3VsdHMgd2l0aCBqYXZhc2NyaXB0Jyk7XG4gICAgICAgICAgICAgICRyZXN1bHRzXG4gICAgICAgICAgICAgICAgLnN0b3AoKVxuICAgICAgICAgICAgICAgIC5mYWRlSW4oc2V0dGluZ3MuZHVyYXRpb24sIHNldHRpbmdzLmVhc2luZylcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc2V0dGluZ3Mub25SZXN1bHRzT3Blbi5jYWxsKCRyZXN1bHRzKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGhpZGVSZXN1bHRzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZiggbW9kdWxlLmlzLnZpc2libGUoKSApIHtcbiAgICAgICAgICAgIGlmKCBtb2R1bGUuY2FuLnRyYW5zaXRpb24oKSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdIaWRpbmcgcmVzdWx0cyB3aXRoIGNzcyBhbmltYXRpb25zJyk7XG4gICAgICAgICAgICAgICRyZXN1bHRzXG4gICAgICAgICAgICAgICAgLnRyYW5zaXRpb24oe1xuICAgICAgICAgICAgICAgICAgYW5pbWF0aW9uICA6IHNldHRpbmdzLnRyYW5zaXRpb24gKyAnIG91dCcsXG4gICAgICAgICAgICAgICAgICBkZWJ1ZyAgICAgIDogc2V0dGluZ3MuZGVidWcsXG4gICAgICAgICAgICAgICAgICB2ZXJib3NlICAgIDogc2V0dGluZ3MudmVyYm9zZSxcbiAgICAgICAgICAgICAgICAgIGR1cmF0aW9uICAgOiBzZXR0aW5ncy5kdXJhdGlvbixcbiAgICAgICAgICAgICAgICAgIHF1ZXVlICAgICAgOiB0cnVlXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSGlkaW5nIHJlc3VsdHMgd2l0aCBqYXZhc2NyaXB0Jyk7XG4gICAgICAgICAgICAgICRyZXN1bHRzXG4gICAgICAgICAgICAgICAgLnN0b3AoKVxuICAgICAgICAgICAgICAgIC5mYWRlT3V0KHNldHRpbmdzLmR1cmF0aW9uLCBzZXR0aW5ncy5lYXNpbmcpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHNldHRpbmdzLm9uUmVzdWx0c0Nsb3NlLmNhbGwoJHJlc3VsdHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBnZW5lcmF0ZVJlc3VsdHM6IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdHZW5lcmF0aW5nIGh0bWwgZnJvbSByZXNwb25zZScsIHJlc3BvbnNlKTtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIHRlbXBsYXRlICAgICAgID0gc2V0dGluZ3MudGVtcGxhdGVzW3NldHRpbmdzLnR5cGVdLFxuICAgICAgICAgICAgaXNQcm9wZXJPYmplY3QgPSAoJC5pc1BsYWluT2JqZWN0KHJlc3BvbnNlW2ZpZWxkcy5yZXN1bHRzXSkgJiYgISQuaXNFbXB0eU9iamVjdChyZXNwb25zZVtmaWVsZHMucmVzdWx0c10pKSxcbiAgICAgICAgICAgIGlzUHJvcGVyQXJyYXkgID0gKCQuaXNBcnJheShyZXNwb25zZVtmaWVsZHMucmVzdWx0c10pICYmIHJlc3BvbnNlW2ZpZWxkcy5yZXN1bHRzXS5sZW5ndGggPiAwKSxcbiAgICAgICAgICAgIGh0bWwgICAgICAgICAgID0gJydcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYoaXNQcm9wZXJPYmplY3QgfHwgaXNQcm9wZXJBcnJheSApIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLm1heFJlc3VsdHMgPiAwKSB7XG4gICAgICAgICAgICAgIGlmKGlzUHJvcGVyT2JqZWN0KSB7XG4gICAgICAgICAgICAgICAgaWYoc2V0dGluZ3MudHlwZSA9PSAnc3RhbmRhcmQnKSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3IubWF4UmVzdWx0cyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHJlc3BvbnNlW2ZpZWxkcy5yZXN1bHRzXSA9IHJlc3BvbnNlW2ZpZWxkcy5yZXN1bHRzXS5zbGljZSgwLCBzZXR0aW5ncy5tYXhSZXN1bHRzKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoJC5pc0Z1bmN0aW9uKHRlbXBsYXRlKSkge1xuICAgICAgICAgICAgICBodG1sID0gdGVtcGxhdGUocmVzcG9uc2UsIGZpZWxkcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm5vVGVtcGxhdGUsIGZhbHNlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihzZXR0aW5ncy5zaG93Tm9SZXN1bHRzKSB7XG4gICAgICAgICAgICBodG1sID0gbW9kdWxlLmRpc3BsYXlNZXNzYWdlKGVycm9yLm5vUmVzdWx0cywgJ2VtcHR5Jyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHNldHRpbmdzLm9uUmVzdWx0cy5jYWxsKGVsZW1lbnQsIHJlc3BvbnNlKTtcbiAgICAgICAgICByZXR1cm4gaHRtbDtcbiAgICAgICAgfSxcblxuICAgICAgICBkaXNwbGF5TWVzc2FnZTogZnVuY3Rpb24odGV4dCwgdHlwZSkge1xuICAgICAgICAgIHR5cGUgPSB0eXBlIHx8ICdzdGFuZGFyZCc7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdEaXNwbGF5aW5nIG1lc3NhZ2UnLCB0ZXh0LCB0eXBlKTtcbiAgICAgICAgICBtb2R1bGUuYWRkUmVzdWx0cyggc2V0dGluZ3MudGVtcGxhdGVzLm1lc3NhZ2UodGV4dCwgdHlwZSkgKTtcbiAgICAgICAgICByZXR1cm4gc2V0dGluZ3MudGVtcGxhdGVzLm1lc3NhZ2UodGV4dCwgdHlwZSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0dGluZzogZnVuY3Rpb24obmFtZSwgdmFsdWUpIHtcbiAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgc2V0dGluZ3MsIG5hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHNldHRpbmdzW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHNldHRpbmdzW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgaW50ZXJuYWw6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChuYW1lKSApIHtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIG1vZHVsZSwgbmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgbW9kdWxlW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZVtuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGRlYnVnOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50ICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UubG9nKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmluZm8sIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHZlcmJvc2U6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQgJiYgc2V0dGluZ3MudmVyYm9zZSAmJiBzZXR0aW5ncy5kZWJ1Zykge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmluZm8sIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZS5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQpIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvciA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5lcnJvciwgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHBlcmZvcm1hbmNlOiB7XG4gICAgICAgICAgbG9nOiBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgY3VycmVudFRpbWUsXG4gICAgICAgICAgICAgIGV4ZWN1dGlvblRpbWUsXG4gICAgICAgICAgICAgIHByZXZpb3VzVGltZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgY3VycmVudFRpbWUgICA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgICAgICAgICAgICBwcmV2aW91c1RpbWUgID0gdGltZSB8fCBjdXJyZW50VGltZTtcbiAgICAgICAgICAgICAgZXhlY3V0aW9uVGltZSA9IGN1cnJlbnRUaW1lIC0gcHJldmlvdXNUaW1lO1xuICAgICAgICAgICAgICB0aW1lICAgICAgICAgID0gY3VycmVudFRpbWU7XG4gICAgICAgICAgICAgIHBlcmZvcm1hbmNlLnB1c2goe1xuICAgICAgICAgICAgICAgICdOYW1lJyAgICAgICAgICAgOiBtZXNzYWdlWzBdLFxuICAgICAgICAgICAgICAgICdBcmd1bWVudHMnICAgICAgOiBbXS5zbGljZS5jYWxsKG1lc3NhZ2UsIDEpIHx8ICcnLFxuICAgICAgICAgICAgICAgICdFbGVtZW50JyAgICAgICAgOiBlbGVtZW50LFxuICAgICAgICAgICAgICAgICdFeGVjdXRpb24gVGltZScgOiBleGVjdXRpb25UaW1lXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lcik7XG4gICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UudGltZXIgPSBzZXRUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS5kaXNwbGF5LCA1MDApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZGlzcGxheTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgdGl0bGUgPSBzZXR0aW5ncy5uYW1lICsgJzonLFxuICAgICAgICAgICAgICB0b3RhbFRpbWUgPSAwXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICB0aW1lID0gZmFsc2U7XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgdG90YWxUaW1lICs9IGRhdGFbJ0V4ZWN1dGlvbiBUaW1lJ107XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHRpdGxlICs9ICcgJyArIHRvdGFsVGltZSArICdtcyc7XG4gICAgICAgICAgICBpZihtb2R1bGVTZWxlY3Rvcikge1xuICAgICAgICAgICAgICB0aXRsZSArPSAnIFxcJycgKyBtb2R1bGVTZWxlY3RvciArICdcXCcnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoJGFsbE1vZHVsZXMubGVuZ3RoID4gMSkge1xuICAgICAgICAgICAgICB0aXRsZSArPSAnICcgKyAnKCcgKyAkYWxsTW9kdWxlcy5sZW5ndGggKyAnKSc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiggKGNvbnNvbGUuZ3JvdXAgIT09IHVuZGVmaW5lZCB8fCBjb25zb2xlLnRhYmxlICE9PSB1bmRlZmluZWQpICYmIHBlcmZvcm1hbmNlLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgY29uc29sZS5ncm91cENvbGxhcHNlZCh0aXRsZSk7XG4gICAgICAgICAgICAgIGlmKGNvbnNvbGUudGFibGUpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLnRhYmxlKHBlcmZvcm1hbmNlKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAkLmVhY2gocGVyZm9ybWFuY2UsIGZ1bmN0aW9uKGluZGV4LCBkYXRhKSB7XG4gICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhWydOYW1lJ10gKyAnOiAnICsgZGF0YVsnRXhlY3V0aW9uIFRpbWUnXSsnbXMnKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwRW5kKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBwZXJmb3JtYW5jZSA9IFtdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgaW52b2tlOiBmdW5jdGlvbihxdWVyeSwgcGFzc2VkQXJndW1lbnRzLCBjb250ZXh0KSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICBvYmplY3QgPSBpbnN0YW5jZSxcbiAgICAgICAgICAgIG1heERlcHRoLFxuICAgICAgICAgICAgZm91bmQsXG4gICAgICAgICAgICByZXNwb25zZVxuICAgICAgICAgIDtcbiAgICAgICAgICBwYXNzZWRBcmd1bWVudHMgPSBwYXNzZWRBcmd1bWVudHMgfHwgcXVlcnlBcmd1bWVudHM7XG4gICAgICAgICAgY29udGV4dCAgICAgICAgID0gZWxlbWVudCAgICAgICAgIHx8IGNvbnRleHQ7XG4gICAgICAgICAgaWYodHlwZW9mIHF1ZXJ5ID09ICdzdHJpbmcnICYmIG9iamVjdCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBxdWVyeSAgICA9IHF1ZXJ5LnNwbGl0KC9bXFwuIF0vKTtcbiAgICAgICAgICAgIG1heERlcHRoID0gcXVlcnkubGVuZ3RoIC0gMTtcbiAgICAgICAgICAgICQuZWFjaChxdWVyeSwgZnVuY3Rpb24oZGVwdGgsIHZhbHVlKSB7XG4gICAgICAgICAgICAgIHZhciBjYW1lbENhc2VWYWx1ZSA9IChkZXB0aCAhPSBtYXhEZXB0aClcbiAgICAgICAgICAgICAgICA/IHZhbHVlICsgcXVlcnlbZGVwdGggKyAxXS5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIHF1ZXJ5W2RlcHRoICsgMV0uc2xpY2UoMSlcbiAgICAgICAgICAgICAgICA6IHF1ZXJ5XG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdCggb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXSApICYmIChkZXB0aCAhPSBtYXhEZXB0aCkgKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBvYmplY3RbY2FtZWxDYXNlVmFsdWVdICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCAkLmlzUGxhaW5PYmplY3QoIG9iamVjdFt2YWx1ZV0gKSAmJiAoZGVwdGggIT0gbWF4RGVwdGgpICkge1xuICAgICAgICAgICAgICAgIG9iamVjdCA9IG9iamVjdFt2YWx1ZV07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggb2JqZWN0W3ZhbHVlXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgICAgIGZvdW5kID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoICQuaXNGdW5jdGlvbiggZm91bmQgKSApIHtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gZm91bmQuYXBwbHkoY29udGV4dCwgcGFzc2VkQXJndW1lbnRzKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihmb3VuZCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXNwb25zZSA9IGZvdW5kO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZigkLmlzQXJyYXkocmV0dXJuZWRWYWx1ZSkpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUucHVzaChyZXNwb25zZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYocmV0dXJuZWRWYWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlID0gW3JldHVybmVkVmFsdWUsIHJlc3BvbnNlXTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihyZXNwb25zZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlID0gcmVzcG9uc2U7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBmb3VuZDtcbiAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIGlmKG1ldGhvZEludm9rZWQpIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICAgIH1cbiAgICAgICAgbW9kdWxlLmludm9rZShxdWVyeSk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIGluc3RhbmNlLmludm9rZSgnZGVzdHJveScpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICB9XG5cbiAgICB9KVxuICA7XG5cbiAgcmV0dXJuIChyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpXG4gICAgPyByZXR1cm5lZFZhbHVlXG4gICAgOiB0aGlzXG4gIDtcbn07XG5cbiQuZm4uc2VhcmNoLnNldHRpbmdzID0ge1xuXG4gIG5hbWUgICAgICAgICAgICAgIDogJ1NlYXJjaCcsXG4gIG5hbWVzcGFjZSAgICAgICAgIDogJ3NlYXJjaCcsXG5cbiAgc2lsZW50ICAgICAgICAgICAgOiBmYWxzZSxcbiAgZGVidWcgICAgICAgICAgICAgOiBmYWxzZSxcbiAgdmVyYm9zZSAgICAgICAgICAgOiBmYWxzZSxcbiAgcGVyZm9ybWFuY2UgICAgICAgOiB0cnVlLFxuXG4gIC8vIHRlbXBsYXRlIHRvIHVzZSAoc3BlY2lmaWVkIGluIHNldHRpbmdzLnRlbXBsYXRlcylcbiAgdHlwZSAgICAgICAgICAgICAgOiAnc3RhbmRhcmQnLFxuXG4gIC8vIG1pbmltdW0gY2hhcmFjdGVycyByZXF1aXJlZCB0byBzZWFyY2hcbiAgbWluQ2hhcmFjdGVycyAgICAgOiAxLFxuXG4gIC8vIHdoZXRoZXIgdG8gc2VsZWN0IGZpcnN0IHJlc3VsdCBhZnRlciBzZWFyY2hpbmcgYXV0b21hdGljYWxseVxuICBzZWxlY3RGaXJzdFJlc3VsdCA6IGZhbHNlLFxuXG4gIC8vIEFQSSBjb25maWdcbiAgYXBpU2V0dGluZ3MgICAgICAgOiBmYWxzZSxcblxuICAvLyBvYmplY3QgdG8gc2VhcmNoXG4gIHNvdXJjZSAgICAgICAgICAgIDogZmFsc2UsXG5cbiAgLy8gZmllbGRzIHRvIHNlYXJjaFxuICBzZWFyY2hGaWVsZHMgICA6IFtcbiAgICAndGl0bGUnLFxuICAgICdkZXNjcmlwdGlvbidcbiAgXSxcblxuICAvLyBmaWVsZCB0byBkaXNwbGF5IGluIHN0YW5kYXJkIHJlc3VsdHMgdGVtcGxhdGVcbiAgZGlzcGxheUZpZWxkICAgOiAnJyxcblxuICAvLyB3aGV0aGVyIHRvIGluY2x1ZGUgZnV6enkgcmVzdWx0cyBpbiBsb2NhbCBzZWFyY2hcbiAgc2VhcmNoRnVsbFRleHQgOiB0cnVlLFxuXG4gIC8vIHdoZXRoZXIgdG8gYWRkIGV2ZW50cyB0byBwcm9tcHQgYXV0b21hdGljYWxseVxuICBhdXRvbWF0aWMgICAgICA6IHRydWUsXG5cbiAgLy8gZGVsYXkgYmVmb3JlIGhpZGluZyBtZW51IGFmdGVyIGJsdXJcbiAgaGlkZURlbGF5ICAgICAgOiAwLFxuXG4gIC8vIGRlbGF5IGJlZm9yZSBzZWFyY2hpbmdcbiAgc2VhcmNoRGVsYXkgICAgOiAyMDAsXG5cbiAgLy8gbWF4aW11bSByZXN1bHRzIHJldHVybmVkIGZyb20gbG9jYWxcbiAgbWF4UmVzdWx0cyAgICAgOiA3LFxuXG4gIC8vIHdoZXRoZXIgdG8gc3RvcmUgbG9va3VwcyBpbiBsb2NhbCBjYWNoZVxuICBjYWNoZSAgICAgICAgICA6IHRydWUsXG5cbiAgLy8gd2hldGhlciBubyByZXN1bHRzIGVycm9ycyBzaG91bGQgYmUgc2hvd25cbiAgc2hvd05vUmVzdWx0cyAgOiB0cnVlLFxuXG4gIC8vIHRyYW5zaXRpb24gc2V0dGluZ3NcbiAgdHJhbnNpdGlvbiAgICAgOiAnc2NhbGUnLFxuICBkdXJhdGlvbiAgICAgICA6IDIwMCxcbiAgZWFzaW5nICAgICAgICAgOiAnZWFzZU91dEV4cG8nLFxuXG4gIC8vIGNhbGxiYWNrc1xuICBvblNlbGVjdCAgICAgICA6IGZhbHNlLFxuICBvblJlc3VsdHNBZGQgICA6IGZhbHNlLFxuXG4gIG9uU2VhcmNoUXVlcnkgIDogZnVuY3Rpb24ocXVlcnkpe30sXG4gIG9uUmVzdWx0cyAgICAgIDogZnVuY3Rpb24ocmVzcG9uc2Upe30sXG5cbiAgb25SZXN1bHRzT3BlbiAgOiBmdW5jdGlvbigpe30sXG4gIG9uUmVzdWx0c0Nsb3NlIDogZnVuY3Rpb24oKXt9LFxuXG4gIGNsYXNzTmFtZToge1xuICAgIGFuaW1hdGluZyA6ICdhbmltYXRpbmcnLFxuICAgIGFjdGl2ZSAgICA6ICdhY3RpdmUnLFxuICAgIGVtcHR5ICAgICA6ICdlbXB0eScsXG4gICAgZm9jdXMgICAgIDogJ2ZvY3VzJyxcbiAgICBoaWRkZW4gICAgOiAnaGlkZGVuJyxcbiAgICBsb2FkaW5nICAgOiAnbG9hZGluZycsXG4gICAgcmVzdWx0cyAgIDogJ3Jlc3VsdHMnLFxuICAgIHByZXNzZWQgICA6ICdkb3duJ1xuICB9LFxuXG4gIGVycm9yIDoge1xuICAgIHNvdXJjZSAgICAgIDogJ0Nhbm5vdCBzZWFyY2guIE5vIHNvdXJjZSB1c2VkLCBhbmQgU2VtYW50aWMgQVBJIG1vZHVsZSB3YXMgbm90IGluY2x1ZGVkJyxcbiAgICBub1Jlc3VsdHMgICA6ICdZb3VyIHNlYXJjaCByZXR1cm5lZCBubyByZXN1bHRzJyxcbiAgICBsb2dnaW5nICAgICA6ICdFcnJvciBpbiBkZWJ1ZyBsb2dnaW5nLCBleGl0aW5nLicsXG4gICAgbm9FbmRwb2ludCAgOiAnTm8gc2VhcmNoIGVuZHBvaW50IHdhcyBzcGVjaWZpZWQnLFxuICAgIG5vVGVtcGxhdGUgIDogJ0EgdmFsaWQgdGVtcGxhdGUgbmFtZSB3YXMgbm90IHNwZWNpZmllZC4nLFxuICAgIHNlcnZlckVycm9yIDogJ1RoZXJlIHdhcyBhbiBpc3N1ZSBxdWVyeWluZyB0aGUgc2VydmVyLicsXG4gICAgbWF4UmVzdWx0cyAgOiAnUmVzdWx0cyBtdXN0IGJlIGFuIGFycmF5IHRvIHVzZSBtYXhSZXN1bHRzIHNldHRpbmcnLFxuICAgIG1ldGhvZCAgICAgIDogJ1RoZSBtZXRob2QgeW91IGNhbGxlZCBpcyBub3QgZGVmaW5lZC4nXG4gIH0sXG5cbiAgbWV0YWRhdGE6IHtcbiAgICBjYWNoZSAgIDogJ2NhY2hlJyxcbiAgICByZXN1bHRzIDogJ3Jlc3VsdHMnLFxuICAgIHJlc3VsdCAgOiAncmVzdWx0J1xuICB9LFxuXG4gIHJlZ0V4cDoge1xuICAgIGVzY2FwZSAgICAgOiAvW1xcLVxcW1xcXVxcL1xce1xcfVxcKFxcKVxcKlxcK1xcP1xcLlxcXFxcXF5cXCRcXHxdL2csXG4gICAgYmVnaW5zV2l0aCA6ICcoPzpcXHN8XiknXG4gIH0sXG5cbiAgLy8gbWFwcyBhcGkgcmVzcG9uc2UgYXR0cmlidXRlcyB0byBpbnRlcm5hbCByZXByZXNlbnRhdGlvblxuICBmaWVsZHM6IHtcbiAgICBjYXRlZ29yaWVzICAgICAgOiAncmVzdWx0cycsICAgICAvLyBhcnJheSBvZiBjYXRlZ29yaWVzIChjYXRlZ29yeSB2aWV3KVxuICAgIGNhdGVnb3J5TmFtZSAgICA6ICduYW1lJywgICAgICAgIC8vIG5hbWUgb2YgY2F0ZWdvcnkgKGNhdGVnb3J5IHZpZXcpXG4gICAgY2F0ZWdvcnlSZXN1bHRzIDogJ3Jlc3VsdHMnLCAgICAgLy8gYXJyYXkgb2YgcmVzdWx0cyAoY2F0ZWdvcnkgdmlldylcbiAgICBkZXNjcmlwdGlvbiAgICAgOiAnZGVzY3JpcHRpb24nLCAvLyByZXN1bHQgZGVzY3JpcHRpb25cbiAgICBpbWFnZSAgICAgICAgICAgOiAnaW1hZ2UnLCAgICAgICAvLyByZXN1bHQgaW1hZ2VcbiAgICBwcmljZSAgICAgICAgICAgOiAncHJpY2UnLCAgICAgICAvLyByZXN1bHQgcHJpY2VcbiAgICByZXN1bHRzICAgICAgICAgOiAncmVzdWx0cycsICAgICAvLyBhcnJheSBvZiByZXN1bHRzIChzdGFuZGFyZClcbiAgICB0aXRsZSAgICAgICAgICAgOiAndGl0bGUnLCAgICAgICAvLyByZXN1bHQgdGl0bGVcbiAgICB1cmwgICAgICAgICAgICAgOiAndXJsJywgICAgICAgICAvLyByZXN1bHQgdXJsXG4gICAgYWN0aW9uICAgICAgICAgIDogJ2FjdGlvbicsICAgICAgLy8gXCJ2aWV3IG1vcmVcIiBvYmplY3QgbmFtZVxuICAgIGFjdGlvblRleHQgICAgICA6ICd0ZXh0JywgICAgICAgIC8vIFwidmlldyBtb3JlXCIgdGV4dFxuICAgIGFjdGlvblVSTCAgICAgICA6ICd1cmwnICAgICAgICAgIC8vIFwidmlldyBtb3JlXCIgdXJsXG4gIH0sXG5cbiAgc2VsZWN0b3IgOiB7XG4gICAgcHJvbXB0ICAgICAgIDogJy5wcm9tcHQnLFxuICAgIHNlYXJjaEJ1dHRvbiA6ICcuc2VhcmNoLmJ1dHRvbicsXG4gICAgcmVzdWx0cyAgICAgIDogJy5yZXN1bHRzJyxcbiAgICBtZXNzYWdlICAgICAgOiAnLnJlc3VsdHMgPiAubWVzc2FnZScsXG4gICAgY2F0ZWdvcnkgICAgIDogJy5jYXRlZ29yeScsXG4gICAgcmVzdWx0ICAgICAgIDogJy5yZXN1bHQnLFxuICAgIHRpdGxlICAgICAgICA6ICcudGl0bGUsIC5uYW1lJ1xuICB9LFxuXG4gIHRlbXBsYXRlczoge1xuICAgIGVzY2FwZTogZnVuY3Rpb24oc3RyaW5nKSB7XG4gICAgICB2YXJcbiAgICAgICAgYmFkQ2hhcnMgICAgID0gL1smPD5cIidgXS9nLFxuICAgICAgICBzaG91bGRFc2NhcGUgPSAvWyY8PlwiJ2BdLyxcbiAgICAgICAgZXNjYXBlICAgICAgID0ge1xuICAgICAgICAgIFwiJlwiOiBcIiZhbXA7XCIsXG4gICAgICAgICAgXCI8XCI6IFwiJmx0O1wiLFxuICAgICAgICAgIFwiPlwiOiBcIiZndDtcIixcbiAgICAgICAgICAnXCInOiBcIiZxdW90O1wiLFxuICAgICAgICAgIFwiJ1wiOiBcIiYjeDI3O1wiLFxuICAgICAgICAgIFwiYFwiOiBcIiYjeDYwO1wiXG4gICAgICAgIH0sXG4gICAgICAgIGVzY2FwZWRDaGFyICA9IGZ1bmN0aW9uKGNocikge1xuICAgICAgICAgIHJldHVybiBlc2NhcGVbY2hyXTtcbiAgICAgICAgfVxuICAgICAgO1xuICAgICAgaWYoc2hvdWxkRXNjYXBlLnRlc3Qoc3RyaW5nKSkge1xuICAgICAgICByZXR1cm4gc3RyaW5nLnJlcGxhY2UoYmFkQ2hhcnMsIGVzY2FwZWRDaGFyKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBzdHJpbmc7XG4gICAgfSxcbiAgICBtZXNzYWdlOiBmdW5jdGlvbihtZXNzYWdlLCB0eXBlKSB7XG4gICAgICB2YXJcbiAgICAgICAgaHRtbCA9ICcnXG4gICAgICA7XG4gICAgICBpZihtZXNzYWdlICE9PSB1bmRlZmluZWQgJiYgdHlwZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGh0bWwgKz0gICcnXG4gICAgICAgICAgKyAnPGRpdiBjbGFzcz1cIm1lc3NhZ2UgJyArIHR5cGUgKyAnXCI+J1xuICAgICAgICA7XG4gICAgICAgIC8vIG1lc3NhZ2UgdHlwZVxuICAgICAgICBpZih0eXBlID09ICdlbXB0eScpIHtcbiAgICAgICAgICBodG1sICs9ICcnXG4gICAgICAgICAgICArICc8ZGl2IGNsYXNzPVwiaGVhZGVyXCI+Tm8gUmVzdWx0czwvZGl2IGNsYXNzPVwiaGVhZGVyXCI+J1xuICAgICAgICAgICAgKyAnPGRpdiBjbGFzcz1cImRlc2NyaXB0aW9uXCI+JyArIG1lc3NhZ2UgKyAnPC9kaXYgY2xhc3M9XCJkZXNjcmlwdGlvblwiPidcbiAgICAgICAgICA7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgaHRtbCArPSAnIDxkaXYgY2xhc3M9XCJkZXNjcmlwdGlvblwiPicgKyBtZXNzYWdlICsgJzwvZGl2Pic7XG4gICAgICAgIH1cbiAgICAgICAgaHRtbCArPSAnPC9kaXY+JztcbiAgICAgIH1cbiAgICAgIHJldHVybiBodG1sO1xuICAgIH0sXG4gICAgY2F0ZWdvcnk6IGZ1bmN0aW9uKHJlc3BvbnNlLCBmaWVsZHMpIHtcbiAgICAgIHZhclxuICAgICAgICBodG1sID0gJycsXG4gICAgICAgIGVzY2FwZSA9ICQuZm4uc2VhcmNoLnNldHRpbmdzLnRlbXBsYXRlcy5lc2NhcGVcbiAgICAgIDtcbiAgICAgIGlmKHJlc3BvbnNlW2ZpZWxkcy5jYXRlZ29yeVJlc3VsdHNdICE9PSB1bmRlZmluZWQpIHtcblxuICAgICAgICAvLyBlYWNoIGNhdGVnb3J5XG4gICAgICAgICQuZWFjaChyZXNwb25zZVtmaWVsZHMuY2F0ZWdvcnlSZXN1bHRzXSwgZnVuY3Rpb24oaW5kZXgsIGNhdGVnb3J5KSB7XG4gICAgICAgICAgaWYoY2F0ZWdvcnlbZmllbGRzLnJlc3VsdHNdICE9PSB1bmRlZmluZWQgJiYgY2F0ZWdvcnkucmVzdWx0cy5sZW5ndGggPiAwKSB7XG5cbiAgICAgICAgICAgIGh0bWwgICs9ICc8ZGl2IGNsYXNzPVwiY2F0ZWdvcnlcIj4nO1xuXG4gICAgICAgICAgICBpZihjYXRlZ29yeVtmaWVsZHMuY2F0ZWdvcnlOYW1lXSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIGh0bWwgKz0gJzxkaXYgY2xhc3M9XCJuYW1lXCI+JyArIGNhdGVnb3J5W2ZpZWxkcy5jYXRlZ29yeU5hbWVdICsgJzwvZGl2Pic7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIGVhY2ggaXRlbSBpbnNpZGUgY2F0ZWdvcnlcbiAgICAgICAgICAgICQuZWFjaChjYXRlZ29yeS5yZXN1bHRzLCBmdW5jdGlvbihpbmRleCwgcmVzdWx0KSB7XG4gICAgICAgICAgICAgIGlmKHJlc3VsdFtmaWVsZHMudXJsXSkge1xuICAgICAgICAgICAgICAgIGh0bWwgICs9ICc8YSBjbGFzcz1cInJlc3VsdFwiIGhyZWY9XCInICsgcmVzdWx0W2ZpZWxkcy51cmxdICsgJ1wiPic7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgaHRtbCAgKz0gJzxhIGNsYXNzPVwicmVzdWx0XCI+JztcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBpZihyZXN1bHRbZmllbGRzLmltYWdlXSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgaHRtbCArPSAnJ1xuICAgICAgICAgICAgICAgICAgKyAnPGRpdiBjbGFzcz1cImltYWdlXCI+J1xuICAgICAgICAgICAgICAgICAgKyAnIDxpbWcgc3JjPVwiJyArIHJlc3VsdFtmaWVsZHMuaW1hZ2VdICsgJ1wiPidcbiAgICAgICAgICAgICAgICAgICsgJzwvZGl2PidcbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaHRtbCArPSAnPGRpdiBjbGFzcz1cImNvbnRlbnRcIj4nO1xuICAgICAgICAgICAgICBpZihyZXN1bHRbZmllbGRzLnByaWNlXSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgaHRtbCArPSAnPGRpdiBjbGFzcz1cInByaWNlXCI+JyArIHJlc3VsdFtmaWVsZHMucHJpY2VdICsgJzwvZGl2Pic7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYocmVzdWx0W2ZpZWxkcy50aXRsZV0gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIGh0bWwgKz0gJzxkaXYgY2xhc3M9XCJ0aXRsZVwiPicgKyByZXN1bHRbZmllbGRzLnRpdGxlXSArICc8L2Rpdj4nO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGlmKHJlc3VsdFtmaWVsZHMuZGVzY3JpcHRpb25dICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICBodG1sICs9ICc8ZGl2IGNsYXNzPVwiZGVzY3JpcHRpb25cIj4nICsgcmVzdWx0W2ZpZWxkcy5kZXNjcmlwdGlvbl0gKyAnPC9kaXY+JztcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBodG1sICArPSAnJ1xuICAgICAgICAgICAgICAgICsgJzwvZGl2PidcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBodG1sICs9ICc8L2E+JztcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaHRtbCAgKz0gJydcbiAgICAgICAgICAgICAgKyAnPC9kaXY+J1xuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIGlmKHJlc3BvbnNlW2ZpZWxkcy5hY3Rpb25dKSB7XG4gICAgICAgICAgaHRtbCArPSAnJ1xuICAgICAgICAgICsgJzxhIGhyZWY9XCInICsgcmVzcG9uc2VbZmllbGRzLmFjdGlvbl1bZmllbGRzLmFjdGlvblVSTF0gKyAnXCIgY2xhc3M9XCJhY3Rpb25cIj4nXG4gICAgICAgICAgKyAgIHJlc3BvbnNlW2ZpZWxkcy5hY3Rpb25dW2ZpZWxkcy5hY3Rpb25UZXh0XVxuICAgICAgICAgICsgJzwvYT4nO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBodG1sO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0sXG4gICAgc3RhbmRhcmQ6IGZ1bmN0aW9uKHJlc3BvbnNlLCBmaWVsZHMpIHtcbiAgICAgIHZhclxuICAgICAgICBodG1sID0gJydcbiAgICAgIDtcbiAgICAgIGlmKHJlc3BvbnNlW2ZpZWxkcy5yZXN1bHRzXSAhPT0gdW5kZWZpbmVkKSB7XG5cbiAgICAgICAgLy8gZWFjaCByZXN1bHRcbiAgICAgICAgJC5lYWNoKHJlc3BvbnNlW2ZpZWxkcy5yZXN1bHRzXSwgZnVuY3Rpb24oaW5kZXgsIHJlc3VsdCkge1xuICAgICAgICAgIGlmKHJlc3VsdFtmaWVsZHMudXJsXSkge1xuICAgICAgICAgICAgaHRtbCAgKz0gJzxhIGNsYXNzPVwicmVzdWx0XCIgaHJlZj1cIicgKyByZXN1bHRbZmllbGRzLnVybF0gKyAnXCI+JztcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBodG1sICArPSAnPGEgY2xhc3M9XCJyZXN1bHRcIj4nO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZihyZXN1bHRbZmllbGRzLmltYWdlXSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBodG1sICs9ICcnXG4gICAgICAgICAgICAgICsgJzxkaXYgY2xhc3M9XCJpbWFnZVwiPidcbiAgICAgICAgICAgICAgKyAnIDxpbWcgc3JjPVwiJyArIHJlc3VsdFtmaWVsZHMuaW1hZ2VdICsgJ1wiPidcbiAgICAgICAgICAgICAgKyAnPC9kaXY+J1xuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgICBodG1sICs9ICc8ZGl2IGNsYXNzPVwiY29udGVudFwiPic7XG4gICAgICAgICAgaWYocmVzdWx0W2ZpZWxkcy5wcmljZV0gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgaHRtbCArPSAnPGRpdiBjbGFzcz1cInByaWNlXCI+JyArIHJlc3VsdFtmaWVsZHMucHJpY2VdICsgJzwvZGl2Pic7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKHJlc3VsdFtmaWVsZHMudGl0bGVdICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGh0bWwgKz0gJzxkaXYgY2xhc3M9XCJ0aXRsZVwiPicgKyByZXN1bHRbZmllbGRzLnRpdGxlXSArICc8L2Rpdj4nO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZihyZXN1bHRbZmllbGRzLmRlc2NyaXB0aW9uXSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBodG1sICs9ICc8ZGl2IGNsYXNzPVwiZGVzY3JpcHRpb25cIj4nICsgcmVzdWx0W2ZpZWxkcy5kZXNjcmlwdGlvbl0gKyAnPC9kaXY+JztcbiAgICAgICAgICB9XG4gICAgICAgICAgaHRtbCAgKz0gJydcbiAgICAgICAgICAgICsgJzwvZGl2PidcbiAgICAgICAgICA7XG4gICAgICAgICAgaHRtbCArPSAnPC9hPic7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmKHJlc3BvbnNlW2ZpZWxkcy5hY3Rpb25dKSB7XG4gICAgICAgICAgaHRtbCArPSAnJ1xuICAgICAgICAgICsgJzxhIGhyZWY9XCInICsgcmVzcG9uc2VbZmllbGRzLmFjdGlvbl1bZmllbGRzLmFjdGlvblVSTF0gKyAnXCIgY2xhc3M9XCJhY3Rpb25cIj4nXG4gICAgICAgICAgKyAgIHJlc3BvbnNlW2ZpZWxkcy5hY3Rpb25dW2ZpZWxkcy5hY3Rpb25UZXh0XVxuICAgICAgICAgICsgJzwvYT4nO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBodG1sO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfVxufTtcblxufSkoIGpRdWVyeSwgd2luZG93LCBkb2N1bWVudCApO1xuIl19