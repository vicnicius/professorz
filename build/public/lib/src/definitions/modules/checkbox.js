/*!
 * # Semantic UI - Checkbox
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.checkbox = function (parameters) {
    var $allModules = $(this),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue;

    $allModules.each(function () {
      var settings = $.extend(true, {}, $.fn.checkbox.settings, parameters),
          className = settings.className,
          namespace = settings.namespace,
          selector = settings.selector,
          error = settings.error,
          eventNamespace = '.' + namespace,
          moduleNamespace = 'module-' + namespace,
          $module = $(this),
          $label = $(this).children(selector.label),
          $input = $(this).children(selector.input),
          input = $input[0],
          initialLoad = false,
          shortcutPressed = false,
          instance = $module.data(moduleNamespace),
          observer,
          element = this,
          module;

      module = {

        initialize: function () {
          module.verbose('Initializing checkbox', settings);

          module.create.label();
          module.bind.events();

          module.set.tabbable();
          module.hide.input();

          module.observeChanges();
          module.instantiate();
          module.setup();
        },

        instantiate: function () {
          module.verbose('Storing instance of module', module);
          instance = module;
          $module.data(moduleNamespace, module);
        },

        destroy: function () {
          module.verbose('Destroying module');
          module.unbind.events();
          module.show.input();
          $module.removeData(moduleNamespace);
        },

        fix: {
          reference: function () {
            if ($module.is(selector.input)) {
              module.debug('Behavior called on <input> adjusting invoked element');
              $module = $module.closest(selector.checkbox);
              module.refresh();
            }
          }
        },

        setup: function () {
          module.set.initialLoad();
          if (module.is.indeterminate()) {
            module.debug('Initial value is indeterminate');
            module.indeterminate();
          } else if (module.is.checked()) {
            module.debug('Initial value is checked');
            module.check();
          } else {
            module.debug('Initial value is unchecked');
            module.uncheck();
          }
          module.remove.initialLoad();
        },

        refresh: function () {
          $label = $module.children(selector.label);
          $input = $module.children(selector.input);
          input = $input[0];
        },

        hide: {
          input: function () {
            module.verbose('Modifying <input> z-index to be unselectable');
            $input.addClass(className.hidden);
          }
        },
        show: {
          input: function () {
            module.verbose('Modifying <input> z-index to be selectable');
            $input.removeClass(className.hidden);
          }
        },

        observeChanges: function () {
          if ('MutationObserver' in window) {
            observer = new MutationObserver(function (mutations) {
              module.debug('DOM tree modified, updating selector cache');
              module.refresh();
            });
            observer.observe(element, {
              childList: true,
              subtree: true
            });
            module.debug('Setting up mutation observer', observer);
          }
        },

        attachEvents: function (selector, event) {
          var $element = $(selector);
          event = $.isFunction(module[event]) ? module[event] : module.toggle;
          if ($element.length > 0) {
            module.debug('Attaching checkbox events to element', selector, event);
            $element.on('click' + eventNamespace, event);
          } else {
            module.error(error.notFound);
          }
        },

        event: {
          click: function (event) {
            var $target = $(event.target);
            if ($target.is(selector.input)) {
              module.verbose('Using default check action on initialized checkbox');
              return;
            }
            if ($target.is(selector.link)) {
              module.debug('Clicking link inside checkbox, skipping toggle');
              return;
            }
            module.toggle();
            $input.focus();
            event.preventDefault();
          },
          keydown: function (event) {
            var key = event.which,
                keyCode = {
              enter: 13,
              space: 32,
              escape: 27
            };
            if (key == keyCode.escape) {
              module.verbose('Escape key pressed blurring field');
              $input.blur();
              shortcutPressed = true;
            } else if (!event.ctrlKey && (key == keyCode.space || key == keyCode.enter)) {
              module.verbose('Enter/space key pressed, toggling checkbox');
              module.toggle();
              shortcutPressed = true;
            } else {
              shortcutPressed = false;
            }
          },
          keyup: function (event) {
            if (shortcutPressed) {
              event.preventDefault();
            }
          }
        },

        check: function () {
          if (!module.should.allowCheck()) {
            return;
          }
          module.debug('Checking checkbox', $input);
          module.set.checked();
          if (!module.should.ignoreCallbacks()) {
            settings.onChecked.call(input);
            settings.onChange.call(input);
          }
        },

        uncheck: function () {
          if (!module.should.allowUncheck()) {
            return;
          }
          module.debug('Unchecking checkbox');
          module.set.unchecked();
          if (!module.should.ignoreCallbacks()) {
            settings.onUnchecked.call(input);
            settings.onChange.call(input);
          }
        },

        indeterminate: function () {
          if (module.should.allowIndeterminate()) {
            module.debug('Checkbox is already indeterminate');
            return;
          }
          module.debug('Making checkbox indeterminate');
          module.set.indeterminate();
          if (!module.should.ignoreCallbacks()) {
            settings.onIndeterminate.call(input);
            settings.onChange.call(input);
          }
        },

        determinate: function () {
          if (module.should.allowDeterminate()) {
            module.debug('Checkbox is already determinate');
            return;
          }
          module.debug('Making checkbox determinate');
          module.set.determinate();
          if (!module.should.ignoreCallbacks()) {
            settings.onDeterminate.call(input);
            settings.onChange.call(input);
          }
        },

        enable: function () {
          if (module.is.enabled()) {
            module.debug('Checkbox is already enabled');
            return;
          }
          module.debug('Enabling checkbox');
          module.set.enabled();
          settings.onEnable.call(input);
          // preserve legacy callbacks
          settings.onEnabled.call(input);
        },

        disable: function () {
          if (module.is.disabled()) {
            module.debug('Checkbox is already disabled');
            return;
          }
          module.debug('Disabling checkbox');
          module.set.disabled();
          settings.onDisable.call(input);
          // preserve legacy callbacks
          settings.onDisabled.call(input);
        },

        get: {
          radios: function () {
            var name = module.get.name();
            return $('input[name="' + name + '"]').closest(selector.checkbox);
          },
          otherRadios: function () {
            return module.get.radios().not($module);
          },
          name: function () {
            return $input.attr('name');
          }
        },

        is: {
          initialLoad: function () {
            return initialLoad;
          },
          radio: function () {
            return $input.hasClass(className.radio) || $input.attr('type') == 'radio';
          },
          indeterminate: function () {
            return $input.prop('indeterminate') !== undefined && $input.prop('indeterminate');
          },
          checked: function () {
            return $input.prop('checked') !== undefined && $input.prop('checked');
          },
          disabled: function () {
            return $input.prop('disabled') !== undefined && $input.prop('disabled');
          },
          enabled: function () {
            return !module.is.disabled();
          },
          determinate: function () {
            return !module.is.indeterminate();
          },
          unchecked: function () {
            return !module.is.checked();
          }
        },

        should: {
          allowCheck: function () {
            if (module.is.determinate() && module.is.checked() && !module.should.forceCallbacks()) {
              module.debug('Should not allow check, checkbox is already checked');
              return false;
            }
            if (settings.beforeChecked.apply(input) === false) {
              module.debug('Should not allow check, beforeChecked cancelled');
              return false;
            }
            return true;
          },
          allowUncheck: function () {
            if (module.is.determinate() && module.is.unchecked() && !module.should.forceCallbacks()) {
              module.debug('Should not allow uncheck, checkbox is already unchecked');
              return false;
            }
            if (settings.beforeUnchecked.apply(input) === false) {
              module.debug('Should not allow uncheck, beforeUnchecked cancelled');
              return false;
            }
            return true;
          },
          allowIndeterminate: function () {
            if (module.is.indeterminate() && !module.should.forceCallbacks()) {
              module.debug('Should not allow indeterminate, checkbox is already indeterminate');
              return false;
            }
            if (settings.beforeIndeterminate.apply(input) === false) {
              module.debug('Should not allow indeterminate, beforeIndeterminate cancelled');
              return false;
            }
            return true;
          },
          allowDeterminate: function () {
            if (module.is.determinate() && !module.should.forceCallbacks()) {
              module.debug('Should not allow determinate, checkbox is already determinate');
              return false;
            }
            if (settings.beforeDeterminate.apply(input) === false) {
              module.debug('Should not allow determinate, beforeDeterminate cancelled');
              return false;
            }
            return true;
          },
          forceCallbacks: function () {
            return module.is.initialLoad() && settings.fireOnInit;
          },
          ignoreCallbacks: function () {
            return initialLoad && !settings.fireOnInit;
          }
        },

        can: {
          change: function () {
            return !($module.hasClass(className.disabled) || $module.hasClass(className.readOnly) || $input.prop('disabled') || $input.prop('readonly'));
          },
          uncheck: function () {
            return typeof settings.uncheckable === 'boolean' ? settings.uncheckable : !module.is.radio();
          }
        },

        set: {
          initialLoad: function () {
            initialLoad = true;
          },
          checked: function () {
            module.verbose('Setting class to checked');
            $module.removeClass(className.indeterminate).addClass(className.checked);
            if (module.is.radio()) {
              module.uncheckOthers();
            }
            if (!module.is.indeterminate() && module.is.checked()) {
              module.debug('Input is already checked, skipping input property change');
              return;
            }
            module.verbose('Setting state to checked', input);
            $input.prop('indeterminate', false).prop('checked', true);
            module.trigger.change();
          },
          unchecked: function () {
            module.verbose('Removing checked class');
            $module.removeClass(className.indeterminate).removeClass(className.checked);
            if (!module.is.indeterminate() && module.is.unchecked()) {
              module.debug('Input is already unchecked');
              return;
            }
            module.debug('Setting state to unchecked');
            $input.prop('indeterminate', false).prop('checked', false);
            module.trigger.change();
          },
          indeterminate: function () {
            module.verbose('Setting class to indeterminate');
            $module.addClass(className.indeterminate);
            if (module.is.indeterminate()) {
              module.debug('Input is already indeterminate, skipping input property change');
              return;
            }
            module.debug('Setting state to indeterminate');
            $input.prop('indeterminate', true);
            module.trigger.change();
          },
          determinate: function () {
            module.verbose('Removing indeterminate class');
            $module.removeClass(className.indeterminate);
            if (module.is.determinate()) {
              module.debug('Input is already determinate, skipping input property change');
              return;
            }
            module.debug('Setting state to determinate');
            $input.prop('indeterminate', false);
          },
          disabled: function () {
            module.verbose('Setting class to disabled');
            $module.addClass(className.disabled);
            if (module.is.disabled()) {
              module.debug('Input is already disabled, skipping input property change');
              return;
            }
            module.debug('Setting state to disabled');
            $input.prop('disabled', 'disabled');
            module.trigger.change();
          },
          enabled: function () {
            module.verbose('Removing disabled class');
            $module.removeClass(className.disabled);
            if (module.is.enabled()) {
              module.debug('Input is already enabled, skipping input property change');
              return;
            }
            module.debug('Setting state to enabled');
            $input.prop('disabled', false);
            module.trigger.change();
          },
          tabbable: function () {
            module.verbose('Adding tabindex to checkbox');
            if ($input.attr('tabindex') === undefined) {
              $input.attr('tabindex', 0);
            }
          }
        },

        remove: {
          initialLoad: function () {
            initialLoad = false;
          }
        },

        trigger: {
          change: function () {
            var events = document.createEvent('HTMLEvents'),
                inputElement = $input[0];
            if (inputElement) {
              module.verbose('Triggering native change event');
              events.initEvent('change', true, false);
              inputElement.dispatchEvent(events);
            }
          }
        },

        create: {
          label: function () {
            if ($input.prevAll(selector.label).length > 0) {
              $input.prev(selector.label).detach().insertAfter($input);
              module.debug('Moving existing label', $label);
            } else if (!module.has.label()) {
              $label = $('<label>').insertAfter($input);
              module.debug('Creating label', $label);
            }
          }
        },

        has: {
          label: function () {
            return $label.length > 0;
          }
        },

        bind: {
          events: function () {
            module.verbose('Attaching checkbox events');
            $module.on('click' + eventNamespace, module.event.click).on('keydown' + eventNamespace, selector.input, module.event.keydown).on('keyup' + eventNamespace, selector.input, module.event.keyup);
          }
        },

        unbind: {
          events: function () {
            module.debug('Removing events');
            $module.off(eventNamespace);
          }
        },

        uncheckOthers: function () {
          var $radios = module.get.otherRadios();
          module.debug('Unchecking other radios', $radios);
          $radios.removeClass(className.checked);
        },

        toggle: function () {
          if (!module.can.change()) {
            if (!module.is.radio()) {
              module.debug('Checkbox is read-only or disabled, ignoring toggle');
            }
            return;
          }
          if (module.is.indeterminate() || module.is.unchecked()) {
            module.debug('Currently unchecked');
            module.check();
          } else if (module.is.checked() && module.can.uncheck()) {
            module.debug('Currently checked');
            module.uncheck();
          }
        },
        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                module.error(error.method, query);
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };

      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.checkbox.settings = {

    name: 'Checkbox',
    namespace: 'checkbox',

    silent: false,
    debug: false,
    verbose: true,
    performance: true,

    // delegated event context
    uncheckable: 'auto',
    fireOnInit: false,

    onChange: function () {},

    beforeChecked: function () {},
    beforeUnchecked: function () {},
    beforeDeterminate: function () {},
    beforeIndeterminate: function () {},

    onChecked: function () {},
    onUnchecked: function () {},

    onDeterminate: function () {},
    onIndeterminate: function () {},

    onEnable: function () {},
    onDisable: function () {},

    // preserve misspelled callbacks (will be removed in 3.0)
    onEnabled: function () {},
    onDisabled: function () {},

    className: {
      checked: 'checked',
      indeterminate: 'indeterminate',
      disabled: 'disabled',
      hidden: 'hidden',
      radio: 'radio',
      readOnly: 'read-only'
    },

    error: {
      method: 'The method you called is not defined'
    },

    selector: {
      checkbox: '.ui.checkbox',
      label: 'label, .box',
      input: 'input[type="checkbox"], input[type="radio"]',
      link: 'a[href]'
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9tb2R1bGVzL2NoZWNrYm94LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFVQSxDQUFDLENBQUMsVUFBVSxDQUFWLEVBQWEsTUFBYixFQUFxQixRQUFyQixFQUErQixTQUEvQixFQUEwQzs7QUFFNUM7O0FBRUEsV0FBVSxPQUFPLE1BQVAsSUFBaUIsV0FBakIsSUFBZ0MsT0FBTyxJQUFQLElBQWUsSUFBaEQsR0FDTCxNQURLLEdBRUosT0FBTyxJQUFQLElBQWUsV0FBZixJQUE4QixLQUFLLElBQUwsSUFBYSxJQUE1QyxHQUNFLElBREYsR0FFRSxTQUFTLGFBQVQsR0FKTjs7QUFPQSxJQUFFLEVBQUYsQ0FBSyxRQUFMLEdBQWdCLFVBQVMsVUFBVCxFQUFxQjtBQUNuQyxRQUNFLGNBQWlCLEVBQUUsSUFBRixDQURuQjtBQUFBLFFBRUUsaUJBQWlCLFlBQVksUUFBWixJQUF3QixFQUYzQztBQUFBLFFBSUUsT0FBaUIsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUpuQjtBQUFBLFFBS0UsY0FBaUIsRUFMbkI7QUFBQSxRQU9FLFFBQWlCLFVBQVUsQ0FBVixDQVBuQjtBQUFBLFFBUUUsZ0JBQWtCLE9BQU8sS0FBUCxJQUFnQixRQVJwQztBQUFBLFFBU0UsaUJBQWlCLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLENBQXpCLENBVG5CO0FBQUEsUUFVRSxhQVZGOztBQWFBLGdCQUNHLElBREgsQ0FDUSxZQUFXO0FBQ2YsVUFDRSxXQUFrQixFQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQixFQUFFLEVBQUYsQ0FBSyxRQUFMLENBQWMsUUFBakMsRUFBMkMsVUFBM0MsQ0FEcEI7QUFBQSxVQUdFLFlBQWtCLFNBQVMsU0FIN0I7QUFBQSxVQUlFLFlBQWtCLFNBQVMsU0FKN0I7QUFBQSxVQUtFLFdBQWtCLFNBQVMsUUFMN0I7QUFBQSxVQU1FLFFBQWtCLFNBQVMsS0FON0I7QUFBQSxVQVFFLGlCQUFrQixNQUFNLFNBUjFCO0FBQUEsVUFTRSxrQkFBa0IsWUFBWSxTQVRoQztBQUFBLFVBV0UsVUFBa0IsRUFBRSxJQUFGLENBWHBCO0FBQUEsVUFZRSxTQUFrQixFQUFFLElBQUYsRUFBUSxRQUFSLENBQWlCLFNBQVMsS0FBMUIsQ0FacEI7QUFBQSxVQWFFLFNBQWtCLEVBQUUsSUFBRixFQUFRLFFBQVIsQ0FBaUIsU0FBUyxLQUExQixDQWJwQjtBQUFBLFVBY0UsUUFBa0IsT0FBTyxDQUFQLENBZHBCO0FBQUEsVUFnQkUsY0FBa0IsS0FoQnBCO0FBQUEsVUFpQkUsa0JBQWtCLEtBakJwQjtBQUFBLFVBa0JFLFdBQWtCLFFBQVEsSUFBUixDQUFhLGVBQWIsQ0FsQnBCO0FBQUEsVUFvQkUsUUFwQkY7QUFBQSxVQXFCRSxVQUFrQixJQXJCcEI7QUFBQSxVQXNCRSxNQXRCRjs7QUF5QkEsZUFBYzs7QUFFWixvQkFBWSxZQUFXO0FBQ3JCLGlCQUFPLE9BQVAsQ0FBZSx1QkFBZixFQUF3QyxRQUF4Qzs7QUFFQSxpQkFBTyxNQUFQLENBQWMsS0FBZDtBQUNBLGlCQUFPLElBQVAsQ0FBWSxNQUFaOztBQUVBLGlCQUFPLEdBQVAsQ0FBVyxRQUFYO0FBQ0EsaUJBQU8sSUFBUCxDQUFZLEtBQVo7O0FBRUEsaUJBQU8sY0FBUDtBQUNBLGlCQUFPLFdBQVA7QUFDQSxpQkFBTyxLQUFQO0FBQ0QsU0FkVzs7QUFnQloscUJBQWEsWUFBVztBQUN0QixpQkFBTyxPQUFQLENBQWUsNEJBQWYsRUFBNkMsTUFBN0M7QUFDQSxxQkFBVyxNQUFYO0FBQ0Esa0JBQ0csSUFESCxDQUNRLGVBRFIsRUFDeUIsTUFEekI7QUFHRCxTQXRCVzs7QUF3QlosaUJBQVMsWUFBVztBQUNsQixpQkFBTyxPQUFQLENBQWUsbUJBQWY7QUFDQSxpQkFBTyxNQUFQLENBQWMsTUFBZDtBQUNBLGlCQUFPLElBQVAsQ0FBWSxLQUFaO0FBQ0Esa0JBQVEsVUFBUixDQUFtQixlQUFuQjtBQUNELFNBN0JXOztBQStCWixhQUFLO0FBQ0gscUJBQVcsWUFBVztBQUNwQixnQkFBSSxRQUFRLEVBQVIsQ0FBVyxTQUFTLEtBQXBCLENBQUosRUFBaUM7QUFDL0IscUJBQU8sS0FBUCxDQUFhLHNEQUFiO0FBQ0Esd0JBQVUsUUFBUSxPQUFSLENBQWdCLFNBQVMsUUFBekIsQ0FBVjtBQUNBLHFCQUFPLE9BQVA7QUFDRDtBQUNGO0FBUEUsU0EvQk87O0FBeUNaLGVBQU8sWUFBVztBQUNoQixpQkFBTyxHQUFQLENBQVcsV0FBWDtBQUNBLGNBQUksT0FBTyxFQUFQLENBQVUsYUFBVixFQUFKLEVBQWdDO0FBQzlCLG1CQUFPLEtBQVAsQ0FBYSxnQ0FBYjtBQUNBLG1CQUFPLGFBQVA7QUFDRCxXQUhELE1BSUssSUFBSSxPQUFPLEVBQVAsQ0FBVSxPQUFWLEVBQUosRUFBMEI7QUFDN0IsbUJBQU8sS0FBUCxDQUFhLDBCQUFiO0FBQ0EsbUJBQU8sS0FBUDtBQUNELFdBSEksTUFJQTtBQUNILG1CQUFPLEtBQVAsQ0FBYSw0QkFBYjtBQUNBLG1CQUFPLE9BQVA7QUFDRDtBQUNELGlCQUFPLE1BQVAsQ0FBYyxXQUFkO0FBQ0QsU0F4RFc7O0FBMERaLGlCQUFTLFlBQVc7QUFDbEIsbUJBQVMsUUFBUSxRQUFSLENBQWlCLFNBQVMsS0FBMUIsQ0FBVDtBQUNBLG1CQUFTLFFBQVEsUUFBUixDQUFpQixTQUFTLEtBQTFCLENBQVQ7QUFDQSxrQkFBUyxPQUFPLENBQVAsQ0FBVDtBQUNELFNBOURXOztBQWdFWixjQUFNO0FBQ0osaUJBQU8sWUFBVztBQUNoQixtQkFBTyxPQUFQLENBQWUsOENBQWY7QUFDQSxtQkFBTyxRQUFQLENBQWdCLFVBQVUsTUFBMUI7QUFDRDtBQUpHLFNBaEVNO0FBc0VaLGNBQU07QUFDSixpQkFBTyxZQUFXO0FBQ2hCLG1CQUFPLE9BQVAsQ0FBZSw0Q0FBZjtBQUNBLG1CQUFPLFdBQVAsQ0FBbUIsVUFBVSxNQUE3QjtBQUNEO0FBSkcsU0F0RU07O0FBNkVaLHdCQUFnQixZQUFXO0FBQ3pCLGNBQUcsc0JBQXNCLE1BQXpCLEVBQWlDO0FBQy9CLHVCQUFXLElBQUksZ0JBQUosQ0FBcUIsVUFBUyxTQUFULEVBQW9CO0FBQ2xELHFCQUFPLEtBQVAsQ0FBYSw0Q0FBYjtBQUNBLHFCQUFPLE9BQVA7QUFDRCxhQUhVLENBQVg7QUFJQSxxQkFBUyxPQUFULENBQWlCLE9BQWpCLEVBQTBCO0FBQ3hCLHlCQUFZLElBRFk7QUFFeEIsdUJBQVk7QUFGWSxhQUExQjtBQUlBLG1CQUFPLEtBQVAsQ0FBYSw4QkFBYixFQUE2QyxRQUE3QztBQUNEO0FBQ0YsU0F6Rlc7O0FBMkZaLHNCQUFjLFVBQVMsUUFBVCxFQUFtQixLQUFuQixFQUEwQjtBQUN0QyxjQUNFLFdBQVcsRUFBRSxRQUFGLENBRGI7QUFHQSxrQkFBUSxFQUFFLFVBQUYsQ0FBYSxPQUFPLEtBQVAsQ0FBYixJQUNKLE9BQU8sS0FBUCxDQURJLEdBRUosT0FBTyxNQUZYO0FBSUEsY0FBRyxTQUFTLE1BQVQsR0FBa0IsQ0FBckIsRUFBd0I7QUFDdEIsbUJBQU8sS0FBUCxDQUFhLHNDQUFiLEVBQXFELFFBQXJELEVBQStELEtBQS9EO0FBQ0EscUJBQ0csRUFESCxDQUNNLFVBQVUsY0FEaEIsRUFDZ0MsS0FEaEM7QUFHRCxXQUxELE1BTUs7QUFDSCxtQkFBTyxLQUFQLENBQWEsTUFBTSxRQUFuQjtBQUNEO0FBQ0YsU0E1R1c7O0FBOEdaLGVBQU87QUFDTCxpQkFBTyxVQUFTLEtBQVQsRUFBZ0I7QUFDckIsZ0JBQ0UsVUFBVSxFQUFFLE1BQU0sTUFBUixDQURaO0FBR0EsZ0JBQUksUUFBUSxFQUFSLENBQVcsU0FBUyxLQUFwQixDQUFKLEVBQWlDO0FBQy9CLHFCQUFPLE9BQVAsQ0FBZSxvREFBZjtBQUNBO0FBQ0Q7QUFDRCxnQkFBSSxRQUFRLEVBQVIsQ0FBVyxTQUFTLElBQXBCLENBQUosRUFBZ0M7QUFDOUIscUJBQU8sS0FBUCxDQUFhLGdEQUFiO0FBQ0E7QUFDRDtBQUNELG1CQUFPLE1BQVA7QUFDQSxtQkFBTyxLQUFQO0FBQ0Esa0JBQU0sY0FBTjtBQUNELFdBaEJJO0FBaUJMLG1CQUFTLFVBQVMsS0FBVCxFQUFnQjtBQUN2QixnQkFDRSxNQUFVLE1BQU0sS0FEbEI7QUFBQSxnQkFFRSxVQUFVO0FBQ1IscUJBQVMsRUFERDtBQUVSLHFCQUFTLEVBRkQ7QUFHUixzQkFBUztBQUhELGFBRlo7QUFRQSxnQkFBRyxPQUFPLFFBQVEsTUFBbEIsRUFBMEI7QUFDeEIscUJBQU8sT0FBUCxDQUFlLG1DQUFmO0FBQ0EscUJBQU8sSUFBUDtBQUNBLGdDQUFrQixJQUFsQjtBQUNELGFBSkQsTUFLSyxJQUFHLENBQUMsTUFBTSxPQUFQLEtBQW9CLE9BQU8sUUFBUSxLQUFmLElBQXdCLE9BQU8sUUFBUSxLQUEzRCxDQUFILEVBQXVFO0FBQzFFLHFCQUFPLE9BQVAsQ0FBZSw0Q0FBZjtBQUNBLHFCQUFPLE1BQVA7QUFDQSxnQ0FBa0IsSUFBbEI7QUFDRCxhQUpJLE1BS0E7QUFDSCxnQ0FBa0IsS0FBbEI7QUFDRDtBQUNGLFdBdkNJO0FBd0NMLGlCQUFPLFVBQVMsS0FBVCxFQUFnQjtBQUNyQixnQkFBRyxlQUFILEVBQW9CO0FBQ2xCLG9CQUFNLGNBQU47QUFDRDtBQUNGO0FBNUNJLFNBOUdLOztBQTZKWixlQUFPLFlBQVc7QUFDaEIsY0FBSSxDQUFDLE9BQU8sTUFBUCxDQUFjLFVBQWQsRUFBTCxFQUFrQztBQUNoQztBQUNEO0FBQ0QsaUJBQU8sS0FBUCxDQUFhLG1CQUFiLEVBQWtDLE1BQWxDO0FBQ0EsaUJBQU8sR0FBUCxDQUFXLE9BQVg7QUFDQSxjQUFJLENBQUMsT0FBTyxNQUFQLENBQWMsZUFBZCxFQUFMLEVBQXVDO0FBQ3JDLHFCQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsS0FBeEI7QUFDQSxxQkFBUyxRQUFULENBQWtCLElBQWxCLENBQXVCLEtBQXZCO0FBQ0Q7QUFDRixTQXZLVzs7QUF5S1osaUJBQVMsWUFBVztBQUNsQixjQUFJLENBQUMsT0FBTyxNQUFQLENBQWMsWUFBZCxFQUFMLEVBQW9DO0FBQ2xDO0FBQ0Q7QUFDRCxpQkFBTyxLQUFQLENBQWEscUJBQWI7QUFDQSxpQkFBTyxHQUFQLENBQVcsU0FBWDtBQUNBLGNBQUksQ0FBQyxPQUFPLE1BQVAsQ0FBYyxlQUFkLEVBQUwsRUFBdUM7QUFDckMscUJBQVMsV0FBVCxDQUFxQixJQUFyQixDQUEwQixLQUExQjtBQUNBLHFCQUFTLFFBQVQsQ0FBa0IsSUFBbEIsQ0FBdUIsS0FBdkI7QUFDRDtBQUNGLFNBbkxXOztBQXFMWix1QkFBZSxZQUFXO0FBQ3hCLGNBQUksT0FBTyxNQUFQLENBQWMsa0JBQWQsRUFBSixFQUF5QztBQUN2QyxtQkFBTyxLQUFQLENBQWEsbUNBQWI7QUFDQTtBQUNEO0FBQ0QsaUJBQU8sS0FBUCxDQUFhLCtCQUFiO0FBQ0EsaUJBQU8sR0FBUCxDQUFXLGFBQVg7QUFDQSxjQUFJLENBQUMsT0FBTyxNQUFQLENBQWMsZUFBZCxFQUFMLEVBQXVDO0FBQ3JDLHFCQUFTLGVBQVQsQ0FBeUIsSUFBekIsQ0FBOEIsS0FBOUI7QUFDQSxxQkFBUyxRQUFULENBQWtCLElBQWxCLENBQXVCLEtBQXZCO0FBQ0Q7QUFDRixTQWhNVzs7QUFrTVoscUJBQWEsWUFBVztBQUN0QixjQUFJLE9BQU8sTUFBUCxDQUFjLGdCQUFkLEVBQUosRUFBdUM7QUFDckMsbUJBQU8sS0FBUCxDQUFhLGlDQUFiO0FBQ0E7QUFDRDtBQUNELGlCQUFPLEtBQVAsQ0FBYSw2QkFBYjtBQUNBLGlCQUFPLEdBQVAsQ0FBVyxXQUFYO0FBQ0EsY0FBSSxDQUFDLE9BQU8sTUFBUCxDQUFjLGVBQWQsRUFBTCxFQUF1QztBQUNyQyxxQkFBUyxhQUFULENBQXVCLElBQXZCLENBQTRCLEtBQTVCO0FBQ0EscUJBQVMsUUFBVCxDQUFrQixJQUFsQixDQUF1QixLQUF2QjtBQUNEO0FBQ0YsU0E3TVc7O0FBK01aLGdCQUFRLFlBQVc7QUFDakIsY0FBSSxPQUFPLEVBQVAsQ0FBVSxPQUFWLEVBQUosRUFBMEI7QUFDeEIsbUJBQU8sS0FBUCxDQUFhLDZCQUFiO0FBQ0E7QUFDRDtBQUNELGlCQUFPLEtBQVAsQ0FBYSxtQkFBYjtBQUNBLGlCQUFPLEdBQVAsQ0FBVyxPQUFYO0FBQ0EsbUJBQVMsUUFBVCxDQUFrQixJQUFsQixDQUF1QixLQUF2Qjs7QUFFQSxtQkFBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLEtBQXhCO0FBQ0QsU0F6Tlc7O0FBMk5aLGlCQUFTLFlBQVc7QUFDbEIsY0FBSSxPQUFPLEVBQVAsQ0FBVSxRQUFWLEVBQUosRUFBMkI7QUFDekIsbUJBQU8sS0FBUCxDQUFhLDhCQUFiO0FBQ0E7QUFDRDtBQUNELGlCQUFPLEtBQVAsQ0FBYSxvQkFBYjtBQUNBLGlCQUFPLEdBQVAsQ0FBVyxRQUFYO0FBQ0EsbUJBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixLQUF4Qjs7QUFFQSxtQkFBUyxVQUFULENBQW9CLElBQXBCLENBQXlCLEtBQXpCO0FBQ0QsU0FyT1c7O0FBdU9aLGFBQUs7QUFDSCxrQkFBUSxZQUFXO0FBQ2pCLGdCQUNFLE9BQU8sT0FBTyxHQUFQLENBQVcsSUFBWCxFQURUO0FBR0EsbUJBQU8sRUFBRSxpQkFBaUIsSUFBakIsR0FBd0IsSUFBMUIsRUFBZ0MsT0FBaEMsQ0FBd0MsU0FBUyxRQUFqRCxDQUFQO0FBQ0QsV0FORTtBQU9ILHVCQUFhLFlBQVc7QUFDdEIsbUJBQU8sT0FBTyxHQUFQLENBQVcsTUFBWCxHQUFvQixHQUFwQixDQUF3QixPQUF4QixDQUFQO0FBQ0QsV0FURTtBQVVILGdCQUFNLFlBQVc7QUFDZixtQkFBTyxPQUFPLElBQVAsQ0FBWSxNQUFaLENBQVA7QUFDRDtBQVpFLFNBdk9POztBQXNQWixZQUFJO0FBQ0YsdUJBQWEsWUFBVztBQUN0QixtQkFBTyxXQUFQO0FBQ0QsV0FIQztBQUlGLGlCQUFPLFlBQVc7QUFDaEIsbUJBQVEsT0FBTyxRQUFQLENBQWdCLFVBQVUsS0FBMUIsS0FBb0MsT0FBTyxJQUFQLENBQVksTUFBWixLQUF1QixPQUFuRTtBQUNELFdBTkM7QUFPRix5QkFBZSxZQUFXO0FBQ3hCLG1CQUFPLE9BQU8sSUFBUCxDQUFZLGVBQVosTUFBaUMsU0FBakMsSUFBOEMsT0FBTyxJQUFQLENBQVksZUFBWixDQUFyRDtBQUNELFdBVEM7QUFVRixtQkFBUyxZQUFXO0FBQ2xCLG1CQUFPLE9BQU8sSUFBUCxDQUFZLFNBQVosTUFBMkIsU0FBM0IsSUFBd0MsT0FBTyxJQUFQLENBQVksU0FBWixDQUEvQztBQUNELFdBWkM7QUFhRixvQkFBVSxZQUFXO0FBQ25CLG1CQUFPLE9BQU8sSUFBUCxDQUFZLFVBQVosTUFBNEIsU0FBNUIsSUFBeUMsT0FBTyxJQUFQLENBQVksVUFBWixDQUFoRDtBQUNELFdBZkM7QUFnQkYsbUJBQVMsWUFBVztBQUNsQixtQkFBTyxDQUFDLE9BQU8sRUFBUCxDQUFVLFFBQVYsRUFBUjtBQUNELFdBbEJDO0FBbUJGLHVCQUFhLFlBQVc7QUFDdEIsbUJBQU8sQ0FBQyxPQUFPLEVBQVAsQ0FBVSxhQUFWLEVBQVI7QUFDRCxXQXJCQztBQXNCRixxQkFBVyxZQUFXO0FBQ3BCLG1CQUFPLENBQUMsT0FBTyxFQUFQLENBQVUsT0FBVixFQUFSO0FBQ0Q7QUF4QkMsU0F0UFE7O0FBaVJaLGdCQUFRO0FBQ04sc0JBQVksWUFBVztBQUNyQixnQkFBRyxPQUFPLEVBQVAsQ0FBVSxXQUFWLE1BQTJCLE9BQU8sRUFBUCxDQUFVLE9BQVYsRUFBM0IsSUFBa0QsQ0FBQyxPQUFPLE1BQVAsQ0FBYyxjQUFkLEVBQXRELEVBQXVGO0FBQ3JGLHFCQUFPLEtBQVAsQ0FBYSxxREFBYjtBQUNBLHFCQUFPLEtBQVA7QUFDRDtBQUNELGdCQUFHLFNBQVMsYUFBVCxDQUF1QixLQUF2QixDQUE2QixLQUE3QixNQUF3QyxLQUEzQyxFQUFrRDtBQUNoRCxxQkFBTyxLQUFQLENBQWEsaURBQWI7QUFDQSxxQkFBTyxLQUFQO0FBQ0Q7QUFDRCxtQkFBTyxJQUFQO0FBQ0QsV0FYSztBQVlOLHdCQUFjLFlBQVc7QUFDdkIsZ0JBQUcsT0FBTyxFQUFQLENBQVUsV0FBVixNQUEyQixPQUFPLEVBQVAsQ0FBVSxTQUFWLEVBQTNCLElBQW9ELENBQUMsT0FBTyxNQUFQLENBQWMsY0FBZCxFQUF4RCxFQUF5RjtBQUN2RixxQkFBTyxLQUFQLENBQWEseURBQWI7QUFDQSxxQkFBTyxLQUFQO0FBQ0Q7QUFDRCxnQkFBRyxTQUFTLGVBQVQsQ0FBeUIsS0FBekIsQ0FBK0IsS0FBL0IsTUFBMEMsS0FBN0MsRUFBb0Q7QUFDbEQscUJBQU8sS0FBUCxDQUFhLHFEQUFiO0FBQ0EscUJBQU8sS0FBUDtBQUNEO0FBQ0QsbUJBQU8sSUFBUDtBQUNELFdBdEJLO0FBdUJOLDhCQUFvQixZQUFXO0FBQzdCLGdCQUFHLE9BQU8sRUFBUCxDQUFVLGFBQVYsTUFBNkIsQ0FBQyxPQUFPLE1BQVAsQ0FBYyxjQUFkLEVBQWpDLEVBQWtFO0FBQ2hFLHFCQUFPLEtBQVAsQ0FBYSxtRUFBYjtBQUNBLHFCQUFPLEtBQVA7QUFDRDtBQUNELGdCQUFHLFNBQVMsbUJBQVQsQ0FBNkIsS0FBN0IsQ0FBbUMsS0FBbkMsTUFBOEMsS0FBakQsRUFBd0Q7QUFDdEQscUJBQU8sS0FBUCxDQUFhLCtEQUFiO0FBQ0EscUJBQU8sS0FBUDtBQUNEO0FBQ0QsbUJBQU8sSUFBUDtBQUNELFdBakNLO0FBa0NOLDRCQUFrQixZQUFXO0FBQzNCLGdCQUFHLE9BQU8sRUFBUCxDQUFVLFdBQVYsTUFBMkIsQ0FBQyxPQUFPLE1BQVAsQ0FBYyxjQUFkLEVBQS9CLEVBQWdFO0FBQzlELHFCQUFPLEtBQVAsQ0FBYSwrREFBYjtBQUNBLHFCQUFPLEtBQVA7QUFDRDtBQUNELGdCQUFHLFNBQVMsaUJBQVQsQ0FBMkIsS0FBM0IsQ0FBaUMsS0FBakMsTUFBNEMsS0FBL0MsRUFBc0Q7QUFDcEQscUJBQU8sS0FBUCxDQUFhLDJEQUFiO0FBQ0EscUJBQU8sS0FBUDtBQUNEO0FBQ0QsbUJBQU8sSUFBUDtBQUNELFdBNUNLO0FBNkNOLDBCQUFnQixZQUFXO0FBQ3pCLG1CQUFRLE9BQU8sRUFBUCxDQUFVLFdBQVYsTUFBMkIsU0FBUyxVQUE1QztBQUNELFdBL0NLO0FBZ0ROLDJCQUFpQixZQUFXO0FBQzFCLG1CQUFRLGVBQWUsQ0FBQyxTQUFTLFVBQWpDO0FBQ0Q7QUFsREssU0FqUkk7O0FBc1VaLGFBQUs7QUFDSCxrQkFBUSxZQUFXO0FBQ2pCLG1CQUFPLEVBQUcsUUFBUSxRQUFSLENBQWlCLFVBQVUsUUFBM0IsS0FBd0MsUUFBUSxRQUFSLENBQWlCLFVBQVUsUUFBM0IsQ0FBeEMsSUFBZ0YsT0FBTyxJQUFQLENBQVksVUFBWixDQUFoRixJQUEyRyxPQUFPLElBQVAsQ0FBWSxVQUFaLENBQTlHLENBQVA7QUFDRCxXQUhFO0FBSUgsbUJBQVMsWUFBVztBQUNsQixtQkFBUSxPQUFPLFNBQVMsV0FBaEIsS0FBZ0MsU0FBakMsR0FDSCxTQUFTLFdBRE4sR0FFSCxDQUFDLE9BQU8sRUFBUCxDQUFVLEtBQVYsRUFGTDtBQUlEO0FBVEUsU0F0VU87O0FBa1ZaLGFBQUs7QUFDSCx1QkFBYSxZQUFXO0FBQ3RCLDBCQUFjLElBQWQ7QUFDRCxXQUhFO0FBSUgsbUJBQVMsWUFBVztBQUNsQixtQkFBTyxPQUFQLENBQWUsMEJBQWY7QUFDQSxvQkFDRyxXQURILENBQ2UsVUFBVSxhQUR6QixFQUVHLFFBRkgsQ0FFWSxVQUFVLE9BRnRCO0FBSUEsZ0JBQUksT0FBTyxFQUFQLENBQVUsS0FBVixFQUFKLEVBQXdCO0FBQ3RCLHFCQUFPLGFBQVA7QUFDRDtBQUNELGdCQUFHLENBQUMsT0FBTyxFQUFQLENBQVUsYUFBVixFQUFELElBQThCLE9BQU8sRUFBUCxDQUFVLE9BQVYsRUFBakMsRUFBc0Q7QUFDcEQscUJBQU8sS0FBUCxDQUFhLDBEQUFiO0FBQ0E7QUFDRDtBQUNELG1CQUFPLE9BQVAsQ0FBZSwwQkFBZixFQUEyQyxLQUEzQztBQUNBLG1CQUNHLElBREgsQ0FDUSxlQURSLEVBQ3lCLEtBRHpCLEVBRUcsSUFGSCxDQUVRLFNBRlIsRUFFbUIsSUFGbkI7QUFJQSxtQkFBTyxPQUFQLENBQWUsTUFBZjtBQUNELFdBdkJFO0FBd0JILHFCQUFXLFlBQVc7QUFDcEIsbUJBQU8sT0FBUCxDQUFlLHdCQUFmO0FBQ0Esb0JBQ0csV0FESCxDQUNlLFVBQVUsYUFEekIsRUFFRyxXQUZILENBRWUsVUFBVSxPQUZ6QjtBQUlBLGdCQUFHLENBQUMsT0FBTyxFQUFQLENBQVUsYUFBVixFQUFELElBQStCLE9BQU8sRUFBUCxDQUFVLFNBQVYsRUFBbEMsRUFBMEQ7QUFDeEQscUJBQU8sS0FBUCxDQUFhLDRCQUFiO0FBQ0E7QUFDRDtBQUNELG1CQUFPLEtBQVAsQ0FBYSw0QkFBYjtBQUNBLG1CQUNHLElBREgsQ0FDUSxlQURSLEVBQ3lCLEtBRHpCLEVBRUcsSUFGSCxDQUVRLFNBRlIsRUFFbUIsS0FGbkI7QUFJQSxtQkFBTyxPQUFQLENBQWUsTUFBZjtBQUNELFdBeENFO0FBeUNILHlCQUFlLFlBQVc7QUFDeEIsbUJBQU8sT0FBUCxDQUFlLGdDQUFmO0FBQ0Esb0JBQ0csUUFESCxDQUNZLFVBQVUsYUFEdEI7QUFHQSxnQkFBSSxPQUFPLEVBQVAsQ0FBVSxhQUFWLEVBQUosRUFBZ0M7QUFDOUIscUJBQU8sS0FBUCxDQUFhLGdFQUFiO0FBQ0E7QUFDRDtBQUNELG1CQUFPLEtBQVAsQ0FBYSxnQ0FBYjtBQUNBLG1CQUNHLElBREgsQ0FDUSxlQURSLEVBQ3lCLElBRHpCO0FBR0EsbUJBQU8sT0FBUCxDQUFlLE1BQWY7QUFDRCxXQXZERTtBQXdESCx1QkFBYSxZQUFXO0FBQ3RCLG1CQUFPLE9BQVAsQ0FBZSw4QkFBZjtBQUNBLG9CQUNHLFdBREgsQ0FDZSxVQUFVLGFBRHpCO0FBR0EsZ0JBQUksT0FBTyxFQUFQLENBQVUsV0FBVixFQUFKLEVBQThCO0FBQzVCLHFCQUFPLEtBQVAsQ0FBYSw4REFBYjtBQUNBO0FBQ0Q7QUFDRCxtQkFBTyxLQUFQLENBQWEsOEJBQWI7QUFDQSxtQkFDRyxJQURILENBQ1EsZUFEUixFQUN5QixLQUR6QjtBQUdELFdBckVFO0FBc0VILG9CQUFVLFlBQVc7QUFDbkIsbUJBQU8sT0FBUCxDQUFlLDJCQUFmO0FBQ0Esb0JBQ0csUUFESCxDQUNZLFVBQVUsUUFEdEI7QUFHQSxnQkFBSSxPQUFPLEVBQVAsQ0FBVSxRQUFWLEVBQUosRUFBMkI7QUFDekIscUJBQU8sS0FBUCxDQUFhLDJEQUFiO0FBQ0E7QUFDRDtBQUNELG1CQUFPLEtBQVAsQ0FBYSwyQkFBYjtBQUNBLG1CQUNHLElBREgsQ0FDUSxVQURSLEVBQ29CLFVBRHBCO0FBR0EsbUJBQU8sT0FBUCxDQUFlLE1BQWY7QUFDRCxXQXBGRTtBQXFGSCxtQkFBUyxZQUFXO0FBQ2xCLG1CQUFPLE9BQVAsQ0FBZSx5QkFBZjtBQUNBLG9CQUFRLFdBQVIsQ0FBb0IsVUFBVSxRQUE5QjtBQUNBLGdCQUFJLE9BQU8sRUFBUCxDQUFVLE9BQVYsRUFBSixFQUEwQjtBQUN4QixxQkFBTyxLQUFQLENBQWEsMERBQWI7QUFDQTtBQUNEO0FBQ0QsbUJBQU8sS0FBUCxDQUFhLDBCQUFiO0FBQ0EsbUJBQ0csSUFESCxDQUNRLFVBRFIsRUFDb0IsS0FEcEI7QUFHQSxtQkFBTyxPQUFQLENBQWUsTUFBZjtBQUNELFdBakdFO0FBa0dILG9CQUFVLFlBQVc7QUFDbkIsbUJBQU8sT0FBUCxDQUFlLDZCQUFmO0FBQ0EsZ0JBQUksT0FBTyxJQUFQLENBQVksVUFBWixNQUE0QixTQUFoQyxFQUEyQztBQUN6QyxxQkFBTyxJQUFQLENBQVksVUFBWixFQUF3QixDQUF4QjtBQUNEO0FBQ0Y7QUF2R0UsU0FsVk87O0FBNGJaLGdCQUFRO0FBQ04sdUJBQWEsWUFBVztBQUN0QiwwQkFBYyxLQUFkO0FBQ0Q7QUFISyxTQTViSTs7QUFrY1osaUJBQVM7QUFDUCxrQkFBUSxZQUFXO0FBQ2pCLGdCQUNFLFNBQWUsU0FBUyxXQUFULENBQXFCLFlBQXJCLENBRGpCO0FBQUEsZ0JBRUUsZUFBZSxPQUFPLENBQVAsQ0FGakI7QUFJQSxnQkFBRyxZQUFILEVBQWlCO0FBQ2YscUJBQU8sT0FBUCxDQUFlLGdDQUFmO0FBQ0EscUJBQU8sU0FBUCxDQUFpQixRQUFqQixFQUEyQixJQUEzQixFQUFpQyxLQUFqQztBQUNBLDJCQUFhLGFBQWIsQ0FBMkIsTUFBM0I7QUFDRDtBQUNGO0FBWE0sU0FsY0c7O0FBaWRaLGdCQUFRO0FBQ04saUJBQU8sWUFBVztBQUNoQixnQkFBRyxPQUFPLE9BQVAsQ0FBZSxTQUFTLEtBQXhCLEVBQStCLE1BQS9CLEdBQXdDLENBQTNDLEVBQThDO0FBQzVDLHFCQUFPLElBQVAsQ0FBWSxTQUFTLEtBQXJCLEVBQTRCLE1BQTVCLEdBQXFDLFdBQXJDLENBQWlELE1BQWpEO0FBQ0EscUJBQU8sS0FBUCxDQUFhLHVCQUFiLEVBQXNDLE1BQXRDO0FBQ0QsYUFIRCxNQUlLLElBQUksQ0FBQyxPQUFPLEdBQVAsQ0FBVyxLQUFYLEVBQUwsRUFBMEI7QUFDN0IsdUJBQVMsRUFBRSxTQUFGLEVBQWEsV0FBYixDQUF5QixNQUF6QixDQUFUO0FBQ0EscUJBQU8sS0FBUCxDQUFhLGdCQUFiLEVBQStCLE1BQS9CO0FBQ0Q7QUFDRjtBQVZLLFNBamRJOztBQThkWixhQUFLO0FBQ0gsaUJBQU8sWUFBVztBQUNoQixtQkFBUSxPQUFPLE1BQVAsR0FBZ0IsQ0FBeEI7QUFDRDtBQUhFLFNBOWRPOztBQW9lWixjQUFNO0FBQ0osa0JBQVEsWUFBVztBQUNqQixtQkFBTyxPQUFQLENBQWUsMkJBQWY7QUFDQSxvQkFDRyxFQURILENBQ00sVUFBWSxjQURsQixFQUNrQyxPQUFPLEtBQVAsQ0FBYSxLQUQvQyxFQUVHLEVBRkgsQ0FFTSxZQUFZLGNBRmxCLEVBRWtDLFNBQVMsS0FGM0MsRUFFa0QsT0FBTyxLQUFQLENBQWEsT0FGL0QsRUFHRyxFQUhILENBR00sVUFBWSxjQUhsQixFQUdrQyxTQUFTLEtBSDNDLEVBR2tELE9BQU8sS0FBUCxDQUFhLEtBSC9EO0FBS0Q7QUFSRyxTQXBlTTs7QUErZVosZ0JBQVE7QUFDTixrQkFBUSxZQUFXO0FBQ2pCLG1CQUFPLEtBQVAsQ0FBYSxpQkFBYjtBQUNBLG9CQUNHLEdBREgsQ0FDTyxjQURQO0FBR0Q7QUFOSyxTQS9lSTs7QUF3ZlosdUJBQWUsWUFBVztBQUN4QixjQUNFLFVBQVUsT0FBTyxHQUFQLENBQVcsV0FBWCxFQURaO0FBR0EsaUJBQU8sS0FBUCxDQUFhLHlCQUFiLEVBQXdDLE9BQXhDO0FBQ0Esa0JBQVEsV0FBUixDQUFvQixVQUFVLE9BQTlCO0FBQ0QsU0E5Zlc7O0FBZ2dCWixnQkFBUSxZQUFXO0FBQ2pCLGNBQUksQ0FBQyxPQUFPLEdBQVAsQ0FBVyxNQUFYLEVBQUwsRUFBMkI7QUFDekIsZ0JBQUcsQ0FBQyxPQUFPLEVBQVAsQ0FBVSxLQUFWLEVBQUosRUFBdUI7QUFDckIscUJBQU8sS0FBUCxDQUFhLG9EQUFiO0FBQ0Q7QUFDRDtBQUNEO0FBQ0QsY0FBSSxPQUFPLEVBQVAsQ0FBVSxhQUFWLE1BQTZCLE9BQU8sRUFBUCxDQUFVLFNBQVYsRUFBakMsRUFBeUQ7QUFDdkQsbUJBQU8sS0FBUCxDQUFhLHFCQUFiO0FBQ0EsbUJBQU8sS0FBUDtBQUNELFdBSEQsTUFJSyxJQUFJLE9BQU8sRUFBUCxDQUFVLE9BQVYsTUFBdUIsT0FBTyxHQUFQLENBQVcsT0FBWCxFQUEzQixFQUFrRDtBQUNyRCxtQkFBTyxLQUFQLENBQWEsbUJBQWI7QUFDQSxtQkFBTyxPQUFQO0FBQ0Q7QUFDRixTQS9nQlc7QUFnaEJaLGlCQUFTLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDN0IsaUJBQU8sS0FBUCxDQUFhLGtCQUFiLEVBQWlDLElBQWpDLEVBQXVDLEtBQXZDO0FBQ0EsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsUUFBZixFQUF5QixJQUF6QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixnQkFBRyxFQUFFLGFBQUYsQ0FBZ0IsU0FBUyxJQUFULENBQWhCLENBQUgsRUFBb0M7QUFDbEMsZ0JBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxTQUFTLElBQVQsQ0FBZixFQUErQixLQUEvQjtBQUNELGFBRkQsTUFHSztBQUNILHVCQUFTLElBQVQsSUFBaUIsS0FBakI7QUFDRDtBQUNGLFdBUEksTUFRQTtBQUNILG1CQUFPLFNBQVMsSUFBVCxDQUFQO0FBQ0Q7QUFDRixTQWhpQlc7QUFpaUJaLGtCQUFVLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDOUIsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsTUFBZixFQUF1QixJQUF2QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixtQkFBTyxJQUFQLElBQWUsS0FBZjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLE9BQU8sSUFBUCxDQUFQO0FBQ0Q7QUFDRixTQTNpQlc7QUE0aUJaLGVBQU8sWUFBVztBQUNoQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsS0FBaEMsRUFBdUM7QUFDckMsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxLQUFQLEdBQWUsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWY7QUFDQSxxQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNEO0FBQ0Y7QUFDRixTQXRqQlc7QUF1akJaLGlCQUFTLFlBQVc7QUFDbEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLE9BQTdCLElBQXdDLFNBQVMsS0FBcEQsRUFBMkQ7QUFDekQsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxPQUFQLEdBQWlCLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFqQjtBQUNBLHFCQUFPLE9BQVAsQ0FBZSxLQUFmLENBQXFCLE9BQXJCLEVBQThCLFNBQTlCO0FBQ0Q7QUFDRjtBQUNGLFNBamtCVztBQWtrQlosZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQWIsRUFBcUI7QUFDbkIsbUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLEtBQXJDLEVBQTRDLE9BQTVDLEVBQXFELFNBQVMsSUFBVCxHQUFnQixHQUFyRSxDQUFmO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGLFNBdmtCVztBQXdrQloscUJBQWE7QUFDWCxlQUFLLFVBQVMsT0FBVCxFQUFrQjtBQUNyQixnQkFDRSxXQURGLEVBRUUsYUFGRixFQUdFLFlBSEY7QUFLQSxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIsNEJBQWdCLElBQUksSUFBSixHQUFXLE9BQVgsRUFBaEI7QUFDQSw2QkFBZ0IsUUFBUSxXQUF4QjtBQUNBLDhCQUFnQixjQUFjLFlBQTlCO0FBQ0EscUJBQWdCLFdBQWhCO0FBQ0EsMEJBQVksSUFBWixDQUFpQjtBQUNmLHdCQUFtQixRQUFRLENBQVIsQ0FESjtBQUVmLDZCQUFtQixHQUFHLEtBQUgsQ0FBUyxJQUFULENBQWMsT0FBZCxFQUF1QixDQUF2QixLQUE2QixFQUZqQztBQUdmLDJCQUFtQixPQUhKO0FBSWYsa0NBQW1CO0FBSkosZUFBakI7QUFNRDtBQUNELHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLG1CQUFPLFdBQVAsQ0FBbUIsS0FBbkIsR0FBMkIsV0FBVyxPQUFPLFdBQVAsQ0FBbUIsT0FBOUIsRUFBdUMsR0FBdkMsQ0FBM0I7QUFDRCxXQXJCVTtBQXNCWCxtQkFBUyxZQUFXO0FBQ2xCLGdCQUNFLFFBQVEsU0FBUyxJQUFULEdBQWdCLEdBRDFCO0FBQUEsZ0JBRUUsWUFBWSxDQUZkO0FBSUEsbUJBQU8sS0FBUDtBQUNBLHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLGNBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDJCQUFhLEtBQUssZ0JBQUwsQ0FBYjtBQUNELGFBRkQ7QUFHQSxxQkFBUyxNQUFNLFNBQU4sR0FBa0IsSUFBM0I7QUFDQSxnQkFBRyxjQUFILEVBQW1CO0FBQ2pCLHVCQUFTLFFBQVEsY0FBUixHQUF5QixJQUFsQztBQUNEO0FBQ0QsZ0JBQUksQ0FBQyxRQUFRLEtBQVIsS0FBa0IsU0FBbEIsSUFBK0IsUUFBUSxLQUFSLEtBQWtCLFNBQWxELEtBQWdFLFlBQVksTUFBWixHQUFxQixDQUF6RixFQUE0RjtBQUMxRixzQkFBUSxjQUFSLENBQXVCLEtBQXZCO0FBQ0Esa0JBQUcsUUFBUSxLQUFYLEVBQWtCO0FBQ2hCLHdCQUFRLEtBQVIsQ0FBYyxXQUFkO0FBQ0QsZUFGRCxNQUdLO0FBQ0gsa0JBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDBCQUFRLEdBQVIsQ0FBWSxLQUFLLE1BQUwsSUFBZSxJQUFmLEdBQXNCLEtBQUssZ0JBQUwsQ0FBdEIsR0FBNkMsSUFBekQ7QUFDRCxpQkFGRDtBQUdEO0FBQ0Qsc0JBQVEsUUFBUjtBQUNEO0FBQ0QsMEJBQWMsRUFBZDtBQUNEO0FBakRVLFNBeGtCRDtBQTJuQlosZ0JBQVEsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDLE9BQWpDLEVBQTBDO0FBQ2hELGNBQ0UsU0FBUyxRQURYO0FBQUEsY0FFRSxRQUZGO0FBQUEsY0FHRSxLQUhGO0FBQUEsY0FJRSxRQUpGO0FBTUEsNEJBQWtCLG1CQUFtQixjQUFyQztBQUNBLG9CQUFrQixXQUFtQixPQUFyQztBQUNBLGNBQUcsT0FBTyxLQUFQLElBQWdCLFFBQWhCLElBQTRCLFdBQVcsU0FBMUMsRUFBcUQ7QUFDbkQsb0JBQVcsTUFBTSxLQUFOLENBQVksT0FBWixDQUFYO0FBQ0EsdUJBQVcsTUFBTSxNQUFOLEdBQWUsQ0FBMUI7QUFDQSxjQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ25DLGtCQUFJLGlCQUFrQixTQUFTLFFBQVYsR0FDakIsUUFBUSxNQUFNLFFBQVEsQ0FBZCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixFQUEyQixXQUEzQixFQUFSLEdBQW1ELE1BQU0sUUFBUSxDQUFkLEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBRGxDLEdBRWpCLEtBRko7QUFJQSxrQkFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxjQUFQLENBQWpCLEtBQThDLFNBQVMsUUFBM0QsRUFBdUU7QUFDckUseUJBQVMsT0FBTyxjQUFQLENBQVQ7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLGNBQVAsTUFBMkIsU0FBL0IsRUFBMkM7QUFDOUMsd0JBQVEsT0FBTyxjQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBLElBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sS0FBUCxDQUFqQixLQUFxQyxTQUFTLFFBQWxELEVBQThEO0FBQ2pFLHlCQUFTLE9BQU8sS0FBUCxDQUFUO0FBQ0QsZUFGSSxNQUdBLElBQUksT0FBTyxLQUFQLE1BQWtCLFNBQXRCLEVBQWtDO0FBQ3JDLHdCQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQTtBQUNILHVCQUFPLEtBQVAsQ0FBYSxNQUFNLE1BQW5CLEVBQTJCLEtBQTNCO0FBQ0EsdUJBQU8sS0FBUDtBQUNEO0FBQ0YsYUF2QkQ7QUF3QkQ7QUFDRCxjQUFLLEVBQUUsVUFBRixDQUFjLEtBQWQsQ0FBTCxFQUE2QjtBQUMzQix1QkFBVyxNQUFNLEtBQU4sQ0FBWSxPQUFaLEVBQXFCLGVBQXJCLENBQVg7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsdUJBQVcsS0FBWDtBQUNEO0FBQ0QsY0FBRyxFQUFFLE9BQUYsQ0FBVSxhQUFWLENBQUgsRUFBNkI7QUFDM0IsMEJBQWMsSUFBZCxDQUFtQixRQUFuQjtBQUNELFdBRkQsTUFHSyxJQUFHLGtCQUFrQixTQUFyQixFQUFnQztBQUNuQyw0QkFBZ0IsQ0FBQyxhQUFELEVBQWdCLFFBQWhCLENBQWhCO0FBQ0QsV0FGSSxNQUdBLElBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUM5Qiw0QkFBZ0IsUUFBaEI7QUFDRDtBQUNELGlCQUFPLEtBQVA7QUFDRDtBQWhyQlcsT0FBZDs7QUFtckJBLFVBQUcsYUFBSCxFQUFrQjtBQUNoQixZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsaUJBQU8sVUFBUDtBQUNEO0FBQ0QsZUFBTyxNQUFQLENBQWMsS0FBZDtBQUNELE9BTEQsTUFNSztBQUNILFlBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUN6QixtQkFBUyxNQUFULENBQWdCLFNBQWhCO0FBQ0Q7QUFDRCxlQUFPLFVBQVA7QUFDRDtBQUNGLEtBMXRCSDs7QUE2dEJBLFdBQVEsa0JBQWtCLFNBQW5CLEdBQ0gsYUFERyxHQUVILElBRko7QUFJRCxHQS91QkQ7O0FBaXZCQSxJQUFFLEVBQUYsQ0FBSyxRQUFMLENBQWMsUUFBZCxHQUF5Qjs7QUFFdkIsVUFBc0IsVUFGQztBQUd2QixlQUFzQixVQUhDOztBQUt2QixZQUFzQixLQUxDO0FBTXZCLFdBQXNCLEtBTkM7QUFPdkIsYUFBc0IsSUFQQztBQVF2QixpQkFBc0IsSUFSQzs7O0FBV3ZCLGlCQUFzQixNQVhDO0FBWXZCLGdCQUFzQixLQVpDOztBQWN2QixjQUFzQixZQUFVLENBQUUsQ0FkWDs7QUFnQnZCLG1CQUFzQixZQUFVLENBQUUsQ0FoQlg7QUFpQnZCLHFCQUFzQixZQUFVLENBQUUsQ0FqQlg7QUFrQnZCLHVCQUFzQixZQUFVLENBQUUsQ0FsQlg7QUFtQnZCLHlCQUFzQixZQUFVLENBQUUsQ0FuQlg7O0FBcUJ2QixlQUFzQixZQUFVLENBQUUsQ0FyQlg7QUFzQnZCLGlCQUFzQixZQUFVLENBQUUsQ0F0Qlg7O0FBd0J2QixtQkFBc0IsWUFBVyxDQUFFLENBeEJaO0FBeUJ2QixxQkFBc0IsWUFBVyxDQUFFLENBekJaOztBQTJCdkIsY0FBc0IsWUFBVSxDQUFFLENBM0JYO0FBNEJ2QixlQUFzQixZQUFVLENBQUUsQ0E1Qlg7OztBQStCdkIsZUFBc0IsWUFBVSxDQUFFLENBL0JYO0FBZ0N2QixnQkFBc0IsWUFBVSxDQUFFLENBaENYOztBQWtDdkIsZUFBa0I7QUFDaEIsZUFBZ0IsU0FEQTtBQUVoQixxQkFBZ0IsZUFGQTtBQUdoQixnQkFBZ0IsVUFIQTtBQUloQixjQUFnQixRQUpBO0FBS2hCLGFBQWdCLE9BTEE7QUFNaEIsZ0JBQWdCO0FBTkEsS0FsQ0s7O0FBMkN2QixXQUFZO0FBQ1YsY0FBZTtBQURMLEtBM0NXOztBQStDdkIsY0FBVztBQUNULGdCQUFXLGNBREY7QUFFVCxhQUFXLGFBRkY7QUFHVCxhQUFXLDZDQUhGO0FBSVQsWUFBVztBQUpGOztBQS9DWSxHQUF6QjtBQXdEQyxDQXB6QkEsRUFvekJHLE1BcHpCSCxFQW96QlcsTUFwekJYLEVBb3pCbUIsUUFwekJuQiIsImZpbGUiOiJjaGVja2JveC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxuICogIyBTZW1hbnRpYyBVSSAtIENoZWNrYm94XG4gKiBodHRwOi8vZ2l0aHViLmNvbS9zZW1hbnRpYy1vcmcvc2VtYW50aWMtdWkvXG4gKlxuICpcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxuICogaHR0cDovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVFxuICpcbiAqL1xuXG47KGZ1bmN0aW9uICgkLCB3aW5kb3csIGRvY3VtZW50LCB1bmRlZmluZWQpIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cbndpbmRvdyA9ICh0eXBlb2Ygd2luZG93ICE9ICd1bmRlZmluZWQnICYmIHdpbmRvdy5NYXRoID09IE1hdGgpXG4gID8gd2luZG93XG4gIDogKHR5cGVvZiBzZWxmICE9ICd1bmRlZmluZWQnICYmIHNlbGYuTWF0aCA9PSBNYXRoKVxuICAgID8gc2VsZlxuICAgIDogRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKVxuO1xuXG4kLmZuLmNoZWNrYm94ID0gZnVuY3Rpb24ocGFyYW1ldGVycykge1xuICB2YXJcbiAgICAkYWxsTW9kdWxlcyAgICA9ICQodGhpcyksXG4gICAgbW9kdWxlU2VsZWN0b3IgPSAkYWxsTW9kdWxlcy5zZWxlY3RvciB8fCAnJyxcblxuICAgIHRpbWUgICAgICAgICAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCksXG4gICAgcGVyZm9ybWFuY2UgICAgPSBbXSxcblxuICAgIHF1ZXJ5ICAgICAgICAgID0gYXJndW1lbnRzWzBdLFxuICAgIG1ldGhvZEludm9rZWQgID0gKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyksXG4gICAgcXVlcnlBcmd1bWVudHMgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSksXG4gICAgcmV0dXJuZWRWYWx1ZVxuICA7XG5cbiAgJGFsbE1vZHVsZXNcbiAgICAuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgIHZhclxuICAgICAgICBzZXR0aW5ncyAgICAgICAgPSAkLmV4dGVuZCh0cnVlLCB7fSwgJC5mbi5jaGVja2JveC5zZXR0aW5ncywgcGFyYW1ldGVycyksXG5cbiAgICAgICAgY2xhc3NOYW1lICAgICAgID0gc2V0dGluZ3MuY2xhc3NOYW1lLFxuICAgICAgICBuYW1lc3BhY2UgICAgICAgPSBzZXR0aW5ncy5uYW1lc3BhY2UsXG4gICAgICAgIHNlbGVjdG9yICAgICAgICA9IHNldHRpbmdzLnNlbGVjdG9yLFxuICAgICAgICBlcnJvciAgICAgICAgICAgPSBzZXR0aW5ncy5lcnJvcixcblxuICAgICAgICBldmVudE5hbWVzcGFjZSAgPSAnLicgKyBuYW1lc3BhY2UsXG4gICAgICAgIG1vZHVsZU5hbWVzcGFjZSA9ICdtb2R1bGUtJyArIG5hbWVzcGFjZSxcblxuICAgICAgICAkbW9kdWxlICAgICAgICAgPSAkKHRoaXMpLFxuICAgICAgICAkbGFiZWwgICAgICAgICAgPSAkKHRoaXMpLmNoaWxkcmVuKHNlbGVjdG9yLmxhYmVsKSxcbiAgICAgICAgJGlucHV0ICAgICAgICAgID0gJCh0aGlzKS5jaGlsZHJlbihzZWxlY3Rvci5pbnB1dCksXG4gICAgICAgIGlucHV0ICAgICAgICAgICA9ICRpbnB1dFswXSxcblxuICAgICAgICBpbml0aWFsTG9hZCAgICAgPSBmYWxzZSxcbiAgICAgICAgc2hvcnRjdXRQcmVzc2VkID0gZmFsc2UsXG4gICAgICAgIGluc3RhbmNlICAgICAgICA9ICRtb2R1bGUuZGF0YShtb2R1bGVOYW1lc3BhY2UpLFxuXG4gICAgICAgIG9ic2VydmVyLFxuICAgICAgICBlbGVtZW50ICAgICAgICAgPSB0aGlzLFxuICAgICAgICBtb2R1bGVcbiAgICAgIDtcblxuICAgICAgbW9kdWxlICAgICAgPSB7XG5cbiAgICAgICAgaW5pdGlhbGl6ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0luaXRpYWxpemluZyBjaGVja2JveCcsIHNldHRpbmdzKTtcblxuICAgICAgICAgIG1vZHVsZS5jcmVhdGUubGFiZWwoKTtcbiAgICAgICAgICBtb2R1bGUuYmluZC5ldmVudHMoKTtcblxuICAgICAgICAgIG1vZHVsZS5zZXQudGFiYmFibGUoKTtcbiAgICAgICAgICBtb2R1bGUuaGlkZS5pbnB1dCgpO1xuXG4gICAgICAgICAgbW9kdWxlLm9ic2VydmVDaGFuZ2VzKCk7XG4gICAgICAgICAgbW9kdWxlLmluc3RhbnRpYXRlKCk7XG4gICAgICAgICAgbW9kdWxlLnNldHVwKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5zdGFudGlhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTdG9yaW5nIGluc3RhbmNlIG9mIG1vZHVsZScsIG1vZHVsZSk7XG4gICAgICAgICAgaW5zdGFuY2UgPSBtb2R1bGU7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLmRhdGEobW9kdWxlTmFtZXNwYWNlLCBtb2R1bGUpXG4gICAgICAgICAgO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlc3Ryb3k6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdEZXN0cm95aW5nIG1vZHVsZScpO1xuICAgICAgICAgIG1vZHVsZS51bmJpbmQuZXZlbnRzKCk7XG4gICAgICAgICAgbW9kdWxlLnNob3cuaW5wdXQoKTtcbiAgICAgICAgICAkbW9kdWxlLnJlbW92ZURhdGEobW9kdWxlTmFtZXNwYWNlKTtcbiAgICAgICAgfSxcblxuICAgICAgICBmaXg6IHtcbiAgICAgICAgICByZWZlcmVuY2U6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoICRtb2R1bGUuaXMoc2VsZWN0b3IuaW5wdXQpICkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0JlaGF2aW9yIGNhbGxlZCBvbiA8aW5wdXQ+IGFkanVzdGluZyBpbnZva2VkIGVsZW1lbnQnKTtcbiAgICAgICAgICAgICAgJG1vZHVsZSA9ICRtb2R1bGUuY2xvc2VzdChzZWxlY3Rvci5jaGVja2JveCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5yZWZyZXNoKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldHVwOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuc2V0LmluaXRpYWxMb2FkKCk7XG4gICAgICAgICAgaWYoIG1vZHVsZS5pcy5pbmRldGVybWluYXRlKCkgKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0luaXRpYWwgdmFsdWUgaXMgaW5kZXRlcm1pbmF0ZScpO1xuICAgICAgICAgICAgbW9kdWxlLmluZGV0ZXJtaW5hdGUoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZiggbW9kdWxlLmlzLmNoZWNrZWQoKSApIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW5pdGlhbCB2YWx1ZSBpcyBjaGVja2VkJyk7XG4gICAgICAgICAgICBtb2R1bGUuY2hlY2soKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0luaXRpYWwgdmFsdWUgaXMgdW5jaGVja2VkJyk7XG4gICAgICAgICAgICBtb2R1bGUudW5jaGVjaygpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBtb2R1bGUucmVtb3ZlLmluaXRpYWxMb2FkKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVmcmVzaDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgJGxhYmVsID0gJG1vZHVsZS5jaGlsZHJlbihzZWxlY3Rvci5sYWJlbCk7XG4gICAgICAgICAgJGlucHV0ID0gJG1vZHVsZS5jaGlsZHJlbihzZWxlY3Rvci5pbnB1dCk7XG4gICAgICAgICAgaW5wdXQgID0gJGlucHV0WzBdO1xuICAgICAgICB9LFxuXG4gICAgICAgIGhpZGU6IHtcbiAgICAgICAgICBpbnB1dDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnTW9kaWZ5aW5nIDxpbnB1dD4gei1pbmRleCB0byBiZSB1bnNlbGVjdGFibGUnKTtcbiAgICAgICAgICAgICRpbnB1dC5hZGRDbGFzcyhjbGFzc05hbWUuaGlkZGVuKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHNob3c6IHtcbiAgICAgICAgICBpbnB1dDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnTW9kaWZ5aW5nIDxpbnB1dD4gei1pbmRleCB0byBiZSBzZWxlY3RhYmxlJyk7XG4gICAgICAgICAgICAkaW5wdXQucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmhpZGRlbik7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIG9ic2VydmVDaGFuZ2VzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZignTXV0YXRpb25PYnNlcnZlcicgaW4gd2luZG93KSB7XG4gICAgICAgICAgICBvYnNlcnZlciA9IG5ldyBNdXRhdGlvbk9ic2VydmVyKGZ1bmN0aW9uKG11dGF0aW9ucykge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0RPTSB0cmVlIG1vZGlmaWVkLCB1cGRhdGluZyBzZWxlY3RvciBjYWNoZScpO1xuICAgICAgICAgICAgICBtb2R1bGUucmVmcmVzaCgpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBvYnNlcnZlci5vYnNlcnZlKGVsZW1lbnQsIHtcbiAgICAgICAgICAgICAgY2hpbGRMaXN0IDogdHJ1ZSxcbiAgICAgICAgICAgICAgc3VidHJlZSAgIDogdHJ1ZVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NldHRpbmcgdXAgbXV0YXRpb24gb2JzZXJ2ZXInLCBvYnNlcnZlcik7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGF0dGFjaEV2ZW50czogZnVuY3Rpb24oc2VsZWN0b3IsIGV2ZW50KSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICAkZWxlbWVudCA9ICQoc2VsZWN0b3IpXG4gICAgICAgICAgO1xuICAgICAgICAgIGV2ZW50ID0gJC5pc0Z1bmN0aW9uKG1vZHVsZVtldmVudF0pXG4gICAgICAgICAgICA/IG1vZHVsZVtldmVudF1cbiAgICAgICAgICAgIDogbW9kdWxlLnRvZ2dsZVxuICAgICAgICAgIDtcbiAgICAgICAgICBpZigkZWxlbWVudC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0F0dGFjaGluZyBjaGVja2JveCBldmVudHMgdG8gZWxlbWVudCcsIHNlbGVjdG9yLCBldmVudCk7XG4gICAgICAgICAgICAkZWxlbWVudFxuICAgICAgICAgICAgICAub24oJ2NsaWNrJyArIGV2ZW50TmFtZXNwYWNlLCBldmVudClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3Iubm90Rm91bmQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBldmVudDoge1xuICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICR0YXJnZXQgPSAkKGV2ZW50LnRhcmdldClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKCAkdGFyZ2V0LmlzKHNlbGVjdG9yLmlucHV0KSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1VzaW5nIGRlZmF1bHQgY2hlY2sgYWN0aW9uIG9uIGluaXRpYWxpemVkIGNoZWNrYm94Jyk7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCAkdGFyZ2V0LmlzKHNlbGVjdG9yLmxpbmspICkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NsaWNraW5nIGxpbmsgaW5zaWRlIGNoZWNrYm94LCBza2lwcGluZyB0b2dnbGUnKTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLnRvZ2dsZSgpO1xuICAgICAgICAgICAgJGlucHV0LmZvY3VzKCk7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAga2V5ZG93bjogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBrZXkgICAgID0gZXZlbnQud2hpY2gsXG4gICAgICAgICAgICAgIGtleUNvZGUgPSB7XG4gICAgICAgICAgICAgICAgZW50ZXIgIDogMTMsXG4gICAgICAgICAgICAgICAgc3BhY2UgIDogMzIsXG4gICAgICAgICAgICAgICAgZXNjYXBlIDogMjdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoa2V5ID09IGtleUNvZGUuZXNjYXBlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdFc2NhcGUga2V5IHByZXNzZWQgYmx1cnJpbmcgZmllbGQnKTtcbiAgICAgICAgICAgICAgJGlucHV0LmJsdXIoKTtcbiAgICAgICAgICAgICAgc2hvcnRjdXRQcmVzc2VkID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoIWV2ZW50LmN0cmxLZXkgJiYgKCBrZXkgPT0ga2V5Q29kZS5zcGFjZSB8fCBrZXkgPT0ga2V5Q29kZS5lbnRlcikgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdFbnRlci9zcGFjZSBrZXkgcHJlc3NlZCwgdG9nZ2xpbmcgY2hlY2tib3gnKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnRvZ2dsZSgpO1xuICAgICAgICAgICAgICBzaG9ydGN1dFByZXNzZWQgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHNob3J0Y3V0UHJlc3NlZCA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAga2V5dXA6IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICBpZihzaG9ydGN1dFByZXNzZWQpIHtcbiAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2hlY2s6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCAhbW9kdWxlLnNob3VsZC5hbGxvd0NoZWNrKCkgKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ2hlY2tpbmcgY2hlY2tib3gnLCAkaW5wdXQpO1xuICAgICAgICAgIG1vZHVsZS5zZXQuY2hlY2tlZCgpO1xuICAgICAgICAgIGlmKCAhbW9kdWxlLnNob3VsZC5pZ25vcmVDYWxsYmFja3MoKSApIHtcbiAgICAgICAgICAgIHNldHRpbmdzLm9uQ2hlY2tlZC5jYWxsKGlucHV0KTtcbiAgICAgICAgICAgIHNldHRpbmdzLm9uQ2hhbmdlLmNhbGwoaW5wdXQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICB1bmNoZWNrOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZiggIW1vZHVsZS5zaG91bGQuYWxsb3dVbmNoZWNrKCkgKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVW5jaGVja2luZyBjaGVja2JveCcpO1xuICAgICAgICAgIG1vZHVsZS5zZXQudW5jaGVja2VkKCk7XG4gICAgICAgICAgaWYoICFtb2R1bGUuc2hvdWxkLmlnbm9yZUNhbGxiYWNrcygpICkge1xuICAgICAgICAgICAgc2V0dGluZ3Mub25VbmNoZWNrZWQuY2FsbChpbnB1dCk7XG4gICAgICAgICAgICBzZXR0aW5ncy5vbkNoYW5nZS5jYWxsKGlucHV0KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5kZXRlcm1pbmF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIG1vZHVsZS5zaG91bGQuYWxsb3dJbmRldGVybWluYXRlKCkgKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NoZWNrYm94IGlzIGFscmVhZHkgaW5kZXRlcm1pbmF0ZScpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgIH1cbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ01ha2luZyBjaGVja2JveCBpbmRldGVybWluYXRlJyk7XG4gICAgICAgICAgbW9kdWxlLnNldC5pbmRldGVybWluYXRlKCk7XG4gICAgICAgICAgaWYoICFtb2R1bGUuc2hvdWxkLmlnbm9yZUNhbGxiYWNrcygpICkge1xuICAgICAgICAgICAgc2V0dGluZ3Mub25JbmRldGVybWluYXRlLmNhbGwoaW5wdXQpO1xuICAgICAgICAgICAgc2V0dGluZ3Mub25DaGFuZ2UuY2FsbChpbnB1dCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGRldGVybWluYXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZiggbW9kdWxlLnNob3VsZC5hbGxvd0RldGVybWluYXRlKCkgKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NoZWNrYm94IGlzIGFscmVhZHkgZGV0ZXJtaW5hdGUnKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdNYWtpbmcgY2hlY2tib3ggZGV0ZXJtaW5hdGUnKTtcbiAgICAgICAgICBtb2R1bGUuc2V0LmRldGVybWluYXRlKCk7XG4gICAgICAgICAgaWYoICFtb2R1bGUuc2hvdWxkLmlnbm9yZUNhbGxiYWNrcygpICkge1xuICAgICAgICAgICAgc2V0dGluZ3Mub25EZXRlcm1pbmF0ZS5jYWxsKGlucHV0KTtcbiAgICAgICAgICAgIHNldHRpbmdzLm9uQ2hhbmdlLmNhbGwoaW5wdXQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBlbmFibGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCBtb2R1bGUuaXMuZW5hYmxlZCgpICkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDaGVja2JveCBpcyBhbHJlYWR5IGVuYWJsZWQnKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdFbmFibGluZyBjaGVja2JveCcpO1xuICAgICAgICAgIG1vZHVsZS5zZXQuZW5hYmxlZCgpO1xuICAgICAgICAgIHNldHRpbmdzLm9uRW5hYmxlLmNhbGwoaW5wdXQpO1xuICAgICAgICAgIC8vIHByZXNlcnZlIGxlZ2FjeSBjYWxsYmFja3NcbiAgICAgICAgICBzZXR0aW5ncy5vbkVuYWJsZWQuY2FsbChpbnB1dCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGlzYWJsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIG1vZHVsZS5pcy5kaXNhYmxlZCgpICkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDaGVja2JveCBpcyBhbHJlYWR5IGRpc2FibGVkJyk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRGlzYWJsaW5nIGNoZWNrYm94Jyk7XG4gICAgICAgICAgbW9kdWxlLnNldC5kaXNhYmxlZCgpO1xuICAgICAgICAgIHNldHRpbmdzLm9uRGlzYWJsZS5jYWxsKGlucHV0KTtcbiAgICAgICAgICAvLyBwcmVzZXJ2ZSBsZWdhY3kgY2FsbGJhY2tzXG4gICAgICAgICAgc2V0dGluZ3Mub25EaXNhYmxlZC5jYWxsKGlucHV0KTtcbiAgICAgICAgfSxcblxuICAgICAgICBnZXQ6IHtcbiAgICAgICAgICByYWRpb3M6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIG5hbWUgPSBtb2R1bGUuZ2V0Lm5hbWUoKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgcmV0dXJuICQoJ2lucHV0W25hbWU9XCInICsgbmFtZSArICdcIl0nKS5jbG9zZXN0KHNlbGVjdG9yLmNoZWNrYm94KTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIG90aGVyUmFkaW9zOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGUuZ2V0LnJhZGlvcygpLm5vdCgkbW9kdWxlKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIG5hbWU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRpbnB1dC5hdHRyKCduYW1lJyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGlzOiB7XG4gICAgICAgICAgaW5pdGlhbExvYWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIGluaXRpYWxMb2FkO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcmFkaW86IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICgkaW5wdXQuaGFzQ2xhc3MoY2xhc3NOYW1lLnJhZGlvKSB8fCAkaW5wdXQuYXR0cigndHlwZScpID09ICdyYWRpbycpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgaW5kZXRlcm1pbmF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJGlucHV0LnByb3AoJ2luZGV0ZXJtaW5hdGUnKSAhPT0gdW5kZWZpbmVkICYmICRpbnB1dC5wcm9wKCdpbmRldGVybWluYXRlJyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBjaGVja2VkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkaW5wdXQucHJvcCgnY2hlY2tlZCcpICE9PSB1bmRlZmluZWQgJiYgJGlucHV0LnByb3AoJ2NoZWNrZWQnKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc2FibGVkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkaW5wdXQucHJvcCgnZGlzYWJsZWQnKSAhPT0gdW5kZWZpbmVkICYmICRpbnB1dC5wcm9wKCdkaXNhYmxlZCcpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZW5hYmxlZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gIW1vZHVsZS5pcy5kaXNhYmxlZCgpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZGV0ZXJtaW5hdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICFtb2R1bGUuaXMuaW5kZXRlcm1pbmF0ZSgpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgdW5jaGVja2VkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAhbW9kdWxlLmlzLmNoZWNrZWQoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2hvdWxkOiB7XG4gICAgICAgICAgYWxsb3dDaGVjazogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihtb2R1bGUuaXMuZGV0ZXJtaW5hdGUoKSAmJiBtb2R1bGUuaXMuY2hlY2tlZCgpICYmICFtb2R1bGUuc2hvdWxkLmZvcmNlQ2FsbGJhY2tzKCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2hvdWxkIG5vdCBhbGxvdyBjaGVjaywgY2hlY2tib3ggaXMgYWxyZWFkeSBjaGVja2VkJyk7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmJlZm9yZUNoZWNrZWQuYXBwbHkoaW5wdXQpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Nob3VsZCBub3QgYWxsb3cgY2hlY2ssIGJlZm9yZUNoZWNrZWQgY2FuY2VsbGVkJyk7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYWxsb3dVbmNoZWNrOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKG1vZHVsZS5pcy5kZXRlcm1pbmF0ZSgpICYmIG1vZHVsZS5pcy51bmNoZWNrZWQoKSAmJiAhbW9kdWxlLnNob3VsZC5mb3JjZUNhbGxiYWNrcygpICkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Nob3VsZCBub3QgYWxsb3cgdW5jaGVjaywgY2hlY2tib3ggaXMgYWxyZWFkeSB1bmNoZWNrZWQnKTtcbiAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoc2V0dGluZ3MuYmVmb3JlVW5jaGVja2VkLmFwcGx5KGlucHV0KSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTaG91bGQgbm90IGFsbG93IHVuY2hlY2ssIGJlZm9yZVVuY2hlY2tlZCBjYW5jZWxsZWQnKTtcbiAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBhbGxvd0luZGV0ZXJtaW5hdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmlzLmluZGV0ZXJtaW5hdGUoKSAmJiAhbW9kdWxlLnNob3VsZC5mb3JjZUNhbGxiYWNrcygpICkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Nob3VsZCBub3QgYWxsb3cgaW5kZXRlcm1pbmF0ZSwgY2hlY2tib3ggaXMgYWxyZWFkeSBpbmRldGVybWluYXRlJyk7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmJlZm9yZUluZGV0ZXJtaW5hdGUuYXBwbHkoaW5wdXQpID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Nob3VsZCBub3QgYWxsb3cgaW5kZXRlcm1pbmF0ZSwgYmVmb3JlSW5kZXRlcm1pbmF0ZSBjYW5jZWxsZWQnKTtcbiAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBhbGxvd0RldGVybWluYXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKG1vZHVsZS5pcy5kZXRlcm1pbmF0ZSgpICYmICFtb2R1bGUuc2hvdWxkLmZvcmNlQ2FsbGJhY2tzKCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2hvdWxkIG5vdCBhbGxvdyBkZXRlcm1pbmF0ZSwgY2hlY2tib3ggaXMgYWxyZWFkeSBkZXRlcm1pbmF0ZScpO1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5iZWZvcmVEZXRlcm1pbmF0ZS5hcHBseShpbnB1dCkgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2hvdWxkIG5vdCBhbGxvdyBkZXRlcm1pbmF0ZSwgYmVmb3JlRGV0ZXJtaW5hdGUgY2FuY2VsbGVkJyk7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZm9yY2VDYWxsYmFja3M6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIChtb2R1bGUuaXMuaW5pdGlhbExvYWQoKSAmJiBzZXR0aW5ncy5maXJlT25Jbml0KTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGlnbm9yZUNhbGxiYWNrczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gKGluaXRpYWxMb2FkICYmICFzZXR0aW5ncy5maXJlT25Jbml0KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2FuOiB7XG4gICAgICAgICAgY2hhbmdlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAhKCAkbW9kdWxlLmhhc0NsYXNzKGNsYXNzTmFtZS5kaXNhYmxlZCkgfHwgJG1vZHVsZS5oYXNDbGFzcyhjbGFzc05hbWUucmVhZE9ubHkpIHx8ICRpbnB1dC5wcm9wKCdkaXNhYmxlZCcpIHx8ICRpbnB1dC5wcm9wKCdyZWFkb25seScpICk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB1bmNoZWNrOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAodHlwZW9mIHNldHRpbmdzLnVuY2hlY2thYmxlID09PSAnYm9vbGVhbicpXG4gICAgICAgICAgICAgID8gc2V0dGluZ3MudW5jaGVja2FibGVcbiAgICAgICAgICAgICAgOiAhbW9kdWxlLmlzLnJhZGlvKClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0OiB7XG4gICAgICAgICAgaW5pdGlhbExvYWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaW5pdGlhbExvYWQgPSB0cnVlO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgY2hlY2tlZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU2V0dGluZyBjbGFzcyB0byBjaGVja2VkJyk7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuaW5kZXRlcm1pbmF0ZSlcbiAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5jaGVja2VkKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoIG1vZHVsZS5pcy5yYWRpbygpICkge1xuICAgICAgICAgICAgICBtb2R1bGUudW5jaGVja090aGVycygpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoIW1vZHVsZS5pcy5pbmRldGVybWluYXRlKCkgJiYgbW9kdWxlLmlzLmNoZWNrZWQoKSkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0lucHV0IGlzIGFscmVhZHkgY2hlY2tlZCwgc2tpcHBpbmcgaW5wdXQgcHJvcGVydHkgY2hhbmdlJyk7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIHN0YXRlIHRvIGNoZWNrZWQnLCBpbnB1dCk7XG4gICAgICAgICAgICAkaW5wdXRcbiAgICAgICAgICAgICAgLnByb3AoJ2luZGV0ZXJtaW5hdGUnLCBmYWxzZSlcbiAgICAgICAgICAgICAgLnByb3AoJ2NoZWNrZWQnLCB0cnVlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnRyaWdnZXIuY2hhbmdlKCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB1bmNoZWNrZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1JlbW92aW5nIGNoZWNrZWQgY2xhc3MnKTtcbiAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5pbmRldGVybWluYXRlKVxuICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmNoZWNrZWQpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZighbW9kdWxlLmlzLmluZGV0ZXJtaW5hdGUoKSAmJiAgbW9kdWxlLmlzLnVuY2hlY2tlZCgpICkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0lucHV0IGlzIGFscmVhZHkgdW5jaGVja2VkJyk7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2V0dGluZyBzdGF0ZSB0byB1bmNoZWNrZWQnKTtcbiAgICAgICAgICAgICRpbnB1dFxuICAgICAgICAgICAgICAucHJvcCgnaW5kZXRlcm1pbmF0ZScsIGZhbHNlKVxuICAgICAgICAgICAgICAucHJvcCgnY2hlY2tlZCcsIGZhbHNlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnRyaWdnZXIuY2hhbmdlKCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBpbmRldGVybWluYXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIGNsYXNzIHRvIGluZGV0ZXJtaW5hdGUnKTtcbiAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5pbmRldGVybWluYXRlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoIG1vZHVsZS5pcy5pbmRldGVybWluYXRlKCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW5wdXQgaXMgYWxyZWFkeSBpbmRldGVybWluYXRlLCBza2lwcGluZyBpbnB1dCBwcm9wZXJ0eSBjaGFuZ2UnKTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTZXR0aW5nIHN0YXRlIHRvIGluZGV0ZXJtaW5hdGUnKTtcbiAgICAgICAgICAgICRpbnB1dFxuICAgICAgICAgICAgICAucHJvcCgnaW5kZXRlcm1pbmF0ZScsIHRydWUpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBtb2R1bGUudHJpZ2dlci5jaGFuZ2UoKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRldGVybWluYXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZW1vdmluZyBpbmRldGVybWluYXRlIGNsYXNzJyk7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuaW5kZXRlcm1pbmF0ZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKCBtb2R1bGUuaXMuZGV0ZXJtaW5hdGUoKSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdJbnB1dCBpcyBhbHJlYWR5IGRldGVybWluYXRlLCBza2lwcGluZyBpbnB1dCBwcm9wZXJ0eSBjaGFuZ2UnKTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTZXR0aW5nIHN0YXRlIHRvIGRldGVybWluYXRlJyk7XG4gICAgICAgICAgICAkaW5wdXRcbiAgICAgICAgICAgICAgLnByb3AoJ2luZGV0ZXJtaW5hdGUnLCBmYWxzZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc2FibGVkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIGNsYXNzIHRvIGRpc2FibGVkJyk7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUuZGlzYWJsZWQpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZiggbW9kdWxlLmlzLmRpc2FibGVkKCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW5wdXQgaXMgYWxyZWFkeSBkaXNhYmxlZCwgc2tpcHBpbmcgaW5wdXQgcHJvcGVydHkgY2hhbmdlJyk7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2V0dGluZyBzdGF0ZSB0byBkaXNhYmxlZCcpO1xuICAgICAgICAgICAgJGlucHV0XG4gICAgICAgICAgICAgIC5wcm9wKCdkaXNhYmxlZCcsICdkaXNhYmxlZCcpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBtb2R1bGUudHJpZ2dlci5jaGFuZ2UoKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGVuYWJsZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1JlbW92aW5nIGRpc2FibGVkIGNsYXNzJyk7XG4gICAgICAgICAgICAkbW9kdWxlLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5kaXNhYmxlZCk7XG4gICAgICAgICAgICBpZiggbW9kdWxlLmlzLmVuYWJsZWQoKSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdJbnB1dCBpcyBhbHJlYWR5IGVuYWJsZWQsIHNraXBwaW5nIGlucHV0IHByb3BlcnR5IGNoYW5nZScpO1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NldHRpbmcgc3RhdGUgdG8gZW5hYmxlZCcpO1xuICAgICAgICAgICAgJGlucHV0XG4gICAgICAgICAgICAgIC5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnRyaWdnZXIuY2hhbmdlKCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB0YWJiYWJsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQWRkaW5nIHRhYmluZGV4IHRvIGNoZWNrYm94Jyk7XG4gICAgICAgICAgICBpZiggJGlucHV0LmF0dHIoJ3RhYmluZGV4JykgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAkaW5wdXQuYXR0cigndGFiaW5kZXgnLCAwKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVtb3ZlOiB7XG4gICAgICAgICAgaW5pdGlhbExvYWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaW5pdGlhbExvYWQgPSBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgdHJpZ2dlcjoge1xuICAgICAgICAgIGNoYW5nZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgZXZlbnRzICAgICAgID0gZG9jdW1lbnQuY3JlYXRlRXZlbnQoJ0hUTUxFdmVudHMnKSxcbiAgICAgICAgICAgICAgaW5wdXRFbGVtZW50ID0gJGlucHV0WzBdXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihpbnB1dEVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1RyaWdnZXJpbmcgbmF0aXZlIGNoYW5nZSBldmVudCcpO1xuICAgICAgICAgICAgICBldmVudHMuaW5pdEV2ZW50KCdjaGFuZ2UnLCB0cnVlLCBmYWxzZSk7XG4gICAgICAgICAgICAgIGlucHV0RWxlbWVudC5kaXNwYXRjaEV2ZW50KGV2ZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG5cbiAgICAgICAgY3JlYXRlOiB7XG4gICAgICAgICAgbGFiZWw6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoJGlucHV0LnByZXZBbGwoc2VsZWN0b3IubGFiZWwpLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgJGlucHV0LnByZXYoc2VsZWN0b3IubGFiZWwpLmRldGFjaCgpLmluc2VydEFmdGVyKCRpbnB1dCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnTW92aW5nIGV4aXN0aW5nIGxhYmVsJywgJGxhYmVsKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoICFtb2R1bGUuaGFzLmxhYmVsKCkgKSB7XG4gICAgICAgICAgICAgICRsYWJlbCA9ICQoJzxsYWJlbD4nKS5pbnNlcnRBZnRlcigkaW5wdXQpO1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NyZWF0aW5nIGxhYmVsJywgJGxhYmVsKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgaGFzOiB7XG4gICAgICAgICAgbGFiZWw6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICgkbGFiZWwubGVuZ3RoID4gMCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGJpbmQ6IHtcbiAgICAgICAgICBldmVudHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0F0dGFjaGluZyBjaGVja2JveCBldmVudHMnKTtcbiAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgLm9uKCdjbGljaycgICArIGV2ZW50TmFtZXNwYWNlLCBtb2R1bGUuZXZlbnQuY2xpY2spXG4gICAgICAgICAgICAgIC5vbigna2V5ZG93bicgKyBldmVudE5hbWVzcGFjZSwgc2VsZWN0b3IuaW5wdXQsIG1vZHVsZS5ldmVudC5rZXlkb3duKVxuICAgICAgICAgICAgICAub24oJ2tleXVwJyAgICsgZXZlbnROYW1lc3BhY2UsIHNlbGVjdG9yLmlucHV0LCBtb2R1bGUuZXZlbnQua2V5dXApXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHVuYmluZDoge1xuICAgICAgICAgIGV2ZW50czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1JlbW92aW5nIGV2ZW50cycpO1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAub2ZmKGV2ZW50TmFtZXNwYWNlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICB1bmNoZWNrT3RoZXJzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgICRyYWRpb3MgPSBtb2R1bGUuZ2V0Lm90aGVyUmFkaW9zKClcbiAgICAgICAgICA7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdVbmNoZWNraW5nIG90aGVyIHJhZGlvcycsICRyYWRpb3MpO1xuICAgICAgICAgICRyYWRpb3MucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmNoZWNrZWQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHRvZ2dsZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoICFtb2R1bGUuY2FuLmNoYW5nZSgpICkge1xuICAgICAgICAgICAgaWYoIW1vZHVsZS5pcy5yYWRpbygpKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ2hlY2tib3ggaXMgcmVhZC1vbmx5IG9yIGRpc2FibGVkLCBpZ25vcmluZyB0b2dnbGUnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoIG1vZHVsZS5pcy5pbmRldGVybWluYXRlKCkgfHwgbW9kdWxlLmlzLnVuY2hlY2tlZCgpICkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDdXJyZW50bHkgdW5jaGVja2VkJyk7XG4gICAgICAgICAgICBtb2R1bGUuY2hlY2soKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZiggbW9kdWxlLmlzLmNoZWNrZWQoKSAmJiBtb2R1bGUuY2FuLnVuY2hlY2soKSApIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ3VycmVudGx5IGNoZWNrZWQnKTtcbiAgICAgICAgICAgIG1vZHVsZS51bmNoZWNrKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBzZXR0aW5nOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ2hhbmdpbmcgc2V0dGluZycsIG5hbWUsIHZhbHVlKTtcbiAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgc2V0dGluZ3MsIG5hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGlmKCQuaXNQbGFpbk9iamVjdChzZXR0aW5nc1tuYW1lXSkpIHtcbiAgICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgc2V0dGluZ3NbbmFtZV0sIHZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBzZXR0aW5nc1tuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBzZXR0aW5nc1tuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludGVybmFsOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBtb2R1bGUsIG5hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIG1vZHVsZVtuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGVbbmFtZV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBkZWJ1ZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy5kZWJ1Zykge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZyA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB2ZXJib3NlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50ICYmIHNldHRpbmdzLnZlcmJvc2UgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGVycm9yOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50KSB7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuZXJyb3IsIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBwZXJmb3JtYW5jZToge1xuICAgICAgICAgIGxvZzogZnVuY3Rpb24obWVzc2FnZSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lLFxuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lLFxuICAgICAgICAgICAgICBwcmV2aW91c1RpbWVcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lICAgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lICA9IHRpbWUgfHwgY3VycmVudFRpbWU7XG4gICAgICAgICAgICAgIGV4ZWN1dGlvblRpbWUgPSBjdXJyZW50VGltZSAtIHByZXZpb3VzVGltZTtcbiAgICAgICAgICAgICAgdGltZSAgICAgICAgICA9IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBwZXJmb3JtYW5jZS5wdXNoKHtcbiAgICAgICAgICAgICAgICAnTmFtZScgICAgICAgICAgIDogbWVzc2FnZVswXSxcbiAgICAgICAgICAgICAgICAnQXJndW1lbnRzJyAgICAgIDogW10uc2xpY2UuY2FsbChtZXNzYWdlLCAxKSB8fCAnJyxcbiAgICAgICAgICAgICAgICAnRWxlbWVudCcgICAgICAgIDogZWxlbWVudCxcbiAgICAgICAgICAgICAgICAnRXhlY3V0aW9uIFRpbWUnIDogZXhlY3V0aW9uVGltZVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyID0gc2V0VGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UuZGlzcGxheSwgNTAwKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc3BsYXk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHRpdGxlID0gc2V0dGluZ3MubmFtZSArICc6JyxcbiAgICAgICAgICAgICAgdG90YWxUaW1lID0gMFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgdGltZSA9IGZhbHNlO1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lcik7XG4gICAgICAgICAgICAkLmVhY2gocGVyZm9ybWFuY2UsIGZ1bmN0aW9uKGluZGV4LCBkYXRhKSB7XG4gICAgICAgICAgICAgIHRvdGFsVGltZSArPSBkYXRhWydFeGVjdXRpb24gVGltZSddO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB0aXRsZSArPSAnICcgKyB0b3RhbFRpbWUgKyAnbXMnO1xuICAgICAgICAgICAgaWYobW9kdWxlU2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgdGl0bGUgKz0gJyBcXCcnICsgbW9kdWxlU2VsZWN0b3IgKyAnXFwnJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCAoY29uc29sZS5ncm91cCAhPT0gdW5kZWZpbmVkIHx8IGNvbnNvbGUudGFibGUgIT09IHVuZGVmaW5lZCkgJiYgcGVyZm9ybWFuY2UubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKHRpdGxlKTtcbiAgICAgICAgICAgICAgaWYoY29uc29sZS50YWJsZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUudGFibGUocGVyZm9ybWFuY2UpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGFbJ05hbWUnXSArICc6ICcgKyBkYXRhWydFeGVjdXRpb24gVGltZSddKydtcycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHBlcmZvcm1hbmNlID0gW107XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnZva2U6IGZ1bmN0aW9uKHF1ZXJ5LCBwYXNzZWRBcmd1bWVudHMsIGNvbnRleHQpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIG9iamVjdCA9IGluc3RhbmNlLFxuICAgICAgICAgICAgbWF4RGVwdGgsXG4gICAgICAgICAgICBmb3VuZCxcbiAgICAgICAgICAgIHJlc3BvbnNlXG4gICAgICAgICAgO1xuICAgICAgICAgIHBhc3NlZEFyZ3VtZW50cyA9IHBhc3NlZEFyZ3VtZW50cyB8fCBxdWVyeUFyZ3VtZW50cztcbiAgICAgICAgICBjb250ZXh0ICAgICAgICAgPSBlbGVtZW50ICAgICAgICAgfHwgY29udGV4dDtcbiAgICAgICAgICBpZih0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycgJiYgb2JqZWN0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5ICAgID0gcXVlcnkuc3BsaXQoL1tcXC4gXS8pO1xuICAgICAgICAgICAgbWF4RGVwdGggPSBxdWVyeS5sZW5ndGggLSAxO1xuICAgICAgICAgICAgJC5lYWNoKHF1ZXJ5LCBmdW5jdGlvbihkZXB0aCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgdmFyIGNhbWVsQ2FzZVZhbHVlID0gKGRlcHRoICE9IG1heERlcHRoKVxuICAgICAgICAgICAgICAgID8gdmFsdWUgKyBxdWVyeVtkZXB0aCArIDFdLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcXVlcnlbZGVwdGggKyAxXS5zbGljZSgxKVxuICAgICAgICAgICAgICAgIDogcXVlcnlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbY2FtZWxDYXNlVmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoICQuaXNQbGFpbk9iamVjdCggb2JqZWN0W3ZhbHVlXSApICYmIChkZXB0aCAhPSBtYXhEZXB0aCkgKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBvYmplY3RbdmFsdWVdICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3IubWV0aG9kLCBxdWVyeSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCAkLmlzRnVuY3Rpb24oIGZvdW5kICkgKSB7XG4gICAgICAgICAgICByZXNwb25zZSA9IGZvdW5kLmFwcGx5KGNvbnRleHQsIHBhc3NlZEFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYoZm91bmQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZDtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoJC5pc0FycmF5KHJldHVybmVkVmFsdWUpKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlLnB1c2gocmVzcG9uc2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IFtyZXR1cm5lZFZhbHVlLCByZXNwb25zZV07XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYocmVzcG9uc2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IHJlc3BvbnNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gZm91bmQ7XG4gICAgICAgIH1cbiAgICAgIH07XG5cbiAgICAgIGlmKG1ldGhvZEludm9rZWQpIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICAgIH1cbiAgICAgICAgbW9kdWxlLmludm9rZShxdWVyeSk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIGluc3RhbmNlLmludm9rZSgnZGVzdHJveScpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICB9XG4gICAgfSlcbiAgO1xuXG4gIHJldHVybiAocmV0dXJuZWRWYWx1ZSAhPT0gdW5kZWZpbmVkKVxuICAgID8gcmV0dXJuZWRWYWx1ZVxuICAgIDogdGhpc1xuICA7XG59O1xuXG4kLmZuLmNoZWNrYm94LnNldHRpbmdzID0ge1xuXG4gIG5hbWUgICAgICAgICAgICAgICAgOiAnQ2hlY2tib3gnLFxuICBuYW1lc3BhY2UgICAgICAgICAgIDogJ2NoZWNrYm94JyxcblxuICBzaWxlbnQgICAgICAgICAgICAgIDogZmFsc2UsXG4gIGRlYnVnICAgICAgICAgICAgICAgOiBmYWxzZSxcbiAgdmVyYm9zZSAgICAgICAgICAgICA6IHRydWUsXG4gIHBlcmZvcm1hbmNlICAgICAgICAgOiB0cnVlLFxuXG4gIC8vIGRlbGVnYXRlZCBldmVudCBjb250ZXh0XG4gIHVuY2hlY2thYmxlICAgICAgICAgOiAnYXV0bycsXG4gIGZpcmVPbkluaXQgICAgICAgICAgOiBmYWxzZSxcblxuICBvbkNoYW5nZSAgICAgICAgICAgIDogZnVuY3Rpb24oKXt9LFxuXG4gIGJlZm9yZUNoZWNrZWQgICAgICAgOiBmdW5jdGlvbigpe30sXG4gIGJlZm9yZVVuY2hlY2tlZCAgICAgOiBmdW5jdGlvbigpe30sXG4gIGJlZm9yZURldGVybWluYXRlICAgOiBmdW5jdGlvbigpe30sXG4gIGJlZm9yZUluZGV0ZXJtaW5hdGUgOiBmdW5jdGlvbigpe30sXG5cbiAgb25DaGVja2VkICAgICAgICAgICA6IGZ1bmN0aW9uKCl7fSxcbiAgb25VbmNoZWNrZWQgICAgICAgICA6IGZ1bmN0aW9uKCl7fSxcblxuICBvbkRldGVybWluYXRlICAgICAgIDogZnVuY3Rpb24oKSB7fSxcbiAgb25JbmRldGVybWluYXRlICAgICA6IGZ1bmN0aW9uKCkge30sXG5cbiAgb25FbmFibGUgICAgICAgICAgICA6IGZ1bmN0aW9uKCl7fSxcbiAgb25EaXNhYmxlICAgICAgICAgICA6IGZ1bmN0aW9uKCl7fSxcblxuICAvLyBwcmVzZXJ2ZSBtaXNzcGVsbGVkIGNhbGxiYWNrcyAod2lsbCBiZSByZW1vdmVkIGluIDMuMClcbiAgb25FbmFibGVkICAgICAgICAgICA6IGZ1bmN0aW9uKCl7fSxcbiAgb25EaXNhYmxlZCAgICAgICAgICA6IGZ1bmN0aW9uKCl7fSxcblxuICBjbGFzc05hbWUgICAgICAgOiB7XG4gICAgY2hlY2tlZCAgICAgICA6ICdjaGVja2VkJyxcbiAgICBpbmRldGVybWluYXRlIDogJ2luZGV0ZXJtaW5hdGUnLFxuICAgIGRpc2FibGVkICAgICAgOiAnZGlzYWJsZWQnLFxuICAgIGhpZGRlbiAgICAgICAgOiAnaGlkZGVuJyxcbiAgICByYWRpbyAgICAgICAgIDogJ3JhZGlvJyxcbiAgICByZWFkT25seSAgICAgIDogJ3JlYWQtb25seSdcbiAgfSxcblxuICBlcnJvciAgICAgOiB7XG4gICAgbWV0aG9kICAgICAgIDogJ1RoZSBtZXRob2QgeW91IGNhbGxlZCBpcyBub3QgZGVmaW5lZCdcbiAgfSxcblxuICBzZWxlY3RvciA6IHtcbiAgICBjaGVja2JveCA6ICcudWkuY2hlY2tib3gnLFxuICAgIGxhYmVsICAgIDogJ2xhYmVsLCAuYm94JyxcbiAgICBpbnB1dCAgICA6ICdpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0sIGlucHV0W3R5cGU9XCJyYWRpb1wiXScsXG4gICAgbGluayAgICAgOiAnYVtocmVmXSdcbiAgfVxuXG59O1xuXG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7XG4iXX0=