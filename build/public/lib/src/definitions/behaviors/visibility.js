/*!
 * # Semantic UI - Visibility
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.visibility = function (parameters) {
    var $allModules = $(this),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue,
        moduleCount = $allModules.length,
        loadedCount = 0;

    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.visibility.settings, parameters) : $.extend({}, $.fn.visibility.settings),
          className = settings.className,
          namespace = settings.namespace,
          error = settings.error,
          metadata = settings.metadata,
          eventNamespace = '.' + namespace,
          moduleNamespace = 'module-' + namespace,
          $window = $(window),
          $module = $(this),
          $context = $(settings.context),
          $placeholder,
          selector = $module.selector || '',
          instance = $module.data(moduleNamespace),
          requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || function (callback) {
        setTimeout(callback, 0);
      },
          element = this,
          disabled = false,
          contextObserver,
          observer,
          module;

      module = {

        initialize: function () {
          module.debug('Initializing', settings);

          module.setup.cache();

          if (module.should.trackChanges()) {

            if (settings.type == 'image') {
              module.setup.image();
            }
            if (settings.type == 'fixed') {
              module.setup.fixed();
            }

            if (settings.observeChanges) {
              module.observeChanges();
            }
            module.bind.events();
          }

          module.save.position();
          if (!module.is.visible()) {
            module.error(error.visible, $module);
          }

          if (settings.initialCheck) {
            module.checkVisibility();
          }
          module.instantiate();
        },

        instantiate: function () {
          module.debug('Storing instance', module);
          $module.data(moduleNamespace, module);
          instance = module;
        },

        destroy: function () {
          module.verbose('Destroying previous module');
          if (observer) {
            observer.disconnect();
          }
          if (contextObserver) {
            contextObserver.disconnect();
          }
          $window.off('load' + eventNamespace, module.event.load).off('resize' + eventNamespace, module.event.resize);
          $context.off('scroll' + eventNamespace, module.event.scroll).off('scrollchange' + eventNamespace, module.event.scrollchange);
          if (settings.type == 'fixed') {
            module.resetFixed();
            module.remove.placeholder();
          }
          $module.off(eventNamespace).removeData(moduleNamespace);
        },

        observeChanges: function () {
          if ('MutationObserver' in window) {
            contextObserver = new MutationObserver(module.event.contextChanged);
            observer = new MutationObserver(module.event.changed);
            contextObserver.observe(document, {
              childList: true,
              subtree: true
            });
            observer.observe(element, {
              childList: true,
              subtree: true
            });
            module.debug('Setting up mutation observer', observer);
          }
        },

        bind: {
          events: function () {
            module.verbose('Binding visibility events to scroll and resize');
            if (settings.refreshOnLoad) {
              $window.on('load' + eventNamespace, module.event.load);
            }
            $window.on('resize' + eventNamespace, module.event.resize);
            // pub/sub pattern
            $context.off('scroll' + eventNamespace).on('scroll' + eventNamespace, module.event.scroll).on('scrollchange' + eventNamespace, module.event.scrollchange);
          }
        },

        event: {
          changed: function (mutations) {
            module.verbose('DOM tree modified, updating visibility calculations');
            module.timer = setTimeout(function () {
              module.verbose('DOM tree modified, updating sticky menu');
              module.refresh();
            }, 100);
          },
          contextChanged: function (mutations) {
            [].forEach.call(mutations, function (mutation) {
              if (mutation.removedNodes) {
                [].forEach.call(mutation.removedNodes, function (node) {
                  if (node == element || $(node).find(element).length > 0) {
                    module.debug('Element removed from DOM, tearing down events');
                    module.destroy();
                  }
                });
              }
            });
          },
          resize: function () {
            module.debug('Window resized');
            if (settings.refreshOnResize) {
              requestAnimationFrame(module.refresh);
            }
          },
          load: function () {
            module.debug('Page finished loading');
            requestAnimationFrame(module.refresh);
          },
          // publishes scrollchange event on one scroll
          scroll: function () {
            if (settings.throttle) {
              clearTimeout(module.timer);
              module.timer = setTimeout(function () {
                $context.triggerHandler('scrollchange' + eventNamespace, [$context.scrollTop()]);
              }, settings.throttle);
            } else {
              requestAnimationFrame(function () {
                $context.triggerHandler('scrollchange' + eventNamespace, [$context.scrollTop()]);
              });
            }
          },
          // subscribes to scrollchange
          scrollchange: function (event, scrollPosition) {
            module.checkVisibility(scrollPosition);
          }
        },

        precache: function (images, callback) {
          if (!(images instanceof Array)) {
            images = [images];
          }
          var imagesLength = images.length,
              loadedCounter = 0,
              cache = [],
              cacheImage = document.createElement('img'),
              handleLoad = function () {
            loadedCounter++;
            if (loadedCounter >= images.length) {
              if ($.isFunction(callback)) {
                callback();
              }
            }
          };
          while (imagesLength--) {
            cacheImage = document.createElement('img');
            cacheImage.onload = handleLoad;
            cacheImage.onerror = handleLoad;
            cacheImage.src = images[imagesLength];
            cache.push(cacheImage);
          }
        },

        enableCallbacks: function () {
          module.debug('Allowing callbacks to occur');
          disabled = false;
        },

        disableCallbacks: function () {
          module.debug('Disabling all callbacks temporarily');
          disabled = true;
        },

        should: {
          trackChanges: function () {
            if (methodInvoked) {
              module.debug('One time query, no need to bind events');
              return false;
            }
            module.debug('Callbacks being attached');
            return true;
          }
        },

        setup: {
          cache: function () {
            module.cache = {
              occurred: {},
              screen: {},
              element: {}
            };
          },
          image: function () {
            var src = $module.data(metadata.src);
            if (src) {
              module.verbose('Lazy loading image', src);
              settings.once = true;
              settings.observeChanges = false;

              // show when top visible
              settings.onOnScreen = function () {
                module.debug('Image on screen', element);
                module.precache(src, function () {
                  module.set.image(src, function () {
                    loadedCount++;
                    if (loadedCount == moduleCount) {
                      settings.onAllLoaded.call(this);
                    }
                    settings.onLoad.call(this);
                  });
                });
              };
            }
          },
          fixed: function () {
            module.debug('Setting up fixed');
            settings.once = false;
            settings.observeChanges = false;
            settings.initialCheck = true;
            settings.refreshOnLoad = true;
            if (!parameters.transition) {
              settings.transition = false;
            }
            module.create.placeholder();
            module.debug('Added placeholder', $placeholder);
            settings.onTopPassed = function () {
              module.debug('Element passed, adding fixed position', $module);
              module.show.placeholder();
              module.set.fixed();
              if (settings.transition) {
                if ($.fn.transition !== undefined) {
                  $module.transition(settings.transition, settings.duration);
                }
              }
            };
            settings.onTopPassedReverse = function () {
              module.debug('Element returned to position, removing fixed', $module);
              module.hide.placeholder();
              module.remove.fixed();
            };
          }
        },

        create: {
          placeholder: function () {
            module.verbose('Creating fixed position placeholder');
            $placeholder = $module.clone(false).css('display', 'none').addClass(className.placeholder).insertAfter($module);
          }
        },

        show: {
          placeholder: function () {
            module.verbose('Showing placeholder');
            $placeholder.css('display', 'block').css('visibility', 'hidden');
          }
        },
        hide: {
          placeholder: function () {
            module.verbose('Hiding placeholder');
            $placeholder.css('display', 'none').css('visibility', '');
          }
        },

        set: {
          fixed: function () {
            module.verbose('Setting element to fixed position');
            $module.addClass(className.fixed).css({
              position: 'fixed',
              top: settings.offset + 'px',
              left: 'auto',
              zIndex: settings.zIndex
            });
            settings.onFixed.call(element);
          },
          image: function (src, callback) {
            $module.attr('src', src);
            if (settings.transition) {
              if ($.fn.transition !== undefined) {
                $module.transition(settings.transition, settings.duration, callback);
              } else {
                $module.fadeIn(settings.duration, callback);
              }
            } else {
              $module.show();
            }
          }
        },

        is: {
          onScreen: function () {
            var calculations = module.get.elementCalculations();
            return calculations.onScreen;
          },
          offScreen: function () {
            var calculations = module.get.elementCalculations();
            return calculations.offScreen;
          },
          visible: function () {
            if (module.cache && module.cache.element) {
              return !(module.cache.element.width === 0 && module.cache.element.offset.top === 0);
            }
            return false;
          }
        },

        refresh: function () {
          module.debug('Refreshing constants (width/height)');
          if (settings.type == 'fixed') {
            module.resetFixed();
          }
          module.reset();
          module.save.position();
          if (settings.checkOnRefresh) {
            module.checkVisibility();
          }
          settings.onRefresh.call(element);
        },

        resetFixed: function () {
          module.remove.fixed();
          module.remove.occurred();
        },

        reset: function () {
          module.verbose('Resetting all cached values');
          if ($.isPlainObject(module.cache)) {
            module.cache.screen = {};
            module.cache.element = {};
          }
        },

        checkVisibility: function (scroll) {
          module.verbose('Checking visibility of element', module.cache.element);

          if (!disabled && module.is.visible()) {

            // save scroll position
            module.save.scroll(scroll);

            // update calculations derived from scroll
            module.save.calculations();

            // percentage
            module.passed();

            // reverse (must be first)
            module.passingReverse();
            module.topVisibleReverse();
            module.bottomVisibleReverse();
            module.topPassedReverse();
            module.bottomPassedReverse();

            // one time
            module.onScreen();
            module.offScreen();
            module.passing();
            module.topVisible();
            module.bottomVisible();
            module.topPassed();
            module.bottomPassed();

            // on update callback
            if (settings.onUpdate) {
              settings.onUpdate.call(element, module.get.elementCalculations());
            }
          }
        },

        passed: function (amount, newCallback) {
          var calculations = module.get.elementCalculations(),
              amountInPixels;
          // assign callback
          if (amount && newCallback) {
            settings.onPassed[amount] = newCallback;
          } else if (amount !== undefined) {
            return module.get.pixelsPassed(amount) > calculations.pixelsPassed;
          } else if (calculations.passing) {
            $.each(settings.onPassed, function (amount, callback) {
              if (calculations.bottomVisible || calculations.pixelsPassed > module.get.pixelsPassed(amount)) {
                module.execute(callback, amount);
              } else if (!settings.once) {
                module.remove.occurred(callback);
              }
            });
          }
        },

        onScreen: function (newCallback) {
          var calculations = module.get.elementCalculations(),
              callback = newCallback || settings.onOnScreen,
              callbackName = 'onScreen';
          if (newCallback) {
            module.debug('Adding callback for onScreen', newCallback);
            settings.onOnScreen = newCallback;
          }
          if (calculations.onScreen) {
            module.execute(callback, callbackName);
          } else if (!settings.once) {
            module.remove.occurred(callbackName);
          }
          if (newCallback !== undefined) {
            return calculations.onOnScreen;
          }
        },

        offScreen: function (newCallback) {
          var calculations = module.get.elementCalculations(),
              callback = newCallback || settings.onOffScreen,
              callbackName = 'offScreen';
          if (newCallback) {
            module.debug('Adding callback for offScreen', newCallback);
            settings.onOffScreen = newCallback;
          }
          if (calculations.offScreen) {
            module.execute(callback, callbackName);
          } else if (!settings.once) {
            module.remove.occurred(callbackName);
          }
          if (newCallback !== undefined) {
            return calculations.onOffScreen;
          }
        },

        passing: function (newCallback) {
          var calculations = module.get.elementCalculations(),
              callback = newCallback || settings.onPassing,
              callbackName = 'passing';
          if (newCallback) {
            module.debug('Adding callback for passing', newCallback);
            settings.onPassing = newCallback;
          }
          if (calculations.passing) {
            module.execute(callback, callbackName);
          } else if (!settings.once) {
            module.remove.occurred(callbackName);
          }
          if (newCallback !== undefined) {
            return calculations.passing;
          }
        },

        topVisible: function (newCallback) {
          var calculations = module.get.elementCalculations(),
              callback = newCallback || settings.onTopVisible,
              callbackName = 'topVisible';
          if (newCallback) {
            module.debug('Adding callback for top visible', newCallback);
            settings.onTopVisible = newCallback;
          }
          if (calculations.topVisible) {
            module.execute(callback, callbackName);
          } else if (!settings.once) {
            module.remove.occurred(callbackName);
          }
          if (newCallback === undefined) {
            return calculations.topVisible;
          }
        },

        bottomVisible: function (newCallback) {
          var calculations = module.get.elementCalculations(),
              callback = newCallback || settings.onBottomVisible,
              callbackName = 'bottomVisible';
          if (newCallback) {
            module.debug('Adding callback for bottom visible', newCallback);
            settings.onBottomVisible = newCallback;
          }
          if (calculations.bottomVisible) {
            module.execute(callback, callbackName);
          } else if (!settings.once) {
            module.remove.occurred(callbackName);
          }
          if (newCallback === undefined) {
            return calculations.bottomVisible;
          }
        },

        topPassed: function (newCallback) {
          var calculations = module.get.elementCalculations(),
              callback = newCallback || settings.onTopPassed,
              callbackName = 'topPassed';
          if (newCallback) {
            module.debug('Adding callback for top passed', newCallback);
            settings.onTopPassed = newCallback;
          }
          if (calculations.topPassed) {
            module.execute(callback, callbackName);
          } else if (!settings.once) {
            module.remove.occurred(callbackName);
          }
          if (newCallback === undefined) {
            return calculations.topPassed;
          }
        },

        bottomPassed: function (newCallback) {
          var calculations = module.get.elementCalculations(),
              callback = newCallback || settings.onBottomPassed,
              callbackName = 'bottomPassed';
          if (newCallback) {
            module.debug('Adding callback for bottom passed', newCallback);
            settings.onBottomPassed = newCallback;
          }
          if (calculations.bottomPassed) {
            module.execute(callback, callbackName);
          } else if (!settings.once) {
            module.remove.occurred(callbackName);
          }
          if (newCallback === undefined) {
            return calculations.bottomPassed;
          }
        },

        passingReverse: function (newCallback) {
          var calculations = module.get.elementCalculations(),
              callback = newCallback || settings.onPassingReverse,
              callbackName = 'passingReverse';
          if (newCallback) {
            module.debug('Adding callback for passing reverse', newCallback);
            settings.onPassingReverse = newCallback;
          }
          if (!calculations.passing) {
            if (module.get.occurred('passing')) {
              module.execute(callback, callbackName);
            }
          } else if (!settings.once) {
            module.remove.occurred(callbackName);
          }
          if (newCallback !== undefined) {
            return !calculations.passing;
          }
        },

        topVisibleReverse: function (newCallback) {
          var calculations = module.get.elementCalculations(),
              callback = newCallback || settings.onTopVisibleReverse,
              callbackName = 'topVisibleReverse';
          if (newCallback) {
            module.debug('Adding callback for top visible reverse', newCallback);
            settings.onTopVisibleReverse = newCallback;
          }
          if (!calculations.topVisible) {
            if (module.get.occurred('topVisible')) {
              module.execute(callback, callbackName);
            }
          } else if (!settings.once) {
            module.remove.occurred(callbackName);
          }
          if (newCallback === undefined) {
            return !calculations.topVisible;
          }
        },

        bottomVisibleReverse: function (newCallback) {
          var calculations = module.get.elementCalculations(),
              callback = newCallback || settings.onBottomVisibleReverse,
              callbackName = 'bottomVisibleReverse';
          if (newCallback) {
            module.debug('Adding callback for bottom visible reverse', newCallback);
            settings.onBottomVisibleReverse = newCallback;
          }
          if (!calculations.bottomVisible) {
            if (module.get.occurred('bottomVisible')) {
              module.execute(callback, callbackName);
            }
          } else if (!settings.once) {
            module.remove.occurred(callbackName);
          }
          if (newCallback === undefined) {
            return !calculations.bottomVisible;
          }
        },

        topPassedReverse: function (newCallback) {
          var calculations = module.get.elementCalculations(),
              callback = newCallback || settings.onTopPassedReverse,
              callbackName = 'topPassedReverse';
          if (newCallback) {
            module.debug('Adding callback for top passed reverse', newCallback);
            settings.onTopPassedReverse = newCallback;
          }
          if (!calculations.topPassed) {
            if (module.get.occurred('topPassed')) {
              module.execute(callback, callbackName);
            }
          } else if (!settings.once) {
            module.remove.occurred(callbackName);
          }
          if (newCallback === undefined) {
            return !calculations.onTopPassed;
          }
        },

        bottomPassedReverse: function (newCallback) {
          var calculations = module.get.elementCalculations(),
              callback = newCallback || settings.onBottomPassedReverse,
              callbackName = 'bottomPassedReverse';
          if (newCallback) {
            module.debug('Adding callback for bottom passed reverse', newCallback);
            settings.onBottomPassedReverse = newCallback;
          }
          if (!calculations.bottomPassed) {
            if (module.get.occurred('bottomPassed')) {
              module.execute(callback, callbackName);
            }
          } else if (!settings.once) {
            module.remove.occurred(callbackName);
          }
          if (newCallback === undefined) {
            return !calculations.bottomPassed;
          }
        },

        execute: function (callback, callbackName) {
          var calculations = module.get.elementCalculations(),
              screen = module.get.screenCalculations();
          callback = callback || false;
          if (callback) {
            if (settings.continuous) {
              module.debug('Callback being called continuously', callbackName, calculations);
              callback.call(element, calculations, screen);
            } else if (!module.get.occurred(callbackName)) {
              module.debug('Conditions met', callbackName, calculations);
              callback.call(element, calculations, screen);
            }
          }
          module.save.occurred(callbackName);
        },

        remove: {
          fixed: function () {
            module.debug('Removing fixed position');
            $module.removeClass(className.fixed).css({
              position: '',
              top: '',
              left: '',
              zIndex: ''
            });
            settings.onUnfixed.call(element);
          },
          placeholder: function () {
            module.debug('Removing placeholder content');
            if ($placeholder) {
              $placeholder.remove();
            }
          },
          occurred: function (callback) {
            if (callback) {
              var occurred = module.cache.occurred;
              if (occurred[callback] !== undefined && occurred[callback] === true) {
                module.debug('Callback can now be called again', callback);
                module.cache.occurred[callback] = false;
              }
            } else {
              module.cache.occurred = {};
            }
          }
        },

        save: {
          calculations: function () {
            module.verbose('Saving all calculations necessary to determine positioning');
            module.save.direction();
            module.save.screenCalculations();
            module.save.elementCalculations();
          },
          occurred: function (callback) {
            if (callback) {
              if (module.cache.occurred[callback] === undefined || module.cache.occurred[callback] !== true) {
                module.verbose('Saving callback occurred', callback);
                module.cache.occurred[callback] = true;
              }
            }
          },
          scroll: function (scrollPosition) {
            scrollPosition = scrollPosition + settings.offset || $context.scrollTop() + settings.offset;
            module.cache.scroll = scrollPosition;
          },
          direction: function () {
            var scroll = module.get.scroll(),
                lastScroll = module.get.lastScroll(),
                direction;
            if (scroll > lastScroll && lastScroll) {
              direction = 'down';
            } else if (scroll < lastScroll && lastScroll) {
              direction = 'up';
            } else {
              direction = 'static';
            }
            module.cache.direction = direction;
            return module.cache.direction;
          },
          elementPosition: function () {
            var element = module.cache.element,
                screen = module.get.screenSize();
            module.verbose('Saving element position');
            // (quicker than $.extend)
            element.fits = element.height < screen.height;
            element.offset = $module.offset();
            element.width = $module.outerWidth();
            element.height = $module.outerHeight();
            // store
            module.cache.element = element;
            return element;
          },
          elementCalculations: function () {
            var screen = module.get.screenCalculations(),
                element = module.get.elementPosition();
            // offset
            if (settings.includeMargin) {
              element.margin = {};
              element.margin.top = parseInt($module.css('margin-top'), 10);
              element.margin.bottom = parseInt($module.css('margin-bottom'), 10);
              element.top = element.offset.top - element.margin.top;
              element.bottom = element.offset.top + element.height + element.margin.bottom;
            } else {
              element.top = element.offset.top;
              element.bottom = element.offset.top + element.height;
            }

            // visibility
            element.topVisible = screen.bottom >= element.top;
            element.topPassed = screen.top >= element.top;
            element.bottomVisible = screen.bottom >= element.bottom;
            element.bottomPassed = screen.top >= element.bottom;
            element.pixelsPassed = 0;
            element.percentagePassed = 0;

            // meta calculations
            element.onScreen = element.topVisible && !element.bottomPassed;
            element.passing = element.topPassed && !element.bottomPassed;
            element.offScreen = !element.onScreen;

            // passing calculations
            if (element.passing) {
              element.pixelsPassed = screen.top - element.top;
              element.percentagePassed = (screen.top - element.top) / element.height;
            }
            module.cache.element = element;
            module.verbose('Updated element calculations', element);
            return element;
          },
          screenCalculations: function () {
            var scroll = module.get.scroll();
            module.save.direction();
            module.cache.screen.top = scroll;
            module.cache.screen.bottom = scroll + module.cache.screen.height;
            return module.cache.screen;
          },
          screenSize: function () {
            module.verbose('Saving window position');
            module.cache.screen = {
              height: $context.height()
            };
          },
          position: function () {
            module.save.screenSize();
            module.save.elementPosition();
          }
        },

        get: {
          pixelsPassed: function (amount) {
            var element = module.get.elementCalculations();
            if (amount.search('%') > -1) {
              return element.height * (parseInt(amount, 10) / 100);
            }
            return parseInt(amount, 10);
          },
          occurred: function (callback) {
            return module.cache.occurred !== undefined ? module.cache.occurred[callback] || false : false;
          },
          direction: function () {
            if (module.cache.direction === undefined) {
              module.save.direction();
            }
            return module.cache.direction;
          },
          elementPosition: function () {
            if (module.cache.element === undefined) {
              module.save.elementPosition();
            }
            return module.cache.element;
          },
          elementCalculations: function () {
            if (module.cache.element === undefined) {
              module.save.elementCalculations();
            }
            return module.cache.element;
          },
          screenCalculations: function () {
            if (module.cache.screen === undefined) {
              module.save.screenCalculations();
            }
            return module.cache.screen;
          },
          screenSize: function () {
            if (module.cache.screen === undefined) {
              module.save.screenSize();
            }
            return module.cache.screen;
          },
          scroll: function () {
            if (module.cache.scroll === undefined) {
              module.save.scroll();
            }
            return module.cache.scroll;
          },
          lastScroll: function () {
            if (module.cache.screen === undefined) {
              module.debug('First scroll event, no last scroll could be found');
              return false;
            }
            return module.cache.screen.top;
          }
        },

        setting: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            settings[name] = value;
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                module.error(error.method, query);
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };

      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        instance.save.scroll();
        instance.save.calculations();
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.visibility.settings = {

    name: 'Visibility',
    namespace: 'visibility',

    debug: false,
    verbose: false,
    performance: true,

    // whether to use mutation observers to follow changes
    observeChanges: true,

    // check position immediately on init
    initialCheck: true,

    // whether to refresh calculations after all page images load
    refreshOnLoad: true,

    // whether to refresh calculations after page resize event
    refreshOnResize: true,

    // should call callbacks on refresh event (resize, etc)
    checkOnRefresh: true,

    // callback should only occur one time
    once: true,

    // callback should fire continuously whe evaluates to true
    continuous: false,

    // offset to use with scroll top
    offset: 0,

    // whether to include margin in elements position
    includeMargin: false,

    // scroll context for visibility checks
    context: window,

    // visibility check delay in ms (defaults to animationFrame)
    throttle: false,

    // special visibility type (image, fixed)
    type: false,

    // z-index to use with visibility 'fixed'
    zIndex: '10',

    // image only animation settings
    transition: 'fade in',
    duration: 1000,

    // array of callbacks for percentage
    onPassed: {},

    // standard callbacks
    onOnScreen: false,
    onOffScreen: false,
    onPassing: false,
    onTopVisible: false,
    onBottomVisible: false,
    onTopPassed: false,
    onBottomPassed: false,

    // reverse callbacks
    onPassingReverse: false,
    onTopVisibleReverse: false,
    onBottomVisibleReverse: false,
    onTopPassedReverse: false,
    onBottomPassedReverse: false,

    // special callbacks for image
    onLoad: function () {},
    onAllLoaded: function () {},

    // special callbacks for fixed position
    onFixed: function () {},
    onUnfixed: function () {},

    // utility callbacks
    onUpdate: false, // disabled by default for performance
    onRefresh: function () {},

    metadata: {
      src: 'src'
    },

    className: {
      fixed: 'fixed',
      placeholder: 'placeholder'
    },

    error: {
      method: 'The method you called is not defined.',
      visible: 'Element is hidden, you must call refresh after element becomes visible'
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9iZWhhdmlvcnMvdmlzaWJpbGl0eS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBVUEsQ0FBQyxDQUFDLFVBQVUsQ0FBVixFQUFhLE1BQWIsRUFBcUIsUUFBckIsRUFBK0IsU0FBL0IsRUFBMEM7O0FBRTVDOztBQUVBLFdBQVUsT0FBTyxNQUFQLElBQWlCLFdBQWpCLElBQWdDLE9BQU8sSUFBUCxJQUFlLElBQWhELEdBQ0wsTUFESyxHQUVKLE9BQU8sSUFBUCxJQUFlLFdBQWYsSUFBOEIsS0FBSyxJQUFMLElBQWEsSUFBNUMsR0FDRSxJQURGLEdBRUUsU0FBUyxhQUFULEdBSk47O0FBT0EsSUFBRSxFQUFGLENBQUssVUFBTCxHQUFrQixVQUFTLFVBQVQsRUFBcUI7QUFDckMsUUFDRSxjQUFpQixFQUFFLElBQUYsQ0FEbkI7QUFBQSxRQUVFLGlCQUFpQixZQUFZLFFBQVosSUFBd0IsRUFGM0M7QUFBQSxRQUlFLE9BQWlCLElBQUksSUFBSixHQUFXLE9BQVgsRUFKbkI7QUFBQSxRQUtFLGNBQWlCLEVBTG5CO0FBQUEsUUFPRSxRQUFpQixVQUFVLENBQVYsQ0FQbkI7QUFBQSxRQVFFLGdCQUFrQixPQUFPLEtBQVAsSUFBZ0IsUUFScEM7QUFBQSxRQVNFLGlCQUFpQixHQUFHLEtBQUgsQ0FBUyxJQUFULENBQWMsU0FBZCxFQUF5QixDQUF6QixDQVRuQjtBQUFBLFFBVUUsYUFWRjtBQUFBLFFBWUUsY0FBaUIsWUFBWSxNQVovQjtBQUFBLFFBYUUsY0FBaUIsQ0FibkI7O0FBZ0JBLGdCQUNHLElBREgsQ0FDUSxZQUFXO0FBQ2YsVUFDRSxXQUFvQixFQUFFLGFBQUYsQ0FBZ0IsVUFBaEIsQ0FBRixHQUNkLEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLEVBQUUsRUFBRixDQUFLLFVBQUwsQ0FBZ0IsUUFBbkMsRUFBNkMsVUFBN0MsQ0FEYyxHQUVkLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxFQUFFLEVBQUYsQ0FBSyxVQUFMLENBQWdCLFFBQTdCLENBSE47QUFBQSxVQUtFLFlBQWtCLFNBQVMsU0FMN0I7QUFBQSxVQU1FLFlBQWtCLFNBQVMsU0FON0I7QUFBQSxVQU9FLFFBQWtCLFNBQVMsS0FQN0I7QUFBQSxVQVFFLFdBQWtCLFNBQVMsUUFSN0I7QUFBQSxVQVVFLGlCQUFrQixNQUFNLFNBVjFCO0FBQUEsVUFXRSxrQkFBa0IsWUFBWSxTQVhoQztBQUFBLFVBYUUsVUFBa0IsRUFBRSxNQUFGLENBYnBCO0FBQUEsVUFlRSxVQUFrQixFQUFFLElBQUYsQ0FmcEI7QUFBQSxVQWdCRSxXQUFrQixFQUFFLFNBQVMsT0FBWCxDQWhCcEI7QUFBQSxVQWtCRSxZQWxCRjtBQUFBLFVBb0JFLFdBQWtCLFFBQVEsUUFBUixJQUFvQixFQXBCeEM7QUFBQSxVQXFCRSxXQUFrQixRQUFRLElBQVIsQ0FBYSxlQUFiLENBckJwQjtBQUFBLFVBdUJFLHdCQUF3QixPQUFPLHFCQUFQLElBQ25CLE9BQU8sd0JBRFksSUFFbkIsT0FBTywyQkFGWSxJQUduQixPQUFPLHVCQUhZLElBSW5CLFVBQVMsUUFBVCxFQUFtQjtBQUFFLG1CQUFXLFFBQVgsRUFBcUIsQ0FBckI7QUFBMEIsT0EzQnREO0FBQUEsVUE2QkUsVUFBa0IsSUE3QnBCO0FBQUEsVUE4QkUsV0FBa0IsS0E5QnBCO0FBQUEsVUFnQ0UsZUFoQ0Y7QUFBQSxVQWlDRSxRQWpDRjtBQUFBLFVBa0NFLE1BbENGOztBQXFDQSxlQUFTOztBQUVQLG9CQUFZLFlBQVc7QUFDckIsaUJBQU8sS0FBUCxDQUFhLGNBQWIsRUFBNkIsUUFBN0I7O0FBRUEsaUJBQU8sS0FBUCxDQUFhLEtBQWI7O0FBRUEsY0FBSSxPQUFPLE1BQVAsQ0FBYyxZQUFkLEVBQUosRUFBbUM7O0FBRWpDLGdCQUFHLFNBQVMsSUFBVCxJQUFpQixPQUFwQixFQUE2QjtBQUMzQixxQkFBTyxLQUFQLENBQWEsS0FBYjtBQUNEO0FBQ0QsZ0JBQUcsU0FBUyxJQUFULElBQWlCLE9BQXBCLEVBQTZCO0FBQzNCLHFCQUFPLEtBQVAsQ0FBYSxLQUFiO0FBQ0Q7O0FBRUQsZ0JBQUcsU0FBUyxjQUFaLEVBQTRCO0FBQzFCLHFCQUFPLGNBQVA7QUFDRDtBQUNELG1CQUFPLElBQVAsQ0FBWSxNQUFaO0FBQ0Q7O0FBRUQsaUJBQU8sSUFBUCxDQUFZLFFBQVo7QUFDQSxjQUFJLENBQUMsT0FBTyxFQUFQLENBQVUsT0FBVixFQUFMLEVBQTJCO0FBQ3pCLG1CQUFPLEtBQVAsQ0FBYSxNQUFNLE9BQW5CLEVBQTRCLE9BQTVCO0FBQ0Q7O0FBRUQsY0FBRyxTQUFTLFlBQVosRUFBMEI7QUFDeEIsbUJBQU8sZUFBUDtBQUNEO0FBQ0QsaUJBQU8sV0FBUDtBQUNELFNBL0JNOztBQWlDUCxxQkFBYSxZQUFXO0FBQ3RCLGlCQUFPLEtBQVAsQ0FBYSxrQkFBYixFQUFpQyxNQUFqQztBQUNBLGtCQUNHLElBREgsQ0FDUSxlQURSLEVBQ3lCLE1BRHpCO0FBR0EscUJBQVcsTUFBWDtBQUNELFNBdkNNOztBQXlDUCxpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLE9BQVAsQ0FBZSw0QkFBZjtBQUNBLGNBQUcsUUFBSCxFQUFhO0FBQ1gscUJBQVMsVUFBVDtBQUNEO0FBQ0QsY0FBRyxlQUFILEVBQW9CO0FBQ2xCLDRCQUFnQixVQUFoQjtBQUNEO0FBQ0Qsa0JBQ0csR0FESCxDQUNPLFNBQVcsY0FEbEIsRUFDa0MsT0FBTyxLQUFQLENBQWEsSUFEL0MsRUFFRyxHQUZILENBRU8sV0FBVyxjQUZsQixFQUVrQyxPQUFPLEtBQVAsQ0FBYSxNQUYvQztBQUlBLG1CQUNHLEdBREgsQ0FDTyxXQUFpQixjQUR4QixFQUN3QyxPQUFPLEtBQVAsQ0FBYSxNQURyRCxFQUVHLEdBRkgsQ0FFTyxpQkFBaUIsY0FGeEIsRUFFd0MsT0FBTyxLQUFQLENBQWEsWUFGckQ7QUFJQSxjQUFHLFNBQVMsSUFBVCxJQUFpQixPQUFwQixFQUE2QjtBQUMzQixtQkFBTyxVQUFQO0FBQ0EsbUJBQU8sTUFBUCxDQUFjLFdBQWQ7QUFDRDtBQUNELGtCQUNHLEdBREgsQ0FDTyxjQURQLEVBRUcsVUFGSCxDQUVjLGVBRmQ7QUFJRCxTQWpFTTs7QUFtRVAsd0JBQWdCLFlBQVc7QUFDekIsY0FBRyxzQkFBc0IsTUFBekIsRUFBaUM7QUFDL0IsOEJBQWtCLElBQUksZ0JBQUosQ0FBcUIsT0FBTyxLQUFQLENBQWEsY0FBbEMsQ0FBbEI7QUFDQSx1QkFBa0IsSUFBSSxnQkFBSixDQUFxQixPQUFPLEtBQVAsQ0FBYSxPQUFsQyxDQUFsQjtBQUNBLDRCQUFnQixPQUFoQixDQUF3QixRQUF4QixFQUFrQztBQUNoQyx5QkFBWSxJQURvQjtBQUVoQyx1QkFBWTtBQUZvQixhQUFsQztBQUlBLHFCQUFTLE9BQVQsQ0FBaUIsT0FBakIsRUFBMEI7QUFDeEIseUJBQVksSUFEWTtBQUV4Qix1QkFBWTtBQUZZLGFBQTFCO0FBSUEsbUJBQU8sS0FBUCxDQUFhLDhCQUFiLEVBQTZDLFFBQTdDO0FBQ0Q7QUFDRixTQWpGTTs7QUFtRlAsY0FBTTtBQUNKLGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sT0FBUCxDQUFlLGdEQUFmO0FBQ0EsZ0JBQUcsU0FBUyxhQUFaLEVBQTJCO0FBQ3pCLHNCQUNHLEVBREgsQ0FDTSxTQUFXLGNBRGpCLEVBQ2lDLE9BQU8sS0FBUCxDQUFhLElBRDlDO0FBR0Q7QUFDRCxvQkFDRyxFQURILENBQ00sV0FBVyxjQURqQixFQUNpQyxPQUFPLEtBQVAsQ0FBYSxNQUQ5Qzs7QUFJQSxxQkFDRyxHQURILENBQ08sV0FBZ0IsY0FEdkIsRUFFRyxFQUZILENBRU0sV0FBaUIsY0FGdkIsRUFFdUMsT0FBTyxLQUFQLENBQWEsTUFGcEQsRUFHRyxFQUhILENBR00saUJBQWlCLGNBSHZCLEVBR3VDLE9BQU8sS0FBUCxDQUFhLFlBSHBEO0FBS0Q7QUFqQkcsU0FuRkM7O0FBdUdQLGVBQU87QUFDTCxtQkFBUyxVQUFTLFNBQVQsRUFBb0I7QUFDM0IsbUJBQU8sT0FBUCxDQUFlLHFEQUFmO0FBQ0EsbUJBQU8sS0FBUCxHQUFlLFdBQVcsWUFBVztBQUNuQyxxQkFBTyxPQUFQLENBQWUseUNBQWY7QUFDQSxxQkFBTyxPQUFQO0FBQ0QsYUFIYyxFQUdaLEdBSFksQ0FBZjtBQUlELFdBUEk7QUFRTCwwQkFBZ0IsVUFBUyxTQUFULEVBQW9CO0FBQ2xDLGVBQUcsT0FBSCxDQUFXLElBQVgsQ0FBZ0IsU0FBaEIsRUFBMkIsVUFBUyxRQUFULEVBQW1CO0FBQzVDLGtCQUFHLFNBQVMsWUFBWixFQUEwQjtBQUN4QixtQkFBRyxPQUFILENBQVcsSUFBWCxDQUFnQixTQUFTLFlBQXpCLEVBQXVDLFVBQVMsSUFBVCxFQUFlO0FBQ3BELHNCQUFHLFFBQVEsT0FBUixJQUFtQixFQUFFLElBQUYsRUFBUSxJQUFSLENBQWEsT0FBYixFQUFzQixNQUF0QixHQUErQixDQUFyRCxFQUF3RDtBQUN0RCwyQkFBTyxLQUFQLENBQWEsK0NBQWI7QUFDQSwyQkFBTyxPQUFQO0FBQ0Q7QUFDRixpQkFMRDtBQU1EO0FBQ0YsYUFURDtBQVVELFdBbkJJO0FBb0JMLGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sS0FBUCxDQUFhLGdCQUFiO0FBQ0EsZ0JBQUcsU0FBUyxlQUFaLEVBQTZCO0FBQzNCLG9DQUFzQixPQUFPLE9BQTdCO0FBQ0Q7QUFDRixXQXpCSTtBQTBCTCxnQkFBTSxZQUFXO0FBQ2YsbUJBQU8sS0FBUCxDQUFhLHVCQUFiO0FBQ0Esa0NBQXNCLE9BQU8sT0FBN0I7QUFDRCxXQTdCSTs7QUErQkwsa0JBQVEsWUFBVztBQUNqQixnQkFBRyxTQUFTLFFBQVosRUFBc0I7QUFDcEIsMkJBQWEsT0FBTyxLQUFwQjtBQUNBLHFCQUFPLEtBQVAsR0FBZSxXQUFXLFlBQVc7QUFDbkMseUJBQVMsY0FBVCxDQUF3QixpQkFBaUIsY0FBekMsRUFBeUQsQ0FBRSxTQUFTLFNBQVQsRUFBRixDQUF6RDtBQUNELGVBRmMsRUFFWixTQUFTLFFBRkcsQ0FBZjtBQUdELGFBTEQsTUFNSztBQUNILG9DQUFzQixZQUFXO0FBQy9CLHlCQUFTLGNBQVQsQ0FBd0IsaUJBQWlCLGNBQXpDLEVBQXlELENBQUUsU0FBUyxTQUFULEVBQUYsQ0FBekQ7QUFDRCxlQUZEO0FBR0Q7QUFDRixXQTNDSTs7QUE2Q0wsd0JBQWMsVUFBUyxLQUFULEVBQWdCLGNBQWhCLEVBQWdDO0FBQzVDLG1CQUFPLGVBQVAsQ0FBdUIsY0FBdkI7QUFDRDtBQS9DSSxTQXZHQTs7QUF5SlAsa0JBQVUsVUFBUyxNQUFULEVBQWlCLFFBQWpCLEVBQTJCO0FBQ25DLGNBQUksRUFBRSxrQkFBa0IsS0FBcEIsQ0FBSixFQUFnQztBQUM5QixxQkFBUyxDQUFDLE1BQUQsQ0FBVDtBQUNEO0FBQ0QsY0FDRSxlQUFnQixPQUFPLE1BRHpCO0FBQUEsY0FFRSxnQkFBZ0IsQ0FGbEI7QUFBQSxjQUdFLFFBQWdCLEVBSGxCO0FBQUEsY0FJRSxhQUFnQixTQUFTLGFBQVQsQ0FBdUIsS0FBdkIsQ0FKbEI7QUFBQSxjQUtFLGFBQWdCLFlBQVc7QUFDekI7QUFDQSxnQkFBSSxpQkFBaUIsT0FBTyxNQUE1QixFQUFvQztBQUNsQyxrQkFBSSxFQUFFLFVBQUYsQ0FBYSxRQUFiLENBQUosRUFBNEI7QUFDMUI7QUFDRDtBQUNGO0FBQ0YsV0FaSDtBQWNBLGlCQUFPLGNBQVAsRUFBdUI7QUFDckIseUJBQXFCLFNBQVMsYUFBVCxDQUF1QixLQUF2QixDQUFyQjtBQUNBLHVCQUFXLE1BQVgsR0FBcUIsVUFBckI7QUFDQSx1QkFBVyxPQUFYLEdBQXFCLFVBQXJCO0FBQ0EsdUJBQVcsR0FBWCxHQUFxQixPQUFPLFlBQVAsQ0FBckI7QUFDQSxrQkFBTSxJQUFOLENBQVcsVUFBWDtBQUNEO0FBQ0YsU0FsTE07O0FBb0xQLHlCQUFpQixZQUFXO0FBQzFCLGlCQUFPLEtBQVAsQ0FBYSw2QkFBYjtBQUNBLHFCQUFXLEtBQVg7QUFDRCxTQXZMTTs7QUF5TFAsMEJBQWtCLFlBQVc7QUFDM0IsaUJBQU8sS0FBUCxDQUFhLHFDQUFiO0FBQ0EscUJBQVcsSUFBWDtBQUNELFNBNUxNOztBQThMUCxnQkFBUTtBQUNOLHdCQUFjLFlBQVc7QUFDdkIsZ0JBQUcsYUFBSCxFQUFrQjtBQUNoQixxQkFBTyxLQUFQLENBQWEsd0NBQWI7QUFDQSxxQkFBTyxLQUFQO0FBQ0Q7QUFDRCxtQkFBTyxLQUFQLENBQWEsMEJBQWI7QUFDQSxtQkFBTyxJQUFQO0FBQ0Q7QUFSSyxTQTlMRDs7QUF5TVAsZUFBTztBQUNMLGlCQUFPLFlBQVc7QUFDaEIsbUJBQU8sS0FBUCxHQUFlO0FBQ2Isd0JBQVcsRUFERTtBQUViLHNCQUFXLEVBRkU7QUFHYix1QkFBVztBQUhFLGFBQWY7QUFLRCxXQVBJO0FBUUwsaUJBQU8sWUFBVztBQUNoQixnQkFDRSxNQUFNLFFBQVEsSUFBUixDQUFhLFNBQVMsR0FBdEIsQ0FEUjtBQUdBLGdCQUFHLEdBQUgsRUFBUTtBQUNOLHFCQUFPLE9BQVAsQ0FBZSxvQkFBZixFQUFxQyxHQUFyQztBQUNBLHVCQUFTLElBQVQsR0FBMEIsSUFBMUI7QUFDQSx1QkFBUyxjQUFULEdBQTBCLEtBQTFCOzs7QUFHQSx1QkFBUyxVQUFULEdBQXNCLFlBQVc7QUFDL0IsdUJBQU8sS0FBUCxDQUFhLGlCQUFiLEVBQWdDLE9BQWhDO0FBQ0EsdUJBQU8sUUFBUCxDQUFnQixHQUFoQixFQUFxQixZQUFXO0FBQzlCLHlCQUFPLEdBQVAsQ0FBVyxLQUFYLENBQWlCLEdBQWpCLEVBQXNCLFlBQVc7QUFDL0I7QUFDQSx3QkFBRyxlQUFlLFdBQWxCLEVBQStCO0FBQzdCLCtCQUFTLFdBQVQsQ0FBcUIsSUFBckIsQ0FBMEIsSUFBMUI7QUFDRDtBQUNELDZCQUFTLE1BQVQsQ0FBZ0IsSUFBaEIsQ0FBcUIsSUFBckI7QUFDRCxtQkFORDtBQU9ELGlCQVJEO0FBU0QsZUFYRDtBQVlEO0FBQ0YsV0EvQkk7QUFnQ0wsaUJBQU8sWUFBVztBQUNoQixtQkFBTyxLQUFQLENBQWEsa0JBQWI7QUFDQSxxQkFBUyxJQUFULEdBQTBCLEtBQTFCO0FBQ0EscUJBQVMsY0FBVCxHQUEwQixLQUExQjtBQUNBLHFCQUFTLFlBQVQsR0FBMEIsSUFBMUI7QUFDQSxxQkFBUyxhQUFULEdBQTBCLElBQTFCO0FBQ0EsZ0JBQUcsQ0FBQyxXQUFXLFVBQWYsRUFBMkI7QUFDekIsdUJBQVMsVUFBVCxHQUFzQixLQUF0QjtBQUNEO0FBQ0QsbUJBQU8sTUFBUCxDQUFjLFdBQWQ7QUFDQSxtQkFBTyxLQUFQLENBQWEsbUJBQWIsRUFBa0MsWUFBbEM7QUFDQSxxQkFBUyxXQUFULEdBQXVCLFlBQVc7QUFDaEMscUJBQU8sS0FBUCxDQUFhLHVDQUFiLEVBQXNELE9BQXREO0FBQ0EscUJBQU8sSUFBUCxDQUFZLFdBQVo7QUFDQSxxQkFBTyxHQUFQLENBQVcsS0FBWDtBQUNBLGtCQUFHLFNBQVMsVUFBWixFQUF3QjtBQUN0QixvQkFBRyxFQUFFLEVBQUYsQ0FBSyxVQUFMLEtBQW9CLFNBQXZCLEVBQWtDO0FBQ2hDLDBCQUFRLFVBQVIsQ0FBbUIsU0FBUyxVQUE1QixFQUF3QyxTQUFTLFFBQWpEO0FBQ0Q7QUFDRjtBQUNGLGFBVEQ7QUFVQSxxQkFBUyxrQkFBVCxHQUE4QixZQUFXO0FBQ3ZDLHFCQUFPLEtBQVAsQ0FBYSw4Q0FBYixFQUE2RCxPQUE3RDtBQUNBLHFCQUFPLElBQVAsQ0FBWSxXQUFaO0FBQ0EscUJBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDRCxhQUpEO0FBS0Q7QUExREksU0F6TUE7O0FBc1FQLGdCQUFRO0FBQ04sdUJBQWEsWUFBVztBQUN0QixtQkFBTyxPQUFQLENBQWUscUNBQWY7QUFDQSwyQkFBZSxRQUNaLEtBRFksQ0FDTixLQURNLEVBRVosR0FGWSxDQUVSLFNBRlEsRUFFRyxNQUZILEVBR1osUUFIWSxDQUdILFVBQVUsV0FIUCxFQUlaLFdBSlksQ0FJQSxPQUpBLENBQWY7QUFNRDtBQVRLLFNBdFFEOztBQWtSUCxjQUFNO0FBQ0osdUJBQWEsWUFBVztBQUN0QixtQkFBTyxPQUFQLENBQWUscUJBQWY7QUFDQSx5QkFDRyxHQURILENBQ08sU0FEUCxFQUNrQixPQURsQixFQUVHLEdBRkgsQ0FFTyxZQUZQLEVBRXFCLFFBRnJCO0FBSUQ7QUFQRyxTQWxSQztBQTJSUCxjQUFNO0FBQ0osdUJBQWEsWUFBVztBQUN0QixtQkFBTyxPQUFQLENBQWUsb0JBQWY7QUFDQSx5QkFDRyxHQURILENBQ08sU0FEUCxFQUNrQixNQURsQixFQUVHLEdBRkgsQ0FFTyxZQUZQLEVBRXFCLEVBRnJCO0FBSUQ7QUFQRyxTQTNSQzs7QUFxU1AsYUFBSztBQUNILGlCQUFPLFlBQVc7QUFDaEIsbUJBQU8sT0FBUCxDQUFlLG1DQUFmO0FBQ0Esb0JBQ0csUUFESCxDQUNZLFVBQVUsS0FEdEIsRUFFRyxHQUZILENBRU87QUFDSCx3QkFBVyxPQURSO0FBRUgsbUJBQVcsU0FBUyxNQUFULEdBQWtCLElBRjFCO0FBR0gsb0JBQVcsTUFIUjtBQUlILHNCQUFXLFNBQVM7QUFKakIsYUFGUDtBQVNBLHFCQUFTLE9BQVQsQ0FBaUIsSUFBakIsQ0FBc0IsT0FBdEI7QUFDRCxXQWJFO0FBY0gsaUJBQU8sVUFBUyxHQUFULEVBQWMsUUFBZCxFQUF3QjtBQUM3QixvQkFDRyxJQURILENBQ1EsS0FEUixFQUNlLEdBRGY7QUFHQSxnQkFBRyxTQUFTLFVBQVosRUFBd0I7QUFDdEIsa0JBQUksRUFBRSxFQUFGLENBQUssVUFBTCxLQUFvQixTQUF4QixFQUFvQztBQUNsQyx3QkFBUSxVQUFSLENBQW1CLFNBQVMsVUFBNUIsRUFBd0MsU0FBUyxRQUFqRCxFQUEyRCxRQUEzRDtBQUNELGVBRkQsTUFHSztBQUNILHdCQUFRLE1BQVIsQ0FBZSxTQUFTLFFBQXhCLEVBQWtDLFFBQWxDO0FBQ0Q7QUFDRixhQVBELE1BUUs7QUFDSCxzQkFBUSxJQUFSO0FBQ0Q7QUFDRjtBQTdCRSxTQXJTRTs7QUFxVVAsWUFBSTtBQUNGLG9CQUFVLFlBQVc7QUFDbkIsZ0JBQ0UsZUFBaUIsT0FBTyxHQUFQLENBQVcsbUJBQVgsRUFEbkI7QUFHQSxtQkFBTyxhQUFhLFFBQXBCO0FBQ0QsV0FOQztBQU9GLHFCQUFXLFlBQVc7QUFDcEIsZ0JBQ0UsZUFBaUIsT0FBTyxHQUFQLENBQVcsbUJBQVgsRUFEbkI7QUFHQSxtQkFBTyxhQUFhLFNBQXBCO0FBQ0QsV0FaQztBQWFGLG1CQUFTLFlBQVc7QUFDbEIsZ0JBQUcsT0FBTyxLQUFQLElBQWdCLE9BQU8sS0FBUCxDQUFhLE9BQWhDLEVBQXlDO0FBQ3ZDLHFCQUFPLEVBQUUsT0FBTyxLQUFQLENBQWEsT0FBYixDQUFxQixLQUFyQixLQUErQixDQUEvQixJQUFvQyxPQUFPLEtBQVAsQ0FBYSxPQUFiLENBQXFCLE1BQXJCLENBQTRCLEdBQTVCLEtBQW9DLENBQTFFLENBQVA7QUFDRDtBQUNELG1CQUFPLEtBQVA7QUFDRDtBQWxCQyxTQXJVRzs7QUEwVlAsaUJBQVMsWUFBVztBQUNsQixpQkFBTyxLQUFQLENBQWEscUNBQWI7QUFDQSxjQUFHLFNBQVMsSUFBVCxJQUFpQixPQUFwQixFQUE2QjtBQUMzQixtQkFBTyxVQUFQO0FBQ0Q7QUFDRCxpQkFBTyxLQUFQO0FBQ0EsaUJBQU8sSUFBUCxDQUFZLFFBQVo7QUFDQSxjQUFHLFNBQVMsY0FBWixFQUE0QjtBQUMxQixtQkFBTyxlQUFQO0FBQ0Q7QUFDRCxtQkFBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLE9BQXhCO0FBQ0QsU0FyV007O0FBdVdQLG9CQUFZLFlBQVk7QUFDdEIsaUJBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDQSxpQkFBTyxNQUFQLENBQWMsUUFBZDtBQUNELFNBMVdNOztBQTRXUCxlQUFPLFlBQVc7QUFDaEIsaUJBQU8sT0FBUCxDQUFlLDZCQUFmO0FBQ0EsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsT0FBTyxLQUF2QixDQUFKLEVBQW9DO0FBQ2xDLG1CQUFPLEtBQVAsQ0FBYSxNQUFiLEdBQXNCLEVBQXRCO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLE9BQWIsR0FBdUIsRUFBdkI7QUFDRDtBQUNGLFNBbFhNOztBQW9YUCx5QkFBaUIsVUFBUyxNQUFULEVBQWlCO0FBQ2hDLGlCQUFPLE9BQVAsQ0FBZSxnQ0FBZixFQUFpRCxPQUFPLEtBQVAsQ0FBYSxPQUE5RDs7QUFFQSxjQUFJLENBQUMsUUFBRCxJQUFhLE9BQU8sRUFBUCxDQUFVLE9BQVYsRUFBakIsRUFBdUM7OztBQUdyQyxtQkFBTyxJQUFQLENBQVksTUFBWixDQUFtQixNQUFuQjs7O0FBR0EsbUJBQU8sSUFBUCxDQUFZLFlBQVo7OztBQUdBLG1CQUFPLE1BQVA7OztBQUdBLG1CQUFPLGNBQVA7QUFDQSxtQkFBTyxpQkFBUDtBQUNBLG1CQUFPLG9CQUFQO0FBQ0EsbUJBQU8sZ0JBQVA7QUFDQSxtQkFBTyxtQkFBUDs7O0FBR0EsbUJBQU8sUUFBUDtBQUNBLG1CQUFPLFNBQVA7QUFDQSxtQkFBTyxPQUFQO0FBQ0EsbUJBQU8sVUFBUDtBQUNBLG1CQUFPLGFBQVA7QUFDQSxtQkFBTyxTQUFQO0FBQ0EsbUJBQU8sWUFBUDs7O0FBR0EsZ0JBQUcsU0FBUyxRQUFaLEVBQXNCO0FBQ3BCLHVCQUFTLFFBQVQsQ0FBa0IsSUFBbEIsQ0FBdUIsT0FBdkIsRUFBZ0MsT0FBTyxHQUFQLENBQVcsbUJBQVgsRUFBaEM7QUFDRDtBQUNGO0FBQ0YsU0F2Wk07O0FBeVpQLGdCQUFRLFVBQVMsTUFBVCxFQUFpQixXQUFqQixFQUE4QjtBQUNwQyxjQUNFLGVBQWlCLE9BQU8sR0FBUCxDQUFXLG1CQUFYLEVBRG5CO0FBQUEsY0FFRSxjQUZGOztBQUtBLGNBQUcsVUFBVSxXQUFiLEVBQTBCO0FBQ3hCLHFCQUFTLFFBQVQsQ0FBa0IsTUFBbEIsSUFBNEIsV0FBNUI7QUFDRCxXQUZELE1BR0ssSUFBRyxXQUFXLFNBQWQsRUFBeUI7QUFDNUIsbUJBQVEsT0FBTyxHQUFQLENBQVcsWUFBWCxDQUF3QixNQUF4QixJQUFrQyxhQUFhLFlBQXZEO0FBQ0QsV0FGSSxNQUdBLElBQUcsYUFBYSxPQUFoQixFQUF5QjtBQUM1QixjQUFFLElBQUYsQ0FBTyxTQUFTLFFBQWhCLEVBQTBCLFVBQVMsTUFBVCxFQUFpQixRQUFqQixFQUEyQjtBQUNuRCxrQkFBRyxhQUFhLGFBQWIsSUFBOEIsYUFBYSxZQUFiLEdBQTRCLE9BQU8sR0FBUCxDQUFXLFlBQVgsQ0FBd0IsTUFBeEIsQ0FBN0QsRUFBOEY7QUFDNUYsdUJBQU8sT0FBUCxDQUFlLFFBQWYsRUFBeUIsTUFBekI7QUFDRCxlQUZELE1BR0ssSUFBRyxDQUFDLFNBQVMsSUFBYixFQUFtQjtBQUN0Qix1QkFBTyxNQUFQLENBQWMsUUFBZCxDQUF1QixRQUF2QjtBQUNEO0FBQ0YsYUFQRDtBQVFEO0FBQ0YsU0EvYU07O0FBaWJQLGtCQUFVLFVBQVMsV0FBVCxFQUFzQjtBQUM5QixjQUNFLGVBQWUsT0FBTyxHQUFQLENBQVcsbUJBQVgsRUFEakI7QUFBQSxjQUVFLFdBQWUsZUFBZSxTQUFTLFVBRnpDO0FBQUEsY0FHRSxlQUFlLFVBSGpCO0FBS0EsY0FBRyxXQUFILEVBQWdCO0FBQ2QsbUJBQU8sS0FBUCxDQUFhLDhCQUFiLEVBQTZDLFdBQTdDO0FBQ0EscUJBQVMsVUFBVCxHQUFzQixXQUF0QjtBQUNEO0FBQ0QsY0FBRyxhQUFhLFFBQWhCLEVBQTBCO0FBQ3hCLG1CQUFPLE9BQVAsQ0FBZSxRQUFmLEVBQXlCLFlBQXpCO0FBQ0QsV0FGRCxNQUdLLElBQUcsQ0FBQyxTQUFTLElBQWIsRUFBbUI7QUFDdEIsbUJBQU8sTUFBUCxDQUFjLFFBQWQsQ0FBdUIsWUFBdkI7QUFDRDtBQUNELGNBQUcsZ0JBQWdCLFNBQW5CLEVBQThCO0FBQzVCLG1CQUFPLGFBQWEsVUFBcEI7QUFDRDtBQUNGLFNBcGNNOztBQXNjUCxtQkFBVyxVQUFTLFdBQVQsRUFBc0I7QUFDL0IsY0FDRSxlQUFlLE9BQU8sR0FBUCxDQUFXLG1CQUFYLEVBRGpCO0FBQUEsY0FFRSxXQUFlLGVBQWUsU0FBUyxXQUZ6QztBQUFBLGNBR0UsZUFBZSxXQUhqQjtBQUtBLGNBQUcsV0FBSCxFQUFnQjtBQUNkLG1CQUFPLEtBQVAsQ0FBYSwrQkFBYixFQUE4QyxXQUE5QztBQUNBLHFCQUFTLFdBQVQsR0FBdUIsV0FBdkI7QUFDRDtBQUNELGNBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUN6QixtQkFBTyxPQUFQLENBQWUsUUFBZixFQUF5QixZQUF6QjtBQUNELFdBRkQsTUFHSyxJQUFHLENBQUMsU0FBUyxJQUFiLEVBQW1CO0FBQ3RCLG1CQUFPLE1BQVAsQ0FBYyxRQUFkLENBQXVCLFlBQXZCO0FBQ0Q7QUFDRCxjQUFHLGdCQUFnQixTQUFuQixFQUE4QjtBQUM1QixtQkFBTyxhQUFhLFdBQXBCO0FBQ0Q7QUFDRixTQXpkTTs7QUEyZFAsaUJBQVMsVUFBUyxXQUFULEVBQXNCO0FBQzdCLGNBQ0UsZUFBZSxPQUFPLEdBQVAsQ0FBVyxtQkFBWCxFQURqQjtBQUFBLGNBRUUsV0FBZSxlQUFlLFNBQVMsU0FGekM7QUFBQSxjQUdFLGVBQWUsU0FIakI7QUFLQSxjQUFHLFdBQUgsRUFBZ0I7QUFDZCxtQkFBTyxLQUFQLENBQWEsNkJBQWIsRUFBNEMsV0FBNUM7QUFDQSxxQkFBUyxTQUFULEdBQXFCLFdBQXJCO0FBQ0Q7QUFDRCxjQUFHLGFBQWEsT0FBaEIsRUFBeUI7QUFDdkIsbUJBQU8sT0FBUCxDQUFlLFFBQWYsRUFBeUIsWUFBekI7QUFDRCxXQUZELE1BR0ssSUFBRyxDQUFDLFNBQVMsSUFBYixFQUFtQjtBQUN0QixtQkFBTyxNQUFQLENBQWMsUUFBZCxDQUF1QixZQUF2QjtBQUNEO0FBQ0QsY0FBRyxnQkFBZ0IsU0FBbkIsRUFBOEI7QUFDNUIsbUJBQU8sYUFBYSxPQUFwQjtBQUNEO0FBQ0YsU0E5ZU07O0FBaWZQLG9CQUFZLFVBQVMsV0FBVCxFQUFzQjtBQUNoQyxjQUNFLGVBQWUsT0FBTyxHQUFQLENBQVcsbUJBQVgsRUFEakI7QUFBQSxjQUVFLFdBQWUsZUFBZSxTQUFTLFlBRnpDO0FBQUEsY0FHRSxlQUFlLFlBSGpCO0FBS0EsY0FBRyxXQUFILEVBQWdCO0FBQ2QsbUJBQU8sS0FBUCxDQUFhLGlDQUFiLEVBQWdELFdBQWhEO0FBQ0EscUJBQVMsWUFBVCxHQUF3QixXQUF4QjtBQUNEO0FBQ0QsY0FBRyxhQUFhLFVBQWhCLEVBQTRCO0FBQzFCLG1CQUFPLE9BQVAsQ0FBZSxRQUFmLEVBQXlCLFlBQXpCO0FBQ0QsV0FGRCxNQUdLLElBQUcsQ0FBQyxTQUFTLElBQWIsRUFBbUI7QUFDdEIsbUJBQU8sTUFBUCxDQUFjLFFBQWQsQ0FBdUIsWUFBdkI7QUFDRDtBQUNELGNBQUcsZ0JBQWdCLFNBQW5CLEVBQThCO0FBQzVCLG1CQUFPLGFBQWEsVUFBcEI7QUFDRDtBQUNGLFNBcGdCTTs7QUFzZ0JQLHVCQUFlLFVBQVMsV0FBVCxFQUFzQjtBQUNuQyxjQUNFLGVBQWUsT0FBTyxHQUFQLENBQVcsbUJBQVgsRUFEakI7QUFBQSxjQUVFLFdBQWUsZUFBZSxTQUFTLGVBRnpDO0FBQUEsY0FHRSxlQUFlLGVBSGpCO0FBS0EsY0FBRyxXQUFILEVBQWdCO0FBQ2QsbUJBQU8sS0FBUCxDQUFhLG9DQUFiLEVBQW1ELFdBQW5EO0FBQ0EscUJBQVMsZUFBVCxHQUEyQixXQUEzQjtBQUNEO0FBQ0QsY0FBRyxhQUFhLGFBQWhCLEVBQStCO0FBQzdCLG1CQUFPLE9BQVAsQ0FBZSxRQUFmLEVBQXlCLFlBQXpCO0FBQ0QsV0FGRCxNQUdLLElBQUcsQ0FBQyxTQUFTLElBQWIsRUFBbUI7QUFDdEIsbUJBQU8sTUFBUCxDQUFjLFFBQWQsQ0FBdUIsWUFBdkI7QUFDRDtBQUNELGNBQUcsZ0JBQWdCLFNBQW5CLEVBQThCO0FBQzVCLG1CQUFPLGFBQWEsYUFBcEI7QUFDRDtBQUNGLFNBemhCTTs7QUEyaEJQLG1CQUFXLFVBQVMsV0FBVCxFQUFzQjtBQUMvQixjQUNFLGVBQWUsT0FBTyxHQUFQLENBQVcsbUJBQVgsRUFEakI7QUFBQSxjQUVFLFdBQWUsZUFBZSxTQUFTLFdBRnpDO0FBQUEsY0FHRSxlQUFlLFdBSGpCO0FBS0EsY0FBRyxXQUFILEVBQWdCO0FBQ2QsbUJBQU8sS0FBUCxDQUFhLGdDQUFiLEVBQStDLFdBQS9DO0FBQ0EscUJBQVMsV0FBVCxHQUF1QixXQUF2QjtBQUNEO0FBQ0QsY0FBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLG1CQUFPLE9BQVAsQ0FBZSxRQUFmLEVBQXlCLFlBQXpCO0FBQ0QsV0FGRCxNQUdLLElBQUcsQ0FBQyxTQUFTLElBQWIsRUFBbUI7QUFDdEIsbUJBQU8sTUFBUCxDQUFjLFFBQWQsQ0FBdUIsWUFBdkI7QUFDRDtBQUNELGNBQUcsZ0JBQWdCLFNBQW5CLEVBQThCO0FBQzVCLG1CQUFPLGFBQWEsU0FBcEI7QUFDRDtBQUNGLFNBOWlCTTs7QUFnakJQLHNCQUFjLFVBQVMsV0FBVCxFQUFzQjtBQUNsQyxjQUNFLGVBQWUsT0FBTyxHQUFQLENBQVcsbUJBQVgsRUFEakI7QUFBQSxjQUVFLFdBQWUsZUFBZSxTQUFTLGNBRnpDO0FBQUEsY0FHRSxlQUFlLGNBSGpCO0FBS0EsY0FBRyxXQUFILEVBQWdCO0FBQ2QsbUJBQU8sS0FBUCxDQUFhLG1DQUFiLEVBQWtELFdBQWxEO0FBQ0EscUJBQVMsY0FBVCxHQUEwQixXQUExQjtBQUNEO0FBQ0QsY0FBRyxhQUFhLFlBQWhCLEVBQThCO0FBQzVCLG1CQUFPLE9BQVAsQ0FBZSxRQUFmLEVBQXlCLFlBQXpCO0FBQ0QsV0FGRCxNQUdLLElBQUcsQ0FBQyxTQUFTLElBQWIsRUFBbUI7QUFDdEIsbUJBQU8sTUFBUCxDQUFjLFFBQWQsQ0FBdUIsWUFBdkI7QUFDRDtBQUNELGNBQUcsZ0JBQWdCLFNBQW5CLEVBQThCO0FBQzVCLG1CQUFPLGFBQWEsWUFBcEI7QUFDRDtBQUNGLFNBbmtCTTs7QUFxa0JQLHdCQUFnQixVQUFTLFdBQVQsRUFBc0I7QUFDcEMsY0FDRSxlQUFlLE9BQU8sR0FBUCxDQUFXLG1CQUFYLEVBRGpCO0FBQUEsY0FFRSxXQUFlLGVBQWUsU0FBUyxnQkFGekM7QUFBQSxjQUdFLGVBQWUsZ0JBSGpCO0FBS0EsY0FBRyxXQUFILEVBQWdCO0FBQ2QsbUJBQU8sS0FBUCxDQUFhLHFDQUFiLEVBQW9ELFdBQXBEO0FBQ0EscUJBQVMsZ0JBQVQsR0FBNEIsV0FBNUI7QUFDRDtBQUNELGNBQUcsQ0FBQyxhQUFhLE9BQWpCLEVBQTBCO0FBQ3hCLGdCQUFHLE9BQU8sR0FBUCxDQUFXLFFBQVgsQ0FBb0IsU0FBcEIsQ0FBSCxFQUFtQztBQUNqQyxxQkFBTyxPQUFQLENBQWUsUUFBZixFQUF5QixZQUF6QjtBQUNEO0FBQ0YsV0FKRCxNQUtLLElBQUcsQ0FBQyxTQUFTLElBQWIsRUFBbUI7QUFDdEIsbUJBQU8sTUFBUCxDQUFjLFFBQWQsQ0FBdUIsWUFBdkI7QUFDRDtBQUNELGNBQUcsZ0JBQWdCLFNBQW5CLEVBQThCO0FBQzVCLG1CQUFPLENBQUMsYUFBYSxPQUFyQjtBQUNEO0FBQ0YsU0ExbEJNOztBQTZsQlAsMkJBQW1CLFVBQVMsV0FBVCxFQUFzQjtBQUN2QyxjQUNFLGVBQWUsT0FBTyxHQUFQLENBQVcsbUJBQVgsRUFEakI7QUFBQSxjQUVFLFdBQWUsZUFBZSxTQUFTLG1CQUZ6QztBQUFBLGNBR0UsZUFBZSxtQkFIakI7QUFLQSxjQUFHLFdBQUgsRUFBZ0I7QUFDZCxtQkFBTyxLQUFQLENBQWEseUNBQWIsRUFBd0QsV0FBeEQ7QUFDQSxxQkFBUyxtQkFBVCxHQUErQixXQUEvQjtBQUNEO0FBQ0QsY0FBRyxDQUFDLGFBQWEsVUFBakIsRUFBNkI7QUFDM0IsZ0JBQUcsT0FBTyxHQUFQLENBQVcsUUFBWCxDQUFvQixZQUFwQixDQUFILEVBQXNDO0FBQ3BDLHFCQUFPLE9BQVAsQ0FBZSxRQUFmLEVBQXlCLFlBQXpCO0FBQ0Q7QUFDRixXQUpELE1BS0ssSUFBRyxDQUFDLFNBQVMsSUFBYixFQUFtQjtBQUN0QixtQkFBTyxNQUFQLENBQWMsUUFBZCxDQUF1QixZQUF2QjtBQUNEO0FBQ0QsY0FBRyxnQkFBZ0IsU0FBbkIsRUFBOEI7QUFDNUIsbUJBQU8sQ0FBQyxhQUFhLFVBQXJCO0FBQ0Q7QUFDRixTQWxuQk07O0FBb25CUCw4QkFBc0IsVUFBUyxXQUFULEVBQXNCO0FBQzFDLGNBQ0UsZUFBZSxPQUFPLEdBQVAsQ0FBVyxtQkFBWCxFQURqQjtBQUFBLGNBRUUsV0FBZSxlQUFlLFNBQVMsc0JBRnpDO0FBQUEsY0FHRSxlQUFlLHNCQUhqQjtBQUtBLGNBQUcsV0FBSCxFQUFnQjtBQUNkLG1CQUFPLEtBQVAsQ0FBYSw0Q0FBYixFQUEyRCxXQUEzRDtBQUNBLHFCQUFTLHNCQUFULEdBQWtDLFdBQWxDO0FBQ0Q7QUFDRCxjQUFHLENBQUMsYUFBYSxhQUFqQixFQUFnQztBQUM5QixnQkFBRyxPQUFPLEdBQVAsQ0FBVyxRQUFYLENBQW9CLGVBQXBCLENBQUgsRUFBeUM7QUFDdkMscUJBQU8sT0FBUCxDQUFlLFFBQWYsRUFBeUIsWUFBekI7QUFDRDtBQUNGLFdBSkQsTUFLSyxJQUFHLENBQUMsU0FBUyxJQUFiLEVBQW1CO0FBQ3RCLG1CQUFPLE1BQVAsQ0FBYyxRQUFkLENBQXVCLFlBQXZCO0FBQ0Q7QUFDRCxjQUFHLGdCQUFnQixTQUFuQixFQUE4QjtBQUM1QixtQkFBTyxDQUFDLGFBQWEsYUFBckI7QUFDRDtBQUNGLFNBem9CTTs7QUEyb0JQLDBCQUFrQixVQUFTLFdBQVQsRUFBc0I7QUFDdEMsY0FDRSxlQUFlLE9BQU8sR0FBUCxDQUFXLG1CQUFYLEVBRGpCO0FBQUEsY0FFRSxXQUFlLGVBQWUsU0FBUyxrQkFGekM7QUFBQSxjQUdFLGVBQWUsa0JBSGpCO0FBS0EsY0FBRyxXQUFILEVBQWdCO0FBQ2QsbUJBQU8sS0FBUCxDQUFhLHdDQUFiLEVBQXVELFdBQXZEO0FBQ0EscUJBQVMsa0JBQVQsR0FBOEIsV0FBOUI7QUFDRDtBQUNELGNBQUcsQ0FBQyxhQUFhLFNBQWpCLEVBQTRCO0FBQzFCLGdCQUFHLE9BQU8sR0FBUCxDQUFXLFFBQVgsQ0FBb0IsV0FBcEIsQ0FBSCxFQUFxQztBQUNuQyxxQkFBTyxPQUFQLENBQWUsUUFBZixFQUF5QixZQUF6QjtBQUNEO0FBQ0YsV0FKRCxNQUtLLElBQUcsQ0FBQyxTQUFTLElBQWIsRUFBbUI7QUFDdEIsbUJBQU8sTUFBUCxDQUFjLFFBQWQsQ0FBdUIsWUFBdkI7QUFDRDtBQUNELGNBQUcsZ0JBQWdCLFNBQW5CLEVBQThCO0FBQzVCLG1CQUFPLENBQUMsYUFBYSxXQUFyQjtBQUNEO0FBQ0YsU0FocUJNOztBQWtxQlAsNkJBQXFCLFVBQVMsV0FBVCxFQUFzQjtBQUN6QyxjQUNFLGVBQWUsT0FBTyxHQUFQLENBQVcsbUJBQVgsRUFEakI7QUFBQSxjQUVFLFdBQWUsZUFBZSxTQUFTLHFCQUZ6QztBQUFBLGNBR0UsZUFBZSxxQkFIakI7QUFLQSxjQUFHLFdBQUgsRUFBZ0I7QUFDZCxtQkFBTyxLQUFQLENBQWEsMkNBQWIsRUFBMEQsV0FBMUQ7QUFDQSxxQkFBUyxxQkFBVCxHQUFpQyxXQUFqQztBQUNEO0FBQ0QsY0FBRyxDQUFDLGFBQWEsWUFBakIsRUFBK0I7QUFDN0IsZ0JBQUcsT0FBTyxHQUFQLENBQVcsUUFBWCxDQUFvQixjQUFwQixDQUFILEVBQXdDO0FBQ3RDLHFCQUFPLE9BQVAsQ0FBZSxRQUFmLEVBQXlCLFlBQXpCO0FBQ0Q7QUFDRixXQUpELE1BS0ssSUFBRyxDQUFDLFNBQVMsSUFBYixFQUFtQjtBQUN0QixtQkFBTyxNQUFQLENBQWMsUUFBZCxDQUF1QixZQUF2QjtBQUNEO0FBQ0QsY0FBRyxnQkFBZ0IsU0FBbkIsRUFBOEI7QUFDNUIsbUJBQU8sQ0FBQyxhQUFhLFlBQXJCO0FBQ0Q7QUFDRixTQXZyQk07O0FBeXJCUCxpQkFBUyxVQUFTLFFBQVQsRUFBbUIsWUFBbkIsRUFBaUM7QUFDeEMsY0FDRSxlQUFlLE9BQU8sR0FBUCxDQUFXLG1CQUFYLEVBRGpCO0FBQUEsY0FFRSxTQUFlLE9BQU8sR0FBUCxDQUFXLGtCQUFYLEVBRmpCO0FBSUEscUJBQVcsWUFBWSxLQUF2QjtBQUNBLGNBQUcsUUFBSCxFQUFhO0FBQ1gsZ0JBQUcsU0FBUyxVQUFaLEVBQXdCO0FBQ3RCLHFCQUFPLEtBQVAsQ0FBYSxvQ0FBYixFQUFtRCxZQUFuRCxFQUFpRSxZQUFqRTtBQUNBLHVCQUFTLElBQVQsQ0FBYyxPQUFkLEVBQXVCLFlBQXZCLEVBQXFDLE1BQXJDO0FBQ0QsYUFIRCxNQUlLLElBQUcsQ0FBQyxPQUFPLEdBQVAsQ0FBVyxRQUFYLENBQW9CLFlBQXBCLENBQUosRUFBdUM7QUFDMUMscUJBQU8sS0FBUCxDQUFhLGdCQUFiLEVBQStCLFlBQS9CLEVBQTZDLFlBQTdDO0FBQ0EsdUJBQVMsSUFBVCxDQUFjLE9BQWQsRUFBdUIsWUFBdkIsRUFBcUMsTUFBckM7QUFDRDtBQUNGO0FBQ0QsaUJBQU8sSUFBUCxDQUFZLFFBQVosQ0FBcUIsWUFBckI7QUFDRCxTQTFzQk07O0FBNHNCUCxnQkFBUTtBQUNOLGlCQUFPLFlBQVc7QUFDaEIsbUJBQU8sS0FBUCxDQUFhLHlCQUFiO0FBQ0Esb0JBQ0csV0FESCxDQUNlLFVBQVUsS0FEekIsRUFFRyxHQUZILENBRU87QUFDSCx3QkFBVyxFQURSO0FBRUgsbUJBQVcsRUFGUjtBQUdILG9CQUFXLEVBSFI7QUFJSCxzQkFBVztBQUpSLGFBRlA7QUFTQSxxQkFBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLE9BQXhCO0FBQ0QsV0FiSztBQWNOLHVCQUFhLFlBQVc7QUFDdEIsbUJBQU8sS0FBUCxDQUFhLDhCQUFiO0FBQ0EsZ0JBQUcsWUFBSCxFQUFpQjtBQUNmLDJCQUFhLE1BQWI7QUFDRDtBQUNGLFdBbkJLO0FBb0JOLG9CQUFVLFVBQVMsUUFBVCxFQUFtQjtBQUMzQixnQkFBRyxRQUFILEVBQWE7QUFDWCxrQkFDRSxXQUFXLE9BQU8sS0FBUCxDQUFhLFFBRDFCO0FBR0Esa0JBQUcsU0FBUyxRQUFULE1BQXVCLFNBQXZCLElBQW9DLFNBQVMsUUFBVCxNQUF1QixJQUE5RCxFQUFvRTtBQUNsRSx1QkFBTyxLQUFQLENBQWEsa0NBQWIsRUFBaUQsUUFBakQ7QUFDQSx1QkFBTyxLQUFQLENBQWEsUUFBYixDQUFzQixRQUF0QixJQUFrQyxLQUFsQztBQUNEO0FBQ0YsYUFSRCxNQVNLO0FBQ0gscUJBQU8sS0FBUCxDQUFhLFFBQWIsR0FBd0IsRUFBeEI7QUFDRDtBQUNGO0FBakNLLFNBNXNCRDs7QUFndkJQLGNBQU07QUFDSix3QkFBYyxZQUFXO0FBQ3ZCLG1CQUFPLE9BQVAsQ0FBZSw0REFBZjtBQUNBLG1CQUFPLElBQVAsQ0FBWSxTQUFaO0FBQ0EsbUJBQU8sSUFBUCxDQUFZLGtCQUFaO0FBQ0EsbUJBQU8sSUFBUCxDQUFZLG1CQUFaO0FBQ0QsV0FORztBQU9KLG9CQUFVLFVBQVMsUUFBVCxFQUFtQjtBQUMzQixnQkFBRyxRQUFILEVBQWE7QUFDWCxrQkFBRyxPQUFPLEtBQVAsQ0FBYSxRQUFiLENBQXNCLFFBQXRCLE1BQW9DLFNBQXBDLElBQWtELE9BQU8sS0FBUCxDQUFhLFFBQWIsQ0FBc0IsUUFBdEIsTUFBb0MsSUFBekYsRUFBZ0c7QUFDOUYsdUJBQU8sT0FBUCxDQUFlLDBCQUFmLEVBQTJDLFFBQTNDO0FBQ0EsdUJBQU8sS0FBUCxDQUFhLFFBQWIsQ0FBc0IsUUFBdEIsSUFBa0MsSUFBbEM7QUFDRDtBQUNGO0FBQ0YsV0FkRztBQWVKLGtCQUFRLFVBQVMsY0FBVCxFQUF5QjtBQUMvQiw2QkFBc0IsaUJBQWlCLFNBQVMsTUFBMUIsSUFBb0MsU0FBUyxTQUFULEtBQXVCLFNBQVMsTUFBMUY7QUFDQSxtQkFBTyxLQUFQLENBQWEsTUFBYixHQUFzQixjQUF0QjtBQUNELFdBbEJHO0FBbUJKLHFCQUFXLFlBQVc7QUFDcEIsZ0JBQ0UsU0FBYSxPQUFPLEdBQVAsQ0FBVyxNQUFYLEVBRGY7QUFBQSxnQkFFRSxhQUFhLE9BQU8sR0FBUCxDQUFXLFVBQVgsRUFGZjtBQUFBLGdCQUdFLFNBSEY7QUFLQSxnQkFBRyxTQUFTLFVBQVQsSUFBdUIsVUFBMUIsRUFBc0M7QUFDcEMsMEJBQVksTUFBWjtBQUNELGFBRkQsTUFHSyxJQUFHLFNBQVMsVUFBVCxJQUF1QixVQUExQixFQUFzQztBQUN6QywwQkFBWSxJQUFaO0FBQ0QsYUFGSSxNQUdBO0FBQ0gsMEJBQVksUUFBWjtBQUNEO0FBQ0QsbUJBQU8sS0FBUCxDQUFhLFNBQWIsR0FBeUIsU0FBekI7QUFDQSxtQkFBTyxPQUFPLEtBQVAsQ0FBYSxTQUFwQjtBQUNELFdBcENHO0FBcUNKLDJCQUFpQixZQUFXO0FBQzFCLGdCQUNFLFVBQVUsT0FBTyxLQUFQLENBQWEsT0FEekI7QUFBQSxnQkFFRSxTQUFVLE9BQU8sR0FBUCxDQUFXLFVBQVgsRUFGWjtBQUlBLG1CQUFPLE9BQVAsQ0FBZSx5QkFBZjs7QUFFQSxvQkFBUSxJQUFSLEdBQXlCLFFBQVEsTUFBUixHQUFpQixPQUFPLE1BQWpEO0FBQ0Esb0JBQVEsTUFBUixHQUF3QixRQUFRLE1BQVIsRUFBeEI7QUFDQSxvQkFBUSxLQUFSLEdBQXdCLFFBQVEsVUFBUixFQUF4QjtBQUNBLG9CQUFRLE1BQVIsR0FBd0IsUUFBUSxXQUFSLEVBQXhCOztBQUVBLG1CQUFPLEtBQVAsQ0FBYSxPQUFiLEdBQXVCLE9BQXZCO0FBQ0EsbUJBQU8sT0FBUDtBQUNELFdBbkRHO0FBb0RKLCtCQUFxQixZQUFXO0FBQzlCLGdCQUNFLFNBQWEsT0FBTyxHQUFQLENBQVcsa0JBQVgsRUFEZjtBQUFBLGdCQUVFLFVBQWEsT0FBTyxHQUFQLENBQVcsZUFBWCxFQUZmOztBQUtBLGdCQUFHLFNBQVMsYUFBWixFQUEyQjtBQUN6QixzQkFBUSxNQUFSLEdBQXdCLEVBQXhCO0FBQ0Esc0JBQVEsTUFBUixDQUFlLEdBQWYsR0FBd0IsU0FBUyxRQUFRLEdBQVIsQ0FBWSxZQUFaLENBQVQsRUFBb0MsRUFBcEMsQ0FBeEI7QUFDQSxzQkFBUSxNQUFSLENBQWUsTUFBZixHQUF3QixTQUFTLFFBQVEsR0FBUixDQUFZLGVBQVosQ0FBVCxFQUF1QyxFQUF2QyxDQUF4QjtBQUNBLHNCQUFRLEdBQVIsR0FBaUIsUUFBUSxNQUFSLENBQWUsR0FBZixHQUFxQixRQUFRLE1BQVIsQ0FBZSxHQUFyRDtBQUNBLHNCQUFRLE1BQVIsR0FBaUIsUUFBUSxNQUFSLENBQWUsR0FBZixHQUFxQixRQUFRLE1BQTdCLEdBQXNDLFFBQVEsTUFBUixDQUFlLE1BQXRFO0FBQ0QsYUFORCxNQU9LO0FBQ0gsc0JBQVEsR0FBUixHQUFpQixRQUFRLE1BQVIsQ0FBZSxHQUFoQztBQUNBLHNCQUFRLE1BQVIsR0FBaUIsUUFBUSxNQUFSLENBQWUsR0FBZixHQUFxQixRQUFRLE1BQTlDO0FBQ0Q7OztBQUdELG9CQUFRLFVBQVIsR0FBNEIsT0FBTyxNQUFQLElBQWlCLFFBQVEsR0FBckQ7QUFDQSxvQkFBUSxTQUFSLEdBQTRCLE9BQU8sR0FBUCxJQUFjLFFBQVEsR0FBbEQ7QUFDQSxvQkFBUSxhQUFSLEdBQTRCLE9BQU8sTUFBUCxJQUFpQixRQUFRLE1BQXJEO0FBQ0Esb0JBQVEsWUFBUixHQUE0QixPQUFPLEdBQVAsSUFBYyxRQUFRLE1BQWxEO0FBQ0Esb0JBQVEsWUFBUixHQUEyQixDQUEzQjtBQUNBLG9CQUFRLGdCQUFSLEdBQTJCLENBQTNCOzs7QUFHQSxvQkFBUSxRQUFSLEdBQXFCLFFBQVEsVUFBUixJQUFzQixDQUFDLFFBQVEsWUFBcEQ7QUFDQSxvQkFBUSxPQUFSLEdBQXFCLFFBQVEsU0FBUixJQUFxQixDQUFDLFFBQVEsWUFBbkQ7QUFDQSxvQkFBUSxTQUFSLEdBQXFCLENBQUMsUUFBUSxRQUE5Qjs7O0FBR0EsZ0JBQUcsUUFBUSxPQUFYLEVBQW9CO0FBQ2xCLHNCQUFRLFlBQVIsR0FBNEIsT0FBTyxHQUFQLEdBQWEsUUFBUSxHQUFqRDtBQUNBLHNCQUFRLGdCQUFSLEdBQTJCLENBQUMsT0FBTyxHQUFQLEdBQWEsUUFBUSxHQUF0QixJQUE2QixRQUFRLE1BQWhFO0FBQ0Q7QUFDRCxtQkFBTyxLQUFQLENBQWEsT0FBYixHQUF1QixPQUF2QjtBQUNBLG1CQUFPLE9BQVAsQ0FBZSw4QkFBZixFQUErQyxPQUEvQztBQUNBLG1CQUFPLE9BQVA7QUFDRCxXQTNGRztBQTRGSiw4QkFBb0IsWUFBVztBQUM3QixnQkFDRSxTQUFTLE9BQU8sR0FBUCxDQUFXLE1BQVgsRUFEWDtBQUdBLG1CQUFPLElBQVAsQ0FBWSxTQUFaO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLE1BQWIsQ0FBb0IsR0FBcEIsR0FBNkIsTUFBN0I7QUFDQSxtQkFBTyxLQUFQLENBQWEsTUFBYixDQUFvQixNQUFwQixHQUE2QixTQUFTLE9BQU8sS0FBUCxDQUFhLE1BQWIsQ0FBb0IsTUFBMUQ7QUFDQSxtQkFBTyxPQUFPLEtBQVAsQ0FBYSxNQUFwQjtBQUNELFdBcEdHO0FBcUdKLHNCQUFZLFlBQVc7QUFDckIsbUJBQU8sT0FBUCxDQUFlLHdCQUFmO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLE1BQWIsR0FBc0I7QUFDcEIsc0JBQVEsU0FBUyxNQUFUO0FBRFksYUFBdEI7QUFHRCxXQTFHRztBQTJHSixvQkFBVSxZQUFXO0FBQ25CLG1CQUFPLElBQVAsQ0FBWSxVQUFaO0FBQ0EsbUJBQU8sSUFBUCxDQUFZLGVBQVo7QUFDRDtBQTlHRyxTQWh2QkM7O0FBaTJCUCxhQUFLO0FBQ0gsd0JBQWMsVUFBUyxNQUFULEVBQWlCO0FBQzdCLGdCQUNFLFVBQVUsT0FBTyxHQUFQLENBQVcsbUJBQVgsRUFEWjtBQUdBLGdCQUFHLE9BQU8sTUFBUCxDQUFjLEdBQWQsSUFBcUIsQ0FBQyxDQUF6QixFQUE0QjtBQUMxQixxQkFBUyxRQUFRLE1BQVIsSUFBa0IsU0FBUyxNQUFULEVBQWlCLEVBQWpCLElBQXVCLEdBQXpDLENBQVQ7QUFDRDtBQUNELG1CQUFPLFNBQVMsTUFBVCxFQUFpQixFQUFqQixDQUFQO0FBQ0QsV0FURTtBQVVILG9CQUFVLFVBQVMsUUFBVCxFQUFtQjtBQUMzQixtQkFBUSxPQUFPLEtBQVAsQ0FBYSxRQUFiLEtBQTBCLFNBQTNCLEdBQ0gsT0FBTyxLQUFQLENBQWEsUUFBYixDQUFzQixRQUF0QixLQUFtQyxLQURoQyxHQUVILEtBRko7QUFJRCxXQWZFO0FBZ0JILHFCQUFXLFlBQVc7QUFDcEIsZ0JBQUcsT0FBTyxLQUFQLENBQWEsU0FBYixLQUEyQixTQUE5QixFQUF5QztBQUN2QyxxQkFBTyxJQUFQLENBQVksU0FBWjtBQUNEO0FBQ0QsbUJBQU8sT0FBTyxLQUFQLENBQWEsU0FBcEI7QUFDRCxXQXJCRTtBQXNCSCwyQkFBaUIsWUFBVztBQUMxQixnQkFBRyxPQUFPLEtBQVAsQ0FBYSxPQUFiLEtBQXlCLFNBQTVCLEVBQXVDO0FBQ3JDLHFCQUFPLElBQVAsQ0FBWSxlQUFaO0FBQ0Q7QUFDRCxtQkFBTyxPQUFPLEtBQVAsQ0FBYSxPQUFwQjtBQUNELFdBM0JFO0FBNEJILCtCQUFxQixZQUFXO0FBQzlCLGdCQUFHLE9BQU8sS0FBUCxDQUFhLE9BQWIsS0FBeUIsU0FBNUIsRUFBdUM7QUFDckMscUJBQU8sSUFBUCxDQUFZLG1CQUFaO0FBQ0Q7QUFDRCxtQkFBTyxPQUFPLEtBQVAsQ0FBYSxPQUFwQjtBQUNELFdBakNFO0FBa0NILDhCQUFvQixZQUFXO0FBQzdCLGdCQUFHLE9BQU8sS0FBUCxDQUFhLE1BQWIsS0FBd0IsU0FBM0IsRUFBc0M7QUFDcEMscUJBQU8sSUFBUCxDQUFZLGtCQUFaO0FBQ0Q7QUFDRCxtQkFBTyxPQUFPLEtBQVAsQ0FBYSxNQUFwQjtBQUNELFdBdkNFO0FBd0NILHNCQUFZLFlBQVc7QUFDckIsZ0JBQUcsT0FBTyxLQUFQLENBQWEsTUFBYixLQUF3QixTQUEzQixFQUFzQztBQUNwQyxxQkFBTyxJQUFQLENBQVksVUFBWjtBQUNEO0FBQ0QsbUJBQU8sT0FBTyxLQUFQLENBQWEsTUFBcEI7QUFDRCxXQTdDRTtBQThDSCxrQkFBUSxZQUFXO0FBQ2pCLGdCQUFHLE9BQU8sS0FBUCxDQUFhLE1BQWIsS0FBd0IsU0FBM0IsRUFBc0M7QUFDcEMscUJBQU8sSUFBUCxDQUFZLE1BQVo7QUFDRDtBQUNELG1CQUFPLE9BQU8sS0FBUCxDQUFhLE1BQXBCO0FBQ0QsV0FuREU7QUFvREgsc0JBQVksWUFBVztBQUNyQixnQkFBRyxPQUFPLEtBQVAsQ0FBYSxNQUFiLEtBQXdCLFNBQTNCLEVBQXNDO0FBQ3BDLHFCQUFPLEtBQVAsQ0FBYSxtREFBYjtBQUNBLHFCQUFPLEtBQVA7QUFDRDtBQUNELG1CQUFPLE9BQU8sS0FBUCxDQUFhLE1BQWIsQ0FBb0IsR0FBM0I7QUFDRDtBQTFERSxTQWoyQkU7O0FBODVCUCxpQkFBUyxVQUFTLElBQVQsRUFBZSxLQUFmLEVBQXNCO0FBQzdCLGNBQUksRUFBRSxhQUFGLENBQWdCLElBQWhCLENBQUosRUFBNEI7QUFDMUIsY0FBRSxNQUFGLENBQVMsSUFBVCxFQUFlLFFBQWYsRUFBeUIsSUFBekI7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IscUJBQVMsSUFBVCxJQUFpQixLQUFqQjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLFNBQVMsSUFBVCxDQUFQO0FBQ0Q7QUFDRixTQXg2Qk07QUF5NkJQLGtCQUFVLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDOUIsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsTUFBZixFQUF1QixJQUF2QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixtQkFBTyxJQUFQLElBQWUsS0FBZjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLE9BQU8sSUFBUCxDQUFQO0FBQ0Q7QUFDRixTQW43Qk07QUFvN0JQLGVBQU8sWUFBVztBQUNoQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsS0FBaEMsRUFBdUM7QUFDckMsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxLQUFQLEdBQWUsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWY7QUFDQSxxQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNEO0FBQ0Y7QUFDRixTQTk3Qk07QUErN0JQLGlCQUFTLFlBQVc7QUFDbEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLE9BQTdCLElBQXdDLFNBQVMsS0FBcEQsRUFBMkQ7QUFDekQsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxPQUFQLEdBQWlCLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFqQjtBQUNBLHFCQUFPLE9BQVAsQ0FBZSxLQUFmLENBQXFCLE9BQXJCLEVBQThCLFNBQTlCO0FBQ0Q7QUFDRjtBQUNGLFNBejhCTTtBQTA4QlAsZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQWIsRUFBcUI7QUFDbkIsbUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLEtBQXJDLEVBQTRDLE9BQTVDLEVBQXFELFNBQVMsSUFBVCxHQUFnQixHQUFyRSxDQUFmO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGLFNBLzhCTTtBQWc5QlAscUJBQWE7QUFDWCxlQUFLLFVBQVMsT0FBVCxFQUFrQjtBQUNyQixnQkFDRSxXQURGLEVBRUUsYUFGRixFQUdFLFlBSEY7QUFLQSxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIsNEJBQWdCLElBQUksSUFBSixHQUFXLE9BQVgsRUFBaEI7QUFDQSw2QkFBZ0IsUUFBUSxXQUF4QjtBQUNBLDhCQUFnQixjQUFjLFlBQTlCO0FBQ0EscUJBQWdCLFdBQWhCO0FBQ0EsMEJBQVksSUFBWixDQUFpQjtBQUNmLHdCQUFtQixRQUFRLENBQVIsQ0FESjtBQUVmLDZCQUFtQixHQUFHLEtBQUgsQ0FBUyxJQUFULENBQWMsT0FBZCxFQUF1QixDQUF2QixLQUE2QixFQUZqQztBQUdmLDJCQUFtQixPQUhKO0FBSWYsa0NBQW1CO0FBSkosZUFBakI7QUFNRDtBQUNELHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLG1CQUFPLFdBQVAsQ0FBbUIsS0FBbkIsR0FBMkIsV0FBVyxPQUFPLFdBQVAsQ0FBbUIsT0FBOUIsRUFBdUMsR0FBdkMsQ0FBM0I7QUFDRCxXQXJCVTtBQXNCWCxtQkFBUyxZQUFXO0FBQ2xCLGdCQUNFLFFBQVEsU0FBUyxJQUFULEdBQWdCLEdBRDFCO0FBQUEsZ0JBRUUsWUFBWSxDQUZkO0FBSUEsbUJBQU8sS0FBUDtBQUNBLHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLGNBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDJCQUFhLEtBQUssZ0JBQUwsQ0FBYjtBQUNELGFBRkQ7QUFHQSxxQkFBUyxNQUFNLFNBQU4sR0FBa0IsSUFBM0I7QUFDQSxnQkFBRyxjQUFILEVBQW1CO0FBQ2pCLHVCQUFTLFFBQVEsY0FBUixHQUF5QixJQUFsQztBQUNEO0FBQ0QsZ0JBQUksQ0FBQyxRQUFRLEtBQVIsS0FBa0IsU0FBbEIsSUFBK0IsUUFBUSxLQUFSLEtBQWtCLFNBQWxELEtBQWdFLFlBQVksTUFBWixHQUFxQixDQUF6RixFQUE0RjtBQUMxRixzQkFBUSxjQUFSLENBQXVCLEtBQXZCO0FBQ0Esa0JBQUcsUUFBUSxLQUFYLEVBQWtCO0FBQ2hCLHdCQUFRLEtBQVIsQ0FBYyxXQUFkO0FBQ0QsZUFGRCxNQUdLO0FBQ0gsa0JBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDBCQUFRLEdBQVIsQ0FBWSxLQUFLLE1BQUwsSUFBZSxJQUFmLEdBQXNCLEtBQUssZ0JBQUwsQ0FBdEIsR0FBNkMsSUFBekQ7QUFDRCxpQkFGRDtBQUdEO0FBQ0Qsc0JBQVEsUUFBUjtBQUNEO0FBQ0QsMEJBQWMsRUFBZDtBQUNEO0FBakRVLFNBaDlCTjtBQW1nQ1AsZ0JBQVEsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDLE9BQWpDLEVBQTBDO0FBQ2hELGNBQ0UsU0FBUyxRQURYO0FBQUEsY0FFRSxRQUZGO0FBQUEsY0FHRSxLQUhGO0FBQUEsY0FJRSxRQUpGO0FBTUEsNEJBQWtCLG1CQUFtQixjQUFyQztBQUNBLG9CQUFrQixXQUFtQixPQUFyQztBQUNBLGNBQUcsT0FBTyxLQUFQLElBQWdCLFFBQWhCLElBQTRCLFdBQVcsU0FBMUMsRUFBcUQ7QUFDbkQsb0JBQVcsTUFBTSxLQUFOLENBQVksT0FBWixDQUFYO0FBQ0EsdUJBQVcsTUFBTSxNQUFOLEdBQWUsQ0FBMUI7QUFDQSxjQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ25DLGtCQUFJLGlCQUFrQixTQUFTLFFBQVYsR0FDakIsUUFBUSxNQUFNLFFBQVEsQ0FBZCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixFQUEyQixXQUEzQixFQUFSLEdBQW1ELE1BQU0sUUFBUSxDQUFkLEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBRGxDLEdBRWpCLEtBRko7QUFJQSxrQkFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxjQUFQLENBQWpCLEtBQThDLFNBQVMsUUFBM0QsRUFBdUU7QUFDckUseUJBQVMsT0FBTyxjQUFQLENBQVQ7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLGNBQVAsTUFBMkIsU0FBL0IsRUFBMkM7QUFDOUMsd0JBQVEsT0FBTyxjQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBLElBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sS0FBUCxDQUFqQixLQUFxQyxTQUFTLFFBQWxELEVBQThEO0FBQ2pFLHlCQUFTLE9BQU8sS0FBUCxDQUFUO0FBQ0QsZUFGSSxNQUdBLElBQUksT0FBTyxLQUFQLE1BQWtCLFNBQXRCLEVBQWtDO0FBQ3JDLHdCQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQTtBQUNILHVCQUFPLEtBQVAsQ0FBYSxNQUFNLE1BQW5CLEVBQTJCLEtBQTNCO0FBQ0EsdUJBQU8sS0FBUDtBQUNEO0FBQ0YsYUF2QkQ7QUF3QkQ7QUFDRCxjQUFLLEVBQUUsVUFBRixDQUFjLEtBQWQsQ0FBTCxFQUE2QjtBQUMzQix1QkFBVyxNQUFNLEtBQU4sQ0FBWSxPQUFaLEVBQXFCLGVBQXJCLENBQVg7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsdUJBQVcsS0FBWDtBQUNEO0FBQ0QsY0FBRyxFQUFFLE9BQUYsQ0FBVSxhQUFWLENBQUgsRUFBNkI7QUFDM0IsMEJBQWMsSUFBZCxDQUFtQixRQUFuQjtBQUNELFdBRkQsTUFHSyxJQUFHLGtCQUFrQixTQUFyQixFQUFnQztBQUNuQyw0QkFBZ0IsQ0FBQyxhQUFELEVBQWdCLFFBQWhCLENBQWhCO0FBQ0QsV0FGSSxNQUdBLElBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUM5Qiw0QkFBZ0IsUUFBaEI7QUFDRDtBQUNELGlCQUFPLEtBQVA7QUFDRDtBQXhqQ00sT0FBVDs7QUEyakNBLFVBQUcsYUFBSCxFQUFrQjtBQUNoQixZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsaUJBQU8sVUFBUDtBQUNEO0FBQ0QsaUJBQVMsSUFBVCxDQUFjLE1BQWQ7QUFDQSxpQkFBUyxJQUFULENBQWMsWUFBZDtBQUNBLGVBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDRCxPQVBELE1BUUs7QUFDSCxZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsbUJBQVMsTUFBVCxDQUFnQixTQUFoQjtBQUNEO0FBQ0QsZUFBTyxVQUFQO0FBQ0Q7QUFDRixLQWhuQ0g7O0FBbW5DQSxXQUFRLGtCQUFrQixTQUFuQixHQUNILGFBREcsR0FFSCxJQUZKO0FBSUQsR0F4b0NEOztBQTBvQ0EsSUFBRSxFQUFGLENBQUssVUFBTCxDQUFnQixRQUFoQixHQUEyQjs7QUFFekIsVUFBeUIsWUFGQTtBQUd6QixlQUF5QixZQUhBOztBQUt6QixXQUF5QixLQUxBO0FBTXpCLGFBQXlCLEtBTkE7QUFPekIsaUJBQXlCLElBUEE7OztBQVV6QixvQkFBeUIsSUFWQTs7O0FBYXpCLGtCQUF5QixJQWJBOzs7QUFnQnpCLG1CQUF5QixJQWhCQTs7O0FBbUJ6QixxQkFBeUIsSUFuQkE7OztBQXNCekIsb0JBQXlCLElBdEJBOzs7QUF5QnpCLFVBQXlCLElBekJBOzs7QUE0QnpCLGdCQUF5QixLQTVCQTs7O0FBK0J6QixZQUF5QixDQS9CQTs7O0FBa0N6QixtQkFBeUIsS0FsQ0E7OztBQXFDekIsYUFBeUIsTUFyQ0E7OztBQXdDekIsY0FBeUIsS0F4Q0E7OztBQTJDekIsVUFBeUIsS0EzQ0E7OztBQThDekIsWUFBeUIsSUE5Q0E7OztBQWlEekIsZ0JBQXlCLFNBakRBO0FBa0R6QixjQUF5QixJQWxEQTs7O0FBcUR6QixjQUF5QixFQXJEQTs7O0FBd0R6QixnQkFBeUIsS0F4REE7QUF5RHpCLGlCQUF5QixLQXpEQTtBQTBEekIsZUFBeUIsS0ExREE7QUEyRHpCLGtCQUF5QixLQTNEQTtBQTREekIscUJBQXlCLEtBNURBO0FBNkR6QixpQkFBeUIsS0E3REE7QUE4RHpCLG9CQUF5QixLQTlEQTs7O0FBaUV6QixzQkFBeUIsS0FqRUE7QUFrRXpCLHlCQUF5QixLQWxFQTtBQW1FekIsNEJBQXlCLEtBbkVBO0FBb0V6Qix3QkFBeUIsS0FwRUE7QUFxRXpCLDJCQUF5QixLQXJFQTs7O0FBd0V6QixZQUF5QixZQUFXLENBQUUsQ0F4RWI7QUF5RXpCLGlCQUF5QixZQUFXLENBQUUsQ0F6RWI7OztBQTRFekIsYUFBeUIsWUFBVyxDQUFFLENBNUViO0FBNkV6QixlQUF5QixZQUFXLENBQUUsQ0E3RWI7OztBQWdGekIsY0FBeUIsS0FoRkEsRTtBQWlGekIsZUFBeUIsWUFBVSxDQUFFLENBakZaOztBQW1GekIsY0FBVztBQUNULFdBQUs7QUFESSxLQW5GYzs7QUF1RnpCLGVBQVc7QUFDVCxhQUFjLE9BREw7QUFFVCxtQkFBYztBQUZMLEtBdkZjOztBQTRGekIsV0FBUTtBQUNOLGNBQVUsdUNBREo7QUFFTixlQUFVO0FBRko7O0FBNUZpQixHQUEzQjtBQW1HQyxDQXh2Q0EsRUF3dkNHLE1BeHZDSCxFQXd2Q1csTUF4dkNYLEVBd3ZDbUIsUUF4dkNuQiIsImZpbGUiOiJ2aXNpYmlsaXR5LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXG4gKiAjIFNlbWFudGljIFVJIC0gVmlzaWJpbGl0eVxuICogaHR0cDovL2dpdGh1Yi5jb20vc2VtYW50aWMtb3JnL3NlbWFudGljLXVpL1xuICpcbiAqXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2VcbiAqIGh0dHA6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9NSVRcbiAqXG4gKi9cblxuOyhmdW5jdGlvbiAoJCwgd2luZG93LCBkb2N1bWVudCwgdW5kZWZpbmVkKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG53aW5kb3cgPSAodHlwZW9mIHdpbmRvdyAhPSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuTWF0aCA9PSBNYXRoKVxuICA/IHdpbmRvd1xuICA6ICh0eXBlb2Ygc2VsZiAhPSAndW5kZWZpbmVkJyAmJiBzZWxmLk1hdGggPT0gTWF0aClcbiAgICA/IHNlbGZcbiAgICA6IEZ1bmN0aW9uKCdyZXR1cm4gdGhpcycpKClcbjtcblxuJC5mbi52aXNpYmlsaXR5ID0gZnVuY3Rpb24ocGFyYW1ldGVycykge1xuICB2YXJcbiAgICAkYWxsTW9kdWxlcyAgICA9ICQodGhpcyksXG4gICAgbW9kdWxlU2VsZWN0b3IgPSAkYWxsTW9kdWxlcy5zZWxlY3RvciB8fCAnJyxcblxuICAgIHRpbWUgICAgICAgICAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCksXG4gICAgcGVyZm9ybWFuY2UgICAgPSBbXSxcblxuICAgIHF1ZXJ5ICAgICAgICAgID0gYXJndW1lbnRzWzBdLFxuICAgIG1ldGhvZEludm9rZWQgID0gKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyksXG4gICAgcXVlcnlBcmd1bWVudHMgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSksXG4gICAgcmV0dXJuZWRWYWx1ZSxcblxuICAgIG1vZHVsZUNvdW50ICAgID0gJGFsbE1vZHVsZXMubGVuZ3RoLFxuICAgIGxvYWRlZENvdW50ICAgID0gMFxuICA7XG5cbiAgJGFsbE1vZHVsZXNcbiAgICAuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgIHZhclxuICAgICAgICBzZXR0aW5ncyAgICAgICAgPSAoICQuaXNQbGFpbk9iamVjdChwYXJhbWV0ZXJzKSApXG4gICAgICAgICAgPyAkLmV4dGVuZCh0cnVlLCB7fSwgJC5mbi52aXNpYmlsaXR5LnNldHRpbmdzLCBwYXJhbWV0ZXJzKVxuICAgICAgICAgIDogJC5leHRlbmQoe30sICQuZm4udmlzaWJpbGl0eS5zZXR0aW5ncyksXG5cbiAgICAgICAgY2xhc3NOYW1lICAgICAgID0gc2V0dGluZ3MuY2xhc3NOYW1lLFxuICAgICAgICBuYW1lc3BhY2UgICAgICAgPSBzZXR0aW5ncy5uYW1lc3BhY2UsXG4gICAgICAgIGVycm9yICAgICAgICAgICA9IHNldHRpbmdzLmVycm9yLFxuICAgICAgICBtZXRhZGF0YSAgICAgICAgPSBzZXR0aW5ncy5tZXRhZGF0YSxcblxuICAgICAgICBldmVudE5hbWVzcGFjZSAgPSAnLicgKyBuYW1lc3BhY2UsXG4gICAgICAgIG1vZHVsZU5hbWVzcGFjZSA9ICdtb2R1bGUtJyArIG5hbWVzcGFjZSxcblxuICAgICAgICAkd2luZG93ICAgICAgICAgPSAkKHdpbmRvdyksXG5cbiAgICAgICAgJG1vZHVsZSAgICAgICAgID0gJCh0aGlzKSxcbiAgICAgICAgJGNvbnRleHQgICAgICAgID0gJChzZXR0aW5ncy5jb250ZXh0KSxcblxuICAgICAgICAkcGxhY2Vob2xkZXIsXG5cbiAgICAgICAgc2VsZWN0b3IgICAgICAgID0gJG1vZHVsZS5zZWxlY3RvciB8fCAnJyxcbiAgICAgICAgaW5zdGFuY2UgICAgICAgID0gJG1vZHVsZS5kYXRhKG1vZHVsZU5hbWVzcGFjZSksXG5cbiAgICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lID0gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZVxuICAgICAgICAgIHx8IHdpbmRvdy5tb3pSZXF1ZXN0QW5pbWF0aW9uRnJhbWVcbiAgICAgICAgICB8fCB3aW5kb3cud2Via2l0UmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gICAgICAgICAgfHwgd2luZG93Lm1zUmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gICAgICAgICAgfHwgZnVuY3Rpb24oY2FsbGJhY2spIHsgc2V0VGltZW91dChjYWxsYmFjaywgMCk7IH0sXG5cbiAgICAgICAgZWxlbWVudCAgICAgICAgID0gdGhpcyxcbiAgICAgICAgZGlzYWJsZWQgICAgICAgID0gZmFsc2UsXG5cbiAgICAgICAgY29udGV4dE9ic2VydmVyLFxuICAgICAgICBvYnNlcnZlcixcbiAgICAgICAgbW9kdWxlXG4gICAgICA7XG5cbiAgICAgIG1vZHVsZSA9IHtcblxuICAgICAgICBpbml0aWFsaXplOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0luaXRpYWxpemluZycsIHNldHRpbmdzKTtcblxuICAgICAgICAgIG1vZHVsZS5zZXR1cC5jYWNoZSgpO1xuXG4gICAgICAgICAgaWYoIG1vZHVsZS5zaG91bGQudHJhY2tDaGFuZ2VzKCkgKSB7XG5cbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnR5cGUgPT0gJ2ltYWdlJykge1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0dXAuaW1hZ2UoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnR5cGUgPT0gJ2ZpeGVkJykge1xuICAgICAgICAgICAgICBtb2R1bGUuc2V0dXAuZml4ZWQoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYoc2V0dGluZ3Mub2JzZXJ2ZUNoYW5nZXMpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLm9ic2VydmVDaGFuZ2VzKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUuYmluZC5ldmVudHMoKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBtb2R1bGUuc2F2ZS5wb3NpdGlvbigpO1xuICAgICAgICAgIGlmKCAhbW9kdWxlLmlzLnZpc2libGUoKSApIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci52aXNpYmxlLCAkbW9kdWxlKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZihzZXR0aW5ncy5pbml0aWFsQ2hlY2spIHtcbiAgICAgICAgICAgIG1vZHVsZS5jaGVja1Zpc2liaWxpdHkoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgbW9kdWxlLmluc3RhbnRpYXRlKCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaW5zdGFudGlhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU3RvcmluZyBpbnN0YW5jZScsIG1vZHVsZSk7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLmRhdGEobW9kdWxlTmFtZXNwYWNlLCBtb2R1bGUpXG4gICAgICAgICAgO1xuICAgICAgICAgIGluc3RhbmNlID0gbW9kdWxlO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlc3Ryb3k6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdEZXN0cm95aW5nIHByZXZpb3VzIG1vZHVsZScpO1xuICAgICAgICAgIGlmKG9ic2VydmVyKSB7XG4gICAgICAgICAgICBvYnNlcnZlci5kaXNjb25uZWN0KCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKGNvbnRleHRPYnNlcnZlcikge1xuICAgICAgICAgICAgY29udGV4dE9ic2VydmVyLmRpc2Nvbm5lY3QoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgJHdpbmRvd1xuICAgICAgICAgICAgLm9mZignbG9hZCcgICArIGV2ZW50TmFtZXNwYWNlLCBtb2R1bGUuZXZlbnQubG9hZClcbiAgICAgICAgICAgIC5vZmYoJ3Jlc2l6ZScgKyBldmVudE5hbWVzcGFjZSwgbW9kdWxlLmV2ZW50LnJlc2l6ZSlcbiAgICAgICAgICA7XG4gICAgICAgICAgJGNvbnRleHRcbiAgICAgICAgICAgIC5vZmYoJ3Njcm9sbCcgICAgICAgKyBldmVudE5hbWVzcGFjZSwgbW9kdWxlLmV2ZW50LnNjcm9sbClcbiAgICAgICAgICAgIC5vZmYoJ3Njcm9sbGNoYW5nZScgKyBldmVudE5hbWVzcGFjZSwgbW9kdWxlLmV2ZW50LnNjcm9sbGNoYW5nZSlcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYoc2V0dGluZ3MudHlwZSA9PSAnZml4ZWQnKSB7XG4gICAgICAgICAgICBtb2R1bGUucmVzZXRGaXhlZCgpO1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5wbGFjZWhvbGRlcigpO1xuICAgICAgICAgIH1cbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAub2ZmKGV2ZW50TmFtZXNwYWNlKVxuICAgICAgICAgICAgLnJlbW92ZURhdGEobW9kdWxlTmFtZXNwYWNlKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBvYnNlcnZlQ2hhbmdlczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoJ011dGF0aW9uT2JzZXJ2ZXInIGluIHdpbmRvdykge1xuICAgICAgICAgICAgY29udGV4dE9ic2VydmVyID0gbmV3IE11dGF0aW9uT2JzZXJ2ZXIobW9kdWxlLmV2ZW50LmNvbnRleHRDaGFuZ2VkKTtcbiAgICAgICAgICAgIG9ic2VydmVyICAgICAgICA9IG5ldyBNdXRhdGlvbk9ic2VydmVyKG1vZHVsZS5ldmVudC5jaGFuZ2VkKTtcbiAgICAgICAgICAgIGNvbnRleHRPYnNlcnZlci5vYnNlcnZlKGRvY3VtZW50LCB7XG4gICAgICAgICAgICAgIGNoaWxkTGlzdCA6IHRydWUsXG4gICAgICAgICAgICAgIHN1YnRyZWUgICA6IHRydWVcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgb2JzZXJ2ZXIub2JzZXJ2ZShlbGVtZW50LCB7XG4gICAgICAgICAgICAgIGNoaWxkTGlzdCA6IHRydWUsXG4gICAgICAgICAgICAgIHN1YnRyZWUgICA6IHRydWVcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTZXR0aW5nIHVwIG11dGF0aW9uIG9ic2VydmVyJywgb2JzZXJ2ZXIpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBiaW5kOiB7XG4gICAgICAgICAgZXZlbnRzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdCaW5kaW5nIHZpc2liaWxpdHkgZXZlbnRzIHRvIHNjcm9sbCBhbmQgcmVzaXplJyk7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5yZWZyZXNoT25Mb2FkKSB7XG4gICAgICAgICAgICAgICR3aW5kb3dcbiAgICAgICAgICAgICAgICAub24oJ2xvYWQnICAgKyBldmVudE5hbWVzcGFjZSwgbW9kdWxlLmV2ZW50LmxvYWQpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICR3aW5kb3dcbiAgICAgICAgICAgICAgLm9uKCdyZXNpemUnICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS5ldmVudC5yZXNpemUpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICAvLyBwdWIvc3ViIHBhdHRlcm5cbiAgICAgICAgICAgICRjb250ZXh0XG4gICAgICAgICAgICAgIC5vZmYoJ3Njcm9sbCcgICAgICArIGV2ZW50TmFtZXNwYWNlKVxuICAgICAgICAgICAgICAub24oJ3Njcm9sbCcgICAgICAgKyBldmVudE5hbWVzcGFjZSwgbW9kdWxlLmV2ZW50LnNjcm9sbClcbiAgICAgICAgICAgICAgLm9uKCdzY3JvbGxjaGFuZ2UnICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS5ldmVudC5zY3JvbGxjaGFuZ2UpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGV2ZW50OiB7XG4gICAgICAgICAgY2hhbmdlZDogZnVuY3Rpb24obXV0YXRpb25zKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnRE9NIHRyZWUgbW9kaWZpZWQsIHVwZGF0aW5nIHZpc2liaWxpdHkgY2FsY3VsYXRpb25zJyk7XG4gICAgICAgICAgICBtb2R1bGUudGltZXIgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnRE9NIHRyZWUgbW9kaWZpZWQsIHVwZGF0aW5nIHN0aWNreSBtZW51Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5yZWZyZXNoKCk7XG4gICAgICAgICAgICB9LCAxMDApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgY29udGV4dENoYW5nZWQ6IGZ1bmN0aW9uKG11dGF0aW9ucykge1xuICAgICAgICAgICAgW10uZm9yRWFjaC5jYWxsKG11dGF0aW9ucywgZnVuY3Rpb24obXV0YXRpb24pIHtcbiAgICAgICAgICAgICAgaWYobXV0YXRpb24ucmVtb3ZlZE5vZGVzKSB7XG4gICAgICAgICAgICAgICAgW10uZm9yRWFjaC5jYWxsKG11dGF0aW9uLnJlbW92ZWROb2RlcywgZnVuY3Rpb24obm9kZSkge1xuICAgICAgICAgICAgICAgICAgaWYobm9kZSA9PSBlbGVtZW50IHx8ICQobm9kZSkuZmluZChlbGVtZW50KS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRWxlbWVudCByZW1vdmVkIGZyb20gRE9NLCB0ZWFyaW5nIGRvd24gZXZlbnRzJyk7XG4gICAgICAgICAgICAgICAgICAgIG1vZHVsZS5kZXN0cm95KCk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcmVzaXplOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnV2luZG93IHJlc2l6ZWQnKTtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnJlZnJlc2hPblJlc2l6ZSkge1xuICAgICAgICAgICAgICByZXF1ZXN0QW5pbWF0aW9uRnJhbWUobW9kdWxlLnJlZnJlc2gpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgbG9hZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1BhZ2UgZmluaXNoZWQgbG9hZGluZycpO1xuICAgICAgICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKG1vZHVsZS5yZWZyZXNoKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIC8vIHB1Ymxpc2hlcyBzY3JvbGxjaGFuZ2UgZXZlbnQgb24gb25lIHNjcm9sbFxuICAgICAgICAgIHNjcm9sbDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy50aHJvdHRsZSkge1xuICAgICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnRpbWVyKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAkY29udGV4dC50cmlnZ2VySGFuZGxlcignc2Nyb2xsY2hhbmdlJyArIGV2ZW50TmFtZXNwYWNlLCBbICRjb250ZXh0LnNjcm9sbFRvcCgpIF0pO1xuICAgICAgICAgICAgICB9LCBzZXR0aW5ncy50aHJvdHRsZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICRjb250ZXh0LnRyaWdnZXJIYW5kbGVyKCdzY3JvbGxjaGFuZ2UnICsgZXZlbnROYW1lc3BhY2UsIFsgJGNvbnRleHQuc2Nyb2xsVG9wKCkgXSk7XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgLy8gc3Vic2NyaWJlcyB0byBzY3JvbGxjaGFuZ2VcbiAgICAgICAgICBzY3JvbGxjaGFuZ2U6IGZ1bmN0aW9uKGV2ZW50LCBzY3JvbGxQb3NpdGlvbikge1xuICAgICAgICAgICAgbW9kdWxlLmNoZWNrVmlzaWJpbGl0eShzY3JvbGxQb3NpdGlvbik7XG4gICAgICAgICAgfSxcbiAgICAgICAgfSxcblxuICAgICAgICBwcmVjYWNoZTogZnVuY3Rpb24oaW1hZ2VzLCBjYWxsYmFjaykge1xuICAgICAgICAgIGlmICghKGltYWdlcyBpbnN0YW5jZW9mIEFycmF5KSkge1xuICAgICAgICAgICAgaW1hZ2VzID0gW2ltYWdlc107XG4gICAgICAgICAgfVxuICAgICAgICAgIHZhclxuICAgICAgICAgICAgaW1hZ2VzTGVuZ3RoICA9IGltYWdlcy5sZW5ndGgsXG4gICAgICAgICAgICBsb2FkZWRDb3VudGVyID0gMCxcbiAgICAgICAgICAgIGNhY2hlICAgICAgICAgPSBbXSxcbiAgICAgICAgICAgIGNhY2hlSW1hZ2UgICAgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpbWcnKSxcbiAgICAgICAgICAgIGhhbmRsZUxvYWQgICAgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgbG9hZGVkQ291bnRlcisrO1xuICAgICAgICAgICAgICBpZiAobG9hZGVkQ291bnRlciA+PSBpbWFnZXMubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgaWYgKCQuaXNGdW5jdGlvbihjYWxsYmFjaykpIHtcbiAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgO1xuICAgICAgICAgIHdoaWxlIChpbWFnZXNMZW5ndGgtLSkge1xuICAgICAgICAgICAgY2FjaGVJbWFnZSAgICAgICAgID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaW1nJyk7XG4gICAgICAgICAgICBjYWNoZUltYWdlLm9ubG9hZCAgPSBoYW5kbGVMb2FkO1xuICAgICAgICAgICAgY2FjaGVJbWFnZS5vbmVycm9yID0gaGFuZGxlTG9hZDtcbiAgICAgICAgICAgIGNhY2hlSW1hZ2Uuc3JjICAgICA9IGltYWdlc1tpbWFnZXNMZW5ndGhdO1xuICAgICAgICAgICAgY2FjaGUucHVzaChjYWNoZUltYWdlKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZW5hYmxlQ2FsbGJhY2tzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0FsbG93aW5nIGNhbGxiYWNrcyB0byBvY2N1cicpO1xuICAgICAgICAgIGRpc2FibGVkID0gZmFsc2U7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGlzYWJsZUNhbGxiYWNrczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdEaXNhYmxpbmcgYWxsIGNhbGxiYWNrcyB0ZW1wb3JhcmlseScpO1xuICAgICAgICAgIGRpc2FibGVkID0gdHJ1ZTtcbiAgICAgICAgfSxcblxuICAgICAgICBzaG91bGQ6IHtcbiAgICAgICAgICB0cmFja0NoYW5nZXM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobWV0aG9kSW52b2tlZCkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ09uZSB0aW1lIHF1ZXJ5LCBubyBuZWVkIHRvIGJpbmQgZXZlbnRzJyk7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ2FsbGJhY2tzIGJlaW5nIGF0dGFjaGVkJyk7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0dXA6IHtcbiAgICAgICAgICBjYWNoZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuY2FjaGUgPSB7XG4gICAgICAgICAgICAgIG9jY3VycmVkIDoge30sXG4gICAgICAgICAgICAgIHNjcmVlbiAgIDoge30sXG4gICAgICAgICAgICAgIGVsZW1lbnQgIDoge30sXG4gICAgICAgICAgICB9O1xuICAgICAgICAgIH0sXG4gICAgICAgICAgaW1hZ2U6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHNyYyA9ICRtb2R1bGUuZGF0YShtZXRhZGF0YS5zcmMpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzcmMpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0xhenkgbG9hZGluZyBpbWFnZScsIHNyYyk7XG4gICAgICAgICAgICAgIHNldHRpbmdzLm9uY2UgICAgICAgICAgID0gdHJ1ZTtcbiAgICAgICAgICAgICAgc2V0dGluZ3Mub2JzZXJ2ZUNoYW5nZXMgPSBmYWxzZTtcblxuICAgICAgICAgICAgICAvLyBzaG93IHdoZW4gdG9wIHZpc2libGVcbiAgICAgICAgICAgICAgc2V0dGluZ3Mub25PblNjcmVlbiA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW1hZ2Ugb24gc2NyZWVuJywgZWxlbWVudCk7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnByZWNhY2hlKHNyYywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuc2V0LmltYWdlKHNyYywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgIGxvYWRlZENvdW50Kys7XG4gICAgICAgICAgICAgICAgICAgIGlmKGxvYWRlZENvdW50ID09IG1vZHVsZUNvdW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3Mub25BbGxMb2FkZWQuY2FsbCh0aGlzKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBzZXR0aW5ncy5vbkxvYWQuY2FsbCh0aGlzKTtcbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgZml4ZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTZXR0aW5nIHVwIGZpeGVkJyk7XG4gICAgICAgICAgICBzZXR0aW5ncy5vbmNlICAgICAgICAgICA9IGZhbHNlO1xuICAgICAgICAgICAgc2V0dGluZ3Mub2JzZXJ2ZUNoYW5nZXMgPSBmYWxzZTtcbiAgICAgICAgICAgIHNldHRpbmdzLmluaXRpYWxDaGVjayAgID0gdHJ1ZTtcbiAgICAgICAgICAgIHNldHRpbmdzLnJlZnJlc2hPbkxvYWQgID0gdHJ1ZTtcbiAgICAgICAgICAgIGlmKCFwYXJhbWV0ZXJzLnRyYW5zaXRpb24pIHtcbiAgICAgICAgICAgICAgc2V0dGluZ3MudHJhbnNpdGlvbiA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLmNyZWF0ZS5wbGFjZWhvbGRlcigpO1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdBZGRlZCBwbGFjZWhvbGRlcicsICRwbGFjZWhvbGRlcik7XG4gICAgICAgICAgICBzZXR0aW5ncy5vblRvcFBhc3NlZCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0VsZW1lbnQgcGFzc2VkLCBhZGRpbmcgZml4ZWQgcG9zaXRpb24nLCAkbW9kdWxlKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnNob3cucGxhY2Vob2xkZXIoKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5maXhlZCgpO1xuICAgICAgICAgICAgICBpZihzZXR0aW5ncy50cmFuc2l0aW9uKSB7XG4gICAgICAgICAgICAgICAgaWYoJC5mbi50cmFuc2l0aW9uICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICRtb2R1bGUudHJhbnNpdGlvbihzZXR0aW5ncy50cmFuc2l0aW9uLCBzZXR0aW5ncy5kdXJhdGlvbik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgc2V0dGluZ3Mub25Ub3BQYXNzZWRSZXZlcnNlID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRWxlbWVudCByZXR1cm5lZCB0byBwb3NpdGlvbiwgcmVtb3ZpbmcgZml4ZWQnLCAkbW9kdWxlKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmhpZGUucGxhY2Vob2xkZXIoKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5maXhlZCgpO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgY3JlYXRlOiB7XG4gICAgICAgICAgcGxhY2Vob2xkZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0NyZWF0aW5nIGZpeGVkIHBvc2l0aW9uIHBsYWNlaG9sZGVyJyk7XG4gICAgICAgICAgICAkcGxhY2Vob2xkZXIgPSAkbW9kdWxlXG4gICAgICAgICAgICAgIC5jbG9uZShmYWxzZSlcbiAgICAgICAgICAgICAgLmNzcygnZGlzcGxheScsICdub25lJylcbiAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5wbGFjZWhvbGRlcilcbiAgICAgICAgICAgICAgLmluc2VydEFmdGVyKCRtb2R1bGUpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNob3c6IHtcbiAgICAgICAgICBwbGFjZWhvbGRlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU2hvd2luZyBwbGFjZWhvbGRlcicpO1xuICAgICAgICAgICAgJHBsYWNlaG9sZGVyXG4gICAgICAgICAgICAgIC5jc3MoJ2Rpc3BsYXknLCAnYmxvY2snKVxuICAgICAgICAgICAgICAuY3NzKCd2aXNpYmlsaXR5JywgJ2hpZGRlbicpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBoaWRlOiB7XG4gICAgICAgICAgcGxhY2Vob2xkZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0hpZGluZyBwbGFjZWhvbGRlcicpO1xuICAgICAgICAgICAgJHBsYWNlaG9sZGVyXG4gICAgICAgICAgICAgIC5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpXG4gICAgICAgICAgICAgIC5jc3MoJ3Zpc2liaWxpdHknLCAnJylcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0OiB7XG4gICAgICAgICAgZml4ZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1NldHRpbmcgZWxlbWVudCB0byBmaXhlZCBwb3NpdGlvbicpO1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLmZpeGVkKVxuICAgICAgICAgICAgICAuY3NzKHtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbiA6ICdmaXhlZCcsXG4gICAgICAgICAgICAgICAgdG9wICAgICAgOiBzZXR0aW5ncy5vZmZzZXQgKyAncHgnLFxuICAgICAgICAgICAgICAgIGxlZnQgICAgIDogJ2F1dG8nLFxuICAgICAgICAgICAgICAgIHpJbmRleCAgIDogc2V0dGluZ3MuekluZGV4XG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBzZXR0aW5ncy5vbkZpeGVkLmNhbGwoZWxlbWVudCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBpbWFnZTogZnVuY3Rpb24oc3JjLCBjYWxsYmFjaykge1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAuYXR0cignc3JjJywgc3JjKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MudHJhbnNpdGlvbikge1xuICAgICAgICAgICAgICBpZiggJC5mbi50cmFuc2l0aW9uICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgJG1vZHVsZS50cmFuc2l0aW9uKHNldHRpbmdzLnRyYW5zaXRpb24sIHNldHRpbmdzLmR1cmF0aW9uLCBjYWxsYmFjayk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJG1vZHVsZS5mYWRlSW4oc2V0dGluZ3MuZHVyYXRpb24sIGNhbGxiYWNrKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICRtb2R1bGUuc2hvdygpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBpczoge1xuICAgICAgICAgIG9uU2NyZWVuOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBjYWxjdWxhdGlvbnMgICA9IG1vZHVsZS5nZXQuZWxlbWVudENhbGN1bGF0aW9ucygpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICByZXR1cm4gY2FsY3VsYXRpb25zLm9uU2NyZWVuO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgb2ZmU2NyZWVuOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBjYWxjdWxhdGlvbnMgICA9IG1vZHVsZS5nZXQuZWxlbWVudENhbGN1bGF0aW9ucygpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICByZXR1cm4gY2FsY3VsYXRpb25zLm9mZlNjcmVlbjtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHZpc2libGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmNhY2hlICYmIG1vZHVsZS5jYWNoZS5lbGVtZW50KSB7XG4gICAgICAgICAgICAgIHJldHVybiAhKG1vZHVsZS5jYWNoZS5lbGVtZW50LndpZHRoID09PSAwICYmIG1vZHVsZS5jYWNoZS5lbGVtZW50Lm9mZnNldC50b3AgPT09IDApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICByZWZyZXNoOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ1JlZnJlc2hpbmcgY29uc3RhbnRzICh3aWR0aC9oZWlnaHQpJyk7XG4gICAgICAgICAgaWYoc2V0dGluZ3MudHlwZSA9PSAnZml4ZWQnKSB7XG4gICAgICAgICAgICBtb2R1bGUucmVzZXRGaXhlZCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBtb2R1bGUucmVzZXQoKTtcbiAgICAgICAgICBtb2R1bGUuc2F2ZS5wb3NpdGlvbigpO1xuICAgICAgICAgIGlmKHNldHRpbmdzLmNoZWNrT25SZWZyZXNoKSB7XG4gICAgICAgICAgICBtb2R1bGUuY2hlY2tWaXNpYmlsaXR5KCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHNldHRpbmdzLm9uUmVmcmVzaC5jYWxsKGVsZW1lbnQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHJlc2V0Rml4ZWQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBtb2R1bGUucmVtb3ZlLmZpeGVkKCk7XG4gICAgICAgICAgbW9kdWxlLnJlbW92ZS5vY2N1cnJlZCgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHJlc2V0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVzZXR0aW5nIGFsbCBjYWNoZWQgdmFsdWVzJyk7XG4gICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChtb2R1bGUuY2FjaGUpICkge1xuICAgICAgICAgICAgbW9kdWxlLmNhY2hlLnNjcmVlbiA9IHt9O1xuICAgICAgICAgICAgbW9kdWxlLmNhY2hlLmVsZW1lbnQgPSB7fTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2hlY2tWaXNpYmlsaXR5OiBmdW5jdGlvbihzY3JvbGwpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQ2hlY2tpbmcgdmlzaWJpbGl0eSBvZiBlbGVtZW50JywgbW9kdWxlLmNhY2hlLmVsZW1lbnQpO1xuXG4gICAgICAgICAgaWYoICFkaXNhYmxlZCAmJiBtb2R1bGUuaXMudmlzaWJsZSgpICkge1xuXG4gICAgICAgICAgICAvLyBzYXZlIHNjcm9sbCBwb3NpdGlvblxuICAgICAgICAgICAgbW9kdWxlLnNhdmUuc2Nyb2xsKHNjcm9sbCk7XG5cbiAgICAgICAgICAgIC8vIHVwZGF0ZSBjYWxjdWxhdGlvbnMgZGVyaXZlZCBmcm9tIHNjcm9sbFxuICAgICAgICAgICAgbW9kdWxlLnNhdmUuY2FsY3VsYXRpb25zKCk7XG5cbiAgICAgICAgICAgIC8vIHBlcmNlbnRhZ2VcbiAgICAgICAgICAgIG1vZHVsZS5wYXNzZWQoKTtcblxuICAgICAgICAgICAgLy8gcmV2ZXJzZSAobXVzdCBiZSBmaXJzdClcbiAgICAgICAgICAgIG1vZHVsZS5wYXNzaW5nUmV2ZXJzZSgpO1xuICAgICAgICAgICAgbW9kdWxlLnRvcFZpc2libGVSZXZlcnNlKCk7XG4gICAgICAgICAgICBtb2R1bGUuYm90dG9tVmlzaWJsZVJldmVyc2UoKTtcbiAgICAgICAgICAgIG1vZHVsZS50b3BQYXNzZWRSZXZlcnNlKCk7XG4gICAgICAgICAgICBtb2R1bGUuYm90dG9tUGFzc2VkUmV2ZXJzZSgpO1xuXG4gICAgICAgICAgICAvLyBvbmUgdGltZVxuICAgICAgICAgICAgbW9kdWxlLm9uU2NyZWVuKCk7XG4gICAgICAgICAgICBtb2R1bGUub2ZmU2NyZWVuKCk7XG4gICAgICAgICAgICBtb2R1bGUucGFzc2luZygpO1xuICAgICAgICAgICAgbW9kdWxlLnRvcFZpc2libGUoKTtcbiAgICAgICAgICAgIG1vZHVsZS5ib3R0b21WaXNpYmxlKCk7XG4gICAgICAgICAgICBtb2R1bGUudG9wUGFzc2VkKCk7XG4gICAgICAgICAgICBtb2R1bGUuYm90dG9tUGFzc2VkKCk7XG5cbiAgICAgICAgICAgIC8vIG9uIHVwZGF0ZSBjYWxsYmFja1xuICAgICAgICAgICAgaWYoc2V0dGluZ3Mub25VcGRhdGUpIHtcbiAgICAgICAgICAgICAgc2V0dGluZ3Mub25VcGRhdGUuY2FsbChlbGVtZW50LCBtb2R1bGUuZ2V0LmVsZW1lbnRDYWxjdWxhdGlvbnMoKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHBhc3NlZDogZnVuY3Rpb24oYW1vdW50LCBuZXdDYWxsYmFjaykge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgY2FsY3VsYXRpb25zICAgPSBtb2R1bGUuZ2V0LmVsZW1lbnRDYWxjdWxhdGlvbnMoKSxcbiAgICAgICAgICAgIGFtb3VudEluUGl4ZWxzXG4gICAgICAgICAgO1xuICAgICAgICAgIC8vIGFzc2lnbiBjYWxsYmFja1xuICAgICAgICAgIGlmKGFtb3VudCAmJiBuZXdDYWxsYmFjaykge1xuICAgICAgICAgICAgc2V0dGluZ3Mub25QYXNzZWRbYW1vdW50XSA9IG5ld0NhbGxiYWNrO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKGFtb3VudCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm4gKG1vZHVsZS5nZXQucGl4ZWxzUGFzc2VkKGFtb3VudCkgPiBjYWxjdWxhdGlvbnMucGl4ZWxzUGFzc2VkKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihjYWxjdWxhdGlvbnMucGFzc2luZykge1xuICAgICAgICAgICAgJC5lYWNoKHNldHRpbmdzLm9uUGFzc2VkLCBmdW5jdGlvbihhbW91bnQsIGNhbGxiYWNrKSB7XG4gICAgICAgICAgICAgIGlmKGNhbGN1bGF0aW9ucy5ib3R0b21WaXNpYmxlIHx8IGNhbGN1bGF0aW9ucy5waXhlbHNQYXNzZWQgPiBtb2R1bGUuZ2V0LnBpeGVsc1Bhc3NlZChhbW91bnQpKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmV4ZWN1dGUoY2FsbGJhY2ssIGFtb3VudCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZighc2V0dGluZ3Mub25jZSkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUub2NjdXJyZWQoY2FsbGJhY2spO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgb25TY3JlZW46IGZ1bmN0aW9uKG5ld0NhbGxiYWNrKSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICBjYWxjdWxhdGlvbnMgPSBtb2R1bGUuZ2V0LmVsZW1lbnRDYWxjdWxhdGlvbnMoKSxcbiAgICAgICAgICAgIGNhbGxiYWNrICAgICA9IG5ld0NhbGxiYWNrIHx8IHNldHRpbmdzLm9uT25TY3JlZW4sXG4gICAgICAgICAgICBjYWxsYmFja05hbWUgPSAnb25TY3JlZW4nXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKG5ld0NhbGxiYWNrKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0FkZGluZyBjYWxsYmFjayBmb3Igb25TY3JlZW4nLCBuZXdDYWxsYmFjayk7XG4gICAgICAgICAgICBzZXR0aW5ncy5vbk9uU2NyZWVuID0gbmV3Q2FsbGJhY2s7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKGNhbGN1bGF0aW9ucy5vblNjcmVlbikge1xuICAgICAgICAgICAgbW9kdWxlLmV4ZWN1dGUoY2FsbGJhY2ssIGNhbGxiYWNrTmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYoIXNldHRpbmdzLm9uY2UpIHtcbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUub2NjdXJyZWQoY2FsbGJhY2tOYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYobmV3Q2FsbGJhY2sgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuIGNhbGN1bGF0aW9ucy5vbk9uU2NyZWVuO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBvZmZTY3JlZW46IGZ1bmN0aW9uKG5ld0NhbGxiYWNrKSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICBjYWxjdWxhdGlvbnMgPSBtb2R1bGUuZ2V0LmVsZW1lbnRDYWxjdWxhdGlvbnMoKSxcbiAgICAgICAgICAgIGNhbGxiYWNrICAgICA9IG5ld0NhbGxiYWNrIHx8IHNldHRpbmdzLm9uT2ZmU2NyZWVuLFxuICAgICAgICAgICAgY2FsbGJhY2tOYW1lID0gJ29mZlNjcmVlbidcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYobmV3Q2FsbGJhY2spIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWRkaW5nIGNhbGxiYWNrIGZvciBvZmZTY3JlZW4nLCBuZXdDYWxsYmFjayk7XG4gICAgICAgICAgICBzZXR0aW5ncy5vbk9mZlNjcmVlbiA9IG5ld0NhbGxiYWNrO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZihjYWxjdWxhdGlvbnMub2ZmU2NyZWVuKSB7XG4gICAgICAgICAgICBtb2R1bGUuZXhlY3V0ZShjYWxsYmFjaywgY2FsbGJhY2tOYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZighc2V0dGluZ3Mub25jZSkge1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5vY2N1cnJlZChjYWxsYmFja05hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZihuZXdDYWxsYmFjayAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm4gY2FsY3VsYXRpb25zLm9uT2ZmU2NyZWVuO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBwYXNzaW5nOiBmdW5jdGlvbihuZXdDYWxsYmFjaykge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgY2FsY3VsYXRpb25zID0gbW9kdWxlLmdldC5lbGVtZW50Q2FsY3VsYXRpb25zKCksXG4gICAgICAgICAgICBjYWxsYmFjayAgICAgPSBuZXdDYWxsYmFjayB8fCBzZXR0aW5ncy5vblBhc3NpbmcsXG4gICAgICAgICAgICBjYWxsYmFja05hbWUgPSAncGFzc2luZydcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYobmV3Q2FsbGJhY2spIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWRkaW5nIGNhbGxiYWNrIGZvciBwYXNzaW5nJywgbmV3Q2FsbGJhY2spO1xuICAgICAgICAgICAgc2V0dGluZ3Mub25QYXNzaW5nID0gbmV3Q2FsbGJhY2s7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKGNhbGN1bGF0aW9ucy5wYXNzaW5nKSB7XG4gICAgICAgICAgICBtb2R1bGUuZXhlY3V0ZShjYWxsYmFjaywgY2FsbGJhY2tOYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZighc2V0dGluZ3Mub25jZSkge1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5vY2N1cnJlZChjYWxsYmFja05hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZihuZXdDYWxsYmFjayAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm4gY2FsY3VsYXRpb25zLnBhc3Npbmc7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG5cbiAgICAgICAgdG9wVmlzaWJsZTogZnVuY3Rpb24obmV3Q2FsbGJhY2spIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIGNhbGN1bGF0aW9ucyA9IG1vZHVsZS5nZXQuZWxlbWVudENhbGN1bGF0aW9ucygpLFxuICAgICAgICAgICAgY2FsbGJhY2sgICAgID0gbmV3Q2FsbGJhY2sgfHwgc2V0dGluZ3Mub25Ub3BWaXNpYmxlLFxuICAgICAgICAgICAgY2FsbGJhY2tOYW1lID0gJ3RvcFZpc2libGUnXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKG5ld0NhbGxiYWNrKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0FkZGluZyBjYWxsYmFjayBmb3IgdG9wIHZpc2libGUnLCBuZXdDYWxsYmFjayk7XG4gICAgICAgICAgICBzZXR0aW5ncy5vblRvcFZpc2libGUgPSBuZXdDYWxsYmFjaztcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoY2FsY3VsYXRpb25zLnRvcFZpc2libGUpIHtcbiAgICAgICAgICAgIG1vZHVsZS5leGVjdXRlKGNhbGxiYWNrLCBjYWxsYmFja05hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKCFzZXR0aW5ncy5vbmNlKSB7XG4gICAgICAgICAgICBtb2R1bGUucmVtb3ZlLm9jY3VycmVkKGNhbGxiYWNrTmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKG5ld0NhbGxiYWNrID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybiBjYWxjdWxhdGlvbnMudG9wVmlzaWJsZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgYm90dG9tVmlzaWJsZTogZnVuY3Rpb24obmV3Q2FsbGJhY2spIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIGNhbGN1bGF0aW9ucyA9IG1vZHVsZS5nZXQuZWxlbWVudENhbGN1bGF0aW9ucygpLFxuICAgICAgICAgICAgY2FsbGJhY2sgICAgID0gbmV3Q2FsbGJhY2sgfHwgc2V0dGluZ3Mub25Cb3R0b21WaXNpYmxlLFxuICAgICAgICAgICAgY2FsbGJhY2tOYW1lID0gJ2JvdHRvbVZpc2libGUnXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKG5ld0NhbGxiYWNrKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0FkZGluZyBjYWxsYmFjayBmb3IgYm90dG9tIHZpc2libGUnLCBuZXdDYWxsYmFjayk7XG4gICAgICAgICAgICBzZXR0aW5ncy5vbkJvdHRvbVZpc2libGUgPSBuZXdDYWxsYmFjaztcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoY2FsY3VsYXRpb25zLmJvdHRvbVZpc2libGUpIHtcbiAgICAgICAgICAgIG1vZHVsZS5leGVjdXRlKGNhbGxiYWNrLCBjYWxsYmFja05hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKCFzZXR0aW5ncy5vbmNlKSB7XG4gICAgICAgICAgICBtb2R1bGUucmVtb3ZlLm9jY3VycmVkKGNhbGxiYWNrTmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKG5ld0NhbGxiYWNrID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybiBjYWxjdWxhdGlvbnMuYm90dG9tVmlzaWJsZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgdG9wUGFzc2VkOiBmdW5jdGlvbihuZXdDYWxsYmFjaykge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgY2FsY3VsYXRpb25zID0gbW9kdWxlLmdldC5lbGVtZW50Q2FsY3VsYXRpb25zKCksXG4gICAgICAgICAgICBjYWxsYmFjayAgICAgPSBuZXdDYWxsYmFjayB8fCBzZXR0aW5ncy5vblRvcFBhc3NlZCxcbiAgICAgICAgICAgIGNhbGxiYWNrTmFtZSA9ICd0b3BQYXNzZWQnXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKG5ld0NhbGxiYWNrKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0FkZGluZyBjYWxsYmFjayBmb3IgdG9wIHBhc3NlZCcsIG5ld0NhbGxiYWNrKTtcbiAgICAgICAgICAgIHNldHRpbmdzLm9uVG9wUGFzc2VkID0gbmV3Q2FsbGJhY2s7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKGNhbGN1bGF0aW9ucy50b3BQYXNzZWQpIHtcbiAgICAgICAgICAgIG1vZHVsZS5leGVjdXRlKGNhbGxiYWNrLCBjYWxsYmFja05hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKCFzZXR0aW5ncy5vbmNlKSB7XG4gICAgICAgICAgICBtb2R1bGUucmVtb3ZlLm9jY3VycmVkKGNhbGxiYWNrTmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKG5ld0NhbGxiYWNrID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybiBjYWxjdWxhdGlvbnMudG9wUGFzc2VkO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBib3R0b21QYXNzZWQ6IGZ1bmN0aW9uKG5ld0NhbGxiYWNrKSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICBjYWxjdWxhdGlvbnMgPSBtb2R1bGUuZ2V0LmVsZW1lbnRDYWxjdWxhdGlvbnMoKSxcbiAgICAgICAgICAgIGNhbGxiYWNrICAgICA9IG5ld0NhbGxiYWNrIHx8IHNldHRpbmdzLm9uQm90dG9tUGFzc2VkLFxuICAgICAgICAgICAgY2FsbGJhY2tOYW1lID0gJ2JvdHRvbVBhc3NlZCdcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYobmV3Q2FsbGJhY2spIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWRkaW5nIGNhbGxiYWNrIGZvciBib3R0b20gcGFzc2VkJywgbmV3Q2FsbGJhY2spO1xuICAgICAgICAgICAgc2V0dGluZ3Mub25Cb3R0b21QYXNzZWQgPSBuZXdDYWxsYmFjaztcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoY2FsY3VsYXRpb25zLmJvdHRvbVBhc3NlZCkge1xuICAgICAgICAgICAgbW9kdWxlLmV4ZWN1dGUoY2FsbGJhY2ssIGNhbGxiYWNrTmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYoIXNldHRpbmdzLm9uY2UpIHtcbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUub2NjdXJyZWQoY2FsbGJhY2tOYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYobmV3Q2FsbGJhY2sgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuIGNhbGN1bGF0aW9ucy5ib3R0b21QYXNzZWQ7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHBhc3NpbmdSZXZlcnNlOiBmdW5jdGlvbihuZXdDYWxsYmFjaykge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgY2FsY3VsYXRpb25zID0gbW9kdWxlLmdldC5lbGVtZW50Q2FsY3VsYXRpb25zKCksXG4gICAgICAgICAgICBjYWxsYmFjayAgICAgPSBuZXdDYWxsYmFjayB8fCBzZXR0aW5ncy5vblBhc3NpbmdSZXZlcnNlLFxuICAgICAgICAgICAgY2FsbGJhY2tOYW1lID0gJ3Bhc3NpbmdSZXZlcnNlJ1xuICAgICAgICAgIDtcbiAgICAgICAgICBpZihuZXdDYWxsYmFjaykge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdBZGRpbmcgY2FsbGJhY2sgZm9yIHBhc3NpbmcgcmV2ZXJzZScsIG5ld0NhbGxiYWNrKTtcbiAgICAgICAgICAgIHNldHRpbmdzLm9uUGFzc2luZ1JldmVyc2UgPSBuZXdDYWxsYmFjaztcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoIWNhbGN1bGF0aW9ucy5wYXNzaW5nKSB7XG4gICAgICAgICAgICBpZihtb2R1bGUuZ2V0Lm9jY3VycmVkKCdwYXNzaW5nJykpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmV4ZWN1dGUoY2FsbGJhY2ssIGNhbGxiYWNrTmFtZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYoIXNldHRpbmdzLm9uY2UpIHtcbiAgICAgICAgICAgIG1vZHVsZS5yZW1vdmUub2NjdXJyZWQoY2FsbGJhY2tOYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYobmV3Q2FsbGJhY2sgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuICFjYWxjdWxhdGlvbnMucGFzc2luZztcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cblxuICAgICAgICB0b3BWaXNpYmxlUmV2ZXJzZTogZnVuY3Rpb24obmV3Q2FsbGJhY2spIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIGNhbGN1bGF0aW9ucyA9IG1vZHVsZS5nZXQuZWxlbWVudENhbGN1bGF0aW9ucygpLFxuICAgICAgICAgICAgY2FsbGJhY2sgICAgID0gbmV3Q2FsbGJhY2sgfHwgc2V0dGluZ3Mub25Ub3BWaXNpYmxlUmV2ZXJzZSxcbiAgICAgICAgICAgIGNhbGxiYWNrTmFtZSA9ICd0b3BWaXNpYmxlUmV2ZXJzZSdcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYobmV3Q2FsbGJhY2spIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWRkaW5nIGNhbGxiYWNrIGZvciB0b3AgdmlzaWJsZSByZXZlcnNlJywgbmV3Q2FsbGJhY2spO1xuICAgICAgICAgICAgc2V0dGluZ3Mub25Ub3BWaXNpYmxlUmV2ZXJzZSA9IG5ld0NhbGxiYWNrO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZighY2FsY3VsYXRpb25zLnRvcFZpc2libGUpIHtcbiAgICAgICAgICAgIGlmKG1vZHVsZS5nZXQub2NjdXJyZWQoJ3RvcFZpc2libGUnKSkge1xuICAgICAgICAgICAgICBtb2R1bGUuZXhlY3V0ZShjYWxsYmFjaywgY2FsbGJhY2tOYW1lKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZighc2V0dGluZ3Mub25jZSkge1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5vY2N1cnJlZChjYWxsYmFja05hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZihuZXdDYWxsYmFjayA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm4gIWNhbGN1bGF0aW9ucy50b3BWaXNpYmxlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBib3R0b21WaXNpYmxlUmV2ZXJzZTogZnVuY3Rpb24obmV3Q2FsbGJhY2spIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIGNhbGN1bGF0aW9ucyA9IG1vZHVsZS5nZXQuZWxlbWVudENhbGN1bGF0aW9ucygpLFxuICAgICAgICAgICAgY2FsbGJhY2sgICAgID0gbmV3Q2FsbGJhY2sgfHwgc2V0dGluZ3Mub25Cb3R0b21WaXNpYmxlUmV2ZXJzZSxcbiAgICAgICAgICAgIGNhbGxiYWNrTmFtZSA9ICdib3R0b21WaXNpYmxlUmV2ZXJzZSdcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYobmV3Q2FsbGJhY2spIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWRkaW5nIGNhbGxiYWNrIGZvciBib3R0b20gdmlzaWJsZSByZXZlcnNlJywgbmV3Q2FsbGJhY2spO1xuICAgICAgICAgICAgc2V0dGluZ3Mub25Cb3R0b21WaXNpYmxlUmV2ZXJzZSA9IG5ld0NhbGxiYWNrO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZighY2FsY3VsYXRpb25zLmJvdHRvbVZpc2libGUpIHtcbiAgICAgICAgICAgIGlmKG1vZHVsZS5nZXQub2NjdXJyZWQoJ2JvdHRvbVZpc2libGUnKSkge1xuICAgICAgICAgICAgICBtb2R1bGUuZXhlY3V0ZShjYWxsYmFjaywgY2FsbGJhY2tOYW1lKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZighc2V0dGluZ3Mub25jZSkge1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5vY2N1cnJlZChjYWxsYmFja05hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZihuZXdDYWxsYmFjayA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm4gIWNhbGN1bGF0aW9ucy5ib3R0b21WaXNpYmxlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICB0b3BQYXNzZWRSZXZlcnNlOiBmdW5jdGlvbihuZXdDYWxsYmFjaykge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgY2FsY3VsYXRpb25zID0gbW9kdWxlLmdldC5lbGVtZW50Q2FsY3VsYXRpb25zKCksXG4gICAgICAgICAgICBjYWxsYmFjayAgICAgPSBuZXdDYWxsYmFjayB8fCBzZXR0aW5ncy5vblRvcFBhc3NlZFJldmVyc2UsXG4gICAgICAgICAgICBjYWxsYmFja05hbWUgPSAndG9wUGFzc2VkUmV2ZXJzZSdcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYobmV3Q2FsbGJhY2spIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWRkaW5nIGNhbGxiYWNrIGZvciB0b3AgcGFzc2VkIHJldmVyc2UnLCBuZXdDYWxsYmFjayk7XG4gICAgICAgICAgICBzZXR0aW5ncy5vblRvcFBhc3NlZFJldmVyc2UgPSBuZXdDYWxsYmFjaztcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoIWNhbGN1bGF0aW9ucy50b3BQYXNzZWQpIHtcbiAgICAgICAgICAgIGlmKG1vZHVsZS5nZXQub2NjdXJyZWQoJ3RvcFBhc3NlZCcpKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5leGVjdXRlKGNhbGxiYWNrLCBjYWxsYmFja05hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKCFzZXR0aW5ncy5vbmNlKSB7XG4gICAgICAgICAgICBtb2R1bGUucmVtb3ZlLm9jY3VycmVkKGNhbGxiYWNrTmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKG5ld0NhbGxiYWNrID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybiAhY2FsY3VsYXRpb25zLm9uVG9wUGFzc2VkO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBib3R0b21QYXNzZWRSZXZlcnNlOiBmdW5jdGlvbihuZXdDYWxsYmFjaykge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgY2FsY3VsYXRpb25zID0gbW9kdWxlLmdldC5lbGVtZW50Q2FsY3VsYXRpb25zKCksXG4gICAgICAgICAgICBjYWxsYmFjayAgICAgPSBuZXdDYWxsYmFjayB8fCBzZXR0aW5ncy5vbkJvdHRvbVBhc3NlZFJldmVyc2UsXG4gICAgICAgICAgICBjYWxsYmFja05hbWUgPSAnYm90dG9tUGFzc2VkUmV2ZXJzZSdcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYobmV3Q2FsbGJhY2spIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWRkaW5nIGNhbGxiYWNrIGZvciBib3R0b20gcGFzc2VkIHJldmVyc2UnLCBuZXdDYWxsYmFjayk7XG4gICAgICAgICAgICBzZXR0aW5ncy5vbkJvdHRvbVBhc3NlZFJldmVyc2UgPSBuZXdDYWxsYmFjaztcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoIWNhbGN1bGF0aW9ucy5ib3R0b21QYXNzZWQpIHtcbiAgICAgICAgICAgIGlmKG1vZHVsZS5nZXQub2NjdXJyZWQoJ2JvdHRvbVBhc3NlZCcpKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5leGVjdXRlKGNhbGxiYWNrLCBjYWxsYmFja05hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKCFzZXR0aW5ncy5vbmNlKSB7XG4gICAgICAgICAgICBtb2R1bGUucmVtb3ZlLm9jY3VycmVkKGNhbGxiYWNrTmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKG5ld0NhbGxiYWNrID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybiAhY2FsY3VsYXRpb25zLmJvdHRvbVBhc3NlZDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZXhlY3V0ZTogZnVuY3Rpb24oY2FsbGJhY2ssIGNhbGxiYWNrTmFtZSkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgY2FsY3VsYXRpb25zID0gbW9kdWxlLmdldC5lbGVtZW50Q2FsY3VsYXRpb25zKCksXG4gICAgICAgICAgICBzY3JlZW4gICAgICAgPSBtb2R1bGUuZ2V0LnNjcmVlbkNhbGN1bGF0aW9ucygpXG4gICAgICAgICAgO1xuICAgICAgICAgIGNhbGxiYWNrID0gY2FsbGJhY2sgfHwgZmFsc2U7XG4gICAgICAgICAgaWYoY2FsbGJhY2spIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmNvbnRpbnVvdXMpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDYWxsYmFjayBiZWluZyBjYWxsZWQgY29udGludW91c2x5JywgY2FsbGJhY2tOYW1lLCBjYWxjdWxhdGlvbnMpO1xuICAgICAgICAgICAgICBjYWxsYmFjay5jYWxsKGVsZW1lbnQsIGNhbGN1bGF0aW9ucywgc2NyZWVuKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoIW1vZHVsZS5nZXQub2NjdXJyZWQoY2FsbGJhY2tOYW1lKSkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NvbmRpdGlvbnMgbWV0JywgY2FsbGJhY2tOYW1lLCBjYWxjdWxhdGlvbnMpO1xuICAgICAgICAgICAgICBjYWxsYmFjay5jYWxsKGVsZW1lbnQsIGNhbGN1bGF0aW9ucywgc2NyZWVuKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgbW9kdWxlLnNhdmUub2NjdXJyZWQoY2FsbGJhY2tOYW1lKTtcbiAgICAgICAgfSxcblxuICAgICAgICByZW1vdmU6IHtcbiAgICAgICAgICBmaXhlZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1JlbW92aW5nIGZpeGVkIHBvc2l0aW9uJyk7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuZml4ZWQpXG4gICAgICAgICAgICAgIC5jc3Moe1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uIDogJycsXG4gICAgICAgICAgICAgICAgdG9wICAgICAgOiAnJyxcbiAgICAgICAgICAgICAgICBsZWZ0ICAgICA6ICcnLFxuICAgICAgICAgICAgICAgIHpJbmRleCAgIDogJydcbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHNldHRpbmdzLm9uVW5maXhlZC5jYWxsKGVsZW1lbnQpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcGxhY2Vob2xkZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZW1vdmluZyBwbGFjZWhvbGRlciBjb250ZW50Jyk7XG4gICAgICAgICAgICBpZigkcGxhY2Vob2xkZXIpIHtcbiAgICAgICAgICAgICAgJHBsYWNlaG9sZGVyLnJlbW92ZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgb2NjdXJyZWQ6IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG4gICAgICAgICAgICBpZihjYWxsYmFjaykge1xuICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICBvY2N1cnJlZCA9IG1vZHVsZS5jYWNoZS5vY2N1cnJlZFxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKG9jY3VycmVkW2NhbGxiYWNrXSAhPT0gdW5kZWZpbmVkICYmIG9jY3VycmVkW2NhbGxiYWNrXSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ2FsbGJhY2sgY2FuIG5vdyBiZSBjYWxsZWQgYWdhaW4nLCBjYWxsYmFjayk7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmNhY2hlLm9jY3VycmVkW2NhbGxiYWNrXSA9IGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmNhY2hlLm9jY3VycmVkID0ge307XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNhdmU6IHtcbiAgICAgICAgICBjYWxjdWxhdGlvbnM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1NhdmluZyBhbGwgY2FsY3VsYXRpb25zIG5lY2Vzc2FyeSB0byBkZXRlcm1pbmUgcG9zaXRpb25pbmcnKTtcbiAgICAgICAgICAgIG1vZHVsZS5zYXZlLmRpcmVjdGlvbigpO1xuICAgICAgICAgICAgbW9kdWxlLnNhdmUuc2NyZWVuQ2FsY3VsYXRpb25zKCk7XG4gICAgICAgICAgICBtb2R1bGUuc2F2ZS5lbGVtZW50Q2FsY3VsYXRpb25zKCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBvY2N1cnJlZDogZnVuY3Rpb24oY2FsbGJhY2spIHtcbiAgICAgICAgICAgIGlmKGNhbGxiYWNrKSB7XG4gICAgICAgICAgICAgIGlmKG1vZHVsZS5jYWNoZS5vY2N1cnJlZFtjYWxsYmFja10gPT09IHVuZGVmaW5lZCB8fCAobW9kdWxlLmNhY2hlLm9jY3VycmVkW2NhbGxiYWNrXSAhPT0gdHJ1ZSkpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU2F2aW5nIGNhbGxiYWNrIG9jY3VycmVkJywgY2FsbGJhY2spO1xuICAgICAgICAgICAgICAgIG1vZHVsZS5jYWNoZS5vY2N1cnJlZFtjYWxsYmFja10gPSB0cnVlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzY3JvbGw6IGZ1bmN0aW9uKHNjcm9sbFBvc2l0aW9uKSB7XG4gICAgICAgICAgICBzY3JvbGxQb3NpdGlvbiAgICAgID0gc2Nyb2xsUG9zaXRpb24gKyBzZXR0aW5ncy5vZmZzZXQgfHwgJGNvbnRleHQuc2Nyb2xsVG9wKCkgKyBzZXR0aW5ncy5vZmZzZXQ7XG4gICAgICAgICAgICBtb2R1bGUuY2FjaGUuc2Nyb2xsID0gc2Nyb2xsUG9zaXRpb247XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXJlY3Rpb246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHNjcm9sbCAgICAgPSBtb2R1bGUuZ2V0LnNjcm9sbCgpLFxuICAgICAgICAgICAgICBsYXN0U2Nyb2xsID0gbW9kdWxlLmdldC5sYXN0U2Nyb2xsKCksXG4gICAgICAgICAgICAgIGRpcmVjdGlvblxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoc2Nyb2xsID4gbGFzdFNjcm9sbCAmJiBsYXN0U2Nyb2xsKSB7XG4gICAgICAgICAgICAgIGRpcmVjdGlvbiA9ICdkb3duJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoc2Nyb2xsIDwgbGFzdFNjcm9sbCAmJiBsYXN0U2Nyb2xsKSB7XG4gICAgICAgICAgICAgIGRpcmVjdGlvbiA9ICd1cCc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgZGlyZWN0aW9uID0gJ3N0YXRpYyc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUuY2FjaGUuZGlyZWN0aW9uID0gZGlyZWN0aW9uO1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZS5jYWNoZS5kaXJlY3Rpb247XG4gICAgICAgICAgfSxcbiAgICAgICAgICBlbGVtZW50UG9zaXRpb246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGVsZW1lbnQgPSBtb2R1bGUuY2FjaGUuZWxlbWVudCxcbiAgICAgICAgICAgICAgc2NyZWVuICA9IG1vZHVsZS5nZXQuc2NyZWVuU2l6ZSgpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU2F2aW5nIGVsZW1lbnQgcG9zaXRpb24nKTtcbiAgICAgICAgICAgIC8vIChxdWlja2VyIHRoYW4gJC5leHRlbmQpXG4gICAgICAgICAgICBlbGVtZW50LmZpdHMgICAgICAgICAgPSAoZWxlbWVudC5oZWlnaHQgPCBzY3JlZW4uaGVpZ2h0KTtcbiAgICAgICAgICAgIGVsZW1lbnQub2Zmc2V0ICAgICAgICA9ICRtb2R1bGUub2Zmc2V0KCk7XG4gICAgICAgICAgICBlbGVtZW50LndpZHRoICAgICAgICAgPSAkbW9kdWxlLm91dGVyV2lkdGgoKTtcbiAgICAgICAgICAgIGVsZW1lbnQuaGVpZ2h0ICAgICAgICA9ICRtb2R1bGUub3V0ZXJIZWlnaHQoKTtcbiAgICAgICAgICAgIC8vIHN0b3JlXG4gICAgICAgICAgICBtb2R1bGUuY2FjaGUuZWxlbWVudCA9IGVsZW1lbnQ7XG4gICAgICAgICAgICByZXR1cm4gZWxlbWVudDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGVsZW1lbnRDYWxjdWxhdGlvbnM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHNjcmVlbiAgICAgPSBtb2R1bGUuZ2V0LnNjcmVlbkNhbGN1bGF0aW9ucygpLFxuICAgICAgICAgICAgICBlbGVtZW50ICAgID0gbW9kdWxlLmdldC5lbGVtZW50UG9zaXRpb24oKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgLy8gb2Zmc2V0XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5pbmNsdWRlTWFyZ2luKSB7XG4gICAgICAgICAgICAgIGVsZW1lbnQubWFyZ2luICAgICAgICA9IHt9O1xuICAgICAgICAgICAgICBlbGVtZW50Lm1hcmdpbi50b3AgICAgPSBwYXJzZUludCgkbW9kdWxlLmNzcygnbWFyZ2luLXRvcCcpLCAxMCk7XG4gICAgICAgICAgICAgIGVsZW1lbnQubWFyZ2luLmJvdHRvbSA9IHBhcnNlSW50KCRtb2R1bGUuY3NzKCdtYXJnaW4tYm90dG9tJyksIDEwKTtcbiAgICAgICAgICAgICAgZWxlbWVudC50b3AgICAgPSBlbGVtZW50Lm9mZnNldC50b3AgLSBlbGVtZW50Lm1hcmdpbi50b3A7XG4gICAgICAgICAgICAgIGVsZW1lbnQuYm90dG9tID0gZWxlbWVudC5vZmZzZXQudG9wICsgZWxlbWVudC5oZWlnaHQgKyBlbGVtZW50Lm1hcmdpbi5ib3R0b207XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgZWxlbWVudC50b3AgICAgPSBlbGVtZW50Lm9mZnNldC50b3A7XG4gICAgICAgICAgICAgIGVsZW1lbnQuYm90dG9tID0gZWxlbWVudC5vZmZzZXQudG9wICsgZWxlbWVudC5oZWlnaHQ7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIHZpc2liaWxpdHlcbiAgICAgICAgICAgIGVsZW1lbnQudG9wVmlzaWJsZSAgICAgICA9IChzY3JlZW4uYm90dG9tID49IGVsZW1lbnQudG9wKTtcbiAgICAgICAgICAgIGVsZW1lbnQudG9wUGFzc2VkICAgICAgICA9IChzY3JlZW4udG9wID49IGVsZW1lbnQudG9wKTtcbiAgICAgICAgICAgIGVsZW1lbnQuYm90dG9tVmlzaWJsZSAgICA9IChzY3JlZW4uYm90dG9tID49IGVsZW1lbnQuYm90dG9tKTtcbiAgICAgICAgICAgIGVsZW1lbnQuYm90dG9tUGFzc2VkICAgICA9IChzY3JlZW4udG9wID49IGVsZW1lbnQuYm90dG9tKTtcbiAgICAgICAgICAgIGVsZW1lbnQucGl4ZWxzUGFzc2VkICAgICA9IDA7XG4gICAgICAgICAgICBlbGVtZW50LnBlcmNlbnRhZ2VQYXNzZWQgPSAwO1xuXG4gICAgICAgICAgICAvLyBtZXRhIGNhbGN1bGF0aW9uc1xuICAgICAgICAgICAgZWxlbWVudC5vblNjcmVlbiAgPSAoZWxlbWVudC50b3BWaXNpYmxlICYmICFlbGVtZW50LmJvdHRvbVBhc3NlZCk7XG4gICAgICAgICAgICBlbGVtZW50LnBhc3NpbmcgICA9IChlbGVtZW50LnRvcFBhc3NlZCAmJiAhZWxlbWVudC5ib3R0b21QYXNzZWQpO1xuICAgICAgICAgICAgZWxlbWVudC5vZmZTY3JlZW4gPSAoIWVsZW1lbnQub25TY3JlZW4pO1xuXG4gICAgICAgICAgICAvLyBwYXNzaW5nIGNhbGN1bGF0aW9uc1xuICAgICAgICAgICAgaWYoZWxlbWVudC5wYXNzaW5nKSB7XG4gICAgICAgICAgICAgIGVsZW1lbnQucGl4ZWxzUGFzc2VkICAgICA9IChzY3JlZW4udG9wIC0gZWxlbWVudC50b3ApO1xuICAgICAgICAgICAgICBlbGVtZW50LnBlcmNlbnRhZ2VQYXNzZWQgPSAoc2NyZWVuLnRvcCAtIGVsZW1lbnQudG9wKSAvIGVsZW1lbnQuaGVpZ2h0O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLmNhY2hlLmVsZW1lbnQgPSBlbGVtZW50O1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1VwZGF0ZWQgZWxlbWVudCBjYWxjdWxhdGlvbnMnLCBlbGVtZW50KTtcbiAgICAgICAgICAgIHJldHVybiBlbGVtZW50O1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc2NyZWVuQ2FsY3VsYXRpb25zOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBzY3JvbGwgPSBtb2R1bGUuZ2V0LnNjcm9sbCgpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBtb2R1bGUuc2F2ZS5kaXJlY3Rpb24oKTtcbiAgICAgICAgICAgIG1vZHVsZS5jYWNoZS5zY3JlZW4udG9wICAgID0gc2Nyb2xsO1xuICAgICAgICAgICAgbW9kdWxlLmNhY2hlLnNjcmVlbi5ib3R0b20gPSBzY3JvbGwgKyBtb2R1bGUuY2FjaGUuc2NyZWVuLmhlaWdodDtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGUuY2FjaGUuc2NyZWVuO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc2NyZWVuU2l6ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU2F2aW5nIHdpbmRvdyBwb3NpdGlvbicpO1xuICAgICAgICAgICAgbW9kdWxlLmNhY2hlLnNjcmVlbiA9IHtcbiAgICAgICAgICAgICAgaGVpZ2h0OiAkY29udGV4dC5oZWlnaHQoKVxuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHBvc2l0aW9uOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS5zYXZlLnNjcmVlblNpemUoKTtcbiAgICAgICAgICAgIG1vZHVsZS5zYXZlLmVsZW1lbnRQb3NpdGlvbigpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBnZXQ6IHtcbiAgICAgICAgICBwaXhlbHNQYXNzZWQ6IGZ1bmN0aW9uKGFtb3VudCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGVsZW1lbnQgPSBtb2R1bGUuZ2V0LmVsZW1lbnRDYWxjdWxhdGlvbnMoKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoYW1vdW50LnNlYXJjaCgnJScpID4gLTEpIHtcbiAgICAgICAgICAgICAgcmV0dXJuICggZWxlbWVudC5oZWlnaHQgKiAocGFyc2VJbnQoYW1vdW50LCAxMCkgLyAxMDApICk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gcGFyc2VJbnQoYW1vdW50LCAxMCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBvY2N1cnJlZDogZnVuY3Rpb24oY2FsbGJhY2spIHtcbiAgICAgICAgICAgIHJldHVybiAobW9kdWxlLmNhY2hlLm9jY3VycmVkICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgID8gbW9kdWxlLmNhY2hlLm9jY3VycmVkW2NhbGxiYWNrXSB8fCBmYWxzZVxuICAgICAgICAgICAgICA6IGZhbHNlXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXJlY3Rpb246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmNhY2hlLmRpcmVjdGlvbiA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5zYXZlLmRpcmVjdGlvbigpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZS5jYWNoZS5kaXJlY3Rpb247XG4gICAgICAgICAgfSxcbiAgICAgICAgICBlbGVtZW50UG9zaXRpb246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmNhY2hlLmVsZW1lbnQgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICBtb2R1bGUuc2F2ZS5lbGVtZW50UG9zaXRpb24oKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBtb2R1bGUuY2FjaGUuZWxlbWVudDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGVsZW1lbnRDYWxjdWxhdGlvbnM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmNhY2hlLmVsZW1lbnQgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICBtb2R1bGUuc2F2ZS5lbGVtZW50Q2FsY3VsYXRpb25zKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlLmNhY2hlLmVsZW1lbnQ7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzY3JlZW5DYWxjdWxhdGlvbnM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmNhY2hlLnNjcmVlbiA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5zYXZlLnNjcmVlbkNhbGN1bGF0aW9ucygpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZS5jYWNoZS5zY3JlZW47XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzY3JlZW5TaXplOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKG1vZHVsZS5jYWNoZS5zY3JlZW4gPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICBtb2R1bGUuc2F2ZS5zY3JlZW5TaXplKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlLmNhY2hlLnNjcmVlbjtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHNjcm9sbDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihtb2R1bGUuY2FjaGUuc2Nyb2xsID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnNhdmUuc2Nyb2xsKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlLmNhY2hlLnNjcm9sbDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGxhc3RTY3JvbGw6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYobW9kdWxlLmNhY2hlLnNjcmVlbiA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRmlyc3Qgc2Nyb2xsIGV2ZW50LCBubyBsYXN0IHNjcm9sbCBjb3VsZCBiZSBmb3VuZCcpO1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlLmNhY2hlLnNjcmVlbi50b3A7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldHRpbmc6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChuYW1lKSApIHtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIHNldHRpbmdzLCBuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBzZXR0aW5nc1tuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBzZXR0aW5nc1tuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludGVybmFsOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBtb2R1bGUsIG5hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIG1vZHVsZVtuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGVbbmFtZV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBkZWJ1ZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy5kZWJ1Zykge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZyA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB2ZXJib3NlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50ICYmIHNldHRpbmdzLnZlcmJvc2UgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5pbmZvLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGVycm9yOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50KSB7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuZXJyb3IsIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBwZXJmb3JtYW5jZToge1xuICAgICAgICAgIGxvZzogZnVuY3Rpb24obWVzc2FnZSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lLFxuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lLFxuICAgICAgICAgICAgICBwcmV2aW91c1RpbWVcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIGN1cnJlbnRUaW1lICAgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lICA9IHRpbWUgfHwgY3VycmVudFRpbWU7XG4gICAgICAgICAgICAgIGV4ZWN1dGlvblRpbWUgPSBjdXJyZW50VGltZSAtIHByZXZpb3VzVGltZTtcbiAgICAgICAgICAgICAgdGltZSAgICAgICAgICA9IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBwZXJmb3JtYW5jZS5wdXNoKHtcbiAgICAgICAgICAgICAgICAnTmFtZScgICAgICAgICAgIDogbWVzc2FnZVswXSxcbiAgICAgICAgICAgICAgICAnQXJndW1lbnRzJyAgICAgIDogW10uc2xpY2UuY2FsbChtZXNzYWdlLCAxKSB8fCAnJyxcbiAgICAgICAgICAgICAgICAnRWxlbWVudCcgICAgICAgIDogZWxlbWVudCxcbiAgICAgICAgICAgICAgICAnRXhlY3V0aW9uIFRpbWUnIDogZXhlY3V0aW9uVGltZVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyID0gc2V0VGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UuZGlzcGxheSwgNTAwKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc3BsYXk6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHRpdGxlID0gc2V0dGluZ3MubmFtZSArICc6JyxcbiAgICAgICAgICAgICAgdG90YWxUaW1lID0gMFxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgdGltZSA9IGZhbHNlO1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lcik7XG4gICAgICAgICAgICAkLmVhY2gocGVyZm9ybWFuY2UsIGZ1bmN0aW9uKGluZGV4LCBkYXRhKSB7XG4gICAgICAgICAgICAgIHRvdGFsVGltZSArPSBkYXRhWydFeGVjdXRpb24gVGltZSddO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB0aXRsZSArPSAnICcgKyB0b3RhbFRpbWUgKyAnbXMnO1xuICAgICAgICAgICAgaWYobW9kdWxlU2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgdGl0bGUgKz0gJyBcXCcnICsgbW9kdWxlU2VsZWN0b3IgKyAnXFwnJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCAoY29uc29sZS5ncm91cCAhPT0gdW5kZWZpbmVkIHx8IGNvbnNvbGUudGFibGUgIT09IHVuZGVmaW5lZCkgJiYgcGVyZm9ybWFuY2UubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKHRpdGxlKTtcbiAgICAgICAgICAgICAgaWYoY29uc29sZS50YWJsZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUudGFibGUocGVyZm9ybWFuY2UpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGFbJ05hbWUnXSArICc6ICcgKyBkYXRhWydFeGVjdXRpb24gVGltZSddKydtcycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHBlcmZvcm1hbmNlID0gW107XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnZva2U6IGZ1bmN0aW9uKHF1ZXJ5LCBwYXNzZWRBcmd1bWVudHMsIGNvbnRleHQpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIG9iamVjdCA9IGluc3RhbmNlLFxuICAgICAgICAgICAgbWF4RGVwdGgsXG4gICAgICAgICAgICBmb3VuZCxcbiAgICAgICAgICAgIHJlc3BvbnNlXG4gICAgICAgICAgO1xuICAgICAgICAgIHBhc3NlZEFyZ3VtZW50cyA9IHBhc3NlZEFyZ3VtZW50cyB8fCBxdWVyeUFyZ3VtZW50cztcbiAgICAgICAgICBjb250ZXh0ICAgICAgICAgPSBlbGVtZW50ICAgICAgICAgfHwgY29udGV4dDtcbiAgICAgICAgICBpZih0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycgJiYgb2JqZWN0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5ICAgID0gcXVlcnkuc3BsaXQoL1tcXC4gXS8pO1xuICAgICAgICAgICAgbWF4RGVwdGggPSBxdWVyeS5sZW5ndGggLSAxO1xuICAgICAgICAgICAgJC5lYWNoKHF1ZXJ5LCBmdW5jdGlvbihkZXB0aCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgdmFyIGNhbWVsQ2FzZVZhbHVlID0gKGRlcHRoICE9IG1heERlcHRoKVxuICAgICAgICAgICAgICAgID8gdmFsdWUgKyBxdWVyeVtkZXB0aCArIDFdLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcXVlcnlbZGVwdGggKyAxXS5zbGljZSgxKVxuICAgICAgICAgICAgICAgIDogcXVlcnlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbY2FtZWxDYXNlVmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoICQuaXNQbGFpbk9iamVjdCggb2JqZWN0W3ZhbHVlXSApICYmIChkZXB0aCAhPSBtYXhEZXB0aCkgKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBvYmplY3RbdmFsdWVdICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3IubWV0aG9kLCBxdWVyeSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCAkLmlzRnVuY3Rpb24oIGZvdW5kICkgKSB7XG4gICAgICAgICAgICByZXNwb25zZSA9IGZvdW5kLmFwcGx5KGNvbnRleHQsIHBhc3NlZEFyZ3VtZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYoZm91bmQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZDtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYoJC5pc0FycmF5KHJldHVybmVkVmFsdWUpKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlLnB1c2gocmVzcG9uc2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IFtyZXR1cm5lZFZhbHVlLCByZXNwb25zZV07XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYocmVzcG9uc2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZSA9IHJlc3BvbnNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gZm91bmQ7XG4gICAgICAgIH1cbiAgICAgIH07XG5cbiAgICAgIGlmKG1ldGhvZEludm9rZWQpIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICAgIH1cbiAgICAgICAgaW5zdGFuY2Uuc2F2ZS5zY3JvbGwoKTtcbiAgICAgICAgaW5zdGFuY2Uuc2F2ZS5jYWxjdWxhdGlvbnMoKTtcbiAgICAgICAgbW9kdWxlLmludm9rZShxdWVyeSk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIGluc3RhbmNlLmludm9rZSgnZGVzdHJveScpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICB9XG4gICAgfSlcbiAgO1xuXG4gIHJldHVybiAocmV0dXJuZWRWYWx1ZSAhPT0gdW5kZWZpbmVkKVxuICAgID8gcmV0dXJuZWRWYWx1ZVxuICAgIDogdGhpc1xuICA7XG59O1xuXG4kLmZuLnZpc2liaWxpdHkuc2V0dGluZ3MgPSB7XG5cbiAgbmFtZSAgICAgICAgICAgICAgICAgICA6ICdWaXNpYmlsaXR5JyxcbiAgbmFtZXNwYWNlICAgICAgICAgICAgICA6ICd2aXNpYmlsaXR5JyxcblxuICBkZWJ1ZyAgICAgICAgICAgICAgICAgIDogZmFsc2UsXG4gIHZlcmJvc2UgICAgICAgICAgICAgICAgOiBmYWxzZSxcbiAgcGVyZm9ybWFuY2UgICAgICAgICAgICA6IHRydWUsXG5cbiAgLy8gd2hldGhlciB0byB1c2UgbXV0YXRpb24gb2JzZXJ2ZXJzIHRvIGZvbGxvdyBjaGFuZ2VzXG4gIG9ic2VydmVDaGFuZ2VzICAgICAgICAgOiB0cnVlLFxuXG4gIC8vIGNoZWNrIHBvc2l0aW9uIGltbWVkaWF0ZWx5IG9uIGluaXRcbiAgaW5pdGlhbENoZWNrICAgICAgICAgICA6IHRydWUsXG5cbiAgLy8gd2hldGhlciB0byByZWZyZXNoIGNhbGN1bGF0aW9ucyBhZnRlciBhbGwgcGFnZSBpbWFnZXMgbG9hZFxuICByZWZyZXNoT25Mb2FkICAgICAgICAgIDogdHJ1ZSxcblxuICAvLyB3aGV0aGVyIHRvIHJlZnJlc2ggY2FsY3VsYXRpb25zIGFmdGVyIHBhZ2UgcmVzaXplIGV2ZW50XG4gIHJlZnJlc2hPblJlc2l6ZSAgICAgICAgOiB0cnVlLFxuXG4gIC8vIHNob3VsZCBjYWxsIGNhbGxiYWNrcyBvbiByZWZyZXNoIGV2ZW50IChyZXNpemUsIGV0YylcbiAgY2hlY2tPblJlZnJlc2ggICAgICAgICA6IHRydWUsXG5cbiAgLy8gY2FsbGJhY2sgc2hvdWxkIG9ubHkgb2NjdXIgb25lIHRpbWVcbiAgb25jZSAgICAgICAgICAgICAgICAgICA6IHRydWUsXG5cbiAgLy8gY2FsbGJhY2sgc2hvdWxkIGZpcmUgY29udGludW91c2x5IHdoZSBldmFsdWF0ZXMgdG8gdHJ1ZVxuICBjb250aW51b3VzICAgICAgICAgICAgIDogZmFsc2UsXG5cbiAgLy8gb2Zmc2V0IHRvIHVzZSB3aXRoIHNjcm9sbCB0b3BcbiAgb2Zmc2V0ICAgICAgICAgICAgICAgICA6IDAsXG5cbiAgLy8gd2hldGhlciB0byBpbmNsdWRlIG1hcmdpbiBpbiBlbGVtZW50cyBwb3NpdGlvblxuICBpbmNsdWRlTWFyZ2luICAgICAgICAgIDogZmFsc2UsXG5cbiAgLy8gc2Nyb2xsIGNvbnRleHQgZm9yIHZpc2liaWxpdHkgY2hlY2tzXG4gIGNvbnRleHQgICAgICAgICAgICAgICAgOiB3aW5kb3csXG5cbiAgLy8gdmlzaWJpbGl0eSBjaGVjayBkZWxheSBpbiBtcyAoZGVmYXVsdHMgdG8gYW5pbWF0aW9uRnJhbWUpXG4gIHRocm90dGxlICAgICAgICAgICAgICAgOiBmYWxzZSxcblxuICAvLyBzcGVjaWFsIHZpc2liaWxpdHkgdHlwZSAoaW1hZ2UsIGZpeGVkKVxuICB0eXBlICAgICAgICAgICAgICAgICAgIDogZmFsc2UsXG5cbiAgLy8gei1pbmRleCB0byB1c2Ugd2l0aCB2aXNpYmlsaXR5ICdmaXhlZCdcbiAgekluZGV4ICAgICAgICAgICAgICAgICA6ICcxMCcsXG5cbiAgLy8gaW1hZ2Ugb25seSBhbmltYXRpb24gc2V0dGluZ3NcbiAgdHJhbnNpdGlvbiAgICAgICAgICAgICA6ICdmYWRlIGluJyxcbiAgZHVyYXRpb24gICAgICAgICAgICAgICA6IDEwMDAsXG5cbiAgLy8gYXJyYXkgb2YgY2FsbGJhY2tzIGZvciBwZXJjZW50YWdlXG4gIG9uUGFzc2VkICAgICAgICAgICAgICAgOiB7fSxcblxuICAvLyBzdGFuZGFyZCBjYWxsYmFja3NcbiAgb25PblNjcmVlbiAgICAgICAgICAgICA6IGZhbHNlLFxuICBvbk9mZlNjcmVlbiAgICAgICAgICAgIDogZmFsc2UsXG4gIG9uUGFzc2luZyAgICAgICAgICAgICAgOiBmYWxzZSxcbiAgb25Ub3BWaXNpYmxlICAgICAgICAgICA6IGZhbHNlLFxuICBvbkJvdHRvbVZpc2libGUgICAgICAgIDogZmFsc2UsXG4gIG9uVG9wUGFzc2VkICAgICAgICAgICAgOiBmYWxzZSxcbiAgb25Cb3R0b21QYXNzZWQgICAgICAgICA6IGZhbHNlLFxuXG4gIC8vIHJldmVyc2UgY2FsbGJhY2tzXG4gIG9uUGFzc2luZ1JldmVyc2UgICAgICAgOiBmYWxzZSxcbiAgb25Ub3BWaXNpYmxlUmV2ZXJzZSAgICA6IGZhbHNlLFxuICBvbkJvdHRvbVZpc2libGVSZXZlcnNlIDogZmFsc2UsXG4gIG9uVG9wUGFzc2VkUmV2ZXJzZSAgICAgOiBmYWxzZSxcbiAgb25Cb3R0b21QYXNzZWRSZXZlcnNlICA6IGZhbHNlLFxuXG4gIC8vIHNwZWNpYWwgY2FsbGJhY2tzIGZvciBpbWFnZVxuICBvbkxvYWQgICAgICAgICAgICAgICAgIDogZnVuY3Rpb24oKSB7fSxcbiAgb25BbGxMb2FkZWQgICAgICAgICAgICA6IGZ1bmN0aW9uKCkge30sXG5cbiAgLy8gc3BlY2lhbCBjYWxsYmFja3MgZm9yIGZpeGVkIHBvc2l0aW9uXG4gIG9uRml4ZWQgICAgICAgICAgICAgICAgOiBmdW5jdGlvbigpIHt9LFxuICBvblVuZml4ZWQgICAgICAgICAgICAgIDogZnVuY3Rpb24oKSB7fSxcblxuICAvLyB1dGlsaXR5IGNhbGxiYWNrc1xuICBvblVwZGF0ZSAgICAgICAgICAgICAgIDogZmFsc2UsIC8vIGRpc2FibGVkIGJ5IGRlZmF1bHQgZm9yIHBlcmZvcm1hbmNlXG4gIG9uUmVmcmVzaCAgICAgICAgICAgICAgOiBmdW5jdGlvbigpe30sXG5cbiAgbWV0YWRhdGEgOiB7XG4gICAgc3JjOiAnc3JjJ1xuICB9LFxuXG4gIGNsYXNzTmFtZToge1xuICAgIGZpeGVkICAgICAgIDogJ2ZpeGVkJyxcbiAgICBwbGFjZWhvbGRlciA6ICdwbGFjZWhvbGRlcidcbiAgfSxcblxuICBlcnJvciA6IHtcbiAgICBtZXRob2QgIDogJ1RoZSBtZXRob2QgeW91IGNhbGxlZCBpcyBub3QgZGVmaW5lZC4nLFxuICAgIHZpc2libGUgOiAnRWxlbWVudCBpcyBoaWRkZW4sIHlvdSBtdXN0IGNhbGwgcmVmcmVzaCBhZnRlciBlbGVtZW50IGJlY29tZXMgdmlzaWJsZSdcbiAgfVxuXG59O1xuXG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7XG4iXX0=