/*!
 * # Semantic UI - State
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.state = function (parameters) {
    var $allModules = $(this),
        moduleSelector = $allModules.selector || '',
        hasTouch = 'ontouchstart' in document.documentElement,
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue;
    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.state.settings, parameters) : $.extend({}, $.fn.state.settings),
          error = settings.error,
          metadata = settings.metadata,
          className = settings.className,
          namespace = settings.namespace,
          states = settings.states,
          text = settings.text,
          eventNamespace = '.' + namespace,
          moduleNamespace = namespace + '-module',
          $module = $(this),
          element = this,
          instance = $module.data(moduleNamespace),
          module;
      module = {

        initialize: function () {
          module.verbose('Initializing module');

          // allow module to guess desired state based on element
          if (settings.automatic) {
            module.add.defaults();
          }

          // bind events with delegated events
          if (settings.context && moduleSelector !== '') {
            $(settings.context).on(moduleSelector, 'mouseenter' + eventNamespace, module.change.text).on(moduleSelector, 'mouseleave' + eventNamespace, module.reset.text).on(moduleSelector, 'click' + eventNamespace, module.toggle.state);
          } else {
            $module.on('mouseenter' + eventNamespace, module.change.text).on('mouseleave' + eventNamespace, module.reset.text).on('click' + eventNamespace, module.toggle.state);
          }
          module.instantiate();
        },

        instantiate: function () {
          module.verbose('Storing instance of module', module);
          instance = module;
          $module.data(moduleNamespace, module);
        },

        destroy: function () {
          module.verbose('Destroying previous module', instance);
          $module.off(eventNamespace).removeData(moduleNamespace);
        },

        refresh: function () {
          module.verbose('Refreshing selector cache');
          $module = $(element);
        },

        add: {
          defaults: function () {
            var userStates = parameters && $.isPlainObject(parameters.states) ? parameters.states : {};
            $.each(settings.defaults, function (type, typeStates) {
              if (module.is[type] !== undefined && module.is[type]()) {
                module.verbose('Adding default states', type, element);
                $.extend(settings.states, typeStates, userStates);
              }
            });
          }
        },

        is: {

          active: function () {
            return $module.hasClass(className.active);
          },
          loading: function () {
            return $module.hasClass(className.loading);
          },
          inactive: function () {
            return !$module.hasClass(className.active);
          },
          state: function (state) {
            if (className[state] === undefined) {
              return false;
            }
            return $module.hasClass(className[state]);
          },

          enabled: function () {
            return !$module.is(settings.filter.active);
          },
          disabled: function () {
            return $module.is(settings.filter.active);
          },
          textEnabled: function () {
            return !$module.is(settings.filter.text);
          },

          // definitions for automatic type detection
          button: function () {
            return $module.is('.button:not(a, .submit)');
          },
          input: function () {
            return $module.is('input');
          },
          progress: function () {
            return $module.is('.ui.progress');
          }
        },

        allow: function (state) {
          module.debug('Now allowing state', state);
          states[state] = true;
        },
        disallow: function (state) {
          module.debug('No longer allowing', state);
          states[state] = false;
        },

        allows: function (state) {
          return states[state] || false;
        },

        enable: function () {
          $module.removeClass(className.disabled);
        },

        disable: function () {
          $module.addClass(className.disabled);
        },

        setState: function (state) {
          if (module.allows(state)) {
            $module.addClass(className[state]);
          }
        },

        removeState: function (state) {
          if (module.allows(state)) {
            $module.removeClass(className[state]);
          }
        },

        toggle: {
          state: function () {
            var apiRequest, requestCancelled;
            if (module.allows('active') && module.is.enabled()) {
              module.refresh();
              if ($.fn.api !== undefined) {
                apiRequest = $module.api('get request');
                requestCancelled = $module.api('was cancelled');
                if (requestCancelled) {
                  module.debug('API Request cancelled by beforesend');
                  settings.activateTest = function () {
                    return false;
                  };
                  settings.deactivateTest = function () {
                    return false;
                  };
                } else if (apiRequest) {
                  module.listenTo(apiRequest);
                  return;
                }
              }
              module.change.state();
            }
          }
        },

        listenTo: function (apiRequest) {
          module.debug('API request detected, waiting for state signal', apiRequest);
          if (apiRequest) {
            if (text.loading) {
              module.update.text(text.loading);
            }
            $.when(apiRequest).then(function () {
              if (apiRequest.state() == 'resolved') {
                module.debug('API request succeeded');
                settings.activateTest = function () {
                  return true;
                };
                settings.deactivateTest = function () {
                  return true;
                };
              } else {
                module.debug('API request failed');
                settings.activateTest = function () {
                  return false;
                };
                settings.deactivateTest = function () {
                  return false;
                };
              }
              module.change.state();
            });
          }
        },

        // checks whether active/inactive state can be given
        change: {

          state: function () {
            module.debug('Determining state change direction');
            // inactive to active change
            if (module.is.inactive()) {
              module.activate();
            } else {
              module.deactivate();
            }
            if (settings.sync) {
              module.sync();
            }
            settings.onChange.call(element);
          },

          text: function () {
            if (module.is.textEnabled()) {
              if (module.is.disabled()) {
                module.verbose('Changing text to disabled text', text.hover);
                module.update.text(text.disabled);
              } else if (module.is.active()) {
                if (text.hover) {
                  module.verbose('Changing text to hover text', text.hover);
                  module.update.text(text.hover);
                } else if (text.deactivate) {
                  module.verbose('Changing text to deactivating text', text.deactivate);
                  module.update.text(text.deactivate);
                }
              } else {
                if (text.hover) {
                  module.verbose('Changing text to hover text', text.hover);
                  module.update.text(text.hover);
                } else if (text.activate) {
                  module.verbose('Changing text to activating text', text.activate);
                  module.update.text(text.activate);
                }
              }
            }
          }

        },

        activate: function () {
          if (settings.activateTest.call(element)) {
            module.debug('Setting state to active');
            $module.addClass(className.active);
            module.update.text(text.active);
            settings.onActivate.call(element);
          }
        },

        deactivate: function () {
          if (settings.deactivateTest.call(element)) {
            module.debug('Setting state to inactive');
            $module.removeClass(className.active);
            module.update.text(text.inactive);
            settings.onDeactivate.call(element);
          }
        },

        sync: function () {
          module.verbose('Syncing other buttons to current state');
          if (module.is.active()) {
            $allModules.not($module).state('activate');
          } else {
            $allModules.not($module).state('deactivate');
          }
        },

        get: {
          text: function () {
            return settings.selector.text ? $module.find(settings.selector.text).text() : $module.html();
          },
          textFor: function (state) {
            return text[state] || false;
          }
        },

        flash: {
          text: function (text, duration, callback) {
            var previousText = module.get.text();
            module.debug('Flashing text message', text, duration);
            text = text || settings.text.flash;
            duration = duration || settings.flashDuration;
            callback = callback || function () {};
            module.update.text(text);
            setTimeout(function () {
              module.update.text(previousText);
              callback.call(element);
            }, duration);
          }
        },

        reset: {
          // on mouseout sets text to previous value
          text: function () {
            var activeText = text.active || $module.data(metadata.storedText),
                inactiveText = text.inactive || $module.data(metadata.storedText);
            if (module.is.textEnabled()) {
              if (module.is.active() && activeText) {
                module.verbose('Resetting active text', activeText);
                module.update.text(activeText);
              } else if (inactiveText) {
                module.verbose('Resetting inactive text', activeText);
                module.update.text(inactiveText);
              }
            }
          }
        },

        update: {
          text: function (text) {
            var currentText = module.get.text();
            if (text && text !== currentText) {
              module.debug('Updating text', text);
              if (settings.selector.text) {
                $module.data(metadata.storedText, text).find(settings.selector.text).text(text);
              } else {
                $module.data(metadata.storedText, text).html(text);
              }
            } else {
              module.debug('Text is already set, ignoring update', text);
            }
          }
        },

        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                module.error(error.method, query);
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };

      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.state.settings = {

    // module info
    name: 'State',

    // debug output
    debug: false,

    // verbose debug output
    verbose: false,

    // namespace for events
    namespace: 'state',

    // debug data includes performance
    performance: true,

    // callback occurs on state change
    onActivate: function () {},
    onDeactivate: function () {},
    onChange: function () {},

    // state test functions
    activateTest: function () {
      return true;
    },
    deactivateTest: function () {
      return true;
    },

    // whether to automatically map default states
    automatic: true,

    // activate / deactivate changes all elements instantiated at same time
    sync: false,

    // default flash text duration, used for temporarily changing text of an element
    flashDuration: 1000,

    // selector filter
    filter: {
      text: '.loading, .disabled',
      active: '.disabled'
    },

    context: false,

    // error
    error: {
      beforeSend: 'The before send function has cancelled state change',
      method: 'The method you called is not defined.'
    },

    // metadata
    metadata: {
      promise: 'promise',
      storedText: 'stored-text'
    },

    // change class on state
    className: {
      active: 'active',
      disabled: 'disabled',
      error: 'error',
      loading: 'loading',
      success: 'success',
      warning: 'warning'
    },

    selector: {
      // selector for text node
      text: false
    },

    defaults: {
      input: {
        disabled: true,
        loading: true,
        active: true
      },
      button: {
        disabled: true,
        loading: true,
        active: true
      },
      progress: {
        active: true,
        success: true,
        warning: true,
        error: true
      }
    },

    states: {
      active: true,
      disabled: true,
      error: true,
      loading: true,
      success: true,
      warning: true
    },

    text: {
      disabled: false,
      flash: false,
      hover: false,
      active: false,
      inactive: false,
      activate: false,
      deactivate: false
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9iZWhhdmlvcnMvc3RhdGUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQVVBLENBQUMsQ0FBQyxVQUFVLENBQVYsRUFBYSxNQUFiLEVBQXFCLFFBQXJCLEVBQStCLFNBQS9CLEVBQTBDOztBQUU1Qzs7QUFFQSxXQUFVLE9BQU8sTUFBUCxJQUFpQixXQUFqQixJQUFnQyxPQUFPLElBQVAsSUFBZSxJQUFoRCxHQUNMLE1BREssR0FFSixPQUFPLElBQVAsSUFBZSxXQUFmLElBQThCLEtBQUssSUFBTCxJQUFhLElBQTVDLEdBQ0UsSUFERixHQUVFLFNBQVMsYUFBVCxHQUpOOztBQU9BLElBQUUsRUFBRixDQUFLLEtBQUwsR0FBYSxVQUFTLFVBQVQsRUFBcUI7QUFDaEMsUUFDRSxjQUFrQixFQUFFLElBQUYsQ0FEcEI7QUFBQSxRQUdFLGlCQUFrQixZQUFZLFFBQVosSUFBd0IsRUFINUM7QUFBQSxRQUtFLFdBQW1CLGtCQUFrQixTQUFTLGVBTGhEO0FBQUEsUUFNRSxPQUFrQixJQUFJLElBQUosR0FBVyxPQUFYLEVBTnBCO0FBQUEsUUFPRSxjQUFrQixFQVBwQjtBQUFBLFFBU0UsUUFBa0IsVUFBVSxDQUFWLENBVHBCO0FBQUEsUUFVRSxnQkFBbUIsT0FBTyxLQUFQLElBQWdCLFFBVnJDO0FBQUEsUUFXRSxpQkFBa0IsR0FBRyxLQUFILENBQVMsSUFBVCxDQUFjLFNBQWQsRUFBeUIsQ0FBekIsQ0FYcEI7QUFBQSxRQWFFLGFBYkY7QUFlQSxnQkFDRyxJQURILENBQ1EsWUFBVztBQUNmLFVBQ0UsV0FBc0IsRUFBRSxhQUFGLENBQWdCLFVBQWhCLENBQUYsR0FDaEIsRUFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUIsRUFBRSxFQUFGLENBQUssS0FBTCxDQUFXLFFBQTlCLEVBQXdDLFVBQXhDLENBRGdCLEdBRWhCLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxFQUFFLEVBQUYsQ0FBSyxLQUFMLENBQVcsUUFBeEIsQ0FITjtBQUFBLFVBS0UsUUFBa0IsU0FBUyxLQUw3QjtBQUFBLFVBTUUsV0FBa0IsU0FBUyxRQU43QjtBQUFBLFVBT0UsWUFBa0IsU0FBUyxTQVA3QjtBQUFBLFVBUUUsWUFBa0IsU0FBUyxTQVI3QjtBQUFBLFVBU0UsU0FBa0IsU0FBUyxNQVQ3QjtBQUFBLFVBVUUsT0FBa0IsU0FBUyxJQVY3QjtBQUFBLFVBWUUsaUJBQWtCLE1BQU0sU0FaMUI7QUFBQSxVQWFFLGtCQUFrQixZQUFZLFNBYmhDO0FBQUEsVUFlRSxVQUFrQixFQUFFLElBQUYsQ0FmcEI7QUFBQSxVQWlCRSxVQUFrQixJQWpCcEI7QUFBQSxVQWtCRSxXQUFrQixRQUFRLElBQVIsQ0FBYSxlQUFiLENBbEJwQjtBQUFBLFVBb0JFLE1BcEJGO0FBc0JBLGVBQVM7O0FBRVAsb0JBQVksWUFBVztBQUNyQixpQkFBTyxPQUFQLENBQWUscUJBQWY7OztBQUdBLGNBQUcsU0FBUyxTQUFaLEVBQXVCO0FBQ3JCLG1CQUFPLEdBQVAsQ0FBVyxRQUFYO0FBQ0Q7OztBQUdELGNBQUcsU0FBUyxPQUFULElBQW9CLG1CQUFtQixFQUExQyxFQUE4QztBQUM1QyxjQUFFLFNBQVMsT0FBWCxFQUNHLEVBREgsQ0FDTSxjQUROLEVBQ3NCLGVBQWUsY0FEckMsRUFDcUQsT0FBTyxNQUFQLENBQWMsSUFEbkUsRUFFRyxFQUZILENBRU0sY0FGTixFQUVzQixlQUFlLGNBRnJDLEVBRXFELE9BQU8sS0FBUCxDQUFhLElBRmxFLEVBR0csRUFISCxDQUdNLGNBSE4sRUFHc0IsVUFBZSxjQUhyQyxFQUdxRCxPQUFPLE1BQVAsQ0FBYyxLQUhuRTtBQUtELFdBTkQsTUFPSztBQUNILG9CQUNHLEVBREgsQ0FDTSxlQUFlLGNBRHJCLEVBQ3FDLE9BQU8sTUFBUCxDQUFjLElBRG5ELEVBRUcsRUFGSCxDQUVNLGVBQWUsY0FGckIsRUFFcUMsT0FBTyxLQUFQLENBQWEsSUFGbEQsRUFHRyxFQUhILENBR00sVUFBZSxjQUhyQixFQUdxQyxPQUFPLE1BQVAsQ0FBYyxLQUhuRDtBQUtEO0FBQ0QsaUJBQU8sV0FBUDtBQUNELFNBMUJNOztBQTRCUCxxQkFBYSxZQUFXO0FBQ3RCLGlCQUFPLE9BQVAsQ0FBZSw0QkFBZixFQUE2QyxNQUE3QztBQUNBLHFCQUFXLE1BQVg7QUFDQSxrQkFDRyxJQURILENBQ1EsZUFEUixFQUN5QixNQUR6QjtBQUdELFNBbENNOztBQW9DUCxpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLE9BQVAsQ0FBZSw0QkFBZixFQUE2QyxRQUE3QztBQUNBLGtCQUNHLEdBREgsQ0FDTyxjQURQLEVBRUcsVUFGSCxDQUVjLGVBRmQ7QUFJRCxTQTFDTTs7QUE0Q1AsaUJBQVMsWUFBVztBQUNsQixpQkFBTyxPQUFQLENBQWUsMkJBQWY7QUFDQSxvQkFBVSxFQUFFLE9BQUYsQ0FBVjtBQUNELFNBL0NNOztBQWlEUCxhQUFLO0FBQ0gsb0JBQVUsWUFBVztBQUNuQixnQkFDRSxhQUFhLGNBQWMsRUFBRSxhQUFGLENBQWdCLFdBQVcsTUFBM0IsQ0FBZCxHQUNULFdBQVcsTUFERixHQUVULEVBSE47QUFLQSxjQUFFLElBQUYsQ0FBTyxTQUFTLFFBQWhCLEVBQTBCLFVBQVMsSUFBVCxFQUFlLFVBQWYsRUFBMkI7QUFDbkQsa0JBQUksT0FBTyxFQUFQLENBQVUsSUFBVixNQUFvQixTQUFwQixJQUFpQyxPQUFPLEVBQVAsQ0FBVSxJQUFWLEdBQXJDLEVBQXlEO0FBQ3ZELHVCQUFPLE9BQVAsQ0FBZSx1QkFBZixFQUF3QyxJQUF4QyxFQUE4QyxPQUE5QztBQUNBLGtCQUFFLE1BQUYsQ0FBUyxTQUFTLE1BQWxCLEVBQTBCLFVBQTFCLEVBQXNDLFVBQXRDO0FBQ0Q7QUFDRixhQUxEO0FBTUQ7QUFiRSxTQWpERTs7QUFpRVAsWUFBSTs7QUFFRixrQkFBUSxZQUFXO0FBQ2pCLG1CQUFPLFFBQVEsUUFBUixDQUFpQixVQUFVLE1BQTNCLENBQVA7QUFDRCxXQUpDO0FBS0YsbUJBQVMsWUFBVztBQUNsQixtQkFBTyxRQUFRLFFBQVIsQ0FBaUIsVUFBVSxPQUEzQixDQUFQO0FBQ0QsV0FQQztBQVFGLG9CQUFVLFlBQVc7QUFDbkIsbUJBQU8sQ0FBRyxRQUFRLFFBQVIsQ0FBaUIsVUFBVSxNQUEzQixDQUFWO0FBQ0QsV0FWQztBQVdGLGlCQUFPLFVBQVMsS0FBVCxFQUFnQjtBQUNyQixnQkFBRyxVQUFVLEtBQVYsTUFBcUIsU0FBeEIsRUFBbUM7QUFDakMscUJBQU8sS0FBUDtBQUNEO0FBQ0QsbUJBQU8sUUFBUSxRQUFSLENBQWtCLFVBQVUsS0FBVixDQUFsQixDQUFQO0FBQ0QsV0FoQkM7O0FBa0JGLG1CQUFTLFlBQVc7QUFDbEIsbUJBQU8sQ0FBRyxRQUFRLEVBQVIsQ0FBVyxTQUFTLE1BQVQsQ0FBZ0IsTUFBM0IsQ0FBVjtBQUNELFdBcEJDO0FBcUJGLG9CQUFVLFlBQVc7QUFDbkIsbUJBQVMsUUFBUSxFQUFSLENBQVcsU0FBUyxNQUFULENBQWdCLE1BQTNCLENBQVQ7QUFDRCxXQXZCQztBQXdCRix1QkFBYSxZQUFXO0FBQ3RCLG1CQUFPLENBQUcsUUFBUSxFQUFSLENBQVcsU0FBUyxNQUFULENBQWdCLElBQTNCLENBQVY7QUFDRCxXQTFCQzs7O0FBNkJGLGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sUUFBUSxFQUFSLENBQVcseUJBQVgsQ0FBUDtBQUNELFdBL0JDO0FBZ0NGLGlCQUFPLFlBQVc7QUFDaEIsbUJBQU8sUUFBUSxFQUFSLENBQVcsT0FBWCxDQUFQO0FBQ0QsV0FsQ0M7QUFtQ0Ysb0JBQVUsWUFBVztBQUNuQixtQkFBTyxRQUFRLEVBQVIsQ0FBVyxjQUFYLENBQVA7QUFDRDtBQXJDQyxTQWpFRzs7QUF5R1AsZUFBTyxVQUFTLEtBQVQsRUFBZ0I7QUFDckIsaUJBQU8sS0FBUCxDQUFhLG9CQUFiLEVBQW1DLEtBQW5DO0FBQ0EsaUJBQU8sS0FBUCxJQUFnQixJQUFoQjtBQUNELFNBNUdNO0FBNkdQLGtCQUFVLFVBQVMsS0FBVCxFQUFnQjtBQUN4QixpQkFBTyxLQUFQLENBQWEsb0JBQWIsRUFBbUMsS0FBbkM7QUFDQSxpQkFBTyxLQUFQLElBQWdCLEtBQWhCO0FBQ0QsU0FoSE07O0FBa0hQLGdCQUFRLFVBQVMsS0FBVCxFQUFnQjtBQUN0QixpQkFBTyxPQUFPLEtBQVAsS0FBaUIsS0FBeEI7QUFDRCxTQXBITTs7QUFzSFAsZ0JBQVEsWUFBVztBQUNqQixrQkFBUSxXQUFSLENBQW9CLFVBQVUsUUFBOUI7QUFDRCxTQXhITTs7QUEwSFAsaUJBQVMsWUFBVztBQUNsQixrQkFBUSxRQUFSLENBQWlCLFVBQVUsUUFBM0I7QUFDRCxTQTVITTs7QUE4SFAsa0JBQVUsVUFBUyxLQUFULEVBQWdCO0FBQ3hCLGNBQUcsT0FBTyxNQUFQLENBQWMsS0FBZCxDQUFILEVBQXlCO0FBQ3ZCLG9CQUFRLFFBQVIsQ0FBa0IsVUFBVSxLQUFWLENBQWxCO0FBQ0Q7QUFDRixTQWxJTTs7QUFvSVAscUJBQWEsVUFBUyxLQUFULEVBQWdCO0FBQzNCLGNBQUcsT0FBTyxNQUFQLENBQWMsS0FBZCxDQUFILEVBQXlCO0FBQ3ZCLG9CQUFRLFdBQVIsQ0FBcUIsVUFBVSxLQUFWLENBQXJCO0FBQ0Q7QUFDRixTQXhJTTs7QUEwSVAsZ0JBQVE7QUFDTixpQkFBTyxZQUFXO0FBQ2hCLGdCQUNFLFVBREYsRUFFRSxnQkFGRjtBQUlBLGdCQUFJLE9BQU8sTUFBUCxDQUFjLFFBQWQsS0FBMkIsT0FBTyxFQUFQLENBQVUsT0FBVixFQUEvQixFQUFxRDtBQUNuRCxxQkFBTyxPQUFQO0FBQ0Esa0JBQUcsRUFBRSxFQUFGLENBQUssR0FBTCxLQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLDZCQUFtQixRQUFRLEdBQVIsQ0FBWSxhQUFaLENBQW5CO0FBQ0EsbUNBQW1CLFFBQVEsR0FBUixDQUFZLGVBQVosQ0FBbkI7QUFDQSxvQkFBSSxnQkFBSixFQUF1QjtBQUNyQix5QkFBTyxLQUFQLENBQWEscUNBQWI7QUFDQSwyQkFBUyxZQUFULEdBQTBCLFlBQVU7QUFBRSwyQkFBTyxLQUFQO0FBQWUsbUJBQXJEO0FBQ0EsMkJBQVMsY0FBVCxHQUEwQixZQUFVO0FBQUUsMkJBQU8sS0FBUDtBQUFlLG1CQUFyRDtBQUNELGlCQUpELE1BS0ssSUFBRyxVQUFILEVBQWU7QUFDbEIseUJBQU8sUUFBUCxDQUFnQixVQUFoQjtBQUNBO0FBQ0Q7QUFDRjtBQUNELHFCQUFPLE1BQVAsQ0FBYyxLQUFkO0FBQ0Q7QUFDRjtBQXZCSyxTQTFJRDs7QUFvS1Asa0JBQVUsVUFBUyxVQUFULEVBQXFCO0FBQzdCLGlCQUFPLEtBQVAsQ0FBYSxnREFBYixFQUErRCxVQUEvRDtBQUNBLGNBQUcsVUFBSCxFQUFlO0FBQ2IsZ0JBQUcsS0FBSyxPQUFSLEVBQWlCO0FBQ2YscUJBQU8sTUFBUCxDQUFjLElBQWQsQ0FBbUIsS0FBSyxPQUF4QjtBQUNEO0FBQ0QsY0FBRSxJQUFGLENBQU8sVUFBUCxFQUNHLElBREgsQ0FDUSxZQUFXO0FBQ2Ysa0JBQUcsV0FBVyxLQUFYLE1BQXNCLFVBQXpCLEVBQXFDO0FBQ25DLHVCQUFPLEtBQVAsQ0FBYSx1QkFBYjtBQUNBLHlCQUFTLFlBQVQsR0FBMEIsWUFBVTtBQUFFLHlCQUFPLElBQVA7QUFBYyxpQkFBcEQ7QUFDQSx5QkFBUyxjQUFULEdBQTBCLFlBQVU7QUFBRSx5QkFBTyxJQUFQO0FBQWMsaUJBQXBEO0FBQ0QsZUFKRCxNQUtLO0FBQ0gsdUJBQU8sS0FBUCxDQUFhLG9CQUFiO0FBQ0EseUJBQVMsWUFBVCxHQUEwQixZQUFVO0FBQUUseUJBQU8sS0FBUDtBQUFlLGlCQUFyRDtBQUNBLHlCQUFTLGNBQVQsR0FBMEIsWUFBVTtBQUFFLHlCQUFPLEtBQVA7QUFBZSxpQkFBckQ7QUFDRDtBQUNELHFCQUFPLE1BQVAsQ0FBYyxLQUFkO0FBQ0QsYUFiSDtBQWVEO0FBQ0YsU0ExTE07OztBQTZMUCxnQkFBUTs7QUFFTixpQkFBTyxZQUFXO0FBQ2hCLG1CQUFPLEtBQVAsQ0FBYSxvQ0FBYjs7QUFFQSxnQkFBSSxPQUFPLEVBQVAsQ0FBVSxRQUFWLEVBQUosRUFBMkI7QUFDekIscUJBQU8sUUFBUDtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLFVBQVA7QUFDRDtBQUNELGdCQUFHLFNBQVMsSUFBWixFQUFrQjtBQUNoQixxQkFBTyxJQUFQO0FBQ0Q7QUFDRCxxQkFBUyxRQUFULENBQWtCLElBQWxCLENBQXVCLE9BQXZCO0FBQ0QsV0FmSzs7QUFpQk4sZ0JBQU0sWUFBVztBQUNmLGdCQUFJLE9BQU8sRUFBUCxDQUFVLFdBQVYsRUFBSixFQUE4QjtBQUM1QixrQkFBRyxPQUFPLEVBQVAsQ0FBVSxRQUFWLEVBQUgsRUFBMEI7QUFDeEIsdUJBQU8sT0FBUCxDQUFlLGdDQUFmLEVBQWlELEtBQUssS0FBdEQ7QUFDQSx1QkFBTyxNQUFQLENBQWMsSUFBZCxDQUFtQixLQUFLLFFBQXhCO0FBQ0QsZUFIRCxNQUlLLElBQUksT0FBTyxFQUFQLENBQVUsTUFBVixFQUFKLEVBQXlCO0FBQzVCLG9CQUFHLEtBQUssS0FBUixFQUFlO0FBQ2IseUJBQU8sT0FBUCxDQUFlLDZCQUFmLEVBQThDLEtBQUssS0FBbkQ7QUFDQSx5QkFBTyxNQUFQLENBQWMsSUFBZCxDQUFtQixLQUFLLEtBQXhCO0FBQ0QsaUJBSEQsTUFJSyxJQUFHLEtBQUssVUFBUixFQUFvQjtBQUN2Qix5QkFBTyxPQUFQLENBQWUsb0NBQWYsRUFBcUQsS0FBSyxVQUExRDtBQUNBLHlCQUFPLE1BQVAsQ0FBYyxJQUFkLENBQW1CLEtBQUssVUFBeEI7QUFDRDtBQUNGLGVBVEksTUFVQTtBQUNILG9CQUFHLEtBQUssS0FBUixFQUFlO0FBQ2IseUJBQU8sT0FBUCxDQUFlLDZCQUFmLEVBQThDLEtBQUssS0FBbkQ7QUFDQSx5QkFBTyxNQUFQLENBQWMsSUFBZCxDQUFtQixLQUFLLEtBQXhCO0FBQ0QsaUJBSEQsTUFJSyxJQUFHLEtBQUssUUFBUixFQUFpQjtBQUNwQix5QkFBTyxPQUFQLENBQWUsa0NBQWYsRUFBbUQsS0FBSyxRQUF4RDtBQUNBLHlCQUFPLE1BQVAsQ0FBYyxJQUFkLENBQW1CLEtBQUssUUFBeEI7QUFDRDtBQUNGO0FBQ0Y7QUFDRjs7QUE1Q0ssU0E3TEQ7O0FBNk9QLGtCQUFVLFlBQVc7QUFDbkIsY0FBSSxTQUFTLFlBQVQsQ0FBc0IsSUFBdEIsQ0FBMkIsT0FBM0IsQ0FBSixFQUEwQztBQUN4QyxtQkFBTyxLQUFQLENBQWEseUJBQWI7QUFDQSxvQkFDRyxRQURILENBQ1ksVUFBVSxNQUR0QjtBQUdBLG1CQUFPLE1BQVAsQ0FBYyxJQUFkLENBQW1CLEtBQUssTUFBeEI7QUFDQSxxQkFBUyxVQUFULENBQW9CLElBQXBCLENBQXlCLE9BQXpCO0FBQ0Q7QUFDRixTQXRQTTs7QUF3UFAsb0JBQVksWUFBVztBQUNyQixjQUFJLFNBQVMsY0FBVCxDQUF3QixJQUF4QixDQUE2QixPQUE3QixDQUFKLEVBQTRDO0FBQzFDLG1CQUFPLEtBQVAsQ0FBYSwyQkFBYjtBQUNBLG9CQUNHLFdBREgsQ0FDZSxVQUFVLE1BRHpCO0FBR0EsbUJBQU8sTUFBUCxDQUFjLElBQWQsQ0FBbUIsS0FBSyxRQUF4QjtBQUNBLHFCQUFTLFlBQVQsQ0FBc0IsSUFBdEIsQ0FBMkIsT0FBM0I7QUFDRDtBQUNGLFNBalFNOztBQW1RUCxjQUFNLFlBQVc7QUFDZixpQkFBTyxPQUFQLENBQWUsd0NBQWY7QUFDQSxjQUFJLE9BQU8sRUFBUCxDQUFVLE1BQVYsRUFBSixFQUF5QjtBQUN2Qix3QkFDRyxHQURILENBQ08sT0FEUCxFQUVLLEtBRkwsQ0FFVyxVQUZYO0FBR0QsV0FKRCxNQUtLO0FBQ0gsd0JBQ0csR0FESCxDQUNPLE9BRFAsRUFFSyxLQUZMLENBRVcsWUFGWDtBQUlEO0FBQ0YsU0FoUk07O0FBa1JQLGFBQUs7QUFDSCxnQkFBTSxZQUFXO0FBQ2YsbUJBQVEsU0FBUyxRQUFULENBQWtCLElBQW5CLEdBQ0gsUUFBUSxJQUFSLENBQWEsU0FBUyxRQUFULENBQWtCLElBQS9CLEVBQXFDLElBQXJDLEVBREcsR0FFSCxRQUFRLElBQVIsRUFGSjtBQUlELFdBTkU7QUFPSCxtQkFBUyxVQUFTLEtBQVQsRUFBZ0I7QUFDdkIsbUJBQU8sS0FBSyxLQUFMLEtBQWUsS0FBdEI7QUFDRDtBQVRFLFNBbFJFOztBQThSUCxlQUFPO0FBQ0wsZ0JBQU0sVUFBUyxJQUFULEVBQWUsUUFBZixFQUF5QixRQUF6QixFQUFtQztBQUN2QyxnQkFDRSxlQUFlLE9BQU8sR0FBUCxDQUFXLElBQVgsRUFEakI7QUFHQSxtQkFBTyxLQUFQLENBQWEsdUJBQWIsRUFBc0MsSUFBdEMsRUFBNEMsUUFBNUM7QUFDQSxtQkFBVyxRQUFZLFNBQVMsSUFBVCxDQUFjLEtBQXJDO0FBQ0EsdUJBQVcsWUFBWSxTQUFTLGFBQWhDO0FBQ0EsdUJBQVcsWUFBWSxZQUFXLENBQUUsQ0FBcEM7QUFDQSxtQkFBTyxNQUFQLENBQWMsSUFBZCxDQUFtQixJQUFuQjtBQUNBLHVCQUFXLFlBQVU7QUFDbkIscUJBQU8sTUFBUCxDQUFjLElBQWQsQ0FBbUIsWUFBbkI7QUFDQSx1QkFBUyxJQUFULENBQWMsT0FBZDtBQUNELGFBSEQsRUFHRyxRQUhIO0FBSUQ7QUFkSSxTQTlSQTs7QUErU1AsZUFBTzs7QUFFTCxnQkFBTSxZQUFXO0FBQ2YsZ0JBQ0UsYUFBZSxLQUFLLE1BQUwsSUFBaUIsUUFBUSxJQUFSLENBQWEsU0FBUyxVQUF0QixDQURsQztBQUFBLGdCQUVFLGVBQWUsS0FBSyxRQUFMLElBQWlCLFFBQVEsSUFBUixDQUFhLFNBQVMsVUFBdEIsQ0FGbEM7QUFJQSxnQkFBSSxPQUFPLEVBQVAsQ0FBVSxXQUFWLEVBQUosRUFBOEI7QUFDNUIsa0JBQUksT0FBTyxFQUFQLENBQVUsTUFBVixNQUFzQixVQUExQixFQUFzQztBQUNwQyx1QkFBTyxPQUFQLENBQWUsdUJBQWYsRUFBd0MsVUFBeEM7QUFDQSx1QkFBTyxNQUFQLENBQWMsSUFBZCxDQUFtQixVQUFuQjtBQUNELGVBSEQsTUFJSyxJQUFHLFlBQUgsRUFBaUI7QUFDcEIsdUJBQU8sT0FBUCxDQUFlLHlCQUFmLEVBQTBDLFVBQTFDO0FBQ0EsdUJBQU8sTUFBUCxDQUFjLElBQWQsQ0FBbUIsWUFBbkI7QUFDRDtBQUNGO0FBQ0Y7QUFqQkksU0EvU0E7O0FBbVVQLGdCQUFRO0FBQ04sZ0JBQU0sVUFBUyxJQUFULEVBQWU7QUFDbkIsZ0JBQ0UsY0FBYyxPQUFPLEdBQVAsQ0FBVyxJQUFYLEVBRGhCO0FBR0EsZ0JBQUcsUUFBUSxTQUFTLFdBQXBCLEVBQWlDO0FBQy9CLHFCQUFPLEtBQVAsQ0FBYSxlQUFiLEVBQThCLElBQTlCO0FBQ0Esa0JBQUcsU0FBUyxRQUFULENBQWtCLElBQXJCLEVBQTJCO0FBQ3pCLHdCQUNHLElBREgsQ0FDUSxTQUFTLFVBRGpCLEVBQzZCLElBRDdCLEVBRUcsSUFGSCxDQUVRLFNBQVMsUUFBVCxDQUFrQixJQUYxQixFQUdLLElBSEwsQ0FHVSxJQUhWO0FBS0QsZUFORCxNQU9LO0FBQ0gsd0JBQ0csSUFESCxDQUNRLFNBQVMsVUFEakIsRUFDNkIsSUFEN0IsRUFFRyxJQUZILENBRVEsSUFGUjtBQUlEO0FBQ0YsYUFmRCxNQWdCSztBQUNILHFCQUFPLEtBQVAsQ0FBYSxzQ0FBYixFQUFxRCxJQUFyRDtBQUNEO0FBQ0Y7QUF4QkssU0FuVUQ7O0FBOFZQLGlCQUFTLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDN0IsaUJBQU8sS0FBUCxDQUFhLGtCQUFiLEVBQWlDLElBQWpDLEVBQXVDLEtBQXZDO0FBQ0EsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsUUFBZixFQUF5QixJQUF6QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixnQkFBRyxFQUFFLGFBQUYsQ0FBZ0IsU0FBUyxJQUFULENBQWhCLENBQUgsRUFBb0M7QUFDbEMsZ0JBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxTQUFTLElBQVQsQ0FBZixFQUErQixLQUEvQjtBQUNELGFBRkQsTUFHSztBQUNILHVCQUFTLElBQVQsSUFBaUIsS0FBakI7QUFDRDtBQUNGLFdBUEksTUFRQTtBQUNILG1CQUFPLFNBQVMsSUFBVCxDQUFQO0FBQ0Q7QUFDRixTQTlXTTtBQStXUCxrQkFBVSxVQUFTLElBQVQsRUFBZSxLQUFmLEVBQXNCO0FBQzlCLGNBQUksRUFBRSxhQUFGLENBQWdCLElBQWhCLENBQUosRUFBNEI7QUFDMUIsY0FBRSxNQUFGLENBQVMsSUFBVCxFQUFlLE1BQWYsRUFBdUIsSUFBdkI7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsbUJBQU8sSUFBUCxJQUFlLEtBQWY7QUFDRCxXQUZJLE1BR0E7QUFDSCxtQkFBTyxPQUFPLElBQVAsQ0FBUDtBQUNEO0FBQ0YsU0F6WE07QUEwWFAsZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQVYsSUFBb0IsU0FBUyxLQUFoQyxFQUF1QztBQUNyQyxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLEtBQVAsR0FBZSxTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxJQUFyQyxFQUEyQyxPQUEzQyxFQUFvRCxTQUFTLElBQVQsR0FBZ0IsR0FBcEUsQ0FBZjtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLE9BQW5CLEVBQTRCLFNBQTVCO0FBQ0Q7QUFDRjtBQUNGLFNBcFlNO0FBcVlQLGlCQUFTLFlBQVc7QUFDbEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLE9BQTdCLElBQXdDLFNBQVMsS0FBcEQsRUFBMkQ7QUFDekQsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxPQUFQLEdBQWlCLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFqQjtBQUNBLHFCQUFPLE9BQVAsQ0FBZSxLQUFmLENBQXFCLE9BQXJCLEVBQThCLFNBQTlCO0FBQ0Q7QUFDRjtBQUNGLFNBL1lNO0FBZ1pQLGVBQU8sWUFBVztBQUNoQixjQUFHLENBQUMsU0FBUyxNQUFiLEVBQXFCO0FBQ25CLG1CQUFPLEtBQVAsR0FBZSxTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxLQUFyQyxFQUE0QyxPQUE1QyxFQUFxRCxTQUFTLElBQVQsR0FBZ0IsR0FBckUsQ0FBZjtBQUNBLG1CQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLE9BQW5CLEVBQTRCLFNBQTVCO0FBQ0Q7QUFDRixTQXJaTTtBQXNaUCxxQkFBYTtBQUNYLGVBQUssVUFBUyxPQUFULEVBQWtCO0FBQ3JCLGdCQUNFLFdBREYsRUFFRSxhQUZGLEVBR0UsWUFIRjtBQUtBLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2Qiw0QkFBZ0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUFoQjtBQUNBLDZCQUFnQixRQUFRLFdBQXhCO0FBQ0EsOEJBQWdCLGNBQWMsWUFBOUI7QUFDQSxxQkFBZ0IsV0FBaEI7QUFDQSwwQkFBWSxJQUFaLENBQWlCO0FBQ2Ysd0JBQW1CLFFBQVEsQ0FBUixDQURKO0FBRWYsNkJBQW1CLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxPQUFkLEVBQXVCLENBQXZCLEtBQTZCLEVBRmpDO0FBR2YsMkJBQW1CLE9BSEo7QUFJZixrQ0FBbUI7QUFKSixlQUFqQjtBQU1EO0FBQ0QseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsbUJBQU8sV0FBUCxDQUFtQixLQUFuQixHQUEyQixXQUFXLE9BQU8sV0FBUCxDQUFtQixPQUE5QixFQUF1QyxHQUF2QyxDQUEzQjtBQUNELFdBckJVO0FBc0JYLG1CQUFTLFlBQVc7QUFDbEIsZ0JBQ0UsUUFBUSxTQUFTLElBQVQsR0FBZ0IsR0FEMUI7QUFBQSxnQkFFRSxZQUFZLENBRmQ7QUFJQSxtQkFBTyxLQUFQO0FBQ0EseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsY0FBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMkJBQWEsS0FBSyxnQkFBTCxDQUFiO0FBQ0QsYUFGRDtBQUdBLHFCQUFTLE1BQU0sU0FBTixHQUFrQixJQUEzQjtBQUNBLGdCQUFHLGNBQUgsRUFBbUI7QUFDakIsdUJBQVMsUUFBUSxjQUFSLEdBQXlCLElBQWxDO0FBQ0Q7QUFDRCxnQkFBSSxDQUFDLFFBQVEsS0FBUixLQUFrQixTQUFsQixJQUErQixRQUFRLEtBQVIsS0FBa0IsU0FBbEQsS0FBZ0UsWUFBWSxNQUFaLEdBQXFCLENBQXpGLEVBQTRGO0FBQzFGLHNCQUFRLGNBQVIsQ0FBdUIsS0FBdkI7QUFDQSxrQkFBRyxRQUFRLEtBQVgsRUFBa0I7QUFDaEIsd0JBQVEsS0FBUixDQUFjLFdBQWQ7QUFDRCxlQUZELE1BR0s7QUFDSCxrQkFBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMEJBQVEsR0FBUixDQUFZLEtBQUssTUFBTCxJQUFlLElBQWYsR0FBc0IsS0FBSyxnQkFBTCxDQUF0QixHQUE2QyxJQUF6RDtBQUNELGlCQUZEO0FBR0Q7QUFDRCxzQkFBUSxRQUFSO0FBQ0Q7QUFDRCwwQkFBYyxFQUFkO0FBQ0Q7QUFqRFUsU0F0Wk47QUF5Y1AsZ0JBQVEsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDLE9BQWpDLEVBQTBDO0FBQ2hELGNBQ0UsU0FBUyxRQURYO0FBQUEsY0FFRSxRQUZGO0FBQUEsY0FHRSxLQUhGO0FBQUEsY0FJRSxRQUpGO0FBTUEsNEJBQWtCLG1CQUFtQixjQUFyQztBQUNBLG9CQUFrQixXQUFtQixPQUFyQztBQUNBLGNBQUcsT0FBTyxLQUFQLElBQWdCLFFBQWhCLElBQTRCLFdBQVcsU0FBMUMsRUFBcUQ7QUFDbkQsb0JBQVcsTUFBTSxLQUFOLENBQVksT0FBWixDQUFYO0FBQ0EsdUJBQVcsTUFBTSxNQUFOLEdBQWUsQ0FBMUI7QUFDQSxjQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ25DLGtCQUFJLGlCQUFrQixTQUFTLFFBQVYsR0FDakIsUUFBUSxNQUFNLFFBQVEsQ0FBZCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixFQUEyQixXQUEzQixFQUFSLEdBQW1ELE1BQU0sUUFBUSxDQUFkLEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBRGxDLEdBRWpCLEtBRko7QUFJQSxrQkFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxjQUFQLENBQWpCLEtBQThDLFNBQVMsUUFBM0QsRUFBdUU7QUFDckUseUJBQVMsT0FBTyxjQUFQLENBQVQ7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLGNBQVAsTUFBMkIsU0FBL0IsRUFBMkM7QUFDOUMsd0JBQVEsT0FBTyxjQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBLElBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sS0FBUCxDQUFqQixLQUFxQyxTQUFTLFFBQWxELEVBQThEO0FBQ2pFLHlCQUFTLE9BQU8sS0FBUCxDQUFUO0FBQ0QsZUFGSSxNQUdBLElBQUksT0FBTyxLQUFQLE1BQWtCLFNBQXRCLEVBQWtDO0FBQ3JDLHdCQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQTtBQUNILHVCQUFPLEtBQVAsQ0FBYSxNQUFNLE1BQW5CLEVBQTJCLEtBQTNCO0FBQ0EsdUJBQU8sS0FBUDtBQUNEO0FBQ0YsYUF2QkQ7QUF3QkQ7QUFDRCxjQUFLLEVBQUUsVUFBRixDQUFjLEtBQWQsQ0FBTCxFQUE2QjtBQUMzQix1QkFBVyxNQUFNLEtBQU4sQ0FBWSxPQUFaLEVBQXFCLGVBQXJCLENBQVg7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsdUJBQVcsS0FBWDtBQUNEO0FBQ0QsY0FBRyxFQUFFLE9BQUYsQ0FBVSxhQUFWLENBQUgsRUFBNkI7QUFDM0IsMEJBQWMsSUFBZCxDQUFtQixRQUFuQjtBQUNELFdBRkQsTUFHSyxJQUFHLGtCQUFrQixTQUFyQixFQUFnQztBQUNuQyw0QkFBZ0IsQ0FBQyxhQUFELEVBQWdCLFFBQWhCLENBQWhCO0FBQ0QsV0FGSSxNQUdBLElBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUM5Qiw0QkFBZ0IsUUFBaEI7QUFDRDtBQUNELGlCQUFPLEtBQVA7QUFDRDtBQTlmTSxPQUFUOztBQWlnQkEsVUFBRyxhQUFILEVBQWtCO0FBQ2hCLFlBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUN6QixpQkFBTyxVQUFQO0FBQ0Q7QUFDRCxlQUFPLE1BQVAsQ0FBYyxLQUFkO0FBQ0QsT0FMRCxNQU1LO0FBQ0gsWUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLG1CQUFTLE1BQVQsQ0FBZ0IsU0FBaEI7QUFDRDtBQUNELGVBQU8sVUFBUDtBQUNEO0FBQ0YsS0FyaUJIOztBQXdpQkEsV0FBUSxrQkFBa0IsU0FBbkIsR0FDSCxhQURHLEdBRUgsSUFGSjtBQUlELEdBNWpCRDs7QUE4akJBLElBQUUsRUFBRixDQUFLLEtBQUwsQ0FBVyxRQUFYLEdBQXNCOzs7QUFHcEIsVUFBaUIsT0FIRzs7O0FBTXBCLFdBQWlCLEtBTkc7OztBQVNwQixhQUFpQixLQVRHOzs7QUFZcEIsZUFBaUIsT0FaRzs7O0FBZXBCLGlCQUFpQixJQWZHOzs7QUFrQnBCLGdCQUFpQixZQUFXLENBQUUsQ0FsQlY7QUFtQnBCLGtCQUFpQixZQUFXLENBQUUsQ0FuQlY7QUFvQnBCLGNBQWlCLFlBQVcsQ0FBRSxDQXBCVjs7O0FBdUJwQixrQkFBaUIsWUFBVztBQUFFLGFBQU8sSUFBUDtBQUFjLEtBdkJ4QjtBQXdCcEIsb0JBQWlCLFlBQVc7QUFBRSxhQUFPLElBQVA7QUFBYyxLQXhCeEI7OztBQTJCcEIsZUFBaUIsSUEzQkc7OztBQThCcEIsVUFBaUIsS0E5Qkc7OztBQWlDcEIsbUJBQWlCLElBakNHOzs7QUFvQ3BCLFlBQWE7QUFDWCxZQUFTLHFCQURFO0FBRVgsY0FBUztBQUZFLEtBcENPOztBQXlDcEIsYUFBYSxLQXpDTzs7O0FBNENwQixXQUFPO0FBQ0wsa0JBQWEscURBRFI7QUFFTCxjQUFhO0FBRlIsS0E1Q2E7OztBQWtEcEIsY0FBVTtBQUNSLGVBQWEsU0FETDtBQUVSLGtCQUFhO0FBRkwsS0FsRFU7OztBQXdEcEIsZUFBVztBQUNULGNBQVcsUUFERjtBQUVULGdCQUFXLFVBRkY7QUFHVCxhQUFXLE9BSEY7QUFJVCxlQUFXLFNBSkY7QUFLVCxlQUFXLFNBTEY7QUFNVCxlQUFXO0FBTkYsS0F4RFM7O0FBaUVwQixjQUFVOztBQUVSLFlBQU07QUFGRSxLQWpFVTs7QUFzRXBCLGNBQVc7QUFDVCxhQUFPO0FBQ0wsa0JBQVcsSUFETjtBQUVMLGlCQUFXLElBRk47QUFHTCxnQkFBVztBQUhOLE9BREU7QUFNVCxjQUFRO0FBQ04sa0JBQVcsSUFETDtBQUVOLGlCQUFXLElBRkw7QUFHTixnQkFBVztBQUhMLE9BTkM7QUFXVCxnQkFBVTtBQUNSLGdCQUFXLElBREg7QUFFUixpQkFBVyxJQUZIO0FBR1IsaUJBQVcsSUFISDtBQUlSLGVBQVc7QUFKSDtBQVhELEtBdEVTOztBQXlGcEIsWUFBYTtBQUNYLGNBQVcsSUFEQTtBQUVYLGdCQUFXLElBRkE7QUFHWCxhQUFXLElBSEE7QUFJWCxlQUFXLElBSkE7QUFLWCxlQUFXLElBTEE7QUFNWCxlQUFXO0FBTkEsS0F6Rk87O0FBa0dwQixVQUFXO0FBQ1QsZ0JBQWEsS0FESjtBQUVULGFBQWEsS0FGSjtBQUdULGFBQWEsS0FISjtBQUlULGNBQWEsS0FKSjtBQUtULGdCQUFhLEtBTEo7QUFNVCxnQkFBYSxLQU5KO0FBT1Qsa0JBQWE7QUFQSjs7QUFsR1MsR0FBdEI7QUFnSEMsQ0F6ckJBLEVBeXJCRyxNQXpyQkgsRUF5ckJXLE1BenJCWCxFQXlyQm1CLFFBenJCbkIiLCJmaWxlIjoic3RhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcbiAqICMgU2VtYW50aWMgVUkgLSBTdGF0ZVxuICogaHR0cDovL2dpdGh1Yi5jb20vc2VtYW50aWMtb3JnL3NlbWFudGljLXVpL1xuICpcbiAqXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2VcbiAqIGh0dHA6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9NSVRcbiAqXG4gKi9cblxuOyhmdW5jdGlvbiAoJCwgd2luZG93LCBkb2N1bWVudCwgdW5kZWZpbmVkKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG53aW5kb3cgPSAodHlwZW9mIHdpbmRvdyAhPSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuTWF0aCA9PSBNYXRoKVxuICA/IHdpbmRvd1xuICA6ICh0eXBlb2Ygc2VsZiAhPSAndW5kZWZpbmVkJyAmJiBzZWxmLk1hdGggPT0gTWF0aClcbiAgICA/IHNlbGZcbiAgICA6IEZ1bmN0aW9uKCdyZXR1cm4gdGhpcycpKClcbjtcblxuJC5mbi5zdGF0ZSA9IGZ1bmN0aW9uKHBhcmFtZXRlcnMpIHtcbiAgdmFyXG4gICAgJGFsbE1vZHVsZXMgICAgID0gJCh0aGlzKSxcblxuICAgIG1vZHVsZVNlbGVjdG9yICA9ICRhbGxNb2R1bGVzLnNlbGVjdG9yIHx8ICcnLFxuXG4gICAgaGFzVG91Y2ggICAgICAgID0gKCdvbnRvdWNoc3RhcnQnIGluIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCksXG4gICAgdGltZSAgICAgICAgICAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCksXG4gICAgcGVyZm9ybWFuY2UgICAgID0gW10sXG5cbiAgICBxdWVyeSAgICAgICAgICAgPSBhcmd1bWVudHNbMF0sXG4gICAgbWV0aG9kSW52b2tlZCAgID0gKHR5cGVvZiBxdWVyeSA9PSAnc3RyaW5nJyksXG4gICAgcXVlcnlBcmd1bWVudHMgID0gW10uc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpLFxuXG4gICAgcmV0dXJuZWRWYWx1ZVxuICA7XG4gICRhbGxNb2R1bGVzXG4gICAgLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICB2YXJcbiAgICAgICAgc2V0dGluZ3MgICAgICAgICAgPSAoICQuaXNQbGFpbk9iamVjdChwYXJhbWV0ZXJzKSApXG4gICAgICAgICAgPyAkLmV4dGVuZCh0cnVlLCB7fSwgJC5mbi5zdGF0ZS5zZXR0aW5ncywgcGFyYW1ldGVycylcbiAgICAgICAgICA6ICQuZXh0ZW5kKHt9LCAkLmZuLnN0YXRlLnNldHRpbmdzKSxcblxuICAgICAgICBlcnJvciAgICAgICAgICAgPSBzZXR0aW5ncy5lcnJvcixcbiAgICAgICAgbWV0YWRhdGEgICAgICAgID0gc2V0dGluZ3MubWV0YWRhdGEsXG4gICAgICAgIGNsYXNzTmFtZSAgICAgICA9IHNldHRpbmdzLmNsYXNzTmFtZSxcbiAgICAgICAgbmFtZXNwYWNlICAgICAgID0gc2V0dGluZ3MubmFtZXNwYWNlLFxuICAgICAgICBzdGF0ZXMgICAgICAgICAgPSBzZXR0aW5ncy5zdGF0ZXMsXG4gICAgICAgIHRleHQgICAgICAgICAgICA9IHNldHRpbmdzLnRleHQsXG5cbiAgICAgICAgZXZlbnROYW1lc3BhY2UgID0gJy4nICsgbmFtZXNwYWNlLFxuICAgICAgICBtb2R1bGVOYW1lc3BhY2UgPSBuYW1lc3BhY2UgKyAnLW1vZHVsZScsXG5cbiAgICAgICAgJG1vZHVsZSAgICAgICAgID0gJCh0aGlzKSxcblxuICAgICAgICBlbGVtZW50ICAgICAgICAgPSB0aGlzLFxuICAgICAgICBpbnN0YW5jZSAgICAgICAgPSAkbW9kdWxlLmRhdGEobW9kdWxlTmFtZXNwYWNlKSxcblxuICAgICAgICBtb2R1bGVcbiAgICAgIDtcbiAgICAgIG1vZHVsZSA9IHtcblxuICAgICAgICBpbml0aWFsaXplOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnSW5pdGlhbGl6aW5nIG1vZHVsZScpO1xuXG4gICAgICAgICAgLy8gYWxsb3cgbW9kdWxlIHRvIGd1ZXNzIGRlc2lyZWQgc3RhdGUgYmFzZWQgb24gZWxlbWVudFxuICAgICAgICAgIGlmKHNldHRpbmdzLmF1dG9tYXRpYykge1xuICAgICAgICAgICAgbW9kdWxlLmFkZC5kZWZhdWx0cygpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIC8vIGJpbmQgZXZlbnRzIHdpdGggZGVsZWdhdGVkIGV2ZW50c1xuICAgICAgICAgIGlmKHNldHRpbmdzLmNvbnRleHQgJiYgbW9kdWxlU2VsZWN0b3IgIT09ICcnKSB7XG4gICAgICAgICAgICAkKHNldHRpbmdzLmNvbnRleHQpXG4gICAgICAgICAgICAgIC5vbihtb2R1bGVTZWxlY3RvciwgJ21vdXNlZW50ZXInICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS5jaGFuZ2UudGV4dClcbiAgICAgICAgICAgICAgLm9uKG1vZHVsZVNlbGVjdG9yLCAnbW91c2VsZWF2ZScgKyBldmVudE5hbWVzcGFjZSwgbW9kdWxlLnJlc2V0LnRleHQpXG4gICAgICAgICAgICAgIC5vbihtb2R1bGVTZWxlY3RvciwgJ2NsaWNrJyAgICAgICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS50b2dnbGUuc3RhdGUpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAub24oJ21vdXNlZW50ZXInICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS5jaGFuZ2UudGV4dClcbiAgICAgICAgICAgICAgLm9uKCdtb3VzZWxlYXZlJyArIGV2ZW50TmFtZXNwYWNlLCBtb2R1bGUucmVzZXQudGV4dClcbiAgICAgICAgICAgICAgLm9uKCdjbGljaycgICAgICArIGV2ZW50TmFtZXNwYWNlLCBtb2R1bGUudG9nZ2xlLnN0YXRlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgICBtb2R1bGUuaW5zdGFudGlhdGUoKTtcbiAgICAgICAgfSxcblxuICAgICAgICBpbnN0YW50aWF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1N0b3JpbmcgaW5zdGFuY2Ugb2YgbW9kdWxlJywgbW9kdWxlKTtcbiAgICAgICAgICBpbnN0YW5jZSA9IG1vZHVsZTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAuZGF0YShtb2R1bGVOYW1lc3BhY2UsIG1vZHVsZSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVzdHJveTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0Rlc3Ryb3lpbmcgcHJldmlvdXMgbW9kdWxlJywgaW5zdGFuY2UpO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5vZmYoZXZlbnROYW1lc3BhY2UpXG4gICAgICAgICAgICAucmVtb3ZlRGF0YShtb2R1bGVOYW1lc3BhY2UpXG4gICAgICAgICAgO1xuICAgICAgICB9LFxuXG4gICAgICAgIHJlZnJlc2g6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZWZyZXNoaW5nIHNlbGVjdG9yIGNhY2hlJyk7XG4gICAgICAgICAgJG1vZHVsZSA9ICQoZWxlbWVudCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgYWRkOiB7XG4gICAgICAgICAgZGVmYXVsdHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHVzZXJTdGF0ZXMgPSBwYXJhbWV0ZXJzICYmICQuaXNQbGFpbk9iamVjdChwYXJhbWV0ZXJzLnN0YXRlcylcbiAgICAgICAgICAgICAgICA/IHBhcmFtZXRlcnMuc3RhdGVzXG4gICAgICAgICAgICAgICAgOiB7fVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJC5lYWNoKHNldHRpbmdzLmRlZmF1bHRzLCBmdW5jdGlvbih0eXBlLCB0eXBlU3RhdGVzKSB7XG4gICAgICAgICAgICAgIGlmKCBtb2R1bGUuaXNbdHlwZV0gIT09IHVuZGVmaW5lZCAmJiBtb2R1bGUuaXNbdHlwZV0oKSApIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQWRkaW5nIGRlZmF1bHQgc3RhdGVzJywgdHlwZSwgZWxlbWVudCk7XG4gICAgICAgICAgICAgICAgJC5leHRlbmQoc2V0dGluZ3Muc3RhdGVzLCB0eXBlU3RhdGVzLCB1c2VyU3RhdGVzKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGlzOiB7XG5cbiAgICAgICAgICBhY3RpdmU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRtb2R1bGUuaGFzQ2xhc3MoY2xhc3NOYW1lLmFjdGl2ZSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBsb2FkaW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkbW9kdWxlLmhhc0NsYXNzKGNsYXNzTmFtZS5sb2FkaW5nKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGluYWN0aXZlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAhKCAkbW9kdWxlLmhhc0NsYXNzKGNsYXNzTmFtZS5hY3RpdmUpICk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzdGF0ZTogZnVuY3Rpb24oc3RhdGUpIHtcbiAgICAgICAgICAgIGlmKGNsYXNzTmFtZVtzdGF0ZV0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5oYXNDbGFzcyggY2xhc3NOYW1lW3N0YXRlXSApO1xuICAgICAgICAgIH0sXG5cbiAgICAgICAgICBlbmFibGVkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAhKCAkbW9kdWxlLmlzKHNldHRpbmdzLmZpbHRlci5hY3RpdmUpICk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXNhYmxlZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gKCAkbW9kdWxlLmlzKHNldHRpbmdzLmZpbHRlci5hY3RpdmUpICk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB0ZXh0RW5hYmxlZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gISggJG1vZHVsZS5pcyhzZXR0aW5ncy5maWx0ZXIudGV4dCkgKTtcbiAgICAgICAgICB9LFxuXG4gICAgICAgICAgLy8gZGVmaW5pdGlvbnMgZm9yIGF1dG9tYXRpYyB0eXBlIGRldGVjdGlvblxuICAgICAgICAgIGJ1dHRvbjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5pcygnLmJ1dHRvbjpub3QoYSwgLnN1Ym1pdCknKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGlucHV0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAkbW9kdWxlLmlzKCdpbnB1dCcpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcHJvZ3Jlc3M6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICRtb2R1bGUuaXMoJy51aS5wcm9ncmVzcycpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBhbGxvdzogZnVuY3Rpb24oc3RhdGUpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ05vdyBhbGxvd2luZyBzdGF0ZScsIHN0YXRlKTtcbiAgICAgICAgICBzdGF0ZXNbc3RhdGVdID0gdHJ1ZTtcbiAgICAgICAgfSxcbiAgICAgICAgZGlzYWxsb3c6IGZ1bmN0aW9uKHN0YXRlKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdObyBsb25nZXIgYWxsb3dpbmcnLCBzdGF0ZSk7XG4gICAgICAgICAgc3RhdGVzW3N0YXRlXSA9IGZhbHNlO1xuICAgICAgICB9LFxuXG4gICAgICAgIGFsbG93czogZnVuY3Rpb24oc3RhdGUpIHtcbiAgICAgICAgICByZXR1cm4gc3RhdGVzW3N0YXRlXSB8fCBmYWxzZTtcbiAgICAgICAgfSxcblxuICAgICAgICBlbmFibGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICRtb2R1bGUucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmRpc2FibGVkKTtcbiAgICAgICAgfSxcblxuICAgICAgICBkaXNhYmxlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAkbW9kdWxlLmFkZENsYXNzKGNsYXNzTmFtZS5kaXNhYmxlZCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0U3RhdGU6IGZ1bmN0aW9uKHN0YXRlKSB7XG4gICAgICAgICAgaWYobW9kdWxlLmFsbG93cyhzdGF0ZSkpIHtcbiAgICAgICAgICAgICRtb2R1bGUuYWRkQ2xhc3MoIGNsYXNzTmFtZVtzdGF0ZV0gKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVtb3ZlU3RhdGU6IGZ1bmN0aW9uKHN0YXRlKSB7XG4gICAgICAgICAgaWYobW9kdWxlLmFsbG93cyhzdGF0ZSkpIHtcbiAgICAgICAgICAgICRtb2R1bGUucmVtb3ZlQ2xhc3MoIGNsYXNzTmFtZVtzdGF0ZV0gKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgdG9nZ2xlOiB7XG4gICAgICAgICAgc3RhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGFwaVJlcXVlc3QsXG4gICAgICAgICAgICAgIHJlcXVlc3RDYW5jZWxsZWRcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKCBtb2R1bGUuYWxsb3dzKCdhY3RpdmUnKSAmJiBtb2R1bGUuaXMuZW5hYmxlZCgpICkge1xuICAgICAgICAgICAgICBtb2R1bGUucmVmcmVzaCgpO1xuICAgICAgICAgICAgICBpZigkLmZuLmFwaSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgYXBpUmVxdWVzdCAgICAgICA9ICRtb2R1bGUuYXBpKCdnZXQgcmVxdWVzdCcpO1xuICAgICAgICAgICAgICAgIHJlcXVlc3RDYW5jZWxsZWQgPSAkbW9kdWxlLmFwaSgnd2FzIGNhbmNlbGxlZCcpO1xuICAgICAgICAgICAgICAgIGlmKCByZXF1ZXN0Q2FuY2VsbGVkICkge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdBUEkgUmVxdWVzdCBjYW5jZWxsZWQgYnkgYmVmb3Jlc2VuZCcpO1xuICAgICAgICAgICAgICAgICAgc2V0dGluZ3MuYWN0aXZhdGVUZXN0ICAgPSBmdW5jdGlvbigpeyByZXR1cm4gZmFsc2U7IH07XG4gICAgICAgICAgICAgICAgICBzZXR0aW5ncy5kZWFjdGl2YXRlVGVzdCA9IGZ1bmN0aW9uKCl7IHJldHVybiBmYWxzZTsgfTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSBpZihhcGlSZXF1ZXN0KSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUubGlzdGVuVG8oYXBpUmVxdWVzdCk7XG4gICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIG1vZHVsZS5jaGFuZ2Uuc3RhdGUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgbGlzdGVuVG86IGZ1bmN0aW9uKGFwaVJlcXVlc3QpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0FQSSByZXF1ZXN0IGRldGVjdGVkLCB3YWl0aW5nIGZvciBzdGF0ZSBzaWduYWwnLCBhcGlSZXF1ZXN0KTtcbiAgICAgICAgICBpZihhcGlSZXF1ZXN0KSB7XG4gICAgICAgICAgICBpZih0ZXh0LmxvYWRpbmcpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnVwZGF0ZS50ZXh0KHRleHQubG9hZGluZyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAkLndoZW4oYXBpUmVxdWVzdClcbiAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgaWYoYXBpUmVxdWVzdC5zdGF0ZSgpID09ICdyZXNvbHZlZCcpIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQVBJIHJlcXVlc3Qgc3VjY2VlZGVkJyk7XG4gICAgICAgICAgICAgICAgICBzZXR0aW5ncy5hY3RpdmF0ZVRlc3QgICA9IGZ1bmN0aW9uKCl7IHJldHVybiB0cnVlOyB9O1xuICAgICAgICAgICAgICAgICAgc2V0dGluZ3MuZGVhY3RpdmF0ZVRlc3QgPSBmdW5jdGlvbigpeyByZXR1cm4gdHJ1ZTsgfTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0FQSSByZXF1ZXN0IGZhaWxlZCcpO1xuICAgICAgICAgICAgICAgICAgc2V0dGluZ3MuYWN0aXZhdGVUZXN0ICAgPSBmdW5jdGlvbigpeyByZXR1cm4gZmFsc2U7IH07XG4gICAgICAgICAgICAgICAgICBzZXR0aW5ncy5kZWFjdGl2YXRlVGVzdCA9IGZ1bmN0aW9uKCl7IHJldHVybiBmYWxzZTsgfTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgbW9kdWxlLmNoYW5nZS5zdGF0ZSgpO1xuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICAvLyBjaGVja3Mgd2hldGhlciBhY3RpdmUvaW5hY3RpdmUgc3RhdGUgY2FuIGJlIGdpdmVuXG4gICAgICAgIGNoYW5nZToge1xuXG4gICAgICAgICAgc3RhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdEZXRlcm1pbmluZyBzdGF0ZSBjaGFuZ2UgZGlyZWN0aW9uJyk7XG4gICAgICAgICAgICAvLyBpbmFjdGl2ZSB0byBhY3RpdmUgY2hhbmdlXG4gICAgICAgICAgICBpZiggbW9kdWxlLmlzLmluYWN0aXZlKCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5hY3RpdmF0ZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWFjdGl2YXRlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5zeW5jKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5zeW5jKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzZXR0aW5ncy5vbkNoYW5nZS5jYWxsKGVsZW1lbnQpO1xuICAgICAgICAgIH0sXG5cbiAgICAgICAgICB0ZXh0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKCBtb2R1bGUuaXMudGV4dEVuYWJsZWQoKSApIHtcbiAgICAgICAgICAgICAgaWYobW9kdWxlLmlzLmRpc2FibGVkKCkgKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0NoYW5naW5nIHRleHQgdG8gZGlzYWJsZWQgdGV4dCcsIHRleHQuaG92ZXIpO1xuICAgICAgICAgICAgICAgIG1vZHVsZS51cGRhdGUudGV4dCh0ZXh0LmRpc2FibGVkKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBtb2R1bGUuaXMuYWN0aXZlKCkgKSB7XG4gICAgICAgICAgICAgICAgaWYodGV4dC5ob3Zlcikge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0NoYW5naW5nIHRleHQgdG8gaG92ZXIgdGV4dCcsIHRleHQuaG92ZXIpO1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnVwZGF0ZS50ZXh0KHRleHQuaG92ZXIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmKHRleHQuZGVhY3RpdmF0ZSkge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0NoYW5naW5nIHRleHQgdG8gZGVhY3RpdmF0aW5nIHRleHQnLCB0ZXh0LmRlYWN0aXZhdGUpO1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnVwZGF0ZS50ZXh0KHRleHQuZGVhY3RpdmF0ZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGlmKHRleHQuaG92ZXIpIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdDaGFuZ2luZyB0ZXh0IHRvIGhvdmVyIHRleHQnLCB0ZXh0LmhvdmVyKTtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS51cGRhdGUudGV4dCh0ZXh0LmhvdmVyKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSBpZih0ZXh0LmFjdGl2YXRlKXtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdDaGFuZ2luZyB0ZXh0IHRvIGFjdGl2YXRpbmcgdGV4dCcsIHRleHQuYWN0aXZhdGUpO1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnVwZGF0ZS50ZXh0KHRleHQuYWN0aXZhdGUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cblxuICAgICAgICB9LFxuXG4gICAgICAgIGFjdGl2YXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZiggc2V0dGluZ3MuYWN0aXZhdGVUZXN0LmNhbGwoZWxlbWVudCkgKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NldHRpbmcgc3RhdGUgdG8gYWN0aXZlJyk7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUuYWN0aXZlKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnVwZGF0ZS50ZXh0KHRleHQuYWN0aXZlKTtcbiAgICAgICAgICAgIHNldHRpbmdzLm9uQWN0aXZhdGUuY2FsbChlbGVtZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVhY3RpdmF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIHNldHRpbmdzLmRlYWN0aXZhdGVUZXN0LmNhbGwoZWxlbWVudCkgKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1NldHRpbmcgc3RhdGUgdG8gaW5hY3RpdmUnKTtcbiAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5hY3RpdmUpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBtb2R1bGUudXBkYXRlLnRleHQodGV4dC5pbmFjdGl2ZSk7XG4gICAgICAgICAgICBzZXR0aW5ncy5vbkRlYWN0aXZhdGUuY2FsbChlbGVtZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc3luYzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1N5bmNpbmcgb3RoZXIgYnV0dG9ucyB0byBjdXJyZW50IHN0YXRlJyk7XG4gICAgICAgICAgaWYoIG1vZHVsZS5pcy5hY3RpdmUoKSApIHtcbiAgICAgICAgICAgICRhbGxNb2R1bGVzXG4gICAgICAgICAgICAgIC5ub3QoJG1vZHVsZSlcbiAgICAgICAgICAgICAgICAuc3RhdGUoJ2FjdGl2YXRlJyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgJGFsbE1vZHVsZXNcbiAgICAgICAgICAgICAgLm5vdCgkbW9kdWxlKVxuICAgICAgICAgICAgICAgIC5zdGF0ZSgnZGVhY3RpdmF0ZScpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGdldDoge1xuICAgICAgICAgIHRleHQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIChzZXR0aW5ncy5zZWxlY3Rvci50ZXh0KVxuICAgICAgICAgICAgICA/ICRtb2R1bGUuZmluZChzZXR0aW5ncy5zZWxlY3Rvci50ZXh0KS50ZXh0KClcbiAgICAgICAgICAgICAgOiAkbW9kdWxlLmh0bWwoKVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgdGV4dEZvcjogZnVuY3Rpb24oc3RhdGUpIHtcbiAgICAgICAgICAgIHJldHVybiB0ZXh0W3N0YXRlXSB8fCBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZmxhc2g6IHtcbiAgICAgICAgICB0ZXh0OiBmdW5jdGlvbih0ZXh0LCBkdXJhdGlvbiwgY2FsbGJhY2spIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBwcmV2aW91c1RleHQgPSBtb2R1bGUuZ2V0LnRleHQoKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdGbGFzaGluZyB0ZXh0IG1lc3NhZ2UnLCB0ZXh0LCBkdXJhdGlvbik7XG4gICAgICAgICAgICB0ZXh0ICAgICA9IHRleHQgICAgIHx8IHNldHRpbmdzLnRleHQuZmxhc2g7XG4gICAgICAgICAgICBkdXJhdGlvbiA9IGR1cmF0aW9uIHx8IHNldHRpbmdzLmZsYXNoRHVyYXRpb247XG4gICAgICAgICAgICBjYWxsYmFjayA9IGNhbGxiYWNrIHx8IGZ1bmN0aW9uKCkge307XG4gICAgICAgICAgICBtb2R1bGUudXBkYXRlLnRleHQodGV4dCk7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgIG1vZHVsZS51cGRhdGUudGV4dChwcmV2aW91c1RleHQpO1xuICAgICAgICAgICAgICBjYWxsYmFjay5jYWxsKGVsZW1lbnQpO1xuICAgICAgICAgICAgfSwgZHVyYXRpb24pO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICByZXNldDoge1xuICAgICAgICAgIC8vIG9uIG1vdXNlb3V0IHNldHMgdGV4dCB0byBwcmV2aW91cyB2YWx1ZVxuICAgICAgICAgIHRleHQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGFjdGl2ZVRleHQgICA9IHRleHQuYWN0aXZlICAgfHwgJG1vZHVsZS5kYXRhKG1ldGFkYXRhLnN0b3JlZFRleHQpLFxuICAgICAgICAgICAgICBpbmFjdGl2ZVRleHQgPSB0ZXh0LmluYWN0aXZlIHx8ICRtb2R1bGUuZGF0YShtZXRhZGF0YS5zdG9yZWRUZXh0KVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoIG1vZHVsZS5pcy50ZXh0RW5hYmxlZCgpICkge1xuICAgICAgICAgICAgICBpZiggbW9kdWxlLmlzLmFjdGl2ZSgpICYmIGFjdGl2ZVRleHQpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVzZXR0aW5nIGFjdGl2ZSB0ZXh0JywgYWN0aXZlVGV4dCk7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnVwZGF0ZS50ZXh0KGFjdGl2ZVRleHQpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoaW5hY3RpdmVUZXh0KSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1Jlc2V0dGluZyBpbmFjdGl2ZSB0ZXh0JywgYWN0aXZlVGV4dCk7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnVwZGF0ZS50ZXh0KGluYWN0aXZlVGV4dCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgdXBkYXRlOiB7XG4gICAgICAgICAgdGV4dDogZnVuY3Rpb24odGV4dCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGN1cnJlbnRUZXh0ID0gbW9kdWxlLmdldC50ZXh0KClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHRleHQgJiYgdGV4dCAhPT0gY3VycmVudFRleHQpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdVcGRhdGluZyB0ZXh0JywgdGV4dCk7XG4gICAgICAgICAgICAgIGlmKHNldHRpbmdzLnNlbGVjdG9yLnRleHQpIHtcbiAgICAgICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgICAgICAuZGF0YShtZXRhZGF0YS5zdG9yZWRUZXh0LCB0ZXh0KVxuICAgICAgICAgICAgICAgICAgLmZpbmQoc2V0dGluZ3Muc2VsZWN0b3IudGV4dClcbiAgICAgICAgICAgICAgICAgICAgLnRleHQodGV4dClcbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAgICAgLmRhdGEobWV0YWRhdGEuc3RvcmVkVGV4dCwgdGV4dClcbiAgICAgICAgICAgICAgICAgIC5odG1sKHRleHQpXG4gICAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdUZXh0IGlzIGFscmVhZHkgc2V0LCBpZ25vcmluZyB1cGRhdGUnLCB0ZXh0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0dGluZzogZnVuY3Rpb24obmFtZSwgdmFsdWUpIHtcbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NoYW5naW5nIHNldHRpbmcnLCBuYW1lLCB2YWx1ZSk7XG4gICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChuYW1lKSApIHtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIHNldHRpbmdzLCBuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBpZigkLmlzUGxhaW5PYmplY3Qoc2V0dGluZ3NbbmFtZV0pKSB7XG4gICAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIHNldHRpbmdzW25hbWVdLCB2YWx1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgc2V0dGluZ3NbbmFtZV0gPSB2YWx1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gc2V0dGluZ3NbbmFtZV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnRlcm5hbDogZnVuY3Rpb24obmFtZSwgdmFsdWUpIHtcbiAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgbW9kdWxlLCBuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBtb2R1bGVbbmFtZV0gPSB2YWx1ZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZGVidWc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1Zy5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdmVyYm9zZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy52ZXJib3NlICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UubG9nKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBlcnJvcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCkge1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmVycm9yLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvci5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgcGVyZm9ybWFuY2U6IHtcbiAgICAgICAgICBsb2c6IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBjdXJyZW50VGltZSxcbiAgICAgICAgICAgICAgZXhlY3V0aW9uVGltZSxcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBjdXJyZW50VGltZSAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG4gICAgICAgICAgICAgIHByZXZpb3VzVGltZSAgPSB0aW1lIHx8IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lID0gY3VycmVudFRpbWUgLSBwcmV2aW91c1RpbWU7XG4gICAgICAgICAgICAgIHRpbWUgICAgICAgICAgPSBjdXJyZW50VGltZTtcbiAgICAgICAgICAgICAgcGVyZm9ybWFuY2UucHVzaCh7XG4gICAgICAgICAgICAgICAgJ05hbWUnICAgICAgICAgICA6IG1lc3NhZ2VbMF0sXG4gICAgICAgICAgICAgICAgJ0FyZ3VtZW50cycgICAgICA6IFtdLnNsaWNlLmNhbGwobWVzc2FnZSwgMSkgfHwgJycsXG4gICAgICAgICAgICAgICAgJ0VsZW1lbnQnICAgICAgICA6IGVsZW1lbnQsXG4gICAgICAgICAgICAgICAgJ0V4ZWN1dGlvbiBUaW1lJyA6IGV4ZWN1dGlvblRpbWVcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lciA9IHNldFRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLmRpc3BsYXksIDUwMCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXNwbGF5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICB0aXRsZSA9IHNldHRpbmdzLm5hbWUgKyAnOicsXG4gICAgICAgICAgICAgIHRvdGFsVGltZSA9IDBcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHRpbWUgPSBmYWxzZTtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICB0b3RhbFRpbWUgKz0gZGF0YVsnRXhlY3V0aW9uIFRpbWUnXTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgdGl0bGUgKz0gJyAnICsgdG90YWxUaW1lICsgJ21zJztcbiAgICAgICAgICAgIGlmKG1vZHVsZVNlbGVjdG9yKSB7XG4gICAgICAgICAgICAgIHRpdGxlICs9ICcgXFwnJyArIG1vZHVsZVNlbGVjdG9yICsgJ1xcJyc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiggKGNvbnNvbGUuZ3JvdXAgIT09IHVuZGVmaW5lZCB8fCBjb25zb2xlLnRhYmxlICE9PSB1bmRlZmluZWQpICYmIHBlcmZvcm1hbmNlLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgY29uc29sZS5ncm91cENvbGxhcHNlZCh0aXRsZSk7XG4gICAgICAgICAgICAgIGlmKGNvbnNvbGUudGFibGUpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLnRhYmxlKHBlcmZvcm1hbmNlKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAkLmVhY2gocGVyZm9ybWFuY2UsIGZ1bmN0aW9uKGluZGV4LCBkYXRhKSB7XG4gICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhWydOYW1lJ10gKyAnOiAnICsgZGF0YVsnRXhlY3V0aW9uIFRpbWUnXSsnbXMnKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwRW5kKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBwZXJmb3JtYW5jZSA9IFtdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgaW52b2tlOiBmdW5jdGlvbihxdWVyeSwgcGFzc2VkQXJndW1lbnRzLCBjb250ZXh0KSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICBvYmplY3QgPSBpbnN0YW5jZSxcbiAgICAgICAgICAgIG1heERlcHRoLFxuICAgICAgICAgICAgZm91bmQsXG4gICAgICAgICAgICByZXNwb25zZVxuICAgICAgICAgIDtcbiAgICAgICAgICBwYXNzZWRBcmd1bWVudHMgPSBwYXNzZWRBcmd1bWVudHMgfHwgcXVlcnlBcmd1bWVudHM7XG4gICAgICAgICAgY29udGV4dCAgICAgICAgID0gZWxlbWVudCAgICAgICAgIHx8IGNvbnRleHQ7XG4gICAgICAgICAgaWYodHlwZW9mIHF1ZXJ5ID09ICdzdHJpbmcnICYmIG9iamVjdCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBxdWVyeSAgICA9IHF1ZXJ5LnNwbGl0KC9bXFwuIF0vKTtcbiAgICAgICAgICAgIG1heERlcHRoID0gcXVlcnkubGVuZ3RoIC0gMTtcbiAgICAgICAgICAgICQuZWFjaChxdWVyeSwgZnVuY3Rpb24oZGVwdGgsIHZhbHVlKSB7XG4gICAgICAgICAgICAgIHZhciBjYW1lbENhc2VWYWx1ZSA9IChkZXB0aCAhPSBtYXhEZXB0aClcbiAgICAgICAgICAgICAgICA/IHZhbHVlICsgcXVlcnlbZGVwdGggKyAxXS5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIHF1ZXJ5W2RlcHRoICsgMV0uc2xpY2UoMSlcbiAgICAgICAgICAgICAgICA6IHF1ZXJ5XG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdCggb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXSApICYmIChkZXB0aCAhPSBtYXhEZXB0aCkgKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBvYmplY3RbY2FtZWxDYXNlVmFsdWVdICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCAkLmlzUGxhaW5PYmplY3QoIG9iamVjdFt2YWx1ZV0gKSAmJiAoZGVwdGggIT0gbWF4RGVwdGgpICkge1xuICAgICAgICAgICAgICAgIG9iamVjdCA9IG9iamVjdFt2YWx1ZV07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggb2JqZWN0W3ZhbHVlXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgICAgIGZvdW5kID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm1ldGhvZCwgcXVlcnkpO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmICggJC5pc0Z1bmN0aW9uKCBmb3VuZCApICkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZC5hcHBseShjb250ZXh0LCBwYXNzZWRBcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKGZvdW5kICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gZm91bmQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKCQuaXNBcnJheShyZXR1cm5lZFZhbHVlKSkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZS5wdXNoKHJlc3BvbnNlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSBbcmV0dXJuZWRWYWx1ZSwgcmVzcG9uc2VdO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJlc3BvbnNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSByZXNwb25zZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGZvdW5kO1xuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICBpZihtZXRob2RJbnZva2VkKSB7XG4gICAgICAgIGlmKGluc3RhbmNlID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbnZva2UocXVlcnkpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGlmKGluc3RhbmNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBpbnN0YW5jZS5pbnZva2UoJ2Rlc3Ryb3knKTtcbiAgICAgICAgfVxuICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgfVxuICAgIH0pXG4gIDtcblxuICByZXR1cm4gKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZClcbiAgICA/IHJldHVybmVkVmFsdWVcbiAgICA6IHRoaXNcbiAgO1xufTtcblxuJC5mbi5zdGF0ZS5zZXR0aW5ncyA9IHtcblxuICAvLyBtb2R1bGUgaW5mb1xuICBuYW1lICAgICAgICAgICA6ICdTdGF0ZScsXG5cbiAgLy8gZGVidWcgb3V0cHV0XG4gIGRlYnVnICAgICAgICAgIDogZmFsc2UsXG5cbiAgLy8gdmVyYm9zZSBkZWJ1ZyBvdXRwdXRcbiAgdmVyYm9zZSAgICAgICAgOiBmYWxzZSxcblxuICAvLyBuYW1lc3BhY2UgZm9yIGV2ZW50c1xuICBuYW1lc3BhY2UgICAgICA6ICdzdGF0ZScsXG5cbiAgLy8gZGVidWcgZGF0YSBpbmNsdWRlcyBwZXJmb3JtYW5jZVxuICBwZXJmb3JtYW5jZSAgICA6IHRydWUsXG5cbiAgLy8gY2FsbGJhY2sgb2NjdXJzIG9uIHN0YXRlIGNoYW5nZVxuICBvbkFjdGl2YXRlICAgICA6IGZ1bmN0aW9uKCkge30sXG4gIG9uRGVhY3RpdmF0ZSAgIDogZnVuY3Rpb24oKSB7fSxcbiAgb25DaGFuZ2UgICAgICAgOiBmdW5jdGlvbigpIHt9LFxuXG4gIC8vIHN0YXRlIHRlc3QgZnVuY3Rpb25zXG4gIGFjdGl2YXRlVGVzdCAgIDogZnVuY3Rpb24oKSB7IHJldHVybiB0cnVlOyB9LFxuICBkZWFjdGl2YXRlVGVzdCA6IGZ1bmN0aW9uKCkgeyByZXR1cm4gdHJ1ZTsgfSxcblxuICAvLyB3aGV0aGVyIHRvIGF1dG9tYXRpY2FsbHkgbWFwIGRlZmF1bHQgc3RhdGVzXG4gIGF1dG9tYXRpYyAgICAgIDogdHJ1ZSxcblxuICAvLyBhY3RpdmF0ZSAvIGRlYWN0aXZhdGUgY2hhbmdlcyBhbGwgZWxlbWVudHMgaW5zdGFudGlhdGVkIGF0IHNhbWUgdGltZVxuICBzeW5jICAgICAgICAgICA6IGZhbHNlLFxuXG4gIC8vIGRlZmF1bHQgZmxhc2ggdGV4dCBkdXJhdGlvbiwgdXNlZCBmb3IgdGVtcG9yYXJpbHkgY2hhbmdpbmcgdGV4dCBvZiBhbiBlbGVtZW50XG4gIGZsYXNoRHVyYXRpb24gIDogMTAwMCxcblxuICAvLyBzZWxlY3RvciBmaWx0ZXJcbiAgZmlsdGVyICAgICA6IHtcbiAgICB0ZXh0ICAgOiAnLmxvYWRpbmcsIC5kaXNhYmxlZCcsXG4gICAgYWN0aXZlIDogJy5kaXNhYmxlZCdcbiAgfSxcblxuICBjb250ZXh0ICAgIDogZmFsc2UsXG5cbiAgLy8gZXJyb3JcbiAgZXJyb3I6IHtcbiAgICBiZWZvcmVTZW5kIDogJ1RoZSBiZWZvcmUgc2VuZCBmdW5jdGlvbiBoYXMgY2FuY2VsbGVkIHN0YXRlIGNoYW5nZScsXG4gICAgbWV0aG9kICAgICA6ICdUaGUgbWV0aG9kIHlvdSBjYWxsZWQgaXMgbm90IGRlZmluZWQuJ1xuICB9LFxuXG4gIC8vIG1ldGFkYXRhXG4gIG1ldGFkYXRhOiB7XG4gICAgcHJvbWlzZSAgICA6ICdwcm9taXNlJyxcbiAgICBzdG9yZWRUZXh0IDogJ3N0b3JlZC10ZXh0J1xuICB9LFxuXG4gIC8vIGNoYW5nZSBjbGFzcyBvbiBzdGF0ZVxuICBjbGFzc05hbWU6IHtcbiAgICBhY3RpdmUgICA6ICdhY3RpdmUnLFxuICAgIGRpc2FibGVkIDogJ2Rpc2FibGVkJyxcbiAgICBlcnJvciAgICA6ICdlcnJvcicsXG4gICAgbG9hZGluZyAgOiAnbG9hZGluZycsXG4gICAgc3VjY2VzcyAgOiAnc3VjY2VzcycsXG4gICAgd2FybmluZyAgOiAnd2FybmluZydcbiAgfSxcblxuICBzZWxlY3Rvcjoge1xuICAgIC8vIHNlbGVjdG9yIGZvciB0ZXh0IG5vZGVcbiAgICB0ZXh0OiBmYWxzZVxuICB9LFxuXG4gIGRlZmF1bHRzIDoge1xuICAgIGlucHV0OiB7XG4gICAgICBkaXNhYmxlZCA6IHRydWUsXG4gICAgICBsb2FkaW5nICA6IHRydWUsXG4gICAgICBhY3RpdmUgICA6IHRydWVcbiAgICB9LFxuICAgIGJ1dHRvbjoge1xuICAgICAgZGlzYWJsZWQgOiB0cnVlLFxuICAgICAgbG9hZGluZyAgOiB0cnVlLFxuICAgICAgYWN0aXZlICAgOiB0cnVlLFxuICAgIH0sXG4gICAgcHJvZ3Jlc3M6IHtcbiAgICAgIGFjdGl2ZSAgIDogdHJ1ZSxcbiAgICAgIHN1Y2Nlc3MgIDogdHJ1ZSxcbiAgICAgIHdhcm5pbmcgIDogdHJ1ZSxcbiAgICAgIGVycm9yICAgIDogdHJ1ZVxuICAgIH1cbiAgfSxcblxuICBzdGF0ZXMgICAgIDoge1xuICAgIGFjdGl2ZSAgIDogdHJ1ZSxcbiAgICBkaXNhYmxlZCA6IHRydWUsXG4gICAgZXJyb3IgICAgOiB0cnVlLFxuICAgIGxvYWRpbmcgIDogdHJ1ZSxcbiAgICBzdWNjZXNzICA6IHRydWUsXG4gICAgd2FybmluZyAgOiB0cnVlXG4gIH0sXG5cbiAgdGV4dCAgICAgOiB7XG4gICAgZGlzYWJsZWQgICA6IGZhbHNlLFxuICAgIGZsYXNoICAgICAgOiBmYWxzZSxcbiAgICBob3ZlciAgICAgIDogZmFsc2UsXG4gICAgYWN0aXZlICAgICA6IGZhbHNlLFxuICAgIGluYWN0aXZlICAgOiBmYWxzZSxcbiAgICBhY3RpdmF0ZSAgIDogZmFsc2UsXG4gICAgZGVhY3RpdmF0ZSA6IGZhbHNlXG4gIH1cblxufTtcblxuXG5cbn0pKCBqUXVlcnksIHdpbmRvdywgZG9jdW1lbnQgKTtcbiJdfQ==