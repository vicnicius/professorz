/*!
 * # Semantic UI - Visit
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.visit = $.fn.visit = function (parameters) {
    var $allModules = $.isFunction(this) ? $(window) : $(this),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue;
    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.visit.settings, parameters) : $.extend({}, $.fn.visit.settings),
          error = settings.error,
          namespace = settings.namespace,
          eventNamespace = '.' + namespace,
          moduleNamespace = namespace + '-module',
          $module = $(this),
          $displays = $(),
          element = this,
          instance = $module.data(moduleNamespace),
          module;
      module = {

        initialize: function () {
          if (settings.count) {
            module.store(settings.key.count, settings.count);
          } else if (settings.id) {
            module.add.id(settings.id);
          } else if (settings.increment && methodInvoked !== 'increment') {
            module.increment();
          }
          module.add.display($module);
          module.instantiate();
        },

        instantiate: function () {
          module.verbose('Storing instance of visit module', module);
          instance = module;
          $module.data(moduleNamespace, module);
        },

        destroy: function () {
          module.verbose('Destroying instance');
          $module.removeData(moduleNamespace);
        },

        increment: function (id) {
          var currentValue = module.get.count(),
              newValue = +currentValue + 1;
          if (id) {
            module.add.id(id);
          } else {
            if (newValue > settings.limit && !settings.surpass) {
              newValue = settings.limit;
            }
            module.debug('Incrementing visits', newValue);
            module.store(settings.key.count, newValue);
          }
        },

        decrement: function (id) {
          var currentValue = module.get.count(),
              newValue = +currentValue - 1;
          if (id) {
            module.remove.id(id);
          } else {
            module.debug('Removing visit');
            module.store(settings.key.count, newValue);
          }
        },

        get: {
          count: function () {
            return +module.retrieve(settings.key.count) || 0;
          },
          idCount: function (ids) {
            ids = ids || module.get.ids();
            return ids.length;
          },
          ids: function (delimitedIDs) {
            var idArray = [];
            delimitedIDs = delimitedIDs || module.retrieve(settings.key.ids);
            if (typeof delimitedIDs === 'string') {
              idArray = delimitedIDs.split(settings.delimiter);
            }
            module.verbose('Found visited ID list', idArray);
            return idArray;
          },
          storageOptions: function (data) {
            var options = {};
            if (settings.expires) {
              options.expires = settings.expires;
            }
            if (settings.domain) {
              options.domain = settings.domain;
            }
            if (settings.path) {
              options.path = settings.path;
            }
            return options;
          }
        },

        has: {
          visited: function (id, ids) {
            var visited = false;
            ids = ids || module.get.ids();
            if (id !== undefined && ids) {
              $.each(ids, function (index, value) {
                if (value == id) {
                  visited = true;
                }
              });
            }
            return visited;
          }
        },

        set: {
          count: function (value) {
            module.store(settings.key.count, value);
          },
          ids: function (value) {
            module.store(settings.key.ids, value);
          }
        },

        reset: function () {
          module.store(settings.key.count, 0);
          module.store(settings.key.ids, null);
        },

        add: {
          id: function (id) {
            var currentIDs = module.retrieve(settings.key.ids),
                newIDs = currentIDs === undefined || currentIDs === '' ? id : currentIDs + settings.delimiter + id;
            if (module.has.visited(id)) {
              module.debug('Unique content already visited, not adding visit', id, currentIDs);
            } else if (id === undefined) {
              module.debug('ID is not defined');
            } else {
              module.debug('Adding visit to unique content', id);
              module.store(settings.key.ids, newIDs);
            }
            module.set.count(module.get.idCount());
          },
          display: function (selector) {
            var $element = $(selector);
            if ($element.length > 0 && !$.isWindow($element[0])) {
              module.debug('Updating visit count for element', $element);
              $displays = $displays.length > 0 ? $displays.add($element) : $element;
            }
          }
        },

        remove: {
          id: function (id) {
            var currentIDs = module.get.ids(),
                newIDs = [];
            if (id !== undefined && currentIDs !== undefined) {
              module.debug('Removing visit to unique content', id, currentIDs);
              $.each(currentIDs, function (index, value) {
                if (value !== id) {
                  newIDs.push(value);
                }
              });
              newIDs = newIDs.join(settings.delimiter);
              module.store(settings.key.ids, newIDs);
            }
            module.set.count(module.get.idCount());
          }
        },

        check: {
          limit: function (value) {
            value = value || module.get.count();
            if (settings.limit) {
              if (value >= settings.limit) {
                module.debug('Pages viewed exceeded limit, firing callback', value, settings.limit);
                settings.onLimit.call(element, value);
              }
              module.debug('Limit not reached', value, settings.limit);
              settings.onChange.call(element, value);
            }
            module.update.display(value);
          }
        },

        update: {
          display: function (value) {
            value = value || module.get.count();
            if ($displays.length > 0) {
              module.debug('Updating displayed view count', $displays);
              $displays.html(value);
            }
          }
        },

        store: function (key, value) {
          var options = module.get.storageOptions(value);
          if (settings.storageMethod == 'localstorage' && window.localStorage !== undefined) {
            window.localStorage.setItem(key, value);
            module.debug('Value stored using local storage', key, value);
          } else if ($.cookie !== undefined) {
            $.cookie(key, value, options);
            module.debug('Value stored using cookie', key, value, options);
          } else {
            module.error(error.noCookieStorage);
            return;
          }
          if (key == settings.key.count) {
            module.check.limit(value);
          }
        },
        retrieve: function (key, value) {
          var storedValue;
          if (settings.storageMethod == 'localstorage' && window.localStorage !== undefined) {
            storedValue = window.localStorage.getItem(key);
          }
          // get by cookie
          else if ($.cookie !== undefined) {
              storedValue = $.cookie(key);
            } else {
              module.error(error.noCookieStorage);
            }
          if (storedValue == 'undefined' || storedValue == 'null' || storedValue === undefined || storedValue === null) {
            storedValue = undefined;
          }
          return storedValue;
        },

        setting: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            settings[name] = value;
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          module.debug('Changing internal', name, value);
          if (value !== undefined) {
            if ($.isPlainObject(name)) {
              $.extend(true, module, name);
            } else {
              module[name] = value;
            }
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ($allModules.length > 1) {
              title += ' ' + '(' + $allModules.length + ')';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };
      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });
    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.visit.settings = {

    name: 'Visit',

    debug: false,
    verbose: false,
    performance: true,

    namespace: 'visit',

    increment: false,
    surpass: false,
    count: false,
    limit: false,

    delimiter: '&',
    storageMethod: 'localstorage',

    key: {
      count: 'visit-count',
      ids: 'visit-ids'
    },

    expires: 30,
    domain: false,
    path: '/',

    onLimit: function () {},
    onChange: function () {},

    error: {
      method: 'The method you called is not defined',
      missingPersist: 'Using the persist setting requires the inclusion of PersistJS',
      noCookieStorage: 'The default storage cookie requires $.cookie to be included.'
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9iZWhhdmlvcnMvdmlzaXQuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQVVBLENBQUMsQ0FBQyxVQUFVLENBQVYsRUFBYSxNQUFiLEVBQXFCLFFBQXJCLEVBQStCLFNBQS9CLEVBQTBDOztBQUU1Qzs7QUFFQSxXQUFVLE9BQU8sTUFBUCxJQUFpQixXQUFqQixJQUFnQyxPQUFPLElBQVAsSUFBZSxJQUFoRCxHQUNMLE1BREssR0FFSixPQUFPLElBQVAsSUFBZSxXQUFmLElBQThCLEtBQUssSUFBTCxJQUFhLElBQTVDLEdBQ0UsSUFERixHQUVFLFNBQVMsYUFBVCxHQUpOOztBQU9BLElBQUUsS0FBRixHQUFVLEVBQUUsRUFBRixDQUFLLEtBQUwsR0FBYSxVQUFTLFVBQVQsRUFBcUI7QUFDMUMsUUFDRSxjQUFrQixFQUFFLFVBQUYsQ0FBYSxJQUFiLElBQ1osRUFBRSxNQUFGLENBRFksR0FFWixFQUFFLElBQUYsQ0FIUjtBQUFBLFFBSUUsaUJBQWtCLFlBQVksUUFBWixJQUF3QixFQUo1QztBQUFBLFFBTUUsT0FBa0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQU5wQjtBQUFBLFFBT0UsY0FBa0IsRUFQcEI7QUFBQSxRQVNFLFFBQWtCLFVBQVUsQ0FBVixDQVRwQjtBQUFBLFFBVUUsZ0JBQW1CLE9BQU8sS0FBUCxJQUFnQixRQVZyQztBQUFBLFFBV0UsaUJBQWtCLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLENBQXpCLENBWHBCO0FBQUEsUUFZRSxhQVpGO0FBY0EsZ0JBQ0csSUFESCxDQUNRLFlBQVc7QUFDZixVQUNFLFdBQXNCLEVBQUUsYUFBRixDQUFnQixVQUFoQixDQUFGLEdBQ2hCLEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLEVBQUUsRUFBRixDQUFLLEtBQUwsQ0FBVyxRQUE5QixFQUF3QyxVQUF4QyxDQURnQixHQUVoQixFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsRUFBRSxFQUFGLENBQUssS0FBTCxDQUFXLFFBQXhCLENBSE47QUFBQSxVQUtFLFFBQWtCLFNBQVMsS0FMN0I7QUFBQSxVQU1FLFlBQWtCLFNBQVMsU0FON0I7QUFBQSxVQVFFLGlCQUFrQixNQUFNLFNBUjFCO0FBQUEsVUFTRSxrQkFBa0IsWUFBWSxTQVRoQztBQUFBLFVBV0UsVUFBa0IsRUFBRSxJQUFGLENBWHBCO0FBQUEsVUFZRSxZQUFrQixHQVpwQjtBQUFBLFVBY0UsVUFBa0IsSUFkcEI7QUFBQSxVQWVFLFdBQWtCLFFBQVEsSUFBUixDQUFhLGVBQWIsQ0FmcEI7QUFBQSxVQWdCRSxNQWhCRjtBQWtCQSxlQUFTOztBQUVQLG9CQUFZLFlBQVc7QUFDckIsY0FBRyxTQUFTLEtBQVosRUFBbUI7QUFDakIsbUJBQU8sS0FBUCxDQUFhLFNBQVMsR0FBVCxDQUFhLEtBQTFCLEVBQWlDLFNBQVMsS0FBMUM7QUFDRCxXQUZELE1BR0ssSUFBRyxTQUFTLEVBQVosRUFBZ0I7QUFDbkIsbUJBQU8sR0FBUCxDQUFXLEVBQVgsQ0FBYyxTQUFTLEVBQXZCO0FBQ0QsV0FGSSxNQUdBLElBQUcsU0FBUyxTQUFULElBQXNCLGtCQUFrQixXQUEzQyxFQUF3RDtBQUMzRCxtQkFBTyxTQUFQO0FBQ0Q7QUFDRCxpQkFBTyxHQUFQLENBQVcsT0FBWCxDQUFtQixPQUFuQjtBQUNBLGlCQUFPLFdBQVA7QUFDRCxTQWRNOztBQWdCUCxxQkFBYSxZQUFXO0FBQ3RCLGlCQUFPLE9BQVAsQ0FBZSxrQ0FBZixFQUFtRCxNQUFuRDtBQUNBLHFCQUFXLE1BQVg7QUFDQSxrQkFDRyxJQURILENBQ1EsZUFEUixFQUN5QixNQUR6QjtBQUdELFNBdEJNOztBQXdCUCxpQkFBUyxZQUFXO0FBQ2xCLGlCQUFPLE9BQVAsQ0FBZSxxQkFBZjtBQUNBLGtCQUNHLFVBREgsQ0FDYyxlQURkO0FBR0QsU0E3Qk07O0FBK0JQLG1CQUFXLFVBQVMsRUFBVCxFQUFhO0FBQ3RCLGNBQ0UsZUFBZSxPQUFPLEdBQVAsQ0FBVyxLQUFYLEVBRGpCO0FBQUEsY0FFRSxXQUFlLENBQUUsWUFBRixHQUFrQixDQUZuQztBQUlBLGNBQUcsRUFBSCxFQUFPO0FBQ0wsbUJBQU8sR0FBUCxDQUFXLEVBQVgsQ0FBYyxFQUFkO0FBQ0QsV0FGRCxNQUdLO0FBQ0gsZ0JBQUcsV0FBVyxTQUFTLEtBQXBCLElBQTZCLENBQUMsU0FBUyxPQUExQyxFQUFtRDtBQUNqRCx5QkFBVyxTQUFTLEtBQXBCO0FBQ0Q7QUFDRCxtQkFBTyxLQUFQLENBQWEscUJBQWIsRUFBb0MsUUFBcEM7QUFDQSxtQkFBTyxLQUFQLENBQWEsU0FBUyxHQUFULENBQWEsS0FBMUIsRUFBaUMsUUFBakM7QUFDRDtBQUNGLFNBOUNNOztBQWdEUCxtQkFBVyxVQUFTLEVBQVQsRUFBYTtBQUN0QixjQUNFLGVBQWUsT0FBTyxHQUFQLENBQVcsS0FBWCxFQURqQjtBQUFBLGNBRUUsV0FBZSxDQUFFLFlBQUYsR0FBa0IsQ0FGbkM7QUFJQSxjQUFHLEVBQUgsRUFBTztBQUNMLG1CQUFPLE1BQVAsQ0FBYyxFQUFkLENBQWlCLEVBQWpCO0FBQ0QsV0FGRCxNQUdLO0FBQ0gsbUJBQU8sS0FBUCxDQUFhLGdCQUFiO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLFNBQVMsR0FBVCxDQUFhLEtBQTFCLEVBQWlDLFFBQWpDO0FBQ0Q7QUFDRixTQTVETTs7QUE4RFAsYUFBSztBQUNILGlCQUFPLFlBQVc7QUFDaEIsbUJBQU8sQ0FBRSxPQUFPLFFBQVAsQ0FBZ0IsU0FBUyxHQUFULENBQWEsS0FBN0IsQ0FBRixJQUEwQyxDQUFqRDtBQUNELFdBSEU7QUFJSCxtQkFBUyxVQUFTLEdBQVQsRUFBYztBQUNyQixrQkFBTSxPQUFPLE9BQU8sR0FBUCxDQUFXLEdBQVgsRUFBYjtBQUNBLG1CQUFPLElBQUksTUFBWDtBQUNELFdBUEU7QUFRSCxlQUFLLFVBQVMsWUFBVCxFQUF1QjtBQUMxQixnQkFDRSxVQUFVLEVBRFo7QUFHQSwyQkFBZSxnQkFBZ0IsT0FBTyxRQUFQLENBQWdCLFNBQVMsR0FBVCxDQUFhLEdBQTdCLENBQS9CO0FBQ0EsZ0JBQUcsT0FBTyxZQUFQLEtBQXdCLFFBQTNCLEVBQXFDO0FBQ25DLHdCQUFVLGFBQWEsS0FBYixDQUFtQixTQUFTLFNBQTVCLENBQVY7QUFDRDtBQUNELG1CQUFPLE9BQVAsQ0FBZSx1QkFBZixFQUF3QyxPQUF4QztBQUNBLG1CQUFPLE9BQVA7QUFDRCxXQWxCRTtBQW1CSCwwQkFBZ0IsVUFBUyxJQUFULEVBQWU7QUFDN0IsZ0JBQ0UsVUFBVSxFQURaO0FBR0EsZ0JBQUcsU0FBUyxPQUFaLEVBQXFCO0FBQ25CLHNCQUFRLE9BQVIsR0FBa0IsU0FBUyxPQUEzQjtBQUNEO0FBQ0QsZ0JBQUcsU0FBUyxNQUFaLEVBQW9CO0FBQ2xCLHNCQUFRLE1BQVIsR0FBaUIsU0FBUyxNQUExQjtBQUNEO0FBQ0QsZ0JBQUcsU0FBUyxJQUFaLEVBQWtCO0FBQ2hCLHNCQUFRLElBQVIsR0FBZSxTQUFTLElBQXhCO0FBQ0Q7QUFDRCxtQkFBTyxPQUFQO0FBQ0Q7QUFqQ0UsU0E5REU7O0FBa0dQLGFBQUs7QUFDSCxtQkFBUyxVQUFTLEVBQVQsRUFBYSxHQUFiLEVBQWtCO0FBQ3pCLGdCQUNFLFVBQVUsS0FEWjtBQUdBLGtCQUFNLE9BQU8sT0FBTyxHQUFQLENBQVcsR0FBWCxFQUFiO0FBQ0EsZ0JBQUcsT0FBTyxTQUFQLElBQW9CLEdBQXZCLEVBQTRCO0FBQzFCLGdCQUFFLElBQUYsQ0FBTyxHQUFQLEVBQVksVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXNCO0FBQ2hDLG9CQUFHLFNBQVMsRUFBWixFQUFnQjtBQUNkLDRCQUFVLElBQVY7QUFDRDtBQUNGLGVBSkQ7QUFLRDtBQUNELG1CQUFPLE9BQVA7QUFDRDtBQWRFLFNBbEdFOztBQW1IUCxhQUFLO0FBQ0gsaUJBQU8sVUFBUyxLQUFULEVBQWdCO0FBQ3JCLG1CQUFPLEtBQVAsQ0FBYSxTQUFTLEdBQVQsQ0FBYSxLQUExQixFQUFpQyxLQUFqQztBQUNELFdBSEU7QUFJSCxlQUFLLFVBQVMsS0FBVCxFQUFnQjtBQUNuQixtQkFBTyxLQUFQLENBQWEsU0FBUyxHQUFULENBQWEsR0FBMUIsRUFBK0IsS0FBL0I7QUFDRDtBQU5FLFNBbkhFOztBQTRIUCxlQUFPLFlBQVc7QUFDaEIsaUJBQU8sS0FBUCxDQUFhLFNBQVMsR0FBVCxDQUFhLEtBQTFCLEVBQWlDLENBQWpDO0FBQ0EsaUJBQU8sS0FBUCxDQUFhLFNBQVMsR0FBVCxDQUFhLEdBQTFCLEVBQStCLElBQS9CO0FBQ0QsU0EvSE07O0FBaUlQLGFBQUs7QUFDSCxjQUFJLFVBQVMsRUFBVCxFQUFhO0FBQ2YsZ0JBQ0UsYUFBYSxPQUFPLFFBQVAsQ0FBZ0IsU0FBUyxHQUFULENBQWEsR0FBN0IsQ0FEZjtBQUFBLGdCQUVFLFNBQVUsZUFBZSxTQUFmLElBQTRCLGVBQWUsRUFBNUMsR0FDTCxFQURLLEdBRUwsYUFBYSxTQUFTLFNBQXRCLEdBQWtDLEVBSnhDO0FBTUEsZ0JBQUksT0FBTyxHQUFQLENBQVcsT0FBWCxDQUFtQixFQUFuQixDQUFKLEVBQTZCO0FBQzNCLHFCQUFPLEtBQVAsQ0FBYSxrREFBYixFQUFpRSxFQUFqRSxFQUFxRSxVQUFyRTtBQUNELGFBRkQsTUFHSyxJQUFHLE9BQU8sU0FBVixFQUFxQjtBQUN4QixxQkFBTyxLQUFQLENBQWEsbUJBQWI7QUFDRCxhQUZJLE1BR0E7QUFDSCxxQkFBTyxLQUFQLENBQWEsZ0NBQWIsRUFBK0MsRUFBL0M7QUFDQSxxQkFBTyxLQUFQLENBQWEsU0FBUyxHQUFULENBQWEsR0FBMUIsRUFBK0IsTUFBL0I7QUFDRDtBQUNELG1CQUFPLEdBQVAsQ0FBVyxLQUFYLENBQWtCLE9BQU8sR0FBUCxDQUFXLE9BQVgsRUFBbEI7QUFDRCxXQW5CRTtBQW9CSCxtQkFBUyxVQUFTLFFBQVQsRUFBbUI7QUFDMUIsZ0JBQ0UsV0FBVyxFQUFFLFFBQUYsQ0FEYjtBQUdBLGdCQUFHLFNBQVMsTUFBVCxHQUFrQixDQUFsQixJQUF1QixDQUFDLEVBQUUsUUFBRixDQUFXLFNBQVMsQ0FBVCxDQUFYLENBQTNCLEVBQW9EO0FBQ2xELHFCQUFPLEtBQVAsQ0FBYSxrQ0FBYixFQUFpRCxRQUFqRDtBQUNBLDBCQUFhLFVBQVUsTUFBVixHQUFtQixDQUFwQixHQUNSLFVBQVUsR0FBVixDQUFjLFFBQWQsQ0FEUSxHQUVSLFFBRko7QUFJRDtBQUNGO0FBL0JFLFNBaklFOztBQW1LUCxnQkFBUTtBQUNOLGNBQUksVUFBUyxFQUFULEVBQWE7QUFDZixnQkFDRSxhQUFhLE9BQU8sR0FBUCxDQUFXLEdBQVgsRUFEZjtBQUFBLGdCQUVFLFNBQWEsRUFGZjtBQUlBLGdCQUFHLE9BQU8sU0FBUCxJQUFvQixlQUFlLFNBQXRDLEVBQWlEO0FBQy9DLHFCQUFPLEtBQVAsQ0FBYSxrQ0FBYixFQUFpRCxFQUFqRCxFQUFxRCxVQUFyRDtBQUNBLGdCQUFFLElBQUYsQ0FBTyxVQUFQLEVBQW1CLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUFzQjtBQUN2QyxvQkFBRyxVQUFVLEVBQWIsRUFBaUI7QUFDZix5QkFBTyxJQUFQLENBQVksS0FBWjtBQUNEO0FBQ0YsZUFKRDtBQUtBLHVCQUFTLE9BQU8sSUFBUCxDQUFZLFNBQVMsU0FBckIsQ0FBVDtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxTQUFTLEdBQVQsQ0FBYSxHQUExQixFQUErQixNQUEvQjtBQUNEO0FBQ0QsbUJBQU8sR0FBUCxDQUFXLEtBQVgsQ0FBa0IsT0FBTyxHQUFQLENBQVcsT0FBWCxFQUFsQjtBQUNEO0FBakJLLFNBbktEOztBQXVMUCxlQUFPO0FBQ0wsaUJBQU8sVUFBUyxLQUFULEVBQWdCO0FBQ3JCLG9CQUFRLFNBQVMsT0FBTyxHQUFQLENBQVcsS0FBWCxFQUFqQjtBQUNBLGdCQUFHLFNBQVMsS0FBWixFQUFtQjtBQUNqQixrQkFBRyxTQUFTLFNBQVMsS0FBckIsRUFBNEI7QUFDMUIsdUJBQU8sS0FBUCxDQUFhLDhDQUFiLEVBQTZELEtBQTdELEVBQW9FLFNBQVMsS0FBN0U7QUFDQSx5QkFBUyxPQUFULENBQWlCLElBQWpCLENBQXNCLE9BQXRCLEVBQStCLEtBQS9CO0FBQ0Q7QUFDRCxxQkFBTyxLQUFQLENBQWEsbUJBQWIsRUFBa0MsS0FBbEMsRUFBeUMsU0FBUyxLQUFsRDtBQUNBLHVCQUFTLFFBQVQsQ0FBa0IsSUFBbEIsQ0FBdUIsT0FBdkIsRUFBZ0MsS0FBaEM7QUFDRDtBQUNELG1CQUFPLE1BQVAsQ0FBYyxPQUFkLENBQXNCLEtBQXRCO0FBQ0Q7QUFaSSxTQXZMQTs7QUFzTVAsZ0JBQVE7QUFDTixtQkFBUyxVQUFTLEtBQVQsRUFBZ0I7QUFDdkIsb0JBQVEsU0FBUyxPQUFPLEdBQVAsQ0FBVyxLQUFYLEVBQWpCO0FBQ0EsZ0JBQUcsVUFBVSxNQUFWLEdBQW1CLENBQXRCLEVBQXlCO0FBQ3ZCLHFCQUFPLEtBQVAsQ0FBYSwrQkFBYixFQUE4QyxTQUE5QztBQUNBLHdCQUFVLElBQVYsQ0FBZSxLQUFmO0FBQ0Q7QUFDRjtBQVBLLFNBdE1EOztBQWdOUCxlQUFPLFVBQVMsR0FBVCxFQUFjLEtBQWQsRUFBcUI7QUFDMUIsY0FDRSxVQUFVLE9BQU8sR0FBUCxDQUFXLGNBQVgsQ0FBMEIsS0FBMUIsQ0FEWjtBQUdBLGNBQUcsU0FBUyxhQUFULElBQTBCLGNBQTFCLElBQTRDLE9BQU8sWUFBUCxLQUF3QixTQUF2RSxFQUFrRjtBQUNoRixtQkFBTyxZQUFQLENBQW9CLE9BQXBCLENBQTRCLEdBQTVCLEVBQWlDLEtBQWpDO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLGtDQUFiLEVBQWlELEdBQWpELEVBQXNELEtBQXREO0FBQ0QsV0FIRCxNQUlLLElBQUcsRUFBRSxNQUFGLEtBQWEsU0FBaEIsRUFBMkI7QUFDOUIsY0FBRSxNQUFGLENBQVMsR0FBVCxFQUFjLEtBQWQsRUFBcUIsT0FBckI7QUFDQSxtQkFBTyxLQUFQLENBQWEsMkJBQWIsRUFBMEMsR0FBMUMsRUFBK0MsS0FBL0MsRUFBc0QsT0FBdEQ7QUFDRCxXQUhJLE1BSUE7QUFDSCxtQkFBTyxLQUFQLENBQWEsTUFBTSxlQUFuQjtBQUNBO0FBQ0Q7QUFDRCxjQUFHLE9BQU8sU0FBUyxHQUFULENBQWEsS0FBdkIsRUFBOEI7QUFDNUIsbUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsS0FBbkI7QUFDRDtBQUNGLFNBbk9NO0FBb09QLGtCQUFVLFVBQVMsR0FBVCxFQUFjLEtBQWQsRUFBcUI7QUFDN0IsY0FDRSxXQURGO0FBR0EsY0FBRyxTQUFTLGFBQVQsSUFBMEIsY0FBMUIsSUFBNEMsT0FBTyxZQUFQLEtBQXdCLFNBQXZFLEVBQWtGO0FBQ2hGLDBCQUFjLE9BQU8sWUFBUCxDQUFvQixPQUFwQixDQUE0QixHQUE1QixDQUFkO0FBQ0Q7O0FBRkQsZUFJSyxJQUFHLEVBQUUsTUFBRixLQUFhLFNBQWhCLEVBQTJCO0FBQzlCLDRCQUFjLEVBQUUsTUFBRixDQUFTLEdBQVQsQ0FBZDtBQUNELGFBRkksTUFHQTtBQUNILHFCQUFPLEtBQVAsQ0FBYSxNQUFNLGVBQW5CO0FBQ0Q7QUFDRCxjQUFHLGVBQWUsV0FBZixJQUE4QixlQUFlLE1BQTdDLElBQXVELGdCQUFnQixTQUF2RSxJQUFvRixnQkFBZ0IsSUFBdkcsRUFBNkc7QUFDM0csMEJBQWMsU0FBZDtBQUNEO0FBQ0QsaUJBQU8sV0FBUDtBQUNELFNBdFBNOztBQXdQUCxpQkFBUyxVQUFTLElBQVQsRUFBZSxLQUFmLEVBQXNCO0FBQzdCLGNBQUksRUFBRSxhQUFGLENBQWdCLElBQWhCLENBQUosRUFBNEI7QUFDMUIsY0FBRSxNQUFGLENBQVMsSUFBVCxFQUFlLFFBQWYsRUFBeUIsSUFBekI7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IscUJBQVMsSUFBVCxJQUFpQixLQUFqQjtBQUNELFdBRkksTUFHQTtBQUNILG1CQUFPLFNBQVMsSUFBVCxDQUFQO0FBQ0Q7QUFDRixTQWxRTTtBQW1RUCxrQkFBVSxVQUFTLElBQVQsRUFBZSxLQUFmLEVBQXNCO0FBQzlCLGlCQUFPLEtBQVAsQ0FBYSxtQkFBYixFQUFrQyxJQUFsQyxFQUF3QyxLQUF4QztBQUNBLGNBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQ3RCLGdCQUFJLEVBQUUsYUFBRixDQUFnQixJQUFoQixDQUFKLEVBQTRCO0FBQzFCLGdCQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsTUFBZixFQUF1QixJQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLElBQVAsSUFBZSxLQUFmO0FBQ0Q7QUFDRixXQVBELE1BUUs7QUFDSCxtQkFBTyxPQUFPLElBQVAsQ0FBUDtBQUNEO0FBQ0YsU0FoUk07QUFpUlAsZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQVYsSUFBb0IsU0FBUyxLQUFoQyxFQUF1QztBQUNyQyxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLEtBQVAsR0FBZSxTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxJQUFyQyxFQUEyQyxPQUEzQyxFQUFvRCxTQUFTLElBQVQsR0FBZ0IsR0FBcEUsQ0FBZjtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLE9BQW5CLEVBQTRCLFNBQTVCO0FBQ0Q7QUFDRjtBQUNGLFNBM1JNO0FBNFJQLGlCQUFTLFlBQVc7QUFDbEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLE9BQTdCLElBQXdDLFNBQVMsS0FBcEQsRUFBMkQ7QUFDekQsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLHFCQUFPLFdBQVAsQ0FBbUIsR0FBbkIsQ0FBdUIsU0FBdkI7QUFDRCxhQUZELE1BR0s7QUFDSCxxQkFBTyxPQUFQLEdBQWlCLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFqQjtBQUNBLHFCQUFPLE9BQVAsQ0FBZSxLQUFmLENBQXFCLE9BQXJCLEVBQThCLFNBQTlCO0FBQ0Q7QUFDRjtBQUNGLFNBdFNNO0FBdVNQLGVBQU8sWUFBVztBQUNoQixjQUFHLENBQUMsU0FBUyxNQUFiLEVBQXFCO0FBQ25CLG1CQUFPLEtBQVAsR0FBZSxTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxLQUFyQyxFQUE0QyxPQUE1QyxFQUFxRCxTQUFTLElBQVQsR0FBZ0IsR0FBckUsQ0FBZjtBQUNBLG1CQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLE9BQW5CLEVBQTRCLFNBQTVCO0FBQ0Q7QUFDRixTQTVTTTtBQTZTUCxxQkFBYTtBQUNYLGVBQUssVUFBUyxPQUFULEVBQWtCO0FBQ3JCLGdCQUNFLFdBREYsRUFFRSxhQUZGLEVBR0UsWUFIRjtBQUtBLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2Qiw0QkFBZ0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUFoQjtBQUNBLDZCQUFnQixRQUFRLFdBQXhCO0FBQ0EsOEJBQWdCLGNBQWMsWUFBOUI7QUFDQSxxQkFBZ0IsV0FBaEI7QUFDQSwwQkFBWSxJQUFaLENBQWlCO0FBQ2Ysd0JBQW1CLFFBQVEsQ0FBUixDQURKO0FBRWYsNkJBQW1CLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxPQUFkLEVBQXVCLENBQXZCLEtBQTZCLEVBRmpDO0FBR2YsMkJBQW1CLE9BSEo7QUFJZixrQ0FBbUI7QUFKSixlQUFqQjtBQU1EO0FBQ0QseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsbUJBQU8sV0FBUCxDQUFtQixLQUFuQixHQUEyQixXQUFXLE9BQU8sV0FBUCxDQUFtQixPQUE5QixFQUF1QyxHQUF2QyxDQUEzQjtBQUNELFdBckJVO0FBc0JYLG1CQUFTLFlBQVc7QUFDbEIsZ0JBQ0UsUUFBUSxTQUFTLElBQVQsR0FBZ0IsR0FEMUI7QUFBQSxnQkFFRSxZQUFZLENBRmQ7QUFJQSxtQkFBTyxLQUFQO0FBQ0EseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsY0FBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMkJBQWEsS0FBSyxnQkFBTCxDQUFiO0FBQ0QsYUFGRDtBQUdBLHFCQUFTLE1BQU0sU0FBTixHQUFrQixJQUEzQjtBQUNBLGdCQUFHLGNBQUgsRUFBbUI7QUFDakIsdUJBQVMsUUFBUSxjQUFSLEdBQXlCLElBQWxDO0FBQ0Q7QUFDRCxnQkFBRyxZQUFZLE1BQVosR0FBcUIsQ0FBeEIsRUFBMkI7QUFDekIsdUJBQVMsTUFBTSxHQUFOLEdBQVksWUFBWSxNQUF4QixHQUFpQyxHQUExQztBQUNEO0FBQ0QsZ0JBQUksQ0FBQyxRQUFRLEtBQVIsS0FBa0IsU0FBbEIsSUFBK0IsUUFBUSxLQUFSLEtBQWtCLFNBQWxELEtBQWdFLFlBQVksTUFBWixHQUFxQixDQUF6RixFQUE0RjtBQUMxRixzQkFBUSxjQUFSLENBQXVCLEtBQXZCO0FBQ0Esa0JBQUcsUUFBUSxLQUFYLEVBQWtCO0FBQ2hCLHdCQUFRLEtBQVIsQ0FBYyxXQUFkO0FBQ0QsZUFGRCxNQUdLO0FBQ0gsa0JBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDBCQUFRLEdBQVIsQ0FBWSxLQUFLLE1BQUwsSUFBZSxJQUFmLEdBQXNCLEtBQUssZ0JBQUwsQ0FBdEIsR0FBNkMsSUFBekQ7QUFDRCxpQkFGRDtBQUdEO0FBQ0Qsc0JBQVEsUUFBUjtBQUNEO0FBQ0QsMEJBQWMsRUFBZDtBQUNEO0FBcERVLFNBN1NOO0FBbVdQLGdCQUFRLFVBQVMsS0FBVCxFQUFnQixlQUFoQixFQUFpQyxPQUFqQyxFQUEwQztBQUNoRCxjQUNFLFNBQVMsUUFEWDtBQUFBLGNBRUUsUUFGRjtBQUFBLGNBR0UsS0FIRjtBQUFBLGNBSUUsUUFKRjtBQU1BLDRCQUFrQixtQkFBbUIsY0FBckM7QUFDQSxvQkFBa0IsV0FBbUIsT0FBckM7QUFDQSxjQUFHLE9BQU8sS0FBUCxJQUFnQixRQUFoQixJQUE0QixXQUFXLFNBQTFDLEVBQXFEO0FBQ25ELG9CQUFXLE1BQU0sS0FBTixDQUFZLE9BQVosQ0FBWDtBQUNBLHVCQUFXLE1BQU0sTUFBTixHQUFlLENBQTFCO0FBQ0EsY0FBRSxJQUFGLENBQU8sS0FBUCxFQUFjLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUF1QjtBQUNuQyxrQkFBSSxpQkFBa0IsU0FBUyxRQUFWLEdBQ2pCLFFBQVEsTUFBTSxRQUFRLENBQWQsRUFBaUIsTUFBakIsQ0FBd0IsQ0FBeEIsRUFBMkIsV0FBM0IsRUFBUixHQUFtRCxNQUFNLFFBQVEsQ0FBZCxFQUFpQixLQUFqQixDQUF1QixDQUF2QixDQURsQyxHQUVqQixLQUZKO0FBSUEsa0JBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sY0FBUCxDQUFqQixLQUE4QyxTQUFTLFFBQTNELEVBQXVFO0FBQ3JFLHlCQUFTLE9BQU8sY0FBUCxDQUFUO0FBQ0QsZUFGRCxNQUdLLElBQUksT0FBTyxjQUFQLE1BQTJCLFNBQS9CLEVBQTJDO0FBQzlDLHdCQUFRLE9BQU8sY0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQSxJQUFJLEVBQUUsYUFBRixDQUFpQixPQUFPLEtBQVAsQ0FBakIsS0FBcUMsU0FBUyxRQUFsRCxFQUE4RDtBQUNqRSx5QkFBUyxPQUFPLEtBQVAsQ0FBVDtBQUNELGVBRkksTUFHQSxJQUFJLE9BQU8sS0FBUCxNQUFrQixTQUF0QixFQUFrQztBQUNyQyx3QkFBUSxPQUFPLEtBQVAsQ0FBUjtBQUNBLHVCQUFPLEtBQVA7QUFDRCxlQUhJLE1BSUE7QUFDSCx1QkFBTyxLQUFQO0FBQ0Q7QUFDRixhQXRCRDtBQXVCRDtBQUNELGNBQUssRUFBRSxVQUFGLENBQWMsS0FBZCxDQUFMLEVBQTZCO0FBQzNCLHVCQUFXLE1BQU0sS0FBTixDQUFZLE9BQVosRUFBcUIsZUFBckIsQ0FBWDtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQix1QkFBVyxLQUFYO0FBQ0Q7QUFDRCxjQUFHLEVBQUUsT0FBRixDQUFVLGFBQVYsQ0FBSCxFQUE2QjtBQUMzQiwwQkFBYyxJQUFkLENBQW1CLFFBQW5CO0FBQ0QsV0FGRCxNQUdLLElBQUcsa0JBQWtCLFNBQXJCLEVBQWdDO0FBQ25DLDRCQUFnQixDQUFDLGFBQUQsRUFBZ0IsUUFBaEIsQ0FBaEI7QUFDRCxXQUZJLE1BR0EsSUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQzlCLDRCQUFnQixRQUFoQjtBQUNEO0FBQ0QsaUJBQU8sS0FBUDtBQUNEO0FBdlpNLE9BQVQ7QUF5WkEsVUFBRyxhQUFILEVBQWtCO0FBQ2hCLFlBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUN6QixpQkFBTyxVQUFQO0FBQ0Q7QUFDRCxlQUFPLE1BQVAsQ0FBYyxLQUFkO0FBQ0QsT0FMRCxNQU1LO0FBQ0gsWUFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLG1CQUFTLE1BQVQsQ0FBZ0IsU0FBaEI7QUFDRDtBQUNELGVBQU8sVUFBUDtBQUNEO0FBRUYsS0ExYkg7QUE0YkEsV0FBUSxrQkFBa0IsU0FBbkIsR0FDSCxhQURHLEdBRUgsSUFGSjtBQUlELEdBL2NEOztBQWlkQSxJQUFFLEVBQUYsQ0FBSyxLQUFMLENBQVcsUUFBWCxHQUFzQjs7QUFFcEIsVUFBZ0IsT0FGSTs7QUFJcEIsV0FBZ0IsS0FKSTtBQUtwQixhQUFnQixLQUxJO0FBTXBCLGlCQUFnQixJQU5JOztBQVFwQixlQUFnQixPQVJJOztBQVVwQixlQUFnQixLQVZJO0FBV3BCLGFBQWdCLEtBWEk7QUFZcEIsV0FBZ0IsS0FaSTtBQWFwQixXQUFnQixLQWJJOztBQWVwQixlQUFnQixHQWZJO0FBZ0JwQixtQkFBZ0IsY0FoQkk7O0FBa0JwQixTQUFnQjtBQUNkLGFBQVEsYUFETTtBQUVkLFdBQVE7QUFGTSxLQWxCSTs7QUF1QnBCLGFBQWdCLEVBdkJJO0FBd0JwQixZQUFnQixLQXhCSTtBQXlCcEIsVUFBZ0IsR0F6Qkk7O0FBMkJwQixhQUFnQixZQUFXLENBQUUsQ0EzQlQ7QUE0QnBCLGNBQWdCLFlBQVcsQ0FBRSxDQTVCVDs7QUE4QnBCLFdBQWdCO0FBQ2QsY0FBa0Isc0NBREo7QUFFZCxzQkFBa0IsK0RBRko7QUFHZCx1QkFBa0I7QUFISjs7QUE5QkksR0FBdEI7QUFzQ0MsQ0FsZ0JBLEVBa2dCRyxNQWxnQkgsRUFrZ0JXLE1BbGdCWCxFQWtnQm1CLFFBbGdCbkIiLCJmaWxlIjoidmlzaXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcbiAqICMgU2VtYW50aWMgVUkgLSBWaXNpdFxuICogaHR0cDovL2dpdGh1Yi5jb20vc2VtYW50aWMtb3JnL3NlbWFudGljLXVpL1xuICpcbiAqXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2VcbiAqIGh0dHA6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9NSVRcbiAqXG4gKi9cblxuOyhmdW5jdGlvbiAoJCwgd2luZG93LCBkb2N1bWVudCwgdW5kZWZpbmVkKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG53aW5kb3cgPSAodHlwZW9mIHdpbmRvdyAhPSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuTWF0aCA9PSBNYXRoKVxuICA/IHdpbmRvd1xuICA6ICh0eXBlb2Ygc2VsZiAhPSAndW5kZWZpbmVkJyAmJiBzZWxmLk1hdGggPT0gTWF0aClcbiAgICA/IHNlbGZcbiAgICA6IEZ1bmN0aW9uKCdyZXR1cm4gdGhpcycpKClcbjtcblxuJC52aXNpdCA9ICQuZm4udmlzaXQgPSBmdW5jdGlvbihwYXJhbWV0ZXJzKSB7XG4gIHZhclxuICAgICRhbGxNb2R1bGVzICAgICA9ICQuaXNGdW5jdGlvbih0aGlzKVxuICAgICAgICA/ICQod2luZG93KVxuICAgICAgICA6ICQodGhpcyksXG4gICAgbW9kdWxlU2VsZWN0b3IgID0gJGFsbE1vZHVsZXMuc2VsZWN0b3IgfHwgJycsXG5cbiAgICB0aW1lICAgICAgICAgICAgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKSxcbiAgICBwZXJmb3JtYW5jZSAgICAgPSBbXSxcblxuICAgIHF1ZXJ5ICAgICAgICAgICA9IGFyZ3VtZW50c1swXSxcbiAgICBtZXRob2RJbnZva2VkICAgPSAodHlwZW9mIHF1ZXJ5ID09ICdzdHJpbmcnKSxcbiAgICBxdWVyeUFyZ3VtZW50cyAgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSksXG4gICAgcmV0dXJuZWRWYWx1ZVxuICA7XG4gICRhbGxNb2R1bGVzXG4gICAgLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICB2YXJcbiAgICAgICAgc2V0dGluZ3MgICAgICAgICAgPSAoICQuaXNQbGFpbk9iamVjdChwYXJhbWV0ZXJzKSApXG4gICAgICAgICAgPyAkLmV4dGVuZCh0cnVlLCB7fSwgJC5mbi52aXNpdC5zZXR0aW5ncywgcGFyYW1ldGVycylcbiAgICAgICAgICA6ICQuZXh0ZW5kKHt9LCAkLmZuLnZpc2l0LnNldHRpbmdzKSxcblxuICAgICAgICBlcnJvciAgICAgICAgICAgPSBzZXR0aW5ncy5lcnJvcixcbiAgICAgICAgbmFtZXNwYWNlICAgICAgID0gc2V0dGluZ3MubmFtZXNwYWNlLFxuXG4gICAgICAgIGV2ZW50TmFtZXNwYWNlICA9ICcuJyArIG5hbWVzcGFjZSxcbiAgICAgICAgbW9kdWxlTmFtZXNwYWNlID0gbmFtZXNwYWNlICsgJy1tb2R1bGUnLFxuXG4gICAgICAgICRtb2R1bGUgICAgICAgICA9ICQodGhpcyksXG4gICAgICAgICRkaXNwbGF5cyAgICAgICA9ICQoKSxcblxuICAgICAgICBlbGVtZW50ICAgICAgICAgPSB0aGlzLFxuICAgICAgICBpbnN0YW5jZSAgICAgICAgPSAkbW9kdWxlLmRhdGEobW9kdWxlTmFtZXNwYWNlKSxcbiAgICAgICAgbW9kdWxlXG4gICAgICA7XG4gICAgICBtb2R1bGUgPSB7XG5cbiAgICAgICAgaW5pdGlhbGl6ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoc2V0dGluZ3MuY291bnQpIHtcbiAgICAgICAgICAgIG1vZHVsZS5zdG9yZShzZXR0aW5ncy5rZXkuY291bnQsIHNldHRpbmdzLmNvdW50KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihzZXR0aW5ncy5pZCkge1xuICAgICAgICAgICAgbW9kdWxlLmFkZC5pZChzZXR0aW5ncy5pZCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYoc2V0dGluZ3MuaW5jcmVtZW50ICYmIG1ldGhvZEludm9rZWQgIT09ICdpbmNyZW1lbnQnKSB7XG4gICAgICAgICAgICBtb2R1bGUuaW5jcmVtZW50KCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIG1vZHVsZS5hZGQuZGlzcGxheSgkbW9kdWxlKTtcbiAgICAgICAgICBtb2R1bGUuaW5zdGFudGlhdGUoKTtcbiAgICAgICAgfSxcblxuICAgICAgICBpbnN0YW50aWF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1N0b3JpbmcgaW5zdGFuY2Ugb2YgdmlzaXQgbW9kdWxlJywgbW9kdWxlKTtcbiAgICAgICAgICBpbnN0YW5jZSA9IG1vZHVsZTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAuZGF0YShtb2R1bGVOYW1lc3BhY2UsIG1vZHVsZSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVzdHJveTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0Rlc3Ryb3lpbmcgaW5zdGFuY2UnKTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAucmVtb3ZlRGF0YShtb2R1bGVOYW1lc3BhY2UpXG4gICAgICAgICAgO1xuICAgICAgICB9LFxuXG4gICAgICAgIGluY3JlbWVudDogZnVuY3Rpb24oaWQpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIGN1cnJlbnRWYWx1ZSA9IG1vZHVsZS5nZXQuY291bnQoKSxcbiAgICAgICAgICAgIG5ld1ZhbHVlICAgICA9ICsoY3VycmVudFZhbHVlKSArIDFcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYoaWQpIHtcbiAgICAgICAgICAgIG1vZHVsZS5hZGQuaWQoaWQpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGlmKG5ld1ZhbHVlID4gc2V0dGluZ3MubGltaXQgJiYgIXNldHRpbmdzLnN1cnBhc3MpIHtcbiAgICAgICAgICAgICAgbmV3VmFsdWUgPSBzZXR0aW5ncy5saW1pdDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW5jcmVtZW50aW5nIHZpc2l0cycsIG5ld1ZhbHVlKTtcbiAgICAgICAgICAgIG1vZHVsZS5zdG9yZShzZXR0aW5ncy5rZXkuY291bnQsIG5ld1ZhbHVlKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVjcmVtZW50OiBmdW5jdGlvbihpZCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgY3VycmVudFZhbHVlID0gbW9kdWxlLmdldC5jb3VudCgpLFxuICAgICAgICAgICAgbmV3VmFsdWUgICAgID0gKyhjdXJyZW50VmFsdWUpIC0gMVxuICAgICAgICAgIDtcbiAgICAgICAgICBpZihpZCkge1xuICAgICAgICAgICAgbW9kdWxlLnJlbW92ZS5pZChpZCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZW1vdmluZyB2aXNpdCcpO1xuICAgICAgICAgICAgbW9kdWxlLnN0b3JlKHNldHRpbmdzLmtleS5jb3VudCwgbmV3VmFsdWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBnZXQ6IHtcbiAgICAgICAgICBjb3VudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gKyhtb2R1bGUucmV0cmlldmUoc2V0dGluZ3Mua2V5LmNvdW50KSkgfHwgMDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGlkQ291bnQ6IGZ1bmN0aW9uKGlkcykge1xuICAgICAgICAgICAgaWRzID0gaWRzIHx8IG1vZHVsZS5nZXQuaWRzKCk7XG4gICAgICAgICAgICByZXR1cm4gaWRzLmxlbmd0aDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGlkczogZnVuY3Rpb24oZGVsaW1pdGVkSURzKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgaWRBcnJheSA9IFtdXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBkZWxpbWl0ZWRJRHMgPSBkZWxpbWl0ZWRJRHMgfHwgbW9kdWxlLnJldHJpZXZlKHNldHRpbmdzLmtleS5pZHMpO1xuICAgICAgICAgICAgaWYodHlwZW9mIGRlbGltaXRlZElEcyA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgaWRBcnJheSA9IGRlbGltaXRlZElEcy5zcGxpdChzZXR0aW5ncy5kZWxpbWl0ZXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0ZvdW5kIHZpc2l0ZWQgSUQgbGlzdCcsIGlkQXJyYXkpO1xuICAgICAgICAgICAgcmV0dXJuIGlkQXJyYXk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzdG9yYWdlT3B0aW9uczogZnVuY3Rpb24oZGF0YSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIG9wdGlvbnMgPSB7fVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MuZXhwaXJlcykge1xuICAgICAgICAgICAgICBvcHRpb25zLmV4cGlyZXMgPSBzZXR0aW5ncy5leHBpcmVzO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoc2V0dGluZ3MuZG9tYWluKSB7XG4gICAgICAgICAgICAgIG9wdGlvbnMuZG9tYWluID0gc2V0dGluZ3MuZG9tYWluO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGF0aCkge1xuICAgICAgICAgICAgICBvcHRpb25zLnBhdGggPSBzZXR0aW5ncy5wYXRoO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG9wdGlvbnM7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGhhczoge1xuICAgICAgICAgIHZpc2l0ZWQ6IGZ1bmN0aW9uKGlkLCBpZHMpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICB2aXNpdGVkID0gZmFsc2VcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlkcyA9IGlkcyB8fCBtb2R1bGUuZ2V0LmlkcygpO1xuICAgICAgICAgICAgaWYoaWQgIT09IHVuZGVmaW5lZCAmJiBpZHMpIHtcbiAgICAgICAgICAgICAgJC5lYWNoKGlkcywgZnVuY3Rpb24oaW5kZXgsIHZhbHVlKXtcbiAgICAgICAgICAgICAgICBpZih2YWx1ZSA9PSBpZCkge1xuICAgICAgICAgICAgICAgICAgdmlzaXRlZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB2aXNpdGVkO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZXQ6IHtcbiAgICAgICAgICBjb3VudDogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgICAgIG1vZHVsZS5zdG9yZShzZXR0aW5ncy5rZXkuY291bnQsIHZhbHVlKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGlkczogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgICAgIG1vZHVsZS5zdG9yZShzZXR0aW5ncy5rZXkuaWRzLCB2YWx1ZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlc2V0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUuc3RvcmUoc2V0dGluZ3Mua2V5LmNvdW50LCAwKTtcbiAgICAgICAgICBtb2R1bGUuc3RvcmUoc2V0dGluZ3Mua2V5LmlkcywgbnVsbCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgYWRkOiB7XG4gICAgICAgICAgaWQ6IGZ1bmN0aW9uKGlkKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgY3VycmVudElEcyA9IG1vZHVsZS5yZXRyaWV2ZShzZXR0aW5ncy5rZXkuaWRzKSxcbiAgICAgICAgICAgICAgbmV3SURzID0gKGN1cnJlbnRJRHMgPT09IHVuZGVmaW5lZCB8fCBjdXJyZW50SURzID09PSAnJylcbiAgICAgICAgICAgICAgICA/IGlkXG4gICAgICAgICAgICAgICAgOiBjdXJyZW50SURzICsgc2V0dGluZ3MuZGVsaW1pdGVyICsgaWRcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKCBtb2R1bGUuaGFzLnZpc2l0ZWQoaWQpICkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1VuaXF1ZSBjb250ZW50IGFscmVhZHkgdmlzaXRlZCwgbm90IGFkZGluZyB2aXNpdCcsIGlkLCBjdXJyZW50SURzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoaWQgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0lEIGlzIG5vdCBkZWZpbmVkJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdBZGRpbmcgdmlzaXQgdG8gdW5pcXVlIGNvbnRlbnQnLCBpZCk7XG4gICAgICAgICAgICAgIG1vZHVsZS5zdG9yZShzZXR0aW5ncy5rZXkuaWRzLCBuZXdJRHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLnNldC5jb3VudCggbW9kdWxlLmdldC5pZENvdW50KCkgKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc3BsYXk6IGZ1bmN0aW9uKHNlbGVjdG9yKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgJGVsZW1lbnQgPSAkKHNlbGVjdG9yKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoJGVsZW1lbnQubGVuZ3RoID4gMCAmJiAhJC5pc1dpbmRvdygkZWxlbWVudFswXSkpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdVcGRhdGluZyB2aXNpdCBjb3VudCBmb3IgZWxlbWVudCcsICRlbGVtZW50KTtcbiAgICAgICAgICAgICAgJGRpc3BsYXlzID0gKCRkaXNwbGF5cy5sZW5ndGggPiAwKVxuICAgICAgICAgICAgICAgID8gJGRpc3BsYXlzLmFkZCgkZWxlbWVudClcbiAgICAgICAgICAgICAgICA6ICRlbGVtZW50XG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVtb3ZlOiB7XG4gICAgICAgICAgaWQ6IGZ1bmN0aW9uKGlkKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgY3VycmVudElEcyA9IG1vZHVsZS5nZXQuaWRzKCksXG4gICAgICAgICAgICAgIG5ld0lEcyAgICAgPSBbXVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoaWQgIT09IHVuZGVmaW5lZCAmJiBjdXJyZW50SURzICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZW1vdmluZyB2aXNpdCB0byB1bmlxdWUgY29udGVudCcsIGlkLCBjdXJyZW50SURzKTtcbiAgICAgICAgICAgICAgJC5lYWNoKGN1cnJlbnRJRHMsIGZ1bmN0aW9uKGluZGV4LCB2YWx1ZSl7XG4gICAgICAgICAgICAgICAgaWYodmFsdWUgIT09IGlkKSB7XG4gICAgICAgICAgICAgICAgICBuZXdJRHMucHVzaCh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgbmV3SURzID0gbmV3SURzLmpvaW4oc2V0dGluZ3MuZGVsaW1pdGVyKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnN0b3JlKHNldHRpbmdzLmtleS5pZHMsIG5ld0lEcyApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLnNldC5jb3VudCggbW9kdWxlLmdldC5pZENvdW50KCkgKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgY2hlY2s6IHtcbiAgICAgICAgICBsaW1pdDogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgICAgIHZhbHVlID0gdmFsdWUgfHwgbW9kdWxlLmdldC5jb3VudCgpO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MubGltaXQpIHtcbiAgICAgICAgICAgICAgaWYodmFsdWUgPj0gc2V0dGluZ3MubGltaXQpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1BhZ2VzIHZpZXdlZCBleGNlZWRlZCBsaW1pdCwgZmlyaW5nIGNhbGxiYWNrJywgdmFsdWUsIHNldHRpbmdzLmxpbWl0KTtcbiAgICAgICAgICAgICAgICBzZXR0aW5ncy5vbkxpbWl0LmNhbGwoZWxlbWVudCwgdmFsdWUpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnTGltaXQgbm90IHJlYWNoZWQnLCB2YWx1ZSwgc2V0dGluZ3MubGltaXQpO1xuICAgICAgICAgICAgICBzZXR0aW5ncy5vbkNoYW5nZS5jYWxsKGVsZW1lbnQsIHZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS51cGRhdGUuZGlzcGxheSh2YWx1ZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHVwZGF0ZToge1xuICAgICAgICAgIGRpc3BsYXk6IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICAgICAgICB2YWx1ZSA9IHZhbHVlIHx8IG1vZHVsZS5nZXQuY291bnQoKTtcbiAgICAgICAgICAgIGlmKCRkaXNwbGF5cy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVXBkYXRpbmcgZGlzcGxheWVkIHZpZXcgY291bnQnLCAkZGlzcGxheXMpO1xuICAgICAgICAgICAgICAkZGlzcGxheXMuaHRtbCh2YWx1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHN0b3JlOiBmdW5jdGlvbihrZXksIHZhbHVlKSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICBvcHRpb25zID0gbW9kdWxlLmdldC5zdG9yYWdlT3B0aW9ucyh2YWx1ZSlcbiAgICAgICAgICA7XG4gICAgICAgICAgaWYoc2V0dGluZ3Muc3RvcmFnZU1ldGhvZCA9PSAnbG9jYWxzdG9yYWdlJyAmJiB3aW5kb3cubG9jYWxTdG9yYWdlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHdpbmRvdy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbShrZXksIHZhbHVlKTtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVmFsdWUgc3RvcmVkIHVzaW5nIGxvY2FsIHN0b3JhZ2UnLCBrZXksIHZhbHVlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZigkLmNvb2tpZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAkLmNvb2tpZShrZXksIHZhbHVlLCBvcHRpb25zKTtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVmFsdWUgc3RvcmVkIHVzaW5nIGNvb2tpZScsIGtleSwgdmFsdWUsIG9wdGlvbnMpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5ub0Nvb2tpZVN0b3JhZ2UpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZihrZXkgPT0gc2V0dGluZ3Mua2V5LmNvdW50KSB7XG4gICAgICAgICAgICBtb2R1bGUuY2hlY2subGltaXQodmFsdWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgcmV0cmlldmU6IGZ1bmN0aW9uKGtleSwgdmFsdWUpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIHN0b3JlZFZhbHVlXG4gICAgICAgICAgO1xuICAgICAgICAgIGlmKHNldHRpbmdzLnN0b3JhZ2VNZXRob2QgPT0gJ2xvY2Fsc3RvcmFnZScgJiYgd2luZG93LmxvY2FsU3RvcmFnZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBzdG9yZWRWYWx1ZSA9IHdpbmRvdy5sb2NhbFN0b3JhZ2UuZ2V0SXRlbShrZXkpO1xuICAgICAgICAgIH1cbiAgICAgICAgICAvLyBnZXQgYnkgY29va2llXG4gICAgICAgICAgZWxzZSBpZigkLmNvb2tpZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBzdG9yZWRWYWx1ZSA9ICQuY29va2llKGtleSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm5vQ29va2llU3RvcmFnZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKHN0b3JlZFZhbHVlID09ICd1bmRlZmluZWQnIHx8IHN0b3JlZFZhbHVlID09ICdudWxsJyB8fCBzdG9yZWRWYWx1ZSA9PT0gdW5kZWZpbmVkIHx8IHN0b3JlZFZhbHVlID09PSBudWxsKSB7XG4gICAgICAgICAgICBzdG9yZWRWYWx1ZSA9IHVuZGVmaW5lZDtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHN0b3JlZFZhbHVlO1xuICAgICAgICB9LFxuXG4gICAgICAgIHNldHRpbmc6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChuYW1lKSApIHtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIHNldHRpbmdzLCBuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBzZXR0aW5nc1tuYW1lXSA9IHZhbHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBzZXR0aW5nc1tuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGludGVybmFsOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQ2hhbmdpbmcgaW50ZXJuYWwnLCBuYW1lLCB2YWx1ZSk7XG4gICAgICAgICAgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChuYW1lKSApIHtcbiAgICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgbW9kdWxlLCBuYW1lKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGVbbmFtZV0gPSB2YWx1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZGVidWc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1Zy5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdmVyYm9zZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy52ZXJib3NlICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UubG9nKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBlcnJvcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCkge1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmVycm9yLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvci5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgcGVyZm9ybWFuY2U6IHtcbiAgICAgICAgICBsb2c6IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBjdXJyZW50VGltZSxcbiAgICAgICAgICAgICAgZXhlY3V0aW9uVGltZSxcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBjdXJyZW50VGltZSAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG4gICAgICAgICAgICAgIHByZXZpb3VzVGltZSAgPSB0aW1lIHx8IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lID0gY3VycmVudFRpbWUgLSBwcmV2aW91c1RpbWU7XG4gICAgICAgICAgICAgIHRpbWUgICAgICAgICAgPSBjdXJyZW50VGltZTtcbiAgICAgICAgICAgICAgcGVyZm9ybWFuY2UucHVzaCh7XG4gICAgICAgICAgICAgICAgJ05hbWUnICAgICAgICAgICA6IG1lc3NhZ2VbMF0sXG4gICAgICAgICAgICAgICAgJ0FyZ3VtZW50cycgICAgICA6IFtdLnNsaWNlLmNhbGwobWVzc2FnZSwgMSkgfHwgJycsXG4gICAgICAgICAgICAgICAgJ0VsZW1lbnQnICAgICAgICA6IGVsZW1lbnQsXG4gICAgICAgICAgICAgICAgJ0V4ZWN1dGlvbiBUaW1lJyA6IGV4ZWN1dGlvblRpbWVcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lciA9IHNldFRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLmRpc3BsYXksIDUwMCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXNwbGF5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICB0aXRsZSA9IHNldHRpbmdzLm5hbWUgKyAnOicsXG4gICAgICAgICAgICAgIHRvdGFsVGltZSA9IDBcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHRpbWUgPSBmYWxzZTtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICB0b3RhbFRpbWUgKz0gZGF0YVsnRXhlY3V0aW9uIFRpbWUnXTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgdGl0bGUgKz0gJyAnICsgdG90YWxUaW1lICsgJ21zJztcbiAgICAgICAgICAgIGlmKG1vZHVsZVNlbGVjdG9yKSB7XG4gICAgICAgICAgICAgIHRpdGxlICs9ICcgXFwnJyArIG1vZHVsZVNlbGVjdG9yICsgJ1xcJyc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZigkYWxsTW9kdWxlcy5sZW5ndGggPiAxKSB7XG4gICAgICAgICAgICAgIHRpdGxlICs9ICcgJyArICcoJyArICRhbGxNb2R1bGVzLmxlbmd0aCArICcpJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCAoY29uc29sZS5ncm91cCAhPT0gdW5kZWZpbmVkIHx8IGNvbnNvbGUudGFibGUgIT09IHVuZGVmaW5lZCkgJiYgcGVyZm9ybWFuY2UubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKHRpdGxlKTtcbiAgICAgICAgICAgICAgaWYoY29uc29sZS50YWJsZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUudGFibGUocGVyZm9ybWFuY2UpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGFbJ05hbWUnXSArICc6ICcgKyBkYXRhWydFeGVjdXRpb24gVGltZSddKydtcycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHBlcmZvcm1hbmNlID0gW107XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnZva2U6IGZ1bmN0aW9uKHF1ZXJ5LCBwYXNzZWRBcmd1bWVudHMsIGNvbnRleHQpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIG9iamVjdCA9IGluc3RhbmNlLFxuICAgICAgICAgICAgbWF4RGVwdGgsXG4gICAgICAgICAgICBmb3VuZCxcbiAgICAgICAgICAgIHJlc3BvbnNlXG4gICAgICAgICAgO1xuICAgICAgICAgIHBhc3NlZEFyZ3VtZW50cyA9IHBhc3NlZEFyZ3VtZW50cyB8fCBxdWVyeUFyZ3VtZW50cztcbiAgICAgICAgICBjb250ZXh0ICAgICAgICAgPSBlbGVtZW50ICAgICAgICAgfHwgY29udGV4dDtcbiAgICAgICAgICBpZih0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycgJiYgb2JqZWN0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5ICAgID0gcXVlcnkuc3BsaXQoL1tcXC4gXS8pO1xuICAgICAgICAgICAgbWF4RGVwdGggPSBxdWVyeS5sZW5ndGggLSAxO1xuICAgICAgICAgICAgJC5lYWNoKHF1ZXJ5LCBmdW5jdGlvbihkZXB0aCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgdmFyIGNhbWVsQ2FzZVZhbHVlID0gKGRlcHRoICE9IG1heERlcHRoKVxuICAgICAgICAgICAgICAgID8gdmFsdWUgKyBxdWVyeVtkZXB0aCArIDFdLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcXVlcnlbZGVwdGggKyAxXS5zbGljZSgxKVxuICAgICAgICAgICAgICAgIDogcXVlcnlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbY2FtZWxDYXNlVmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoICQuaXNQbGFpbk9iamVjdCggb2JqZWN0W3ZhbHVlXSApICYmIChkZXB0aCAhPSBtYXhEZXB0aCkgKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBvYmplY3RbdmFsdWVdICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoICQuaXNGdW5jdGlvbiggZm91bmQgKSApIHtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gZm91bmQuYXBwbHkoY29udGV4dCwgcGFzc2VkQXJndW1lbnRzKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihmb3VuZCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXNwb25zZSA9IGZvdW5kO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZigkLmlzQXJyYXkocmV0dXJuZWRWYWx1ZSkpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUucHVzaChyZXNwb25zZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYocmV0dXJuZWRWYWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlID0gW3JldHVybmVkVmFsdWUsIHJlc3BvbnNlXTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihyZXNwb25zZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm5lZFZhbHVlID0gcmVzcG9uc2U7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBmb3VuZDtcbiAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIGlmKG1ldGhvZEludm9rZWQpIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICAgIH1cbiAgICAgICAgbW9kdWxlLmludm9rZShxdWVyeSk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgaWYoaW5zdGFuY2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIGluc3RhbmNlLmludm9rZSgnZGVzdHJveScpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICB9XG5cbiAgICB9KVxuICA7XG4gIHJldHVybiAocmV0dXJuZWRWYWx1ZSAhPT0gdW5kZWZpbmVkKVxuICAgID8gcmV0dXJuZWRWYWx1ZVxuICAgIDogdGhpc1xuICA7XG59O1xuXG4kLmZuLnZpc2l0LnNldHRpbmdzID0ge1xuXG4gIG5hbWUgICAgICAgICAgOiAnVmlzaXQnLFxuXG4gIGRlYnVnICAgICAgICAgOiBmYWxzZSxcbiAgdmVyYm9zZSAgICAgICA6IGZhbHNlLFxuICBwZXJmb3JtYW5jZSAgIDogdHJ1ZSxcblxuICBuYW1lc3BhY2UgICAgIDogJ3Zpc2l0JyxcblxuICBpbmNyZW1lbnQgICAgIDogZmFsc2UsXG4gIHN1cnBhc3MgICAgICAgOiBmYWxzZSxcbiAgY291bnQgICAgICAgICA6IGZhbHNlLFxuICBsaW1pdCAgICAgICAgIDogZmFsc2UsXG5cbiAgZGVsaW1pdGVyICAgICA6ICcmJyxcbiAgc3RvcmFnZU1ldGhvZCA6ICdsb2NhbHN0b3JhZ2UnLFxuXG4gIGtleSAgICAgICAgICAgOiB7XG4gICAgY291bnQgOiAndmlzaXQtY291bnQnLFxuICAgIGlkcyAgIDogJ3Zpc2l0LWlkcydcbiAgfSxcblxuICBleHBpcmVzICAgICAgIDogMzAsXG4gIGRvbWFpbiAgICAgICAgOiBmYWxzZSxcbiAgcGF0aCAgICAgICAgICA6ICcvJyxcblxuICBvbkxpbWl0ICAgICAgIDogZnVuY3Rpb24oKSB7fSxcbiAgb25DaGFuZ2UgICAgICA6IGZ1bmN0aW9uKCkge30sXG5cbiAgZXJyb3IgICAgICAgICA6IHtcbiAgICBtZXRob2QgICAgICAgICAgOiAnVGhlIG1ldGhvZCB5b3UgY2FsbGVkIGlzIG5vdCBkZWZpbmVkJyxcbiAgICBtaXNzaW5nUGVyc2lzdCAgOiAnVXNpbmcgdGhlIHBlcnNpc3Qgc2V0dGluZyByZXF1aXJlcyB0aGUgaW5jbHVzaW9uIG9mIFBlcnNpc3RKUycsXG4gICAgbm9Db29raWVTdG9yYWdlIDogJ1RoZSBkZWZhdWx0IHN0b3JhZ2UgY29va2llIHJlcXVpcmVzICQuY29va2llIHRvIGJlIGluY2x1ZGVkLidcbiAgfVxuXG59O1xuXG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7XG4iXX0=