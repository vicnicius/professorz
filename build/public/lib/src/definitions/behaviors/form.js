/*!
 * # Semantic UI - Form Validation
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.form = function (parameters) {
    var $allModules = $(this),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        legacyParameters = arguments[1],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue;
    $allModules.each(function () {
      var $module = $(this),
          element = this,
          formErrors = [],
          keyHeldDown = false,


      // set at run-time
      $field,
          $group,
          $message,
          $prompt,
          $submit,
          $clear,
          $reset,
          settings,
          validation,
          metadata,
          selector,
          className,
          error,
          namespace,
          moduleNamespace,
          eventNamespace,
          instance,
          module;

      module = {

        initialize: function () {

          // settings grabbed at run time
          module.get.settings();
          if (methodInvoked) {
            if (instance === undefined) {
              module.instantiate();
            }
            module.invoke(query);
          } else {
            if (instance !== undefined) {
              instance.invoke('destroy');
            }
            module.verbose('Initializing form validation', $module, settings);
            module.bindEvents();
            module.set.defaults();
            module.instantiate();
          }
        },

        instantiate: function () {
          module.verbose('Storing instance of module', module);
          instance = module;
          $module.data(moduleNamespace, module);
        },

        destroy: function () {
          module.verbose('Destroying previous module', instance);
          module.removeEvents();
          $module.removeData(moduleNamespace);
        },

        refresh: function () {
          module.verbose('Refreshing selector cache');
          $field = $module.find(selector.field);
          $group = $module.find(selector.group);
          $message = $module.find(selector.message);
          $prompt = $module.find(selector.prompt);

          $submit = $module.find(selector.submit);
          $clear = $module.find(selector.clear);
          $reset = $module.find(selector.reset);
        },

        submit: function () {
          module.verbose('Submitting form', $module);
          $module.submit();
        },

        attachEvents: function (selector, action) {
          action = action || 'submit';
          $(selector).on('click' + eventNamespace, function (event) {
            module[action]();
            event.preventDefault();
          });
        },

        bindEvents: function () {
          module.verbose('Attaching form events');
          $module.on('submit' + eventNamespace, module.validate.form).on('blur' + eventNamespace, selector.field, module.event.field.blur).on('click' + eventNamespace, selector.submit, module.submit).on('click' + eventNamespace, selector.reset, module.reset).on('click' + eventNamespace, selector.clear, module.clear);
          if (settings.keyboardShortcuts) {
            $module.on('keydown' + eventNamespace, selector.field, module.event.field.keydown);
          }
          $field.each(function () {
            var $input = $(this),
                type = $input.prop('type'),
                inputEvent = module.get.changeEvent(type, $input);
            $(this).on(inputEvent + eventNamespace, module.event.field.change);
          });
        },

        clear: function () {
          $field.each(function () {
            var $field = $(this),
                $element = $field.parent(),
                $fieldGroup = $field.closest($group),
                $prompt = $fieldGroup.find(selector.prompt),
                defaultValue = $field.data(metadata.defaultValue) || '',
                isCheckbox = $element.is(selector.uiCheckbox),
                isDropdown = $element.is(selector.uiDropdown),
                isErrored = $fieldGroup.hasClass(className.error);
            if (isErrored) {
              module.verbose('Resetting error on field', $fieldGroup);
              $fieldGroup.removeClass(className.error);
              $prompt.remove();
            }
            if (isDropdown) {
              module.verbose('Resetting dropdown value', $element, defaultValue);
              $element.dropdown('clear');
            } else if (isCheckbox) {
              $field.prop('checked', false);
            } else {
              module.verbose('Resetting field value', $field, defaultValue);
              $field.val('');
            }
          });
        },

        reset: function () {
          $field.each(function () {
            var $field = $(this),
                $element = $field.parent(),
                $fieldGroup = $field.closest($group),
                $prompt = $fieldGroup.find(selector.prompt),
                defaultValue = $field.data(metadata.defaultValue),
                isCheckbox = $element.is(selector.uiCheckbox),
                isDropdown = $element.is(selector.uiDropdown),
                isErrored = $fieldGroup.hasClass(className.error);
            if (defaultValue === undefined) {
              return;
            }
            if (isErrored) {
              module.verbose('Resetting error on field', $fieldGroup);
              $fieldGroup.removeClass(className.error);
              $prompt.remove();
            }
            if (isDropdown) {
              module.verbose('Resetting dropdown value', $element, defaultValue);
              $element.dropdown('restore defaults');
            } else if (isCheckbox) {
              module.verbose('Resetting checkbox value', $element, defaultValue);
              $field.prop('checked', defaultValue);
            } else {
              module.verbose('Resetting field value', $field, defaultValue);
              $field.val(defaultValue);
            }
          });
        },

        is: {
          bracketedRule: function (rule) {
            return rule.type && rule.type.match(settings.regExp.bracket);
          },
          empty: function ($field) {
            if (!$field || $field.length === 0) {
              return true;
            } else if ($field.is('input[type="checkbox"]')) {
              return !$field.is(':checked');
            } else {
              return module.is.blank($field);
            }
          },
          blank: function ($field) {
            return $.trim($field.val()) === '';
          },
          valid: function () {
            var allValid = true;
            module.verbose('Checking if form is valid');
            $.each(validation, function (fieldName, field) {
              if (!module.validate.field(field, fieldName)) {
                allValid = false;
              }
            });
            return allValid;
          }
        },

        removeEvents: function () {
          $module.off(eventNamespace);
          $field.off(eventNamespace);
          $submit.off(eventNamespace);
          $field.off(eventNamespace);
        },

        event: {
          field: {
            keydown: function (event) {
              var $field = $(this),
                  key = event.which,
                  isInput = $field.is(selector.input),
                  isCheckbox = $field.is(selector.checkbox),
                  isInDropdown = $field.closest(selector.uiDropdown).length > 0,
                  keyCode = {
                enter: 13,
                escape: 27
              };
              if (key == keyCode.escape) {
                module.verbose('Escape key pressed blurring field');
                $field.blur();
              }
              if (!event.ctrlKey && key == keyCode.enter && isInput && !isInDropdown && !isCheckbox) {
                if (!keyHeldDown) {
                  $field.one('keyup' + eventNamespace, module.event.field.keyup);
                  module.submit();
                  module.debug('Enter pressed on input submitting form');
                }
                keyHeldDown = true;
              }
            },
            keyup: function () {
              keyHeldDown = false;
            },
            blur: function (event) {
              var $field = $(this),
                  $fieldGroup = $field.closest($group),
                  validationRules = module.get.validation($field);
              if ($fieldGroup.hasClass(className.error)) {
                module.debug('Revalidating field', $field, validationRules);
                if (validationRules) {
                  module.validate.field(validationRules);
                }
              } else if (settings.on == 'blur' || settings.on == 'change') {
                if (validationRules) {
                  module.validate.field(validationRules);
                }
              }
            },
            change: function (event) {
              var $field = $(this),
                  $fieldGroup = $field.closest($group),
                  validationRules = module.get.validation($field);
              if (settings.on == 'change' || $fieldGroup.hasClass(className.error) && settings.revalidate) {
                clearTimeout(module.timer);
                module.timer = setTimeout(function () {
                  module.debug('Revalidating field', $field, module.get.validation($field));
                  module.validate.field(validationRules);
                }, settings.delay);
              }
            }
          }

        },

        get: {
          ancillaryValue: function (rule) {
            if (!rule.type || !module.is.bracketedRule(rule)) {
              return false;
            }
            return rule.type.match(settings.regExp.bracket)[1] + '';
          },
          ruleName: function (rule) {
            if (module.is.bracketedRule(rule)) {
              return rule.type.replace(rule.type.match(settings.regExp.bracket)[0], '');
            }
            return rule.type;
          },
          changeEvent: function (type, $input) {
            if (type == 'checkbox' || type == 'radio' || type == 'hidden' || $input.is('select')) {
              return 'change';
            } else {
              return module.get.inputEvent();
            }
          },
          inputEvent: function () {
            return document.createElement('input').oninput !== undefined ? 'input' : document.createElement('input').onpropertychange !== undefined ? 'propertychange' : 'keyup';
          },
          prompt: function (rule, field) {
            var ruleName = module.get.ruleName(rule),
                ancillary = module.get.ancillaryValue(rule),
                prompt = rule.prompt || settings.prompt[ruleName] || settings.text.unspecifiedRule,
                requiresValue = prompt.search('{value}') !== -1,
                requiresName = prompt.search('{name}') !== -1,
                $label,
                $field,
                name;
            if (requiresName || requiresValue) {
              $field = module.get.field(field.identifier);
            }
            if (requiresValue) {
              prompt = prompt.replace('{value}', $field.val());
            }
            if (requiresName) {
              $label = $field.closest(selector.group).find('label').eq(0);
              name = $label.length == 1 ? $label.text() : $field.prop('placeholder') || settings.text.unspecifiedField;
              prompt = prompt.replace('{name}', name);
            }
            prompt = prompt.replace('{identifier}', field.identifier);
            prompt = prompt.replace('{ruleValue}', ancillary);
            if (!rule.prompt) {
              module.verbose('Using default validation prompt for type', prompt, ruleName);
            }
            return prompt;
          },
          settings: function () {
            if ($.isPlainObject(parameters)) {
              var keys = Object.keys(parameters),
                  isLegacySettings = keys.length > 0 ? parameters[keys[0]].identifier !== undefined && parameters[keys[0]].rules !== undefined : false,
                  ruleKeys;
              if (isLegacySettings) {
                // 1.x (ducktyped)
                settings = $.extend(true, {}, $.fn.form.settings, legacyParameters);
                validation = $.extend({}, $.fn.form.settings.defaults, parameters);
                module.error(settings.error.oldSyntax, element);
                module.verbose('Extending settings from legacy parameters', validation, settings);
              } else {
                // 2.x
                if (parameters.fields) {
                  ruleKeys = Object.keys(parameters.fields);
                  if (typeof parameters.fields[ruleKeys[0]] == 'string' || $.isArray(parameters.fields[ruleKeys[0]])) {
                    $.each(parameters.fields, function (name, rules) {
                      if (typeof rules == 'string') {
                        rules = [rules];
                      }
                      parameters.fields[name] = {
                        rules: []
                      };
                      $.each(rules, function (index, rule) {
                        parameters.fields[name].rules.push({ type: rule });
                      });
                    });
                  }
                }

                settings = $.extend(true, {}, $.fn.form.settings, parameters);
                validation = $.extend({}, $.fn.form.settings.defaults, settings.fields);
                module.verbose('Extending settings', validation, settings);
              }
            } else {
              settings = $.fn.form.settings;
              validation = $.fn.form.settings.defaults;
              module.verbose('Using default form validation', validation, settings);
            }

            // shorthand
            namespace = settings.namespace;
            metadata = settings.metadata;
            selector = settings.selector;
            className = settings.className;
            error = settings.error;
            moduleNamespace = 'module-' + namespace;
            eventNamespace = '.' + namespace;

            // grab instance
            instance = $module.data(moduleNamespace);

            // refresh selector cache
            module.refresh();
          },
          field: function (identifier) {
            module.verbose('Finding field with identifier', identifier);
            if ($field.filter('#' + identifier).length > 0) {
              return $field.filter('#' + identifier);
            } else if ($field.filter('[name="' + identifier + '"]').length > 0) {
              return $field.filter('[name="' + identifier + '"]');
            } else if ($field.filter('[name="' + identifier + '[]"]').length > 0) {
              return $field.filter('[name="' + identifier + '[]"]');
            } else if ($field.filter('[data-' + metadata.validate + '="' + identifier + '"]').length > 0) {
              return $field.filter('[data-' + metadata.validate + '="' + identifier + '"]');
            }
            return $('<input/>');
          },
          fields: function (fields) {
            var $fields = $();
            $.each(fields, function (index, name) {
              $fields = $fields.add(module.get.field(name));
            });
            return $fields;
          },
          validation: function ($field) {
            var fieldValidation, identifier;
            if (!validation) {
              return false;
            }
            $.each(validation, function (fieldName, field) {
              identifier = field.identifier || fieldName;
              if (module.get.field(identifier)[0] == $field[0]) {
                field.identifier = identifier;
                fieldValidation = field;
              }
            });
            return fieldValidation || false;
          },
          value: function (field) {
            var fields = [],
                results;
            fields.push(field);
            results = module.get.values.call(element, fields);
            return results[field];
          },
          values: function (fields) {
            var $fields = $.isArray(fields) ? module.get.fields(fields) : $field,
                values = {};
            $fields.each(function (index, field) {
              var $field = $(field),
                  type = $field.prop('type'),
                  name = $field.prop('name'),
                  value = $field.val(),
                  isCheckbox = $field.is(selector.checkbox),
                  isRadio = $field.is(selector.radio),
                  isMultiple = name.indexOf('[]') !== -1,
                  isChecked = isCheckbox ? $field.is(':checked') : false;
              if (name) {
                if (isMultiple) {
                  name = name.replace('[]', '');
                  if (!values[name]) {
                    values[name] = [];
                  }
                  if (isCheckbox) {
                    if (isChecked) {
                      values[name].push(value || true);
                    } else {
                      values[name].push(false);
                    }
                  } else {
                    values[name].push(value);
                  }
                } else {
                  if (isRadio) {
                    if (isChecked) {
                      values[name] = value;
                    }
                  } else if (isCheckbox) {
                    if (isChecked) {
                      values[name] = value || true;
                    } else {
                      values[name] = false;
                    }
                  } else {
                    values[name] = value;
                  }
                }
              }
            });
            return values;
          }
        },

        has: {

          field: function (identifier) {
            module.verbose('Checking for existence of a field with identifier', identifier);
            if (typeof identifier !== 'string') {
              module.error(error.identifier, identifier);
            }
            if ($field.filter('#' + identifier).length > 0) {
              return true;
            } else if ($field.filter('[name="' + identifier + '"]').length > 0) {
              return true;
            } else if ($field.filter('[data-' + metadata.validate + '="' + identifier + '"]').length > 0) {
              return true;
            }
            return false;
          }

        },

        add: {
          prompt: function (identifier, errors) {
            var $field = module.get.field(identifier),
                $fieldGroup = $field.closest($group),
                $prompt = $fieldGroup.children(selector.prompt),
                promptExists = $prompt.length !== 0;
            errors = typeof errors == 'string' ? [errors] : errors;
            module.verbose('Adding field error state', identifier);
            $fieldGroup.addClass(className.error);
            if (settings.inline) {
              if (!promptExists) {
                $prompt = settings.templates.prompt(errors);
                $prompt.appendTo($fieldGroup);
              }
              $prompt.html(errors[0]);
              if (!promptExists) {
                if (settings.transition && $.fn.transition !== undefined && $module.transition('is supported')) {
                  module.verbose('Displaying error with css transition', settings.transition);
                  $prompt.transition(settings.transition + ' in', settings.duration);
                } else {
                  module.verbose('Displaying error with fallback javascript animation');
                  $prompt.fadeIn(settings.duration);
                }
              } else {
                module.verbose('Inline errors are disabled, no inline error added', identifier);
              }
            }
          },
          errors: function (errors) {
            module.debug('Adding form error messages', errors);
            module.set.error();
            $message.html(settings.templates.error(errors));
          }
        },

        remove: {
          prompt: function (identifier) {
            var $field = module.get.field(identifier),
                $fieldGroup = $field.closest($group),
                $prompt = $fieldGroup.children(selector.prompt);
            $fieldGroup.removeClass(className.error);
            if (settings.inline && $prompt.is(':visible')) {
              module.verbose('Removing prompt for field', identifier);
              if (settings.transition && $.fn.transition !== undefined && $module.transition('is supported')) {
                $prompt.transition(settings.transition + ' out', settings.duration, function () {
                  $prompt.remove();
                });
              } else {
                $prompt.fadeOut(settings.duration, function () {
                  $prompt.remove();
                });
              }
            }
          }
        },

        set: {
          success: function () {
            $module.removeClass(className.error).addClass(className.success);
          },
          defaults: function () {
            $field.each(function () {
              var $field = $(this),
                  isCheckbox = $field.filter(selector.checkbox).length > 0,
                  value = isCheckbox ? $field.is(':checked') : $field.val();
              $field.data(metadata.defaultValue, value);
            });
          },
          error: function () {
            $module.removeClass(className.success).addClass(className.error);
          },
          value: function (field, value) {
            var fields = {};
            fields[field] = value;
            return module.set.values.call(element, fields);
          },
          values: function (fields) {
            if ($.isEmptyObject(fields)) {
              return;
            }
            $.each(fields, function (key, value) {
              var $field = module.get.field(key),
                  $element = $field.parent(),
                  isMultiple = $.isArray(value),
                  isCheckbox = $element.is(selector.uiCheckbox),
                  isDropdown = $element.is(selector.uiDropdown),
                  isRadio = $field.is(selector.radio) && isCheckbox,
                  fieldExists = $field.length > 0,
                  $multipleField;
              if (fieldExists) {
                if (isMultiple && isCheckbox) {
                  module.verbose('Selecting multiple', value, $field);
                  $element.checkbox('uncheck');
                  $.each(value, function (index, value) {
                    $multipleField = $field.filter('[value="' + value + '"]');
                    $element = $multipleField.parent();
                    if ($multipleField.length > 0) {
                      $element.checkbox('check');
                    }
                  });
                } else if (isRadio) {
                  module.verbose('Selecting radio value', value, $field);
                  $field.filter('[value="' + value + '"]').parent(selector.uiCheckbox).checkbox('check');
                } else if (isCheckbox) {
                  module.verbose('Setting checkbox value', value, $element);
                  if (value === true) {
                    $element.checkbox('check');
                  } else {
                    $element.checkbox('uncheck');
                  }
                } else if (isDropdown) {
                  module.verbose('Setting dropdown value', value, $element);
                  $element.dropdown('set selected', value);
                } else {
                  module.verbose('Setting field value', value, $field);
                  $field.val(value);
                }
              }
            });
          }
        },

        validate: {

          form: function (event, ignoreCallbacks) {
            var values = module.get.values(),
                apiRequest;

            // input keydown event will fire submit repeatedly by browser default
            if (keyHeldDown) {
              return false;
            }

            // reset errors
            formErrors = [];
            if (module.is.valid()) {
              module.debug('Form has no validation errors, submitting');
              module.set.success();
              if (ignoreCallbacks !== true) {
                return settings.onSuccess.call(element, event, values);
              }
            } else {
              module.debug('Form has errors');
              module.set.error();
              if (!settings.inline) {
                module.add.errors(formErrors);
              }
              // prevent ajax submit
              if ($module.data('moduleApi') !== undefined) {
                event.stopImmediatePropagation();
              }
              if (ignoreCallbacks !== true) {
                return settings.onFailure.call(element, formErrors, values);
              }
            }
          },

          // takes a validation object and returns whether field passes validation
          field: function (field, fieldName) {
            var identifier = field.identifier || fieldName,
                $field = module.get.field(identifier),
                $dependsField = field.depends ? module.get.field(field.depends) : false,
                fieldValid = true,
                fieldErrors = [];
            if (!field.identifier) {
              module.debug('Using field name as identifier', identifier);
              field.identifier = identifier;
            }
            if ($field.prop('disabled')) {
              module.debug('Field is disabled. Skipping', identifier);
              fieldValid = true;
            } else if (field.optional && module.is.blank($field)) {
              module.debug('Field is optional and blank. Skipping', identifier);
              fieldValid = true;
            } else if (field.depends && module.is.empty($dependsField)) {
              module.debug('Field depends on another value that is not present or empty. Skipping', $dependsField);
              fieldValid = true;
            } else if (field.rules !== undefined) {
              $.each(field.rules, function (index, rule) {
                if (module.has.field(identifier) && !module.validate.rule(field, rule)) {
                  module.debug('Field is invalid', identifier, rule.type);
                  fieldErrors.push(module.get.prompt(rule, field));
                  fieldValid = false;
                }
              });
            }
            if (fieldValid) {
              module.remove.prompt(identifier, fieldErrors);
              settings.onValid.call($field);
            } else {
              formErrors = formErrors.concat(fieldErrors);
              module.add.prompt(identifier, fieldErrors);
              settings.onInvalid.call($field, fieldErrors);
              return false;
            }
            return true;
          },

          // takes validation rule and returns whether field passes rule
          rule: function (field, rule) {
            var $field = module.get.field(field.identifier),
                type = rule.type,
                value = $field.val(),
                isValid = true,
                ancillary = module.get.ancillaryValue(rule),
                ruleName = module.get.ruleName(rule),
                ruleFunction = settings.rules[ruleName];
            if (!$.isFunction(ruleFunction)) {
              module.error(error.noRule, ruleName);
              return;
            }
            // cast to string avoiding encoding special values
            value = value === undefined || value === '' || value === null ? '' : $.trim(value + '');
            return ruleFunction.call($field, value, ancillary);
          }
        },

        setting: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            settings[name] = value;
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                'Element': element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ($allModules.length > 1) {
              title += ' ' + '(' + $allModules.length + ')';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };
      module.initialize();
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.fn.form.settings = {

    name: 'Form',
    namespace: 'form',

    debug: false,
    verbose: false,
    performance: true,

    fields: false,

    keyboardShortcuts: true,
    on: 'submit',
    inline: false,

    delay: 200,
    revalidate: true,

    transition: 'scale',
    duration: 200,

    onValid: function () {},
    onInvalid: function () {},
    onSuccess: function () {
      return true;
    },
    onFailure: function () {
      return false;
    },

    metadata: {
      defaultValue: 'default',
      validate: 'validate'
    },

    regExp: {
      bracket: /\[(.*)\]/i,
      decimal: /^\d*(\.)\d+/,
      email: /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i,
      escape: /[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g,
      flags: /^\/(.*)\/(.*)?/,
      integer: /^\-?\d+$/,
      number: /^\-?\d*(\.\d+)?$/,
      url: /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/i
    },

    text: {
      unspecifiedRule: 'Please enter a valid value',
      unspecifiedField: 'This field'
    },

    prompt: {
      empty: '{name} must have a value',
      checked: '{name} must be checked',
      email: '{name} must be a valid e-mail',
      url: '{name} must be a valid url',
      regExp: '{name} is not formatted correctly',
      integer: '{name} must be an integer',
      decimal: '{name} must be a decimal number',
      number: '{name} must be set to a number',
      is: '{name} must be "{ruleValue}"',
      isExactly: '{name} must be exactly "{ruleValue}"',
      not: '{name} cannot be set to "{ruleValue}"',
      notExactly: '{name} cannot be set to exactly "{ruleValue}"',
      contain: '{name} cannot contain "{ruleValue}"',
      containExactly: '{name} cannot contain exactly "{ruleValue}"',
      doesntContain: '{name} must contain  "{ruleValue}"',
      doesntContainExactly: '{name} must contain exactly "{ruleValue}"',
      minLength: '{name} must be at least {ruleValue} characters',
      length: '{name} must be at least {ruleValue} characters',
      exactLength: '{name} must be exactly {ruleValue} characters',
      maxLength: '{name} cannot be longer than {ruleValue} characters',
      match: '{name} must match {ruleValue} field',
      different: '{name} must have a different value than {ruleValue} field',
      creditCard: '{name} must be a valid credit card number',
      minCount: '{name} must have at least {ruleValue} choices',
      exactCount: '{name} must have exactly {ruleValue} choices',
      maxCount: '{name} must have {ruleValue} or less choices'
    },

    selector: {
      checkbox: 'input[type="checkbox"], input[type="radio"]',
      clear: '.clear',
      field: 'input, textarea, select',
      group: '.field',
      input: 'input',
      message: '.error.message',
      prompt: '.prompt.label',
      radio: 'input[type="radio"]',
      reset: '.reset:not([type="reset"])',
      submit: '.submit:not([type="submit"])',
      uiCheckbox: '.ui.checkbox',
      uiDropdown: '.ui.dropdown'
    },

    className: {
      error: 'error',
      label: 'ui prompt label',
      pressed: 'down',
      success: 'success'
    },

    error: {
      identifier: 'You must specify a string identifier for each field',
      method: 'The method you called is not defined.',
      noRule: 'There is no rule matching the one you specified',
      oldSyntax: 'Starting in 2.0 forms now only take a single settings object. Validation settings converted to new syntax automatically.'
    },

    templates: {

      // template that produces error message
      error: function (errors) {
        var html = '<ul class="list">';
        $.each(errors, function (index, value) {
          html += '<li>' + value + '</li>';
        });
        html += '</ul>';
        return $(html);
      },

      // template that produces label
      prompt: function (errors) {
        return $('<div/>').addClass('ui basic red pointing prompt label').html(errors[0]);
      }
    },

    rules: {

      // is not empty or blank string
      empty: function (value) {
        return !(value === undefined || '' === value || $.isArray(value) && value.length === 0);
      },

      // checkbox checked
      checked: function () {
        return $(this).filter(':checked').length > 0;
      },

      // is most likely an email
      email: function (value) {
        return $.fn.form.settings.regExp.email.test(value);
      },

      // value is most likely url
      url: function (value) {
        return $.fn.form.settings.regExp.url.test(value);
      },

      // matches specified regExp
      regExp: function (value, regExp) {
        var regExpParts = regExp.match($.fn.form.settings.regExp.flags),
            flags;
        // regular expression specified as /baz/gi (flags)
        if (regExpParts) {
          regExp = regExpParts.length >= 2 ? regExpParts[1] : regExp;
          flags = regExpParts.length >= 3 ? regExpParts[2] : '';
        }
        return value.match(new RegExp(regExp, flags));
      },

      // is valid integer or matches range
      integer: function (value, range) {
        var intRegExp = $.fn.form.settings.regExp.integer,
            min,
            max,
            parts;
        if (!range || ['', '..'].indexOf(range) !== -1) {
          // do nothing
        } else if (range.indexOf('..') == -1) {
          if (intRegExp.test(range)) {
            min = max = range - 0;
          }
        } else {
          parts = range.split('..', 2);
          if (intRegExp.test(parts[0])) {
            min = parts[0] - 0;
          }
          if (intRegExp.test(parts[1])) {
            max = parts[1] - 0;
          }
        }
        return intRegExp.test(value) && (min === undefined || value >= min) && (max === undefined || value <= max);
      },

      // is valid number (with decimal)
      decimal: function (value) {
        return $.fn.form.settings.regExp.decimal.test(value);
      },

      // is valid number
      number: function (value) {
        return $.fn.form.settings.regExp.number.test(value);
      },

      // is value (case insensitive)
      is: function (value, text) {
        text = typeof text == 'string' ? text.toLowerCase() : text;
        value = typeof value == 'string' ? value.toLowerCase() : value;
        return value == text;
      },

      // is value
      isExactly: function (value, text) {
        return value == text;
      },

      // value is not another value (case insensitive)
      not: function (value, notValue) {
        value = typeof value == 'string' ? value.toLowerCase() : value;
        notValue = typeof notValue == 'string' ? notValue.toLowerCase() : notValue;
        return value != notValue;
      },

      // value is not another value (case sensitive)
      notExactly: function (value, notValue) {
        return value != notValue;
      },

      // value contains text (insensitive)
      contains: function (value, text) {
        // escape regex characters
        text = text.replace($.fn.form.settings.regExp.escape, "\\$&");
        return value.search(new RegExp(text, 'i')) !== -1;
      },

      // value contains text (case sensitive)
      containsExactly: function (value, text) {
        // escape regex characters
        text = text.replace($.fn.form.settings.regExp.escape, "\\$&");
        return value.search(new RegExp(text)) !== -1;
      },

      // value contains text (insensitive)
      doesntContain: function (value, text) {
        // escape regex characters
        text = text.replace($.fn.form.settings.regExp.escape, "\\$&");
        return value.search(new RegExp(text, 'i')) === -1;
      },

      // value contains text (case sensitive)
      doesntContainExactly: function (value, text) {
        // escape regex characters
        text = text.replace($.fn.form.settings.regExp.escape, "\\$&");
        return value.search(new RegExp(text)) === -1;
      },

      // is at least string length
      minLength: function (value, requiredLength) {
        return value !== undefined ? value.length >= requiredLength : false;
      },

      // see rls notes for 2.0.6 (this is a duplicate of minLength)
      length: function (value, requiredLength) {
        return value !== undefined ? value.length >= requiredLength : false;
      },

      // is exactly length
      exactLength: function (value, requiredLength) {
        return value !== undefined ? value.length == requiredLength : false;
      },

      // is less than length
      maxLength: function (value, maxLength) {
        return value !== undefined ? value.length <= maxLength : false;
      },

      // matches another field
      match: function (value, identifier) {
        var $form = $(this),
            matchingValue;
        if ($('[data-validate="' + identifier + '"]').length > 0) {
          matchingValue = $('[data-validate="' + identifier + '"]').val();
        } else if ($('#' + identifier).length > 0) {
          matchingValue = $('#' + identifier).val();
        } else if ($('[name="' + identifier + '"]').length > 0) {
          matchingValue = $('[name="' + identifier + '"]').val();
        } else if ($('[name="' + identifier + '[]"]').length > 0) {
          matchingValue = $('[name="' + identifier + '[]"]');
        }
        return matchingValue !== undefined ? value.toString() == matchingValue.toString() : false;
      },

      // different than another field
      different: function (value, identifier) {
        // use either id or name of field
        var $form = $(this),
            matchingValue;
        if ($('[data-validate="' + identifier + '"]').length > 0) {
          matchingValue = $('[data-validate="' + identifier + '"]').val();
        } else if ($('#' + identifier).length > 0) {
          matchingValue = $('#' + identifier).val();
        } else if ($('[name="' + identifier + '"]').length > 0) {
          matchingValue = $('[name="' + identifier + '"]').val();
        } else if ($('[name="' + identifier + '[]"]').length > 0) {
          matchingValue = $('[name="' + identifier + '[]"]');
        }
        return matchingValue !== undefined ? value.toString() !== matchingValue.toString() : false;
      },

      creditCard: function (cardNumber, cardTypes) {
        var cards = {
          visa: {
            pattern: /^4/,
            length: [16]
          },
          amex: {
            pattern: /^3[47]/,
            length: [15]
          },
          mastercard: {
            pattern: /^5[1-5]/,
            length: [16]
          },
          discover: {
            pattern: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/,
            length: [16]
          },
          unionPay: {
            pattern: /^(62|88)/,
            length: [16, 17, 18, 19]
          },
          jcb: {
            pattern: /^35(2[89]|[3-8][0-9])/,
            length: [16]
          },
          maestro: {
            pattern: /^(5018|5020|5038|6304|6759|676[1-3])/,
            length: [12, 13, 14, 15, 16, 17, 18, 19]
          },
          dinersClub: {
            pattern: /^(30[0-5]|^36)/,
            length: [14]
          },
          laser: {
            pattern: /^(6304|670[69]|6771)/,
            length: [16, 17, 18, 19]
          },
          visaElectron: {
            pattern: /^(4026|417500|4508|4844|491(3|7))/,
            length: [16]
          }
        },
            valid = {},
            validCard = false,
            requiredTypes = typeof cardTypes == 'string' ? cardTypes.split(',') : false,
            unionPay,
            validation;

        if (typeof cardNumber !== 'string' || cardNumber.length === 0) {
          return;
        }

        // verify card types
        if (requiredTypes) {
          $.each(requiredTypes, function (index, type) {
            // verify each card type
            validation = cards[type];
            if (validation) {
              valid = {
                length: $.inArray(cardNumber.length, validation.length) !== -1,
                pattern: cardNumber.search(validation.pattern) !== -1
              };
              if (valid.length && valid.pattern) {
                validCard = true;
              }
            }
          });

          if (!validCard) {
            return false;
          }
        }

        // skip luhn for UnionPay
        unionPay = {
          number: $.inArray(cardNumber.length, cards.unionPay.length) !== -1,
          pattern: cardNumber.search(cards.unionPay.pattern) !== -1
        };
        if (unionPay.number && unionPay.pattern) {
          return true;
        }

        // verify luhn, adapted from  <https://gist.github.com/2134376>
        var length = cardNumber.length,
            multiple = 0,
            producedValue = [[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]],
            sum = 0;
        while (length--) {
          sum += producedValue[multiple][parseInt(cardNumber.charAt(length), 10)];
          multiple ^= 1;
        }
        return sum % 10 === 0 && sum > 0;
      },

      minCount: function (value, minCount) {
        if (minCount == 0) {
          return true;
        }
        if (minCount == 1) {
          return value !== '';
        }
        return value.split(',').length >= minCount;
      },

      exactCount: function (value, exactCount) {
        if (exactCount == 0) {
          return value === '';
        }
        if (exactCount == 1) {
          return value !== '' && value.search(',') === -1;
        }
        return value.split(',').length == exactCount;
      },

      maxCount: function (value, maxCount) {
        if (maxCount == 0) {
          return false;
        }
        if (maxCount == 1) {
          return value.search(',') === -1;
        }
        return value.split(',').length <= maxCount;
      }
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9iZWhhdmlvcnMvZm9ybS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBVUEsQ0FBQyxDQUFDLFVBQVUsQ0FBVixFQUFhLE1BQWIsRUFBcUIsUUFBckIsRUFBK0IsU0FBL0IsRUFBMEM7O0FBRTVDOztBQUVBLFdBQVUsT0FBTyxNQUFQLElBQWlCLFdBQWpCLElBQWdDLE9BQU8sSUFBUCxJQUFlLElBQWhELEdBQ0wsTUFESyxHQUVKLE9BQU8sSUFBUCxJQUFlLFdBQWYsSUFBOEIsS0FBSyxJQUFMLElBQWEsSUFBNUMsR0FDRSxJQURGLEdBRUUsU0FBUyxhQUFULEdBSk47O0FBT0EsSUFBRSxFQUFGLENBQUssSUFBTCxHQUFZLFVBQVMsVUFBVCxFQUFxQjtBQUMvQixRQUNFLGNBQW1CLEVBQUUsSUFBRixDQURyQjtBQUFBLFFBRUUsaUJBQW1CLFlBQVksUUFBWixJQUF3QixFQUY3QztBQUFBLFFBSUUsT0FBbUIsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUpyQjtBQUFBLFFBS0UsY0FBbUIsRUFMckI7QUFBQSxRQU9FLFFBQW1CLFVBQVUsQ0FBVixDQVByQjtBQUFBLFFBUUUsbUJBQW1CLFVBQVUsQ0FBVixDQVJyQjtBQUFBLFFBU0UsZ0JBQW9CLE9BQU8sS0FBUCxJQUFnQixRQVR0QztBQUFBLFFBVUUsaUJBQW1CLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLENBQXpCLENBVnJCO0FBQUEsUUFXRSxhQVhGO0FBYUEsZ0JBQ0csSUFESCxDQUNRLFlBQVc7QUFDZixVQUNFLFVBQWMsRUFBRSxJQUFGLENBRGhCO0FBQUEsVUFFRSxVQUFjLElBRmhCO0FBQUEsVUFJRSxhQUFjLEVBSmhCO0FBQUEsVUFLRSxjQUFjLEtBTGhCO0FBQUE7OztBQVFFLFlBUkY7QUFBQSxVQVNFLE1BVEY7QUFBQSxVQVVFLFFBVkY7QUFBQSxVQVdFLE9BWEY7QUFBQSxVQVlFLE9BWkY7QUFBQSxVQWFFLE1BYkY7QUFBQSxVQWNFLE1BZEY7QUFBQSxVQWdCRSxRQWhCRjtBQUFBLFVBaUJFLFVBakJGO0FBQUEsVUFtQkUsUUFuQkY7QUFBQSxVQW9CRSxRQXBCRjtBQUFBLFVBcUJFLFNBckJGO0FBQUEsVUFzQkUsS0F0QkY7QUFBQSxVQXdCRSxTQXhCRjtBQUFBLFVBeUJFLGVBekJGO0FBQUEsVUEwQkUsY0ExQkY7QUFBQSxVQTRCRSxRQTVCRjtBQUFBLFVBNkJFLE1BN0JGOztBQWdDQSxlQUFjOztBQUVaLG9CQUFZLFlBQVc7OztBQUdyQixpQkFBTyxHQUFQLENBQVcsUUFBWDtBQUNBLGNBQUcsYUFBSCxFQUFrQjtBQUNoQixnQkFBRyxhQUFhLFNBQWhCLEVBQTJCO0FBQ3pCLHFCQUFPLFdBQVA7QUFDRDtBQUNELG1CQUFPLE1BQVAsQ0FBYyxLQUFkO0FBQ0QsV0FMRCxNQU1LO0FBQ0gsZ0JBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUN6Qix1QkFBUyxNQUFULENBQWdCLFNBQWhCO0FBQ0Q7QUFDRCxtQkFBTyxPQUFQLENBQWUsOEJBQWYsRUFBK0MsT0FBL0MsRUFBd0QsUUFBeEQ7QUFDQSxtQkFBTyxVQUFQO0FBQ0EsbUJBQU8sR0FBUCxDQUFXLFFBQVg7QUFDQSxtQkFBTyxXQUFQO0FBQ0Q7QUFDRixTQXJCVzs7QUF1QloscUJBQWEsWUFBVztBQUN0QixpQkFBTyxPQUFQLENBQWUsNEJBQWYsRUFBNkMsTUFBN0M7QUFDQSxxQkFBVyxNQUFYO0FBQ0Esa0JBQ0csSUFESCxDQUNRLGVBRFIsRUFDeUIsTUFEekI7QUFHRCxTQTdCVzs7QUErQlosaUJBQVMsWUFBVztBQUNsQixpQkFBTyxPQUFQLENBQWUsNEJBQWYsRUFBNkMsUUFBN0M7QUFDQSxpQkFBTyxZQUFQO0FBQ0Esa0JBQ0csVUFESCxDQUNjLGVBRGQ7QUFHRCxTQXJDVzs7QUF1Q1osaUJBQVMsWUFBVztBQUNsQixpQkFBTyxPQUFQLENBQWUsMkJBQWY7QUFDQSxtQkFBYyxRQUFRLElBQVIsQ0FBYSxTQUFTLEtBQXRCLENBQWQ7QUFDQSxtQkFBYyxRQUFRLElBQVIsQ0FBYSxTQUFTLEtBQXRCLENBQWQ7QUFDQSxxQkFBYyxRQUFRLElBQVIsQ0FBYSxTQUFTLE9BQXRCLENBQWQ7QUFDQSxvQkFBYyxRQUFRLElBQVIsQ0FBYSxTQUFTLE1BQXRCLENBQWQ7O0FBRUEsb0JBQWMsUUFBUSxJQUFSLENBQWEsU0FBUyxNQUF0QixDQUFkO0FBQ0EsbUJBQWMsUUFBUSxJQUFSLENBQWEsU0FBUyxLQUF0QixDQUFkO0FBQ0EsbUJBQWMsUUFBUSxJQUFSLENBQWEsU0FBUyxLQUF0QixDQUFkO0FBQ0QsU0FqRFc7O0FBbURaLGdCQUFRLFlBQVc7QUFDakIsaUJBQU8sT0FBUCxDQUFlLGlCQUFmLEVBQWtDLE9BQWxDO0FBQ0Esa0JBQ0csTUFESDtBQUdELFNBeERXOztBQTBEWixzQkFBYyxVQUFTLFFBQVQsRUFBbUIsTUFBbkIsRUFBMkI7QUFDdkMsbUJBQVMsVUFBVSxRQUFuQjtBQUNBLFlBQUUsUUFBRixFQUNHLEVBREgsQ0FDTSxVQUFVLGNBRGhCLEVBQ2dDLFVBQVMsS0FBVCxFQUFnQjtBQUM1QyxtQkFBTyxNQUFQO0FBQ0Esa0JBQU0sY0FBTjtBQUNELFdBSkg7QUFNRCxTQWxFVzs7QUFvRVosb0JBQVksWUFBVztBQUNyQixpQkFBTyxPQUFQLENBQWUsdUJBQWY7QUFDQSxrQkFDRyxFQURILENBQ00sV0FBVyxjQURqQixFQUNpQyxPQUFPLFFBQVAsQ0FBZ0IsSUFEakQsRUFFRyxFQUZILENBRU0sU0FBVyxjQUZqQixFQUVpQyxTQUFTLEtBRjFDLEVBRWlELE9BQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsSUFGcEUsRUFHRyxFQUhILENBR00sVUFBVyxjQUhqQixFQUdpQyxTQUFTLE1BSDFDLEVBR2tELE9BQU8sTUFIekQsRUFJRyxFQUpILENBSU0sVUFBVyxjQUpqQixFQUlpQyxTQUFTLEtBSjFDLEVBSWlELE9BQU8sS0FKeEQsRUFLRyxFQUxILENBS00sVUFBVyxjQUxqQixFQUtpQyxTQUFTLEtBTDFDLEVBS2lELE9BQU8sS0FMeEQ7QUFPQSxjQUFHLFNBQVMsaUJBQVosRUFBK0I7QUFDN0Isb0JBQ0csRUFESCxDQUNNLFlBQVksY0FEbEIsRUFDa0MsU0FBUyxLQUQzQyxFQUNrRCxPQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLE9BRHJFO0FBR0Q7QUFDRCxpQkFDRyxJQURILENBQ1EsWUFBVztBQUNmLGdCQUNFLFNBQWEsRUFBRSxJQUFGLENBRGY7QUFBQSxnQkFFRSxPQUFhLE9BQU8sSUFBUCxDQUFZLE1BQVosQ0FGZjtBQUFBLGdCQUdFLGFBQWEsT0FBTyxHQUFQLENBQVcsV0FBWCxDQUF1QixJQUF2QixFQUE2QixNQUE3QixDQUhmO0FBS0EsY0FBRSxJQUFGLEVBQ0csRUFESCxDQUNNLGFBQWEsY0FEbkIsRUFDbUMsT0FBTyxLQUFQLENBQWEsS0FBYixDQUFtQixNQUR0RDtBQUdELFdBVkg7QUFZRCxTQTlGVzs7QUFnR1osZUFBTyxZQUFXO0FBQ2hCLGlCQUNHLElBREgsQ0FDUSxZQUFZO0FBQ2hCLGdCQUNFLFNBQWUsRUFBRSxJQUFGLENBRGpCO0FBQUEsZ0JBRUUsV0FBZSxPQUFPLE1BQVAsRUFGakI7QUFBQSxnQkFHRSxjQUFlLE9BQU8sT0FBUCxDQUFlLE1BQWYsQ0FIakI7QUFBQSxnQkFJRSxVQUFlLFlBQVksSUFBWixDQUFpQixTQUFTLE1BQTFCLENBSmpCO0FBQUEsZ0JBS0UsZUFBZSxPQUFPLElBQVAsQ0FBWSxTQUFTLFlBQXJCLEtBQXNDLEVBTHZEO0FBQUEsZ0JBTUUsYUFBZSxTQUFTLEVBQVQsQ0FBWSxTQUFTLFVBQXJCLENBTmpCO0FBQUEsZ0JBT0UsYUFBZSxTQUFTLEVBQVQsQ0FBWSxTQUFTLFVBQXJCLENBUGpCO0FBQUEsZ0JBUUUsWUFBZSxZQUFZLFFBQVosQ0FBcUIsVUFBVSxLQUEvQixDQVJqQjtBQVVBLGdCQUFHLFNBQUgsRUFBYztBQUNaLHFCQUFPLE9BQVAsQ0FBZSwwQkFBZixFQUEyQyxXQUEzQztBQUNBLDBCQUFZLFdBQVosQ0FBd0IsVUFBVSxLQUFsQztBQUNBLHNCQUFRLE1BQVI7QUFDRDtBQUNELGdCQUFHLFVBQUgsRUFBZTtBQUNiLHFCQUFPLE9BQVAsQ0FBZSwwQkFBZixFQUEyQyxRQUEzQyxFQUFxRCxZQUFyRDtBQUNBLHVCQUFTLFFBQVQsQ0FBa0IsT0FBbEI7QUFDRCxhQUhELE1BSUssSUFBRyxVQUFILEVBQWU7QUFDbEIscUJBQU8sSUFBUCxDQUFZLFNBQVosRUFBdUIsS0FBdkI7QUFDRCxhQUZJLE1BR0E7QUFDSCxxQkFBTyxPQUFQLENBQWUsdUJBQWYsRUFBd0MsTUFBeEMsRUFBZ0QsWUFBaEQ7QUFDQSxxQkFBTyxHQUFQLENBQVcsRUFBWDtBQUNEO0FBQ0YsV0E1Qkg7QUE4QkQsU0EvSFc7O0FBaUlaLGVBQU8sWUFBVztBQUNoQixpQkFDRyxJQURILENBQ1EsWUFBWTtBQUNoQixnQkFDRSxTQUFlLEVBQUUsSUFBRixDQURqQjtBQUFBLGdCQUVFLFdBQWUsT0FBTyxNQUFQLEVBRmpCO0FBQUEsZ0JBR0UsY0FBZSxPQUFPLE9BQVAsQ0FBZSxNQUFmLENBSGpCO0FBQUEsZ0JBSUUsVUFBZSxZQUFZLElBQVosQ0FBaUIsU0FBUyxNQUExQixDQUpqQjtBQUFBLGdCQUtFLGVBQWUsT0FBTyxJQUFQLENBQVksU0FBUyxZQUFyQixDQUxqQjtBQUFBLGdCQU1FLGFBQWUsU0FBUyxFQUFULENBQVksU0FBUyxVQUFyQixDQU5qQjtBQUFBLGdCQU9FLGFBQWUsU0FBUyxFQUFULENBQVksU0FBUyxVQUFyQixDQVBqQjtBQUFBLGdCQVFFLFlBQWUsWUFBWSxRQUFaLENBQXFCLFVBQVUsS0FBL0IsQ0FSakI7QUFVQSxnQkFBRyxpQkFBaUIsU0FBcEIsRUFBK0I7QUFDN0I7QUFDRDtBQUNELGdCQUFHLFNBQUgsRUFBYztBQUNaLHFCQUFPLE9BQVAsQ0FBZSwwQkFBZixFQUEyQyxXQUEzQztBQUNBLDBCQUFZLFdBQVosQ0FBd0IsVUFBVSxLQUFsQztBQUNBLHNCQUFRLE1BQVI7QUFDRDtBQUNELGdCQUFHLFVBQUgsRUFBZTtBQUNiLHFCQUFPLE9BQVAsQ0FBZSwwQkFBZixFQUEyQyxRQUEzQyxFQUFxRCxZQUFyRDtBQUNBLHVCQUFTLFFBQVQsQ0FBa0Isa0JBQWxCO0FBQ0QsYUFIRCxNQUlLLElBQUcsVUFBSCxFQUFlO0FBQ2xCLHFCQUFPLE9BQVAsQ0FBZSwwQkFBZixFQUEyQyxRQUEzQyxFQUFxRCxZQUFyRDtBQUNBLHFCQUFPLElBQVAsQ0FBWSxTQUFaLEVBQXVCLFlBQXZCO0FBQ0QsYUFISSxNQUlBO0FBQ0gscUJBQU8sT0FBUCxDQUFlLHVCQUFmLEVBQXdDLE1BQXhDLEVBQWdELFlBQWhEO0FBQ0EscUJBQU8sR0FBUCxDQUFXLFlBQVg7QUFDRDtBQUNGLFdBaENIO0FBa0NELFNBcEtXOztBQXNLWixZQUFJO0FBQ0YseUJBQWUsVUFBUyxJQUFULEVBQWU7QUFDNUIsbUJBQVEsS0FBSyxJQUFMLElBQWEsS0FBSyxJQUFMLENBQVUsS0FBVixDQUFnQixTQUFTLE1BQVQsQ0FBZ0IsT0FBaEMsQ0FBckI7QUFDRCxXQUhDO0FBSUYsaUJBQU8sVUFBUyxNQUFULEVBQWlCO0FBQ3RCLGdCQUFHLENBQUMsTUFBRCxJQUFXLE9BQU8sTUFBUCxLQUFrQixDQUFoQyxFQUFtQztBQUNqQyxxQkFBTyxJQUFQO0FBQ0QsYUFGRCxNQUdLLElBQUcsT0FBTyxFQUFQLENBQVUsd0JBQVYsQ0FBSCxFQUF3QztBQUMzQyxxQkFBTyxDQUFDLE9BQU8sRUFBUCxDQUFVLFVBQVYsQ0FBUjtBQUNELGFBRkksTUFHQTtBQUNILHFCQUFPLE9BQU8sRUFBUCxDQUFVLEtBQVYsQ0FBZ0IsTUFBaEIsQ0FBUDtBQUNEO0FBQ0YsV0FkQztBQWVGLGlCQUFPLFVBQVMsTUFBVCxFQUFpQjtBQUN0QixtQkFBTyxFQUFFLElBQUYsQ0FBTyxPQUFPLEdBQVAsRUFBUCxNQUF5QixFQUFoQztBQUNELFdBakJDO0FBa0JGLGlCQUFPLFlBQVc7QUFDaEIsZ0JBQ0UsV0FBVyxJQURiO0FBR0EsbUJBQU8sT0FBUCxDQUFlLDJCQUFmO0FBQ0EsY0FBRSxJQUFGLENBQU8sVUFBUCxFQUFtQixVQUFTLFNBQVQsRUFBb0IsS0FBcEIsRUFBMkI7QUFDNUMsa0JBQUksQ0FBRyxPQUFPLFFBQVAsQ0FBZ0IsS0FBaEIsQ0FBc0IsS0FBdEIsRUFBNkIsU0FBN0IsQ0FBUCxFQUFtRDtBQUNqRCwyQkFBVyxLQUFYO0FBQ0Q7QUFDRixhQUpEO0FBS0EsbUJBQU8sUUFBUDtBQUNEO0FBN0JDLFNBdEtROztBQXNNWixzQkFBYyxZQUFXO0FBQ3ZCLGtCQUNHLEdBREgsQ0FDTyxjQURQO0FBR0EsaUJBQ0csR0FESCxDQUNPLGNBRFA7QUFHQSxrQkFDRyxHQURILENBQ08sY0FEUDtBQUdBLGlCQUNHLEdBREgsQ0FDTyxjQURQO0FBR0QsU0FuTlc7O0FBcU5aLGVBQU87QUFDTCxpQkFBTztBQUNMLHFCQUFTLFVBQVMsS0FBVCxFQUFnQjtBQUN2QixrQkFDRSxTQUFlLEVBQUUsSUFBRixDQURqQjtBQUFBLGtCQUVFLE1BQWUsTUFBTSxLQUZ2QjtBQUFBLGtCQUdFLFVBQWUsT0FBTyxFQUFQLENBQVUsU0FBUyxLQUFuQixDQUhqQjtBQUFBLGtCQUlFLGFBQWUsT0FBTyxFQUFQLENBQVUsU0FBUyxRQUFuQixDQUpqQjtBQUFBLGtCQUtFLGVBQWdCLE9BQU8sT0FBUCxDQUFlLFNBQVMsVUFBeEIsRUFBb0MsTUFBcEMsR0FBNkMsQ0FML0Q7QUFBQSxrQkFNRSxVQUFlO0FBQ2IsdUJBQVMsRUFESTtBQUViLHdCQUFTO0FBRkksZUFOakI7QUFXQSxrQkFBSSxPQUFPLFFBQVEsTUFBbkIsRUFBMkI7QUFDekIsdUJBQU8sT0FBUCxDQUFlLG1DQUFmO0FBQ0EsdUJBQ0csSUFESDtBQUdEO0FBQ0Qsa0JBQUcsQ0FBQyxNQUFNLE9BQVAsSUFBa0IsT0FBTyxRQUFRLEtBQWpDLElBQTBDLE9BQTFDLElBQXFELENBQUMsWUFBdEQsSUFBc0UsQ0FBQyxVQUExRSxFQUFzRjtBQUNwRixvQkFBRyxDQUFDLFdBQUosRUFBaUI7QUFDZix5QkFDRyxHQURILENBQ08sVUFBVSxjQURqQixFQUNpQyxPQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLEtBRHBEO0FBR0EseUJBQU8sTUFBUDtBQUNBLHlCQUFPLEtBQVAsQ0FBYSx3Q0FBYjtBQUNEO0FBQ0QsOEJBQWMsSUFBZDtBQUNEO0FBQ0YsYUE3Qkk7QUE4QkwsbUJBQU8sWUFBVztBQUNoQiw0QkFBYyxLQUFkO0FBQ0QsYUFoQ0k7QUFpQ0wsa0JBQU0sVUFBUyxLQUFULEVBQWdCO0FBQ3BCLGtCQUNFLFNBQWtCLEVBQUUsSUFBRixDQURwQjtBQUFBLGtCQUVFLGNBQWtCLE9BQU8sT0FBUCxDQUFlLE1BQWYsQ0FGcEI7QUFBQSxrQkFHRSxrQkFBa0IsT0FBTyxHQUFQLENBQVcsVUFBWCxDQUFzQixNQUF0QixDQUhwQjtBQUtBLGtCQUFJLFlBQVksUUFBWixDQUFxQixVQUFVLEtBQS9CLENBQUosRUFBNEM7QUFDMUMsdUJBQU8sS0FBUCxDQUFhLG9CQUFiLEVBQW1DLE1BQW5DLEVBQTJDLGVBQTNDO0FBQ0Esb0JBQUcsZUFBSCxFQUFvQjtBQUNsQix5QkFBTyxRQUFQLENBQWdCLEtBQWhCLENBQXVCLGVBQXZCO0FBQ0Q7QUFDRixlQUxELE1BTUssSUFBRyxTQUFTLEVBQVQsSUFBZSxNQUFmLElBQXlCLFNBQVMsRUFBVCxJQUFlLFFBQTNDLEVBQXFEO0FBQ3hELG9CQUFHLGVBQUgsRUFBb0I7QUFDbEIseUJBQU8sUUFBUCxDQUFnQixLQUFoQixDQUF1QixlQUF2QjtBQUNEO0FBQ0Y7QUFDRixhQWxESTtBQW1ETCxvQkFBUSxVQUFTLEtBQVQsRUFBZ0I7QUFDdEIsa0JBQ0UsU0FBYyxFQUFFLElBQUYsQ0FEaEI7QUFBQSxrQkFFRSxjQUFjLE9BQU8sT0FBUCxDQUFlLE1BQWYsQ0FGaEI7QUFBQSxrQkFHRSxrQkFBa0IsT0FBTyxHQUFQLENBQVcsVUFBWCxDQUFzQixNQUF0QixDQUhwQjtBQUtBLGtCQUFHLFNBQVMsRUFBVCxJQUFlLFFBQWYsSUFBNkIsWUFBWSxRQUFaLENBQXFCLFVBQVUsS0FBL0IsS0FBeUMsU0FBUyxVQUFsRixFQUFnRztBQUM5Riw2QkFBYSxPQUFPLEtBQXBCO0FBQ0EsdUJBQU8sS0FBUCxHQUFlLFdBQVcsWUFBVztBQUNuQyx5QkFBTyxLQUFQLENBQWEsb0JBQWIsRUFBbUMsTUFBbkMsRUFBNEMsT0FBTyxHQUFQLENBQVcsVUFBWCxDQUFzQixNQUF0QixDQUE1QztBQUNBLHlCQUFPLFFBQVAsQ0FBZ0IsS0FBaEIsQ0FBdUIsZUFBdkI7QUFDRCxpQkFIYyxFQUdaLFNBQVMsS0FIRyxDQUFmO0FBSUQ7QUFDRjtBQWhFSTs7QUFERixTQXJOSzs7QUEyUlosYUFBSztBQUNILDBCQUFnQixVQUFTLElBQVQsRUFBZTtBQUM3QixnQkFBRyxDQUFDLEtBQUssSUFBTixJQUFjLENBQUMsT0FBTyxFQUFQLENBQVUsYUFBVixDQUF3QixJQUF4QixDQUFsQixFQUFpRDtBQUMvQyxxQkFBTyxLQUFQO0FBQ0Q7QUFDRCxtQkFBTyxLQUFLLElBQUwsQ0FBVSxLQUFWLENBQWdCLFNBQVMsTUFBVCxDQUFnQixPQUFoQyxFQUF5QyxDQUF6QyxJQUE4QyxFQUFyRDtBQUNELFdBTkU7QUFPSCxvQkFBVSxVQUFTLElBQVQsRUFBZTtBQUN2QixnQkFBSSxPQUFPLEVBQVAsQ0FBVSxhQUFWLENBQXdCLElBQXhCLENBQUosRUFBb0M7QUFDbEMscUJBQU8sS0FBSyxJQUFMLENBQVUsT0FBVixDQUFrQixLQUFLLElBQUwsQ0FBVSxLQUFWLENBQWdCLFNBQVMsTUFBVCxDQUFnQixPQUFoQyxFQUF5QyxDQUF6QyxDQUFsQixFQUErRCxFQUEvRCxDQUFQO0FBQ0Q7QUFDRCxtQkFBTyxLQUFLLElBQVo7QUFDRCxXQVpFO0FBYUgsdUJBQWEsVUFBUyxJQUFULEVBQWUsTUFBZixFQUF1QjtBQUNsQyxnQkFBRyxRQUFRLFVBQVIsSUFBc0IsUUFBUSxPQUE5QixJQUF5QyxRQUFRLFFBQWpELElBQTZELE9BQU8sRUFBUCxDQUFVLFFBQVYsQ0FBaEUsRUFBcUY7QUFDbkYscUJBQU8sUUFBUDtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLE9BQU8sR0FBUCxDQUFXLFVBQVgsRUFBUDtBQUNEO0FBQ0YsV0FwQkU7QUFxQkgsc0JBQVksWUFBVztBQUNyQixtQkFBUSxTQUFTLGFBQVQsQ0FBdUIsT0FBdkIsRUFBZ0MsT0FBaEMsS0FBNEMsU0FBN0MsR0FDSCxPQURHLEdBRUYsU0FBUyxhQUFULENBQXVCLE9BQXZCLEVBQWdDLGdCQUFoQyxLQUFxRCxTQUF0RCxHQUNFLGdCQURGLEdBRUUsT0FKTjtBQU1ELFdBNUJFO0FBNkJILGtCQUFRLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDNUIsZ0JBQ0UsV0FBZ0IsT0FBTyxHQUFQLENBQVcsUUFBWCxDQUFvQixJQUFwQixDQURsQjtBQUFBLGdCQUVFLFlBQWdCLE9BQU8sR0FBUCxDQUFXLGNBQVgsQ0FBMEIsSUFBMUIsQ0FGbEI7QUFBQSxnQkFHRSxTQUFnQixLQUFLLE1BQUwsSUFBZSxTQUFTLE1BQVQsQ0FBZ0IsUUFBaEIsQ0FBZixJQUE0QyxTQUFTLElBQVQsQ0FBYyxlQUg1RTtBQUFBLGdCQUlFLGdCQUFpQixPQUFPLE1BQVAsQ0FBYyxTQUFkLE1BQTZCLENBQUMsQ0FKakQ7QUFBQSxnQkFLRSxlQUFpQixPQUFPLE1BQVAsQ0FBYyxRQUFkLE1BQTRCLENBQUMsQ0FMaEQ7QUFBQSxnQkFNRSxNQU5GO0FBQUEsZ0JBT0UsTUFQRjtBQUFBLGdCQVFFLElBUkY7QUFVQSxnQkFBRyxnQkFBZ0IsYUFBbkIsRUFBa0M7QUFDaEMsdUJBQVMsT0FBTyxHQUFQLENBQVcsS0FBWCxDQUFpQixNQUFNLFVBQXZCLENBQVQ7QUFDRDtBQUNELGdCQUFHLGFBQUgsRUFBa0I7QUFDaEIsdUJBQVMsT0FBTyxPQUFQLENBQWUsU0FBZixFQUEwQixPQUFPLEdBQVAsRUFBMUIsQ0FBVDtBQUNEO0FBQ0QsZ0JBQUcsWUFBSCxFQUFpQjtBQUNmLHVCQUFTLE9BQU8sT0FBUCxDQUFlLFNBQVMsS0FBeEIsRUFBK0IsSUFBL0IsQ0FBb0MsT0FBcEMsRUFBNkMsRUFBN0MsQ0FBZ0QsQ0FBaEQsQ0FBVDtBQUNBLHFCQUFRLE9BQU8sTUFBUCxJQUFpQixDQUFsQixHQUNILE9BQU8sSUFBUCxFQURHLEdBRUgsT0FBTyxJQUFQLENBQVksYUFBWixLQUE4QixTQUFTLElBQVQsQ0FBYyxnQkFGaEQ7QUFJQSx1QkFBUyxPQUFPLE9BQVAsQ0FBZSxRQUFmLEVBQXlCLElBQXpCLENBQVQ7QUFDRDtBQUNELHFCQUFTLE9BQU8sT0FBUCxDQUFlLGNBQWYsRUFBK0IsTUFBTSxVQUFyQyxDQUFUO0FBQ0EscUJBQVMsT0FBTyxPQUFQLENBQWUsYUFBZixFQUE4QixTQUE5QixDQUFUO0FBQ0EsZ0JBQUcsQ0FBQyxLQUFLLE1BQVQsRUFBaUI7QUFDZixxQkFBTyxPQUFQLENBQWUsMENBQWYsRUFBMkQsTUFBM0QsRUFBbUUsUUFBbkU7QUFDRDtBQUNELG1CQUFPLE1BQVA7QUFDRCxXQTVERTtBQTZESCxvQkFBVSxZQUFXO0FBQ25CLGdCQUFHLEVBQUUsYUFBRixDQUFnQixVQUFoQixDQUFILEVBQWdDO0FBQzlCLGtCQUNFLE9BQVcsT0FBTyxJQUFQLENBQVksVUFBWixDQURiO0FBQUEsa0JBRUUsbUJBQW9CLEtBQUssTUFBTCxHQUFjLENBQWYsR0FDZCxXQUFXLEtBQUssQ0FBTCxDQUFYLEVBQW9CLFVBQXBCLEtBQW1DLFNBQW5DLElBQWdELFdBQVcsS0FBSyxDQUFMLENBQVgsRUFBb0IsS0FBcEIsS0FBOEIsU0FEaEUsR0FFZixLQUpOO0FBQUEsa0JBS0UsUUFMRjtBQU9BLGtCQUFHLGdCQUFILEVBQXFCOztBQUVuQiwyQkFBYSxFQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQixFQUFFLEVBQUYsQ0FBSyxJQUFMLENBQVUsUUFBN0IsRUFBdUMsZ0JBQXZDLENBQWI7QUFDQSw2QkFBYSxFQUFFLE1BQUYsQ0FBUyxFQUFULEVBQWEsRUFBRSxFQUFGLENBQUssSUFBTCxDQUFVLFFBQVYsQ0FBbUIsUUFBaEMsRUFBMEMsVUFBMUMsQ0FBYjtBQUNBLHVCQUFPLEtBQVAsQ0FBYSxTQUFTLEtBQVQsQ0FBZSxTQUE1QixFQUF1QyxPQUF2QztBQUNBLHVCQUFPLE9BQVAsQ0FBZSwyQ0FBZixFQUE0RCxVQUE1RCxFQUF3RSxRQUF4RTtBQUNELGVBTkQsTUFPSzs7QUFFSCxvQkFBRyxXQUFXLE1BQWQsRUFBc0I7QUFDcEIsNkJBQVcsT0FBTyxJQUFQLENBQVksV0FBVyxNQUF2QixDQUFYO0FBQ0Esc0JBQUksT0FBTyxXQUFXLE1BQVgsQ0FBa0IsU0FBUyxDQUFULENBQWxCLENBQVAsSUFBeUMsUUFBekMsSUFBcUQsRUFBRSxPQUFGLENBQVUsV0FBVyxNQUFYLENBQWtCLFNBQVMsQ0FBVCxDQUFsQixDQUFWLENBQXpELEVBQXFHO0FBQ25HLHNCQUFFLElBQUYsQ0FBTyxXQUFXLE1BQWxCLEVBQTBCLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDOUMsMEJBQUcsT0FBTyxLQUFQLElBQWdCLFFBQW5CLEVBQTZCO0FBQzNCLGdDQUFRLENBQUMsS0FBRCxDQUFSO0FBQ0Q7QUFDRCxpQ0FBVyxNQUFYLENBQWtCLElBQWxCLElBQTBCO0FBQ3hCLCtCQUFPO0FBRGlCLHVCQUExQjtBQUdBLHdCQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ2xDLG1DQUFXLE1BQVgsQ0FBa0IsSUFBbEIsRUFBd0IsS0FBeEIsQ0FBOEIsSUFBOUIsQ0FBbUMsRUFBRSxNQUFNLElBQVIsRUFBbkM7QUFDRCx1QkFGRDtBQUdELHFCQVZEO0FBV0Q7QUFDRjs7QUFFRCwyQkFBYSxFQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQixFQUFFLEVBQUYsQ0FBSyxJQUFMLENBQVUsUUFBN0IsRUFBdUMsVUFBdkMsQ0FBYjtBQUNBLDZCQUFhLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxFQUFFLEVBQUYsQ0FBSyxJQUFMLENBQVUsUUFBVixDQUFtQixRQUFoQyxFQUEwQyxTQUFTLE1BQW5ELENBQWI7QUFDQSx1QkFBTyxPQUFQLENBQWUsb0JBQWYsRUFBcUMsVUFBckMsRUFBaUQsUUFBakQ7QUFDRDtBQUNGLGFBdENELE1BdUNLO0FBQ0gseUJBQWEsRUFBRSxFQUFGLENBQUssSUFBTCxDQUFVLFFBQXZCO0FBQ0EsMkJBQWEsRUFBRSxFQUFGLENBQUssSUFBTCxDQUFVLFFBQVYsQ0FBbUIsUUFBaEM7QUFDQSxxQkFBTyxPQUFQLENBQWUsK0JBQWYsRUFBZ0QsVUFBaEQsRUFBNEQsUUFBNUQ7QUFDRDs7O0FBR0Qsd0JBQWtCLFNBQVMsU0FBM0I7QUFDQSx1QkFBa0IsU0FBUyxRQUEzQjtBQUNBLHVCQUFrQixTQUFTLFFBQTNCO0FBQ0Esd0JBQWtCLFNBQVMsU0FBM0I7QUFDQSxvQkFBa0IsU0FBUyxLQUEzQjtBQUNBLDhCQUFrQixZQUFZLFNBQTlCO0FBQ0EsNkJBQWtCLE1BQU0sU0FBeEI7OztBQUdBLHVCQUFXLFFBQVEsSUFBUixDQUFhLGVBQWIsQ0FBWDs7O0FBR0EsbUJBQU8sT0FBUDtBQUNELFdBekhFO0FBMEhILGlCQUFPLFVBQVMsVUFBVCxFQUFxQjtBQUMxQixtQkFBTyxPQUFQLENBQWUsK0JBQWYsRUFBZ0QsVUFBaEQ7QUFDQSxnQkFBSSxPQUFPLE1BQVAsQ0FBYyxNQUFNLFVBQXBCLEVBQWdDLE1BQWhDLEdBQXlDLENBQTdDLEVBQWlEO0FBQy9DLHFCQUFPLE9BQU8sTUFBUCxDQUFjLE1BQU0sVUFBcEIsQ0FBUDtBQUNELGFBRkQsTUFHSyxJQUFJLE9BQU8sTUFBUCxDQUFjLFlBQVksVUFBWixHQUF3QixJQUF0QyxFQUE0QyxNQUE1QyxHQUFxRCxDQUF6RCxFQUE2RDtBQUNoRSxxQkFBTyxPQUFPLE1BQVAsQ0FBYyxZQUFZLFVBQVosR0FBd0IsSUFBdEMsQ0FBUDtBQUNELGFBRkksTUFHQSxJQUFJLE9BQU8sTUFBUCxDQUFjLFlBQVksVUFBWixHQUF3QixNQUF0QyxFQUE4QyxNQUE5QyxHQUF1RCxDQUEzRCxFQUErRDtBQUNsRSxxQkFBTyxPQUFPLE1BQVAsQ0FBYyxZQUFZLFVBQVosR0FBd0IsTUFBdEMsQ0FBUDtBQUNELGFBRkksTUFHQSxJQUFJLE9BQU8sTUFBUCxDQUFjLFdBQVcsU0FBUyxRQUFwQixHQUErQixJQUEvQixHQUFxQyxVQUFyQyxHQUFpRCxJQUEvRCxFQUFxRSxNQUFyRSxHQUE4RSxDQUFsRixFQUFzRjtBQUN6RixxQkFBTyxPQUFPLE1BQVAsQ0FBYyxXQUFXLFNBQVMsUUFBcEIsR0FBK0IsSUFBL0IsR0FBcUMsVUFBckMsR0FBaUQsSUFBL0QsQ0FBUDtBQUNEO0FBQ0QsbUJBQU8sRUFBRSxVQUFGLENBQVA7QUFDRCxXQXpJRTtBQTBJSCxrQkFBUSxVQUFTLE1BQVQsRUFBaUI7QUFDdkIsZ0JBQ0UsVUFBVSxHQURaO0FBR0EsY0FBRSxJQUFGLENBQU8sTUFBUCxFQUFlLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjtBQUNuQyx3QkFBVSxRQUFRLEdBQVIsQ0FBYSxPQUFPLEdBQVAsQ0FBVyxLQUFYLENBQWlCLElBQWpCLENBQWIsQ0FBVjtBQUNELGFBRkQ7QUFHQSxtQkFBTyxPQUFQO0FBQ0QsV0FsSkU7QUFtSkgsc0JBQVksVUFBUyxNQUFULEVBQWlCO0FBQzNCLGdCQUNFLGVBREYsRUFFRSxVQUZGO0FBSUEsZ0JBQUcsQ0FBQyxVQUFKLEVBQWdCO0FBQ2QscUJBQU8sS0FBUDtBQUNEO0FBQ0QsY0FBRSxJQUFGLENBQU8sVUFBUCxFQUFtQixVQUFTLFNBQVQsRUFBb0IsS0FBcEIsRUFBMkI7QUFDNUMsMkJBQWEsTUFBTSxVQUFOLElBQW9CLFNBQWpDO0FBQ0Esa0JBQUksT0FBTyxHQUFQLENBQVcsS0FBWCxDQUFpQixVQUFqQixFQUE2QixDQUE3QixLQUFtQyxPQUFPLENBQVAsQ0FBdkMsRUFBbUQ7QUFDakQsc0JBQU0sVUFBTixHQUFtQixVQUFuQjtBQUNBLGtDQUFrQixLQUFsQjtBQUNEO0FBQ0YsYUFORDtBQU9BLG1CQUFPLG1CQUFtQixLQUExQjtBQUNELFdBbktFO0FBb0tILGlCQUFPLFVBQVUsS0FBVixFQUFpQjtBQUN0QixnQkFDRSxTQUFTLEVBRFg7QUFBQSxnQkFFRSxPQUZGO0FBSUEsbUJBQU8sSUFBUCxDQUFZLEtBQVo7QUFDQSxzQkFBVSxPQUFPLEdBQVAsQ0FBVyxNQUFYLENBQWtCLElBQWxCLENBQXVCLE9BQXZCLEVBQWdDLE1BQWhDLENBQVY7QUFDQSxtQkFBTyxRQUFRLEtBQVIsQ0FBUDtBQUNELFdBNUtFO0FBNktILGtCQUFRLFVBQVUsTUFBVixFQUFrQjtBQUN4QixnQkFDRSxVQUFVLEVBQUUsT0FBRixDQUFVLE1BQVYsSUFDTixPQUFPLEdBQVAsQ0FBVyxNQUFYLENBQWtCLE1BQWxCLENBRE0sR0FFTixNQUhOO0FBQUEsZ0JBSUUsU0FBUyxFQUpYO0FBTUEsb0JBQVEsSUFBUixDQUFhLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUF1QjtBQUNsQyxrQkFDRSxTQUFhLEVBQUUsS0FBRixDQURmO0FBQUEsa0JBRUUsT0FBYSxPQUFPLElBQVAsQ0FBWSxNQUFaLENBRmY7QUFBQSxrQkFHRSxPQUFhLE9BQU8sSUFBUCxDQUFZLE1BQVosQ0FIZjtBQUFBLGtCQUlFLFFBQWEsT0FBTyxHQUFQLEVBSmY7QUFBQSxrQkFLRSxhQUFhLE9BQU8sRUFBUCxDQUFVLFNBQVMsUUFBbkIsQ0FMZjtBQUFBLGtCQU1FLFVBQWEsT0FBTyxFQUFQLENBQVUsU0FBUyxLQUFuQixDQU5mO0FBQUEsa0JBT0UsYUFBYyxLQUFLLE9BQUwsQ0FBYSxJQUFiLE1BQXVCLENBQUMsQ0FQeEM7QUFBQSxrQkFRRSxZQUFjLFVBQUQsR0FDVCxPQUFPLEVBQVAsQ0FBVSxVQUFWLENBRFMsR0FFVCxLQVZOO0FBWUEsa0JBQUcsSUFBSCxFQUFTO0FBQ1Asb0JBQUcsVUFBSCxFQUFlO0FBQ2IseUJBQU8sS0FBSyxPQUFMLENBQWEsSUFBYixFQUFtQixFQUFuQixDQUFQO0FBQ0Esc0JBQUcsQ0FBQyxPQUFPLElBQVAsQ0FBSixFQUFrQjtBQUNoQiwyQkFBTyxJQUFQLElBQWUsRUFBZjtBQUNEO0FBQ0Qsc0JBQUcsVUFBSCxFQUFlO0FBQ2Isd0JBQUcsU0FBSCxFQUFjO0FBQ1osNkJBQU8sSUFBUCxFQUFhLElBQWIsQ0FBa0IsU0FBUyxJQUEzQjtBQUNELHFCQUZELE1BR0s7QUFDSCw2QkFBTyxJQUFQLEVBQWEsSUFBYixDQUFrQixLQUFsQjtBQUNEO0FBQ0YsbUJBUEQsTUFRSztBQUNILDJCQUFPLElBQVAsRUFBYSxJQUFiLENBQWtCLEtBQWxCO0FBQ0Q7QUFDRixpQkFoQkQsTUFpQks7QUFDSCxzQkFBRyxPQUFILEVBQVk7QUFDVix3QkFBRyxTQUFILEVBQWM7QUFDWiw2QkFBTyxJQUFQLElBQWUsS0FBZjtBQUNEO0FBQ0YsbUJBSkQsTUFLSyxJQUFHLFVBQUgsRUFBZTtBQUNsQix3QkFBRyxTQUFILEVBQWM7QUFDWiw2QkFBTyxJQUFQLElBQWUsU0FBUyxJQUF4QjtBQUNELHFCQUZELE1BR0s7QUFDSCw2QkFBTyxJQUFQLElBQWUsS0FBZjtBQUNEO0FBQ0YsbUJBUEksTUFRQTtBQUNILDJCQUFPLElBQVAsSUFBZSxLQUFmO0FBQ0Q7QUFDRjtBQUNGO0FBQ0YsYUFsREQ7QUFtREEsbUJBQU8sTUFBUDtBQUNEO0FBeE9FLFNBM1JPOztBQXNnQlosYUFBSzs7QUFFSCxpQkFBTyxVQUFTLFVBQVQsRUFBcUI7QUFDMUIsbUJBQU8sT0FBUCxDQUFlLG1EQUFmLEVBQW9FLFVBQXBFO0FBQ0EsZ0JBQUcsT0FBTyxVQUFQLEtBQXNCLFFBQXpCLEVBQW1DO0FBQ2pDLHFCQUFPLEtBQVAsQ0FBYSxNQUFNLFVBQW5CLEVBQStCLFVBQS9CO0FBQ0Q7QUFDRCxnQkFBSSxPQUFPLE1BQVAsQ0FBYyxNQUFNLFVBQXBCLEVBQWdDLE1BQWhDLEdBQXlDLENBQTdDLEVBQWlEO0FBQy9DLHFCQUFPLElBQVA7QUFDRCxhQUZELE1BR0ssSUFBSSxPQUFPLE1BQVAsQ0FBYyxZQUFZLFVBQVosR0FBd0IsSUFBdEMsRUFBNEMsTUFBNUMsR0FBcUQsQ0FBekQsRUFBNkQ7QUFDaEUscUJBQU8sSUFBUDtBQUNELGFBRkksTUFHQSxJQUFJLE9BQU8sTUFBUCxDQUFjLFdBQVcsU0FBUyxRQUFwQixHQUErQixJQUEvQixHQUFxQyxVQUFyQyxHQUFpRCxJQUEvRCxFQUFxRSxNQUFyRSxHQUE4RSxDQUFsRixFQUFzRjtBQUN6RixxQkFBTyxJQUFQO0FBQ0Q7QUFDRCxtQkFBTyxLQUFQO0FBQ0Q7O0FBakJFLFNBdGdCTzs7QUEyaEJaLGFBQUs7QUFDSCxrQkFBUSxVQUFTLFVBQVQsRUFBcUIsTUFBckIsRUFBNkI7QUFDbkMsZ0JBQ0UsU0FBZSxPQUFPLEdBQVAsQ0FBVyxLQUFYLENBQWlCLFVBQWpCLENBRGpCO0FBQUEsZ0JBRUUsY0FBZSxPQUFPLE9BQVAsQ0FBZSxNQUFmLENBRmpCO0FBQUEsZ0JBR0UsVUFBZSxZQUFZLFFBQVosQ0FBcUIsU0FBUyxNQUE5QixDQUhqQjtBQUFBLGdCQUlFLGVBQWdCLFFBQVEsTUFBUixLQUFtQixDQUpyQztBQU1BLHFCQUFVLE9BQU8sTUFBUCxJQUFpQixRQUFsQixHQUNMLENBQUMsTUFBRCxDQURLLEdBRUwsTUFGSjtBQUlBLG1CQUFPLE9BQVAsQ0FBZSwwQkFBZixFQUEyQyxVQUEzQztBQUNBLHdCQUNHLFFBREgsQ0FDWSxVQUFVLEtBRHRCO0FBR0EsZ0JBQUcsU0FBUyxNQUFaLEVBQW9CO0FBQ2xCLGtCQUFHLENBQUMsWUFBSixFQUFrQjtBQUNoQiwwQkFBVSxTQUFTLFNBQVQsQ0FBbUIsTUFBbkIsQ0FBMEIsTUFBMUIsQ0FBVjtBQUNBLHdCQUNHLFFBREgsQ0FDWSxXQURaO0FBR0Q7QUFDRCxzQkFDRyxJQURILENBQ1EsT0FBTyxDQUFQLENBRFI7QUFHQSxrQkFBRyxDQUFDLFlBQUosRUFBa0I7QUFDaEIsb0JBQUcsU0FBUyxVQUFULElBQXVCLEVBQUUsRUFBRixDQUFLLFVBQUwsS0FBb0IsU0FBM0MsSUFBd0QsUUFBUSxVQUFSLENBQW1CLGNBQW5CLENBQTNELEVBQStGO0FBQzdGLHlCQUFPLE9BQVAsQ0FBZSxzQ0FBZixFQUF1RCxTQUFTLFVBQWhFO0FBQ0EsMEJBQVEsVUFBUixDQUFtQixTQUFTLFVBQVQsR0FBc0IsS0FBekMsRUFBZ0QsU0FBUyxRQUF6RDtBQUNELGlCQUhELE1BSUs7QUFDSCx5QkFBTyxPQUFQLENBQWUscURBQWY7QUFDQSwwQkFDRyxNQURILENBQ1UsU0FBUyxRQURuQjtBQUdEO0FBQ0YsZUFYRCxNQVlLO0FBQ0gsdUJBQU8sT0FBUCxDQUFlLG1EQUFmLEVBQW9FLFVBQXBFO0FBQ0Q7QUFDRjtBQUNGLFdBMUNFO0FBMkNILGtCQUFRLFVBQVMsTUFBVCxFQUFpQjtBQUN2QixtQkFBTyxLQUFQLENBQWEsNEJBQWIsRUFBMkMsTUFBM0M7QUFDQSxtQkFBTyxHQUFQLENBQVcsS0FBWDtBQUNBLHFCQUNHLElBREgsQ0FDUyxTQUFTLFNBQVQsQ0FBbUIsS0FBbkIsQ0FBeUIsTUFBekIsQ0FEVDtBQUdEO0FBakRFLFNBM2hCTzs7QUEra0JaLGdCQUFRO0FBQ04sa0JBQVEsVUFBUyxVQUFULEVBQXFCO0FBQzNCLGdCQUNFLFNBQWMsT0FBTyxHQUFQLENBQVcsS0FBWCxDQUFpQixVQUFqQixDQURoQjtBQUFBLGdCQUVFLGNBQWMsT0FBTyxPQUFQLENBQWUsTUFBZixDQUZoQjtBQUFBLGdCQUdFLFVBQWMsWUFBWSxRQUFaLENBQXFCLFNBQVMsTUFBOUIsQ0FIaEI7QUFLQSx3QkFDRyxXQURILENBQ2UsVUFBVSxLQUR6QjtBQUdBLGdCQUFHLFNBQVMsTUFBVCxJQUFtQixRQUFRLEVBQVIsQ0FBVyxVQUFYLENBQXRCLEVBQThDO0FBQzVDLHFCQUFPLE9BQVAsQ0FBZSwyQkFBZixFQUE0QyxVQUE1QztBQUNBLGtCQUFHLFNBQVMsVUFBVCxJQUF1QixFQUFFLEVBQUYsQ0FBSyxVQUFMLEtBQW9CLFNBQTNDLElBQXdELFFBQVEsVUFBUixDQUFtQixjQUFuQixDQUEzRCxFQUErRjtBQUM3Rix3QkFBUSxVQUFSLENBQW1CLFNBQVMsVUFBVCxHQUFzQixNQUF6QyxFQUFpRCxTQUFTLFFBQTFELEVBQW9FLFlBQVc7QUFDN0UsMEJBQVEsTUFBUjtBQUNELGlCQUZEO0FBR0QsZUFKRCxNQUtLO0FBQ0gsd0JBQ0csT0FESCxDQUNXLFNBQVMsUUFEcEIsRUFDOEIsWUFBVTtBQUNwQywwQkFBUSxNQUFSO0FBQ0QsaUJBSEg7QUFLRDtBQUNGO0FBQ0Y7QUF6QkssU0Eva0JJOztBQTJtQlosYUFBSztBQUNILG1CQUFTLFlBQVc7QUFDbEIsb0JBQ0csV0FESCxDQUNlLFVBQVUsS0FEekIsRUFFRyxRQUZILENBRVksVUFBVSxPQUZ0QjtBQUlELFdBTkU7QUFPSCxvQkFBVSxZQUFZO0FBQ3BCLG1CQUNHLElBREgsQ0FDUSxZQUFZO0FBQ2hCLGtCQUNFLFNBQWEsRUFBRSxJQUFGLENBRGY7QUFBQSxrQkFFRSxhQUFjLE9BQU8sTUFBUCxDQUFjLFNBQVMsUUFBdkIsRUFBaUMsTUFBakMsR0FBMEMsQ0FGMUQ7QUFBQSxrQkFHRSxRQUFjLFVBQUQsR0FDVCxPQUFPLEVBQVAsQ0FBVSxVQUFWLENBRFMsR0FFVCxPQUFPLEdBQVAsRUFMTjtBQU9BLHFCQUFPLElBQVAsQ0FBWSxTQUFTLFlBQXJCLEVBQW1DLEtBQW5DO0FBQ0QsYUFWSDtBQVlELFdBcEJFO0FBcUJILGlCQUFPLFlBQVc7QUFDaEIsb0JBQ0csV0FESCxDQUNlLFVBQVUsT0FEekIsRUFFRyxRQUZILENBRVksVUFBVSxLQUZ0QjtBQUlELFdBMUJFO0FBMkJILGlCQUFPLFVBQVUsS0FBVixFQUFpQixLQUFqQixFQUF3QjtBQUM3QixnQkFDRSxTQUFTLEVBRFg7QUFHQSxtQkFBTyxLQUFQLElBQWdCLEtBQWhCO0FBQ0EsbUJBQU8sT0FBTyxHQUFQLENBQVcsTUFBWCxDQUFrQixJQUFsQixDQUF1QixPQUF2QixFQUFnQyxNQUFoQyxDQUFQO0FBQ0QsV0FqQ0U7QUFrQ0gsa0JBQVEsVUFBVSxNQUFWLEVBQWtCO0FBQ3hCLGdCQUFHLEVBQUUsYUFBRixDQUFnQixNQUFoQixDQUFILEVBQTRCO0FBQzFCO0FBQ0Q7QUFDRCxjQUFFLElBQUYsQ0FBTyxNQUFQLEVBQWUsVUFBUyxHQUFULEVBQWMsS0FBZCxFQUFxQjtBQUNsQyxrQkFDRSxTQUFjLE9BQU8sR0FBUCxDQUFXLEtBQVgsQ0FBaUIsR0FBakIsQ0FEaEI7QUFBQSxrQkFFRSxXQUFjLE9BQU8sTUFBUCxFQUZoQjtBQUFBLGtCQUdFLGFBQWMsRUFBRSxPQUFGLENBQVUsS0FBVixDQUhoQjtBQUFBLGtCQUlFLGFBQWMsU0FBUyxFQUFULENBQVksU0FBUyxVQUFyQixDQUpoQjtBQUFBLGtCQUtFLGFBQWMsU0FBUyxFQUFULENBQVksU0FBUyxVQUFyQixDQUxoQjtBQUFBLGtCQU1FLFVBQWUsT0FBTyxFQUFQLENBQVUsU0FBUyxLQUFuQixLQUE2QixVQU45QztBQUFBLGtCQU9FLGNBQWUsT0FBTyxNQUFQLEdBQWdCLENBUGpDO0FBQUEsa0JBUUUsY0FSRjtBQVVBLGtCQUFHLFdBQUgsRUFBZ0I7QUFDZCxvQkFBRyxjQUFjLFVBQWpCLEVBQTZCO0FBQzNCLHlCQUFPLE9BQVAsQ0FBZSxvQkFBZixFQUFxQyxLQUFyQyxFQUE0QyxNQUE1QztBQUNBLDJCQUFTLFFBQVQsQ0FBa0IsU0FBbEI7QUFDQSxvQkFBRSxJQUFGLENBQU8sS0FBUCxFQUFjLFVBQVMsS0FBVCxFQUFnQixLQUFoQixFQUF1QjtBQUNuQyxxQ0FBaUIsT0FBTyxNQUFQLENBQWMsYUFBYSxLQUFiLEdBQXFCLElBQW5DLENBQWpCO0FBQ0EsK0JBQWlCLGVBQWUsTUFBZixFQUFqQjtBQUNBLHdCQUFHLGVBQWUsTUFBZixHQUF3QixDQUEzQixFQUE4QjtBQUM1QiwrQkFBUyxRQUFULENBQWtCLE9BQWxCO0FBQ0Q7QUFDRixtQkFORDtBQU9ELGlCQVZELE1BV0ssSUFBRyxPQUFILEVBQVk7QUFDZix5QkFBTyxPQUFQLENBQWUsdUJBQWYsRUFBd0MsS0FBeEMsRUFBK0MsTUFBL0M7QUFDQSx5QkFBTyxNQUFQLENBQWMsYUFBYSxLQUFiLEdBQXFCLElBQW5DLEVBQ0csTUFESCxDQUNVLFNBQVMsVUFEbkIsRUFFSyxRQUZMLENBRWMsT0FGZDtBQUlELGlCQU5JLE1BT0EsSUFBRyxVQUFILEVBQWU7QUFDbEIseUJBQU8sT0FBUCxDQUFlLHdCQUFmLEVBQXlDLEtBQXpDLEVBQWdELFFBQWhEO0FBQ0Esc0JBQUcsVUFBVSxJQUFiLEVBQW1CO0FBQ2pCLDZCQUFTLFFBQVQsQ0FBa0IsT0FBbEI7QUFDRCxtQkFGRCxNQUdLO0FBQ0gsNkJBQVMsUUFBVCxDQUFrQixTQUFsQjtBQUNEO0FBQ0YsaUJBUkksTUFTQSxJQUFHLFVBQUgsRUFBZTtBQUNsQix5QkFBTyxPQUFQLENBQWUsd0JBQWYsRUFBeUMsS0FBekMsRUFBZ0QsUUFBaEQ7QUFDQSwyQkFBUyxRQUFULENBQWtCLGNBQWxCLEVBQWtDLEtBQWxDO0FBQ0QsaUJBSEksTUFJQTtBQUNILHlCQUFPLE9BQVAsQ0FBZSxxQkFBZixFQUFzQyxLQUF0QyxFQUE2QyxNQUE3QztBQUNBLHlCQUFPLEdBQVAsQ0FBVyxLQUFYO0FBQ0Q7QUFDRjtBQUNGLGFBaEREO0FBaUREO0FBdkZFLFNBM21CTzs7QUFxc0JaLGtCQUFVOztBQUVSLGdCQUFNLFVBQVMsS0FBVCxFQUFnQixlQUFoQixFQUFpQztBQUNyQyxnQkFDRSxTQUFTLE9BQU8sR0FBUCxDQUFXLE1BQVgsRUFEWDtBQUFBLGdCQUVFLFVBRkY7OztBQU1BLGdCQUFHLFdBQUgsRUFBZ0I7QUFDZCxxQkFBTyxLQUFQO0FBQ0Q7OztBQUdELHlCQUFhLEVBQWI7QUFDQSxnQkFBSSxPQUFPLEVBQVAsQ0FBVSxLQUFWLEVBQUosRUFBd0I7QUFDdEIscUJBQU8sS0FBUCxDQUFhLDJDQUFiO0FBQ0EscUJBQU8sR0FBUCxDQUFXLE9BQVg7QUFDQSxrQkFBRyxvQkFBb0IsSUFBdkIsRUFBNkI7QUFDM0IsdUJBQU8sU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLE9BQXhCLEVBQWlDLEtBQWpDLEVBQXdDLE1BQXhDLENBQVA7QUFDRDtBQUNGLGFBTkQsTUFPSztBQUNILHFCQUFPLEtBQVAsQ0FBYSxpQkFBYjtBQUNBLHFCQUFPLEdBQVAsQ0FBVyxLQUFYO0FBQ0Esa0JBQUcsQ0FBQyxTQUFTLE1BQWIsRUFBcUI7QUFDbkIsdUJBQU8sR0FBUCxDQUFXLE1BQVgsQ0FBa0IsVUFBbEI7QUFDRDs7QUFFRCxrQkFBRyxRQUFRLElBQVIsQ0FBYSxXQUFiLE1BQThCLFNBQWpDLEVBQTRDO0FBQzFDLHNCQUFNLHdCQUFOO0FBQ0Q7QUFDRCxrQkFBRyxvQkFBb0IsSUFBdkIsRUFBNkI7QUFDM0IsdUJBQU8sU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLE9BQXhCLEVBQWlDLFVBQWpDLEVBQTZDLE1BQTdDLENBQVA7QUFDRDtBQUNGO0FBQ0YsV0FwQ087OztBQXVDUixpQkFBTyxVQUFTLEtBQVQsRUFBZ0IsU0FBaEIsRUFBMkI7QUFDaEMsZ0JBQ0UsYUFBZ0IsTUFBTSxVQUFOLElBQW9CLFNBRHRDO0FBQUEsZ0JBRUUsU0FBZ0IsT0FBTyxHQUFQLENBQVcsS0FBWCxDQUFpQixVQUFqQixDQUZsQjtBQUFBLGdCQUdFLGdCQUFpQixNQUFNLE9BQVAsR0FDWixPQUFPLEdBQVAsQ0FBVyxLQUFYLENBQWlCLE1BQU0sT0FBdkIsQ0FEWSxHQUVaLEtBTE47QUFBQSxnQkFNRSxhQUFjLElBTmhCO0FBQUEsZ0JBT0UsY0FBYyxFQVBoQjtBQVNBLGdCQUFHLENBQUMsTUFBTSxVQUFWLEVBQXNCO0FBQ3BCLHFCQUFPLEtBQVAsQ0FBYSxnQ0FBYixFQUErQyxVQUEvQztBQUNBLG9CQUFNLFVBQU4sR0FBbUIsVUFBbkI7QUFDRDtBQUNELGdCQUFHLE9BQU8sSUFBUCxDQUFZLFVBQVosQ0FBSCxFQUE0QjtBQUMxQixxQkFBTyxLQUFQLENBQWEsNkJBQWIsRUFBNEMsVUFBNUM7QUFDQSwyQkFBYSxJQUFiO0FBQ0QsYUFIRCxNQUlLLElBQUcsTUFBTSxRQUFOLElBQWtCLE9BQU8sRUFBUCxDQUFVLEtBQVYsQ0FBZ0IsTUFBaEIsQ0FBckIsRUFBNkM7QUFDaEQscUJBQU8sS0FBUCxDQUFhLHVDQUFiLEVBQXNELFVBQXREO0FBQ0EsMkJBQWEsSUFBYjtBQUNELGFBSEksTUFJQSxJQUFHLE1BQU0sT0FBTixJQUFpQixPQUFPLEVBQVAsQ0FBVSxLQUFWLENBQWdCLGFBQWhCLENBQXBCLEVBQW9EO0FBQ3ZELHFCQUFPLEtBQVAsQ0FBYSx1RUFBYixFQUFzRixhQUF0RjtBQUNBLDJCQUFhLElBQWI7QUFDRCxhQUhJLE1BSUEsSUFBRyxNQUFNLEtBQU4sS0FBZ0IsU0FBbkIsRUFBOEI7QUFDakMsZ0JBQUUsSUFBRixDQUFPLE1BQU0sS0FBYixFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsb0JBQUksT0FBTyxHQUFQLENBQVcsS0FBWCxDQUFpQixVQUFqQixLQUFnQyxDQUFHLE9BQU8sUUFBUCxDQUFnQixJQUFoQixDQUFxQixLQUFyQixFQUE0QixJQUE1QixDQUF2QyxFQUE2RTtBQUMzRSx5QkFBTyxLQUFQLENBQWEsa0JBQWIsRUFBaUMsVUFBakMsRUFBNkMsS0FBSyxJQUFsRDtBQUNBLDhCQUFZLElBQVosQ0FBaUIsT0FBTyxHQUFQLENBQVcsTUFBWCxDQUFrQixJQUFsQixFQUF3QixLQUF4QixDQUFqQjtBQUNBLCtCQUFhLEtBQWI7QUFDRDtBQUNGLGVBTkQ7QUFPRDtBQUNELGdCQUFHLFVBQUgsRUFBZTtBQUNiLHFCQUFPLE1BQVAsQ0FBYyxNQUFkLENBQXFCLFVBQXJCLEVBQWlDLFdBQWpDO0FBQ0EsdUJBQVMsT0FBVCxDQUFpQixJQUFqQixDQUFzQixNQUF0QjtBQUNELGFBSEQsTUFJSztBQUNILDJCQUFhLFdBQVcsTUFBWCxDQUFrQixXQUFsQixDQUFiO0FBQ0EscUJBQU8sR0FBUCxDQUFXLE1BQVgsQ0FBa0IsVUFBbEIsRUFBOEIsV0FBOUI7QUFDQSx1QkFBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLE1BQXhCLEVBQWdDLFdBQWhDO0FBQ0EscUJBQU8sS0FBUDtBQUNEO0FBQ0QsbUJBQU8sSUFBUDtBQUNELFdBckZPOzs7QUF3RlIsZ0JBQU0sVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQzFCLGdCQUNFLFNBQWUsT0FBTyxHQUFQLENBQVcsS0FBWCxDQUFpQixNQUFNLFVBQXZCLENBRGpCO0FBQUEsZ0JBRUUsT0FBZSxLQUFLLElBRnRCO0FBQUEsZ0JBR0UsUUFBZSxPQUFPLEdBQVAsRUFIakI7QUFBQSxnQkFJRSxVQUFlLElBSmpCO0FBQUEsZ0JBS0UsWUFBZSxPQUFPLEdBQVAsQ0FBVyxjQUFYLENBQTBCLElBQTFCLENBTGpCO0FBQUEsZ0JBTUUsV0FBZSxPQUFPLEdBQVAsQ0FBVyxRQUFYLENBQW9CLElBQXBCLENBTmpCO0FBQUEsZ0JBT0UsZUFBZSxTQUFTLEtBQVQsQ0FBZSxRQUFmLENBUGpCO0FBU0EsZ0JBQUksQ0FBQyxFQUFFLFVBQUYsQ0FBYSxZQUFiLENBQUwsRUFBa0M7QUFDaEMscUJBQU8sS0FBUCxDQUFhLE1BQU0sTUFBbkIsRUFBMkIsUUFBM0I7QUFDQTtBQUNEOztBQUVELG9CQUFTLFVBQVUsU0FBVixJQUF1QixVQUFVLEVBQWpDLElBQXVDLFVBQVUsSUFBbEQsR0FDSixFQURJLEdBRUosRUFBRSxJQUFGLENBQU8sUUFBUSxFQUFmLENBRko7QUFJQSxtQkFBTyxhQUFhLElBQWIsQ0FBa0IsTUFBbEIsRUFBMEIsS0FBMUIsRUFBaUMsU0FBakMsQ0FBUDtBQUNEO0FBNUdPLFNBcnNCRTs7QUFvekJaLGlCQUFTLFVBQVMsSUFBVCxFQUFlLEtBQWYsRUFBc0I7QUFDN0IsY0FBSSxFQUFFLGFBQUYsQ0FBZ0IsSUFBaEIsQ0FBSixFQUE0QjtBQUMxQixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsUUFBZixFQUF5QixJQUF6QjtBQUNELFdBRkQsTUFHSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQixxQkFBUyxJQUFULElBQWlCLEtBQWpCO0FBQ0QsV0FGSSxNQUdBO0FBQ0gsbUJBQU8sU0FBUyxJQUFULENBQVA7QUFDRDtBQUNGLFNBOXpCVztBQSt6Qlosa0JBQVUsVUFBUyxJQUFULEVBQWUsS0FBZixFQUFzQjtBQUM5QixjQUFJLEVBQUUsYUFBRixDQUFnQixJQUFoQixDQUFKLEVBQTRCO0FBQzFCLGNBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxNQUFmLEVBQXVCLElBQXZCO0FBQ0QsV0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLG1CQUFPLElBQVAsSUFBZSxLQUFmO0FBQ0QsV0FGSSxNQUdBO0FBQ0gsbUJBQU8sT0FBTyxJQUFQLENBQVA7QUFDRDtBQUNGLFNBejBCVztBQTAwQlosZUFBTyxZQUFXO0FBQ2hCLGNBQUcsQ0FBQyxTQUFTLE1BQVYsSUFBb0IsU0FBUyxLQUFoQyxFQUF1QztBQUNyQyxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLEtBQVAsR0FBZSxTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxJQUFyQyxFQUEyQyxPQUEzQyxFQUFvRCxTQUFTLElBQVQsR0FBZ0IsR0FBcEUsQ0FBZjtBQUNBLHFCQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLE9BQW5CLEVBQTRCLFNBQTVCO0FBQ0Q7QUFDRjtBQUNGLFNBcDFCVztBQXExQlosaUJBQVMsWUFBVztBQUNsQixjQUFHLENBQUMsU0FBUyxNQUFWLElBQW9CLFNBQVMsT0FBN0IsSUFBd0MsU0FBUyxLQUFwRCxFQUEyRDtBQUN6RCxnQkFBRyxTQUFTLFdBQVosRUFBeUI7QUFDdkIscUJBQU8sV0FBUCxDQUFtQixHQUFuQixDQUF1QixTQUF2QjtBQUNELGFBRkQsTUFHSztBQUNILHFCQUFPLE9BQVAsR0FBaUIsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsSUFBckMsRUFBMkMsT0FBM0MsRUFBb0QsU0FBUyxJQUFULEdBQWdCLEdBQXBFLENBQWpCO0FBQ0EscUJBQU8sT0FBUCxDQUFlLEtBQWYsQ0FBcUIsT0FBckIsRUFBOEIsU0FBOUI7QUFDRDtBQUNGO0FBQ0YsU0EvMUJXO0FBZzJCWixlQUFPLFlBQVc7QUFDaEIsY0FBRyxDQUFDLFNBQVMsTUFBYixFQUFxQjtBQUNuQixtQkFBTyxLQUFQLEdBQWUsU0FBUyxTQUFULENBQW1CLElBQW5CLENBQXdCLElBQXhCLENBQTZCLFFBQVEsS0FBckMsRUFBNEMsT0FBNUMsRUFBcUQsU0FBUyxJQUFULEdBQWdCLEdBQXJFLENBQWY7QUFDQSxtQkFBTyxLQUFQLENBQWEsS0FBYixDQUFtQixPQUFuQixFQUE0QixTQUE1QjtBQUNEO0FBQ0YsU0FyMkJXO0FBczJCWixxQkFBYTtBQUNYLGVBQUssVUFBUyxPQUFULEVBQWtCO0FBQ3JCLGdCQUNFLFdBREYsRUFFRSxhQUZGLEVBR0UsWUFIRjtBQUtBLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2Qiw0QkFBZ0IsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUFoQjtBQUNBLDZCQUFnQixRQUFRLFdBQXhCO0FBQ0EsOEJBQWdCLGNBQWMsWUFBOUI7QUFDQSxxQkFBZ0IsV0FBaEI7QUFDQSwwQkFBWSxJQUFaLENBQWlCO0FBQ2Ysd0JBQW1CLFFBQVEsQ0FBUixDQURKO0FBRWYsNkJBQW1CLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxPQUFkLEVBQXVCLENBQXZCLEtBQTZCLEVBRmpDO0FBR2YsMkJBQW1CLE9BSEo7QUFJZixrQ0FBbUI7QUFKSixlQUFqQjtBQU1EO0FBQ0QseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsbUJBQU8sV0FBUCxDQUFtQixLQUFuQixHQUEyQixXQUFXLE9BQU8sV0FBUCxDQUFtQixPQUE5QixFQUF1QyxHQUF2QyxDQUEzQjtBQUNELFdBckJVO0FBc0JYLG1CQUFTLFlBQVc7QUFDbEIsZ0JBQ0UsUUFBUSxTQUFTLElBQVQsR0FBZ0IsR0FEMUI7QUFBQSxnQkFFRSxZQUFZLENBRmQ7QUFJQSxtQkFBTyxLQUFQO0FBQ0EseUJBQWEsT0FBTyxXQUFQLENBQW1CLEtBQWhDO0FBQ0EsY0FBRSxJQUFGLENBQU8sV0FBUCxFQUFvQixVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDeEMsMkJBQWEsS0FBSyxnQkFBTCxDQUFiO0FBQ0QsYUFGRDtBQUdBLHFCQUFTLE1BQU0sU0FBTixHQUFrQixJQUEzQjtBQUNBLGdCQUFHLGNBQUgsRUFBbUI7QUFDakIsdUJBQVMsUUFBUSxjQUFSLEdBQXlCLElBQWxDO0FBQ0Q7QUFDRCxnQkFBRyxZQUFZLE1BQVosR0FBcUIsQ0FBeEIsRUFBMkI7QUFDekIsdUJBQVMsTUFBTSxHQUFOLEdBQVksWUFBWSxNQUF4QixHQUFpQyxHQUExQztBQUNEO0FBQ0QsZ0JBQUksQ0FBQyxRQUFRLEtBQVIsS0FBa0IsU0FBbEIsSUFBK0IsUUFBUSxLQUFSLEtBQWtCLFNBQWxELEtBQWdFLFlBQVksTUFBWixHQUFxQixDQUF6RixFQUE0RjtBQUMxRixzQkFBUSxjQUFSLENBQXVCLEtBQXZCO0FBQ0Esa0JBQUcsUUFBUSxLQUFYLEVBQWtCO0FBQ2hCLHdCQUFRLEtBQVIsQ0FBYyxXQUFkO0FBQ0QsZUFGRCxNQUdLO0FBQ0gsa0JBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDBCQUFRLEdBQVIsQ0FBWSxLQUFLLE1BQUwsSUFBZSxJQUFmLEdBQXNCLEtBQUssZ0JBQUwsQ0FBdEIsR0FBNkMsSUFBekQ7QUFDRCxpQkFGRDtBQUdEO0FBQ0Qsc0JBQVEsUUFBUjtBQUNEO0FBQ0QsMEJBQWMsRUFBZDtBQUNEO0FBcERVLFNBdDJCRDtBQTQ1QlosZ0JBQVEsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDLE9BQWpDLEVBQTBDO0FBQ2hELGNBQ0UsU0FBUyxRQURYO0FBQUEsY0FFRSxRQUZGO0FBQUEsY0FHRSxLQUhGO0FBQUEsY0FJRSxRQUpGO0FBTUEsNEJBQWtCLG1CQUFtQixjQUFyQztBQUNBLG9CQUFrQixXQUFtQixPQUFyQztBQUNBLGNBQUcsT0FBTyxLQUFQLElBQWdCLFFBQWhCLElBQTRCLFdBQVcsU0FBMUMsRUFBcUQ7QUFDbkQsb0JBQVcsTUFBTSxLQUFOLENBQVksT0FBWixDQUFYO0FBQ0EsdUJBQVcsTUFBTSxNQUFOLEdBQWUsQ0FBMUI7QUFDQSxjQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ25DLGtCQUFJLGlCQUFrQixTQUFTLFFBQVYsR0FDakIsUUFBUSxNQUFNLFFBQVEsQ0FBZCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixFQUEyQixXQUEzQixFQUFSLEdBQW1ELE1BQU0sUUFBUSxDQUFkLEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBRGxDLEdBRWpCLEtBRko7QUFJQSxrQkFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxjQUFQLENBQWpCLEtBQThDLFNBQVMsUUFBM0QsRUFBdUU7QUFDckUseUJBQVMsT0FBTyxjQUFQLENBQVQ7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLGNBQVAsTUFBMkIsU0FBL0IsRUFBMkM7QUFDOUMsd0JBQVEsT0FBTyxjQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBLElBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sS0FBUCxDQUFqQixLQUFxQyxTQUFTLFFBQWxELEVBQThEO0FBQ2pFLHlCQUFTLE9BQU8sS0FBUCxDQUFUO0FBQ0QsZUFGSSxNQUdBLElBQUksT0FBTyxLQUFQLE1BQWtCLFNBQXRCLEVBQWtDO0FBQ3JDLHdCQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQTtBQUNILHVCQUFPLEtBQVA7QUFDRDtBQUNGLGFBdEJEO0FBdUJEO0FBQ0QsY0FBSSxFQUFFLFVBQUYsQ0FBYyxLQUFkLENBQUosRUFBNEI7QUFDMUIsdUJBQVcsTUFBTSxLQUFOLENBQVksT0FBWixFQUFxQixlQUFyQixDQUFYO0FBQ0QsV0FGRCxNQUdLLElBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQzNCLHVCQUFXLEtBQVg7QUFDRDtBQUNELGNBQUcsRUFBRSxPQUFGLENBQVUsYUFBVixDQUFILEVBQTZCO0FBQzNCLDBCQUFjLElBQWQsQ0FBbUIsUUFBbkI7QUFDRCxXQUZELE1BR0ssSUFBRyxrQkFBa0IsU0FBckIsRUFBZ0M7QUFDbkMsNEJBQWdCLENBQUMsYUFBRCxFQUFnQixRQUFoQixDQUFoQjtBQUNELFdBRkksTUFHQSxJQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDOUIsNEJBQWdCLFFBQWhCO0FBQ0Q7QUFDRCxpQkFBTyxLQUFQO0FBQ0Q7QUFoOUJXLE9BQWQ7QUFrOUJBLGFBQU8sVUFBUDtBQUNELEtBci9CSDs7QUF3L0JBLFdBQVEsa0JBQWtCLFNBQW5CLEdBQ0gsYUFERyxHQUVILElBRko7QUFJRCxHQTFnQ0Q7O0FBNGdDQSxJQUFFLEVBQUYsQ0FBSyxJQUFMLENBQVUsUUFBVixHQUFxQjs7QUFFbkIsVUFBb0IsTUFGRDtBQUduQixlQUFvQixNQUhEOztBQUtuQixXQUFvQixLQUxEO0FBTW5CLGFBQW9CLEtBTkQ7QUFPbkIsaUJBQW9CLElBUEQ7O0FBU25CLFlBQW9CLEtBVEQ7O0FBV25CLHVCQUFvQixJQVhEO0FBWW5CLFFBQW9CLFFBWkQ7QUFhbkIsWUFBb0IsS0FiRDs7QUFlbkIsV0FBb0IsR0FmRDtBQWdCbkIsZ0JBQW9CLElBaEJEOztBQWtCbkIsZ0JBQW9CLE9BbEJEO0FBbUJuQixjQUFvQixHQW5CRDs7QUFxQm5CLGFBQW9CLFlBQVcsQ0FBRSxDQXJCZDtBQXNCbkIsZUFBb0IsWUFBVyxDQUFFLENBdEJkO0FBdUJuQixlQUFvQixZQUFXO0FBQUUsYUFBTyxJQUFQO0FBQWMsS0F2QjVCO0FBd0JuQixlQUFvQixZQUFXO0FBQUUsYUFBTyxLQUFQO0FBQWUsS0F4QjdCOztBQTBCbkIsY0FBVztBQUNULG9CQUFlLFNBRE47QUFFVCxnQkFBZTtBQUZOLEtBMUJROztBQStCbkIsWUFBUTtBQUNOLGVBQVUsV0FESjtBQUVOLGVBQVUsYUFGSjtBQUdOLGFBQVUsbUdBSEo7QUFJTixjQUFVLHFDQUpKO0FBS04sYUFBVSxnQkFMSjtBQU1OLGVBQVUsVUFOSjtBQU9OLGNBQVUsa0JBUEo7QUFRTixXQUFVO0FBUkosS0EvQlc7O0FBMENuQixVQUFNO0FBQ0osdUJBQW1CLDRCQURmO0FBRUosd0JBQW1CO0FBRmYsS0ExQ2E7O0FBK0NuQixZQUFRO0FBQ04sYUFBdUIsMEJBRGpCO0FBRU4sZUFBdUIsd0JBRmpCO0FBR04sYUFBdUIsK0JBSGpCO0FBSU4sV0FBdUIsNEJBSmpCO0FBS04sY0FBdUIsbUNBTGpCO0FBTU4sZUFBdUIsMkJBTmpCO0FBT04sZUFBdUIsaUNBUGpCO0FBUU4sY0FBdUIsZ0NBUmpCO0FBU04sVUFBdUIsOEJBVGpCO0FBVU4saUJBQXVCLHNDQVZqQjtBQVdOLFdBQXVCLHVDQVhqQjtBQVlOLGtCQUF1QiwrQ0FaakI7QUFhTixlQUF1QixxQ0FiakI7QUFjTixzQkFBdUIsNkNBZGpCO0FBZU4scUJBQXVCLG9DQWZqQjtBQWdCTiw0QkFBdUIsMkNBaEJqQjtBQWlCTixpQkFBdUIsZ0RBakJqQjtBQWtCTixjQUF1QixnREFsQmpCO0FBbUJOLG1CQUF1QiwrQ0FuQmpCO0FBb0JOLGlCQUF1QixxREFwQmpCO0FBcUJOLGFBQXVCLHFDQXJCakI7QUFzQk4saUJBQXVCLDJEQXRCakI7QUF1Qk4sa0JBQXVCLDJDQXZCakI7QUF3Qk4sZ0JBQXVCLCtDQXhCakI7QUF5Qk4sa0JBQXVCLDhDQXpCakI7QUEwQk4sZ0JBQXVCO0FBMUJqQixLQS9DVzs7QUE0RW5CLGNBQVc7QUFDVCxnQkFBYSw2Q0FESjtBQUVULGFBQWEsUUFGSjtBQUdULGFBQWEseUJBSEo7QUFJVCxhQUFhLFFBSko7QUFLVCxhQUFhLE9BTEo7QUFNVCxlQUFhLGdCQU5KO0FBT1QsY0FBYSxlQVBKO0FBUVQsYUFBYSxxQkFSSjtBQVNULGFBQWEsNEJBVEo7QUFVVCxjQUFhLDhCQVZKO0FBV1Qsa0JBQWEsY0FYSjtBQVlULGtCQUFhO0FBWkosS0E1RVE7O0FBMkZuQixlQUFZO0FBQ1YsYUFBVSxPQURBO0FBRVYsYUFBVSxpQkFGQTtBQUdWLGVBQVUsTUFIQTtBQUlWLGVBQVU7QUFKQSxLQTNGTzs7QUFrR25CLFdBQU87QUFDTCxrQkFBYSxxREFEUjtBQUVMLGNBQWEsdUNBRlI7QUFHTCxjQUFhLGlEQUhSO0FBSUwsaUJBQWE7QUFKUixLQWxHWTs7QUF5R25CLGVBQVc7OztBQUdULGFBQU8sVUFBUyxNQUFULEVBQWlCO0FBQ3RCLFlBQ0UsT0FBTyxtQkFEVDtBQUdBLFVBQUUsSUFBRixDQUFPLE1BQVAsRUFBZSxVQUFTLEtBQVQsRUFBZ0IsS0FBaEIsRUFBdUI7QUFDcEMsa0JBQVEsU0FBUyxLQUFULEdBQWlCLE9BQXpCO0FBQ0QsU0FGRDtBQUdBLGdCQUFRLE9BQVI7QUFDQSxlQUFPLEVBQUUsSUFBRixDQUFQO0FBQ0QsT0FaUTs7O0FBZVQsY0FBUSxVQUFTLE1BQVQsRUFBaUI7QUFDdkIsZUFBTyxFQUFFLFFBQUYsRUFDSixRQURJLENBQ0ssb0NBREwsRUFFSixJQUZJLENBRUMsT0FBTyxDQUFQLENBRkQsQ0FBUDtBQUlEO0FBcEJRLEtBekdROztBQWdJbkIsV0FBTzs7O0FBR0wsYUFBTyxVQUFTLEtBQVQsRUFBZ0I7QUFDckIsZUFBTyxFQUFFLFVBQVUsU0FBVixJQUF1QixPQUFPLEtBQTlCLElBQXVDLEVBQUUsT0FBRixDQUFVLEtBQVYsS0FBb0IsTUFBTSxNQUFOLEtBQWlCLENBQTlFLENBQVA7QUFDRCxPQUxJOzs7QUFRTCxlQUFTLFlBQVc7QUFDbEIsZUFBUSxFQUFFLElBQUYsRUFBUSxNQUFSLENBQWUsVUFBZixFQUEyQixNQUEzQixHQUFvQyxDQUE1QztBQUNELE9BVkk7OztBQWFMLGFBQU8sVUFBUyxLQUFULEVBQWU7QUFDcEIsZUFBTyxFQUFFLEVBQUYsQ0FBSyxJQUFMLENBQVUsUUFBVixDQUFtQixNQUFuQixDQUEwQixLQUExQixDQUFnQyxJQUFoQyxDQUFxQyxLQUFyQyxDQUFQO0FBQ0QsT0FmSTs7O0FBa0JMLFdBQUssVUFBUyxLQUFULEVBQWdCO0FBQ25CLGVBQU8sRUFBRSxFQUFGLENBQUssSUFBTCxDQUFVLFFBQVYsQ0FBbUIsTUFBbkIsQ0FBMEIsR0FBMUIsQ0FBOEIsSUFBOUIsQ0FBbUMsS0FBbkMsQ0FBUDtBQUNELE9BcEJJOzs7QUF1QkwsY0FBUSxVQUFTLEtBQVQsRUFBZ0IsTUFBaEIsRUFBd0I7QUFDOUIsWUFDRSxjQUFjLE9BQU8sS0FBUCxDQUFhLEVBQUUsRUFBRixDQUFLLElBQUwsQ0FBVSxRQUFWLENBQW1CLE1BQW5CLENBQTBCLEtBQXZDLENBRGhCO0FBQUEsWUFFRSxLQUZGOztBQUtBLFlBQUcsV0FBSCxFQUFnQjtBQUNkLG1CQUFVLFlBQVksTUFBWixJQUFzQixDQUF2QixHQUNMLFlBQVksQ0FBWixDQURLLEdBRUwsTUFGSjtBQUlBLGtCQUFTLFlBQVksTUFBWixJQUFzQixDQUF2QixHQUNKLFlBQVksQ0FBWixDQURJLEdBRUosRUFGSjtBQUlEO0FBQ0QsZUFBTyxNQUFNLEtBQU4sQ0FBYSxJQUFJLE1BQUosQ0FBVyxNQUFYLEVBQW1CLEtBQW5CLENBQWIsQ0FBUDtBQUNELE9BeENJOzs7QUEyQ0wsZUFBUyxVQUFTLEtBQVQsRUFBZ0IsS0FBaEIsRUFBdUI7QUFDOUIsWUFDRSxZQUFZLEVBQUUsRUFBRixDQUFLLElBQUwsQ0FBVSxRQUFWLENBQW1CLE1BQW5CLENBQTBCLE9BRHhDO0FBQUEsWUFFRSxHQUZGO0FBQUEsWUFHRSxHQUhGO0FBQUEsWUFJRSxLQUpGO0FBTUEsWUFBSSxDQUFDLEtBQUQsSUFBVSxDQUFDLEVBQUQsRUFBSyxJQUFMLEVBQVcsT0FBWCxDQUFtQixLQUFuQixNQUE4QixDQUFDLENBQTdDLEVBQWdEOztBQUUvQyxTQUZELE1BR0ssSUFBRyxNQUFNLE9BQU4sQ0FBYyxJQUFkLEtBQXVCLENBQUMsQ0FBM0IsRUFBOEI7QUFDakMsY0FBRyxVQUFVLElBQVYsQ0FBZSxLQUFmLENBQUgsRUFBMEI7QUFDeEIsa0JBQU0sTUFBTSxRQUFRLENBQXBCO0FBQ0Q7QUFDRixTQUpJLE1BS0E7QUFDSCxrQkFBUSxNQUFNLEtBQU4sQ0FBWSxJQUFaLEVBQWtCLENBQWxCLENBQVI7QUFDQSxjQUFHLFVBQVUsSUFBVixDQUFlLE1BQU0sQ0FBTixDQUFmLENBQUgsRUFBNkI7QUFDM0Isa0JBQU0sTUFBTSxDQUFOLElBQVcsQ0FBakI7QUFDRDtBQUNELGNBQUcsVUFBVSxJQUFWLENBQWUsTUFBTSxDQUFOLENBQWYsQ0FBSCxFQUE2QjtBQUMzQixrQkFBTSxNQUFNLENBQU4sSUFBVyxDQUFqQjtBQUNEO0FBQ0Y7QUFDRCxlQUNFLFVBQVUsSUFBVixDQUFlLEtBQWYsTUFDQyxRQUFRLFNBQVIsSUFBcUIsU0FBUyxHQUQvQixNQUVDLFFBQVEsU0FBUixJQUFxQixTQUFTLEdBRi9CLENBREY7QUFLRCxPQXhFSTs7O0FBMkVMLGVBQVMsVUFBUyxLQUFULEVBQWdCO0FBQ3ZCLGVBQU8sRUFBRSxFQUFGLENBQUssSUFBTCxDQUFVLFFBQVYsQ0FBbUIsTUFBbkIsQ0FBMEIsT0FBMUIsQ0FBa0MsSUFBbEMsQ0FBdUMsS0FBdkMsQ0FBUDtBQUNELE9BN0VJOzs7QUFnRkwsY0FBUSxVQUFTLEtBQVQsRUFBZ0I7QUFDdEIsZUFBTyxFQUFFLEVBQUYsQ0FBSyxJQUFMLENBQVUsUUFBVixDQUFtQixNQUFuQixDQUEwQixNQUExQixDQUFpQyxJQUFqQyxDQUFzQyxLQUF0QyxDQUFQO0FBQ0QsT0FsRkk7OztBQXFGTCxVQUFJLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjtBQUN4QixlQUFRLE9BQU8sSUFBUCxJQUFlLFFBQWhCLEdBQ0gsS0FBSyxXQUFMLEVBREcsR0FFSCxJQUZKO0FBSUEsZ0JBQVMsT0FBTyxLQUFQLElBQWdCLFFBQWpCLEdBQ0osTUFBTSxXQUFOLEVBREksR0FFSixLQUZKO0FBSUEsZUFBUSxTQUFTLElBQWpCO0FBQ0QsT0EvRkk7OztBQWtHTCxpQkFBVyxVQUFTLEtBQVQsRUFBZ0IsSUFBaEIsRUFBc0I7QUFDL0IsZUFBUSxTQUFTLElBQWpCO0FBQ0QsT0FwR0k7OztBQXVHTCxXQUFLLFVBQVMsS0FBVCxFQUFnQixRQUFoQixFQUEwQjtBQUM3QixnQkFBUyxPQUFPLEtBQVAsSUFBZ0IsUUFBakIsR0FDSixNQUFNLFdBQU4sRUFESSxHQUVKLEtBRko7QUFJQSxtQkFBWSxPQUFPLFFBQVAsSUFBbUIsUUFBcEIsR0FDUCxTQUFTLFdBQVQsRUFETyxHQUVQLFFBRko7QUFJQSxlQUFRLFNBQVMsUUFBakI7QUFDRCxPQWpISTs7O0FBb0hMLGtCQUFZLFVBQVMsS0FBVCxFQUFnQixRQUFoQixFQUEwQjtBQUNwQyxlQUFRLFNBQVMsUUFBakI7QUFDRCxPQXRISTs7O0FBeUhMLGdCQUFVLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjs7QUFFOUIsZUFBTyxLQUFLLE9BQUwsQ0FBYSxFQUFFLEVBQUYsQ0FBSyxJQUFMLENBQVUsUUFBVixDQUFtQixNQUFuQixDQUEwQixNQUF2QyxFQUErQyxNQUEvQyxDQUFQO0FBQ0EsZUFBUSxNQUFNLE1BQU4sQ0FBYyxJQUFJLE1BQUosQ0FBVyxJQUFYLEVBQWlCLEdBQWpCLENBQWQsTUFBMEMsQ0FBQyxDQUFuRDtBQUNELE9BN0hJOzs7QUFnSUwsdUJBQWlCLFVBQVMsS0FBVCxFQUFnQixJQUFoQixFQUFzQjs7QUFFckMsZUFBTyxLQUFLLE9BQUwsQ0FBYSxFQUFFLEVBQUYsQ0FBSyxJQUFMLENBQVUsUUFBVixDQUFtQixNQUFuQixDQUEwQixNQUF2QyxFQUErQyxNQUEvQyxDQUFQO0FBQ0EsZUFBUSxNQUFNLE1BQU4sQ0FBYyxJQUFJLE1BQUosQ0FBVyxJQUFYLENBQWQsTUFBcUMsQ0FBQyxDQUE5QztBQUNELE9BcElJOzs7QUF1SUwscUJBQWUsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCOztBQUVuQyxlQUFPLEtBQUssT0FBTCxDQUFhLEVBQUUsRUFBRixDQUFLLElBQUwsQ0FBVSxRQUFWLENBQW1CLE1BQW5CLENBQTBCLE1BQXZDLEVBQStDLE1BQS9DLENBQVA7QUFDQSxlQUFRLE1BQU0sTUFBTixDQUFjLElBQUksTUFBSixDQUFXLElBQVgsRUFBaUIsR0FBakIsQ0FBZCxNQUEwQyxDQUFDLENBQW5EO0FBQ0QsT0EzSUk7OztBQThJTCw0QkFBc0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCOztBQUUxQyxlQUFPLEtBQUssT0FBTCxDQUFhLEVBQUUsRUFBRixDQUFLLElBQUwsQ0FBVSxRQUFWLENBQW1CLE1BQW5CLENBQTBCLE1BQXZDLEVBQStDLE1BQS9DLENBQVA7QUFDQSxlQUFRLE1BQU0sTUFBTixDQUFjLElBQUksTUFBSixDQUFXLElBQVgsQ0FBZCxNQUFxQyxDQUFDLENBQTlDO0FBQ0QsT0FsSkk7OztBQXFKTCxpQkFBVyxVQUFTLEtBQVQsRUFBZ0IsY0FBaEIsRUFBZ0M7QUFDekMsZUFBUSxVQUFVLFNBQVgsR0FDRixNQUFNLE1BQU4sSUFBZ0IsY0FEZCxHQUVILEtBRko7QUFJRCxPQTFKSTs7O0FBNkpMLGNBQVEsVUFBUyxLQUFULEVBQWdCLGNBQWhCLEVBQWdDO0FBQ3RDLGVBQVEsVUFBVSxTQUFYLEdBQ0YsTUFBTSxNQUFOLElBQWdCLGNBRGQsR0FFSCxLQUZKO0FBSUQsT0FsS0k7OztBQXFLTCxtQkFBYSxVQUFTLEtBQVQsRUFBZ0IsY0FBaEIsRUFBZ0M7QUFDM0MsZUFBUSxVQUFVLFNBQVgsR0FDRixNQUFNLE1BQU4sSUFBZ0IsY0FEZCxHQUVILEtBRko7QUFJRCxPQTFLSTs7O0FBNktMLGlCQUFXLFVBQVMsS0FBVCxFQUFnQixTQUFoQixFQUEyQjtBQUNwQyxlQUFRLFVBQVUsU0FBWCxHQUNGLE1BQU0sTUFBTixJQUFnQixTQURkLEdBRUgsS0FGSjtBQUlELE9BbExJOzs7QUFxTEwsYUFBTyxVQUFTLEtBQVQsRUFBZ0IsVUFBaEIsRUFBNEI7QUFDakMsWUFDRSxRQUFRLEVBQUUsSUFBRixDQURWO0FBQUEsWUFFRSxhQUZGO0FBSUEsWUFBSSxFQUFFLHFCQUFvQixVQUFwQixHQUFnQyxJQUFsQyxFQUF3QyxNQUF4QyxHQUFpRCxDQUFyRCxFQUF5RDtBQUN2RCwwQkFBZ0IsRUFBRSxxQkFBb0IsVUFBcEIsR0FBZ0MsSUFBbEMsRUFBd0MsR0FBeEMsRUFBaEI7QUFDRCxTQUZELE1BR0ssSUFBRyxFQUFFLE1BQU0sVUFBUixFQUFvQixNQUFwQixHQUE2QixDQUFoQyxFQUFtQztBQUN0QywwQkFBZ0IsRUFBRSxNQUFNLFVBQVIsRUFBb0IsR0FBcEIsRUFBaEI7QUFDRCxTQUZJLE1BR0EsSUFBRyxFQUFFLFlBQVksVUFBWixHQUF3QixJQUExQixFQUFnQyxNQUFoQyxHQUF5QyxDQUE1QyxFQUErQztBQUNsRCwwQkFBZ0IsRUFBRSxZQUFZLFVBQVosR0FBeUIsSUFBM0IsRUFBaUMsR0FBakMsRUFBaEI7QUFDRCxTQUZJLE1BR0EsSUFBSSxFQUFFLFlBQVksVUFBWixHQUF3QixNQUExQixFQUFrQyxNQUFsQyxHQUEyQyxDQUEvQyxFQUFtRDtBQUN0RCwwQkFBZ0IsRUFBRSxZQUFZLFVBQVosR0FBd0IsTUFBMUIsQ0FBaEI7QUFDRDtBQUNELGVBQVEsa0JBQWtCLFNBQW5CLEdBQ0QsTUFBTSxRQUFOLE1BQW9CLGNBQWMsUUFBZCxFQURuQixHQUVILEtBRko7QUFJRCxPQTFNSTs7O0FBNk1MLGlCQUFXLFVBQVMsS0FBVCxFQUFnQixVQUFoQixFQUE0Qjs7QUFFckMsWUFDRSxRQUFRLEVBQUUsSUFBRixDQURWO0FBQUEsWUFFRSxhQUZGO0FBSUEsWUFBSSxFQUFFLHFCQUFvQixVQUFwQixHQUFnQyxJQUFsQyxFQUF3QyxNQUF4QyxHQUFpRCxDQUFyRCxFQUF5RDtBQUN2RCwwQkFBZ0IsRUFBRSxxQkFBb0IsVUFBcEIsR0FBZ0MsSUFBbEMsRUFBd0MsR0FBeEMsRUFBaEI7QUFDRCxTQUZELE1BR0ssSUFBRyxFQUFFLE1BQU0sVUFBUixFQUFvQixNQUFwQixHQUE2QixDQUFoQyxFQUFtQztBQUN0QywwQkFBZ0IsRUFBRSxNQUFNLFVBQVIsRUFBb0IsR0FBcEIsRUFBaEI7QUFDRCxTQUZJLE1BR0EsSUFBRyxFQUFFLFlBQVksVUFBWixHQUF3QixJQUExQixFQUFnQyxNQUFoQyxHQUF5QyxDQUE1QyxFQUErQztBQUNsRCwwQkFBZ0IsRUFBRSxZQUFZLFVBQVosR0FBeUIsSUFBM0IsRUFBaUMsR0FBakMsRUFBaEI7QUFDRCxTQUZJLE1BR0EsSUFBSSxFQUFFLFlBQVksVUFBWixHQUF3QixNQUExQixFQUFrQyxNQUFsQyxHQUEyQyxDQUEvQyxFQUFtRDtBQUN0RCwwQkFBZ0IsRUFBRSxZQUFZLFVBQVosR0FBd0IsTUFBMUIsQ0FBaEI7QUFDRDtBQUNELGVBQVEsa0JBQWtCLFNBQW5CLEdBQ0QsTUFBTSxRQUFOLE9BQXFCLGNBQWMsUUFBZCxFQURwQixHQUVILEtBRko7QUFJRCxPQW5PSTs7QUFxT0wsa0JBQVksVUFBUyxVQUFULEVBQXFCLFNBQXJCLEVBQWdDO0FBQzFDLFlBQ0UsUUFBUTtBQUNOLGdCQUFNO0FBQ0oscUJBQVUsSUFETjtBQUVKLG9CQUFVLENBQUMsRUFBRDtBQUZOLFdBREE7QUFLTixnQkFBTTtBQUNKLHFCQUFVLFFBRE47QUFFSixvQkFBVSxDQUFDLEVBQUQ7QUFGTixXQUxBO0FBU04sc0JBQVk7QUFDVixxQkFBVSxTQURBO0FBRVYsb0JBQVUsQ0FBQyxFQUFEO0FBRkEsV0FUTjtBQWFOLG9CQUFVO0FBQ1IscUJBQVUsK0VBREY7QUFFUixvQkFBVSxDQUFDLEVBQUQ7QUFGRixXQWJKO0FBaUJOLG9CQUFVO0FBQ1IscUJBQVUsVUFERjtBQUVSLG9CQUFVLENBQUMsRUFBRCxFQUFLLEVBQUwsRUFBUyxFQUFULEVBQWEsRUFBYjtBQUZGLFdBakJKO0FBcUJOLGVBQUs7QUFDSCxxQkFBVSx1QkFEUDtBQUVILG9CQUFVLENBQUMsRUFBRDtBQUZQLFdBckJDO0FBeUJOLG1CQUFTO0FBQ1AscUJBQVUsc0NBREg7QUFFUCxvQkFBVSxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVMsRUFBVCxFQUFhLEVBQWIsRUFBaUIsRUFBakIsRUFBcUIsRUFBckIsRUFBeUIsRUFBekIsRUFBNkIsRUFBN0I7QUFGSCxXQXpCSDtBQTZCTixzQkFBWTtBQUNWLHFCQUFVLGdCQURBO0FBRVYsb0JBQVUsQ0FBQyxFQUFEO0FBRkEsV0E3Qk47QUFpQ04saUJBQU87QUFDTCxxQkFBVSxzQkFETDtBQUVMLG9CQUFVLENBQUMsRUFBRCxFQUFLLEVBQUwsRUFBUyxFQUFULEVBQWEsRUFBYjtBQUZMLFdBakNEO0FBcUNOLHdCQUFjO0FBQ1oscUJBQVUsbUNBREU7QUFFWixvQkFBVSxDQUFDLEVBQUQ7QUFGRTtBQXJDUixTQURWO0FBQUEsWUEyQ0UsUUFBZ0IsRUEzQ2xCO0FBQUEsWUE0Q0UsWUFBZ0IsS0E1Q2xCO0FBQUEsWUE2Q0UsZ0JBQWlCLE9BQU8sU0FBUCxJQUFvQixRQUFyQixHQUNaLFVBQVUsS0FBVixDQUFnQixHQUFoQixDQURZLEdBRVosS0EvQ047QUFBQSxZQWdERSxRQWhERjtBQUFBLFlBaURFLFVBakRGOztBQW9EQSxZQUFHLE9BQU8sVUFBUCxLQUFzQixRQUF0QixJQUFrQyxXQUFXLE1BQVgsS0FBc0IsQ0FBM0QsRUFBOEQ7QUFDNUQ7QUFDRDs7O0FBR0QsWUFBRyxhQUFILEVBQWtCO0FBQ2hCLFlBQUUsSUFBRixDQUFPLGFBQVAsRUFBc0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXFCOztBQUV6Qyx5QkFBYSxNQUFNLElBQU4sQ0FBYjtBQUNBLGdCQUFHLFVBQUgsRUFBZTtBQUNiLHNCQUFRO0FBQ04sd0JBQVcsRUFBRSxPQUFGLENBQVUsV0FBVyxNQUFyQixFQUE2QixXQUFXLE1BQXhDLE1BQW9ELENBQUMsQ0FEMUQ7QUFFTix5QkFBVyxXQUFXLE1BQVgsQ0FBa0IsV0FBVyxPQUE3QixNQUEwQyxDQUFDO0FBRmhELGVBQVI7QUFJQSxrQkFBRyxNQUFNLE1BQU4sSUFBZ0IsTUFBTSxPQUF6QixFQUFrQztBQUNoQyw0QkFBWSxJQUFaO0FBQ0Q7QUFDRjtBQUNGLFdBWkQ7O0FBY0EsY0FBRyxDQUFDLFNBQUosRUFBZTtBQUNiLG1CQUFPLEtBQVA7QUFDRDtBQUNGOzs7QUFHRCxtQkFBVztBQUNULGtCQUFXLEVBQUUsT0FBRixDQUFVLFdBQVcsTUFBckIsRUFBNkIsTUFBTSxRQUFOLENBQWUsTUFBNUMsTUFBd0QsQ0FBQyxDQUQzRDtBQUVULG1CQUFXLFdBQVcsTUFBWCxDQUFrQixNQUFNLFFBQU4sQ0FBZSxPQUFqQyxNQUE4QyxDQUFDO0FBRmpELFNBQVg7QUFJQSxZQUFHLFNBQVMsTUFBVCxJQUFtQixTQUFTLE9BQS9CLEVBQXdDO0FBQ3RDLGlCQUFPLElBQVA7QUFDRDs7O0FBR0QsWUFDRSxTQUFnQixXQUFXLE1BRDdCO0FBQUEsWUFFRSxXQUFnQixDQUZsQjtBQUFBLFlBR0UsZ0JBQWdCLENBQ2QsQ0FBQyxDQUFELEVBQUksQ0FBSixFQUFPLENBQVAsRUFBVSxDQUFWLEVBQWEsQ0FBYixFQUFnQixDQUFoQixFQUFtQixDQUFuQixFQUFzQixDQUF0QixFQUF5QixDQUF6QixFQUE0QixDQUE1QixDQURjLEVBRWQsQ0FBQyxDQUFELEVBQUksQ0FBSixFQUFPLENBQVAsRUFBVSxDQUFWLEVBQWEsQ0FBYixFQUFnQixDQUFoQixFQUFtQixDQUFuQixFQUFzQixDQUF0QixFQUF5QixDQUF6QixFQUE0QixDQUE1QixDQUZjLENBSGxCO0FBQUEsWUFPRSxNQUFnQixDQVBsQjtBQVNBLGVBQU8sUUFBUCxFQUFpQjtBQUNmLGlCQUFPLGNBQWMsUUFBZCxFQUF3QixTQUFTLFdBQVcsTUFBWCxDQUFrQixNQUFsQixDQUFULEVBQW9DLEVBQXBDLENBQXhCLENBQVA7QUFDQSxzQkFBWSxDQUFaO0FBQ0Q7QUFDRCxlQUFRLE1BQU0sRUFBTixLQUFhLENBQWIsSUFBa0IsTUFBTSxDQUFoQztBQUNELE9BM1VJOztBQTZVTCxnQkFBVSxVQUFTLEtBQVQsRUFBZ0IsUUFBaEIsRUFBMEI7QUFDbEMsWUFBRyxZQUFZLENBQWYsRUFBa0I7QUFDaEIsaUJBQU8sSUFBUDtBQUNEO0FBQ0QsWUFBRyxZQUFZLENBQWYsRUFBa0I7QUFDaEIsaUJBQVEsVUFBVSxFQUFsQjtBQUNEO0FBQ0QsZUFBUSxNQUFNLEtBQU4sQ0FBWSxHQUFaLEVBQWlCLE1BQWpCLElBQTJCLFFBQW5DO0FBQ0QsT0FyVkk7O0FBdVZMLGtCQUFZLFVBQVMsS0FBVCxFQUFnQixVQUFoQixFQUE0QjtBQUN0QyxZQUFHLGNBQWMsQ0FBakIsRUFBb0I7QUFDbEIsaUJBQVEsVUFBVSxFQUFsQjtBQUNEO0FBQ0QsWUFBRyxjQUFjLENBQWpCLEVBQW9CO0FBQ2xCLGlCQUFRLFVBQVUsRUFBVixJQUFnQixNQUFNLE1BQU4sQ0FBYSxHQUFiLE1BQXNCLENBQUMsQ0FBL0M7QUFDRDtBQUNELGVBQVEsTUFBTSxLQUFOLENBQVksR0FBWixFQUFpQixNQUFqQixJQUEyQixVQUFuQztBQUNELE9BL1ZJOztBQWlXTCxnQkFBVSxVQUFTLEtBQVQsRUFBZ0IsUUFBaEIsRUFBMEI7QUFDbEMsWUFBRyxZQUFZLENBQWYsRUFBa0I7QUFDaEIsaUJBQU8sS0FBUDtBQUNEO0FBQ0QsWUFBRyxZQUFZLENBQWYsRUFBa0I7QUFDaEIsaUJBQVEsTUFBTSxNQUFOLENBQWEsR0FBYixNQUFzQixDQUFDLENBQS9CO0FBQ0Q7QUFDRCxlQUFRLE1BQU0sS0FBTixDQUFZLEdBQVosRUFBaUIsTUFBakIsSUFBMkIsUUFBbkM7QUFDRDtBQXpXSTs7QUFoSVksR0FBckI7QUE4ZUMsQ0FyZ0RBLEVBcWdERyxNQXJnREgsRUFxZ0RXLE1BcmdEWCxFQXFnRG1CLFFBcmdEbkIiLCJmaWxlIjoiZm9ybS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxuICogIyBTZW1hbnRpYyBVSSAtIEZvcm0gVmFsaWRhdGlvblxuICogaHR0cDovL2dpdGh1Yi5jb20vc2VtYW50aWMtb3JnL3NlbWFudGljLXVpL1xuICpcbiAqXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2VcbiAqIGh0dHA6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9NSVRcbiAqXG4gKi9cblxuOyhmdW5jdGlvbiAoJCwgd2luZG93LCBkb2N1bWVudCwgdW5kZWZpbmVkKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG53aW5kb3cgPSAodHlwZW9mIHdpbmRvdyAhPSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuTWF0aCA9PSBNYXRoKVxuICA/IHdpbmRvd1xuICA6ICh0eXBlb2Ygc2VsZiAhPSAndW5kZWZpbmVkJyAmJiBzZWxmLk1hdGggPT0gTWF0aClcbiAgICA/IHNlbGZcbiAgICA6IEZ1bmN0aW9uKCdyZXR1cm4gdGhpcycpKClcbjtcblxuJC5mbi5mb3JtID0gZnVuY3Rpb24ocGFyYW1ldGVycykge1xuICB2YXJcbiAgICAkYWxsTW9kdWxlcyAgICAgID0gJCh0aGlzKSxcbiAgICBtb2R1bGVTZWxlY3RvciAgID0gJGFsbE1vZHVsZXMuc2VsZWN0b3IgfHwgJycsXG5cbiAgICB0aW1lICAgICAgICAgICAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCksXG4gICAgcGVyZm9ybWFuY2UgICAgICA9IFtdLFxuXG4gICAgcXVlcnkgICAgICAgICAgICA9IGFyZ3VtZW50c1swXSxcbiAgICBsZWdhY3lQYXJhbWV0ZXJzID0gYXJndW1lbnRzWzFdLFxuICAgIG1ldGhvZEludm9rZWQgICAgPSAodHlwZW9mIHF1ZXJ5ID09ICdzdHJpbmcnKSxcbiAgICBxdWVyeUFyZ3VtZW50cyAgID0gW10uc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpLFxuICAgIHJldHVybmVkVmFsdWVcbiAgO1xuICAkYWxsTW9kdWxlc1xuICAgIC5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgdmFyXG4gICAgICAgICRtb2R1bGUgICAgID0gJCh0aGlzKSxcbiAgICAgICAgZWxlbWVudCAgICAgPSB0aGlzLFxuXG4gICAgICAgIGZvcm1FcnJvcnMgID0gW10sXG4gICAgICAgIGtleUhlbGREb3duID0gZmFsc2UsXG5cbiAgICAgICAgLy8gc2V0IGF0IHJ1bi10aW1lXG4gICAgICAgICRmaWVsZCxcbiAgICAgICAgJGdyb3VwLFxuICAgICAgICAkbWVzc2FnZSxcbiAgICAgICAgJHByb21wdCxcbiAgICAgICAgJHN1Ym1pdCxcbiAgICAgICAgJGNsZWFyLFxuICAgICAgICAkcmVzZXQsXG5cbiAgICAgICAgc2V0dGluZ3MsXG4gICAgICAgIHZhbGlkYXRpb24sXG5cbiAgICAgICAgbWV0YWRhdGEsXG4gICAgICAgIHNlbGVjdG9yLFxuICAgICAgICBjbGFzc05hbWUsXG4gICAgICAgIGVycm9yLFxuXG4gICAgICAgIG5hbWVzcGFjZSxcbiAgICAgICAgbW9kdWxlTmFtZXNwYWNlLFxuICAgICAgICBldmVudE5hbWVzcGFjZSxcblxuICAgICAgICBpbnN0YW5jZSxcbiAgICAgICAgbW9kdWxlXG4gICAgICA7XG5cbiAgICAgIG1vZHVsZSAgICAgID0ge1xuXG4gICAgICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICAgLy8gc2V0dGluZ3MgZ3JhYmJlZCBhdCBydW4gdGltZVxuICAgICAgICAgIG1vZHVsZS5nZXQuc2V0dGluZ3MoKTtcbiAgICAgICAgICBpZihtZXRob2RJbnZva2VkKSB7XG4gICAgICAgICAgICBpZihpbnN0YW5jZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5pbnN0YW50aWF0ZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLmludm9rZShxdWVyeSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgaWYoaW5zdGFuY2UgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICBpbnN0YW5jZS5pbnZva2UoJ2Rlc3Ryb3knKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdJbml0aWFsaXppbmcgZm9ybSB2YWxpZGF0aW9uJywgJG1vZHVsZSwgc2V0dGluZ3MpO1xuICAgICAgICAgICAgbW9kdWxlLmJpbmRFdmVudHMoKTtcbiAgICAgICAgICAgIG1vZHVsZS5zZXQuZGVmYXVsdHMoKTtcbiAgICAgICAgICAgIG1vZHVsZS5pbnN0YW50aWF0ZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBpbnN0YW50aWF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1N0b3JpbmcgaW5zdGFuY2Ugb2YgbW9kdWxlJywgbW9kdWxlKTtcbiAgICAgICAgICBpbnN0YW5jZSA9IG1vZHVsZTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAuZGF0YShtb2R1bGVOYW1lc3BhY2UsIG1vZHVsZSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVzdHJveTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0Rlc3Ryb3lpbmcgcHJldmlvdXMgbW9kdWxlJywgaW5zdGFuY2UpO1xuICAgICAgICAgIG1vZHVsZS5yZW1vdmVFdmVudHMoKTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAucmVtb3ZlRGF0YShtb2R1bGVOYW1lc3BhY2UpXG4gICAgICAgICAgO1xuICAgICAgICB9LFxuXG4gICAgICAgIHJlZnJlc2g6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZWZyZXNoaW5nIHNlbGVjdG9yIGNhY2hlJyk7XG4gICAgICAgICAgJGZpZWxkICAgICAgPSAkbW9kdWxlLmZpbmQoc2VsZWN0b3IuZmllbGQpO1xuICAgICAgICAgICRncm91cCAgICAgID0gJG1vZHVsZS5maW5kKHNlbGVjdG9yLmdyb3VwKTtcbiAgICAgICAgICAkbWVzc2FnZSAgICA9ICRtb2R1bGUuZmluZChzZWxlY3Rvci5tZXNzYWdlKTtcbiAgICAgICAgICAkcHJvbXB0ICAgICA9ICRtb2R1bGUuZmluZChzZWxlY3Rvci5wcm9tcHQpO1xuXG4gICAgICAgICAgJHN1Ym1pdCAgICAgPSAkbW9kdWxlLmZpbmQoc2VsZWN0b3Iuc3VibWl0KTtcbiAgICAgICAgICAkY2xlYXIgICAgICA9ICRtb2R1bGUuZmluZChzZWxlY3Rvci5jbGVhcik7XG4gICAgICAgICAgJHJlc2V0ICAgICAgPSAkbW9kdWxlLmZpbmQoc2VsZWN0b3IucmVzZXQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHN1Ym1pdDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1N1Ym1pdHRpbmcgZm9ybScsICRtb2R1bGUpO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5zdWJtaXQoKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBhdHRhY2hFdmVudHM6IGZ1bmN0aW9uKHNlbGVjdG9yLCBhY3Rpb24pIHtcbiAgICAgICAgICBhY3Rpb24gPSBhY3Rpb24gfHwgJ3N1Ym1pdCc7XG4gICAgICAgICAgJChzZWxlY3RvcilcbiAgICAgICAgICAgIC5vbignY2xpY2snICsgZXZlbnROYW1lc3BhY2UsIGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICAgIG1vZHVsZVthY3Rpb25dKCk7XG4gICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBiaW5kRXZlbnRzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQXR0YWNoaW5nIGZvcm0gZXZlbnRzJyk7XG4gICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgLm9uKCdzdWJtaXQnICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS52YWxpZGF0ZS5mb3JtKVxuICAgICAgICAgICAgLm9uKCdibHVyJyAgICsgZXZlbnROYW1lc3BhY2UsIHNlbGVjdG9yLmZpZWxkLCBtb2R1bGUuZXZlbnQuZmllbGQuYmx1cilcbiAgICAgICAgICAgIC5vbignY2xpY2snICArIGV2ZW50TmFtZXNwYWNlLCBzZWxlY3Rvci5zdWJtaXQsIG1vZHVsZS5zdWJtaXQpXG4gICAgICAgICAgICAub24oJ2NsaWNrJyAgKyBldmVudE5hbWVzcGFjZSwgc2VsZWN0b3IucmVzZXQsIG1vZHVsZS5yZXNldClcbiAgICAgICAgICAgIC5vbignY2xpY2snICArIGV2ZW50TmFtZXNwYWNlLCBzZWxlY3Rvci5jbGVhciwgbW9kdWxlLmNsZWFyKVxuICAgICAgICAgIDtcbiAgICAgICAgICBpZihzZXR0aW5ncy5rZXlib2FyZFNob3J0Y3V0cykge1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAub24oJ2tleWRvd24nICsgZXZlbnROYW1lc3BhY2UsIHNlbGVjdG9yLmZpZWxkLCBtb2R1bGUuZXZlbnQuZmllbGQua2V5ZG93bilcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9XG4gICAgICAgICAgJGZpZWxkXG4gICAgICAgICAgICAuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICAgJGlucHV0ICAgICA9ICQodGhpcyksXG4gICAgICAgICAgICAgICAgdHlwZSAgICAgICA9ICRpbnB1dC5wcm9wKCd0eXBlJyksXG4gICAgICAgICAgICAgICAgaW5wdXRFdmVudCA9IG1vZHVsZS5nZXQuY2hhbmdlRXZlbnQodHlwZSwgJGlucHV0KVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgICQodGhpcylcbiAgICAgICAgICAgICAgICAub24oaW5wdXRFdmVudCArIGV2ZW50TmFtZXNwYWNlLCBtb2R1bGUuZXZlbnQuZmllbGQuY2hhbmdlKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBjbGVhcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgJGZpZWxkXG4gICAgICAgICAgICAuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAgICRmaWVsZCAgICAgICA9ICQodGhpcyksXG4gICAgICAgICAgICAgICAgJGVsZW1lbnQgICAgID0gJGZpZWxkLnBhcmVudCgpLFxuICAgICAgICAgICAgICAgICRmaWVsZEdyb3VwICA9ICRmaWVsZC5jbG9zZXN0KCRncm91cCksXG4gICAgICAgICAgICAgICAgJHByb21wdCAgICAgID0gJGZpZWxkR3JvdXAuZmluZChzZWxlY3Rvci5wcm9tcHQpLFxuICAgICAgICAgICAgICAgIGRlZmF1bHRWYWx1ZSA9ICRmaWVsZC5kYXRhKG1ldGFkYXRhLmRlZmF1bHRWYWx1ZSkgfHwgJycsXG4gICAgICAgICAgICAgICAgaXNDaGVja2JveCAgID0gJGVsZW1lbnQuaXMoc2VsZWN0b3IudWlDaGVja2JveCksXG4gICAgICAgICAgICAgICAgaXNEcm9wZG93biAgID0gJGVsZW1lbnQuaXMoc2VsZWN0b3IudWlEcm9wZG93biksXG4gICAgICAgICAgICAgICAgaXNFcnJvcmVkICAgID0gJGZpZWxkR3JvdXAuaGFzQ2xhc3MoY2xhc3NOYW1lLmVycm9yKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKGlzRXJyb3JlZCkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZXNldHRpbmcgZXJyb3Igb24gZmllbGQnLCAkZmllbGRHcm91cCk7XG4gICAgICAgICAgICAgICAgJGZpZWxkR3JvdXAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmVycm9yKTtcbiAgICAgICAgICAgICAgICAkcHJvbXB0LnJlbW92ZSgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGlmKGlzRHJvcGRvd24pIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVzZXR0aW5nIGRyb3Bkb3duIHZhbHVlJywgJGVsZW1lbnQsIGRlZmF1bHRWYWx1ZSk7XG4gICAgICAgICAgICAgICAgJGVsZW1lbnQuZHJvcGRvd24oJ2NsZWFyJyk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZihpc0NoZWNrYm94KSB7XG4gICAgICAgICAgICAgICAgJGZpZWxkLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1Jlc2V0dGluZyBmaWVsZCB2YWx1ZScsICRmaWVsZCwgZGVmYXVsdFZhbHVlKTtcbiAgICAgICAgICAgICAgICAkZmllbGQudmFsKCcnKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVzZXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICRmaWVsZFxuICAgICAgICAgICAgLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICAkZmllbGQgICAgICAgPSAkKHRoaXMpLFxuICAgICAgICAgICAgICAgICRlbGVtZW50ICAgICA9ICRmaWVsZC5wYXJlbnQoKSxcbiAgICAgICAgICAgICAgICAkZmllbGRHcm91cCAgPSAkZmllbGQuY2xvc2VzdCgkZ3JvdXApLFxuICAgICAgICAgICAgICAgICRwcm9tcHQgICAgICA9ICRmaWVsZEdyb3VwLmZpbmQoc2VsZWN0b3IucHJvbXB0KSxcbiAgICAgICAgICAgICAgICBkZWZhdWx0VmFsdWUgPSAkZmllbGQuZGF0YShtZXRhZGF0YS5kZWZhdWx0VmFsdWUpLFxuICAgICAgICAgICAgICAgIGlzQ2hlY2tib3ggICA9ICRlbGVtZW50LmlzKHNlbGVjdG9yLnVpQ2hlY2tib3gpLFxuICAgICAgICAgICAgICAgIGlzRHJvcGRvd24gICA9ICRlbGVtZW50LmlzKHNlbGVjdG9yLnVpRHJvcGRvd24pLFxuICAgICAgICAgICAgICAgIGlzRXJyb3JlZCAgICA9ICRmaWVsZEdyb3VwLmhhc0NsYXNzKGNsYXNzTmFtZS5lcnJvcilcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBpZihkZWZhdWx0VmFsdWUgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBpZihpc0Vycm9yZWQpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVzZXR0aW5nIGVycm9yIG9uIGZpZWxkJywgJGZpZWxkR3JvdXApO1xuICAgICAgICAgICAgICAgICRmaWVsZEdyb3VwLnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5lcnJvcik7XG4gICAgICAgICAgICAgICAgJHByb21wdC5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBpZihpc0Ryb3Bkb3duKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1Jlc2V0dGluZyBkcm9wZG93biB2YWx1ZScsICRlbGVtZW50LCBkZWZhdWx0VmFsdWUpO1xuICAgICAgICAgICAgICAgICRlbGVtZW50LmRyb3Bkb3duKCdyZXN0b3JlIGRlZmF1bHRzJyk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZihpc0NoZWNrYm94KSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1Jlc2V0dGluZyBjaGVja2JveCB2YWx1ZScsICRlbGVtZW50LCBkZWZhdWx0VmFsdWUpO1xuICAgICAgICAgICAgICAgICRmaWVsZC5wcm9wKCdjaGVja2VkJywgZGVmYXVsdFZhbHVlKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVzZXR0aW5nIGZpZWxkIHZhbHVlJywgJGZpZWxkLCBkZWZhdWx0VmFsdWUpO1xuICAgICAgICAgICAgICAgICRmaWVsZC52YWwoZGVmYXVsdFZhbHVlKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgaXM6IHtcbiAgICAgICAgICBicmFja2V0ZWRSdWxlOiBmdW5jdGlvbihydWxlKSB7XG4gICAgICAgICAgICByZXR1cm4gKHJ1bGUudHlwZSAmJiBydWxlLnR5cGUubWF0Y2goc2V0dGluZ3MucmVnRXhwLmJyYWNrZXQpKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGVtcHR5OiBmdW5jdGlvbigkZmllbGQpIHtcbiAgICAgICAgICAgIGlmKCEkZmllbGQgfHwgJGZpZWxkLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoJGZpZWxkLmlzKCdpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0nKSkge1xuICAgICAgICAgICAgICByZXR1cm4gISRmaWVsZC5pcygnOmNoZWNrZWQnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICByZXR1cm4gbW9kdWxlLmlzLmJsYW5rKCRmaWVsZCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBibGFuazogZnVuY3Rpb24oJGZpZWxkKSB7XG4gICAgICAgICAgICByZXR1cm4gJC50cmltKCRmaWVsZC52YWwoKSkgPT09ICcnO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgdmFsaWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGFsbFZhbGlkID0gdHJ1ZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0NoZWNraW5nIGlmIGZvcm0gaXMgdmFsaWQnKTtcbiAgICAgICAgICAgICQuZWFjaCh2YWxpZGF0aW9uLCBmdW5jdGlvbihmaWVsZE5hbWUsIGZpZWxkKSB7XG4gICAgICAgICAgICAgIGlmKCAhKCBtb2R1bGUudmFsaWRhdGUuZmllbGQoZmllbGQsIGZpZWxkTmFtZSkgKSApIHtcbiAgICAgICAgICAgICAgICBhbGxWYWxpZCA9IGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJldHVybiBhbGxWYWxpZDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVtb3ZlRXZlbnRzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAub2ZmKGV2ZW50TmFtZXNwYWNlKVxuICAgICAgICAgIDtcbiAgICAgICAgICAkZmllbGRcbiAgICAgICAgICAgIC5vZmYoZXZlbnROYW1lc3BhY2UpXG4gICAgICAgICAgO1xuICAgICAgICAgICRzdWJtaXRcbiAgICAgICAgICAgIC5vZmYoZXZlbnROYW1lc3BhY2UpXG4gICAgICAgICAgO1xuICAgICAgICAgICRmaWVsZFxuICAgICAgICAgICAgLm9mZihldmVudE5hbWVzcGFjZSlcbiAgICAgICAgICA7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZXZlbnQ6IHtcbiAgICAgICAgICBmaWVsZDoge1xuICAgICAgICAgICAga2V5ZG93bjogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICAgJGZpZWxkICAgICAgID0gJCh0aGlzKSxcbiAgICAgICAgICAgICAgICBrZXkgICAgICAgICAgPSBldmVudC53aGljaCxcbiAgICAgICAgICAgICAgICBpc0lucHV0ICAgICAgPSAkZmllbGQuaXMoc2VsZWN0b3IuaW5wdXQpLFxuICAgICAgICAgICAgICAgIGlzQ2hlY2tib3ggICA9ICRmaWVsZC5pcyhzZWxlY3Rvci5jaGVja2JveCksXG4gICAgICAgICAgICAgICAgaXNJbkRyb3Bkb3duID0gKCRmaWVsZC5jbG9zZXN0KHNlbGVjdG9yLnVpRHJvcGRvd24pLmxlbmd0aCA+IDApLFxuICAgICAgICAgICAgICAgIGtleUNvZGUgICAgICA9IHtcbiAgICAgICAgICAgICAgICAgIGVudGVyICA6IDEzLFxuICAgICAgICAgICAgICAgICAgZXNjYXBlIDogMjdcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgaWYoIGtleSA9PSBrZXlDb2RlLmVzY2FwZSkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdFc2NhcGUga2V5IHByZXNzZWQgYmx1cnJpbmcgZmllbGQnKTtcbiAgICAgICAgICAgICAgICAkZmllbGRcbiAgICAgICAgICAgICAgICAgIC5ibHVyKClcbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYoIWV2ZW50LmN0cmxLZXkgJiYga2V5ID09IGtleUNvZGUuZW50ZXIgJiYgaXNJbnB1dCAmJiAhaXNJbkRyb3Bkb3duICYmICFpc0NoZWNrYm94KSB7XG4gICAgICAgICAgICAgICAgaWYoIWtleUhlbGREb3duKSB7XG4gICAgICAgICAgICAgICAgICAkZmllbGRcbiAgICAgICAgICAgICAgICAgICAgLm9uZSgna2V5dXAnICsgZXZlbnROYW1lc3BhY2UsIG1vZHVsZS5ldmVudC5maWVsZC5rZXl1cClcbiAgICAgICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5zdWJtaXQoKTtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRW50ZXIgcHJlc3NlZCBvbiBpbnB1dCBzdWJtaXR0aW5nIGZvcm0nKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAga2V5SGVsZERvd24gPSB0cnVlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAga2V5dXA6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICBrZXlIZWxkRG93biA9IGZhbHNlO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGJsdXI6IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAgICRmaWVsZCAgICAgICAgICA9ICQodGhpcyksXG4gICAgICAgICAgICAgICAgJGZpZWxkR3JvdXAgICAgID0gJGZpZWxkLmNsb3Nlc3QoJGdyb3VwKSxcbiAgICAgICAgICAgICAgICB2YWxpZGF0aW9uUnVsZXMgPSBtb2R1bGUuZ2V0LnZhbGlkYXRpb24oJGZpZWxkKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKCAkZmllbGRHcm91cC5oYXNDbGFzcyhjbGFzc05hbWUuZXJyb3IpICkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnUmV2YWxpZGF0aW5nIGZpZWxkJywgJGZpZWxkLCB2YWxpZGF0aW9uUnVsZXMpO1xuICAgICAgICAgICAgICAgIGlmKHZhbGlkYXRpb25SdWxlcykge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnZhbGlkYXRlLmZpZWxkKCB2YWxpZGF0aW9uUnVsZXMgKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZihzZXR0aW5ncy5vbiA9PSAnYmx1cicgfHwgc2V0dGluZ3Mub24gPT0gJ2NoYW5nZScpIHtcbiAgICAgICAgICAgICAgICBpZih2YWxpZGF0aW9uUnVsZXMpIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS52YWxpZGF0ZS5maWVsZCggdmFsaWRhdGlvblJ1bGVzICk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgY2hhbmdlOiBmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICAkZmllbGQgICAgICA9ICQodGhpcyksXG4gICAgICAgICAgICAgICAgJGZpZWxkR3JvdXAgPSAkZmllbGQuY2xvc2VzdCgkZ3JvdXApLFxuICAgICAgICAgICAgICAgIHZhbGlkYXRpb25SdWxlcyA9IG1vZHVsZS5nZXQudmFsaWRhdGlvbigkZmllbGQpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgaWYoc2V0dGluZ3Mub24gPT0gJ2NoYW5nZScgfHwgKCAkZmllbGRHcm91cC5oYXNDbGFzcyhjbGFzc05hbWUuZXJyb3IpICYmIHNldHRpbmdzLnJldmFsaWRhdGUpICkge1xuICAgICAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUudGltZXIpO1xuICAgICAgICAgICAgICAgIG1vZHVsZS50aW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1JldmFsaWRhdGluZyBmaWVsZCcsICRmaWVsZCwgIG1vZHVsZS5nZXQudmFsaWRhdGlvbigkZmllbGQpKTtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS52YWxpZGF0ZS5maWVsZCggdmFsaWRhdGlvblJ1bGVzICk7XG4gICAgICAgICAgICAgICAgfSwgc2V0dGluZ3MuZGVsYXkpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuXG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0OiB7XG4gICAgICAgICAgYW5jaWxsYXJ5VmFsdWU6IGZ1bmN0aW9uKHJ1bGUpIHtcbiAgICAgICAgICAgIGlmKCFydWxlLnR5cGUgfHwgIW1vZHVsZS5pcy5icmFja2V0ZWRSdWxlKHJ1bGUpKSB7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBydWxlLnR5cGUubWF0Y2goc2V0dGluZ3MucmVnRXhwLmJyYWNrZXQpWzFdICsgJyc7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBydWxlTmFtZTogZnVuY3Rpb24ocnVsZSkge1xuICAgICAgICAgICAgaWYoIG1vZHVsZS5pcy5icmFja2V0ZWRSdWxlKHJ1bGUpICkge1xuICAgICAgICAgICAgICByZXR1cm4gcnVsZS50eXBlLnJlcGxhY2UocnVsZS50eXBlLm1hdGNoKHNldHRpbmdzLnJlZ0V4cC5icmFja2V0KVswXSwgJycpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHJ1bGUudHlwZTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGNoYW5nZUV2ZW50OiBmdW5jdGlvbih0eXBlLCAkaW5wdXQpIHtcbiAgICAgICAgICAgIGlmKHR5cGUgPT0gJ2NoZWNrYm94JyB8fCB0eXBlID09ICdyYWRpbycgfHwgdHlwZSA9PSAnaGlkZGVuJyB8fCAkaW5wdXQuaXMoJ3NlbGVjdCcpKSB7XG4gICAgICAgICAgICAgIHJldHVybiAnY2hhbmdlJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICByZXR1cm4gbW9kdWxlLmdldC5pbnB1dEV2ZW50KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBpbnB1dEV2ZW50OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAoZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaW5wdXQnKS5vbmlucHV0ICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgID8gJ2lucHV0J1xuICAgICAgICAgICAgICA6IChkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpbnB1dCcpLm9ucHJvcGVydHljaGFuZ2UgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICA/ICdwcm9wZXJ0eWNoYW5nZSdcbiAgICAgICAgICAgICAgICA6ICdrZXl1cCdcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHByb21wdDogZnVuY3Rpb24ocnVsZSwgZmllbGQpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBydWxlTmFtZSAgICAgID0gbW9kdWxlLmdldC5ydWxlTmFtZShydWxlKSxcbiAgICAgICAgICAgICAgYW5jaWxsYXJ5ICAgICA9IG1vZHVsZS5nZXQuYW5jaWxsYXJ5VmFsdWUocnVsZSksXG4gICAgICAgICAgICAgIHByb21wdCAgICAgICAgPSBydWxlLnByb21wdCB8fCBzZXR0aW5ncy5wcm9tcHRbcnVsZU5hbWVdIHx8IHNldHRpbmdzLnRleHQudW5zcGVjaWZpZWRSdWxlLFxuICAgICAgICAgICAgICByZXF1aXJlc1ZhbHVlID0gKHByb21wdC5zZWFyY2goJ3t2YWx1ZX0nKSAhPT0gLTEpLFxuICAgICAgICAgICAgICByZXF1aXJlc05hbWUgID0gKHByb21wdC5zZWFyY2goJ3tuYW1lfScpICE9PSAtMSksXG4gICAgICAgICAgICAgICRsYWJlbCxcbiAgICAgICAgICAgICAgJGZpZWxkLFxuICAgICAgICAgICAgICBuYW1lXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihyZXF1aXJlc05hbWUgfHwgcmVxdWlyZXNWYWx1ZSkge1xuICAgICAgICAgICAgICAkZmllbGQgPSBtb2R1bGUuZ2V0LmZpZWxkKGZpZWxkLmlkZW50aWZpZXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYocmVxdWlyZXNWYWx1ZSkge1xuICAgICAgICAgICAgICBwcm9tcHQgPSBwcm9tcHQucmVwbGFjZSgne3ZhbHVlfScsICRmaWVsZC52YWwoKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihyZXF1aXJlc05hbWUpIHtcbiAgICAgICAgICAgICAgJGxhYmVsID0gJGZpZWxkLmNsb3Nlc3Qoc2VsZWN0b3IuZ3JvdXApLmZpbmQoJ2xhYmVsJykuZXEoMCk7XG4gICAgICAgICAgICAgIG5hbWUgPSAoJGxhYmVsLmxlbmd0aCA9PSAxKVxuICAgICAgICAgICAgICAgID8gJGxhYmVsLnRleHQoKVxuICAgICAgICAgICAgICAgIDogJGZpZWxkLnByb3AoJ3BsYWNlaG9sZGVyJykgfHwgc2V0dGluZ3MudGV4dC51bnNwZWNpZmllZEZpZWxkXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgcHJvbXB0ID0gcHJvbXB0LnJlcGxhY2UoJ3tuYW1lfScsIG5hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcHJvbXB0ID0gcHJvbXB0LnJlcGxhY2UoJ3tpZGVudGlmaWVyfScsIGZpZWxkLmlkZW50aWZpZXIpO1xuICAgICAgICAgICAgcHJvbXB0ID0gcHJvbXB0LnJlcGxhY2UoJ3tydWxlVmFsdWV9JywgYW5jaWxsYXJ5KTtcbiAgICAgICAgICAgIGlmKCFydWxlLnByb21wdCkge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnVXNpbmcgZGVmYXVsdCB2YWxpZGF0aW9uIHByb21wdCBmb3IgdHlwZScsIHByb21wdCwgcnVsZU5hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHByb21wdDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHNldHRpbmdzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKCQuaXNQbGFpbk9iamVjdChwYXJhbWV0ZXJzKSkge1xuICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICBrZXlzICAgICA9IE9iamVjdC5rZXlzKHBhcmFtZXRlcnMpLFxuICAgICAgICAgICAgICAgIGlzTGVnYWN5U2V0dGluZ3MgPSAoa2V5cy5sZW5ndGggPiAwKVxuICAgICAgICAgICAgICAgICAgPyAocGFyYW1ldGVyc1trZXlzWzBdXS5pZGVudGlmaWVyICE9PSB1bmRlZmluZWQgJiYgcGFyYW1ldGVyc1trZXlzWzBdXS5ydWxlcyAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICAgICAgOiBmYWxzZSxcbiAgICAgICAgICAgICAgICBydWxlS2V5c1xuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKGlzTGVnYWN5U2V0dGluZ3MpIHtcbiAgICAgICAgICAgICAgICAvLyAxLnggKGR1Y2t0eXBlZClcbiAgICAgICAgICAgICAgICBzZXR0aW5ncyAgID0gJC5leHRlbmQodHJ1ZSwge30sICQuZm4uZm9ybS5zZXR0aW5ncywgbGVnYWN5UGFyYW1ldGVycyk7XG4gICAgICAgICAgICAgICAgdmFsaWRhdGlvbiA9ICQuZXh0ZW5kKHt9LCAkLmZuLmZvcm0uc2V0dGluZ3MuZGVmYXVsdHMsIHBhcmFtZXRlcnMpO1xuICAgICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihzZXR0aW5ncy5lcnJvci5vbGRTeW50YXgsIGVsZW1lbnQpO1xuICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdFeHRlbmRpbmcgc2V0dGluZ3MgZnJvbSBsZWdhY3kgcGFyYW1ldGVycycsIHZhbGlkYXRpb24sIHNldHRpbmdzKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAvLyAyLnhcbiAgICAgICAgICAgICAgICBpZihwYXJhbWV0ZXJzLmZpZWxkcykge1xuICAgICAgICAgICAgICAgICAgcnVsZUtleXMgPSBPYmplY3Qua2V5cyhwYXJhbWV0ZXJzLmZpZWxkcyk7XG4gICAgICAgICAgICAgICAgICBpZiggdHlwZW9mIHBhcmFtZXRlcnMuZmllbGRzW3J1bGVLZXlzWzBdXSA9PSAnc3RyaW5nJyB8fCAkLmlzQXJyYXkocGFyYW1ldGVycy5maWVsZHNbcnVsZUtleXNbMF1dKSApIHtcbiAgICAgICAgICAgICAgICAgICAgJC5lYWNoKHBhcmFtZXRlcnMuZmllbGRzLCBmdW5jdGlvbihuYW1lLCBydWxlcykge1xuICAgICAgICAgICAgICAgICAgICAgIGlmKHR5cGVvZiBydWxlcyA9PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgcnVsZXMgPSBbcnVsZXNdO1xuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICBwYXJhbWV0ZXJzLmZpZWxkc1tuYW1lXSA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJ1bGVzOiBbXVxuICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgICAgJC5lYWNoKHJ1bGVzLCBmdW5jdGlvbihpbmRleCwgcnVsZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1ldGVycy5maWVsZHNbbmFtZV0ucnVsZXMucHVzaCh7IHR5cGU6IHJ1bGUgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHNldHRpbmdzICAgPSAkLmV4dGVuZCh0cnVlLCB7fSwgJC5mbi5mb3JtLnNldHRpbmdzLCBwYXJhbWV0ZXJzKTtcbiAgICAgICAgICAgICAgICB2YWxpZGF0aW9uID0gJC5leHRlbmQoe30sICQuZm4uZm9ybS5zZXR0aW5ncy5kZWZhdWx0cywgc2V0dGluZ3MuZmllbGRzKTtcbiAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnRXh0ZW5kaW5nIHNldHRpbmdzJywgdmFsaWRhdGlvbiwgc2V0dGluZ3MpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgc2V0dGluZ3MgICA9ICQuZm4uZm9ybS5zZXR0aW5ncztcbiAgICAgICAgICAgICAgdmFsaWRhdGlvbiA9ICQuZm4uZm9ybS5zZXR0aW5ncy5kZWZhdWx0cztcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1VzaW5nIGRlZmF1bHQgZm9ybSB2YWxpZGF0aW9uJywgdmFsaWRhdGlvbiwgc2V0dGluZ3MpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBzaG9ydGhhbmRcbiAgICAgICAgICAgIG5hbWVzcGFjZSAgICAgICA9IHNldHRpbmdzLm5hbWVzcGFjZTtcbiAgICAgICAgICAgIG1ldGFkYXRhICAgICAgICA9IHNldHRpbmdzLm1ldGFkYXRhO1xuICAgICAgICAgICAgc2VsZWN0b3IgICAgICAgID0gc2V0dGluZ3Muc2VsZWN0b3I7XG4gICAgICAgICAgICBjbGFzc05hbWUgICAgICAgPSBzZXR0aW5ncy5jbGFzc05hbWU7XG4gICAgICAgICAgICBlcnJvciAgICAgICAgICAgPSBzZXR0aW5ncy5lcnJvcjtcbiAgICAgICAgICAgIG1vZHVsZU5hbWVzcGFjZSA9ICdtb2R1bGUtJyArIG5hbWVzcGFjZTtcbiAgICAgICAgICAgIGV2ZW50TmFtZXNwYWNlICA9ICcuJyArIG5hbWVzcGFjZTtcblxuICAgICAgICAgICAgLy8gZ3JhYiBpbnN0YW5jZVxuICAgICAgICAgICAgaW5zdGFuY2UgPSAkbW9kdWxlLmRhdGEobW9kdWxlTmFtZXNwYWNlKTtcblxuICAgICAgICAgICAgLy8gcmVmcmVzaCBzZWxlY3RvciBjYWNoZVxuICAgICAgICAgICAgbW9kdWxlLnJlZnJlc2goKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGZpZWxkOiBmdW5jdGlvbihpZGVudGlmaWVyKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnRmluZGluZyBmaWVsZCB3aXRoIGlkZW50aWZpZXInLCBpZGVudGlmaWVyKTtcbiAgICAgICAgICAgIGlmKCAkZmllbGQuZmlsdGVyKCcjJyArIGlkZW50aWZpZXIpLmxlbmd0aCA+IDAgKSB7XG4gICAgICAgICAgICAgIHJldHVybiAkZmllbGQuZmlsdGVyKCcjJyArIGlkZW50aWZpZXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiggJGZpZWxkLmZpbHRlcignW25hbWU9XCInICsgaWRlbnRpZmllciArJ1wiXScpLmxlbmd0aCA+IDAgKSB7XG4gICAgICAgICAgICAgIHJldHVybiAkZmllbGQuZmlsdGVyKCdbbmFtZT1cIicgKyBpZGVudGlmaWVyICsnXCJdJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKCAkZmllbGQuZmlsdGVyKCdbbmFtZT1cIicgKyBpZGVudGlmaWVyICsnW11cIl0nKS5sZW5ndGggPiAwICkge1xuICAgICAgICAgICAgICByZXR1cm4gJGZpZWxkLmZpbHRlcignW25hbWU9XCInICsgaWRlbnRpZmllciArJ1tdXCJdJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKCAkZmllbGQuZmlsdGVyKCdbZGF0YS0nICsgbWV0YWRhdGEudmFsaWRhdGUgKyAnPVwiJysgaWRlbnRpZmllciArJ1wiXScpLmxlbmd0aCA+IDAgKSB7XG4gICAgICAgICAgICAgIHJldHVybiAkZmllbGQuZmlsdGVyKCdbZGF0YS0nICsgbWV0YWRhdGEudmFsaWRhdGUgKyAnPVwiJysgaWRlbnRpZmllciArJ1wiXScpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuICQoJzxpbnB1dC8+Jyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBmaWVsZHM6IGZ1bmN0aW9uKGZpZWxkcykge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICRmaWVsZHMgPSAkKClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgICQuZWFjaChmaWVsZHMsIGZ1bmN0aW9uKGluZGV4LCBuYW1lKSB7XG4gICAgICAgICAgICAgICRmaWVsZHMgPSAkZmllbGRzLmFkZCggbW9kdWxlLmdldC5maWVsZChuYW1lKSApO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXR1cm4gJGZpZWxkcztcbiAgICAgICAgICB9LFxuICAgICAgICAgIHZhbGlkYXRpb246IGZ1bmN0aW9uKCRmaWVsZCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGZpZWxkVmFsaWRhdGlvbixcbiAgICAgICAgICAgICAgaWRlbnRpZmllclxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoIXZhbGlkYXRpb24pIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgJC5lYWNoKHZhbGlkYXRpb24sIGZ1bmN0aW9uKGZpZWxkTmFtZSwgZmllbGQpIHtcbiAgICAgICAgICAgICAgaWRlbnRpZmllciA9IGZpZWxkLmlkZW50aWZpZXIgfHwgZmllbGROYW1lO1xuICAgICAgICAgICAgICBpZiggbW9kdWxlLmdldC5maWVsZChpZGVudGlmaWVyKVswXSA9PSAkZmllbGRbMF0gKSB7XG4gICAgICAgICAgICAgICAgZmllbGQuaWRlbnRpZmllciA9IGlkZW50aWZpZXI7XG4gICAgICAgICAgICAgICAgZmllbGRWYWxpZGF0aW9uID0gZmllbGQ7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgcmV0dXJuIGZpZWxkVmFsaWRhdGlvbiB8fCBmYWxzZTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbiAoZmllbGQpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBmaWVsZHMgPSBbXSxcbiAgICAgICAgICAgICAgcmVzdWx0c1xuICAgICAgICAgICAgO1xuICAgICAgICAgICAgZmllbGRzLnB1c2goZmllbGQpO1xuICAgICAgICAgICAgcmVzdWx0cyA9IG1vZHVsZS5nZXQudmFsdWVzLmNhbGwoZWxlbWVudCwgZmllbGRzKTtcbiAgICAgICAgICAgIHJldHVybiByZXN1bHRzW2ZpZWxkXTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHZhbHVlczogZnVuY3Rpb24gKGZpZWxkcykge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICRmaWVsZHMgPSAkLmlzQXJyYXkoZmllbGRzKVxuICAgICAgICAgICAgICAgID8gbW9kdWxlLmdldC5maWVsZHMoZmllbGRzKVxuICAgICAgICAgICAgICAgIDogJGZpZWxkLFxuICAgICAgICAgICAgICB2YWx1ZXMgPSB7fVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgJGZpZWxkcy5lYWNoKGZ1bmN0aW9uKGluZGV4LCBmaWVsZCkge1xuICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICAkZmllbGQgICAgID0gJChmaWVsZCksXG4gICAgICAgICAgICAgICAgdHlwZSAgICAgICA9ICRmaWVsZC5wcm9wKCd0eXBlJyksXG4gICAgICAgICAgICAgICAgbmFtZSAgICAgICA9ICRmaWVsZC5wcm9wKCduYW1lJyksXG4gICAgICAgICAgICAgICAgdmFsdWUgICAgICA9ICRmaWVsZC52YWwoKSxcbiAgICAgICAgICAgICAgICBpc0NoZWNrYm94ID0gJGZpZWxkLmlzKHNlbGVjdG9yLmNoZWNrYm94KSxcbiAgICAgICAgICAgICAgICBpc1JhZGlvICAgID0gJGZpZWxkLmlzKHNlbGVjdG9yLnJhZGlvKSxcbiAgICAgICAgICAgICAgICBpc011bHRpcGxlID0gKG5hbWUuaW5kZXhPZignW10nKSAhPT0gLTEpLFxuICAgICAgICAgICAgICAgIGlzQ2hlY2tlZCAgPSAoaXNDaGVja2JveClcbiAgICAgICAgICAgICAgICAgID8gJGZpZWxkLmlzKCc6Y2hlY2tlZCcpXG4gICAgICAgICAgICAgICAgICA6IGZhbHNlXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgaWYobmFtZSkge1xuICAgICAgICAgICAgICAgIGlmKGlzTXVsdGlwbGUpIHtcbiAgICAgICAgICAgICAgICAgIG5hbWUgPSBuYW1lLnJlcGxhY2UoJ1tdJywgJycpO1xuICAgICAgICAgICAgICAgICAgaWYoIXZhbHVlc1tuYW1lXSkge1xuICAgICAgICAgICAgICAgICAgICB2YWx1ZXNbbmFtZV0gPSBbXTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGlmKGlzQ2hlY2tib3gpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYoaXNDaGVja2VkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgdmFsdWVzW25hbWVdLnB1c2godmFsdWUgfHwgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgdmFsdWVzW25hbWVdLnB1c2goZmFsc2UpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdmFsdWVzW25hbWVdLnB1c2godmFsdWUpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIGlmKGlzUmFkaW8pIHtcbiAgICAgICAgICAgICAgICAgICAgaWYoaXNDaGVja2VkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgdmFsdWVzW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGVsc2UgaWYoaXNDaGVja2JveCkge1xuICAgICAgICAgICAgICAgICAgICBpZihpc0NoZWNrZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICB2YWx1ZXNbbmFtZV0gPSB2YWx1ZSB8fCB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgIHZhbHVlc1tuYW1lXSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdmFsdWVzW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJldHVybiB2YWx1ZXM7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGhhczoge1xuXG4gICAgICAgICAgZmllbGQ6IGZ1bmN0aW9uKGlkZW50aWZpZXIpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdDaGVja2luZyBmb3IgZXhpc3RlbmNlIG9mIGEgZmllbGQgd2l0aCBpZGVudGlmaWVyJywgaWRlbnRpZmllcik7XG4gICAgICAgICAgICBpZih0eXBlb2YgaWRlbnRpZmllciAhPT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLmlkZW50aWZpZXIsIGlkZW50aWZpZXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoICRmaWVsZC5maWx0ZXIoJyMnICsgaWRlbnRpZmllcikubGVuZ3RoID4gMCApIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKCAkZmllbGQuZmlsdGVyKCdbbmFtZT1cIicgKyBpZGVudGlmaWVyICsnXCJdJykubGVuZ3RoID4gMCApIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKCAkZmllbGQuZmlsdGVyKCdbZGF0YS0nICsgbWV0YWRhdGEudmFsaWRhdGUgKyAnPVwiJysgaWRlbnRpZmllciArJ1wiXScpLmxlbmd0aCA+IDAgKSB7XG4gICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH1cblxuICAgICAgICB9LFxuXG4gICAgICAgIGFkZDoge1xuICAgICAgICAgIHByb21wdDogZnVuY3Rpb24oaWRlbnRpZmllciwgZXJyb3JzKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgJGZpZWxkICAgICAgID0gbW9kdWxlLmdldC5maWVsZChpZGVudGlmaWVyKSxcbiAgICAgICAgICAgICAgJGZpZWxkR3JvdXAgID0gJGZpZWxkLmNsb3Nlc3QoJGdyb3VwKSxcbiAgICAgICAgICAgICAgJHByb21wdCAgICAgID0gJGZpZWxkR3JvdXAuY2hpbGRyZW4oc2VsZWN0b3IucHJvbXB0KSxcbiAgICAgICAgICAgICAgcHJvbXB0RXhpc3RzID0gKCRwcm9tcHQubGVuZ3RoICE9PSAwKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgZXJyb3JzID0gKHR5cGVvZiBlcnJvcnMgPT0gJ3N0cmluZycpXG4gICAgICAgICAgICAgID8gW2Vycm9yc11cbiAgICAgICAgICAgICAgOiBlcnJvcnNcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdBZGRpbmcgZmllbGQgZXJyb3Igc3RhdGUnLCBpZGVudGlmaWVyKTtcbiAgICAgICAgICAgICRmaWVsZEdyb3VwXG4gICAgICAgICAgICAgIC5hZGRDbGFzcyhjbGFzc05hbWUuZXJyb3IpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5pbmxpbmUpIHtcbiAgICAgICAgICAgICAgaWYoIXByb21wdEV4aXN0cykge1xuICAgICAgICAgICAgICAgICRwcm9tcHQgPSBzZXR0aW5ncy50ZW1wbGF0ZXMucHJvbXB0KGVycm9ycyk7XG4gICAgICAgICAgICAgICAgJHByb21wdFxuICAgICAgICAgICAgICAgICAgLmFwcGVuZFRvKCRmaWVsZEdyb3VwKVxuICAgICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAkcHJvbXB0XG4gICAgICAgICAgICAgICAgLmh0bWwoZXJyb3JzWzBdKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKCFwcm9tcHRFeGlzdHMpIHtcbiAgICAgICAgICAgICAgICBpZihzZXR0aW5ncy50cmFuc2l0aW9uICYmICQuZm4udHJhbnNpdGlvbiAhPT0gdW5kZWZpbmVkICYmICRtb2R1bGUudHJhbnNpdGlvbignaXMgc3VwcG9ydGVkJykpIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdEaXNwbGF5aW5nIGVycm9yIHdpdGggY3NzIHRyYW5zaXRpb24nLCBzZXR0aW5ncy50cmFuc2l0aW9uKTtcbiAgICAgICAgICAgICAgICAgICRwcm9tcHQudHJhbnNpdGlvbihzZXR0aW5ncy50cmFuc2l0aW9uICsgJyBpbicsIHNldHRpbmdzLmR1cmF0aW9uKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnRGlzcGxheWluZyBlcnJvciB3aXRoIGZhbGxiYWNrIGphdmFzY3JpcHQgYW5pbWF0aW9uJyk7XG4gICAgICAgICAgICAgICAgICAkcHJvbXB0XG4gICAgICAgICAgICAgICAgICAgIC5mYWRlSW4oc2V0dGluZ3MuZHVyYXRpb24pXG4gICAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdJbmxpbmUgZXJyb3JzIGFyZSBkaXNhYmxlZCwgbm8gaW5saW5lIGVycm9yIGFkZGVkJywgaWRlbnRpZmllcik7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGVycm9yczogZnVuY3Rpb24oZXJyb3JzKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0FkZGluZyBmb3JtIGVycm9yIG1lc3NhZ2VzJywgZXJyb3JzKTtcbiAgICAgICAgICAgIG1vZHVsZS5zZXQuZXJyb3IoKTtcbiAgICAgICAgICAgICRtZXNzYWdlXG4gICAgICAgICAgICAgIC5odG1sKCBzZXR0aW5ncy50ZW1wbGF0ZXMuZXJyb3IoZXJyb3JzKSApXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlbW92ZToge1xuICAgICAgICAgIHByb21wdDogZnVuY3Rpb24oaWRlbnRpZmllcikge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICRmaWVsZCAgICAgID0gbW9kdWxlLmdldC5maWVsZChpZGVudGlmaWVyKSxcbiAgICAgICAgICAgICAgJGZpZWxkR3JvdXAgPSAkZmllbGQuY2xvc2VzdCgkZ3JvdXApLFxuICAgICAgICAgICAgICAkcHJvbXB0ICAgICA9ICRmaWVsZEdyb3VwLmNoaWxkcmVuKHNlbGVjdG9yLnByb21wdClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgICRmaWVsZEdyb3VwXG4gICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuZXJyb3IpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5pbmxpbmUgJiYgJHByb21wdC5pcygnOnZpc2libGUnKSkge1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnUmVtb3ZpbmcgcHJvbXB0IGZvciBmaWVsZCcsIGlkZW50aWZpZXIpO1xuICAgICAgICAgICAgICBpZihzZXR0aW5ncy50cmFuc2l0aW9uICYmICQuZm4udHJhbnNpdGlvbiAhPT0gdW5kZWZpbmVkICYmICRtb2R1bGUudHJhbnNpdGlvbignaXMgc3VwcG9ydGVkJykpIHtcbiAgICAgICAgICAgICAgICAkcHJvbXB0LnRyYW5zaXRpb24oc2V0dGluZ3MudHJhbnNpdGlvbiArICcgb3V0Jywgc2V0dGluZ3MuZHVyYXRpb24sIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgJHByb21wdC5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAkcHJvbXB0XG4gICAgICAgICAgICAgICAgICAuZmFkZU91dChzZXR0aW5ncy5kdXJhdGlvbiwgZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgICAgJHByb21wdC5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldDoge1xuICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lLmVycm9yKVxuICAgICAgICAgICAgICAuYWRkQ2xhc3MoY2xhc3NOYW1lLnN1Y2Nlc3MpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkZWZhdWx0czogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJGZpZWxkXG4gICAgICAgICAgICAgIC5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICAgICRmaWVsZCAgICAgPSAkKHRoaXMpLFxuICAgICAgICAgICAgICAgICAgaXNDaGVja2JveCA9ICgkZmllbGQuZmlsdGVyKHNlbGVjdG9yLmNoZWNrYm94KS5sZW5ndGggPiAwKSxcbiAgICAgICAgICAgICAgICAgIHZhbHVlICAgICAgPSAoaXNDaGVja2JveClcbiAgICAgICAgICAgICAgICAgICAgPyAkZmllbGQuaXMoJzpjaGVja2VkJylcbiAgICAgICAgICAgICAgICAgICAgOiAkZmllbGQudmFsKClcbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgICAgJGZpZWxkLmRhdGEobWV0YWRhdGEuZGVmYXVsdFZhbHVlLCB2YWx1ZSk7XG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBlcnJvcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAgIC5yZW1vdmVDbGFzcyhjbGFzc05hbWUuc3VjY2VzcylcbiAgICAgICAgICAgICAgLmFkZENsYXNzKGNsYXNzTmFtZS5lcnJvcilcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHZhbHVlOiBmdW5jdGlvbiAoZmllbGQsIHZhbHVlKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgZmllbGRzID0ge31cbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGZpZWxkc1tmaWVsZF0gPSB2YWx1ZTtcbiAgICAgICAgICAgIHJldHVybiBtb2R1bGUuc2V0LnZhbHVlcy5jYWxsKGVsZW1lbnQsIGZpZWxkcyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB2YWx1ZXM6IGZ1bmN0aW9uIChmaWVsZHMpIHtcbiAgICAgICAgICAgIGlmKCQuaXNFbXB0eU9iamVjdChmaWVsZHMpKSB7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICQuZWFjaChmaWVsZHMsIGZ1bmN0aW9uKGtleSwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICAgJGZpZWxkICAgICAgPSBtb2R1bGUuZ2V0LmZpZWxkKGtleSksXG4gICAgICAgICAgICAgICAgJGVsZW1lbnQgICAgPSAkZmllbGQucGFyZW50KCksXG4gICAgICAgICAgICAgICAgaXNNdWx0aXBsZSAgPSAkLmlzQXJyYXkodmFsdWUpLFxuICAgICAgICAgICAgICAgIGlzQ2hlY2tib3ggID0gJGVsZW1lbnQuaXMoc2VsZWN0b3IudWlDaGVja2JveCksXG4gICAgICAgICAgICAgICAgaXNEcm9wZG93biAgPSAkZWxlbWVudC5pcyhzZWxlY3Rvci51aURyb3Bkb3duKSxcbiAgICAgICAgICAgICAgICBpc1JhZGlvICAgICA9ICgkZmllbGQuaXMoc2VsZWN0b3IucmFkaW8pICYmIGlzQ2hlY2tib3gpLFxuICAgICAgICAgICAgICAgIGZpZWxkRXhpc3RzID0gKCRmaWVsZC5sZW5ndGggPiAwKSxcbiAgICAgICAgICAgICAgICAkbXVsdGlwbGVGaWVsZFxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKGZpZWxkRXhpc3RzKSB7XG4gICAgICAgICAgICAgICAgaWYoaXNNdWx0aXBsZSAmJiBpc0NoZWNrYm94KSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU2VsZWN0aW5nIG11bHRpcGxlJywgdmFsdWUsICRmaWVsZCk7XG4gICAgICAgICAgICAgICAgICAkZWxlbWVudC5jaGVja2JveCgndW5jaGVjaycpO1xuICAgICAgICAgICAgICAgICAgJC5lYWNoKHZhbHVlLCBmdW5jdGlvbihpbmRleCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgJG11bHRpcGxlRmllbGQgPSAkZmllbGQuZmlsdGVyKCdbdmFsdWU9XCInICsgdmFsdWUgKyAnXCJdJyk7XG4gICAgICAgICAgICAgICAgICAgICRlbGVtZW50ICAgICAgID0gJG11bHRpcGxlRmllbGQucGFyZW50KCk7XG4gICAgICAgICAgICAgICAgICAgIGlmKCRtdWx0aXBsZUZpZWxkLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAkZWxlbWVudC5jaGVja2JveCgnY2hlY2snKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2UgaWYoaXNSYWRpbykge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1NlbGVjdGluZyByYWRpbyB2YWx1ZScsIHZhbHVlLCAkZmllbGQpO1xuICAgICAgICAgICAgICAgICAgJGZpZWxkLmZpbHRlcignW3ZhbHVlPVwiJyArIHZhbHVlICsgJ1wiXScpXG4gICAgICAgICAgICAgICAgICAgIC5wYXJlbnQoc2VsZWN0b3IudWlDaGVja2JveClcbiAgICAgICAgICAgICAgICAgICAgICAuY2hlY2tib3goJ2NoZWNrJylcbiAgICAgICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSBpZihpc0NoZWNrYm94KSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU2V0dGluZyBjaGVja2JveCB2YWx1ZScsIHZhbHVlLCAkZWxlbWVudCk7XG4gICAgICAgICAgICAgICAgICBpZih2YWx1ZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICAkZWxlbWVudC5jaGVja2JveCgnY2hlY2snKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAkZWxlbWVudC5jaGVja2JveCgndW5jaGVjaycpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmKGlzRHJvcGRvd24pIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTZXR0aW5nIGRyb3Bkb3duIHZhbHVlJywgdmFsdWUsICRlbGVtZW50KTtcbiAgICAgICAgICAgICAgICAgICRlbGVtZW50LmRyb3Bkb3duKCdzZXQgc2VsZWN0ZWQnLCB2YWx1ZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1NldHRpbmcgZmllbGQgdmFsdWUnLCB2YWx1ZSwgJGZpZWxkKTtcbiAgICAgICAgICAgICAgICAgICRmaWVsZC52YWwodmFsdWUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHZhbGlkYXRlOiB7XG5cbiAgICAgICAgICBmb3JtOiBmdW5jdGlvbihldmVudCwgaWdub3JlQ2FsbGJhY2tzKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgdmFsdWVzID0gbW9kdWxlLmdldC52YWx1ZXMoKSxcbiAgICAgICAgICAgICAgYXBpUmVxdWVzdFxuICAgICAgICAgICAgO1xuXG4gICAgICAgICAgICAvLyBpbnB1dCBrZXlkb3duIGV2ZW50IHdpbGwgZmlyZSBzdWJtaXQgcmVwZWF0ZWRseSBieSBicm93c2VyIGRlZmF1bHRcbiAgICAgICAgICAgIGlmKGtleUhlbGREb3duKSB7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gcmVzZXQgZXJyb3JzXG4gICAgICAgICAgICBmb3JtRXJyb3JzID0gW107XG4gICAgICAgICAgICBpZiggbW9kdWxlLmlzLnZhbGlkKCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRm9ybSBoYXMgbm8gdmFsaWRhdGlvbiBlcnJvcnMsIHN1Ym1pdHRpbmcnKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnNldC5zdWNjZXNzKCk7XG4gICAgICAgICAgICAgIGlmKGlnbm9yZUNhbGxiYWNrcyAhPT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBzZXR0aW5ncy5vblN1Y2Nlc3MuY2FsbChlbGVtZW50LCBldmVudCwgdmFsdWVzKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRm9ybSBoYXMgZXJyb3JzJyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5zZXQuZXJyb3IoKTtcbiAgICAgICAgICAgICAgaWYoIXNldHRpbmdzLmlubGluZSkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5hZGQuZXJyb3JzKGZvcm1FcnJvcnMpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIC8vIHByZXZlbnQgYWpheCBzdWJtaXRcbiAgICAgICAgICAgICAgaWYoJG1vZHVsZS5kYXRhKCdtb2R1bGVBcGknKSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgZXZlbnQuc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYoaWdub3JlQ2FsbGJhY2tzICE9PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHNldHRpbmdzLm9uRmFpbHVyZS5jYWxsKGVsZW1lbnQsIGZvcm1FcnJvcnMsIHZhbHVlcyk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuXG4gICAgICAgICAgLy8gdGFrZXMgYSB2YWxpZGF0aW9uIG9iamVjdCBhbmQgcmV0dXJucyB3aGV0aGVyIGZpZWxkIHBhc3NlcyB2YWxpZGF0aW9uXG4gICAgICAgICAgZmllbGQ6IGZ1bmN0aW9uKGZpZWxkLCBmaWVsZE5hbWUpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBpZGVudGlmaWVyICAgID0gZmllbGQuaWRlbnRpZmllciB8fCBmaWVsZE5hbWUsXG4gICAgICAgICAgICAgICRmaWVsZCAgICAgICAgPSBtb2R1bGUuZ2V0LmZpZWxkKGlkZW50aWZpZXIpLFxuICAgICAgICAgICAgICAkZGVwZW5kc0ZpZWxkID0gKGZpZWxkLmRlcGVuZHMpXG4gICAgICAgICAgICAgICAgPyBtb2R1bGUuZ2V0LmZpZWxkKGZpZWxkLmRlcGVuZHMpXG4gICAgICAgICAgICAgICAgOiBmYWxzZSxcbiAgICAgICAgICAgICAgZmllbGRWYWxpZCAgPSB0cnVlLFxuICAgICAgICAgICAgICBmaWVsZEVycm9ycyA9IFtdXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZighZmllbGQuaWRlbnRpZmllcikge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1VzaW5nIGZpZWxkIG5hbWUgYXMgaWRlbnRpZmllcicsIGlkZW50aWZpZXIpO1xuICAgICAgICAgICAgICBmaWVsZC5pZGVudGlmaWVyID0gaWRlbnRpZmllcjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCRmaWVsZC5wcm9wKCdkaXNhYmxlZCcpKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRmllbGQgaXMgZGlzYWJsZWQuIFNraXBwaW5nJywgaWRlbnRpZmllcik7XG4gICAgICAgICAgICAgIGZpZWxkVmFsaWQgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZihmaWVsZC5vcHRpb25hbCAmJiBtb2R1bGUuaXMuYmxhbmsoJGZpZWxkKSl7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnRmllbGQgaXMgb3B0aW9uYWwgYW5kIGJsYW5rLiBTa2lwcGluZycsIGlkZW50aWZpZXIpO1xuICAgICAgICAgICAgICBmaWVsZFZhbGlkID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoZmllbGQuZGVwZW5kcyAmJiBtb2R1bGUuaXMuZW1wdHkoJGRlcGVuZHNGaWVsZCkpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdGaWVsZCBkZXBlbmRzIG9uIGFub3RoZXIgdmFsdWUgdGhhdCBpcyBub3QgcHJlc2VudCBvciBlbXB0eS4gU2tpcHBpbmcnLCAkZGVwZW5kc0ZpZWxkKTtcbiAgICAgICAgICAgICAgZmllbGRWYWxpZCA9IHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKGZpZWxkLnJ1bGVzICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgJC5lYWNoKGZpZWxkLnJ1bGVzLCBmdW5jdGlvbihpbmRleCwgcnVsZSkge1xuICAgICAgICAgICAgICAgIGlmKCBtb2R1bGUuaGFzLmZpZWxkKGlkZW50aWZpZXIpICYmICEoIG1vZHVsZS52YWxpZGF0ZS5ydWxlKGZpZWxkLCBydWxlKSApICkge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdGaWVsZCBpcyBpbnZhbGlkJywgaWRlbnRpZmllciwgcnVsZS50eXBlKTtcbiAgICAgICAgICAgICAgICAgIGZpZWxkRXJyb3JzLnB1c2gobW9kdWxlLmdldC5wcm9tcHQocnVsZSwgZmllbGQpKTtcbiAgICAgICAgICAgICAgICAgIGZpZWxkVmFsaWQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoZmllbGRWYWxpZCkge1xuICAgICAgICAgICAgICBtb2R1bGUucmVtb3ZlLnByb21wdChpZGVudGlmaWVyLCBmaWVsZEVycm9ycyk7XG4gICAgICAgICAgICAgIHNldHRpbmdzLm9uVmFsaWQuY2FsbCgkZmllbGQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIGZvcm1FcnJvcnMgPSBmb3JtRXJyb3JzLmNvbmNhdChmaWVsZEVycm9ycyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5hZGQucHJvbXB0KGlkZW50aWZpZXIsIGZpZWxkRXJyb3JzKTtcbiAgICAgICAgICAgICAgc2V0dGluZ3Mub25JbnZhbGlkLmNhbGwoJGZpZWxkLCBmaWVsZEVycm9ycyk7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH0sXG5cbiAgICAgICAgICAvLyB0YWtlcyB2YWxpZGF0aW9uIHJ1bGUgYW5kIHJldHVybnMgd2hldGhlciBmaWVsZCBwYXNzZXMgcnVsZVxuICAgICAgICAgIHJ1bGU6IGZ1bmN0aW9uKGZpZWxkLCBydWxlKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgJGZpZWxkICAgICAgID0gbW9kdWxlLmdldC5maWVsZChmaWVsZC5pZGVudGlmaWVyKSxcbiAgICAgICAgICAgICAgdHlwZSAgICAgICAgID0gcnVsZS50eXBlLFxuICAgICAgICAgICAgICB2YWx1ZSAgICAgICAgPSAkZmllbGQudmFsKCksXG4gICAgICAgICAgICAgIGlzVmFsaWQgICAgICA9IHRydWUsXG4gICAgICAgICAgICAgIGFuY2lsbGFyeSAgICA9IG1vZHVsZS5nZXQuYW5jaWxsYXJ5VmFsdWUocnVsZSksXG4gICAgICAgICAgICAgIHJ1bGVOYW1lICAgICA9IG1vZHVsZS5nZXQucnVsZU5hbWUocnVsZSksXG4gICAgICAgICAgICAgIHJ1bGVGdW5jdGlvbiA9IHNldHRpbmdzLnJ1bGVzW3J1bGVOYW1lXVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoICEkLmlzRnVuY3Rpb24ocnVsZUZ1bmN0aW9uKSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm5vUnVsZSwgcnVsZU5hbWUpO1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBjYXN0IHRvIHN0cmluZyBhdm9pZGluZyBlbmNvZGluZyBzcGVjaWFsIHZhbHVlc1xuICAgICAgICAgICAgdmFsdWUgPSAodmFsdWUgPT09IHVuZGVmaW5lZCB8fCB2YWx1ZSA9PT0gJycgfHwgdmFsdWUgPT09IG51bGwpXG4gICAgICAgICAgICAgID8gJydcbiAgICAgICAgICAgICAgOiAkLnRyaW0odmFsdWUgKyAnJylcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHJldHVybiBydWxlRnVuY3Rpb24uY2FsbCgkZmllbGQsIHZhbHVlLCBhbmNpbGxhcnkpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZXR0aW5nOiBmdW5jdGlvbihuYW1lLCB2YWx1ZSkge1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBzZXR0aW5ncywgbmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgc2V0dGluZ3NbbmFtZV0gPSB2YWx1ZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gc2V0dGluZ3NbbmFtZV07XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnRlcm5hbDogZnVuY3Rpb24obmFtZSwgdmFsdWUpIHtcbiAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KG5hbWUpICkge1xuICAgICAgICAgICAgJC5leHRlbmQodHJ1ZSwgbW9kdWxlLCBuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZih2YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBtb2R1bGVbbmFtZV0gPSB2YWx1ZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZGVidWc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQgJiYgc2V0dGluZ3MuZGVidWcpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLnBlcmZvcm1hbmNlKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS5sb2coYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1Zy5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdmVyYm9zZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCAmJiBzZXR0aW5ncy52ZXJib3NlICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UubG9nKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZC5jYWxsKGNvbnNvbGUuaW5mbywgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlLmFwcGx5KGNvbnNvbGUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBlcnJvcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYoIXNldHRpbmdzLnNpbGVudCkge1xuICAgICAgICAgICAgbW9kdWxlLmVycm9yID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmVycm9yLCBjb25zb2xlLCBzZXR0aW5ncy5uYW1lICsgJzonKTtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvci5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgcGVyZm9ybWFuY2U6IHtcbiAgICAgICAgICBsb2c6IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICBjdXJyZW50VGltZSxcbiAgICAgICAgICAgICAgZXhlY3V0aW9uVGltZSxcbiAgICAgICAgICAgICAgcHJldmlvdXNUaW1lXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBjdXJyZW50VGltZSAgID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG4gICAgICAgICAgICAgIHByZXZpb3VzVGltZSAgPSB0aW1lIHx8IGN1cnJlbnRUaW1lO1xuICAgICAgICAgICAgICBleGVjdXRpb25UaW1lID0gY3VycmVudFRpbWUgLSBwcmV2aW91c1RpbWU7XG4gICAgICAgICAgICAgIHRpbWUgICAgICAgICAgPSBjdXJyZW50VGltZTtcbiAgICAgICAgICAgICAgcGVyZm9ybWFuY2UucHVzaCh7XG4gICAgICAgICAgICAgICAgJ05hbWUnICAgICAgICAgICA6IG1lc3NhZ2VbMF0sXG4gICAgICAgICAgICAgICAgJ0FyZ3VtZW50cycgICAgICA6IFtdLnNsaWNlLmNhbGwobWVzc2FnZSwgMSkgfHwgJycsXG4gICAgICAgICAgICAgICAgJ0VsZW1lbnQnICAgICAgICA6IGVsZW1lbnQsXG4gICAgICAgICAgICAgICAgJ0V4ZWN1dGlvbiBUaW1lJyA6IGV4ZWN1dGlvblRpbWVcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lciA9IHNldFRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLmRpc3BsYXksIDUwMCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXNwbGF5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICB0aXRsZSA9IHNldHRpbmdzLm5hbWUgKyAnOicsXG4gICAgICAgICAgICAgIHRvdGFsVGltZSA9IDBcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHRpbWUgPSBmYWxzZTtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICB0b3RhbFRpbWUgKz0gZGF0YVsnRXhlY3V0aW9uIFRpbWUnXTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgdGl0bGUgKz0gJyAnICsgdG90YWxUaW1lICsgJ21zJztcbiAgICAgICAgICAgIGlmKG1vZHVsZVNlbGVjdG9yKSB7XG4gICAgICAgICAgICAgIHRpdGxlICs9ICcgXFwnJyArIG1vZHVsZVNlbGVjdG9yICsgJ1xcJyc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZigkYWxsTW9kdWxlcy5sZW5ndGggPiAxKSB7XG4gICAgICAgICAgICAgIHRpdGxlICs9ICcgJyArICcoJyArICRhbGxNb2R1bGVzLmxlbmd0aCArICcpJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKCAoY29uc29sZS5ncm91cCAhPT0gdW5kZWZpbmVkIHx8IGNvbnNvbGUudGFibGUgIT09IHVuZGVmaW5lZCkgJiYgcGVyZm9ybWFuY2UubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKHRpdGxlKTtcbiAgICAgICAgICAgICAgaWYoY29uc29sZS50YWJsZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUudGFibGUocGVyZm9ybWFuY2UpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICQuZWFjaChwZXJmb3JtYW5jZSwgZnVuY3Rpb24oaW5kZXgsIGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGFbJ05hbWUnXSArICc6ICcgKyBkYXRhWydFeGVjdXRpb24gVGltZSddKydtcycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHBlcmZvcm1hbmNlID0gW107XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBpbnZva2U6IGZ1bmN0aW9uKHF1ZXJ5LCBwYXNzZWRBcmd1bWVudHMsIGNvbnRleHQpIHtcbiAgICAgICAgICB2YXJcbiAgICAgICAgICAgIG9iamVjdCA9IGluc3RhbmNlLFxuICAgICAgICAgICAgbWF4RGVwdGgsXG4gICAgICAgICAgICBmb3VuZCxcbiAgICAgICAgICAgIHJlc3BvbnNlXG4gICAgICAgICAgO1xuICAgICAgICAgIHBhc3NlZEFyZ3VtZW50cyA9IHBhc3NlZEFyZ3VtZW50cyB8fCBxdWVyeUFyZ3VtZW50cztcbiAgICAgICAgICBjb250ZXh0ICAgICAgICAgPSBlbGVtZW50ICAgICAgICAgfHwgY29udGV4dDtcbiAgICAgICAgICBpZih0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycgJiYgb2JqZWN0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHF1ZXJ5ICAgID0gcXVlcnkuc3BsaXQoL1tcXC4gXS8pO1xuICAgICAgICAgICAgbWF4RGVwdGggPSBxdWVyeS5sZW5ndGggLSAxO1xuICAgICAgICAgICAgJC5lYWNoKHF1ZXJ5LCBmdW5jdGlvbihkZXB0aCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgdmFyIGNhbWVsQ2FzZVZhbHVlID0gKGRlcHRoICE9IG1heERlcHRoKVxuICAgICAgICAgICAgICAgID8gdmFsdWUgKyBxdWVyeVtkZXB0aCArIDFdLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcXVlcnlbZGVwdGggKyAxXS5zbGljZSgxKVxuICAgICAgICAgICAgICAgIDogcXVlcnlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICBpZiggJC5pc1BsYWluT2JqZWN0KCBvYmplY3RbY2FtZWxDYXNlVmFsdWVdICkgJiYgKGRlcHRoICE9IG1heERlcHRoKSApIHtcbiAgICAgICAgICAgICAgICBvYmplY3QgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoIG9iamVjdFtjYW1lbENhc2VWYWx1ZV0gIT09IHVuZGVmaW5lZCApIHtcbiAgICAgICAgICAgICAgICBmb3VuZCA9IG9iamVjdFtjYW1lbENhc2VWYWx1ZV07XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoICQuaXNQbGFpbk9iamVjdCggb2JqZWN0W3ZhbHVlXSApICYmIChkZXB0aCAhPSBtYXhEZXB0aCkgKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBvYmplY3RbdmFsdWVdICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbdmFsdWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiggJC5pc0Z1bmN0aW9uKCBmb3VuZCApICkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZC5hcHBseShjb250ZXh0LCBwYXNzZWRBcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKGZvdW5kICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gZm91bmQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKCQuaXNBcnJheShyZXR1cm5lZFZhbHVlKSkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZS5wdXNoKHJlc3BvbnNlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSBbcmV0dXJuZWRWYWx1ZSwgcmVzcG9uc2VdO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJlc3BvbnNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSByZXNwb25zZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGZvdW5kO1xuICAgICAgICB9XG4gICAgICB9O1xuICAgICAgbW9kdWxlLmluaXRpYWxpemUoKTtcbiAgICB9KVxuICA7XG5cbiAgcmV0dXJuIChyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpXG4gICAgPyByZXR1cm5lZFZhbHVlXG4gICAgOiB0aGlzXG4gIDtcbn07XG5cbiQuZm4uZm9ybS5zZXR0aW5ncyA9IHtcblxuICBuYW1lICAgICAgICAgICAgICA6ICdGb3JtJyxcbiAgbmFtZXNwYWNlICAgICAgICAgOiAnZm9ybScsXG5cbiAgZGVidWcgICAgICAgICAgICAgOiBmYWxzZSxcbiAgdmVyYm9zZSAgICAgICAgICAgOiBmYWxzZSxcbiAgcGVyZm9ybWFuY2UgICAgICAgOiB0cnVlLFxuXG4gIGZpZWxkcyAgICAgICAgICAgIDogZmFsc2UsXG5cbiAga2V5Ym9hcmRTaG9ydGN1dHMgOiB0cnVlLFxuICBvbiAgICAgICAgICAgICAgICA6ICdzdWJtaXQnLFxuICBpbmxpbmUgICAgICAgICAgICA6IGZhbHNlLFxuXG4gIGRlbGF5ICAgICAgICAgICAgIDogMjAwLFxuICByZXZhbGlkYXRlICAgICAgICA6IHRydWUsXG5cbiAgdHJhbnNpdGlvbiAgICAgICAgOiAnc2NhbGUnLFxuICBkdXJhdGlvbiAgICAgICAgICA6IDIwMCxcblxuICBvblZhbGlkICAgICAgICAgICA6IGZ1bmN0aW9uKCkge30sXG4gIG9uSW52YWxpZCAgICAgICAgIDogZnVuY3Rpb24oKSB7fSxcbiAgb25TdWNjZXNzICAgICAgICAgOiBmdW5jdGlvbigpIHsgcmV0dXJuIHRydWU7IH0sXG4gIG9uRmFpbHVyZSAgICAgICAgIDogZnVuY3Rpb24oKSB7IHJldHVybiBmYWxzZTsgfSxcblxuICBtZXRhZGF0YSA6IHtcbiAgICBkZWZhdWx0VmFsdWUgOiAnZGVmYXVsdCcsXG4gICAgdmFsaWRhdGUgICAgIDogJ3ZhbGlkYXRlJ1xuICB9LFxuXG4gIHJlZ0V4cDoge1xuICAgIGJyYWNrZXQgOiAvXFxbKC4qKVxcXS9pLFxuICAgIGRlY2ltYWwgOiAvXlxcZCooXFwuKVxcZCsvLFxuICAgIGVtYWlsICAgOiAvXlthLXowLTkhIyQlJicqK1xcLz0/Xl9ge3x9fi4tXStAW2EtejAtOV0oW2EtejAtOS1dKlthLXowLTldKT8oXFwuW2EtejAtOV0oW2EtejAtOS1dKlthLXowLTldKT8pKiQvaSxcbiAgICBlc2NhcGUgIDogL1tcXC1cXFtcXF1cXC9cXHtcXH1cXChcXClcXCpcXCtcXD9cXC5cXFxcXFxeXFwkXFx8XS9nLFxuICAgIGZsYWdzICAgOiAvXlxcLyguKilcXC8oLiopPy8sXG4gICAgaW50ZWdlciA6IC9eXFwtP1xcZCskLyxcbiAgICBudW1iZXIgIDogL15cXC0/XFxkKihcXC5cXGQrKT8kLyxcbiAgICB1cmwgICAgIDogLyhodHRwcz86XFwvXFwvKD86d3d3XFwufCg/IXd3dykpW15cXHNcXC5dK1xcLlteXFxzXXsyLH18d3d3XFwuW15cXHNdK1xcLlteXFxzXXsyLH0pL2lcbiAgfSxcblxuICB0ZXh0OiB7XG4gICAgdW5zcGVjaWZpZWRSdWxlICA6ICdQbGVhc2UgZW50ZXIgYSB2YWxpZCB2YWx1ZScsXG4gICAgdW5zcGVjaWZpZWRGaWVsZCA6ICdUaGlzIGZpZWxkJ1xuICB9LFxuXG4gIHByb21wdDoge1xuICAgIGVtcHR5ICAgICAgICAgICAgICAgIDogJ3tuYW1lfSBtdXN0IGhhdmUgYSB2YWx1ZScsXG4gICAgY2hlY2tlZCAgICAgICAgICAgICAgOiAne25hbWV9IG11c3QgYmUgY2hlY2tlZCcsXG4gICAgZW1haWwgICAgICAgICAgICAgICAgOiAne25hbWV9IG11c3QgYmUgYSB2YWxpZCBlLW1haWwnLFxuICAgIHVybCAgICAgICAgICAgICAgICAgIDogJ3tuYW1lfSBtdXN0IGJlIGEgdmFsaWQgdXJsJyxcbiAgICByZWdFeHAgICAgICAgICAgICAgICA6ICd7bmFtZX0gaXMgbm90IGZvcm1hdHRlZCBjb3JyZWN0bHknLFxuICAgIGludGVnZXIgICAgICAgICAgICAgIDogJ3tuYW1lfSBtdXN0IGJlIGFuIGludGVnZXInLFxuICAgIGRlY2ltYWwgICAgICAgICAgICAgIDogJ3tuYW1lfSBtdXN0IGJlIGEgZGVjaW1hbCBudW1iZXInLFxuICAgIG51bWJlciAgICAgICAgICAgICAgIDogJ3tuYW1lfSBtdXN0IGJlIHNldCB0byBhIG51bWJlcicsXG4gICAgaXMgICAgICAgICAgICAgICAgICAgOiAne25hbWV9IG11c3QgYmUgXCJ7cnVsZVZhbHVlfVwiJyxcbiAgICBpc0V4YWN0bHkgICAgICAgICAgICA6ICd7bmFtZX0gbXVzdCBiZSBleGFjdGx5IFwie3J1bGVWYWx1ZX1cIicsXG4gICAgbm90ICAgICAgICAgICAgICAgICAgOiAne25hbWV9IGNhbm5vdCBiZSBzZXQgdG8gXCJ7cnVsZVZhbHVlfVwiJyxcbiAgICBub3RFeGFjdGx5ICAgICAgICAgICA6ICd7bmFtZX0gY2Fubm90IGJlIHNldCB0byBleGFjdGx5IFwie3J1bGVWYWx1ZX1cIicsXG4gICAgY29udGFpbiAgICAgICAgICAgICAgOiAne25hbWV9IGNhbm5vdCBjb250YWluIFwie3J1bGVWYWx1ZX1cIicsXG4gICAgY29udGFpbkV4YWN0bHkgICAgICAgOiAne25hbWV9IGNhbm5vdCBjb250YWluIGV4YWN0bHkgXCJ7cnVsZVZhbHVlfVwiJyxcbiAgICBkb2VzbnRDb250YWluICAgICAgICA6ICd7bmFtZX0gbXVzdCBjb250YWluICBcIntydWxlVmFsdWV9XCInLFxuICAgIGRvZXNudENvbnRhaW5FeGFjdGx5IDogJ3tuYW1lfSBtdXN0IGNvbnRhaW4gZXhhY3RseSBcIntydWxlVmFsdWV9XCInLFxuICAgIG1pbkxlbmd0aCAgICAgICAgICAgIDogJ3tuYW1lfSBtdXN0IGJlIGF0IGxlYXN0IHtydWxlVmFsdWV9IGNoYXJhY3RlcnMnLFxuICAgIGxlbmd0aCAgICAgICAgICAgICAgIDogJ3tuYW1lfSBtdXN0IGJlIGF0IGxlYXN0IHtydWxlVmFsdWV9IGNoYXJhY3RlcnMnLFxuICAgIGV4YWN0TGVuZ3RoICAgICAgICAgIDogJ3tuYW1lfSBtdXN0IGJlIGV4YWN0bHkge3J1bGVWYWx1ZX0gY2hhcmFjdGVycycsXG4gICAgbWF4TGVuZ3RoICAgICAgICAgICAgOiAne25hbWV9IGNhbm5vdCBiZSBsb25nZXIgdGhhbiB7cnVsZVZhbHVlfSBjaGFyYWN0ZXJzJyxcbiAgICBtYXRjaCAgICAgICAgICAgICAgICA6ICd7bmFtZX0gbXVzdCBtYXRjaCB7cnVsZVZhbHVlfSBmaWVsZCcsXG4gICAgZGlmZmVyZW50ICAgICAgICAgICAgOiAne25hbWV9IG11c3QgaGF2ZSBhIGRpZmZlcmVudCB2YWx1ZSB0aGFuIHtydWxlVmFsdWV9IGZpZWxkJyxcbiAgICBjcmVkaXRDYXJkICAgICAgICAgICA6ICd7bmFtZX0gbXVzdCBiZSBhIHZhbGlkIGNyZWRpdCBjYXJkIG51bWJlcicsXG4gICAgbWluQ291bnQgICAgICAgICAgICAgOiAne25hbWV9IG11c3QgaGF2ZSBhdCBsZWFzdCB7cnVsZVZhbHVlfSBjaG9pY2VzJyxcbiAgICBleGFjdENvdW50ICAgICAgICAgICA6ICd7bmFtZX0gbXVzdCBoYXZlIGV4YWN0bHkge3J1bGVWYWx1ZX0gY2hvaWNlcycsXG4gICAgbWF4Q291bnQgICAgICAgICAgICAgOiAne25hbWV9IG11c3QgaGF2ZSB7cnVsZVZhbHVlfSBvciBsZXNzIGNob2ljZXMnXG4gIH0sXG5cbiAgc2VsZWN0b3IgOiB7XG4gICAgY2hlY2tib3ggICA6ICdpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0sIGlucHV0W3R5cGU9XCJyYWRpb1wiXScsXG4gICAgY2xlYXIgICAgICA6ICcuY2xlYXInLFxuICAgIGZpZWxkICAgICAgOiAnaW5wdXQsIHRleHRhcmVhLCBzZWxlY3QnLFxuICAgIGdyb3VwICAgICAgOiAnLmZpZWxkJyxcbiAgICBpbnB1dCAgICAgIDogJ2lucHV0JyxcbiAgICBtZXNzYWdlICAgIDogJy5lcnJvci5tZXNzYWdlJyxcbiAgICBwcm9tcHQgICAgIDogJy5wcm9tcHQubGFiZWwnLFxuICAgIHJhZGlvICAgICAgOiAnaW5wdXRbdHlwZT1cInJhZGlvXCJdJyxcbiAgICByZXNldCAgICAgIDogJy5yZXNldDpub3QoW3R5cGU9XCJyZXNldFwiXSknLFxuICAgIHN1Ym1pdCAgICAgOiAnLnN1Ym1pdDpub3QoW3R5cGU9XCJzdWJtaXRcIl0pJyxcbiAgICB1aUNoZWNrYm94IDogJy51aS5jaGVja2JveCcsXG4gICAgdWlEcm9wZG93biA6ICcudWkuZHJvcGRvd24nXG4gIH0sXG5cbiAgY2xhc3NOYW1lIDoge1xuICAgIGVycm9yICAgOiAnZXJyb3InLFxuICAgIGxhYmVsICAgOiAndWkgcHJvbXB0IGxhYmVsJyxcbiAgICBwcmVzc2VkIDogJ2Rvd24nLFxuICAgIHN1Y2Nlc3MgOiAnc3VjY2VzcydcbiAgfSxcblxuICBlcnJvcjoge1xuICAgIGlkZW50aWZpZXIgOiAnWW91IG11c3Qgc3BlY2lmeSBhIHN0cmluZyBpZGVudGlmaWVyIGZvciBlYWNoIGZpZWxkJyxcbiAgICBtZXRob2QgICAgIDogJ1RoZSBtZXRob2QgeW91IGNhbGxlZCBpcyBub3QgZGVmaW5lZC4nLFxuICAgIG5vUnVsZSAgICAgOiAnVGhlcmUgaXMgbm8gcnVsZSBtYXRjaGluZyB0aGUgb25lIHlvdSBzcGVjaWZpZWQnLFxuICAgIG9sZFN5bnRheCAgOiAnU3RhcnRpbmcgaW4gMi4wIGZvcm1zIG5vdyBvbmx5IHRha2UgYSBzaW5nbGUgc2V0dGluZ3Mgb2JqZWN0LiBWYWxpZGF0aW9uIHNldHRpbmdzIGNvbnZlcnRlZCB0byBuZXcgc3ludGF4IGF1dG9tYXRpY2FsbHkuJ1xuICB9LFxuXG4gIHRlbXBsYXRlczoge1xuXG4gICAgLy8gdGVtcGxhdGUgdGhhdCBwcm9kdWNlcyBlcnJvciBtZXNzYWdlXG4gICAgZXJyb3I6IGZ1bmN0aW9uKGVycm9ycykge1xuICAgICAgdmFyXG4gICAgICAgIGh0bWwgPSAnPHVsIGNsYXNzPVwibGlzdFwiPidcbiAgICAgIDtcbiAgICAgICQuZWFjaChlcnJvcnMsIGZ1bmN0aW9uKGluZGV4LCB2YWx1ZSkge1xuICAgICAgICBodG1sICs9ICc8bGk+JyArIHZhbHVlICsgJzwvbGk+JztcbiAgICAgIH0pO1xuICAgICAgaHRtbCArPSAnPC91bD4nO1xuICAgICAgcmV0dXJuICQoaHRtbCk7XG4gICAgfSxcblxuICAgIC8vIHRlbXBsYXRlIHRoYXQgcHJvZHVjZXMgbGFiZWxcbiAgICBwcm9tcHQ6IGZ1bmN0aW9uKGVycm9ycykge1xuICAgICAgcmV0dXJuICQoJzxkaXYvPicpXG4gICAgICAgIC5hZGRDbGFzcygndWkgYmFzaWMgcmVkIHBvaW50aW5nIHByb21wdCBsYWJlbCcpXG4gICAgICAgIC5odG1sKGVycm9yc1swXSlcbiAgICAgIDtcbiAgICB9XG4gIH0sXG5cbiAgcnVsZXM6IHtcblxuICAgIC8vIGlzIG5vdCBlbXB0eSBvciBibGFuayBzdHJpbmdcbiAgICBlbXB0eTogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgIHJldHVybiAhKHZhbHVlID09PSB1bmRlZmluZWQgfHwgJycgPT09IHZhbHVlIHx8ICQuaXNBcnJheSh2YWx1ZSkgJiYgdmFsdWUubGVuZ3RoID09PSAwKTtcbiAgICB9LFxuXG4gICAgLy8gY2hlY2tib3ggY2hlY2tlZFxuICAgIGNoZWNrZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuICgkKHRoaXMpLmZpbHRlcignOmNoZWNrZWQnKS5sZW5ndGggPiAwKTtcbiAgICB9LFxuXG4gICAgLy8gaXMgbW9zdCBsaWtlbHkgYW4gZW1haWxcbiAgICBlbWFpbDogZnVuY3Rpb24odmFsdWUpe1xuICAgICAgcmV0dXJuICQuZm4uZm9ybS5zZXR0aW5ncy5yZWdFeHAuZW1haWwudGVzdCh2YWx1ZSk7XG4gICAgfSxcblxuICAgIC8vIHZhbHVlIGlzIG1vc3QgbGlrZWx5IHVybFxuICAgIHVybDogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgIHJldHVybiAkLmZuLmZvcm0uc2V0dGluZ3MucmVnRXhwLnVybC50ZXN0KHZhbHVlKTtcbiAgICB9LFxuXG4gICAgLy8gbWF0Y2hlcyBzcGVjaWZpZWQgcmVnRXhwXG4gICAgcmVnRXhwOiBmdW5jdGlvbih2YWx1ZSwgcmVnRXhwKSB7XG4gICAgICB2YXJcbiAgICAgICAgcmVnRXhwUGFydHMgPSByZWdFeHAubWF0Y2goJC5mbi5mb3JtLnNldHRpbmdzLnJlZ0V4cC5mbGFncyksXG4gICAgICAgIGZsYWdzXG4gICAgICA7XG4gICAgICAvLyByZWd1bGFyIGV4cHJlc3Npb24gc3BlY2lmaWVkIGFzIC9iYXovZ2kgKGZsYWdzKVxuICAgICAgaWYocmVnRXhwUGFydHMpIHtcbiAgICAgICAgcmVnRXhwID0gKHJlZ0V4cFBhcnRzLmxlbmd0aCA+PSAyKVxuICAgICAgICAgID8gcmVnRXhwUGFydHNbMV1cbiAgICAgICAgICA6IHJlZ0V4cFxuICAgICAgICA7XG4gICAgICAgIGZsYWdzID0gKHJlZ0V4cFBhcnRzLmxlbmd0aCA+PSAzKVxuICAgICAgICAgID8gcmVnRXhwUGFydHNbMl1cbiAgICAgICAgICA6ICcnXG4gICAgICAgIDtcbiAgICAgIH1cbiAgICAgIHJldHVybiB2YWx1ZS5tYXRjaCggbmV3IFJlZ0V4cChyZWdFeHAsIGZsYWdzKSApO1xuICAgIH0sXG5cbiAgICAvLyBpcyB2YWxpZCBpbnRlZ2VyIG9yIG1hdGNoZXMgcmFuZ2VcbiAgICBpbnRlZ2VyOiBmdW5jdGlvbih2YWx1ZSwgcmFuZ2UpIHtcbiAgICAgIHZhclxuICAgICAgICBpbnRSZWdFeHAgPSAkLmZuLmZvcm0uc2V0dGluZ3MucmVnRXhwLmludGVnZXIsXG4gICAgICAgIG1pbixcbiAgICAgICAgbWF4LFxuICAgICAgICBwYXJ0c1xuICAgICAgO1xuICAgICAgaWYoICFyYW5nZSB8fCBbJycsICcuLiddLmluZGV4T2YocmFuZ2UpICE9PSAtMSkge1xuICAgICAgICAvLyBkbyBub3RoaW5nXG4gICAgICB9XG4gICAgICBlbHNlIGlmKHJhbmdlLmluZGV4T2YoJy4uJykgPT0gLTEpIHtcbiAgICAgICAgaWYoaW50UmVnRXhwLnRlc3QocmFuZ2UpKSB7XG4gICAgICAgICAgbWluID0gbWF4ID0gcmFuZ2UgLSAwO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgcGFydHMgPSByYW5nZS5zcGxpdCgnLi4nLCAyKTtcbiAgICAgICAgaWYoaW50UmVnRXhwLnRlc3QocGFydHNbMF0pKSB7XG4gICAgICAgICAgbWluID0gcGFydHNbMF0gLSAwO1xuICAgICAgICB9XG4gICAgICAgIGlmKGludFJlZ0V4cC50ZXN0KHBhcnRzWzFdKSkge1xuICAgICAgICAgIG1heCA9IHBhcnRzWzFdIC0gMDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIChcbiAgICAgICAgaW50UmVnRXhwLnRlc3QodmFsdWUpICYmXG4gICAgICAgIChtaW4gPT09IHVuZGVmaW5lZCB8fCB2YWx1ZSA+PSBtaW4pICYmXG4gICAgICAgIChtYXggPT09IHVuZGVmaW5lZCB8fCB2YWx1ZSA8PSBtYXgpXG4gICAgICApO1xuICAgIH0sXG5cbiAgICAvLyBpcyB2YWxpZCBudW1iZXIgKHdpdGggZGVjaW1hbClcbiAgICBkZWNpbWFsOiBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgcmV0dXJuICQuZm4uZm9ybS5zZXR0aW5ncy5yZWdFeHAuZGVjaW1hbC50ZXN0KHZhbHVlKTtcbiAgICB9LFxuXG4gICAgLy8gaXMgdmFsaWQgbnVtYmVyXG4gICAgbnVtYmVyOiBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgcmV0dXJuICQuZm4uZm9ybS5zZXR0aW5ncy5yZWdFeHAubnVtYmVyLnRlc3QodmFsdWUpO1xuICAgIH0sXG5cbiAgICAvLyBpcyB2YWx1ZSAoY2FzZSBpbnNlbnNpdGl2ZSlcbiAgICBpczogZnVuY3Rpb24odmFsdWUsIHRleHQpIHtcbiAgICAgIHRleHQgPSAodHlwZW9mIHRleHQgPT0gJ3N0cmluZycpXG4gICAgICAgID8gdGV4dC50b0xvd2VyQ2FzZSgpXG4gICAgICAgIDogdGV4dFxuICAgICAgO1xuICAgICAgdmFsdWUgPSAodHlwZW9mIHZhbHVlID09ICdzdHJpbmcnKVxuICAgICAgICA/IHZhbHVlLnRvTG93ZXJDYXNlKClcbiAgICAgICAgOiB2YWx1ZVxuICAgICAgO1xuICAgICAgcmV0dXJuICh2YWx1ZSA9PSB0ZXh0KTtcbiAgICB9LFxuXG4gICAgLy8gaXMgdmFsdWVcbiAgICBpc0V4YWN0bHk6IGZ1bmN0aW9uKHZhbHVlLCB0ZXh0KSB7XG4gICAgICByZXR1cm4gKHZhbHVlID09IHRleHQpO1xuICAgIH0sXG5cbiAgICAvLyB2YWx1ZSBpcyBub3QgYW5vdGhlciB2YWx1ZSAoY2FzZSBpbnNlbnNpdGl2ZSlcbiAgICBub3Q6IGZ1bmN0aW9uKHZhbHVlLCBub3RWYWx1ZSkge1xuICAgICAgdmFsdWUgPSAodHlwZW9mIHZhbHVlID09ICdzdHJpbmcnKVxuICAgICAgICA/IHZhbHVlLnRvTG93ZXJDYXNlKClcbiAgICAgICAgOiB2YWx1ZVxuICAgICAgO1xuICAgICAgbm90VmFsdWUgPSAodHlwZW9mIG5vdFZhbHVlID09ICdzdHJpbmcnKVxuICAgICAgICA/IG5vdFZhbHVlLnRvTG93ZXJDYXNlKClcbiAgICAgICAgOiBub3RWYWx1ZVxuICAgICAgO1xuICAgICAgcmV0dXJuICh2YWx1ZSAhPSBub3RWYWx1ZSk7XG4gICAgfSxcblxuICAgIC8vIHZhbHVlIGlzIG5vdCBhbm90aGVyIHZhbHVlIChjYXNlIHNlbnNpdGl2ZSlcbiAgICBub3RFeGFjdGx5OiBmdW5jdGlvbih2YWx1ZSwgbm90VmFsdWUpIHtcbiAgICAgIHJldHVybiAodmFsdWUgIT0gbm90VmFsdWUpO1xuICAgIH0sXG5cbiAgICAvLyB2YWx1ZSBjb250YWlucyB0ZXh0IChpbnNlbnNpdGl2ZSlcbiAgICBjb250YWluczogZnVuY3Rpb24odmFsdWUsIHRleHQpIHtcbiAgICAgIC8vIGVzY2FwZSByZWdleCBjaGFyYWN0ZXJzXG4gICAgICB0ZXh0ID0gdGV4dC5yZXBsYWNlKCQuZm4uZm9ybS5zZXR0aW5ncy5yZWdFeHAuZXNjYXBlLCBcIlxcXFwkJlwiKTtcbiAgICAgIHJldHVybiAodmFsdWUuc2VhcmNoKCBuZXcgUmVnRXhwKHRleHQsICdpJykgKSAhPT0gLTEpO1xuICAgIH0sXG5cbiAgICAvLyB2YWx1ZSBjb250YWlucyB0ZXh0IChjYXNlIHNlbnNpdGl2ZSlcbiAgICBjb250YWluc0V4YWN0bHk6IGZ1bmN0aW9uKHZhbHVlLCB0ZXh0KSB7XG4gICAgICAvLyBlc2NhcGUgcmVnZXggY2hhcmFjdGVyc1xuICAgICAgdGV4dCA9IHRleHQucmVwbGFjZSgkLmZuLmZvcm0uc2V0dGluZ3MucmVnRXhwLmVzY2FwZSwgXCJcXFxcJCZcIik7XG4gICAgICByZXR1cm4gKHZhbHVlLnNlYXJjaCggbmV3IFJlZ0V4cCh0ZXh0KSApICE9PSAtMSk7XG4gICAgfSxcblxuICAgIC8vIHZhbHVlIGNvbnRhaW5zIHRleHQgKGluc2Vuc2l0aXZlKVxuICAgIGRvZXNudENvbnRhaW46IGZ1bmN0aW9uKHZhbHVlLCB0ZXh0KSB7XG4gICAgICAvLyBlc2NhcGUgcmVnZXggY2hhcmFjdGVyc1xuICAgICAgdGV4dCA9IHRleHQucmVwbGFjZSgkLmZuLmZvcm0uc2V0dGluZ3MucmVnRXhwLmVzY2FwZSwgXCJcXFxcJCZcIik7XG4gICAgICByZXR1cm4gKHZhbHVlLnNlYXJjaCggbmV3IFJlZ0V4cCh0ZXh0LCAnaScpICkgPT09IC0xKTtcbiAgICB9LFxuXG4gICAgLy8gdmFsdWUgY29udGFpbnMgdGV4dCAoY2FzZSBzZW5zaXRpdmUpXG4gICAgZG9lc250Q29udGFpbkV4YWN0bHk6IGZ1bmN0aW9uKHZhbHVlLCB0ZXh0KSB7XG4gICAgICAvLyBlc2NhcGUgcmVnZXggY2hhcmFjdGVyc1xuICAgICAgdGV4dCA9IHRleHQucmVwbGFjZSgkLmZuLmZvcm0uc2V0dGluZ3MucmVnRXhwLmVzY2FwZSwgXCJcXFxcJCZcIik7XG4gICAgICByZXR1cm4gKHZhbHVlLnNlYXJjaCggbmV3IFJlZ0V4cCh0ZXh0KSApID09PSAtMSk7XG4gICAgfSxcblxuICAgIC8vIGlzIGF0IGxlYXN0IHN0cmluZyBsZW5ndGhcbiAgICBtaW5MZW5ndGg6IGZ1bmN0aW9uKHZhbHVlLCByZXF1aXJlZExlbmd0aCkge1xuICAgICAgcmV0dXJuICh2YWx1ZSAhPT0gdW5kZWZpbmVkKVxuICAgICAgICA/ICh2YWx1ZS5sZW5ndGggPj0gcmVxdWlyZWRMZW5ndGgpXG4gICAgICAgIDogZmFsc2VcbiAgICAgIDtcbiAgICB9LFxuXG4gICAgLy8gc2VlIHJscyBub3RlcyBmb3IgMi4wLjYgKHRoaXMgaXMgYSBkdXBsaWNhdGUgb2YgbWluTGVuZ3RoKVxuICAgIGxlbmd0aDogZnVuY3Rpb24odmFsdWUsIHJlcXVpcmVkTGVuZ3RoKSB7XG4gICAgICByZXR1cm4gKHZhbHVlICE9PSB1bmRlZmluZWQpXG4gICAgICAgID8gKHZhbHVlLmxlbmd0aCA+PSByZXF1aXJlZExlbmd0aClcbiAgICAgICAgOiBmYWxzZVxuICAgICAgO1xuICAgIH0sXG5cbiAgICAvLyBpcyBleGFjdGx5IGxlbmd0aFxuICAgIGV4YWN0TGVuZ3RoOiBmdW5jdGlvbih2YWx1ZSwgcmVxdWlyZWRMZW5ndGgpIHtcbiAgICAgIHJldHVybiAodmFsdWUgIT09IHVuZGVmaW5lZClcbiAgICAgICAgPyAodmFsdWUubGVuZ3RoID09IHJlcXVpcmVkTGVuZ3RoKVxuICAgICAgICA6IGZhbHNlXG4gICAgICA7XG4gICAgfSxcblxuICAgIC8vIGlzIGxlc3MgdGhhbiBsZW5ndGhcbiAgICBtYXhMZW5ndGg6IGZ1bmN0aW9uKHZhbHVlLCBtYXhMZW5ndGgpIHtcbiAgICAgIHJldHVybiAodmFsdWUgIT09IHVuZGVmaW5lZClcbiAgICAgICAgPyAodmFsdWUubGVuZ3RoIDw9IG1heExlbmd0aClcbiAgICAgICAgOiBmYWxzZVxuICAgICAgO1xuICAgIH0sXG5cbiAgICAvLyBtYXRjaGVzIGFub3RoZXIgZmllbGRcbiAgICBtYXRjaDogZnVuY3Rpb24odmFsdWUsIGlkZW50aWZpZXIpIHtcbiAgICAgIHZhclxuICAgICAgICAkZm9ybSA9ICQodGhpcyksXG4gICAgICAgIG1hdGNoaW5nVmFsdWVcbiAgICAgIDtcbiAgICAgIGlmKCAkKCdbZGF0YS12YWxpZGF0ZT1cIicrIGlkZW50aWZpZXIgKydcIl0nKS5sZW5ndGggPiAwICkge1xuICAgICAgICBtYXRjaGluZ1ZhbHVlID0gJCgnW2RhdGEtdmFsaWRhdGU9XCInKyBpZGVudGlmaWVyICsnXCJdJykudmFsKCk7XG4gICAgICB9XG4gICAgICBlbHNlIGlmKCQoJyMnICsgaWRlbnRpZmllcikubGVuZ3RoID4gMCkge1xuICAgICAgICBtYXRjaGluZ1ZhbHVlID0gJCgnIycgKyBpZGVudGlmaWVyKS52YWwoKTtcbiAgICAgIH1cbiAgICAgIGVsc2UgaWYoJCgnW25hbWU9XCInICsgaWRlbnRpZmllciArJ1wiXScpLmxlbmd0aCA+IDApIHtcbiAgICAgICAgbWF0Y2hpbmdWYWx1ZSA9ICQoJ1tuYW1lPVwiJyArIGlkZW50aWZpZXIgKyAnXCJdJykudmFsKCk7XG4gICAgICB9XG4gICAgICBlbHNlIGlmKCAkKCdbbmFtZT1cIicgKyBpZGVudGlmaWVyICsnW11cIl0nKS5sZW5ndGggPiAwICkge1xuICAgICAgICBtYXRjaGluZ1ZhbHVlID0gJCgnW25hbWU9XCInICsgaWRlbnRpZmllciArJ1tdXCJdJyk7XG4gICAgICB9XG4gICAgICByZXR1cm4gKG1hdGNoaW5nVmFsdWUgIT09IHVuZGVmaW5lZClcbiAgICAgICAgPyAoIHZhbHVlLnRvU3RyaW5nKCkgPT0gbWF0Y2hpbmdWYWx1ZS50b1N0cmluZygpIClcbiAgICAgICAgOiBmYWxzZVxuICAgICAgO1xuICAgIH0sXG5cbiAgICAvLyBkaWZmZXJlbnQgdGhhbiBhbm90aGVyIGZpZWxkXG4gICAgZGlmZmVyZW50OiBmdW5jdGlvbih2YWx1ZSwgaWRlbnRpZmllcikge1xuICAgICAgLy8gdXNlIGVpdGhlciBpZCBvciBuYW1lIG9mIGZpZWxkXG4gICAgICB2YXJcbiAgICAgICAgJGZvcm0gPSAkKHRoaXMpLFxuICAgICAgICBtYXRjaGluZ1ZhbHVlXG4gICAgICA7XG4gICAgICBpZiggJCgnW2RhdGEtdmFsaWRhdGU9XCInKyBpZGVudGlmaWVyICsnXCJdJykubGVuZ3RoID4gMCApIHtcbiAgICAgICAgbWF0Y2hpbmdWYWx1ZSA9ICQoJ1tkYXRhLXZhbGlkYXRlPVwiJysgaWRlbnRpZmllciArJ1wiXScpLnZhbCgpO1xuICAgICAgfVxuICAgICAgZWxzZSBpZigkKCcjJyArIGlkZW50aWZpZXIpLmxlbmd0aCA+IDApIHtcbiAgICAgICAgbWF0Y2hpbmdWYWx1ZSA9ICQoJyMnICsgaWRlbnRpZmllcikudmFsKCk7XG4gICAgICB9XG4gICAgICBlbHNlIGlmKCQoJ1tuYW1lPVwiJyArIGlkZW50aWZpZXIgKydcIl0nKS5sZW5ndGggPiAwKSB7XG4gICAgICAgIG1hdGNoaW5nVmFsdWUgPSAkKCdbbmFtZT1cIicgKyBpZGVudGlmaWVyICsgJ1wiXScpLnZhbCgpO1xuICAgICAgfVxuICAgICAgZWxzZSBpZiggJCgnW25hbWU9XCInICsgaWRlbnRpZmllciArJ1tdXCJdJykubGVuZ3RoID4gMCApIHtcbiAgICAgICAgbWF0Y2hpbmdWYWx1ZSA9ICQoJ1tuYW1lPVwiJyArIGlkZW50aWZpZXIgKydbXVwiXScpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIChtYXRjaGluZ1ZhbHVlICE9PSB1bmRlZmluZWQpXG4gICAgICAgID8gKCB2YWx1ZS50b1N0cmluZygpICE9PSBtYXRjaGluZ1ZhbHVlLnRvU3RyaW5nKCkgKVxuICAgICAgICA6IGZhbHNlXG4gICAgICA7XG4gICAgfSxcblxuICAgIGNyZWRpdENhcmQ6IGZ1bmN0aW9uKGNhcmROdW1iZXIsIGNhcmRUeXBlcykge1xuICAgICAgdmFyXG4gICAgICAgIGNhcmRzID0ge1xuICAgICAgICAgIHZpc2E6IHtcbiAgICAgICAgICAgIHBhdHRlcm4gOiAvXjQvLFxuICAgICAgICAgICAgbGVuZ3RoICA6IFsxNl1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGFtZXg6IHtcbiAgICAgICAgICAgIHBhdHRlcm4gOiAvXjNbNDddLyxcbiAgICAgICAgICAgIGxlbmd0aCAgOiBbMTVdXG4gICAgICAgICAgfSxcbiAgICAgICAgICBtYXN0ZXJjYXJkOiB7XG4gICAgICAgICAgICBwYXR0ZXJuIDogL141WzEtNV0vLFxuICAgICAgICAgICAgbGVuZ3RoICA6IFsxNl1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGRpc2NvdmVyOiB7XG4gICAgICAgICAgICBwYXR0ZXJuIDogL14oNjAxMXw2MjIoMTJbNi05XXwxWzMtOV1bMC05XXxbMi04XVswLTldezJ9fDlbMC0xXVswLTldfDkyWzAtNV18NjRbNC05XSl8NjUpLyxcbiAgICAgICAgICAgIGxlbmd0aCAgOiBbMTZdXG4gICAgICAgICAgfSxcbiAgICAgICAgICB1bmlvblBheToge1xuICAgICAgICAgICAgcGF0dGVybiA6IC9eKDYyfDg4KS8sXG4gICAgICAgICAgICBsZW5ndGggIDogWzE2LCAxNywgMTgsIDE5XVxuICAgICAgICAgIH0sXG4gICAgICAgICAgamNiOiB7XG4gICAgICAgICAgICBwYXR0ZXJuIDogL14zNSgyWzg5XXxbMy04XVswLTldKS8sXG4gICAgICAgICAgICBsZW5ndGggIDogWzE2XVxuICAgICAgICAgIH0sXG4gICAgICAgICAgbWFlc3Rybzoge1xuICAgICAgICAgICAgcGF0dGVybiA6IC9eKDUwMTh8NTAyMHw1MDM4fDYzMDR8Njc1OXw2NzZbMS0zXSkvLFxuICAgICAgICAgICAgbGVuZ3RoICA6IFsxMiwgMTMsIDE0LCAxNSwgMTYsIDE3LCAxOCwgMTldXG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaW5lcnNDbHViOiB7XG4gICAgICAgICAgICBwYXR0ZXJuIDogL14oMzBbMC01XXxeMzYpLyxcbiAgICAgICAgICAgIGxlbmd0aCAgOiBbMTRdXG4gICAgICAgICAgfSxcbiAgICAgICAgICBsYXNlcjoge1xuICAgICAgICAgICAgcGF0dGVybiA6IC9eKDYzMDR8NjcwWzY5XXw2NzcxKS8sXG4gICAgICAgICAgICBsZW5ndGggIDogWzE2LCAxNywgMTgsIDE5XVxuICAgICAgICAgIH0sXG4gICAgICAgICAgdmlzYUVsZWN0cm9uOiB7XG4gICAgICAgICAgICBwYXR0ZXJuIDogL14oNDAyNnw0MTc1MDB8NDUwOHw0ODQ0fDQ5MSgzfDcpKS8sXG4gICAgICAgICAgICBsZW5ndGggIDogWzE2XVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdmFsaWQgICAgICAgICA9IHt9LFxuICAgICAgICB2YWxpZENhcmQgICAgID0gZmFsc2UsXG4gICAgICAgIHJlcXVpcmVkVHlwZXMgPSAodHlwZW9mIGNhcmRUeXBlcyA9PSAnc3RyaW5nJylcbiAgICAgICAgICA/IGNhcmRUeXBlcy5zcGxpdCgnLCcpXG4gICAgICAgICAgOiBmYWxzZSxcbiAgICAgICAgdW5pb25QYXksXG4gICAgICAgIHZhbGlkYXRpb25cbiAgICAgIDtcblxuICAgICAgaWYodHlwZW9mIGNhcmROdW1iZXIgIT09ICdzdHJpbmcnIHx8IGNhcmROdW1iZXIubGVuZ3RoID09PSAwKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgLy8gdmVyaWZ5IGNhcmQgdHlwZXNcbiAgICAgIGlmKHJlcXVpcmVkVHlwZXMpIHtcbiAgICAgICAgJC5lYWNoKHJlcXVpcmVkVHlwZXMsIGZ1bmN0aW9uKGluZGV4LCB0eXBlKXtcbiAgICAgICAgICAvLyB2ZXJpZnkgZWFjaCBjYXJkIHR5cGVcbiAgICAgICAgICB2YWxpZGF0aW9uID0gY2FyZHNbdHlwZV07XG4gICAgICAgICAgaWYodmFsaWRhdGlvbikge1xuICAgICAgICAgICAgdmFsaWQgPSB7XG4gICAgICAgICAgICAgIGxlbmd0aCAgOiAoJC5pbkFycmF5KGNhcmROdW1iZXIubGVuZ3RoLCB2YWxpZGF0aW9uLmxlbmd0aCkgIT09IC0xKSxcbiAgICAgICAgICAgICAgcGF0dGVybiA6IChjYXJkTnVtYmVyLnNlYXJjaCh2YWxpZGF0aW9uLnBhdHRlcm4pICE9PSAtMSlcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBpZih2YWxpZC5sZW5ndGggJiYgdmFsaWQucGF0dGVybikge1xuICAgICAgICAgICAgICB2YWxpZENhcmQgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYoIXZhbGlkQ2FyZCkge1xuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAvLyBza2lwIGx1aG4gZm9yIFVuaW9uUGF5XG4gICAgICB1bmlvblBheSA9IHtcbiAgICAgICAgbnVtYmVyICA6ICgkLmluQXJyYXkoY2FyZE51bWJlci5sZW5ndGgsIGNhcmRzLnVuaW9uUGF5Lmxlbmd0aCkgIT09IC0xKSxcbiAgICAgICAgcGF0dGVybiA6IChjYXJkTnVtYmVyLnNlYXJjaChjYXJkcy51bmlvblBheS5wYXR0ZXJuKSAhPT0gLTEpXG4gICAgICB9O1xuICAgICAgaWYodW5pb25QYXkubnVtYmVyICYmIHVuaW9uUGF5LnBhdHRlcm4pIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG5cbiAgICAgIC8vIHZlcmlmeSBsdWhuLCBhZGFwdGVkIGZyb20gIDxodHRwczovL2dpc3QuZ2l0aHViLmNvbS8yMTM0Mzc2PlxuICAgICAgdmFyXG4gICAgICAgIGxlbmd0aCAgICAgICAgPSBjYXJkTnVtYmVyLmxlbmd0aCxcbiAgICAgICAgbXVsdGlwbGUgICAgICA9IDAsXG4gICAgICAgIHByb2R1Y2VkVmFsdWUgPSBbXG4gICAgICAgICAgWzAsIDEsIDIsIDMsIDQsIDUsIDYsIDcsIDgsIDldLFxuICAgICAgICAgIFswLCAyLCA0LCA2LCA4LCAxLCAzLCA1LCA3LCA5XVxuICAgICAgICBdLFxuICAgICAgICBzdW0gICAgICAgICAgID0gMFxuICAgICAgO1xuICAgICAgd2hpbGUgKGxlbmd0aC0tKSB7XG4gICAgICAgIHN1bSArPSBwcm9kdWNlZFZhbHVlW211bHRpcGxlXVtwYXJzZUludChjYXJkTnVtYmVyLmNoYXJBdChsZW5ndGgpLCAxMCldO1xuICAgICAgICBtdWx0aXBsZSBePSAxO1xuICAgICAgfVxuICAgICAgcmV0dXJuIChzdW0gJSAxMCA9PT0gMCAmJiBzdW0gPiAwKTtcbiAgICB9LFxuXG4gICAgbWluQ291bnQ6IGZ1bmN0aW9uKHZhbHVlLCBtaW5Db3VudCkge1xuICAgICAgaWYobWluQ291bnQgPT0gMCkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICAgIGlmKG1pbkNvdW50ID09IDEpIHtcbiAgICAgICAgcmV0dXJuICh2YWx1ZSAhPT0gJycpO1xuICAgICAgfVxuICAgICAgcmV0dXJuICh2YWx1ZS5zcGxpdCgnLCcpLmxlbmd0aCA+PSBtaW5Db3VudCk7XG4gICAgfSxcblxuICAgIGV4YWN0Q291bnQ6IGZ1bmN0aW9uKHZhbHVlLCBleGFjdENvdW50KSB7XG4gICAgICBpZihleGFjdENvdW50ID09IDApIHtcbiAgICAgICAgcmV0dXJuICh2YWx1ZSA9PT0gJycpO1xuICAgICAgfVxuICAgICAgaWYoZXhhY3RDb3VudCA9PSAxKSB7XG4gICAgICAgIHJldHVybiAodmFsdWUgIT09ICcnICYmIHZhbHVlLnNlYXJjaCgnLCcpID09PSAtMSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gKHZhbHVlLnNwbGl0KCcsJykubGVuZ3RoID09IGV4YWN0Q291bnQpO1xuICAgIH0sXG5cbiAgICBtYXhDb3VudDogZnVuY3Rpb24odmFsdWUsIG1heENvdW50KSB7XG4gICAgICBpZihtYXhDb3VudCA9PSAwKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICAgIGlmKG1heENvdW50ID09IDEpIHtcbiAgICAgICAgcmV0dXJuICh2YWx1ZS5zZWFyY2goJywnKSA9PT0gLTEpO1xuICAgICAgfVxuICAgICAgcmV0dXJuICh2YWx1ZS5zcGxpdCgnLCcpLmxlbmd0aCA8PSBtYXhDb3VudCk7XG4gICAgfVxuICB9XG5cbn07XG5cbn0pKCBqUXVlcnksIHdpbmRvdywgZG9jdW1lbnQgKTtcbiJdfQ==