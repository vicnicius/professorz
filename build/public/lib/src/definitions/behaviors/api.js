/*!
 * # Semantic UI - API
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  var window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.api = $.fn.api = function (parameters) {

    var
    // use window context if none specified
    $allModules = $.isFunction(this) ? $(window) : $(this),
        moduleSelector = $allModules.selector || '',
        time = new Date().getTime(),
        performance = [],
        query = arguments[0],
        methodInvoked = typeof query == 'string',
        queryArguments = [].slice.call(arguments, 1),
        returnedValue;

    $allModules.each(function () {
      var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.api.settings, parameters) : $.extend({}, $.fn.api.settings),


      // internal aliases
      namespace = settings.namespace,
          metadata = settings.metadata,
          selector = settings.selector,
          error = settings.error,
          className = settings.className,


      // define namespaces for modules
      eventNamespace = '.' + namespace,
          moduleNamespace = 'module-' + namespace,


      // element that creates request
      $module = $(this),
          $form = $module.closest(selector.form),


      // context used for state
      $context = settings.stateContext ? $(settings.stateContext) : $module,


      // request details
      ajaxSettings,
          requestSettings,
          url,
          data,
          requestStartTime,


      // standard module
      element = this,
          context = $context[0],
          instance = $module.data(moduleNamespace),
          module;

      module = {

        initialize: function () {
          if (!methodInvoked) {
            module.bind.events();
          }
          module.instantiate();
        },

        instantiate: function () {
          module.verbose('Storing instance of module', module);
          instance = module;
          $module.data(moduleNamespace, instance);
        },

        destroy: function () {
          module.verbose('Destroying previous module for', element);
          $module.removeData(moduleNamespace).off(eventNamespace);
        },

        bind: {
          events: function () {
            var triggerEvent = module.get.event();
            if (triggerEvent) {
              module.verbose('Attaching API events to element', triggerEvent);
              $module.on(triggerEvent + eventNamespace, module.event.trigger);
            } else if (settings.on == 'now') {
              module.debug('Querying API endpoint immediately');
              module.query();
            }
          }
        },

        decode: {
          json: function (response) {
            if (response !== undefined && typeof response == 'string') {
              try {
                response = JSON.parse(response);
              } catch (e) {
                // isnt json string
              }
            }
            return response;
          }
        },

        read: {
          cachedResponse: function (url) {
            var response;
            if (window.Storage === undefined) {
              module.error(error.noStorage);
              return;
            }
            response = sessionStorage.getItem(url);
            module.debug('Using cached response', url, response);
            response = module.decode.json(response);
            return response;
          }
        },
        write: {
          cachedResponse: function (url, response) {
            if (response && response === '') {
              module.debug('Response empty, not caching', response);
              return;
            }
            if (window.Storage === undefined) {
              module.error(error.noStorage);
              return;
            }
            if ($.isPlainObject(response)) {
              response = JSON.stringify(response);
            }
            sessionStorage.setItem(url, response);
            module.verbose('Storing cached response for url', url, response);
          }
        },

        query: function () {

          if (module.is.disabled()) {
            module.debug('Element is disabled API request aborted');
            return;
          }

          if (module.is.loading()) {
            if (settings.interruptRequests) {
              module.debug('Interrupting previous request');
              module.abort();
            } else {
              module.debug('Cancelling request, previous request is still pending');
              return;
            }
          }

          // pass element metadata to url (value, text)
          if (settings.defaultData) {
            $.extend(true, settings.urlData, module.get.defaultData());
          }

          // Add form content
          if (settings.serializeForm) {
            settings.data = module.add.formData(settings.data);
          }

          // call beforesend and get any settings changes
          requestSettings = module.get.settings();

          // check if before send cancelled request
          if (requestSettings === false) {
            module.cancelled = true;
            module.error(error.beforeSend);
            return;
          } else {
            module.cancelled = false;
          }

          // get url
          url = module.get.templatedURL();

          if (!url && !module.is.mocked()) {
            module.error(error.missingURL);
            return;
          }

          // replace variables
          url = module.add.urlData(url);
          // missing url parameters
          if (!url && !module.is.mocked()) {
            return;
          }

          requestSettings.url = settings.base + url;

          // look for jQuery ajax parameters in settings
          ajaxSettings = $.extend(true, {}, settings, {
            type: settings.method || settings.type,
            data: data,
            url: settings.base + url,
            beforeSend: settings.beforeXHR,
            success: function () {},
            failure: function () {},
            complete: function () {}
          });

          module.debug('Querying URL', ajaxSettings.url);
          module.verbose('Using AJAX settings', ajaxSettings);
          if (settings.cache === 'local' && module.read.cachedResponse(url)) {
            module.debug('Response returned from local cache');
            module.request = module.create.request();
            module.request.resolveWith(context, [module.read.cachedResponse(url)]);
            return;
          }

          if (!settings.throttle) {
            module.debug('Sending request', data, ajaxSettings.method);
            module.send.request();
          } else {
            if (!settings.throttleFirstRequest && !module.timer) {
              module.debug('Sending request', data, ajaxSettings.method);
              module.send.request();
              module.timer = setTimeout(function () {}, settings.throttle);
            } else {
              module.debug('Throttling request', settings.throttle);
              clearTimeout(module.timer);
              module.timer = setTimeout(function () {
                if (module.timer) {
                  delete module.timer;
                }
                module.debug('Sending throttled request', data, ajaxSettings.method);
                module.send.request();
              }, settings.throttle);
            }
          }
        },

        should: {
          removeError: function () {
            return settings.hideError === true || settings.hideError === 'auto' && !module.is.form();
          }
        },

        is: {
          disabled: function () {
            return $module.filter(selector.disabled).length > 0;
          },
          expectingJSON: function () {
            return settings.dataType === 'json' || settings.dataType === 'jsonp';
          },
          form: function () {
            return $module.is('form') || $context.is('form');
          },
          mocked: function () {
            return settings.mockResponse || settings.mockResponseAsync || settings.response || settings.responseAsync;
          },
          input: function () {
            return $module.is('input');
          },
          loading: function () {
            return module.request ? module.request.state() == 'pending' : false;
          },
          abortedRequest: function (xhr) {
            if (xhr && xhr.readyState !== undefined && xhr.readyState === 0) {
              module.verbose('XHR request determined to be aborted');
              return true;
            } else {
              module.verbose('XHR request was not aborted');
              return false;
            }
          },
          validResponse: function (response) {
            if (!module.is.expectingJSON() || !$.isFunction(settings.successTest)) {
              module.verbose('Response is not JSON, skipping validation', settings.successTest, response);
              return true;
            }
            module.debug('Checking JSON returned success', settings.successTest, response);
            if (settings.successTest(response)) {
              module.debug('Response passed success test', response);
              return true;
            } else {
              module.debug('Response failed success test', response);
              return false;
            }
          }
        },

        was: {
          cancelled: function () {
            return module.cancelled || false;
          },
          succesful: function () {
            return module.request && module.request.state() == 'resolved';
          },
          failure: function () {
            return module.request && module.request.state() == 'rejected';
          },
          complete: function () {
            return module.request && (module.request.state() == 'resolved' || module.request.state() == 'rejected');
          }
        },

        add: {
          urlData: function (url, urlData) {
            var requiredVariables, optionalVariables;
            if (url) {
              requiredVariables = url.match(settings.regExp.required);
              optionalVariables = url.match(settings.regExp.optional);
              urlData = urlData || settings.urlData;
              if (requiredVariables) {
                module.debug('Looking for required URL variables', requiredVariables);
                $.each(requiredVariables, function (index, templatedString) {
                  var
                  // allow legacy {$var} style
                  variable = templatedString.indexOf('$') !== -1 ? templatedString.substr(2, templatedString.length - 3) : templatedString.substr(1, templatedString.length - 2),
                      value = $.isPlainObject(urlData) && urlData[variable] !== undefined ? urlData[variable] : $module.data(variable) !== undefined ? $module.data(variable) : $context.data(variable) !== undefined ? $context.data(variable) : urlData[variable];
                  // remove value
                  if (value === undefined) {
                    module.error(error.requiredParameter, variable, url);
                    url = false;
                    return false;
                  } else {
                    module.verbose('Found required variable', variable, value);
                    value = settings.encodeParameters ? module.get.urlEncodedValue(value) : value;
                    url = url.replace(templatedString, value);
                  }
                });
              }
              if (optionalVariables) {
                module.debug('Looking for optional URL variables', requiredVariables);
                $.each(optionalVariables, function (index, templatedString) {
                  var
                  // allow legacy {/$var} style
                  variable = templatedString.indexOf('$') !== -1 ? templatedString.substr(3, templatedString.length - 4) : templatedString.substr(2, templatedString.length - 3),
                      value = $.isPlainObject(urlData) && urlData[variable] !== undefined ? urlData[variable] : $module.data(variable) !== undefined ? $module.data(variable) : $context.data(variable) !== undefined ? $context.data(variable) : urlData[variable];
                  // optional replacement
                  if (value !== undefined) {
                    module.verbose('Optional variable Found', variable, value);
                    url = url.replace(templatedString, value);
                  } else {
                    module.verbose('Optional variable not found', variable);
                    // remove preceding slash if set
                    if (url.indexOf('/' + templatedString) !== -1) {
                      url = url.replace('/' + templatedString, '');
                    } else {
                      url = url.replace(templatedString, '');
                    }
                  }
                });
              }
            }
            return url;
          },
          formData: function (data) {
            var canSerialize = $.fn.serializeObject !== undefined,
                formData = canSerialize ? $form.serializeObject() : $form.serialize(),
                hasOtherData;
            data = data || settings.data;
            hasOtherData = $.isPlainObject(data);

            if (hasOtherData) {
              if (canSerialize) {
                module.debug('Extending existing data with form data', data, formData);
                data = $.extend(true, {}, data, formData);
              } else {
                module.error(error.missingSerialize);
                module.debug('Cant extend data. Replacing data with form data', data, formData);
                data = formData;
              }
            } else {
              module.debug('Adding form data', formData);
              data = formData;
            }
            return data;
          }
        },

        send: {
          request: function () {
            module.set.loading();
            module.request = module.create.request();
            if (module.is.mocked()) {
              module.mockedXHR = module.create.mockedXHR();
            } else {
              module.xhr = module.create.xhr();
            }
            settings.onRequest.call(context, module.request, module.xhr);
          }
        },

        event: {
          trigger: function (event) {
            module.query();
            if (event.type == 'submit' || event.type == 'click') {
              event.preventDefault();
            }
          },
          xhr: {
            always: function () {
              // nothing special
            },
            done: function (response, textStatus, xhr) {
              var context = this,
                  elapsedTime = new Date().getTime() - requestStartTime,
                  timeLeft = settings.loadingDuration - elapsedTime,
                  translatedResponse = $.isFunction(settings.onResponse) ? module.is.expectingJSON() ? settings.onResponse.call(context, $.extend(true, {}, response)) : settings.onResponse.call(context, response) : false;
              timeLeft = timeLeft > 0 ? timeLeft : 0;
              if (translatedResponse) {
                module.debug('Modified API response in onResponse callback', settings.onResponse, translatedResponse, response);
                response = translatedResponse;
              }
              if (timeLeft > 0) {
                module.debug('Response completed early delaying state change by', timeLeft);
              }
              setTimeout(function () {
                if (module.is.validResponse(response)) {
                  module.request.resolveWith(context, [response, xhr]);
                } else {
                  module.request.rejectWith(context, [xhr, 'invalid']);
                }
              }, timeLeft);
            },
            fail: function (xhr, status, httpMessage) {
              var context = this,
                  elapsedTime = new Date().getTime() - requestStartTime,
                  timeLeft = settings.loadingDuration - elapsedTime;
              timeLeft = timeLeft > 0 ? timeLeft : 0;
              if (timeLeft > 0) {
                module.debug('Response completed early delaying state change by', timeLeft);
              }
              setTimeout(function () {
                if (module.is.abortedRequest(xhr)) {
                  module.request.rejectWith(context, [xhr, 'aborted', httpMessage]);
                } else {
                  module.request.rejectWith(context, [xhr, 'error', status, httpMessage]);
                }
              }, timeLeft);
            }
          },
          request: {
            done: function (response, xhr) {
              module.debug('Successful API Response', response);
              if (settings.cache === 'local' && url) {
                module.write.cachedResponse(url, response);
                module.debug('Saving server response locally', module.cache);
              }
              settings.onSuccess.call(context, response, $module, xhr);
            },
            complete: function (firstParameter, secondParameter) {
              var xhr, response;
              // have to guess callback parameters based on request success
              if (module.was.succesful()) {
                response = firstParameter;
                xhr = secondParameter;
              } else {
                xhr = firstParameter;
                response = module.get.responseFromXHR(xhr);
              }
              module.remove.loading();
              settings.onComplete.call(context, response, $module, xhr);
            },
            fail: function (xhr, status, httpMessage) {
              var
              // pull response from xhr if available
              response = module.get.responseFromXHR(xhr),
                  errorMessage = module.get.errorFromRequest(response, status, httpMessage);
              if (status == 'aborted') {
                module.debug('XHR Aborted (Most likely caused by page navigation or CORS Policy)', status, httpMessage);
                settings.onAbort.call(context, status, $module, xhr);
                return true;
              } else if (status == 'invalid') {
                module.debug('JSON did not pass success test. A server-side error has most likely occurred', response);
              } else if (status == 'error') {
                if (xhr !== undefined) {
                  module.debug('XHR produced a server error', status, httpMessage);
                  // make sure we have an error to display to console
                  if (xhr.status != 200 && httpMessage !== undefined && httpMessage !== '') {
                    module.error(error.statusMessage + httpMessage, ajaxSettings.url);
                  }
                  settings.onError.call(context, errorMessage, $module, xhr);
                }
              }

              if (settings.errorDuration && status !== 'aborted') {
                module.debug('Adding error state');
                module.set.error();
                if (module.should.removeError()) {
                  setTimeout(module.remove.error, settings.errorDuration);
                }
              }
              module.debug('API Request failed', errorMessage, xhr);
              settings.onFailure.call(context, response, $module, xhr);
            }
          }
        },

        create: {

          request: function () {
            // api request promise
            return $.Deferred().always(module.event.request.complete).done(module.event.request.done).fail(module.event.request.fail);
          },

          mockedXHR: function () {
            var
            // xhr does not simulate these properties of xhr but must return them
            textStatus = false,
                status = false,
                httpMessage = false,
                responder = settings.mockResponse || settings.response,
                asyncResponder = settings.mockResponseAsync || settings.responseAsync,
                asyncCallback,
                response,
                mockedXHR;

            mockedXHR = $.Deferred().always(module.event.xhr.complete).done(module.event.xhr.done).fail(module.event.xhr.fail);

            if (responder) {
              if ($.isFunction(responder)) {
                module.debug('Using specified synchronous callback', responder);
                response = responder.call(context, requestSettings);
              } else {
                module.debug('Using settings specified response', responder);
                response = responder;
              }
              // simulating response
              mockedXHR.resolveWith(context, [response, textStatus, { responseText: response }]);
            } else if ($.isFunction(asyncResponder)) {
              asyncCallback = function (response) {
                module.debug('Async callback returned response', response);

                if (response) {
                  mockedXHR.resolveWith(context, [response, textStatus, { responseText: response }]);
                } else {
                  mockedXHR.rejectWith(context, [{ responseText: response }, status, httpMessage]);
                }
              };
              module.debug('Using specified async response callback', asyncResponder);
              asyncResponder.call(context, requestSettings, asyncCallback);
            }
            return mockedXHR;
          },

          xhr: function () {
            var xhr;
            // ajax request promise
            xhr = $.ajax(ajaxSettings).always(module.event.xhr.always).done(module.event.xhr.done).fail(module.event.xhr.fail);
            module.verbose('Created server request', xhr, ajaxSettings);
            return xhr;
          }
        },

        set: {
          error: function () {
            module.verbose('Adding error state to element', $context);
            $context.addClass(className.error);
          },
          loading: function () {
            module.verbose('Adding loading state to element', $context);
            $context.addClass(className.loading);
            requestStartTime = new Date().getTime();
          }
        },

        remove: {
          error: function () {
            module.verbose('Removing error state from element', $context);
            $context.removeClass(className.error);
          },
          loading: function () {
            module.verbose('Removing loading state from element', $context);
            $context.removeClass(className.loading);
          }
        },

        get: {
          responseFromXHR: function (xhr) {
            return $.isPlainObject(xhr) ? module.is.expectingJSON() ? module.decode.json(xhr.responseText) : xhr.responseText : false;
          },
          errorFromRequest: function (response, status, httpMessage) {
            return $.isPlainObject(response) && response.error !== undefined ? response.error // use json error message
            : settings.error[status] !== undefined ? // use server error message
            settings.error[status] : httpMessage;
          },
          request: function () {
            return module.request || false;
          },
          xhr: function () {
            return module.xhr || false;
          },
          settings: function () {
            var runSettings;
            runSettings = settings.beforeSend.call(context, settings);
            if (runSettings) {
              if (runSettings.success !== undefined) {
                module.debug('Legacy success callback detected', runSettings);
                module.error(error.legacyParameters, runSettings.success);
                runSettings.onSuccess = runSettings.success;
              }
              if (runSettings.failure !== undefined) {
                module.debug('Legacy failure callback detected', runSettings);
                module.error(error.legacyParameters, runSettings.failure);
                runSettings.onFailure = runSettings.failure;
              }
              if (runSettings.complete !== undefined) {
                module.debug('Legacy complete callback detected', runSettings);
                module.error(error.legacyParameters, runSettings.complete);
                runSettings.onComplete = runSettings.complete;
              }
            }
            if (runSettings === undefined) {
              module.error(error.noReturnedValue);
            }
            if (runSettings === false) {
              return runSettings;
            }
            return runSettings !== undefined ? $.extend(true, {}, runSettings) : $.extend(true, {}, settings);
          },
          urlEncodedValue: function (value) {
            var decodedValue = window.decodeURIComponent(value),
                encodedValue = window.encodeURIComponent(value),
                alreadyEncoded = decodedValue !== value;
            if (alreadyEncoded) {
              module.debug('URL value is already encoded, avoiding double encoding', value);
              return value;
            }
            module.verbose('Encoding value using encodeURIComponent', value, encodedValue);
            return encodedValue;
          },
          defaultData: function () {
            var data = {};
            if (!$.isWindow(element)) {
              if (module.is.input()) {
                data.value = $module.val();
              } else if (module.is.form()) {} else {
                data.text = $module.text();
              }
            }
            return data;
          },
          event: function () {
            if ($.isWindow(element) || settings.on == 'now') {
              module.debug('API called without element, no events attached');
              return false;
            } else if (settings.on == 'auto') {
              if ($module.is('input')) {
                return element.oninput !== undefined ? 'input' : element.onpropertychange !== undefined ? 'propertychange' : 'keyup';
              } else if ($module.is('form')) {
                return 'submit';
              } else {
                return 'click';
              }
            } else {
              return settings.on;
            }
          },
          templatedURL: function (action) {
            action = action || $module.data(metadata.action) || settings.action || false;
            url = $module.data(metadata.url) || settings.url || false;
            if (url) {
              module.debug('Using specified url', url);
              return url;
            }
            if (action) {
              module.debug('Looking up url for action', action, settings.api);
              if (settings.api[action] === undefined && !module.is.mocked()) {
                module.error(error.missingAction, settings.action, settings.api);
                return;
              }
              url = settings.api[action];
            } else if (module.is.form()) {
              url = $module.attr('action') || $context.attr('action') || false;
              module.debug('No url or action specified, defaulting to form action', url);
            }
            return url;
          }
        },

        abort: function () {
          var xhr = module.get.xhr();
          if (xhr && xhr.state() !== 'resolved') {
            module.debug('Cancelling API request');
            xhr.abort();
          }
        },

        // reset state
        reset: function () {
          module.remove.error();
          module.remove.loading();
        },

        setting: function (name, value) {
          module.debug('Changing setting', name, value);
          if ($.isPlainObject(name)) {
            $.extend(true, settings, name);
          } else if (value !== undefined) {
            if ($.isPlainObject(settings[name])) {
              $.extend(true, settings[name], value);
            } else {
              settings[name] = value;
            }
          } else {
            return settings[name];
          }
        },
        internal: function (name, value) {
          if ($.isPlainObject(name)) {
            $.extend(true, module, name);
          } else if (value !== undefined) {
            module[name] = value;
          } else {
            return module[name];
          }
        },
        debug: function () {
          if (!settings.silent && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.debug = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.debug.apply(console, arguments);
            }
          }
        },
        verbose: function () {
          if (!settings.silent && settings.verbose && settings.debug) {
            if (settings.performance) {
              module.performance.log(arguments);
            } else {
              module.verbose = Function.prototype.bind.call(console.info, console, settings.name + ':');
              module.verbose.apply(console, arguments);
            }
          }
        },
        error: function () {
          if (!settings.silent) {
            module.error = Function.prototype.bind.call(console.error, console, settings.name + ':');
            module.error.apply(console, arguments);
          }
        },
        performance: {
          log: function (message) {
            var currentTime, executionTime, previousTime;
            if (settings.performance) {
              currentTime = new Date().getTime();
              previousTime = time || currentTime;
              executionTime = currentTime - previousTime;
              time = currentTime;
              performance.push({
                'Name': message[0],
                'Arguments': [].slice.call(message, 1) || '',
                //'Element'        : element,
                'Execution Time': executionTime
              });
            }
            clearTimeout(module.performance.timer);
            module.performance.timer = setTimeout(module.performance.display, 500);
          },
          display: function () {
            var title = settings.name + ':',
                totalTime = 0;
            time = false;
            clearTimeout(module.performance.timer);
            $.each(performance, function (index, data) {
              totalTime += data['Execution Time'];
            });
            title += ' ' + totalTime + 'ms';
            if (moduleSelector) {
              title += ' \'' + moduleSelector + '\'';
            }
            if ((console.group !== undefined || console.table !== undefined) && performance.length > 0) {
              console.groupCollapsed(title);
              if (console.table) {
                console.table(performance);
              } else {
                $.each(performance, function (index, data) {
                  console.log(data['Name'] + ': ' + data['Execution Time'] + 'ms');
                });
              }
              console.groupEnd();
            }
            performance = [];
          }
        },
        invoke: function (query, passedArguments, context) {
          var object = instance,
              maxDepth,
              found,
              response;
          passedArguments = passedArguments || queryArguments;
          context = element || context;
          if (typeof query == 'string' && object !== undefined) {
            query = query.split(/[\. ]/);
            maxDepth = query.length - 1;
            $.each(query, function (depth, value) {
              var camelCaseValue = depth != maxDepth ? value + query[depth + 1].charAt(0).toUpperCase() + query[depth + 1].slice(1) : query;
              if ($.isPlainObject(object[camelCaseValue]) && depth != maxDepth) {
                object = object[camelCaseValue];
              } else if (object[camelCaseValue] !== undefined) {
                found = object[camelCaseValue];
                return false;
              } else if ($.isPlainObject(object[value]) && depth != maxDepth) {
                object = object[value];
              } else if (object[value] !== undefined) {
                found = object[value];
                return false;
              } else {
                module.error(error.method, query);
                return false;
              }
            });
          }
          if ($.isFunction(found)) {
            response = found.apply(context, passedArguments);
          } else if (found !== undefined) {
            response = found;
          }
          if ($.isArray(returnedValue)) {
            returnedValue.push(response);
          } else if (returnedValue !== undefined) {
            returnedValue = [returnedValue, response];
          } else if (response !== undefined) {
            returnedValue = response;
          }
          return found;
        }
      };

      if (methodInvoked) {
        if (instance === undefined) {
          module.initialize();
        }
        module.invoke(query);
      } else {
        if (instance !== undefined) {
          instance.invoke('destroy');
        }
        module.initialize();
      }
    });

    return returnedValue !== undefined ? returnedValue : this;
  };

  $.api.settings = {

    name: 'API',
    namespace: 'api',

    debug: false,
    verbose: false,
    performance: true,

    // object containing all templates endpoints
    api: {},

    // whether to cache responses
    cache: true,

    // whether new requests should abort previous requests
    interruptRequests: true,

    // event binding
    on: 'auto',

    // context for applying state classes
    stateContext: false,

    // duration for loading state
    loadingDuration: 0,

    // whether to hide errors after a period of time
    hideError: 'auto',

    // duration for error state
    errorDuration: 2000,

    // whether parameters should be encoded with encodeURIComponent
    encodeParameters: true,

    // API action to use
    action: false,

    // templated URL to use
    url: false,

    // base URL to apply to all endpoints
    base: '',

    // data that will
    urlData: {},

    // whether to add default data to url data
    defaultData: true,

    // whether to serialize closest form
    serializeForm: false,

    // how long to wait before request should occur
    throttle: 0,

    // whether to throttle first request or only repeated
    throttleFirstRequest: true,

    // standard ajax settings
    method: 'get',
    data: {},
    dataType: 'json',

    // mock response
    mockResponse: false,
    mockResponseAsync: false,

    // aliases for mock
    response: false,
    responseAsync: false,

    // callbacks before request
    beforeSend: function (settings) {
      return settings;
    },
    beforeXHR: function (xhr) {},
    onRequest: function (promise, xhr) {},

    // after request
    onResponse: false, // function(response) { },

    // response was successful, if JSON passed validation
    onSuccess: function (response, $module) {},

    // request finished without aborting
    onComplete: function (response, $module) {},

    // failed JSON success test
    onFailure: function (response, $module) {},

    // server error
    onError: function (errorMessage, $module) {},

    // request aborted
    onAbort: function (errorMessage, $module) {},

    successTest: false,

    // errors
    error: {
      beforeSend: 'The before send function has aborted the request',
      error: 'There was an error with your request',
      exitConditions: 'API Request Aborted. Exit conditions met',
      JSONParse: 'JSON could not be parsed during error handling',
      legacyParameters: 'You are using legacy API success callback names',
      method: 'The method you called is not defined',
      missingAction: 'API action used but no url was defined',
      missingSerialize: 'jquery-serialize-object is required to add form data to an existing data object',
      missingURL: 'No URL specified for api event',
      noReturnedValue: 'The beforeSend callback must return a settings object, beforeSend ignored.',
      noStorage: 'Caching responses locally requires session storage',
      parseError: 'There was an error parsing your request',
      requiredParameter: 'Missing a required URL parameter: ',
      statusMessage: 'Server gave an error: ',
      timeout: 'Your request timed out'
    },

    regExp: {
      required: /\{\$*[A-z0-9]+\}/g,
      optional: /\{\/\$*[A-z0-9]+\}/g
    },

    className: {
      loading: 'loading',
      error: 'error'
    },

    selector: {
      disabled: '.disabled',
      form: 'form'
    },

    metadata: {
      action: 'action',
      url: 'url'
    }
  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9iZWhhdmlvcnMvYXBpLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFVQSxDQUFDLENBQUMsVUFBVSxDQUFWLEVBQWEsTUFBYixFQUFxQixRQUFyQixFQUErQixTQUEvQixFQUEwQzs7QUFFNUM7O0FBRUEsTUFDRSxTQUFVLE9BQU8sTUFBUCxJQUFpQixXQUFqQixJQUFnQyxPQUFPLElBQVAsSUFBZSxJQUFoRCxHQUNMLE1BREssR0FFSixPQUFPLElBQVAsSUFBZSxXQUFmLElBQThCLEtBQUssSUFBTCxJQUFhLElBQTVDLEdBQ0UsSUFERixHQUVFLFNBQVMsYUFBVCxHQUxSOztBQVFBLElBQUUsR0FBRixHQUFRLEVBQUUsRUFBRixDQUFLLEdBQUwsR0FBVyxVQUFTLFVBQVQsRUFBcUI7O0FBRXRDOztBQUVFLGtCQUFrQixFQUFFLFVBQUYsQ0FBYSxJQUFiLElBQ1osRUFBRSxNQUFGLENBRFksR0FFWixFQUFFLElBQUYsQ0FKUjtBQUFBLFFBS0UsaUJBQWlCLFlBQVksUUFBWixJQUF3QixFQUwzQztBQUFBLFFBTUUsT0FBaUIsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQU5uQjtBQUFBLFFBT0UsY0FBaUIsRUFQbkI7QUFBQSxRQVNFLFFBQWlCLFVBQVUsQ0FBVixDQVRuQjtBQUFBLFFBVUUsZ0JBQWtCLE9BQU8sS0FBUCxJQUFnQixRQVZwQztBQUFBLFFBV0UsaUJBQWlCLEdBQUcsS0FBSCxDQUFTLElBQVQsQ0FBYyxTQUFkLEVBQXlCLENBQXpCLENBWG5CO0FBQUEsUUFhRSxhQWJGOztBQWdCQSxnQkFDRyxJQURILENBQ1EsWUFBVztBQUNmLFVBQ0UsV0FBc0IsRUFBRSxhQUFGLENBQWdCLFVBQWhCLENBQUYsR0FDaEIsRUFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUIsRUFBRSxFQUFGLENBQUssR0FBTCxDQUFTLFFBQTVCLEVBQXNDLFVBQXRDLENBRGdCLEdBRWhCLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxFQUFFLEVBQUYsQ0FBSyxHQUFMLENBQVMsUUFBdEIsQ0FITjtBQUFBOzs7QUFNRSxrQkFBa0IsU0FBUyxTQU43QjtBQUFBLFVBT0UsV0FBa0IsU0FBUyxRQVA3QjtBQUFBLFVBUUUsV0FBa0IsU0FBUyxRQVI3QjtBQUFBLFVBU0UsUUFBa0IsU0FBUyxLQVQ3QjtBQUFBLFVBVUUsWUFBa0IsU0FBUyxTQVY3QjtBQUFBOzs7QUFhRSx1QkFBa0IsTUFBTSxTQWIxQjtBQUFBLFVBY0Usa0JBQWtCLFlBQVksU0FkaEM7QUFBQTs7O0FBaUJFLGdCQUFrQixFQUFFLElBQUYsQ0FqQnBCO0FBQUEsVUFrQkUsUUFBa0IsUUFBUSxPQUFSLENBQWdCLFNBQVMsSUFBekIsQ0FsQnBCO0FBQUE7OztBQXFCRSxpQkFBbUIsU0FBUyxZQUFWLEdBQ2QsRUFBRSxTQUFTLFlBQVgsQ0FEYyxHQUVkLE9BdkJOO0FBQUE7OztBQTBCRSxrQkExQkY7QUFBQSxVQTJCRSxlQTNCRjtBQUFBLFVBNEJFLEdBNUJGO0FBQUEsVUE2QkUsSUE3QkY7QUFBQSxVQThCRSxnQkE5QkY7QUFBQTs7O0FBaUNFLGdCQUFrQixJQWpDcEI7QUFBQSxVQWtDRSxVQUFrQixTQUFTLENBQVQsQ0FsQ3BCO0FBQUEsVUFtQ0UsV0FBa0IsUUFBUSxJQUFSLENBQWEsZUFBYixDQW5DcEI7QUFBQSxVQW9DRSxNQXBDRjs7QUF1Q0EsZUFBUzs7QUFFUCxvQkFBWSxZQUFXO0FBQ3JCLGNBQUcsQ0FBQyxhQUFKLEVBQW1CO0FBQ2pCLG1CQUFPLElBQVAsQ0FBWSxNQUFaO0FBQ0Q7QUFDRCxpQkFBTyxXQUFQO0FBQ0QsU0FQTTs7QUFTUCxxQkFBYSxZQUFXO0FBQ3RCLGlCQUFPLE9BQVAsQ0FBZSw0QkFBZixFQUE2QyxNQUE3QztBQUNBLHFCQUFXLE1BQVg7QUFDQSxrQkFDRyxJQURILENBQ1EsZUFEUixFQUN5QixRQUR6QjtBQUdELFNBZk07O0FBaUJQLGlCQUFTLFlBQVc7QUFDbEIsaUJBQU8sT0FBUCxDQUFlLGdDQUFmLEVBQWlELE9BQWpEO0FBQ0Esa0JBQ0csVUFESCxDQUNjLGVBRGQsRUFFRyxHQUZILENBRU8sY0FGUDtBQUlELFNBdkJNOztBQXlCUCxjQUFNO0FBQ0osa0JBQVEsWUFBVztBQUNqQixnQkFDRSxlQUFlLE9BQU8sR0FBUCxDQUFXLEtBQVgsRUFEakI7QUFHQSxnQkFBSSxZQUFKLEVBQW1CO0FBQ2pCLHFCQUFPLE9BQVAsQ0FBZSxpQ0FBZixFQUFrRCxZQUFsRDtBQUNBLHNCQUNHLEVBREgsQ0FDTSxlQUFlLGNBRHJCLEVBQ3FDLE9BQU8sS0FBUCxDQUFhLE9BRGxEO0FBR0QsYUFMRCxNQU1LLElBQUcsU0FBUyxFQUFULElBQWUsS0FBbEIsRUFBeUI7QUFDNUIscUJBQU8sS0FBUCxDQUFhLG1DQUFiO0FBQ0EscUJBQU8sS0FBUDtBQUNEO0FBQ0Y7QUFmRyxTQXpCQzs7QUEyQ1AsZ0JBQVE7QUFDTixnQkFBTSxVQUFTLFFBQVQsRUFBbUI7QUFDdkIsZ0JBQUcsYUFBYSxTQUFiLElBQTBCLE9BQU8sUUFBUCxJQUFtQixRQUFoRCxFQUEwRDtBQUN4RCxrQkFBSTtBQUNILDJCQUFXLEtBQUssS0FBTCxDQUFXLFFBQVgsQ0FBWDtBQUNBLGVBRkQsQ0FHQSxPQUFNLENBQU4sRUFBUzs7QUFFUjtBQUNGO0FBQ0QsbUJBQU8sUUFBUDtBQUNEO0FBWEssU0EzQ0Q7O0FBeURQLGNBQU07QUFDSiwwQkFBZ0IsVUFBUyxHQUFULEVBQWM7QUFDNUIsZ0JBQ0UsUUFERjtBQUdBLGdCQUFHLE9BQU8sT0FBUCxLQUFtQixTQUF0QixFQUFpQztBQUMvQixxQkFBTyxLQUFQLENBQWEsTUFBTSxTQUFuQjtBQUNBO0FBQ0Q7QUFDRCx1QkFBVyxlQUFlLE9BQWYsQ0FBdUIsR0FBdkIsQ0FBWDtBQUNBLG1CQUFPLEtBQVAsQ0FBYSx1QkFBYixFQUFzQyxHQUF0QyxFQUEyQyxRQUEzQztBQUNBLHVCQUFXLE9BQU8sTUFBUCxDQUFjLElBQWQsQ0FBbUIsUUFBbkIsQ0FBWDtBQUNBLG1CQUFPLFFBQVA7QUFDRDtBQWJHLFNBekRDO0FBd0VQLGVBQU87QUFDTCwwQkFBZ0IsVUFBUyxHQUFULEVBQWMsUUFBZCxFQUF3QjtBQUN0QyxnQkFBRyxZQUFZLGFBQWEsRUFBNUIsRUFBZ0M7QUFDOUIscUJBQU8sS0FBUCxDQUFhLDZCQUFiLEVBQTRDLFFBQTVDO0FBQ0E7QUFDRDtBQUNELGdCQUFHLE9BQU8sT0FBUCxLQUFtQixTQUF0QixFQUFpQztBQUMvQixxQkFBTyxLQUFQLENBQWEsTUFBTSxTQUFuQjtBQUNBO0FBQ0Q7QUFDRCxnQkFBSSxFQUFFLGFBQUYsQ0FBZ0IsUUFBaEIsQ0FBSixFQUFnQztBQUM5Qix5QkFBVyxLQUFLLFNBQUwsQ0FBZSxRQUFmLENBQVg7QUFDRDtBQUNELDJCQUFlLE9BQWYsQ0FBdUIsR0FBdkIsRUFBNEIsUUFBNUI7QUFDQSxtQkFBTyxPQUFQLENBQWUsaUNBQWYsRUFBa0QsR0FBbEQsRUFBdUQsUUFBdkQ7QUFDRDtBQWZJLFNBeEVBOztBQTBGUCxlQUFPLFlBQVc7O0FBRWhCLGNBQUcsT0FBTyxFQUFQLENBQVUsUUFBVixFQUFILEVBQXlCO0FBQ3ZCLG1CQUFPLEtBQVAsQ0FBYSx5Q0FBYjtBQUNBO0FBQ0Q7O0FBRUQsY0FBRyxPQUFPLEVBQVAsQ0FBVSxPQUFWLEVBQUgsRUFBd0I7QUFDdEIsZ0JBQUcsU0FBUyxpQkFBWixFQUErQjtBQUM3QixxQkFBTyxLQUFQLENBQWEsK0JBQWI7QUFDQSxxQkFBTyxLQUFQO0FBQ0QsYUFIRCxNQUlLO0FBQ0gscUJBQU8sS0FBUCxDQUFhLHVEQUFiO0FBQ0E7QUFDRDtBQUNGOzs7QUFHRCxjQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2QixjQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsU0FBUyxPQUF4QixFQUFpQyxPQUFPLEdBQVAsQ0FBVyxXQUFYLEVBQWpDO0FBQ0Q7OztBQUdELGNBQUcsU0FBUyxhQUFaLEVBQTJCO0FBQ3pCLHFCQUFTLElBQVQsR0FBZ0IsT0FBTyxHQUFQLENBQVcsUUFBWCxDQUFvQixTQUFTLElBQTdCLENBQWhCO0FBQ0Q7OztBQUdELDRCQUFrQixPQUFPLEdBQVAsQ0FBVyxRQUFYLEVBQWxCOzs7QUFHQSxjQUFHLG9CQUFvQixLQUF2QixFQUE4QjtBQUM1QixtQkFBTyxTQUFQLEdBQW1CLElBQW5CO0FBQ0EsbUJBQU8sS0FBUCxDQUFhLE1BQU0sVUFBbkI7QUFDQTtBQUNELFdBSkQsTUFLSztBQUNILG1CQUFPLFNBQVAsR0FBbUIsS0FBbkI7QUFDRDs7O0FBR0QsZ0JBQU0sT0FBTyxHQUFQLENBQVcsWUFBWCxFQUFOOztBQUVBLGNBQUcsQ0FBQyxHQUFELElBQVEsQ0FBQyxPQUFPLEVBQVAsQ0FBVSxNQUFWLEVBQVosRUFBZ0M7QUFDOUIsbUJBQU8sS0FBUCxDQUFhLE1BQU0sVUFBbkI7QUFDQTtBQUNEOzs7QUFHRCxnQkFBTSxPQUFPLEdBQVAsQ0FBVyxPQUFYLENBQW9CLEdBQXBCLENBQU47O0FBRUEsY0FBSSxDQUFDLEdBQUQsSUFBUSxDQUFDLE9BQU8sRUFBUCxDQUFVLE1BQVYsRUFBYixFQUFpQztBQUMvQjtBQUNEOztBQUVELDBCQUFnQixHQUFoQixHQUFzQixTQUFTLElBQVQsR0FBZ0IsR0FBdEM7OztBQUdBLHlCQUFlLEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLFFBQW5CLEVBQTZCO0FBQzFDLGtCQUFhLFNBQVMsTUFBVCxJQUFtQixTQUFTLElBREM7QUFFMUMsa0JBQWEsSUFGNkI7QUFHMUMsaUJBQWEsU0FBUyxJQUFULEdBQWdCLEdBSGE7QUFJMUMsd0JBQWEsU0FBUyxTQUpvQjtBQUsxQyxxQkFBYSxZQUFXLENBQUUsQ0FMZ0I7QUFNMUMscUJBQWEsWUFBVyxDQUFFLENBTmdCO0FBTzFDLHNCQUFhLFlBQVcsQ0FBRTtBQVBnQixXQUE3QixDQUFmOztBQVVBLGlCQUFPLEtBQVAsQ0FBYSxjQUFiLEVBQTZCLGFBQWEsR0FBMUM7QUFDQSxpQkFBTyxPQUFQLENBQWUscUJBQWYsRUFBc0MsWUFBdEM7QUFDQSxjQUFHLFNBQVMsS0FBVCxLQUFtQixPQUFuQixJQUE4QixPQUFPLElBQVAsQ0FBWSxjQUFaLENBQTJCLEdBQTNCLENBQWpDLEVBQWtFO0FBQ2hFLG1CQUFPLEtBQVAsQ0FBYSxvQ0FBYjtBQUNBLG1CQUFPLE9BQVAsR0FBaUIsT0FBTyxNQUFQLENBQWMsT0FBZCxFQUFqQjtBQUNBLG1CQUFPLE9BQVAsQ0FBZSxXQUFmLENBQTJCLE9BQTNCLEVBQW9DLENBQUUsT0FBTyxJQUFQLENBQVksY0FBWixDQUEyQixHQUEzQixDQUFGLENBQXBDO0FBQ0E7QUFDRDs7QUFFRCxjQUFJLENBQUMsU0FBUyxRQUFkLEVBQXlCO0FBQ3ZCLG1CQUFPLEtBQVAsQ0FBYSxpQkFBYixFQUFnQyxJQUFoQyxFQUFzQyxhQUFhLE1BQW5EO0FBQ0EsbUJBQU8sSUFBUCxDQUFZLE9BQVo7QUFDRCxXQUhELE1BSUs7QUFDSCxnQkFBRyxDQUFDLFNBQVMsb0JBQVYsSUFBa0MsQ0FBQyxPQUFPLEtBQTdDLEVBQW9EO0FBQ2xELHFCQUFPLEtBQVAsQ0FBYSxpQkFBYixFQUFnQyxJQUFoQyxFQUFzQyxhQUFhLE1BQW5EO0FBQ0EscUJBQU8sSUFBUCxDQUFZLE9BQVo7QUFDQSxxQkFBTyxLQUFQLEdBQWUsV0FBVyxZQUFVLENBQUUsQ0FBdkIsRUFBeUIsU0FBUyxRQUFsQyxDQUFmO0FBQ0QsYUFKRCxNQUtLO0FBQ0gscUJBQU8sS0FBUCxDQUFhLG9CQUFiLEVBQW1DLFNBQVMsUUFBNUM7QUFDQSwyQkFBYSxPQUFPLEtBQXBCO0FBQ0EscUJBQU8sS0FBUCxHQUFlLFdBQVcsWUFBVztBQUNuQyxvQkFBRyxPQUFPLEtBQVYsRUFBaUI7QUFDZix5QkFBTyxPQUFPLEtBQWQ7QUFDRDtBQUNELHVCQUFPLEtBQVAsQ0FBYSwyQkFBYixFQUEwQyxJQUExQyxFQUFnRCxhQUFhLE1BQTdEO0FBQ0EsdUJBQU8sSUFBUCxDQUFZLE9BQVo7QUFDRCxlQU5jLEVBTVosU0FBUyxRQU5HLENBQWY7QUFPRDtBQUNGO0FBRUYsU0EvTE07O0FBaU1QLGdCQUFRO0FBQ04sdUJBQWEsWUFBVztBQUN0QixtQkFBUyxTQUFTLFNBQVQsS0FBdUIsSUFBdkIsSUFBZ0MsU0FBUyxTQUFULEtBQXVCLE1BQXZCLElBQWlDLENBQUMsT0FBTyxFQUFQLENBQVUsSUFBVixFQUEzRTtBQUNEO0FBSEssU0FqTUQ7O0FBdU1QLFlBQUk7QUFDRixvQkFBVSxZQUFXO0FBQ25CLG1CQUFRLFFBQVEsTUFBUixDQUFlLFNBQVMsUUFBeEIsRUFBa0MsTUFBbEMsR0FBMkMsQ0FBbkQ7QUFDRCxXQUhDO0FBSUYseUJBQWUsWUFBVztBQUN4QixtQkFBTyxTQUFTLFFBQVQsS0FBc0IsTUFBdEIsSUFBZ0MsU0FBUyxRQUFULEtBQXNCLE9BQTdEO0FBQ0QsV0FOQztBQU9GLGdCQUFNLFlBQVc7QUFDZixtQkFBTyxRQUFRLEVBQVIsQ0FBVyxNQUFYLEtBQXNCLFNBQVMsRUFBVCxDQUFZLE1BQVosQ0FBN0I7QUFDRCxXQVRDO0FBVUYsa0JBQVEsWUFBVztBQUNqQixtQkFBUSxTQUFTLFlBQVQsSUFBeUIsU0FBUyxpQkFBbEMsSUFBdUQsU0FBUyxRQUFoRSxJQUE0RSxTQUFTLGFBQTdGO0FBQ0QsV0FaQztBQWFGLGlCQUFPLFlBQVc7QUFDaEIsbUJBQU8sUUFBUSxFQUFSLENBQVcsT0FBWCxDQUFQO0FBQ0QsV0FmQztBQWdCRixtQkFBUyxZQUFXO0FBQ2xCLG1CQUFRLE9BQU8sT0FBUixHQUNGLE9BQU8sT0FBUCxDQUFlLEtBQWYsTUFBMEIsU0FEeEIsR0FFSCxLQUZKO0FBSUQsV0FyQkM7QUFzQkYsMEJBQWdCLFVBQVMsR0FBVCxFQUFjO0FBQzVCLGdCQUFHLE9BQU8sSUFBSSxVQUFKLEtBQW1CLFNBQTFCLElBQXVDLElBQUksVUFBSixLQUFtQixDQUE3RCxFQUFnRTtBQUM5RCxxQkFBTyxPQUFQLENBQWUsc0NBQWY7QUFDQSxxQkFBTyxJQUFQO0FBQ0QsYUFIRCxNQUlLO0FBQ0gscUJBQU8sT0FBUCxDQUFlLDZCQUFmO0FBQ0EscUJBQU8sS0FBUDtBQUNEO0FBQ0YsV0EvQkM7QUFnQ0YseUJBQWUsVUFBUyxRQUFULEVBQW1CO0FBQ2hDLGdCQUFLLENBQUMsT0FBTyxFQUFQLENBQVUsYUFBVixFQUFGLElBQWdDLENBQUMsRUFBRSxVQUFGLENBQWEsU0FBUyxXQUF0QixDQUFyQyxFQUEwRTtBQUN4RSxxQkFBTyxPQUFQLENBQWUsMkNBQWYsRUFBNEQsU0FBUyxXQUFyRSxFQUFrRixRQUFsRjtBQUNBLHFCQUFPLElBQVA7QUFDRDtBQUNELG1CQUFPLEtBQVAsQ0FBYSxnQ0FBYixFQUErQyxTQUFTLFdBQXhELEVBQXFFLFFBQXJFO0FBQ0EsZ0JBQUksU0FBUyxXQUFULENBQXFCLFFBQXJCLENBQUosRUFBcUM7QUFDbkMscUJBQU8sS0FBUCxDQUFhLDhCQUFiLEVBQTZDLFFBQTdDO0FBQ0EscUJBQU8sSUFBUDtBQUNELGFBSEQsTUFJSztBQUNILHFCQUFPLEtBQVAsQ0FBYSw4QkFBYixFQUE2QyxRQUE3QztBQUNBLHFCQUFPLEtBQVA7QUFDRDtBQUNGO0FBOUNDLFNBdk1HOztBQXdQUCxhQUFLO0FBQ0gscUJBQVcsWUFBVztBQUNwQixtQkFBUSxPQUFPLFNBQVAsSUFBb0IsS0FBNUI7QUFDRCxXQUhFO0FBSUgscUJBQVcsWUFBVztBQUNwQixtQkFBUSxPQUFPLE9BQVAsSUFBa0IsT0FBTyxPQUFQLENBQWUsS0FBZixNQUEwQixVQUFwRDtBQUNELFdBTkU7QUFPSCxtQkFBUyxZQUFXO0FBQ2xCLG1CQUFRLE9BQU8sT0FBUCxJQUFrQixPQUFPLE9BQVAsQ0FBZSxLQUFmLE1BQTBCLFVBQXBEO0FBQ0QsV0FURTtBQVVILG9CQUFVLFlBQVc7QUFDbkIsbUJBQVEsT0FBTyxPQUFQLEtBQW1CLE9BQU8sT0FBUCxDQUFlLEtBQWYsTUFBMEIsVUFBMUIsSUFBd0MsT0FBTyxPQUFQLENBQWUsS0FBZixNQUEwQixVQUFyRixDQUFSO0FBQ0Q7QUFaRSxTQXhQRTs7QUF1UVAsYUFBSztBQUNILG1CQUFTLFVBQVMsR0FBVCxFQUFjLE9BQWQsRUFBdUI7QUFDOUIsZ0JBQ0UsaUJBREYsRUFFRSxpQkFGRjtBQUlBLGdCQUFHLEdBQUgsRUFBUTtBQUNOLGtDQUFvQixJQUFJLEtBQUosQ0FBVSxTQUFTLE1BQVQsQ0FBZ0IsUUFBMUIsQ0FBcEI7QUFDQSxrQ0FBb0IsSUFBSSxLQUFKLENBQVUsU0FBUyxNQUFULENBQWdCLFFBQTFCLENBQXBCO0FBQ0Esd0JBQW9CLFdBQVcsU0FBUyxPQUF4QztBQUNBLGtCQUFHLGlCQUFILEVBQXNCO0FBQ3BCLHVCQUFPLEtBQVAsQ0FBYSxvQ0FBYixFQUFtRCxpQkFBbkQ7QUFDQSxrQkFBRSxJQUFGLENBQU8saUJBQVAsRUFBMEIsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDO0FBQ3pEOztBQUVFLDZCQUFZLGdCQUFnQixPQUFoQixDQUF3QixHQUF4QixNQUFpQyxDQUFDLENBQW5DLEdBQ1AsZ0JBQWdCLE1BQWhCLENBQXVCLENBQXZCLEVBQTBCLGdCQUFnQixNQUFoQixHQUF5QixDQUFuRCxDQURPLEdBRVAsZ0JBQWdCLE1BQWhCLENBQXVCLENBQXZCLEVBQTBCLGdCQUFnQixNQUFoQixHQUF5QixDQUFuRCxDQUpOO0FBQUEsc0JBS0UsUUFBVyxFQUFFLGFBQUYsQ0FBZ0IsT0FBaEIsS0FBNEIsUUFBUSxRQUFSLE1BQXNCLFNBQW5ELEdBQ04sUUFBUSxRQUFSLENBRE0sR0FFTCxRQUFRLElBQVIsQ0FBYSxRQUFiLE1BQTJCLFNBQTVCLEdBQ0UsUUFBUSxJQUFSLENBQWEsUUFBYixDQURGLEdBRUcsU0FBUyxJQUFULENBQWMsUUFBZCxNQUE0QixTQUE3QixHQUNFLFNBQVMsSUFBVCxDQUFjLFFBQWQsQ0FERixHQUVFLFFBQVEsUUFBUixDQVhWOztBQWNBLHNCQUFHLFVBQVUsU0FBYixFQUF3QjtBQUN0QiwyQkFBTyxLQUFQLENBQWEsTUFBTSxpQkFBbkIsRUFBc0MsUUFBdEMsRUFBZ0QsR0FBaEQ7QUFDQSwwQkFBTSxLQUFOO0FBQ0EsMkJBQU8sS0FBUDtBQUNELG1CQUpELE1BS0s7QUFDSCwyQkFBTyxPQUFQLENBQWUseUJBQWYsRUFBMEMsUUFBMUMsRUFBb0QsS0FBcEQ7QUFDQSw0QkFBUyxTQUFTLGdCQUFWLEdBQ0osT0FBTyxHQUFQLENBQVcsZUFBWCxDQUEyQixLQUEzQixDQURJLEdBRUosS0FGSjtBQUlBLDBCQUFNLElBQUksT0FBSixDQUFZLGVBQVosRUFBNkIsS0FBN0IsQ0FBTjtBQUNEO0FBQ0YsaUJBNUJEO0FBNkJEO0FBQ0Qsa0JBQUcsaUJBQUgsRUFBc0I7QUFDcEIsdUJBQU8sS0FBUCxDQUFhLG9DQUFiLEVBQW1ELGlCQUFuRDtBQUNBLGtCQUFFLElBQUYsQ0FBTyxpQkFBUCxFQUEwQixVQUFTLEtBQVQsRUFBZ0IsZUFBaEIsRUFBaUM7QUFDekQ7O0FBRUUsNkJBQVksZ0JBQWdCLE9BQWhCLENBQXdCLEdBQXhCLE1BQWlDLENBQUMsQ0FBbkMsR0FDUCxnQkFBZ0IsTUFBaEIsQ0FBdUIsQ0FBdkIsRUFBMEIsZ0JBQWdCLE1BQWhCLEdBQXlCLENBQW5ELENBRE8sR0FFUCxnQkFBZ0IsTUFBaEIsQ0FBdUIsQ0FBdkIsRUFBMEIsZ0JBQWdCLE1BQWhCLEdBQXlCLENBQW5ELENBSk47QUFBQSxzQkFLRSxRQUFXLEVBQUUsYUFBRixDQUFnQixPQUFoQixLQUE0QixRQUFRLFFBQVIsTUFBc0IsU0FBbkQsR0FDTixRQUFRLFFBQVIsQ0FETSxHQUVMLFFBQVEsSUFBUixDQUFhLFFBQWIsTUFBMkIsU0FBNUIsR0FDRSxRQUFRLElBQVIsQ0FBYSxRQUFiLENBREYsR0FFRyxTQUFTLElBQVQsQ0FBYyxRQUFkLE1BQTRCLFNBQTdCLEdBQ0UsU0FBUyxJQUFULENBQWMsUUFBZCxDQURGLEdBRUUsUUFBUSxRQUFSLENBWFY7O0FBY0Esc0JBQUcsVUFBVSxTQUFiLEVBQXdCO0FBQ3RCLDJCQUFPLE9BQVAsQ0FBZSx5QkFBZixFQUEwQyxRQUExQyxFQUFvRCxLQUFwRDtBQUNBLDBCQUFNLElBQUksT0FBSixDQUFZLGVBQVosRUFBNkIsS0FBN0IsQ0FBTjtBQUNELG1CQUhELE1BSUs7QUFDSCwyQkFBTyxPQUFQLENBQWUsNkJBQWYsRUFBOEMsUUFBOUM7O0FBRUEsd0JBQUcsSUFBSSxPQUFKLENBQVksTUFBTSxlQUFsQixNQUF1QyxDQUFDLENBQTNDLEVBQThDO0FBQzVDLDRCQUFNLElBQUksT0FBSixDQUFZLE1BQU0sZUFBbEIsRUFBbUMsRUFBbkMsQ0FBTjtBQUNELHFCQUZELE1BR0s7QUFDSCw0QkFBTSxJQUFJLE9BQUosQ0FBWSxlQUFaLEVBQTZCLEVBQTdCLENBQU47QUFDRDtBQUNGO0FBQ0YsaUJBN0JEO0FBOEJEO0FBQ0Y7QUFDRCxtQkFBTyxHQUFQO0FBQ0QsV0E3RUU7QUE4RUgsb0JBQVUsVUFBUyxJQUFULEVBQWU7QUFDdkIsZ0JBQ0UsZUFBZ0IsRUFBRSxFQUFGLENBQUssZUFBTCxLQUF5QixTQUQzQztBQUFBLGdCQUVFLFdBQWdCLFlBQUQsR0FDWCxNQUFNLGVBQU4sRUFEVyxHQUVYLE1BQU0sU0FBTixFQUpOO0FBQUEsZ0JBS0UsWUFMRjtBQU9BLG1CQUFlLFFBQVEsU0FBUyxJQUFoQztBQUNBLDJCQUFlLEVBQUUsYUFBRixDQUFnQixJQUFoQixDQUFmOztBQUVBLGdCQUFHLFlBQUgsRUFBaUI7QUFDZixrQkFBRyxZQUFILEVBQWlCO0FBQ2YsdUJBQU8sS0FBUCxDQUFhLHdDQUFiLEVBQXVELElBQXZELEVBQTZELFFBQTdEO0FBQ0EsdUJBQU8sRUFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUIsSUFBbkIsRUFBeUIsUUFBekIsQ0FBUDtBQUNELGVBSEQsTUFJSztBQUNILHVCQUFPLEtBQVAsQ0FBYSxNQUFNLGdCQUFuQjtBQUNBLHVCQUFPLEtBQVAsQ0FBYSxpREFBYixFQUFnRSxJQUFoRSxFQUFzRSxRQUF0RTtBQUNBLHVCQUFPLFFBQVA7QUFDRDtBQUNGLGFBVkQsTUFXSztBQUNILHFCQUFPLEtBQVAsQ0FBYSxrQkFBYixFQUFpQyxRQUFqQztBQUNBLHFCQUFPLFFBQVA7QUFDRDtBQUNELG1CQUFPLElBQVA7QUFDRDtBQXpHRSxTQXZRRTs7QUFtWFAsY0FBTTtBQUNKLG1CQUFTLFlBQVc7QUFDbEIsbUJBQU8sR0FBUCxDQUFXLE9BQVg7QUFDQSxtQkFBTyxPQUFQLEdBQWlCLE9BQU8sTUFBUCxDQUFjLE9BQWQsRUFBakI7QUFDQSxnQkFBSSxPQUFPLEVBQVAsQ0FBVSxNQUFWLEVBQUosRUFBeUI7QUFDdkIscUJBQU8sU0FBUCxHQUFtQixPQUFPLE1BQVAsQ0FBYyxTQUFkLEVBQW5CO0FBQ0QsYUFGRCxNQUdLO0FBQ0gscUJBQU8sR0FBUCxHQUFhLE9BQU8sTUFBUCxDQUFjLEdBQWQsRUFBYjtBQUNEO0FBQ0QscUJBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixPQUF4QixFQUFpQyxPQUFPLE9BQXhDLEVBQWlELE9BQU8sR0FBeEQ7QUFDRDtBQVhHLFNBblhDOztBQWlZUCxlQUFPO0FBQ0wsbUJBQVMsVUFBUyxLQUFULEVBQWdCO0FBQ3ZCLG1CQUFPLEtBQVA7QUFDQSxnQkFBRyxNQUFNLElBQU4sSUFBYyxRQUFkLElBQTBCLE1BQU0sSUFBTixJQUFjLE9BQTNDLEVBQW9EO0FBQ2xELG9CQUFNLGNBQU47QUFDRDtBQUNGLFdBTkk7QUFPTCxlQUFLO0FBQ0gsb0JBQVEsWUFBVzs7QUFFbEIsYUFIRTtBQUlILGtCQUFNLFVBQVMsUUFBVCxFQUFtQixVQUFuQixFQUErQixHQUEvQixFQUFvQztBQUN4QyxrQkFDRSxVQUFxQixJQUR2QjtBQUFBLGtCQUVFLGNBQXNCLElBQUksSUFBSixHQUFXLE9BQVgsS0FBdUIsZ0JBRi9DO0FBQUEsa0JBR0UsV0FBc0IsU0FBUyxlQUFULEdBQTJCLFdBSG5EO0FBQUEsa0JBSUUscUJBQXVCLEVBQUUsVUFBRixDQUFhLFNBQVMsVUFBdEIsQ0FBRixHQUNqQixPQUFPLEVBQVAsQ0FBVSxhQUFWLEtBQ0UsU0FBUyxVQUFULENBQW9CLElBQXBCLENBQXlCLE9BQXpCLEVBQWtDLEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLFFBQW5CLENBQWxDLENBREYsR0FFRSxTQUFTLFVBQVQsQ0FBb0IsSUFBcEIsQ0FBeUIsT0FBekIsRUFBa0MsUUFBbEMsQ0FIZSxHQUlqQixLQVJOO0FBVUEseUJBQVksV0FBVyxDQUFaLEdBQ1AsUUFETyxHQUVQLENBRko7QUFJQSxrQkFBRyxrQkFBSCxFQUF1QjtBQUNyQix1QkFBTyxLQUFQLENBQWEsOENBQWIsRUFBNkQsU0FBUyxVQUF0RSxFQUFrRixrQkFBbEYsRUFBc0csUUFBdEc7QUFDQSwyQkFBVyxrQkFBWDtBQUNEO0FBQ0Qsa0JBQUcsV0FBVyxDQUFkLEVBQWlCO0FBQ2YsdUJBQU8sS0FBUCxDQUFhLG1EQUFiLEVBQWtFLFFBQWxFO0FBQ0Q7QUFDRCx5QkFBVyxZQUFXO0FBQ3BCLG9CQUFJLE9BQU8sRUFBUCxDQUFVLGFBQVYsQ0FBd0IsUUFBeEIsQ0FBSixFQUF3QztBQUN0Qyx5QkFBTyxPQUFQLENBQWUsV0FBZixDQUEyQixPQUEzQixFQUFvQyxDQUFDLFFBQUQsRUFBVyxHQUFYLENBQXBDO0FBQ0QsaUJBRkQsTUFHSztBQUNILHlCQUFPLE9BQVAsQ0FBZSxVQUFmLENBQTBCLE9BQTFCLEVBQW1DLENBQUMsR0FBRCxFQUFNLFNBQU4sQ0FBbkM7QUFDRDtBQUNGLGVBUEQsRUFPRyxRQVBIO0FBUUQsYUFsQ0U7QUFtQ0gsa0JBQU0sVUFBUyxHQUFULEVBQWMsTUFBZCxFQUFzQixXQUF0QixFQUFtQztBQUN2QyxrQkFDRSxVQUFjLElBRGhCO0FBQUEsa0JBRUUsY0FBZSxJQUFJLElBQUosR0FBVyxPQUFYLEtBQXVCLGdCQUZ4QztBQUFBLGtCQUdFLFdBQWUsU0FBUyxlQUFULEdBQTJCLFdBSDVDO0FBS0EseUJBQVksV0FBVyxDQUFaLEdBQ1AsUUFETyxHQUVQLENBRko7QUFJQSxrQkFBRyxXQUFXLENBQWQsRUFBaUI7QUFDZix1QkFBTyxLQUFQLENBQWEsbURBQWIsRUFBa0UsUUFBbEU7QUFDRDtBQUNELHlCQUFXLFlBQVc7QUFDcEIsb0JBQUksT0FBTyxFQUFQLENBQVUsY0FBVixDQUF5QixHQUF6QixDQUFKLEVBQW9DO0FBQ2xDLHlCQUFPLE9BQVAsQ0FBZSxVQUFmLENBQTBCLE9BQTFCLEVBQW1DLENBQUMsR0FBRCxFQUFNLFNBQU4sRUFBaUIsV0FBakIsQ0FBbkM7QUFDRCxpQkFGRCxNQUdLO0FBQ0gseUJBQU8sT0FBUCxDQUFlLFVBQWYsQ0FBMEIsT0FBMUIsRUFBbUMsQ0FBQyxHQUFELEVBQU0sT0FBTixFQUFlLE1BQWYsRUFBdUIsV0FBdkIsQ0FBbkM7QUFDRDtBQUNGLGVBUEQsRUFPRyxRQVBIO0FBUUQ7QUF4REUsV0FQQTtBQWlFTCxtQkFBUztBQUNQLGtCQUFNLFVBQVMsUUFBVCxFQUFtQixHQUFuQixFQUF3QjtBQUM1QixxQkFBTyxLQUFQLENBQWEseUJBQWIsRUFBd0MsUUFBeEM7QUFDQSxrQkFBRyxTQUFTLEtBQVQsS0FBbUIsT0FBbkIsSUFBOEIsR0FBakMsRUFBc0M7QUFDcEMsdUJBQU8sS0FBUCxDQUFhLGNBQWIsQ0FBNEIsR0FBNUIsRUFBaUMsUUFBakM7QUFDQSx1QkFBTyxLQUFQLENBQWEsZ0NBQWIsRUFBK0MsT0FBTyxLQUF0RDtBQUNEO0FBQ0QsdUJBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixPQUF4QixFQUFpQyxRQUFqQyxFQUEyQyxPQUEzQyxFQUFvRCxHQUFwRDtBQUNELGFBUk07QUFTUCxzQkFBVSxVQUFTLGNBQVQsRUFBeUIsZUFBekIsRUFBMEM7QUFDbEQsa0JBQ0UsR0FERixFQUVFLFFBRkY7O0FBS0Esa0JBQUksT0FBTyxHQUFQLENBQVcsU0FBWCxFQUFKLEVBQTZCO0FBQzNCLDJCQUFXLGNBQVg7QUFDQSxzQkFBVyxlQUFYO0FBQ0QsZUFIRCxNQUlLO0FBQ0gsc0JBQVcsY0FBWDtBQUNBLDJCQUFXLE9BQU8sR0FBUCxDQUFXLGVBQVgsQ0FBMkIsR0FBM0IsQ0FBWDtBQUNEO0FBQ0QscUJBQU8sTUFBUCxDQUFjLE9BQWQ7QUFDQSx1QkFBUyxVQUFULENBQW9CLElBQXBCLENBQXlCLE9BQXpCLEVBQWtDLFFBQWxDLEVBQTRDLE9BQTVDLEVBQXFELEdBQXJEO0FBQ0QsYUF6Qk07QUEwQlAsa0JBQU0sVUFBUyxHQUFULEVBQWMsTUFBZCxFQUFzQixXQUF0QixFQUFtQztBQUN2Qzs7QUFFRSx5QkFBZSxPQUFPLEdBQVAsQ0FBVyxlQUFYLENBQTJCLEdBQTNCLENBRmpCO0FBQUEsa0JBR0UsZUFBZSxPQUFPLEdBQVAsQ0FBVyxnQkFBWCxDQUE0QixRQUE1QixFQUFzQyxNQUF0QyxFQUE4QyxXQUE5QyxDQUhqQjtBQUtBLGtCQUFHLFVBQVUsU0FBYixFQUF3QjtBQUN0Qix1QkFBTyxLQUFQLENBQWEsb0VBQWIsRUFBbUYsTUFBbkYsRUFBMkYsV0FBM0Y7QUFDQSx5QkFBUyxPQUFULENBQWlCLElBQWpCLENBQXNCLE9BQXRCLEVBQStCLE1BQS9CLEVBQXVDLE9BQXZDLEVBQWdELEdBQWhEO0FBQ0EsdUJBQU8sSUFBUDtBQUNELGVBSkQsTUFLSyxJQUFHLFVBQVUsU0FBYixFQUF3QjtBQUMzQix1QkFBTyxLQUFQLENBQWEsOEVBQWIsRUFBNkYsUUFBN0Y7QUFDRCxlQUZJLE1BR0EsSUFBRyxVQUFVLE9BQWIsRUFBc0I7QUFDekIsb0JBQUcsUUFBUSxTQUFYLEVBQXNCO0FBQ3BCLHlCQUFPLEtBQVAsQ0FBYSw2QkFBYixFQUE0QyxNQUE1QyxFQUFvRCxXQUFwRDs7QUFFQSxzQkFBSSxJQUFJLE1BQUosSUFBYyxHQUFkLElBQXFCLGdCQUFnQixTQUFyQyxJQUFrRCxnQkFBZ0IsRUFBdEUsRUFBMEU7QUFDeEUsMkJBQU8sS0FBUCxDQUFhLE1BQU0sYUFBTixHQUFzQixXQUFuQyxFQUFnRCxhQUFhLEdBQTdEO0FBQ0Q7QUFDRCwyQkFBUyxPQUFULENBQWlCLElBQWpCLENBQXNCLE9BQXRCLEVBQStCLFlBQS9CLEVBQTZDLE9BQTdDLEVBQXNELEdBQXREO0FBQ0Q7QUFDRjs7QUFFRCxrQkFBRyxTQUFTLGFBQVQsSUFBMEIsV0FBVyxTQUF4QyxFQUFtRDtBQUNqRCx1QkFBTyxLQUFQLENBQWEsb0JBQWI7QUFDQSx1QkFBTyxHQUFQLENBQVcsS0FBWDtBQUNBLG9CQUFJLE9BQU8sTUFBUCxDQUFjLFdBQWQsRUFBSixFQUFrQztBQUNoQyw2QkFBVyxPQUFPLE1BQVAsQ0FBYyxLQUF6QixFQUFnQyxTQUFTLGFBQXpDO0FBQ0Q7QUFDRjtBQUNELHFCQUFPLEtBQVAsQ0FBYSxvQkFBYixFQUFtQyxZQUFuQyxFQUFpRCxHQUFqRDtBQUNBLHVCQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsT0FBeEIsRUFBaUMsUUFBakMsRUFBMkMsT0FBM0MsRUFBb0QsR0FBcEQ7QUFDRDtBQTVETTtBQWpFSixTQWpZQTs7QUFrZ0JQLGdCQUFROztBQUVOLG1CQUFTLFlBQVc7O0FBRWxCLG1CQUFPLEVBQUUsUUFBRixHQUNKLE1BREksQ0FDRyxPQUFPLEtBQVAsQ0FBYSxPQUFiLENBQXFCLFFBRHhCLEVBRUosSUFGSSxDQUVDLE9BQU8sS0FBUCxDQUFhLE9BQWIsQ0FBcUIsSUFGdEIsRUFHSixJQUhJLENBR0MsT0FBTyxLQUFQLENBQWEsT0FBYixDQUFxQixJQUh0QixDQUFQO0FBS0QsV0FUSzs7QUFXTixxQkFBVyxZQUFZO0FBQ3JCOztBQUVFLHlCQUFpQixLQUZuQjtBQUFBLGdCQUdFLFNBQWlCLEtBSG5CO0FBQUEsZ0JBSUUsY0FBaUIsS0FKbkI7QUFBQSxnQkFLRSxZQUFpQixTQUFTLFlBQVQsSUFBOEIsU0FBUyxRQUwxRDtBQUFBLGdCQU1FLGlCQUFpQixTQUFTLGlCQUFULElBQThCLFNBQVMsYUFOMUQ7QUFBQSxnQkFPRSxhQVBGO0FBQUEsZ0JBUUUsUUFSRjtBQUFBLGdCQVNFLFNBVEY7O0FBWUEsd0JBQVksRUFBRSxRQUFGLEdBQ1QsTUFEUyxDQUNGLE9BQU8sS0FBUCxDQUFhLEdBQWIsQ0FBaUIsUUFEZixFQUVULElBRlMsQ0FFSixPQUFPLEtBQVAsQ0FBYSxHQUFiLENBQWlCLElBRmIsRUFHVCxJQUhTLENBR0osT0FBTyxLQUFQLENBQWEsR0FBYixDQUFpQixJQUhiLENBQVo7O0FBTUEsZ0JBQUcsU0FBSCxFQUFjO0FBQ1osa0JBQUksRUFBRSxVQUFGLENBQWEsU0FBYixDQUFKLEVBQThCO0FBQzVCLHVCQUFPLEtBQVAsQ0FBYSxzQ0FBYixFQUFxRCxTQUFyRDtBQUNBLDJCQUFXLFVBQVUsSUFBVixDQUFlLE9BQWYsRUFBd0IsZUFBeEIsQ0FBWDtBQUNELGVBSEQsTUFJSztBQUNILHVCQUFPLEtBQVAsQ0FBYSxtQ0FBYixFQUFrRCxTQUFsRDtBQUNBLDJCQUFXLFNBQVg7QUFDRDs7QUFFRCx3QkFBVSxXQUFWLENBQXNCLE9BQXRCLEVBQStCLENBQUUsUUFBRixFQUFZLFVBQVosRUFBd0IsRUFBRSxjQUFjLFFBQWhCLEVBQXhCLENBQS9CO0FBQ0QsYUFYRCxNQVlLLElBQUksRUFBRSxVQUFGLENBQWEsY0FBYixDQUFKLEVBQW1DO0FBQ3RDLDhCQUFnQixVQUFTLFFBQVQsRUFBbUI7QUFDakMsdUJBQU8sS0FBUCxDQUFhLGtDQUFiLEVBQWlELFFBQWpEOztBQUVBLG9CQUFHLFFBQUgsRUFBYTtBQUNYLDRCQUFVLFdBQVYsQ0FBc0IsT0FBdEIsRUFBK0IsQ0FBRSxRQUFGLEVBQVksVUFBWixFQUF3QixFQUFFLGNBQWMsUUFBaEIsRUFBeEIsQ0FBL0I7QUFDRCxpQkFGRCxNQUdLO0FBQ0gsNEJBQVUsVUFBVixDQUFxQixPQUFyQixFQUE4QixDQUFDLEVBQUUsY0FBYyxRQUFoQixFQUFELEVBQTZCLE1BQTdCLEVBQXFDLFdBQXJDLENBQTlCO0FBQ0Q7QUFDRixlQVREO0FBVUEscUJBQU8sS0FBUCxDQUFhLHlDQUFiLEVBQXdELGNBQXhEO0FBQ0EsNkJBQWUsSUFBZixDQUFvQixPQUFwQixFQUE2QixlQUE3QixFQUE4QyxhQUE5QztBQUNEO0FBQ0QsbUJBQU8sU0FBUDtBQUNELFdBekRLOztBQTJETixlQUFLLFlBQVc7QUFDZCxnQkFDRSxHQURGOztBQUlBLGtCQUFNLEVBQUUsSUFBRixDQUFPLFlBQVAsRUFDSCxNQURHLENBQ0ksT0FBTyxLQUFQLENBQWEsR0FBYixDQUFpQixNQURyQixFQUVILElBRkcsQ0FFRSxPQUFPLEtBQVAsQ0FBYSxHQUFiLENBQWlCLElBRm5CLEVBR0gsSUFIRyxDQUdFLE9BQU8sS0FBUCxDQUFhLEdBQWIsQ0FBaUIsSUFIbkIsQ0FBTjtBQUtBLG1CQUFPLE9BQVAsQ0FBZSx3QkFBZixFQUF5QyxHQUF6QyxFQUE4QyxZQUE5QztBQUNBLG1CQUFPLEdBQVA7QUFDRDtBQXZFSyxTQWxnQkQ7O0FBNGtCUCxhQUFLO0FBQ0gsaUJBQU8sWUFBVztBQUNoQixtQkFBTyxPQUFQLENBQWUsK0JBQWYsRUFBZ0QsUUFBaEQ7QUFDQSxxQkFBUyxRQUFULENBQWtCLFVBQVUsS0FBNUI7QUFDRCxXQUpFO0FBS0gsbUJBQVMsWUFBVztBQUNsQixtQkFBTyxPQUFQLENBQWUsaUNBQWYsRUFBa0QsUUFBbEQ7QUFDQSxxQkFBUyxRQUFULENBQWtCLFVBQVUsT0FBNUI7QUFDQSwrQkFBbUIsSUFBSSxJQUFKLEdBQVcsT0FBWCxFQUFuQjtBQUNEO0FBVEUsU0E1a0JFOztBQXdsQlAsZ0JBQVE7QUFDTixpQkFBTyxZQUFXO0FBQ2hCLG1CQUFPLE9BQVAsQ0FBZSxtQ0FBZixFQUFvRCxRQUFwRDtBQUNBLHFCQUFTLFdBQVQsQ0FBcUIsVUFBVSxLQUEvQjtBQUNELFdBSks7QUFLTixtQkFBUyxZQUFXO0FBQ2xCLG1CQUFPLE9BQVAsQ0FBZSxxQ0FBZixFQUFzRCxRQUF0RDtBQUNBLHFCQUFTLFdBQVQsQ0FBcUIsVUFBVSxPQUEvQjtBQUNEO0FBUkssU0F4bEJEOztBQW1tQlAsYUFBSztBQUNILDJCQUFpQixVQUFTLEdBQVQsRUFBYztBQUM3QixtQkFBTyxFQUFFLGFBQUYsQ0FBZ0IsR0FBaEIsSUFDRixPQUFPLEVBQVAsQ0FBVSxhQUFWLEVBQUQsR0FDRSxPQUFPLE1BQVAsQ0FBYyxJQUFkLENBQW1CLElBQUksWUFBdkIsQ0FERixHQUVFLElBQUksWUFISCxHQUlILEtBSko7QUFNRCxXQVJFO0FBU0gsNEJBQWtCLFVBQVMsUUFBVCxFQUFtQixNQUFuQixFQUEyQixXQUEzQixFQUF3QztBQUN4RCxtQkFBUSxFQUFFLGFBQUYsQ0FBZ0IsUUFBaEIsS0FBNkIsU0FBUyxLQUFULEtBQW1CLFNBQWpELEdBQ0gsU0FBUyxLO0FBRE4sY0FFRixTQUFTLEtBQVQsQ0FBZSxNQUFmLE1BQTJCLFNBQTVCLEc7QUFDRSxxQkFBUyxLQUFULENBQWUsTUFBZixDQURGLEdBRUUsV0FKTjtBQU1ELFdBaEJFO0FBaUJILG1CQUFTLFlBQVc7QUFDbEIsbUJBQU8sT0FBTyxPQUFQLElBQWtCLEtBQXpCO0FBQ0QsV0FuQkU7QUFvQkgsZUFBSyxZQUFXO0FBQ2QsbUJBQU8sT0FBTyxHQUFQLElBQWMsS0FBckI7QUFDRCxXQXRCRTtBQXVCSCxvQkFBVSxZQUFXO0FBQ25CLGdCQUNFLFdBREY7QUFHQSwwQkFBYyxTQUFTLFVBQVQsQ0FBb0IsSUFBcEIsQ0FBeUIsT0FBekIsRUFBa0MsUUFBbEMsQ0FBZDtBQUNBLGdCQUFHLFdBQUgsRUFBZ0I7QUFDZCxrQkFBRyxZQUFZLE9BQVosS0FBd0IsU0FBM0IsRUFBc0M7QUFDcEMsdUJBQU8sS0FBUCxDQUFhLGtDQUFiLEVBQWlELFdBQWpEO0FBQ0EsdUJBQU8sS0FBUCxDQUFhLE1BQU0sZ0JBQW5CLEVBQXFDLFlBQVksT0FBakQ7QUFDQSw0QkFBWSxTQUFaLEdBQXdCLFlBQVksT0FBcEM7QUFDRDtBQUNELGtCQUFHLFlBQVksT0FBWixLQUF3QixTQUEzQixFQUFzQztBQUNwQyx1QkFBTyxLQUFQLENBQWEsa0NBQWIsRUFBaUQsV0FBakQ7QUFDQSx1QkFBTyxLQUFQLENBQWEsTUFBTSxnQkFBbkIsRUFBcUMsWUFBWSxPQUFqRDtBQUNBLDRCQUFZLFNBQVosR0FBd0IsWUFBWSxPQUFwQztBQUNEO0FBQ0Qsa0JBQUcsWUFBWSxRQUFaLEtBQXlCLFNBQTVCLEVBQXVDO0FBQ3JDLHVCQUFPLEtBQVAsQ0FBYSxtQ0FBYixFQUFrRCxXQUFsRDtBQUNBLHVCQUFPLEtBQVAsQ0FBYSxNQUFNLGdCQUFuQixFQUFxQyxZQUFZLFFBQWpEO0FBQ0EsNEJBQVksVUFBWixHQUF5QixZQUFZLFFBQXJDO0FBQ0Q7QUFDRjtBQUNELGdCQUFHLGdCQUFnQixTQUFuQixFQUE4QjtBQUM1QixxQkFBTyxLQUFQLENBQWEsTUFBTSxlQUFuQjtBQUNEO0FBQ0QsZ0JBQUcsZ0JBQWdCLEtBQW5CLEVBQTBCO0FBQ3hCLHFCQUFPLFdBQVA7QUFDRDtBQUNELG1CQUFRLGdCQUFnQixTQUFqQixHQUNILEVBQUUsTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CLFdBQW5CLENBREcsR0FFSCxFQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQixRQUFuQixDQUZKO0FBSUQsV0F2REU7QUF3REgsMkJBQWlCLFVBQVMsS0FBVCxFQUFnQjtBQUMvQixnQkFDRSxlQUFpQixPQUFPLGtCQUFQLENBQTBCLEtBQTFCLENBRG5CO0FBQUEsZ0JBRUUsZUFBaUIsT0FBTyxrQkFBUCxDQUEwQixLQUExQixDQUZuQjtBQUFBLGdCQUdFLGlCQUFrQixpQkFBaUIsS0FIckM7QUFLQSxnQkFBRyxjQUFILEVBQW1CO0FBQ2pCLHFCQUFPLEtBQVAsQ0FBYSx3REFBYixFQUF1RSxLQUF2RTtBQUNBLHFCQUFPLEtBQVA7QUFDRDtBQUNELG1CQUFPLE9BQVAsQ0FBZSx5Q0FBZixFQUEwRCxLQUExRCxFQUFpRSxZQUFqRTtBQUNBLG1CQUFPLFlBQVA7QUFDRCxXQXBFRTtBQXFFSCx1QkFBYSxZQUFXO0FBQ3RCLGdCQUNFLE9BQU8sRUFEVDtBQUdBLGdCQUFJLENBQUMsRUFBRSxRQUFGLENBQVcsT0FBWCxDQUFMLEVBQTJCO0FBQ3pCLGtCQUFJLE9BQU8sRUFBUCxDQUFVLEtBQVYsRUFBSixFQUF3QjtBQUN0QixxQkFBSyxLQUFMLEdBQWEsUUFBUSxHQUFSLEVBQWI7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLEVBQVAsQ0FBVSxJQUFWLEVBQUosRUFBdUIsQ0FFM0IsQ0FGSSxNQUdBO0FBQ0gscUJBQUssSUFBTCxHQUFZLFFBQVEsSUFBUixFQUFaO0FBQ0Q7QUFDRjtBQUNELG1CQUFPLElBQVA7QUFDRCxXQXJGRTtBQXNGSCxpQkFBTyxZQUFXO0FBQ2hCLGdCQUFJLEVBQUUsUUFBRixDQUFXLE9BQVgsS0FBdUIsU0FBUyxFQUFULElBQWUsS0FBMUMsRUFBa0Q7QUFDaEQscUJBQU8sS0FBUCxDQUFhLGdEQUFiO0FBQ0EscUJBQU8sS0FBUDtBQUNELGFBSEQsTUFJSyxJQUFHLFNBQVMsRUFBVCxJQUFlLE1BQWxCLEVBQTBCO0FBQzdCLGtCQUFJLFFBQVEsRUFBUixDQUFXLE9BQVgsQ0FBSixFQUEwQjtBQUN4Qix1QkFBUSxRQUFRLE9BQVIsS0FBb0IsU0FBckIsR0FDSCxPQURHLEdBRUYsUUFBUSxnQkFBUixLQUE2QixTQUE5QixHQUNFLGdCQURGLEdBRUUsT0FKTjtBQU1ELGVBUEQsTUFRSyxJQUFJLFFBQVEsRUFBUixDQUFXLE1BQVgsQ0FBSixFQUF5QjtBQUM1Qix1QkFBTyxRQUFQO0FBQ0QsZUFGSSxNQUdBO0FBQ0gsdUJBQU8sT0FBUDtBQUNEO0FBQ0YsYUFmSSxNQWdCQTtBQUNILHFCQUFPLFNBQVMsRUFBaEI7QUFDRDtBQUNGLFdBOUdFO0FBK0dILHdCQUFjLFVBQVMsTUFBVCxFQUFpQjtBQUM3QixxQkFBUyxVQUFVLFFBQVEsSUFBUixDQUFhLFNBQVMsTUFBdEIsQ0FBVixJQUEyQyxTQUFTLE1BQXBELElBQThELEtBQXZFO0FBQ0Esa0JBQVMsUUFBUSxJQUFSLENBQWEsU0FBUyxHQUF0QixLQUE4QixTQUFTLEdBQXZDLElBQThDLEtBQXZEO0FBQ0EsZ0JBQUcsR0FBSCxFQUFRO0FBQ04scUJBQU8sS0FBUCxDQUFhLHFCQUFiLEVBQW9DLEdBQXBDO0FBQ0EscUJBQU8sR0FBUDtBQUNEO0FBQ0QsZ0JBQUcsTUFBSCxFQUFXO0FBQ1QscUJBQU8sS0FBUCxDQUFhLDJCQUFiLEVBQTBDLE1BQTFDLEVBQWtELFNBQVMsR0FBM0Q7QUFDQSxrQkFBRyxTQUFTLEdBQVQsQ0FBYSxNQUFiLE1BQXlCLFNBQXpCLElBQXNDLENBQUMsT0FBTyxFQUFQLENBQVUsTUFBVixFQUExQyxFQUE4RDtBQUM1RCx1QkFBTyxLQUFQLENBQWEsTUFBTSxhQUFuQixFQUFrQyxTQUFTLE1BQTNDLEVBQW1ELFNBQVMsR0FBNUQ7QUFDQTtBQUNEO0FBQ0Qsb0JBQU0sU0FBUyxHQUFULENBQWEsTUFBYixDQUFOO0FBQ0QsYUFQRCxNQVFLLElBQUksT0FBTyxFQUFQLENBQVUsSUFBVixFQUFKLEVBQXVCO0FBQzFCLG9CQUFNLFFBQVEsSUFBUixDQUFhLFFBQWIsS0FBMEIsU0FBUyxJQUFULENBQWMsUUFBZCxDQUExQixJQUFxRCxLQUEzRDtBQUNBLHFCQUFPLEtBQVAsQ0FBYSx1REFBYixFQUFzRSxHQUF0RTtBQUNEO0FBQ0QsbUJBQU8sR0FBUDtBQUNEO0FBbklFLFNBbm1CRTs7QUF5dUJQLGVBQU8sWUFBVztBQUNoQixjQUNFLE1BQU0sT0FBTyxHQUFQLENBQVcsR0FBWCxFQURSO0FBR0EsY0FBSSxPQUFPLElBQUksS0FBSixPQUFnQixVQUEzQixFQUF1QztBQUNyQyxtQkFBTyxLQUFQLENBQWEsd0JBQWI7QUFDQSxnQkFBSSxLQUFKO0FBQ0Q7QUFDRixTQWp2Qk07OztBQW92QlAsZUFBTyxZQUFXO0FBQ2hCLGlCQUFPLE1BQVAsQ0FBYyxLQUFkO0FBQ0EsaUJBQU8sTUFBUCxDQUFjLE9BQWQ7QUFDRCxTQXZ2Qk07O0FBeXZCUCxpQkFBUyxVQUFTLElBQVQsRUFBZSxLQUFmLEVBQXNCO0FBQzdCLGlCQUFPLEtBQVAsQ0FBYSxrQkFBYixFQUFpQyxJQUFqQyxFQUF1QyxLQUF2QztBQUNBLGNBQUksRUFBRSxhQUFGLENBQWdCLElBQWhCLENBQUosRUFBNEI7QUFDMUIsY0FBRSxNQUFGLENBQVMsSUFBVCxFQUFlLFFBQWYsRUFBeUIsSUFBekI7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsZ0JBQUcsRUFBRSxhQUFGLENBQWdCLFNBQVMsSUFBVCxDQUFoQixDQUFILEVBQW9DO0FBQ2xDLGdCQUFFLE1BQUYsQ0FBUyxJQUFULEVBQWUsU0FBUyxJQUFULENBQWYsRUFBK0IsS0FBL0I7QUFDRCxhQUZELE1BR0s7QUFDSCx1QkFBUyxJQUFULElBQWlCLEtBQWpCO0FBQ0Q7QUFDRixXQVBJLE1BUUE7QUFDSCxtQkFBTyxTQUFTLElBQVQsQ0FBUDtBQUNEO0FBQ0YsU0F6d0JNO0FBMHdCUCxrQkFBVSxVQUFTLElBQVQsRUFBZSxLQUFmLEVBQXNCO0FBQzlCLGNBQUksRUFBRSxhQUFGLENBQWdCLElBQWhCLENBQUosRUFBNEI7QUFDMUIsY0FBRSxNQUFGLENBQVMsSUFBVCxFQUFlLE1BQWYsRUFBdUIsSUFBdkI7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsbUJBQU8sSUFBUCxJQUFlLEtBQWY7QUFDRCxXQUZJLE1BR0E7QUFDSCxtQkFBTyxPQUFPLElBQVAsQ0FBUDtBQUNEO0FBQ0YsU0FweEJNO0FBcXhCUCxlQUFPLFlBQVc7QUFDaEIsY0FBRyxDQUFDLFNBQVMsTUFBVixJQUFvQixTQUFTLEtBQWhDLEVBQXVDO0FBQ3JDLGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2QixxQkFBTyxXQUFQLENBQW1CLEdBQW5CLENBQXVCLFNBQXZCO0FBQ0QsYUFGRCxNQUdLO0FBQ0gscUJBQU8sS0FBUCxHQUFlLFNBQVMsU0FBVCxDQUFtQixJQUFuQixDQUF3QixJQUF4QixDQUE2QixRQUFRLElBQXJDLEVBQTJDLE9BQTNDLEVBQW9ELFNBQVMsSUFBVCxHQUFnQixHQUFwRSxDQUFmO0FBQ0EscUJBQU8sS0FBUCxDQUFhLEtBQWIsQ0FBbUIsT0FBbkIsRUFBNEIsU0FBNUI7QUFDRDtBQUNGO0FBQ0YsU0EveEJNO0FBZ3lCUCxpQkFBUyxZQUFXO0FBQ2xCLGNBQUcsQ0FBQyxTQUFTLE1BQVYsSUFBb0IsU0FBUyxPQUE3QixJQUF3QyxTQUFTLEtBQXBELEVBQTJEO0FBQ3pELGdCQUFHLFNBQVMsV0FBWixFQUF5QjtBQUN2QixxQkFBTyxXQUFQLENBQW1CLEdBQW5CLENBQXVCLFNBQXZCO0FBQ0QsYUFGRCxNQUdLO0FBQ0gscUJBQU8sT0FBUCxHQUFpQixTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxJQUFyQyxFQUEyQyxPQUEzQyxFQUFvRCxTQUFTLElBQVQsR0FBZ0IsR0FBcEUsQ0FBakI7QUFDQSxxQkFBTyxPQUFQLENBQWUsS0FBZixDQUFxQixPQUFyQixFQUE4QixTQUE5QjtBQUNEO0FBQ0Y7QUFDRixTQTF5Qk07QUEyeUJQLGVBQU8sWUFBVztBQUNoQixjQUFHLENBQUMsU0FBUyxNQUFiLEVBQXFCO0FBQ25CLG1CQUFPLEtBQVAsR0FBZSxTQUFTLFNBQVQsQ0FBbUIsSUFBbkIsQ0FBd0IsSUFBeEIsQ0FBNkIsUUFBUSxLQUFyQyxFQUE0QyxPQUE1QyxFQUFxRCxTQUFTLElBQVQsR0FBZ0IsR0FBckUsQ0FBZjtBQUNBLG1CQUFPLEtBQVAsQ0FBYSxLQUFiLENBQW1CLE9BQW5CLEVBQTRCLFNBQTVCO0FBQ0Q7QUFDRixTQWh6Qk07QUFpekJQLHFCQUFhO0FBQ1gsZUFBSyxVQUFTLE9BQVQsRUFBa0I7QUFDckIsZ0JBQ0UsV0FERixFQUVFLGFBRkYsRUFHRSxZQUhGO0FBS0EsZ0JBQUcsU0FBUyxXQUFaLEVBQXlCO0FBQ3ZCLDRCQUFnQixJQUFJLElBQUosR0FBVyxPQUFYLEVBQWhCO0FBQ0EsNkJBQWdCLFFBQVEsV0FBeEI7QUFDQSw4QkFBZ0IsY0FBYyxZQUE5QjtBQUNBLHFCQUFnQixXQUFoQjtBQUNBLDBCQUFZLElBQVosQ0FBaUI7QUFDZix3QkFBbUIsUUFBUSxDQUFSLENBREo7QUFFZiw2QkFBbUIsR0FBRyxLQUFILENBQVMsSUFBVCxDQUFjLE9BQWQsRUFBdUIsQ0FBdkIsS0FBNkIsRUFGakM7O0FBSWYsa0NBQW1CO0FBSkosZUFBakI7QUFNRDtBQUNELHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLG1CQUFPLFdBQVAsQ0FBbUIsS0FBbkIsR0FBMkIsV0FBVyxPQUFPLFdBQVAsQ0FBbUIsT0FBOUIsRUFBdUMsR0FBdkMsQ0FBM0I7QUFDRCxXQXJCVTtBQXNCWCxtQkFBUyxZQUFXO0FBQ2xCLGdCQUNFLFFBQVEsU0FBUyxJQUFULEdBQWdCLEdBRDFCO0FBQUEsZ0JBRUUsWUFBWSxDQUZkO0FBSUEsbUJBQU8sS0FBUDtBQUNBLHlCQUFhLE9BQU8sV0FBUCxDQUFtQixLQUFoQztBQUNBLGNBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDJCQUFhLEtBQUssZ0JBQUwsQ0FBYjtBQUNELGFBRkQ7QUFHQSxxQkFBUyxNQUFNLFNBQU4sR0FBa0IsSUFBM0I7QUFDQSxnQkFBRyxjQUFILEVBQW1CO0FBQ2pCLHVCQUFTLFFBQVEsY0FBUixHQUF5QixJQUFsQztBQUNEO0FBQ0QsZ0JBQUksQ0FBQyxRQUFRLEtBQVIsS0FBa0IsU0FBbEIsSUFBK0IsUUFBUSxLQUFSLEtBQWtCLFNBQWxELEtBQWdFLFlBQVksTUFBWixHQUFxQixDQUF6RixFQUE0RjtBQUMxRixzQkFBUSxjQUFSLENBQXVCLEtBQXZCO0FBQ0Esa0JBQUcsUUFBUSxLQUFYLEVBQWtCO0FBQ2hCLHdCQUFRLEtBQVIsQ0FBYyxXQUFkO0FBQ0QsZUFGRCxNQUdLO0FBQ0gsa0JBQUUsSUFBRixDQUFPLFdBQVAsRUFBb0IsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3hDLDBCQUFRLEdBQVIsQ0FBWSxLQUFLLE1BQUwsSUFBZSxJQUFmLEdBQXNCLEtBQUssZ0JBQUwsQ0FBdEIsR0FBNkMsSUFBekQ7QUFDRCxpQkFGRDtBQUdEO0FBQ0Qsc0JBQVEsUUFBUjtBQUNEO0FBQ0QsMEJBQWMsRUFBZDtBQUNEO0FBakRVLFNBanpCTjtBQW8yQlAsZ0JBQVEsVUFBUyxLQUFULEVBQWdCLGVBQWhCLEVBQWlDLE9BQWpDLEVBQTBDO0FBQ2hELGNBQ0UsU0FBUyxRQURYO0FBQUEsY0FFRSxRQUZGO0FBQUEsY0FHRSxLQUhGO0FBQUEsY0FJRSxRQUpGO0FBTUEsNEJBQWtCLG1CQUFtQixjQUFyQztBQUNBLG9CQUFrQixXQUFtQixPQUFyQztBQUNBLGNBQUcsT0FBTyxLQUFQLElBQWdCLFFBQWhCLElBQTRCLFdBQVcsU0FBMUMsRUFBcUQ7QUFDbkQsb0JBQVcsTUFBTSxLQUFOLENBQVksT0FBWixDQUFYO0FBQ0EsdUJBQVcsTUFBTSxNQUFOLEdBQWUsQ0FBMUI7QUFDQSxjQUFFLElBQUYsQ0FBTyxLQUFQLEVBQWMsVUFBUyxLQUFULEVBQWdCLEtBQWhCLEVBQXVCO0FBQ25DLGtCQUFJLGlCQUFrQixTQUFTLFFBQVYsR0FDakIsUUFBUSxNQUFNLFFBQVEsQ0FBZCxFQUFpQixNQUFqQixDQUF3QixDQUF4QixFQUEyQixXQUEzQixFQUFSLEdBQW1ELE1BQU0sUUFBUSxDQUFkLEVBQWlCLEtBQWpCLENBQXVCLENBQXZCLENBRGxDLEdBRWpCLEtBRko7QUFJQSxrQkFBSSxFQUFFLGFBQUYsQ0FBaUIsT0FBTyxjQUFQLENBQWpCLEtBQThDLFNBQVMsUUFBM0QsRUFBdUU7QUFDckUseUJBQVMsT0FBTyxjQUFQLENBQVQ7QUFDRCxlQUZELE1BR0ssSUFBSSxPQUFPLGNBQVAsTUFBMkIsU0FBL0IsRUFBMkM7QUFDOUMsd0JBQVEsT0FBTyxjQUFQLENBQVI7QUFDQSx1QkFBTyxLQUFQO0FBQ0QsZUFISSxNQUlBLElBQUksRUFBRSxhQUFGLENBQWlCLE9BQU8sS0FBUCxDQUFqQixLQUFxQyxTQUFTLFFBQWxELEVBQThEO0FBQ2pFLHlCQUFTLE9BQU8sS0FBUCxDQUFUO0FBQ0QsZUFGSSxNQUdBLElBQUksT0FBTyxLQUFQLE1BQWtCLFNBQXRCLEVBQWtDO0FBQ3JDLHdCQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsdUJBQU8sS0FBUDtBQUNELGVBSEksTUFJQTtBQUNILHVCQUFPLEtBQVAsQ0FBYSxNQUFNLE1BQW5CLEVBQTJCLEtBQTNCO0FBQ0EsdUJBQU8sS0FBUDtBQUNEO0FBQ0YsYUF2QkQ7QUF3QkQ7QUFDRCxjQUFLLEVBQUUsVUFBRixDQUFjLEtBQWQsQ0FBTCxFQUE2QjtBQUMzQix1QkFBVyxNQUFNLEtBQU4sQ0FBWSxPQUFaLEVBQXFCLGVBQXJCLENBQVg7QUFDRCxXQUZELE1BR0ssSUFBRyxVQUFVLFNBQWIsRUFBd0I7QUFDM0IsdUJBQVcsS0FBWDtBQUNEO0FBQ0QsY0FBRyxFQUFFLE9BQUYsQ0FBVSxhQUFWLENBQUgsRUFBNkI7QUFDM0IsMEJBQWMsSUFBZCxDQUFtQixRQUFuQjtBQUNELFdBRkQsTUFHSyxJQUFHLGtCQUFrQixTQUFyQixFQUFnQztBQUNuQyw0QkFBZ0IsQ0FBQyxhQUFELEVBQWdCLFFBQWhCLENBQWhCO0FBQ0QsV0FGSSxNQUdBLElBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUM5Qiw0QkFBZ0IsUUFBaEI7QUFDRDtBQUNELGlCQUFPLEtBQVA7QUFDRDtBQXo1Qk0sT0FBVDs7QUE0NUJBLFVBQUcsYUFBSCxFQUFrQjtBQUNoQixZQUFHLGFBQWEsU0FBaEIsRUFBMkI7QUFDekIsaUJBQU8sVUFBUDtBQUNEO0FBQ0QsZUFBTyxNQUFQLENBQWMsS0FBZDtBQUNELE9BTEQsTUFNSztBQUNILFlBQUcsYUFBYSxTQUFoQixFQUEyQjtBQUN6QixtQkFBUyxNQUFULENBQWdCLFNBQWhCO0FBQ0Q7QUFDRCxlQUFPLFVBQVA7QUFDRDtBQUNGLEtBajlCSDs7QUFvOUJBLFdBQVEsa0JBQWtCLFNBQW5CLEdBQ0gsYUFERyxHQUVILElBRko7QUFJRCxHQTErQkQ7O0FBNCtCQSxJQUFFLEdBQUYsQ0FBTSxRQUFOLEdBQWlCOztBQUVmLFVBQW9CLEtBRkw7QUFHZixlQUFvQixLQUhMOztBQUtmLFdBQW9CLEtBTEw7QUFNZixhQUFvQixLQU5MO0FBT2YsaUJBQW9CLElBUEw7OztBQVVmLFNBQW9CLEVBVkw7OztBQWFmLFdBQW9CLElBYkw7OztBQWdCZix1QkFBb0IsSUFoQkw7OztBQW1CZixRQUFvQixNQW5CTDs7O0FBc0JmLGtCQUFvQixLQXRCTDs7O0FBeUJmLHFCQUFvQixDQXpCTDs7O0FBNEJmLGVBQW9CLE1BNUJMOzs7QUErQmYsbUJBQW9CLElBL0JMOzs7QUFrQ2Ysc0JBQW9CLElBbENMOzs7QUFxQ2YsWUFBb0IsS0FyQ0w7OztBQXdDZixTQUFvQixLQXhDTDs7O0FBMkNmLFVBQW9CLEVBM0NMOzs7QUE4Q2YsYUFBb0IsRUE5Q0w7OztBQWlEZixpQkFBdUIsSUFqRFI7OztBQW9EZixtQkFBdUIsS0FwRFI7OztBQXVEZixjQUF1QixDQXZEUjs7O0FBMERmLDBCQUF1QixJQTFEUjs7O0FBNkRmLFlBQW9CLEtBN0RMO0FBOERmLFVBQW9CLEVBOURMO0FBK0RmLGNBQW9CLE1BL0RMOzs7QUFrRWYsa0JBQW9CLEtBbEVMO0FBbUVmLHVCQUFvQixLQW5FTDs7O0FBc0VmLGNBQW9CLEtBdEVMO0FBdUVmLG1CQUFvQixLQXZFTDs7O0FBMEVmLGdCQUFjLFVBQVMsUUFBVCxFQUFtQjtBQUFFLGFBQU8sUUFBUDtBQUFrQixLQTFFdEM7QUEyRWYsZUFBYyxVQUFTLEdBQVQsRUFBYyxDQUFFLENBM0VmO0FBNEVmLGVBQWMsVUFBUyxPQUFULEVBQWtCLEdBQWxCLEVBQXVCLENBQUUsQ0E1RXhCOzs7QUErRWYsZ0JBQWMsS0EvRUMsRTs7O0FBa0ZmLGVBQWMsVUFBUyxRQUFULEVBQW1CLE9BQW5CLEVBQTRCLENBQUUsQ0FsRjdCOzs7QUFxRmYsZ0JBQWMsVUFBUyxRQUFULEVBQW1CLE9BQW5CLEVBQTRCLENBQUUsQ0FyRjdCOzs7QUF3RmYsZUFBYyxVQUFTLFFBQVQsRUFBbUIsT0FBbkIsRUFBNEIsQ0FBRSxDQXhGN0I7OztBQTJGZixhQUFjLFVBQVMsWUFBVCxFQUF1QixPQUF2QixFQUFnQyxDQUFFLENBM0ZqQzs7O0FBOEZmLGFBQWMsVUFBUyxZQUFULEVBQXVCLE9BQXZCLEVBQWdDLENBQUUsQ0E5RmpDOztBQWdHZixpQkFBYyxLQWhHQzs7O0FBbUdmLFdBQVE7QUFDTixrQkFBb0Isa0RBRGQ7QUFFTixhQUFvQixzQ0FGZDtBQUdOLHNCQUFvQiwwQ0FIZDtBQUlOLGlCQUFvQixnREFKZDtBQUtOLHdCQUFvQixpREFMZDtBQU1OLGNBQW9CLHNDQU5kO0FBT04scUJBQW9CLHdDQVBkO0FBUU4sd0JBQW9CLGlGQVJkO0FBU04sa0JBQW9CLGdDQVRkO0FBVU4sdUJBQW9CLDRFQVZkO0FBV04saUJBQW9CLG9EQVhkO0FBWU4sa0JBQW9CLHlDQVpkO0FBYU4seUJBQW9CLG9DQWJkO0FBY04scUJBQW9CLHdCQWRkO0FBZU4sZUFBb0I7QUFmZCxLQW5HTzs7QUFxSGYsWUFBVTtBQUNSLGdCQUFXLG1CQURIO0FBRVIsZ0JBQVc7QUFGSCxLQXJISzs7QUEwSGYsZUFBVztBQUNULGVBQVUsU0FERDtBQUVULGFBQVU7QUFGRCxLQTFISTs7QUErSGYsY0FBVTtBQUNSLGdCQUFXLFdBREg7QUFFUixZQUFZO0FBRkosS0EvSEs7O0FBb0lmLGNBQVU7QUFDUixjQUFVLFFBREY7QUFFUixXQUFVO0FBRkY7QUFwSUssR0FBakI7QUE0SUMsQ0Fwb0NBLEVBb29DRyxNQXBvQ0gsRUFvb0NXLE1BcG9DWCxFQW9vQ21CLFFBcG9DbkIiLCJmaWxlIjoiYXBpLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXG4gKiAjIFNlbWFudGljIFVJIC0gQVBJXG4gKiBodHRwOi8vZ2l0aHViLmNvbS9zZW1hbnRpYy1vcmcvc2VtYW50aWMtdWkvXG4gKlxuICpcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxuICogaHR0cDovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVFxuICpcbiAqL1xuXG47KGZ1bmN0aW9uICgkLCB3aW5kb3csIGRvY3VtZW50LCB1bmRlZmluZWQpIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhclxuICB3aW5kb3cgPSAodHlwZW9mIHdpbmRvdyAhPSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuTWF0aCA9PSBNYXRoKVxuICAgID8gd2luZG93XG4gICAgOiAodHlwZW9mIHNlbGYgIT0gJ3VuZGVmaW5lZCcgJiYgc2VsZi5NYXRoID09IE1hdGgpXG4gICAgICA/IHNlbGZcbiAgICAgIDogRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKVxuO1xuXG4kLmFwaSA9ICQuZm4uYXBpID0gZnVuY3Rpb24ocGFyYW1ldGVycykge1xuXG4gIHZhclxuICAgIC8vIHVzZSB3aW5kb3cgY29udGV4dCBpZiBub25lIHNwZWNpZmllZFxuICAgICRhbGxNb2R1bGVzICAgICA9ICQuaXNGdW5jdGlvbih0aGlzKVxuICAgICAgICA/ICQod2luZG93KVxuICAgICAgICA6ICQodGhpcyksXG4gICAgbW9kdWxlU2VsZWN0b3IgPSAkYWxsTW9kdWxlcy5zZWxlY3RvciB8fCAnJyxcbiAgICB0aW1lICAgICAgICAgICA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpLFxuICAgIHBlcmZvcm1hbmNlICAgID0gW10sXG5cbiAgICBxdWVyeSAgICAgICAgICA9IGFyZ3VtZW50c1swXSxcbiAgICBtZXRob2RJbnZva2VkICA9ICh0eXBlb2YgcXVlcnkgPT0gJ3N0cmluZycpLFxuICAgIHF1ZXJ5QXJndW1lbnRzID0gW10uc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpLFxuXG4gICAgcmV0dXJuZWRWYWx1ZVxuICA7XG5cbiAgJGFsbE1vZHVsZXNcbiAgICAuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgIHZhclxuICAgICAgICBzZXR0aW5ncyAgICAgICAgICA9ICggJC5pc1BsYWluT2JqZWN0KHBhcmFtZXRlcnMpIClcbiAgICAgICAgICA/ICQuZXh0ZW5kKHRydWUsIHt9LCAkLmZuLmFwaS5zZXR0aW5ncywgcGFyYW1ldGVycylcbiAgICAgICAgICA6ICQuZXh0ZW5kKHt9LCAkLmZuLmFwaS5zZXR0aW5ncyksXG5cbiAgICAgICAgLy8gaW50ZXJuYWwgYWxpYXNlc1xuICAgICAgICBuYW1lc3BhY2UgICAgICAgPSBzZXR0aW5ncy5uYW1lc3BhY2UsXG4gICAgICAgIG1ldGFkYXRhICAgICAgICA9IHNldHRpbmdzLm1ldGFkYXRhLFxuICAgICAgICBzZWxlY3RvciAgICAgICAgPSBzZXR0aW5ncy5zZWxlY3RvcixcbiAgICAgICAgZXJyb3IgICAgICAgICAgID0gc2V0dGluZ3MuZXJyb3IsXG4gICAgICAgIGNsYXNzTmFtZSAgICAgICA9IHNldHRpbmdzLmNsYXNzTmFtZSxcblxuICAgICAgICAvLyBkZWZpbmUgbmFtZXNwYWNlcyBmb3IgbW9kdWxlc1xuICAgICAgICBldmVudE5hbWVzcGFjZSAgPSAnLicgKyBuYW1lc3BhY2UsXG4gICAgICAgIG1vZHVsZU5hbWVzcGFjZSA9ICdtb2R1bGUtJyArIG5hbWVzcGFjZSxcblxuICAgICAgICAvLyBlbGVtZW50IHRoYXQgY3JlYXRlcyByZXF1ZXN0XG4gICAgICAgICRtb2R1bGUgICAgICAgICA9ICQodGhpcyksXG4gICAgICAgICRmb3JtICAgICAgICAgICA9ICRtb2R1bGUuY2xvc2VzdChzZWxlY3Rvci5mb3JtKSxcblxuICAgICAgICAvLyBjb250ZXh0IHVzZWQgZm9yIHN0YXRlXG4gICAgICAgICRjb250ZXh0ICAgICAgICA9IChzZXR0aW5ncy5zdGF0ZUNvbnRleHQpXG4gICAgICAgICAgPyAkKHNldHRpbmdzLnN0YXRlQ29udGV4dClcbiAgICAgICAgICA6ICRtb2R1bGUsXG5cbiAgICAgICAgLy8gcmVxdWVzdCBkZXRhaWxzXG4gICAgICAgIGFqYXhTZXR0aW5ncyxcbiAgICAgICAgcmVxdWVzdFNldHRpbmdzLFxuICAgICAgICB1cmwsXG4gICAgICAgIGRhdGEsXG4gICAgICAgIHJlcXVlc3RTdGFydFRpbWUsXG5cbiAgICAgICAgLy8gc3RhbmRhcmQgbW9kdWxlXG4gICAgICAgIGVsZW1lbnQgICAgICAgICA9IHRoaXMsXG4gICAgICAgIGNvbnRleHQgICAgICAgICA9ICRjb250ZXh0WzBdLFxuICAgICAgICBpbnN0YW5jZSAgICAgICAgPSAkbW9kdWxlLmRhdGEobW9kdWxlTmFtZXNwYWNlKSxcbiAgICAgICAgbW9kdWxlXG4gICAgICA7XG5cbiAgICAgIG1vZHVsZSA9IHtcblxuICAgICAgICBpbml0aWFsaXplOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighbWV0aG9kSW52b2tlZCkge1xuICAgICAgICAgICAgbW9kdWxlLmJpbmQuZXZlbnRzKCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIG1vZHVsZS5pbnN0YW50aWF0ZSgpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGluc3RhbnRpYXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnU3RvcmluZyBpbnN0YW5jZSBvZiBtb2R1bGUnLCBtb2R1bGUpO1xuICAgICAgICAgIGluc3RhbmNlID0gbW9kdWxlO1xuICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgIC5kYXRhKG1vZHVsZU5hbWVzcGFjZSwgaW5zdGFuY2UpXG4gICAgICAgICAgO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlc3Ryb3k6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdEZXN0cm95aW5nIHByZXZpb3VzIG1vZHVsZSBmb3InLCBlbGVtZW50KTtcbiAgICAgICAgICAkbW9kdWxlXG4gICAgICAgICAgICAucmVtb3ZlRGF0YShtb2R1bGVOYW1lc3BhY2UpXG4gICAgICAgICAgICAub2ZmKGV2ZW50TmFtZXNwYWNlKVxuICAgICAgICAgIDtcbiAgICAgICAgfSxcblxuICAgICAgICBiaW5kOiB7XG4gICAgICAgICAgZXZlbnRzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICB0cmlnZ2VyRXZlbnQgPSBtb2R1bGUuZ2V0LmV2ZW50KClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKCB0cmlnZ2VyRXZlbnQgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdBdHRhY2hpbmcgQVBJIGV2ZW50cyB0byBlbGVtZW50JywgdHJpZ2dlckV2ZW50KTtcbiAgICAgICAgICAgICAgJG1vZHVsZVxuICAgICAgICAgICAgICAgIC5vbih0cmlnZ2VyRXZlbnQgKyBldmVudE5hbWVzcGFjZSwgbW9kdWxlLmV2ZW50LnRyaWdnZXIpXG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoc2V0dGluZ3Mub24gPT0gJ25vdycpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdRdWVyeWluZyBBUEkgZW5kcG9pbnQgaW1tZWRpYXRlbHknKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnF1ZXJ5KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGRlY29kZToge1xuICAgICAgICAgIGpzb246IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICBpZihyZXNwb25zZSAhPT0gdW5kZWZpbmVkICYmIHR5cGVvZiByZXNwb25zZSA9PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgcmVzcG9uc2UgPSBKU09OLnBhcnNlKHJlc3BvbnNlKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBjYXRjaChlKSB7XG4gICAgICAgICAgICAgICAgLy8gaXNudCBqc29uIHN0cmluZ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlYWQ6IHtcbiAgICAgICAgICBjYWNoZWRSZXNwb25zZTogZnVuY3Rpb24odXJsKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgcmVzcG9uc2VcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHdpbmRvdy5TdG9yYWdlID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm5vU3RvcmFnZSk7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJlc3BvbnNlID0gc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbSh1cmwpO1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdVc2luZyBjYWNoZWQgcmVzcG9uc2UnLCB1cmwsIHJlc3BvbnNlKTtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gbW9kdWxlLmRlY29kZS5qc29uKHJlc3BvbnNlKTtcbiAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHdyaXRlOiB7XG4gICAgICAgICAgY2FjaGVkUmVzcG9uc2U6IGZ1bmN0aW9uKHVybCwgcmVzcG9uc2UpIHtcbiAgICAgICAgICAgIGlmKHJlc3BvbnNlICYmIHJlc3BvbnNlID09PSAnJykge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Jlc3BvbnNlIGVtcHR5LCBub3QgY2FjaGluZycsIHJlc3BvbnNlKTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYod2luZG93LlN0b3JhZ2UgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3Iubm9TdG9yYWdlKTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChyZXNwb25zZSkgKSB7XG4gICAgICAgICAgICAgIHJlc3BvbnNlID0gSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSh1cmwsIHJlc3BvbnNlKTtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdTdG9yaW5nIGNhY2hlZCByZXNwb25zZSBmb3IgdXJsJywgdXJsLCByZXNwb25zZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHF1ZXJ5OiBmdW5jdGlvbigpIHtcblxuICAgICAgICAgIGlmKG1vZHVsZS5pcy5kaXNhYmxlZCgpKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0VsZW1lbnQgaXMgZGlzYWJsZWQgQVBJIHJlcXVlc3QgYWJvcnRlZCcpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmKG1vZHVsZS5pcy5sb2FkaW5nKCkpIHtcbiAgICAgICAgICAgIGlmKHNldHRpbmdzLmludGVycnVwdFJlcXVlc3RzKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW50ZXJydXB0aW5nIHByZXZpb3VzIHJlcXVlc3QnKTtcbiAgICAgICAgICAgICAgbW9kdWxlLmFib3J0KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDYW5jZWxsaW5nIHJlcXVlc3QsIHByZXZpb3VzIHJlcXVlc3QgaXMgc3RpbGwgcGVuZGluZycpO1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gcGFzcyBlbGVtZW50IG1ldGFkYXRhIHRvIHVybCAodmFsdWUsIHRleHQpXG4gICAgICAgICAgaWYoc2V0dGluZ3MuZGVmYXVsdERhdGEpIHtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIHNldHRpbmdzLnVybERhdGEsIG1vZHVsZS5nZXQuZGVmYXVsdERhdGEoKSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gQWRkIGZvcm0gY29udGVudFxuICAgICAgICAgIGlmKHNldHRpbmdzLnNlcmlhbGl6ZUZvcm0pIHtcbiAgICAgICAgICAgIHNldHRpbmdzLmRhdGEgPSBtb2R1bGUuYWRkLmZvcm1EYXRhKHNldHRpbmdzLmRhdGEpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIC8vIGNhbGwgYmVmb3Jlc2VuZCBhbmQgZ2V0IGFueSBzZXR0aW5ncyBjaGFuZ2VzXG4gICAgICAgICAgcmVxdWVzdFNldHRpbmdzID0gbW9kdWxlLmdldC5zZXR0aW5ncygpO1xuXG4gICAgICAgICAgLy8gY2hlY2sgaWYgYmVmb3JlIHNlbmQgY2FuY2VsbGVkIHJlcXVlc3RcbiAgICAgICAgICBpZihyZXF1ZXN0U2V0dGluZ3MgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICBtb2R1bGUuY2FuY2VsbGVkID0gdHJ1ZTtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5iZWZvcmVTZW5kKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBtb2R1bGUuY2FuY2VsbGVkID0gZmFsc2U7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gZ2V0IHVybFxuICAgICAgICAgIHVybCA9IG1vZHVsZS5nZXQudGVtcGxhdGVkVVJMKCk7XG5cbiAgICAgICAgICBpZighdXJsICYmICFtb2R1bGUuaXMubW9ja2VkKCkpIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5taXNzaW5nVVJMKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAvLyByZXBsYWNlIHZhcmlhYmxlc1xuICAgICAgICAgIHVybCA9IG1vZHVsZS5hZGQudXJsRGF0YSggdXJsICk7XG4gICAgICAgICAgLy8gbWlzc2luZyB1cmwgcGFyYW1ldGVyc1xuICAgICAgICAgIGlmKCAhdXJsICYmICFtb2R1bGUuaXMubW9ja2VkKCkpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICByZXF1ZXN0U2V0dGluZ3MudXJsID0gc2V0dGluZ3MuYmFzZSArIHVybDtcblxuICAgICAgICAgIC8vIGxvb2sgZm9yIGpRdWVyeSBhamF4IHBhcmFtZXRlcnMgaW4gc2V0dGluZ3NcbiAgICAgICAgICBhamF4U2V0dGluZ3MgPSAkLmV4dGVuZCh0cnVlLCB7fSwgc2V0dGluZ3MsIHtcbiAgICAgICAgICAgIHR5cGUgICAgICAgOiBzZXR0aW5ncy5tZXRob2QgfHwgc2V0dGluZ3MudHlwZSxcbiAgICAgICAgICAgIGRhdGEgICAgICAgOiBkYXRhLFxuICAgICAgICAgICAgdXJsICAgICAgICA6IHNldHRpbmdzLmJhc2UgKyB1cmwsXG4gICAgICAgICAgICBiZWZvcmVTZW5kIDogc2V0dGluZ3MuYmVmb3JlWEhSLFxuICAgICAgICAgICAgc3VjY2VzcyAgICA6IGZ1bmN0aW9uKCkge30sXG4gICAgICAgICAgICBmYWlsdXJlICAgIDogZnVuY3Rpb24oKSB7fSxcbiAgICAgICAgICAgIGNvbXBsZXRlICAgOiBmdW5jdGlvbigpIHt9XG4gICAgICAgICAgfSk7XG5cbiAgICAgICAgICBtb2R1bGUuZGVidWcoJ1F1ZXJ5aW5nIFVSTCcsIGFqYXhTZXR0aW5ncy51cmwpO1xuICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdVc2luZyBBSkFYIHNldHRpbmdzJywgYWpheFNldHRpbmdzKTtcbiAgICAgICAgICBpZihzZXR0aW5ncy5jYWNoZSA9PT0gJ2xvY2FsJyAmJiBtb2R1bGUucmVhZC5jYWNoZWRSZXNwb25zZSh1cmwpKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Jlc3BvbnNlIHJldHVybmVkIGZyb20gbG9jYWwgY2FjaGUnKTtcbiAgICAgICAgICAgIG1vZHVsZS5yZXF1ZXN0ID0gbW9kdWxlLmNyZWF0ZS5yZXF1ZXN0KCk7XG4gICAgICAgICAgICBtb2R1bGUucmVxdWVzdC5yZXNvbHZlV2l0aChjb250ZXh0LCBbIG1vZHVsZS5yZWFkLmNhY2hlZFJlc3BvbnNlKHVybCkgXSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYoICFzZXR0aW5ncy50aHJvdHRsZSApIHtcbiAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2VuZGluZyByZXF1ZXN0JywgZGF0YSwgYWpheFNldHRpbmdzLm1ldGhvZCk7XG4gICAgICAgICAgICBtb2R1bGUuc2VuZC5yZXF1ZXN0KCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgaWYoIXNldHRpbmdzLnRocm90dGxlRmlyc3RSZXF1ZXN0ICYmICFtb2R1bGUudGltZXIpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTZW5kaW5nIHJlcXVlc3QnLCBkYXRhLCBhamF4U2V0dGluZ3MubWV0aG9kKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnNlbmQucmVxdWVzdCgpO1xuICAgICAgICAgICAgICBtb2R1bGUudGltZXIgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7fSwgc2V0dGluZ3MudGhyb3R0bGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVGhyb3R0bGluZyByZXF1ZXN0Jywgc2V0dGluZ3MudGhyb3R0bGUpO1xuICAgICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnRpbWVyKTtcbiAgICAgICAgICAgICAgbW9kdWxlLnRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZihtb2R1bGUudGltZXIpIHtcbiAgICAgICAgICAgICAgICAgIGRlbGV0ZSBtb2R1bGUudGltZXI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnU2VuZGluZyB0aHJvdHRsZWQgcmVxdWVzdCcsIGRhdGEsIGFqYXhTZXR0aW5ncy5tZXRob2QpO1xuICAgICAgICAgICAgICAgIG1vZHVsZS5zZW5kLnJlcXVlc3QoKTtcbiAgICAgICAgICAgICAgfSwgc2V0dGluZ3MudGhyb3R0bGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cblxuICAgICAgICB9LFxuXG4gICAgICAgIHNob3VsZDoge1xuICAgICAgICAgIHJlbW92ZUVycm9yOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAoIHNldHRpbmdzLmhpZGVFcnJvciA9PT0gdHJ1ZSB8fCAoc2V0dGluZ3MuaGlkZUVycm9yID09PSAnYXV0bycgJiYgIW1vZHVsZS5pcy5mb3JtKCkpICk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGlzOiB7XG4gICAgICAgICAgZGlzYWJsZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuICgkbW9kdWxlLmZpbHRlcihzZWxlY3Rvci5kaXNhYmxlZCkubGVuZ3RoID4gMCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBleHBlY3RpbmdKU09OOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBzZXR0aW5ncy5kYXRhVHlwZSA9PT0gJ2pzb24nIHx8IHNldHRpbmdzLmRhdGFUeXBlID09PSAnanNvbnAnO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZm9ybTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5pcygnZm9ybScpIHx8ICRjb250ZXh0LmlzKCdmb3JtJyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBtb2NrZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIChzZXR0aW5ncy5tb2NrUmVzcG9uc2UgfHwgc2V0dGluZ3MubW9ja1Jlc3BvbnNlQXN5bmMgfHwgc2V0dGluZ3MucmVzcG9uc2UgfHwgc2V0dGluZ3MucmVzcG9uc2VBc3luYyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBpbnB1dDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJG1vZHVsZS5pcygnaW5wdXQnKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGxvYWRpbmc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIChtb2R1bGUucmVxdWVzdClcbiAgICAgICAgICAgICAgPyAobW9kdWxlLnJlcXVlc3Quc3RhdGUoKSA9PSAncGVuZGluZycpXG4gICAgICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFib3J0ZWRSZXF1ZXN0OiBmdW5jdGlvbih4aHIpIHtcbiAgICAgICAgICAgIGlmKHhociAmJiB4aHIucmVhZHlTdGF0ZSAhPT0gdW5kZWZpbmVkICYmIHhoci5yZWFkeVN0YXRlID09PSAwKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdYSFIgcmVxdWVzdCBkZXRlcm1pbmVkIHRvIGJlIGFib3J0ZWQnKTtcbiAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1hIUiByZXF1ZXN0IHdhcyBub3QgYWJvcnRlZCcpO1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICB2YWxpZFJlc3BvbnNlOiBmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgaWYoICghbW9kdWxlLmlzLmV4cGVjdGluZ0pTT04oKSkgfHwgISQuaXNGdW5jdGlvbihzZXR0aW5ncy5zdWNjZXNzVGVzdCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZXNwb25zZSBpcyBub3QgSlNPTiwgc2tpcHBpbmcgdmFsaWRhdGlvbicsIHNldHRpbmdzLnN1Y2Nlc3NUZXN0LCByZXNwb25zZSk7XG4gICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDaGVja2luZyBKU09OIHJldHVybmVkIHN1Y2Nlc3MnLCBzZXR0aW5ncy5zdWNjZXNzVGVzdCwgcmVzcG9uc2UpO1xuICAgICAgICAgICAgaWYoIHNldHRpbmdzLnN1Y2Nlc3NUZXN0KHJlc3BvbnNlKSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZXNwb25zZSBwYXNzZWQgc3VjY2VzcyB0ZXN0JywgcmVzcG9uc2UpO1xuICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Jlc3BvbnNlIGZhaWxlZCBzdWNjZXNzIHRlc3QnLCByZXNwb25zZSk7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgd2FzOiB7XG4gICAgICAgICAgY2FuY2VsbGVkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiAobW9kdWxlLmNhbmNlbGxlZCB8fCBmYWxzZSk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzdWNjZXNmdWw6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIChtb2R1bGUucmVxdWVzdCAmJiBtb2R1bGUucmVxdWVzdC5zdGF0ZSgpID09ICdyZXNvbHZlZCcpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZmFpbHVyZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gKG1vZHVsZS5yZXF1ZXN0ICYmIG1vZHVsZS5yZXF1ZXN0LnN0YXRlKCkgPT0gJ3JlamVjdGVkJyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBjb21wbGV0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gKG1vZHVsZS5yZXF1ZXN0ICYmIChtb2R1bGUucmVxdWVzdC5zdGF0ZSgpID09ICdyZXNvbHZlZCcgfHwgbW9kdWxlLnJlcXVlc3Quc3RhdGUoKSA9PSAncmVqZWN0ZWQnKSApO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBhZGQ6IHtcbiAgICAgICAgICB1cmxEYXRhOiBmdW5jdGlvbih1cmwsIHVybERhdGEpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICByZXF1aXJlZFZhcmlhYmxlcyxcbiAgICAgICAgICAgICAgb3B0aW9uYWxWYXJpYWJsZXNcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKHVybCkge1xuICAgICAgICAgICAgICByZXF1aXJlZFZhcmlhYmxlcyA9IHVybC5tYXRjaChzZXR0aW5ncy5yZWdFeHAucmVxdWlyZWQpO1xuICAgICAgICAgICAgICBvcHRpb25hbFZhcmlhYmxlcyA9IHVybC5tYXRjaChzZXR0aW5ncy5yZWdFeHAub3B0aW9uYWwpO1xuICAgICAgICAgICAgICB1cmxEYXRhICAgICAgICAgICA9IHVybERhdGEgfHwgc2V0dGluZ3MudXJsRGF0YTtcbiAgICAgICAgICAgICAgaWYocmVxdWlyZWRWYXJpYWJsZXMpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0xvb2tpbmcgZm9yIHJlcXVpcmVkIFVSTCB2YXJpYWJsZXMnLCByZXF1aXJlZFZhcmlhYmxlcyk7XG4gICAgICAgICAgICAgICAgJC5lYWNoKHJlcXVpcmVkVmFyaWFibGVzLCBmdW5jdGlvbihpbmRleCwgdGVtcGxhdGVkU3RyaW5nKSB7XG4gICAgICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICAgICAgLy8gYWxsb3cgbGVnYWN5IHskdmFyfSBzdHlsZVxuICAgICAgICAgICAgICAgICAgICB2YXJpYWJsZSA9ICh0ZW1wbGF0ZWRTdHJpbmcuaW5kZXhPZignJCcpICE9PSAtMSlcbiAgICAgICAgICAgICAgICAgICAgICA/IHRlbXBsYXRlZFN0cmluZy5zdWJzdHIoMiwgdGVtcGxhdGVkU3RyaW5nLmxlbmd0aCAtIDMpXG4gICAgICAgICAgICAgICAgICAgICAgOiB0ZW1wbGF0ZWRTdHJpbmcuc3Vic3RyKDEsIHRlbXBsYXRlZFN0cmluZy5sZW5ndGggLSAyKSxcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUgICA9ICgkLmlzUGxhaW5PYmplY3QodXJsRGF0YSkgJiYgdXJsRGF0YVt2YXJpYWJsZV0gIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICAgICAgICA/IHVybERhdGFbdmFyaWFibGVdXG4gICAgICAgICAgICAgICAgICAgICAgOiAoJG1vZHVsZS5kYXRhKHZhcmlhYmxlKSAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICAgICAgICAgICAgPyAkbW9kdWxlLmRhdGEodmFyaWFibGUpXG4gICAgICAgICAgICAgICAgICAgICAgICA6ICgkY29udGV4dC5kYXRhKHZhcmlhYmxlKSAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICAgICAgICAgICAgICA/ICRjb250ZXh0LmRhdGEodmFyaWFibGUpXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDogdXJsRGF0YVt2YXJpYWJsZV1cbiAgICAgICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgICAgIC8vIHJlbW92ZSB2YWx1ZVxuICAgICAgICAgICAgICAgICAgaWYodmFsdWUgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3IucmVxdWlyZWRQYXJhbWV0ZXIsIHZhcmlhYmxlLCB1cmwpO1xuICAgICAgICAgICAgICAgICAgICB1cmwgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdGb3VuZCByZXF1aXJlZCB2YXJpYWJsZScsIHZhcmlhYmxlLCB2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgIHZhbHVlID0gKHNldHRpbmdzLmVuY29kZVBhcmFtZXRlcnMpXG4gICAgICAgICAgICAgICAgICAgICAgPyBtb2R1bGUuZ2V0LnVybEVuY29kZWRWYWx1ZSh2YWx1ZSlcbiAgICAgICAgICAgICAgICAgICAgICA6IHZhbHVlXG4gICAgICAgICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgICAgICAgdXJsID0gdXJsLnJlcGxhY2UodGVtcGxhdGVkU3RyaW5nLCB2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYob3B0aW9uYWxWYXJpYWJsZXMpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0xvb2tpbmcgZm9yIG9wdGlvbmFsIFVSTCB2YXJpYWJsZXMnLCByZXF1aXJlZFZhcmlhYmxlcyk7XG4gICAgICAgICAgICAgICAgJC5lYWNoKG9wdGlvbmFsVmFyaWFibGVzLCBmdW5jdGlvbihpbmRleCwgdGVtcGxhdGVkU3RyaW5nKSB7XG4gICAgICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICAgICAgLy8gYWxsb3cgbGVnYWN5IHsvJHZhcn0gc3R5bGVcbiAgICAgICAgICAgICAgICAgICAgdmFyaWFibGUgPSAodGVtcGxhdGVkU3RyaW5nLmluZGV4T2YoJyQnKSAhPT0gLTEpXG4gICAgICAgICAgICAgICAgICAgICAgPyB0ZW1wbGF0ZWRTdHJpbmcuc3Vic3RyKDMsIHRlbXBsYXRlZFN0cmluZy5sZW5ndGggLSA0KVxuICAgICAgICAgICAgICAgICAgICAgIDogdGVtcGxhdGVkU3RyaW5nLnN1YnN0cigyLCB0ZW1wbGF0ZWRTdHJpbmcubGVuZ3RoIC0gMyksXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlICAgPSAoJC5pc1BsYWluT2JqZWN0KHVybERhdGEpICYmIHVybERhdGFbdmFyaWFibGVdICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgICAgICAgICAgPyB1cmxEYXRhW3ZhcmlhYmxlXVxuICAgICAgICAgICAgICAgICAgICAgIDogKCRtb2R1bGUuZGF0YSh2YXJpYWJsZSkgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICAgICAgICAgID8gJG1vZHVsZS5kYXRhKHZhcmlhYmxlKVxuICAgICAgICAgICAgICAgICAgICAgICAgOiAoJGNvbnRleHQuZGF0YSh2YXJpYWJsZSkgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPyAkY29udGV4dC5kYXRhKHZhcmlhYmxlKVxuICAgICAgICAgICAgICAgICAgICAgICAgICA6IHVybERhdGFbdmFyaWFibGVdXG4gICAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgICAgICAvLyBvcHRpb25hbCByZXBsYWNlbWVudFxuICAgICAgICAgICAgICAgICAgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnT3B0aW9uYWwgdmFyaWFibGUgRm91bmQnLCB2YXJpYWJsZSwgdmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICB1cmwgPSB1cmwucmVwbGFjZSh0ZW1wbGF0ZWRTdHJpbmcsIHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnT3B0aW9uYWwgdmFyaWFibGUgbm90IGZvdW5kJywgdmFyaWFibGUpO1xuICAgICAgICAgICAgICAgICAgICAvLyByZW1vdmUgcHJlY2VkaW5nIHNsYXNoIGlmIHNldFxuICAgICAgICAgICAgICAgICAgICBpZih1cmwuaW5kZXhPZignLycgKyB0ZW1wbGF0ZWRTdHJpbmcpICE9PSAtMSkge1xuICAgICAgICAgICAgICAgICAgICAgIHVybCA9IHVybC5yZXBsYWNlKCcvJyArIHRlbXBsYXRlZFN0cmluZywgJycpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgIHVybCA9IHVybC5yZXBsYWNlKHRlbXBsYXRlZFN0cmluZywgJycpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB1cmw7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBmb3JtRGF0YTogZnVuY3Rpb24oZGF0YSkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGNhblNlcmlhbGl6ZSA9ICgkLmZuLnNlcmlhbGl6ZU9iamVjdCAhPT0gdW5kZWZpbmVkKSxcbiAgICAgICAgICAgICAgZm9ybURhdGEgICAgID0gKGNhblNlcmlhbGl6ZSlcbiAgICAgICAgICAgICAgICA/ICRmb3JtLnNlcmlhbGl6ZU9iamVjdCgpXG4gICAgICAgICAgICAgICAgOiAkZm9ybS5zZXJpYWxpemUoKSxcbiAgICAgICAgICAgICAgaGFzT3RoZXJEYXRhXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBkYXRhICAgICAgICAgPSBkYXRhIHx8IHNldHRpbmdzLmRhdGE7XG4gICAgICAgICAgICBoYXNPdGhlckRhdGEgPSAkLmlzUGxhaW5PYmplY3QoZGF0YSk7XG5cbiAgICAgICAgICAgIGlmKGhhc090aGVyRGF0YSkge1xuICAgICAgICAgICAgICBpZihjYW5TZXJpYWxpemUpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0V4dGVuZGluZyBleGlzdGluZyBkYXRhIHdpdGggZm9ybSBkYXRhJywgZGF0YSwgZm9ybURhdGEpO1xuICAgICAgICAgICAgICAgIGRhdGEgPSAkLmV4dGVuZCh0cnVlLCB7fSwgZGF0YSwgZm9ybURhdGEpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5taXNzaW5nU2VyaWFsaXplKTtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NhbnQgZXh0ZW5kIGRhdGEuIFJlcGxhY2luZyBkYXRhIHdpdGggZm9ybSBkYXRhJywgZGF0YSwgZm9ybURhdGEpO1xuICAgICAgICAgICAgICAgIGRhdGEgPSBmb3JtRGF0YTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQWRkaW5nIGZvcm0gZGF0YScsIGZvcm1EYXRhKTtcbiAgICAgICAgICAgICAgZGF0YSA9IGZvcm1EYXRhO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGRhdGE7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNlbmQ6IHtcbiAgICAgICAgICByZXF1ZXN0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS5zZXQubG9hZGluZygpO1xuICAgICAgICAgICAgbW9kdWxlLnJlcXVlc3QgPSBtb2R1bGUuY3JlYXRlLnJlcXVlc3QoKTtcbiAgICAgICAgICAgIGlmKCBtb2R1bGUuaXMubW9ja2VkKCkgKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5tb2NrZWRYSFIgPSBtb2R1bGUuY3JlYXRlLm1vY2tlZFhIUigpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS54aHIgPSBtb2R1bGUuY3JlYXRlLnhocigpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc2V0dGluZ3Mub25SZXF1ZXN0LmNhbGwoY29udGV4dCwgbW9kdWxlLnJlcXVlc3QsIG1vZHVsZS54aHIpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBldmVudDoge1xuICAgICAgICAgIHRyaWdnZXI6IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICBtb2R1bGUucXVlcnkoKTtcbiAgICAgICAgICAgIGlmKGV2ZW50LnR5cGUgPT0gJ3N1Ym1pdCcgfHwgZXZlbnQudHlwZSA9PSAnY2xpY2snKSB7XG4gICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICB4aHI6IHtcbiAgICAgICAgICAgIGFsd2F5czogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIC8vIG5vdGhpbmcgc3BlY2lhbFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGRvbmU6IGZ1bmN0aW9uKHJlc3BvbnNlLCB0ZXh0U3RhdHVzLCB4aHIpIHtcbiAgICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgICAgY29udGV4dCAgICAgICAgICAgID0gdGhpcyxcbiAgICAgICAgICAgICAgICBlbGFwc2VkVGltZSAgICAgICAgPSAobmV3IERhdGUoKS5nZXRUaW1lKCkgLSByZXF1ZXN0U3RhcnRUaW1lKSxcbiAgICAgICAgICAgICAgICB0aW1lTGVmdCAgICAgICAgICAgPSAoc2V0dGluZ3MubG9hZGluZ0R1cmF0aW9uIC0gZWxhcHNlZFRpbWUpLFxuICAgICAgICAgICAgICAgIHRyYW5zbGF0ZWRSZXNwb25zZSA9ICggJC5pc0Z1bmN0aW9uKHNldHRpbmdzLm9uUmVzcG9uc2UpIClcbiAgICAgICAgICAgICAgICAgID8gbW9kdWxlLmlzLmV4cGVjdGluZ0pTT04oKVxuICAgICAgICAgICAgICAgICAgICA/IHNldHRpbmdzLm9uUmVzcG9uc2UuY2FsbChjb250ZXh0LCAkLmV4dGVuZCh0cnVlLCB7fSwgcmVzcG9uc2UpKVxuICAgICAgICAgICAgICAgICAgICA6IHNldHRpbmdzLm9uUmVzcG9uc2UuY2FsbChjb250ZXh0LCByZXNwb25zZSlcbiAgICAgICAgICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICB0aW1lTGVmdCA9ICh0aW1lTGVmdCA+IDApXG4gICAgICAgICAgICAgICAgPyB0aW1lTGVmdFxuICAgICAgICAgICAgICAgIDogMFxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKHRyYW5zbGF0ZWRSZXNwb25zZSkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnTW9kaWZpZWQgQVBJIHJlc3BvbnNlIGluIG9uUmVzcG9uc2UgY2FsbGJhY2snLCBzZXR0aW5ncy5vblJlc3BvbnNlLCB0cmFuc2xhdGVkUmVzcG9uc2UsIHJlc3BvbnNlKTtcbiAgICAgICAgICAgICAgICByZXNwb25zZSA9IHRyYW5zbGF0ZWRSZXNwb25zZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBpZih0aW1lTGVmdCA+IDApIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1Jlc3BvbnNlIGNvbXBsZXRlZCBlYXJseSBkZWxheWluZyBzdGF0ZSBjaGFuZ2UgYnknLCB0aW1lTGVmdCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZiggbW9kdWxlLmlzLnZhbGlkUmVzcG9uc2UocmVzcG9uc2UpICkge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLnJlcXVlc3QucmVzb2x2ZVdpdGgoY29udGV4dCwgW3Jlc3BvbnNlLCB4aHJdKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICBtb2R1bGUucmVxdWVzdC5yZWplY3RXaXRoKGNvbnRleHQsIFt4aHIsICdpbnZhbGlkJ10pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSwgdGltZUxlZnQpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGZhaWw6IGZ1bmN0aW9uKHhociwgc3RhdHVzLCBodHRwTWVzc2FnZSkge1xuICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICBjb250ZXh0ICAgICA9IHRoaXMsXG4gICAgICAgICAgICAgICAgZWxhcHNlZFRpbWUgPSAobmV3IERhdGUoKS5nZXRUaW1lKCkgLSByZXF1ZXN0U3RhcnRUaW1lKSxcbiAgICAgICAgICAgICAgICB0aW1lTGVmdCAgICA9IChzZXR0aW5ncy5sb2FkaW5nRHVyYXRpb24gLSBlbGFwc2VkVGltZSlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgICB0aW1lTGVmdCA9ICh0aW1lTGVmdCA+IDApXG4gICAgICAgICAgICAgICAgPyB0aW1lTGVmdFxuICAgICAgICAgICAgICAgIDogMFxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKHRpbWVMZWZ0ID4gMCkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnUmVzcG9uc2UgY29tcGxldGVkIGVhcmx5IGRlbGF5aW5nIHN0YXRlIGNoYW5nZSBieScsIHRpbWVMZWZ0KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIGlmKCBtb2R1bGUuaXMuYWJvcnRlZFJlcXVlc3QoeGhyKSApIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5yZXF1ZXN0LnJlamVjdFdpdGgoY29udGV4dCwgW3hociwgJ2Fib3J0ZWQnLCBodHRwTWVzc2FnZV0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIG1vZHVsZS5yZXF1ZXN0LnJlamVjdFdpdGgoY29udGV4dCwgW3hociwgJ2Vycm9yJywgc3RhdHVzLCBodHRwTWVzc2FnZV0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSwgdGltZUxlZnQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgcmVxdWVzdDoge1xuICAgICAgICAgICAgZG9uZTogZnVuY3Rpb24ocmVzcG9uc2UsIHhocikge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1N1Y2Nlc3NmdWwgQVBJIFJlc3BvbnNlJywgcmVzcG9uc2UpO1xuICAgICAgICAgICAgICBpZihzZXR0aW5ncy5jYWNoZSA9PT0gJ2xvY2FsJyAmJiB1cmwpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUud3JpdGUuY2FjaGVkUmVzcG9uc2UodXJsLCByZXNwb25zZSk7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdTYXZpbmcgc2VydmVyIHJlc3BvbnNlIGxvY2FsbHknLCBtb2R1bGUuY2FjaGUpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHNldHRpbmdzLm9uU3VjY2Vzcy5jYWxsKGNvbnRleHQsIHJlc3BvbnNlLCAkbW9kdWxlLCB4aHIpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGNvbXBsZXRlOiBmdW5jdGlvbihmaXJzdFBhcmFtZXRlciwgc2Vjb25kUGFyYW1ldGVyKSB7XG4gICAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICAgIHhocixcbiAgICAgICAgICAgICAgICByZXNwb25zZVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIC8vIGhhdmUgdG8gZ3Vlc3MgY2FsbGJhY2sgcGFyYW1ldGVycyBiYXNlZCBvbiByZXF1ZXN0IHN1Y2Nlc3NcbiAgICAgICAgICAgICAgaWYoIG1vZHVsZS53YXMuc3VjY2VzZnVsKCkgKSB7XG4gICAgICAgICAgICAgICAgcmVzcG9uc2UgPSBmaXJzdFBhcmFtZXRlcjtcbiAgICAgICAgICAgICAgICB4aHIgICAgICA9IHNlY29uZFBhcmFtZXRlcjtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICB4aHIgICAgICA9IGZpcnN0UGFyYW1ldGVyO1xuICAgICAgICAgICAgICAgIHJlc3BvbnNlID0gbW9kdWxlLmdldC5yZXNwb25zZUZyb21YSFIoeGhyKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBtb2R1bGUucmVtb3ZlLmxvYWRpbmcoKTtcbiAgICAgICAgICAgICAgc2V0dGluZ3Mub25Db21wbGV0ZS5jYWxsKGNvbnRleHQsIHJlc3BvbnNlLCAkbW9kdWxlLCB4aHIpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGZhaWw6IGZ1bmN0aW9uKHhociwgc3RhdHVzLCBodHRwTWVzc2FnZSkge1xuICAgICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgICAvLyBwdWxsIHJlc3BvbnNlIGZyb20geGhyIGlmIGF2YWlsYWJsZVxuICAgICAgICAgICAgICAgIHJlc3BvbnNlICAgICA9IG1vZHVsZS5nZXQucmVzcG9uc2VGcm9tWEhSKHhociksXG4gICAgICAgICAgICAgICAgZXJyb3JNZXNzYWdlID0gbW9kdWxlLmdldC5lcnJvckZyb21SZXF1ZXN0KHJlc3BvbnNlLCBzdGF0dXMsIGh0dHBNZXNzYWdlKVxuICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIGlmKHN0YXR1cyA9PSAnYWJvcnRlZCcpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1hIUiBBYm9ydGVkIChNb3N0IGxpa2VseSBjYXVzZWQgYnkgcGFnZSBuYXZpZ2F0aW9uIG9yIENPUlMgUG9saWN5KScsIHN0YXR1cywgaHR0cE1lc3NhZ2UpO1xuICAgICAgICAgICAgICAgIHNldHRpbmdzLm9uQWJvcnQuY2FsbChjb250ZXh0LCBzdGF0dXMsICRtb2R1bGUsIHhocik7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZihzdGF0dXMgPT0gJ2ludmFsaWQnKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdKU09OIGRpZCBub3QgcGFzcyBzdWNjZXNzIHRlc3QuIEEgc2VydmVyLXNpZGUgZXJyb3IgaGFzIG1vc3QgbGlrZWx5IG9jY3VycmVkJywgcmVzcG9uc2UpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoc3RhdHVzID09ICdlcnJvcicpIHtcbiAgICAgICAgICAgICAgICBpZih4aHIgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdYSFIgcHJvZHVjZWQgYSBzZXJ2ZXIgZXJyb3InLCBzdGF0dXMsIGh0dHBNZXNzYWdlKTtcbiAgICAgICAgICAgICAgICAgIC8vIG1ha2Ugc3VyZSB3ZSBoYXZlIGFuIGVycm9yIHRvIGRpc3BsYXkgdG8gY29uc29sZVxuICAgICAgICAgICAgICAgICAgaWYoIHhoci5zdGF0dXMgIT0gMjAwICYmIGh0dHBNZXNzYWdlICE9PSB1bmRlZmluZWQgJiYgaHR0cE1lc3NhZ2UgIT09ICcnKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5zdGF0dXNNZXNzYWdlICsgaHR0cE1lc3NhZ2UsIGFqYXhTZXR0aW5ncy51cmwpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgc2V0dGluZ3Mub25FcnJvci5jYWxsKGNvbnRleHQsIGVycm9yTWVzc2FnZSwgJG1vZHVsZSwgeGhyKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICBpZihzZXR0aW5ncy5lcnJvckR1cmF0aW9uICYmIHN0YXR1cyAhPT0gJ2Fib3J0ZWQnKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdBZGRpbmcgZXJyb3Igc3RhdGUnKTtcbiAgICAgICAgICAgICAgICBtb2R1bGUuc2V0LmVycm9yKCk7XG4gICAgICAgICAgICAgICAgaWYoIG1vZHVsZS5zaG91bGQucmVtb3ZlRXJyb3IoKSApIHtcbiAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQobW9kdWxlLnJlbW92ZS5lcnJvciwgc2V0dGluZ3MuZXJyb3JEdXJhdGlvbik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnQVBJIFJlcXVlc3QgZmFpbGVkJywgZXJyb3JNZXNzYWdlLCB4aHIpO1xuICAgICAgICAgICAgICBzZXR0aW5ncy5vbkZhaWx1cmUuY2FsbChjb250ZXh0LCByZXNwb25zZSwgJG1vZHVsZSwgeGhyKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgY3JlYXRlOiB7XG5cbiAgICAgICAgICByZXF1ZXN0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIC8vIGFwaSByZXF1ZXN0IHByb21pc2VcbiAgICAgICAgICAgIHJldHVybiAkLkRlZmVycmVkKClcbiAgICAgICAgICAgICAgLmFsd2F5cyhtb2R1bGUuZXZlbnQucmVxdWVzdC5jb21wbGV0ZSlcbiAgICAgICAgICAgICAgLmRvbmUobW9kdWxlLmV2ZW50LnJlcXVlc3QuZG9uZSlcbiAgICAgICAgICAgICAgLmZhaWwobW9kdWxlLmV2ZW50LnJlcXVlc3QuZmFpbClcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuXG4gICAgICAgICAgbW9ja2VkWEhSOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgLy8geGhyIGRvZXMgbm90IHNpbXVsYXRlIHRoZXNlIHByb3BlcnRpZXMgb2YgeGhyIGJ1dCBtdXN0IHJldHVybiB0aGVtXG4gICAgICAgICAgICAgIHRleHRTdGF0dXMgICAgID0gZmFsc2UsXG4gICAgICAgICAgICAgIHN0YXR1cyAgICAgICAgID0gZmFsc2UsXG4gICAgICAgICAgICAgIGh0dHBNZXNzYWdlICAgID0gZmFsc2UsXG4gICAgICAgICAgICAgIHJlc3BvbmRlciAgICAgID0gc2V0dGluZ3MubW9ja1Jlc3BvbnNlICAgICAgfHwgc2V0dGluZ3MucmVzcG9uc2UsXG4gICAgICAgICAgICAgIGFzeW5jUmVzcG9uZGVyID0gc2V0dGluZ3MubW9ja1Jlc3BvbnNlQXN5bmMgfHwgc2V0dGluZ3MucmVzcG9uc2VBc3luYyxcbiAgICAgICAgICAgICAgYXN5bmNDYWxsYmFjayxcbiAgICAgICAgICAgICAgcmVzcG9uc2UsXG4gICAgICAgICAgICAgIG1vY2tlZFhIUlxuICAgICAgICAgICAgO1xuXG4gICAgICAgICAgICBtb2NrZWRYSFIgPSAkLkRlZmVycmVkKClcbiAgICAgICAgICAgICAgLmFsd2F5cyhtb2R1bGUuZXZlbnQueGhyLmNvbXBsZXRlKVxuICAgICAgICAgICAgICAuZG9uZShtb2R1bGUuZXZlbnQueGhyLmRvbmUpXG4gICAgICAgICAgICAgIC5mYWlsKG1vZHVsZS5ldmVudC54aHIuZmFpbClcbiAgICAgICAgICAgIDtcblxuICAgICAgICAgICAgaWYocmVzcG9uZGVyKSB7XG4gICAgICAgICAgICAgIGlmKCAkLmlzRnVuY3Rpb24ocmVzcG9uZGVyKSApIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ1VzaW5nIHNwZWNpZmllZCBzeW5jaHJvbm91cyBjYWxsYmFjaycsIHJlc3BvbmRlcik7XG4gICAgICAgICAgICAgICAgcmVzcG9uc2UgPSByZXNwb25kZXIuY2FsbChjb250ZXh0LCByZXF1ZXN0U2V0dGluZ3MpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVXNpbmcgc2V0dGluZ3Mgc3BlY2lmaWVkIHJlc3BvbnNlJywgcmVzcG9uZGVyKTtcbiAgICAgICAgICAgICAgICByZXNwb25zZSA9IHJlc3BvbmRlcjtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAvLyBzaW11bGF0aW5nIHJlc3BvbnNlXG4gICAgICAgICAgICAgIG1vY2tlZFhIUi5yZXNvbHZlV2l0aChjb250ZXh0LCBbIHJlc3BvbnNlLCB0ZXh0U3RhdHVzLCB7IHJlc3BvbnNlVGV4dDogcmVzcG9uc2UgfV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiggJC5pc0Z1bmN0aW9uKGFzeW5jUmVzcG9uZGVyKSApIHtcbiAgICAgICAgICAgICAgYXN5bmNDYWxsYmFjayA9IGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdBc3luYyBjYWxsYmFjayByZXR1cm5lZCByZXNwb25zZScsIHJlc3BvbnNlKTtcblxuICAgICAgICAgICAgICAgIGlmKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICBtb2NrZWRYSFIucmVzb2x2ZVdpdGgoY29udGV4dCwgWyByZXNwb25zZSwgdGV4dFN0YXR1cywgeyByZXNwb25zZVRleHQ6IHJlc3BvbnNlIH1dKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICBtb2NrZWRYSFIucmVqZWN0V2l0aChjb250ZXh0LCBbeyByZXNwb25zZVRleHQ6IHJlc3BvbnNlIH0sIHN0YXR1cywgaHR0cE1lc3NhZ2VdKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVXNpbmcgc3BlY2lmaWVkIGFzeW5jIHJlc3BvbnNlIGNhbGxiYWNrJywgYXN5bmNSZXNwb25kZXIpO1xuICAgICAgICAgICAgICBhc3luY1Jlc3BvbmRlci5jYWxsKGNvbnRleHQsIHJlcXVlc3RTZXR0aW5ncywgYXN5bmNDYWxsYmFjayk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gbW9ja2VkWEhSO1xuICAgICAgICAgIH0sXG5cbiAgICAgICAgICB4aHI6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIHhoclxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgLy8gYWpheCByZXF1ZXN0IHByb21pc2VcbiAgICAgICAgICAgIHhociA9ICQuYWpheChhamF4U2V0dGluZ3MpXG4gICAgICAgICAgICAgIC5hbHdheXMobW9kdWxlLmV2ZW50Lnhoci5hbHdheXMpXG4gICAgICAgICAgICAgIC5kb25lKG1vZHVsZS5ldmVudC54aHIuZG9uZSlcbiAgICAgICAgICAgICAgLmZhaWwobW9kdWxlLmV2ZW50Lnhoci5mYWlsKVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ0NyZWF0ZWQgc2VydmVyIHJlcXVlc3QnLCB4aHIsIGFqYXhTZXR0aW5ncyk7XG4gICAgICAgICAgICByZXR1cm4geGhyO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZXQ6IHtcbiAgICAgICAgICBlcnJvcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQWRkaW5nIGVycm9yIHN0YXRlIHRvIGVsZW1lbnQnLCAkY29udGV4dCk7XG4gICAgICAgICAgICAkY29udGV4dC5hZGRDbGFzcyhjbGFzc05hbWUuZXJyb3IpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgbG9hZGluZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnQWRkaW5nIGxvYWRpbmcgc3RhdGUgdG8gZWxlbWVudCcsICRjb250ZXh0KTtcbiAgICAgICAgICAgICRjb250ZXh0LmFkZENsYXNzKGNsYXNzTmFtZS5sb2FkaW5nKTtcbiAgICAgICAgICAgIHJlcXVlc3RTdGFydFRpbWUgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVtb3ZlOiB7XG4gICAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLnZlcmJvc2UoJ1JlbW92aW5nIGVycm9yIHN0YXRlIGZyb20gZWxlbWVudCcsICRjb250ZXh0KTtcbiAgICAgICAgICAgICRjb250ZXh0LnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5lcnJvcik7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBsb2FkaW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlKCdSZW1vdmluZyBsb2FkaW5nIHN0YXRlIGZyb20gZWxlbWVudCcsICRjb250ZXh0KTtcbiAgICAgICAgICAgICRjb250ZXh0LnJlbW92ZUNsYXNzKGNsYXNzTmFtZS5sb2FkaW5nKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0OiB7XG4gICAgICAgICAgcmVzcG9uc2VGcm9tWEhSOiBmdW5jdGlvbih4aHIpIHtcbiAgICAgICAgICAgIHJldHVybiAkLmlzUGxhaW5PYmplY3QoeGhyKVxuICAgICAgICAgICAgICA/IChtb2R1bGUuaXMuZXhwZWN0aW5nSlNPTigpKVxuICAgICAgICAgICAgICAgID8gbW9kdWxlLmRlY29kZS5qc29uKHhoci5yZXNwb25zZVRleHQpXG4gICAgICAgICAgICAgICAgOiB4aHIucmVzcG9uc2VUZXh0XG4gICAgICAgICAgICAgIDogZmFsc2VcbiAgICAgICAgICAgIDtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGVycm9yRnJvbVJlcXVlc3Q6IGZ1bmN0aW9uKHJlc3BvbnNlLCBzdGF0dXMsIGh0dHBNZXNzYWdlKSB7XG4gICAgICAgICAgICByZXR1cm4gKCQuaXNQbGFpbk9iamVjdChyZXNwb25zZSkgJiYgcmVzcG9uc2UuZXJyb3IgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgPyByZXNwb25zZS5lcnJvciAvLyB1c2UganNvbiBlcnJvciBtZXNzYWdlXG4gICAgICAgICAgICAgIDogKHNldHRpbmdzLmVycm9yW3N0YXR1c10gIT09IHVuZGVmaW5lZCkgLy8gdXNlIHNlcnZlciBlcnJvciBtZXNzYWdlXG4gICAgICAgICAgICAgICAgPyBzZXR0aW5ncy5lcnJvcltzdGF0dXNdXG4gICAgICAgICAgICAgICAgOiBodHRwTWVzc2FnZVxuICAgICAgICAgICAgO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcmVxdWVzdDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gbW9kdWxlLnJlcXVlc3QgfHwgZmFsc2U7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB4aHI6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZS54aHIgfHwgZmFsc2U7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBzZXR0aW5nczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgcnVuU2V0dGluZ3NcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHJ1blNldHRpbmdzID0gc2V0dGluZ3MuYmVmb3JlU2VuZC5jYWxsKGNvbnRleHQsIHNldHRpbmdzKTtcbiAgICAgICAgICAgIGlmKHJ1blNldHRpbmdzKSB7XG4gICAgICAgICAgICAgIGlmKHJ1blNldHRpbmdzLnN1Y2Nlc3MgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnTGVnYWN5IHN1Y2Nlc3MgY2FsbGJhY2sgZGV0ZWN0ZWQnLCBydW5TZXR0aW5ncyk7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLmxlZ2FjeVBhcmFtZXRlcnMsIHJ1blNldHRpbmdzLnN1Y2Nlc3MpO1xuICAgICAgICAgICAgICAgIHJ1blNldHRpbmdzLm9uU3VjY2VzcyA9IHJ1blNldHRpbmdzLnN1Y2Nlc3M7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYocnVuU2V0dGluZ3MuZmFpbHVyZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdMZWdhY3kgZmFpbHVyZSBjYWxsYmFjayBkZXRlY3RlZCcsIHJ1blNldHRpbmdzKTtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3IubGVnYWN5UGFyYW1ldGVycywgcnVuU2V0dGluZ3MuZmFpbHVyZSk7XG4gICAgICAgICAgICAgICAgcnVuU2V0dGluZ3Mub25GYWlsdXJlID0gcnVuU2V0dGluZ3MuZmFpbHVyZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBpZihydW5TZXR0aW5ncy5jb21wbGV0ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdMZWdhY3kgY29tcGxldGUgY2FsbGJhY2sgZGV0ZWN0ZWQnLCBydW5TZXR0aW5ncyk7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLmxlZ2FjeVBhcmFtZXRlcnMsIHJ1blNldHRpbmdzLmNvbXBsZXRlKTtcbiAgICAgICAgICAgICAgICBydW5TZXR0aW5ncy5vbkNvbXBsZXRlID0gcnVuU2V0dGluZ3MuY29tcGxldGU7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKHJ1blNldHRpbmdzID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm5vUmV0dXJuZWRWYWx1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihydW5TZXR0aW5ncyA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHJ1blNldHRpbmdzO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIChydW5TZXR0aW5ncyAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgICAgICA/ICQuZXh0ZW5kKHRydWUsIHt9LCBydW5TZXR0aW5ncylcbiAgICAgICAgICAgICAgOiAkLmV4dGVuZCh0cnVlLCB7fSwgc2V0dGluZ3MpXG4gICAgICAgICAgICA7XG4gICAgICAgICAgfSxcbiAgICAgICAgICB1cmxFbmNvZGVkVmFsdWU6IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgZGVjb2RlZFZhbHVlICAgPSB3aW5kb3cuZGVjb2RlVVJJQ29tcG9uZW50KHZhbHVlKSxcbiAgICAgICAgICAgICAgZW5jb2RlZFZhbHVlICAgPSB3aW5kb3cuZW5jb2RlVVJJQ29tcG9uZW50KHZhbHVlKSxcbiAgICAgICAgICAgICAgYWxyZWFkeUVuY29kZWQgPSAoZGVjb2RlZFZhbHVlICE9PSB2YWx1ZSlcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIGlmKGFscmVhZHlFbmNvZGVkKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVVJMIHZhbHVlIGlzIGFscmVhZHkgZW5jb2RlZCwgYXZvaWRpbmcgZG91YmxlIGVuY29kaW5nJywgdmFsdWUpO1xuICAgICAgICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtb2R1bGUudmVyYm9zZSgnRW5jb2RpbmcgdmFsdWUgdXNpbmcgZW5jb2RlVVJJQ29tcG9uZW50JywgdmFsdWUsIGVuY29kZWRWYWx1ZSk7XG4gICAgICAgICAgICByZXR1cm4gZW5jb2RlZFZhbHVlO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgZGVmYXVsdERhdGE6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyXG4gICAgICAgICAgICAgIGRhdGEgPSB7fVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoICEkLmlzV2luZG93KGVsZW1lbnQpICkge1xuICAgICAgICAgICAgICBpZiggbW9kdWxlLmlzLmlucHV0KCkgKSB7XG4gICAgICAgICAgICAgICAgZGF0YS52YWx1ZSA9ICRtb2R1bGUudmFsKCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggbW9kdWxlLmlzLmZvcm0oKSApIHtcblxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGRhdGEudGV4dCA9ICRtb2R1bGUudGV4dCgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZGF0YTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGV2ZW50OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKCAkLmlzV2luZG93KGVsZW1lbnQpIHx8IHNldHRpbmdzLm9uID09ICdub3cnICkge1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0FQSSBjYWxsZWQgd2l0aG91dCBlbGVtZW50LCBubyBldmVudHMgYXR0YWNoZWQnKTtcbiAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZihzZXR0aW5ncy5vbiA9PSAnYXV0bycpIHtcbiAgICAgICAgICAgICAgaWYoICRtb2R1bGUuaXMoJ2lucHV0JykgKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIChlbGVtZW50Lm9uaW5wdXQgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICAgICAgICAgID8gJ2lucHV0J1xuICAgICAgICAgICAgICAgICAgOiAoZWxlbWVudC5vbnByb3BlcnR5Y2hhbmdlICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgICAgICAgICAgID8gJ3Byb3BlcnR5Y2hhbmdlJ1xuICAgICAgICAgICAgICAgICAgICA6ICdrZXl1cCdcbiAgICAgICAgICAgICAgICA7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggJG1vZHVsZS5pcygnZm9ybScpICkge1xuICAgICAgICAgICAgICAgIHJldHVybiAnc3VibWl0JztcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gJ2NsaWNrJztcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHJldHVybiBzZXR0aW5ncy5vbjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHRlbXBsYXRlZFVSTDogZnVuY3Rpb24oYWN0aW9uKSB7XG4gICAgICAgICAgICBhY3Rpb24gPSBhY3Rpb24gfHwgJG1vZHVsZS5kYXRhKG1ldGFkYXRhLmFjdGlvbikgfHwgc2V0dGluZ3MuYWN0aW9uIHx8IGZhbHNlO1xuICAgICAgICAgICAgdXJsICAgID0gJG1vZHVsZS5kYXRhKG1ldGFkYXRhLnVybCkgfHwgc2V0dGluZ3MudXJsIHx8IGZhbHNlO1xuICAgICAgICAgICAgaWYodXJsKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnVXNpbmcgc3BlY2lmaWVkIHVybCcsIHVybCk7XG4gICAgICAgICAgICAgIHJldHVybiB1cmw7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihhY3Rpb24pIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdMb29raW5nIHVwIHVybCBmb3IgYWN0aW9uJywgYWN0aW9uLCBzZXR0aW5ncy5hcGkpO1xuICAgICAgICAgICAgICBpZihzZXR0aW5ncy5hcGlbYWN0aW9uXSA9PT0gdW5kZWZpbmVkICYmICFtb2R1bGUuaXMubW9ja2VkKCkpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3IubWlzc2luZ0FjdGlvbiwgc2V0dGluZ3MuYWN0aW9uLCBzZXR0aW5ncy5hcGkpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB1cmwgPSBzZXR0aW5ncy5hcGlbYWN0aW9uXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoIG1vZHVsZS5pcy5mb3JtKCkgKSB7XG4gICAgICAgICAgICAgIHVybCA9ICRtb2R1bGUuYXR0cignYWN0aW9uJykgfHwgJGNvbnRleHQuYXR0cignYWN0aW9uJykgfHwgZmFsc2U7XG4gICAgICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnTm8gdXJsIG9yIGFjdGlvbiBzcGVjaWZpZWQsIGRlZmF1bHRpbmcgdG8gZm9ybSBhY3Rpb24nLCB1cmwpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHVybDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgYWJvcnQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIHZhclxuICAgICAgICAgICAgeGhyID0gbW9kdWxlLmdldC54aHIoKVxuICAgICAgICAgIDtcbiAgICAgICAgICBpZiggeGhyICYmIHhoci5zdGF0ZSgpICE9PSAncmVzb2x2ZWQnKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0NhbmNlbGxpbmcgQVBJIHJlcXVlc3QnKTtcbiAgICAgICAgICAgIHhoci5hYm9ydCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICAvLyByZXNldCBzdGF0ZVxuICAgICAgICByZXNldDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLnJlbW92ZS5lcnJvcigpO1xuICAgICAgICAgIG1vZHVsZS5yZW1vdmUubG9hZGluZygpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHNldHRpbmc6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdDaGFuZ2luZyBzZXR0aW5nJywgbmFtZSwgdmFsdWUpO1xuICAgICAgICAgIGlmKCAkLmlzUGxhaW5PYmplY3QobmFtZSkgKSB7XG4gICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBzZXR0aW5ncywgbmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgaWYoJC5pc1BsYWluT2JqZWN0KHNldHRpbmdzW25hbWVdKSkge1xuICAgICAgICAgICAgICAkLmV4dGVuZCh0cnVlLCBzZXR0aW5nc1tuYW1lXSwgdmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHNldHRpbmdzW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHNldHRpbmdzW25hbWVdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgaW50ZXJuYWw6IGZ1bmN0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdChuYW1lKSApIHtcbiAgICAgICAgICAgICQuZXh0ZW5kKHRydWUsIG1vZHVsZSwgbmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYodmFsdWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgbW9kdWxlW25hbWVdID0gdmFsdWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIG1vZHVsZVtuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGRlYnVnOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZighc2V0dGluZ3Muc2lsZW50ICYmIHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZihzZXR0aW5ncy5wZXJmb3JtYW5jZSkge1xuICAgICAgICAgICAgICBtb2R1bGUucGVyZm9ybWFuY2UubG9nKGFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmRlYnVnID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmluZm8sIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgICBtb2R1bGUuZGVidWcuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHZlcmJvc2U6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQgJiYgc2V0dGluZ3MudmVyYm9zZSAmJiBzZXR0aW5ncy5kZWJ1Zykge1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgbW9kdWxlLnBlcmZvcm1hbmNlLmxvZyhhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG1vZHVsZS52ZXJib3NlID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQuY2FsbChjb25zb2xlLmluZm8sIGNvbnNvbGUsIHNldHRpbmdzLm5hbWUgKyAnOicpO1xuICAgICAgICAgICAgICBtb2R1bGUudmVyYm9zZS5hcHBseShjb25zb2xlLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIGlmKCFzZXR0aW5ncy5zaWxlbnQpIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvciA9IEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kLmNhbGwoY29uc29sZS5lcnJvciwgY29uc29sZSwgc2V0dGluZ3MubmFtZSArICc6Jyk7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IuYXBwbHkoY29uc29sZSwgYXJndW1lbnRzKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHBlcmZvcm1hbmNlOiB7XG4gICAgICAgICAgbG9nOiBmdW5jdGlvbihtZXNzYWdlKSB7XG4gICAgICAgICAgICB2YXJcbiAgICAgICAgICAgICAgY3VycmVudFRpbWUsXG4gICAgICAgICAgICAgIGV4ZWN1dGlvblRpbWUsXG4gICAgICAgICAgICAgIHByZXZpb3VzVGltZVxuICAgICAgICAgICAgO1xuICAgICAgICAgICAgaWYoc2V0dGluZ3MucGVyZm9ybWFuY2UpIHtcbiAgICAgICAgICAgICAgY3VycmVudFRpbWUgICA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgICAgICAgICAgICBwcmV2aW91c1RpbWUgID0gdGltZSB8fCBjdXJyZW50VGltZTtcbiAgICAgICAgICAgICAgZXhlY3V0aW9uVGltZSA9IGN1cnJlbnRUaW1lIC0gcHJldmlvdXNUaW1lO1xuICAgICAgICAgICAgICB0aW1lICAgICAgICAgID0gY3VycmVudFRpbWU7XG4gICAgICAgICAgICAgIHBlcmZvcm1hbmNlLnB1c2goe1xuICAgICAgICAgICAgICAgICdOYW1lJyAgICAgICAgICAgOiBtZXNzYWdlWzBdLFxuICAgICAgICAgICAgICAgICdBcmd1bWVudHMnICAgICAgOiBbXS5zbGljZS5jYWxsKG1lc3NhZ2UsIDEpIHx8ICcnLFxuICAgICAgICAgICAgICAgIC8vJ0VsZW1lbnQnICAgICAgICA6IGVsZW1lbnQsXG4gICAgICAgICAgICAgICAgJ0V4ZWN1dGlvbiBUaW1lJyA6IGV4ZWN1dGlvblRpbWVcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLnRpbWVyKTtcbiAgICAgICAgICAgIG1vZHVsZS5wZXJmb3JtYW5jZS50aW1lciA9IHNldFRpbWVvdXQobW9kdWxlLnBlcmZvcm1hbmNlLmRpc3BsYXksIDUwMCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBkaXNwbGF5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhclxuICAgICAgICAgICAgICB0aXRsZSA9IHNldHRpbmdzLm5hbWUgKyAnOicsXG4gICAgICAgICAgICAgIHRvdGFsVGltZSA9IDBcbiAgICAgICAgICAgIDtcbiAgICAgICAgICAgIHRpbWUgPSBmYWxzZTtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dChtb2R1bGUucGVyZm9ybWFuY2UudGltZXIpO1xuICAgICAgICAgICAgJC5lYWNoKHBlcmZvcm1hbmNlLCBmdW5jdGlvbihpbmRleCwgZGF0YSkge1xuICAgICAgICAgICAgICB0b3RhbFRpbWUgKz0gZGF0YVsnRXhlY3V0aW9uIFRpbWUnXTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgdGl0bGUgKz0gJyAnICsgdG90YWxUaW1lICsgJ21zJztcbiAgICAgICAgICAgIGlmKG1vZHVsZVNlbGVjdG9yKSB7XG4gICAgICAgICAgICAgIHRpdGxlICs9ICcgXFwnJyArIG1vZHVsZVNlbGVjdG9yICsgJ1xcJyc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiggKGNvbnNvbGUuZ3JvdXAgIT09IHVuZGVmaW5lZCB8fCBjb25zb2xlLnRhYmxlICE9PSB1bmRlZmluZWQpICYmIHBlcmZvcm1hbmNlLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgY29uc29sZS5ncm91cENvbGxhcHNlZCh0aXRsZSk7XG4gICAgICAgICAgICAgIGlmKGNvbnNvbGUudGFibGUpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLnRhYmxlKHBlcmZvcm1hbmNlKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAkLmVhY2gocGVyZm9ybWFuY2UsIGZ1bmN0aW9uKGluZGV4LCBkYXRhKSB7XG4gICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhWydOYW1lJ10gKyAnOiAnICsgZGF0YVsnRXhlY3V0aW9uIFRpbWUnXSsnbXMnKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBjb25zb2xlLmdyb3VwRW5kKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBwZXJmb3JtYW5jZSA9IFtdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgaW52b2tlOiBmdW5jdGlvbihxdWVyeSwgcGFzc2VkQXJndW1lbnRzLCBjb250ZXh0KSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICBvYmplY3QgPSBpbnN0YW5jZSxcbiAgICAgICAgICAgIG1heERlcHRoLFxuICAgICAgICAgICAgZm91bmQsXG4gICAgICAgICAgICByZXNwb25zZVxuICAgICAgICAgIDtcbiAgICAgICAgICBwYXNzZWRBcmd1bWVudHMgPSBwYXNzZWRBcmd1bWVudHMgfHwgcXVlcnlBcmd1bWVudHM7XG4gICAgICAgICAgY29udGV4dCAgICAgICAgID0gZWxlbWVudCAgICAgICAgIHx8IGNvbnRleHQ7XG4gICAgICAgICAgaWYodHlwZW9mIHF1ZXJ5ID09ICdzdHJpbmcnICYmIG9iamVjdCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBxdWVyeSAgICA9IHF1ZXJ5LnNwbGl0KC9bXFwuIF0vKTtcbiAgICAgICAgICAgIG1heERlcHRoID0gcXVlcnkubGVuZ3RoIC0gMTtcbiAgICAgICAgICAgICQuZWFjaChxdWVyeSwgZnVuY3Rpb24oZGVwdGgsIHZhbHVlKSB7XG4gICAgICAgICAgICAgIHZhciBjYW1lbENhc2VWYWx1ZSA9IChkZXB0aCAhPSBtYXhEZXB0aClcbiAgICAgICAgICAgICAgICA/IHZhbHVlICsgcXVlcnlbZGVwdGggKyAxXS5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIHF1ZXJ5W2RlcHRoICsgMV0uc2xpY2UoMSlcbiAgICAgICAgICAgICAgICA6IHF1ZXJ5XG4gICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdCggb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXSApICYmIChkZXB0aCAhPSBtYXhEZXB0aCkgKSB7XG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gb2JqZWN0W2NhbWVsQ2FzZVZhbHVlXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCBvYmplY3RbY2FtZWxDYXNlVmFsdWVdICE9PSB1bmRlZmluZWQgKSB7XG4gICAgICAgICAgICAgICAgZm91bmQgPSBvYmplY3RbY2FtZWxDYXNlVmFsdWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmKCAkLmlzUGxhaW5PYmplY3QoIG9iamVjdFt2YWx1ZV0gKSAmJiAoZGVwdGggIT0gbWF4RGVwdGgpICkge1xuICAgICAgICAgICAgICAgIG9iamVjdCA9IG9iamVjdFt2YWx1ZV07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSBpZiggb2JqZWN0W3ZhbHVlXSAhPT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgICAgIGZvdW5kID0gb2JqZWN0W3ZhbHVlXTtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm1ldGhvZCwgcXVlcnkpO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmICggJC5pc0Z1bmN0aW9uKCBmb3VuZCApICkge1xuICAgICAgICAgICAgcmVzcG9uc2UgPSBmb3VuZC5hcHBseShjb250ZXh0LCBwYXNzZWRBcmd1bWVudHMpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKGZvdW5kICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJlc3BvbnNlID0gZm91bmQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmKCQuaXNBcnJheShyZXR1cm5lZFZhbHVlKSkge1xuICAgICAgICAgICAgcmV0dXJuZWRWYWx1ZS5wdXNoKHJlc3BvbnNlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZihyZXR1cm5lZFZhbHVlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSBbcmV0dXJuZWRWYWx1ZSwgcmVzcG9uc2VdO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmKHJlc3BvbnNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybmVkVmFsdWUgPSByZXNwb25zZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGZvdW5kO1xuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICBpZihtZXRob2RJbnZva2VkKSB7XG4gICAgICAgIGlmKGluc3RhbmNlID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgICB9XG4gICAgICAgIG1vZHVsZS5pbnZva2UocXVlcnkpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGlmKGluc3RhbmNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBpbnN0YW5jZS5pbnZva2UoJ2Rlc3Ryb3knKTtcbiAgICAgICAgfVxuICAgICAgICBtb2R1bGUuaW5pdGlhbGl6ZSgpO1xuICAgICAgfVxuICAgIH0pXG4gIDtcblxuICByZXR1cm4gKHJldHVybmVkVmFsdWUgIT09IHVuZGVmaW5lZClcbiAgICA/IHJldHVybmVkVmFsdWVcbiAgICA6IHRoaXNcbiAgO1xufTtcblxuJC5hcGkuc2V0dGluZ3MgPSB7XG5cbiAgbmFtZSAgICAgICAgICAgICAgOiAnQVBJJyxcbiAgbmFtZXNwYWNlICAgICAgICAgOiAnYXBpJyxcblxuICBkZWJ1ZyAgICAgICAgICAgICA6IGZhbHNlLFxuICB2ZXJib3NlICAgICAgICAgICA6IGZhbHNlLFxuICBwZXJmb3JtYW5jZSAgICAgICA6IHRydWUsXG5cbiAgLy8gb2JqZWN0IGNvbnRhaW5pbmcgYWxsIHRlbXBsYXRlcyBlbmRwb2ludHNcbiAgYXBpICAgICAgICAgICAgICAgOiB7fSxcblxuICAvLyB3aGV0aGVyIHRvIGNhY2hlIHJlc3BvbnNlc1xuICBjYWNoZSAgICAgICAgICAgICA6IHRydWUsXG5cbiAgLy8gd2hldGhlciBuZXcgcmVxdWVzdHMgc2hvdWxkIGFib3J0IHByZXZpb3VzIHJlcXVlc3RzXG4gIGludGVycnVwdFJlcXVlc3RzIDogdHJ1ZSxcblxuICAvLyBldmVudCBiaW5kaW5nXG4gIG9uICAgICAgICAgICAgICAgIDogJ2F1dG8nLFxuXG4gIC8vIGNvbnRleHQgZm9yIGFwcGx5aW5nIHN0YXRlIGNsYXNzZXNcbiAgc3RhdGVDb250ZXh0ICAgICAgOiBmYWxzZSxcblxuICAvLyBkdXJhdGlvbiBmb3IgbG9hZGluZyBzdGF0ZVxuICBsb2FkaW5nRHVyYXRpb24gICA6IDAsXG5cbiAgLy8gd2hldGhlciB0byBoaWRlIGVycm9ycyBhZnRlciBhIHBlcmlvZCBvZiB0aW1lXG4gIGhpZGVFcnJvciAgICAgICAgIDogJ2F1dG8nLFxuXG4gIC8vIGR1cmF0aW9uIGZvciBlcnJvciBzdGF0ZVxuICBlcnJvckR1cmF0aW9uICAgICA6IDIwMDAsXG5cbiAgLy8gd2hldGhlciBwYXJhbWV0ZXJzIHNob3VsZCBiZSBlbmNvZGVkIHdpdGggZW5jb2RlVVJJQ29tcG9uZW50XG4gIGVuY29kZVBhcmFtZXRlcnMgIDogdHJ1ZSxcblxuICAvLyBBUEkgYWN0aW9uIHRvIHVzZVxuICBhY3Rpb24gICAgICAgICAgICA6IGZhbHNlLFxuXG4gIC8vIHRlbXBsYXRlZCBVUkwgdG8gdXNlXG4gIHVybCAgICAgICAgICAgICAgIDogZmFsc2UsXG5cbiAgLy8gYmFzZSBVUkwgdG8gYXBwbHkgdG8gYWxsIGVuZHBvaW50c1xuICBiYXNlICAgICAgICAgICAgICA6ICcnLFxuXG4gIC8vIGRhdGEgdGhhdCB3aWxsXG4gIHVybERhdGEgICAgICAgICAgIDoge30sXG5cbiAgLy8gd2hldGhlciB0byBhZGQgZGVmYXVsdCBkYXRhIHRvIHVybCBkYXRhXG4gIGRlZmF1bHREYXRhICAgICAgICAgIDogdHJ1ZSxcblxuICAvLyB3aGV0aGVyIHRvIHNlcmlhbGl6ZSBjbG9zZXN0IGZvcm1cbiAgc2VyaWFsaXplRm9ybSAgICAgICAgOiBmYWxzZSxcblxuICAvLyBob3cgbG9uZyB0byB3YWl0IGJlZm9yZSByZXF1ZXN0IHNob3VsZCBvY2N1clxuICB0aHJvdHRsZSAgICAgICAgICAgICA6IDAsXG5cbiAgLy8gd2hldGhlciB0byB0aHJvdHRsZSBmaXJzdCByZXF1ZXN0IG9yIG9ubHkgcmVwZWF0ZWRcbiAgdGhyb3R0bGVGaXJzdFJlcXVlc3QgOiB0cnVlLFxuXG4gIC8vIHN0YW5kYXJkIGFqYXggc2V0dGluZ3NcbiAgbWV0aG9kICAgICAgICAgICAgOiAnZ2V0JyxcbiAgZGF0YSAgICAgICAgICAgICAgOiB7fSxcbiAgZGF0YVR5cGUgICAgICAgICAgOiAnanNvbicsXG5cbiAgLy8gbW9jayByZXNwb25zZVxuICBtb2NrUmVzcG9uc2UgICAgICA6IGZhbHNlLFxuICBtb2NrUmVzcG9uc2VBc3luYyA6IGZhbHNlLFxuXG4gIC8vIGFsaWFzZXMgZm9yIG1vY2tcbiAgcmVzcG9uc2UgICAgICAgICAgOiBmYWxzZSxcbiAgcmVzcG9uc2VBc3luYyAgICAgOiBmYWxzZSxcblxuICAvLyBjYWxsYmFja3MgYmVmb3JlIHJlcXVlc3RcbiAgYmVmb3JlU2VuZCAgOiBmdW5jdGlvbihzZXR0aW5ncykgeyByZXR1cm4gc2V0dGluZ3M7IH0sXG4gIGJlZm9yZVhIUiAgIDogZnVuY3Rpb24oeGhyKSB7fSxcbiAgb25SZXF1ZXN0ICAgOiBmdW5jdGlvbihwcm9taXNlLCB4aHIpIHt9LFxuXG4gIC8vIGFmdGVyIHJlcXVlc3RcbiAgb25SZXNwb25zZSAgOiBmYWxzZSwgLy8gZnVuY3Rpb24ocmVzcG9uc2UpIHsgfSxcblxuICAvLyByZXNwb25zZSB3YXMgc3VjY2Vzc2Z1bCwgaWYgSlNPTiBwYXNzZWQgdmFsaWRhdGlvblxuICBvblN1Y2Nlc3MgICA6IGZ1bmN0aW9uKHJlc3BvbnNlLCAkbW9kdWxlKSB7fSxcblxuICAvLyByZXF1ZXN0IGZpbmlzaGVkIHdpdGhvdXQgYWJvcnRpbmdcbiAgb25Db21wbGV0ZSAgOiBmdW5jdGlvbihyZXNwb25zZSwgJG1vZHVsZSkge30sXG5cbiAgLy8gZmFpbGVkIEpTT04gc3VjY2VzcyB0ZXN0XG4gIG9uRmFpbHVyZSAgIDogZnVuY3Rpb24ocmVzcG9uc2UsICRtb2R1bGUpIHt9LFxuXG4gIC8vIHNlcnZlciBlcnJvclxuICBvbkVycm9yICAgICA6IGZ1bmN0aW9uKGVycm9yTWVzc2FnZSwgJG1vZHVsZSkge30sXG5cbiAgLy8gcmVxdWVzdCBhYm9ydGVkXG4gIG9uQWJvcnQgICAgIDogZnVuY3Rpb24oZXJyb3JNZXNzYWdlLCAkbW9kdWxlKSB7fSxcblxuICBzdWNjZXNzVGVzdCA6IGZhbHNlLFxuXG4gIC8vIGVycm9yc1xuICBlcnJvciA6IHtcbiAgICBiZWZvcmVTZW5kICAgICAgICA6ICdUaGUgYmVmb3JlIHNlbmQgZnVuY3Rpb24gaGFzIGFib3J0ZWQgdGhlIHJlcXVlc3QnLFxuICAgIGVycm9yICAgICAgICAgICAgIDogJ1RoZXJlIHdhcyBhbiBlcnJvciB3aXRoIHlvdXIgcmVxdWVzdCcsXG4gICAgZXhpdENvbmRpdGlvbnMgICAgOiAnQVBJIFJlcXVlc3QgQWJvcnRlZC4gRXhpdCBjb25kaXRpb25zIG1ldCcsXG4gICAgSlNPTlBhcnNlICAgICAgICAgOiAnSlNPTiBjb3VsZCBub3QgYmUgcGFyc2VkIGR1cmluZyBlcnJvciBoYW5kbGluZycsXG4gICAgbGVnYWN5UGFyYW1ldGVycyAgOiAnWW91IGFyZSB1c2luZyBsZWdhY3kgQVBJIHN1Y2Nlc3MgY2FsbGJhY2sgbmFtZXMnLFxuICAgIG1ldGhvZCAgICAgICAgICAgIDogJ1RoZSBtZXRob2QgeW91IGNhbGxlZCBpcyBub3QgZGVmaW5lZCcsXG4gICAgbWlzc2luZ0FjdGlvbiAgICAgOiAnQVBJIGFjdGlvbiB1c2VkIGJ1dCBubyB1cmwgd2FzIGRlZmluZWQnLFxuICAgIG1pc3NpbmdTZXJpYWxpemUgIDogJ2pxdWVyeS1zZXJpYWxpemUtb2JqZWN0IGlzIHJlcXVpcmVkIHRvIGFkZCBmb3JtIGRhdGEgdG8gYW4gZXhpc3RpbmcgZGF0YSBvYmplY3QnLFxuICAgIG1pc3NpbmdVUkwgICAgICAgIDogJ05vIFVSTCBzcGVjaWZpZWQgZm9yIGFwaSBldmVudCcsXG4gICAgbm9SZXR1cm5lZFZhbHVlICAgOiAnVGhlIGJlZm9yZVNlbmQgY2FsbGJhY2sgbXVzdCByZXR1cm4gYSBzZXR0aW5ncyBvYmplY3QsIGJlZm9yZVNlbmQgaWdub3JlZC4nLFxuICAgIG5vU3RvcmFnZSAgICAgICAgIDogJ0NhY2hpbmcgcmVzcG9uc2VzIGxvY2FsbHkgcmVxdWlyZXMgc2Vzc2lvbiBzdG9yYWdlJyxcbiAgICBwYXJzZUVycm9yICAgICAgICA6ICdUaGVyZSB3YXMgYW4gZXJyb3IgcGFyc2luZyB5b3VyIHJlcXVlc3QnLFxuICAgIHJlcXVpcmVkUGFyYW1ldGVyIDogJ01pc3NpbmcgYSByZXF1aXJlZCBVUkwgcGFyYW1ldGVyOiAnLFxuICAgIHN0YXR1c01lc3NhZ2UgICAgIDogJ1NlcnZlciBnYXZlIGFuIGVycm9yOiAnLFxuICAgIHRpbWVvdXQgICAgICAgICAgIDogJ1lvdXIgcmVxdWVzdCB0aW1lZCBvdXQnXG4gIH0sXG5cbiAgcmVnRXhwICA6IHtcbiAgICByZXF1aXJlZCA6IC9cXHtcXCQqW0EtejAtOV0rXFx9L2csXG4gICAgb3B0aW9uYWwgOiAvXFx7XFwvXFwkKltBLXowLTldK1xcfS9nLFxuICB9LFxuXG4gIGNsYXNzTmFtZToge1xuICAgIGxvYWRpbmcgOiAnbG9hZGluZycsXG4gICAgZXJyb3IgICA6ICdlcnJvcidcbiAgfSxcblxuICBzZWxlY3Rvcjoge1xuICAgIGRpc2FibGVkIDogJy5kaXNhYmxlZCcsXG4gICAgZm9ybSAgICAgIDogJ2Zvcm0nXG4gIH0sXG5cbiAgbWV0YWRhdGE6IHtcbiAgICBhY3Rpb24gIDogJ2FjdGlvbicsXG4gICAgdXJsICAgICA6ICd1cmwnXG4gIH1cbn07XG5cblxuXG59KSggalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50ICk7XG4iXX0=