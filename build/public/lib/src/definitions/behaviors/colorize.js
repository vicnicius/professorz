/*!
 * # Semantic UI - Colorize
 * http://github.com/semantic-org/semantic-ui/
 *
 *
 * Released under the MIT license
 * http://opensource.org/licenses/MIT
 *
 */

;(function ($, window, document, undefined) {

  "use strict";

  window = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();

  $.fn.colorize = function (parameters) {
    var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.colorize.settings, parameters) : $.extend({}, $.fn.colorize.settings),

    // hoist arguments
    moduleArguments = arguments || false;
    $(this).each(function (instanceIndex) {

      var $module = $(this),
          mainCanvas = $('<canvas />')[0],
          imageCanvas = $('<canvas />')[0],
          overlayCanvas = $('<canvas />')[0],
          backgroundImage = new Image(),


      // defs
      mainContext,
          imageContext,
          overlayContext,
          image,
          imageName,
          width,
          height,


      // shortcuts
      colors = settings.colors,
          paths = settings.paths,
          namespace = settings.namespace,
          error = settings.error,


      // boilerplate
      instance = $module.data('module-' + namespace),
          module;

      module = {

        checkPreconditions: function () {
          module.debug('Checking pre-conditions');

          if (!$.isPlainObject(colors) || $.isEmptyObject(colors)) {
            module.error(error.undefinedColors);
            return false;
          }
          return true;
        },

        async: function (callback) {
          if (settings.async) {
            setTimeout(callback, 0);
          } else {
            callback();
          }
        },

        getMetadata: function () {
          module.debug('Grabbing metadata');
          image = $module.data('image') || settings.image || undefined;
          imageName = $module.data('name') || settings.name || instanceIndex;
          width = settings.width || $module.width();
          height = settings.height || $module.height();
          if (width === 0 || height === 0) {
            module.error(error.undefinedSize);
          }
        },

        initialize: function () {
          module.debug('Initializing with colors', colors);
          if (module.checkPreconditions()) {

            module.async(function () {
              module.getMetadata();
              module.canvas.create();

              module.draw.image(function () {
                module.draw.colors();
                module.canvas.merge();
              });
              $module.data('module-' + namespace, module);
            });
          }
        },

        redraw: function () {
          module.debug('Redrawing image');
          module.async(function () {
            module.canvas.clear();
            module.draw.colors();
            module.canvas.merge();
          });
        },

        change: {
          color: function (colorName, color) {
            module.debug('Changing color', colorName);
            if (colors[colorName] === undefined) {
              module.error(error.missingColor);
              return false;
            }
            colors[colorName] = color;
            module.redraw();
          }
        },

        canvas: {
          create: function () {
            module.debug('Creating canvases');

            mainCanvas.width = width;
            mainCanvas.height = height;
            imageCanvas.width = width;
            imageCanvas.height = height;
            overlayCanvas.width = width;
            overlayCanvas.height = height;

            mainContext = mainCanvas.getContext('2d');
            imageContext = imageCanvas.getContext('2d');
            overlayContext = overlayCanvas.getContext('2d');

            $module.append(mainCanvas);
            mainContext = $module.children('canvas')[0].getContext('2d');
          },
          clear: function (context) {
            module.debug('Clearing canvas');
            overlayContext.fillStyle = '#FFFFFF';
            overlayContext.fillRect(0, 0, width, height);
          },
          merge: function () {
            if (!$.isFunction(mainContext.blendOnto)) {
              module.error(error.missingPlugin);
              return;
            }
            mainContext.putImageData(imageContext.getImageData(0, 0, width, height), 0, 0);
            overlayContext.blendOnto(mainContext, 'multiply');
          }
        },

        draw: {

          image: function (callback) {
            module.debug('Drawing image');
            callback = callback || function () {};
            if (image) {
              backgroundImage.src = image;
              backgroundImage.onload = function () {
                imageContext.drawImage(backgroundImage, 0, 0);
                callback();
              };
            } else {
              module.error(error.noImage);
              callback();
            }
          },

          colors: function () {
            module.debug('Drawing color overlays', colors);
            $.each(colors, function (colorName, color) {
              settings.onDraw(overlayContext, imageName, colorName, color);
            });
          }

        },

        debug: function (message, variableName) {
          if (settings.debug) {
            if (variableName !== undefined) {
              console.info(settings.name + ': ' + message, variableName);
            } else {
              console.info(settings.name + ': ' + message);
            }
          }
        },
        error: function (errorMessage) {
          console.warn(settings.name + ': ' + errorMessage);
        },
        invoke: function (methodName, context, methodArguments) {
          var method;
          methodArguments = methodArguments || Array.prototype.slice.call(arguments, 2);

          if (typeof methodName == 'string' && instance !== undefined) {
            methodName = methodName.split('.');
            $.each(methodName, function (index, name) {
              if ($.isPlainObject(instance[name])) {
                instance = instance[name];
                return true;
              } else if ($.isFunction(instance[name])) {
                method = instance[name];
                return true;
              }
              module.error(settings.error.method);
              return false;
            });
          }
          return $.isFunction(method) ? method.apply(context, methodArguments) : false;
        }

      };
      if (instance !== undefined && moduleArguments) {
        // simpler than invoke realizing to invoke itself (and losing scope due prototype.call()
        if (moduleArguments[0] == 'invoke') {
          moduleArguments = Array.prototype.slice.call(moduleArguments, 1);
        }
        return module.invoke(moduleArguments[0], this, Array.prototype.slice.call(moduleArguments, 1));
      }
      // initializing
      module.initialize();
    });
    return this;
  };

  $.fn.colorize.settings = {
    name: 'Image Colorizer',
    debug: true,
    namespace: 'colorize',

    onDraw: function (overlayContext, imageName, colorName, color) {},

    // whether to block execution while updating canvas
    async: true,
    // object containing names and default values of color regions
    colors: {},

    metadata: {
      image: 'image',
      name: 'name'
    },

    error: {
      noImage: 'No tracing image specified',
      undefinedColors: 'No default colors specified.',
      missingColor: 'Attempted to change color that does not exist',
      missingPlugin: 'Blend onto plug-in must be included',
      undefinedHeight: 'The width or height of image canvas could not be automatically determined. Please specify a height.'
    }

  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9wdWJsaWMvbGliL3NyYy9kZWZpbml0aW9ucy9iZWhhdmlvcnMvY29sb3JpemUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQVVBLENBQUMsQ0FBQyxVQUFVLENBQVYsRUFBYSxNQUFiLEVBQXFCLFFBQXJCLEVBQStCLFNBQS9CLEVBQTBDOztBQUU1Qzs7QUFFQSxXQUFVLE9BQU8sTUFBUCxJQUFpQixXQUFqQixJQUFnQyxPQUFPLElBQVAsSUFBZSxJQUFoRCxHQUNMLE1BREssR0FFSixPQUFPLElBQVAsSUFBZSxXQUFmLElBQThCLEtBQUssSUFBTCxJQUFhLElBQTVDLEdBQ0UsSUFERixHQUVFLFNBQVMsYUFBVCxHQUpOOztBQU9BLElBQUUsRUFBRixDQUFLLFFBQUwsR0FBZ0IsVUFBUyxVQUFULEVBQXFCO0FBQ25DLFFBQ0UsV0FBc0IsRUFBRSxhQUFGLENBQWdCLFVBQWhCLENBQUYsR0FDaEIsRUFBRSxNQUFGLENBQVMsSUFBVCxFQUFlLEVBQWYsRUFBbUIsRUFBRSxFQUFGLENBQUssUUFBTCxDQUFjLFFBQWpDLEVBQTJDLFVBQTNDLENBRGdCLEdBRWhCLEVBQUUsTUFBRixDQUFTLEVBQVQsRUFBYSxFQUFFLEVBQUYsQ0FBSyxRQUFMLENBQWMsUUFBM0IsQ0FITjtBQUFBOztBQUtFLHNCQUFrQixhQUFhLEtBTGpDO0FBT0EsTUFBRSxJQUFGLEVBQ0csSUFESCxDQUNRLFVBQVMsYUFBVCxFQUF3Qjs7QUFFNUIsVUFDRSxVQUFrQixFQUFFLElBQUYsQ0FEcEI7QUFBQSxVQUdFLGFBQWtCLEVBQUUsWUFBRixFQUFnQixDQUFoQixDQUhwQjtBQUFBLFVBSUUsY0FBa0IsRUFBRSxZQUFGLEVBQWdCLENBQWhCLENBSnBCO0FBQUEsVUFLRSxnQkFBa0IsRUFBRSxZQUFGLEVBQWdCLENBQWhCLENBTHBCO0FBQUEsVUFPRSxrQkFBa0IsSUFBSSxLQUFKLEVBUHBCO0FBQUE7OztBQVVFLGlCQVZGO0FBQUEsVUFXRSxZQVhGO0FBQUEsVUFZRSxjQVpGO0FBQUEsVUFjRSxLQWRGO0FBQUEsVUFlRSxTQWZGO0FBQUEsVUFpQkUsS0FqQkY7QUFBQSxVQWtCRSxNQWxCRjtBQUFBOzs7QUFxQkUsZUFBWSxTQUFTLE1BckJ2QjtBQUFBLFVBc0JFLFFBQVksU0FBUyxLQXRCdkI7QUFBQSxVQXVCRSxZQUFZLFNBQVMsU0F2QnZCO0FBQUEsVUF3QkUsUUFBWSxTQUFTLEtBeEJ2QjtBQUFBOzs7QUEyQkUsaUJBQWEsUUFBUSxJQUFSLENBQWEsWUFBWSxTQUF6QixDQTNCZjtBQUFBLFVBNEJFLE1BNUJGOztBQStCQSxlQUFTOztBQUVQLDRCQUFvQixZQUFXO0FBQzdCLGlCQUFPLEtBQVAsQ0FBYSx5QkFBYjs7QUFFQSxjQUFJLENBQUMsRUFBRSxhQUFGLENBQWdCLE1BQWhCLENBQUQsSUFBNEIsRUFBRSxhQUFGLENBQWdCLE1BQWhCLENBQWhDLEVBQTBEO0FBQ3hELG1CQUFPLEtBQVAsQ0FBYSxNQUFNLGVBQW5CO0FBQ0EsbUJBQU8sS0FBUDtBQUNEO0FBQ0QsaUJBQU8sSUFBUDtBQUNELFNBVk07O0FBWVAsZUFBTyxVQUFTLFFBQVQsRUFBbUI7QUFDeEIsY0FBRyxTQUFTLEtBQVosRUFBbUI7QUFDakIsdUJBQVcsUUFBWCxFQUFxQixDQUFyQjtBQUNELFdBRkQsTUFHSztBQUNIO0FBQ0Q7QUFDRixTQW5CTTs7QUFxQlAscUJBQWEsWUFBVztBQUN0QixpQkFBTyxLQUFQLENBQWEsbUJBQWI7QUFDQSxrQkFBWSxRQUFRLElBQVIsQ0FBYSxPQUFiLEtBQXlCLFNBQVMsS0FBbEMsSUFBMkMsU0FBdkQ7QUFDQSxzQkFBWSxRQUFRLElBQVIsQ0FBYSxNQUFiLEtBQXlCLFNBQVMsSUFBbEMsSUFBMkMsYUFBdkQ7QUFDQSxrQkFBWSxTQUFTLEtBQVQsSUFBeUIsUUFBUSxLQUFSLEVBQXJDO0FBQ0EsbUJBQVksU0FBUyxNQUFULElBQXlCLFFBQVEsTUFBUixFQUFyQztBQUNBLGNBQUcsVUFBVSxDQUFWLElBQWUsV0FBVyxDQUE3QixFQUFnQztBQUM5QixtQkFBTyxLQUFQLENBQWEsTUFBTSxhQUFuQjtBQUNEO0FBQ0YsU0E5Qk07O0FBZ0NQLG9CQUFZLFlBQVc7QUFDckIsaUJBQU8sS0FBUCxDQUFhLDBCQUFiLEVBQXlDLE1BQXpDO0FBQ0EsY0FBSSxPQUFPLGtCQUFQLEVBQUosRUFBa0M7O0FBRWhDLG1CQUFPLEtBQVAsQ0FBYSxZQUFXO0FBQ3RCLHFCQUFPLFdBQVA7QUFDQSxxQkFBTyxNQUFQLENBQWMsTUFBZDs7QUFFQSxxQkFBTyxJQUFQLENBQVksS0FBWixDQUFrQixZQUFXO0FBQzNCLHVCQUFPLElBQVAsQ0FBWSxNQUFaO0FBQ0EsdUJBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDRCxlQUhEO0FBSUEsc0JBQ0csSUFESCxDQUNRLFlBQVksU0FEcEIsRUFDK0IsTUFEL0I7QUFHRCxhQVhEO0FBWUQ7QUFDRixTQWpETTs7QUFtRFAsZ0JBQVEsWUFBVztBQUNqQixpQkFBTyxLQUFQLENBQWEsaUJBQWI7QUFDQSxpQkFBTyxLQUFQLENBQWEsWUFBVztBQUN0QixtQkFBTyxNQUFQLENBQWMsS0FBZDtBQUNBLG1CQUFPLElBQVAsQ0FBWSxNQUFaO0FBQ0EsbUJBQU8sTUFBUCxDQUFjLEtBQWQ7QUFDRCxXQUpEO0FBS0QsU0ExRE07O0FBNERQLGdCQUFRO0FBQ04saUJBQU8sVUFBUyxTQUFULEVBQW9CLEtBQXBCLEVBQTJCO0FBQ2hDLG1CQUFPLEtBQVAsQ0FBYSxnQkFBYixFQUErQixTQUEvQjtBQUNBLGdCQUFHLE9BQU8sU0FBUCxNQUFzQixTQUF6QixFQUFvQztBQUNsQyxxQkFBTyxLQUFQLENBQWEsTUFBTSxZQUFuQjtBQUNBLHFCQUFPLEtBQVA7QUFDRDtBQUNELG1CQUFPLFNBQVAsSUFBb0IsS0FBcEI7QUFDQSxtQkFBTyxNQUFQO0FBQ0Q7QUFUSyxTQTVERDs7QUF3RVAsZ0JBQVE7QUFDTixrQkFBUSxZQUFXO0FBQ2pCLG1CQUFPLEtBQVAsQ0FBYSxtQkFBYjs7QUFFQSx1QkFBVyxLQUFYLEdBQXVCLEtBQXZCO0FBQ0EsdUJBQVcsTUFBWCxHQUF1QixNQUF2QjtBQUNBLHdCQUFZLEtBQVosR0FBdUIsS0FBdkI7QUFDQSx3QkFBWSxNQUFaLEdBQXVCLE1BQXZCO0FBQ0EsMEJBQWMsS0FBZCxHQUF1QixLQUF2QjtBQUNBLDBCQUFjLE1BQWQsR0FBdUIsTUFBdkI7O0FBRUEsMEJBQWlCLFdBQVcsVUFBWCxDQUFzQixJQUF0QixDQUFqQjtBQUNBLDJCQUFpQixZQUFZLFVBQVosQ0FBdUIsSUFBdkIsQ0FBakI7QUFDQSw2QkFBaUIsY0FBYyxVQUFkLENBQXlCLElBQXpCLENBQWpCOztBQUVBLG9CQUNHLE1BREgsQ0FDVyxVQURYO0FBR0EsMEJBQWlCLFFBQVEsUUFBUixDQUFpQixRQUFqQixFQUEyQixDQUEzQixFQUE4QixVQUE5QixDQUF5QyxJQUF6QyxDQUFqQjtBQUNELFdBbkJLO0FBb0JOLGlCQUFPLFVBQVMsT0FBVCxFQUFrQjtBQUN2QixtQkFBTyxLQUFQLENBQWEsaUJBQWI7QUFDQSwyQkFBZSxTQUFmLEdBQTJCLFNBQTNCO0FBQ0EsMkJBQWUsUUFBZixDQUF3QixDQUF4QixFQUEyQixDQUEzQixFQUE4QixLQUE5QixFQUFxQyxNQUFyQztBQUNELFdBeEJLO0FBeUJOLGlCQUFPLFlBQVc7QUFDaEIsZ0JBQUksQ0FBQyxFQUFFLFVBQUYsQ0FBYSxZQUFZLFNBQXpCLENBQUwsRUFBMkM7QUFDekMscUJBQU8sS0FBUCxDQUFhLE1BQU0sYUFBbkI7QUFDQTtBQUNEO0FBQ0Qsd0JBQVksWUFBWixDQUEwQixhQUFhLFlBQWIsQ0FBMEIsQ0FBMUIsRUFBNkIsQ0FBN0IsRUFBZ0MsS0FBaEMsRUFBdUMsTUFBdkMsQ0FBMUIsRUFBMEUsQ0FBMUUsRUFBNkUsQ0FBN0U7QUFDQSwyQkFBZSxTQUFmLENBQXlCLFdBQXpCLEVBQXNDLFVBQXRDO0FBQ0Q7QUFoQ0ssU0F4RUQ7O0FBMkdQLGNBQU07O0FBRUosaUJBQU8sVUFBUyxRQUFULEVBQW1CO0FBQ3hCLG1CQUFPLEtBQVAsQ0FBYSxlQUFiO0FBQ0EsdUJBQVcsWUFBWSxZQUFVLENBQUUsQ0FBbkM7QUFDQSxnQkFBRyxLQUFILEVBQVU7QUFDUiw4QkFBZ0IsR0FBaEIsR0FBeUIsS0FBekI7QUFDQSw4QkFBZ0IsTUFBaEIsR0FBeUIsWUFBVztBQUNsQyw2QkFBYSxTQUFiLENBQXVCLGVBQXZCLEVBQXdDLENBQXhDLEVBQTJDLENBQTNDO0FBQ0E7QUFDRCxlQUhEO0FBSUQsYUFORCxNQU9LO0FBQ0gscUJBQU8sS0FBUCxDQUFhLE1BQU0sT0FBbkI7QUFDQTtBQUNEO0FBQ0YsV0FoQkc7O0FBa0JKLGtCQUFRLFlBQVc7QUFDakIsbUJBQU8sS0FBUCxDQUFhLHdCQUFiLEVBQXVDLE1BQXZDO0FBQ0EsY0FBRSxJQUFGLENBQU8sTUFBUCxFQUFlLFVBQVMsU0FBVCxFQUFvQixLQUFwQixFQUEyQjtBQUN4Qyx1QkFBUyxNQUFULENBQWdCLGNBQWhCLEVBQWdDLFNBQWhDLEVBQTJDLFNBQTNDLEVBQXNELEtBQXREO0FBQ0QsYUFGRDtBQUdEOztBQXZCRyxTQTNHQzs7QUFzSVAsZUFBTyxVQUFTLE9BQVQsRUFBa0IsWUFBbEIsRUFBZ0M7QUFDckMsY0FBRyxTQUFTLEtBQVosRUFBbUI7QUFDakIsZ0JBQUcsaUJBQWlCLFNBQXBCLEVBQStCO0FBQzdCLHNCQUFRLElBQVIsQ0FBYSxTQUFTLElBQVQsR0FBZ0IsSUFBaEIsR0FBdUIsT0FBcEMsRUFBNkMsWUFBN0M7QUFDRCxhQUZELE1BR0s7QUFDSCxzQkFBUSxJQUFSLENBQWEsU0FBUyxJQUFULEdBQWdCLElBQWhCLEdBQXVCLE9BQXBDO0FBQ0Q7QUFDRjtBQUNGLFNBL0lNO0FBZ0pQLGVBQU8sVUFBUyxZQUFULEVBQXVCO0FBQzVCLGtCQUFRLElBQVIsQ0FBYSxTQUFTLElBQVQsR0FBZ0IsSUFBaEIsR0FBdUIsWUFBcEM7QUFDRCxTQWxKTTtBQW1KUCxnQkFBUSxVQUFTLFVBQVQsRUFBcUIsT0FBckIsRUFBOEIsZUFBOUIsRUFBK0M7QUFDckQsY0FDRSxNQURGO0FBR0EsNEJBQWtCLG1CQUFtQixNQUFNLFNBQU4sQ0FBZ0IsS0FBaEIsQ0FBc0IsSUFBdEIsQ0FBNEIsU0FBNUIsRUFBdUMsQ0FBdkMsQ0FBckM7O0FBRUEsY0FBRyxPQUFPLFVBQVAsSUFBcUIsUUFBckIsSUFBaUMsYUFBYSxTQUFqRCxFQUE0RDtBQUMxRCx5QkFBYSxXQUFXLEtBQVgsQ0FBaUIsR0FBakIsQ0FBYjtBQUNBLGNBQUUsSUFBRixDQUFPLFVBQVAsRUFBbUIsVUFBUyxLQUFULEVBQWdCLElBQWhCLEVBQXNCO0FBQ3ZDLGtCQUFJLEVBQUUsYUFBRixDQUFpQixTQUFTLElBQVQsQ0FBakIsQ0FBSixFQUF3QztBQUN0QywyQkFBVyxTQUFTLElBQVQsQ0FBWDtBQUNBLHVCQUFPLElBQVA7QUFDRCxlQUhELE1BSUssSUFBSSxFQUFFLFVBQUYsQ0FBYyxTQUFTLElBQVQsQ0FBZCxDQUFKLEVBQXFDO0FBQ3hDLHlCQUFTLFNBQVMsSUFBVCxDQUFUO0FBQ0EsdUJBQU8sSUFBUDtBQUNEO0FBQ0QscUJBQU8sS0FBUCxDQUFhLFNBQVMsS0FBVCxDQUFlLE1BQTVCO0FBQ0EscUJBQU8sS0FBUDtBQUNELGFBWEQ7QUFZRDtBQUNELGlCQUFTLEVBQUUsVUFBRixDQUFjLE1BQWQsQ0FBRixHQUNILE9BQU8sS0FBUCxDQUFhLE9BQWIsRUFBc0IsZUFBdEIsQ0FERyxHQUVILEtBRko7QUFJRDs7QUE1S00sT0FBVDtBQStLQSxVQUFHLGFBQWEsU0FBYixJQUEwQixlQUE3QixFQUE4Qzs7QUFFNUMsWUFBRyxnQkFBZ0IsQ0FBaEIsS0FBc0IsUUFBekIsRUFBbUM7QUFDakMsNEJBQWtCLE1BQU0sU0FBTixDQUFnQixLQUFoQixDQUFzQixJQUF0QixDQUE0QixlQUE1QixFQUE2QyxDQUE3QyxDQUFsQjtBQUNEO0FBQ0QsZUFBTyxPQUFPLE1BQVAsQ0FBYyxnQkFBZ0IsQ0FBaEIsQ0FBZCxFQUFrQyxJQUFsQyxFQUF3QyxNQUFNLFNBQU4sQ0FBZ0IsS0FBaEIsQ0FBc0IsSUFBdEIsQ0FBNEIsZUFBNUIsRUFBNkMsQ0FBN0MsQ0FBeEMsQ0FBUDtBQUNEOztBQUVELGFBQU8sVUFBUDtBQUNELEtBMU5IO0FBNE5BLFdBQU8sSUFBUDtBQUNELEdBck9EOztBQXVPQSxJQUFFLEVBQUYsQ0FBSyxRQUFMLENBQWMsUUFBZCxHQUF5QjtBQUN2QixVQUFZLGlCQURXO0FBRXZCLFdBQVksSUFGVztBQUd2QixlQUFZLFVBSFc7O0FBS3ZCLFlBQVksVUFBUyxjQUFULEVBQXlCLFNBQXpCLEVBQW9DLFNBQXBDLEVBQStDLEtBQS9DLEVBQXNELENBQUUsQ0FMN0M7OztBQVF2QixXQUFZLElBUlc7O0FBVXZCLFlBQVksRUFWVzs7QUFZdkIsY0FBVTtBQUNSLGFBQVEsT0FEQTtBQUVSLFlBQVE7QUFGQSxLQVphOztBQWlCdkIsV0FBTztBQUNMLGVBQWtCLDRCQURiO0FBRUwsdUJBQWtCLDhCQUZiO0FBR0wsb0JBQWtCLCtDQUhiO0FBSUwscUJBQWtCLHFDQUpiO0FBS0wsdUJBQWtCO0FBTGI7O0FBakJnQixHQUF6QjtBQTJCQyxDQTdRQSxFQTZRRyxNQTdRSCxFQTZRVyxNQTdRWCxFQTZRbUIsUUE3UW5CIiwiZmlsZSI6ImNvbG9yaXplLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXG4gKiAjIFNlbWFudGljIFVJIC0gQ29sb3JpemVcbiAqIGh0dHA6Ly9naXRodWIuY29tL3NlbWFudGljLW9yZy9zZW1hbnRpYy11aS9cbiAqXG4gKlxuICogUmVsZWFzZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlXG4gKiBodHRwOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvTUlUXG4gKlxuICovXG5cbjsoZnVuY3Rpb24gKCQsIHdpbmRvdywgZG9jdW1lbnQsIHVuZGVmaW5lZCkge1xuXG5cInVzZSBzdHJpY3RcIjtcblxud2luZG93ID0gKHR5cGVvZiB3aW5kb3cgIT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93Lk1hdGggPT0gTWF0aClcbiAgPyB3aW5kb3dcbiAgOiAodHlwZW9mIHNlbGYgIT0gJ3VuZGVmaW5lZCcgJiYgc2VsZi5NYXRoID09IE1hdGgpXG4gICAgPyBzZWxmXG4gICAgOiBGdW5jdGlvbigncmV0dXJuIHRoaXMnKSgpXG47XG5cbiQuZm4uY29sb3JpemUgPSBmdW5jdGlvbihwYXJhbWV0ZXJzKSB7XG4gIHZhclxuICAgIHNldHRpbmdzICAgICAgICAgID0gKCAkLmlzUGxhaW5PYmplY3QocGFyYW1ldGVycykgKVxuICAgICAgPyAkLmV4dGVuZCh0cnVlLCB7fSwgJC5mbi5jb2xvcml6ZS5zZXR0aW5ncywgcGFyYW1ldGVycylcbiAgICAgIDogJC5leHRlbmQoe30sICQuZm4uY29sb3JpemUuc2V0dGluZ3MpLFxuICAgIC8vIGhvaXN0IGFyZ3VtZW50c1xuICAgIG1vZHVsZUFyZ3VtZW50cyA9IGFyZ3VtZW50cyB8fCBmYWxzZVxuICA7XG4gICQodGhpcylcbiAgICAuZWFjaChmdW5jdGlvbihpbnN0YW5jZUluZGV4KSB7XG5cbiAgICAgIHZhclxuICAgICAgICAkbW9kdWxlICAgICAgICAgPSAkKHRoaXMpLFxuXG4gICAgICAgIG1haW5DYW52YXMgICAgICA9ICQoJzxjYW52YXMgLz4nKVswXSxcbiAgICAgICAgaW1hZ2VDYW52YXMgICAgID0gJCgnPGNhbnZhcyAvPicpWzBdLFxuICAgICAgICBvdmVybGF5Q2FudmFzICAgPSAkKCc8Y2FudmFzIC8+JylbMF0sXG5cbiAgICAgICAgYmFja2dyb3VuZEltYWdlID0gbmV3IEltYWdlKCksXG5cbiAgICAgICAgLy8gZGVmc1xuICAgICAgICBtYWluQ29udGV4dCxcbiAgICAgICAgaW1hZ2VDb250ZXh0LFxuICAgICAgICBvdmVybGF5Q29udGV4dCxcblxuICAgICAgICBpbWFnZSxcbiAgICAgICAgaW1hZ2VOYW1lLFxuXG4gICAgICAgIHdpZHRoLFxuICAgICAgICBoZWlnaHQsXG5cbiAgICAgICAgLy8gc2hvcnRjdXRzXG4gICAgICAgIGNvbG9ycyAgICA9IHNldHRpbmdzLmNvbG9ycyxcbiAgICAgICAgcGF0aHMgICAgID0gc2V0dGluZ3MucGF0aHMsXG4gICAgICAgIG5hbWVzcGFjZSA9IHNldHRpbmdzLm5hbWVzcGFjZSxcbiAgICAgICAgZXJyb3IgICAgID0gc2V0dGluZ3MuZXJyb3IsXG5cbiAgICAgICAgLy8gYm9pbGVycGxhdGVcbiAgICAgICAgaW5zdGFuY2UgICA9ICRtb2R1bGUuZGF0YSgnbW9kdWxlLScgKyBuYW1lc3BhY2UpLFxuICAgICAgICBtb2R1bGVcbiAgICAgIDtcblxuICAgICAgbW9kdWxlID0ge1xuXG4gICAgICAgIGNoZWNrUHJlY29uZGl0aW9uczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdDaGVja2luZyBwcmUtY29uZGl0aW9ucycpO1xuXG4gICAgICAgICAgaWYoICEkLmlzUGxhaW5PYmplY3QoY29sb3JzKSB8fCAkLmlzRW1wdHlPYmplY3QoY29sb3JzKSApIHtcbiAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci51bmRlZmluZWRDb2xvcnMpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfSxcblxuICAgICAgICBhc3luYzogZnVuY3Rpb24oY2FsbGJhY2spIHtcbiAgICAgICAgICBpZihzZXR0aW5ncy5hc3luYykge1xuICAgICAgICAgICAgc2V0VGltZW91dChjYWxsYmFjaywgMCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0TWV0YWRhdGE6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnR3JhYmJpbmcgbWV0YWRhdGEnKTtcbiAgICAgICAgICBpbWFnZSAgICAgPSAkbW9kdWxlLmRhdGEoJ2ltYWdlJykgfHwgc2V0dGluZ3MuaW1hZ2UgfHwgdW5kZWZpbmVkO1xuICAgICAgICAgIGltYWdlTmFtZSA9ICRtb2R1bGUuZGF0YSgnbmFtZScpICB8fCBzZXR0aW5ncy5uYW1lICB8fCBpbnN0YW5jZUluZGV4O1xuICAgICAgICAgIHdpZHRoICAgICA9IHNldHRpbmdzLndpZHRoICAgICAgICB8fCAkbW9kdWxlLndpZHRoKCk7XG4gICAgICAgICAgaGVpZ2h0ICAgID0gc2V0dGluZ3MuaGVpZ2h0ICAgICAgIHx8ICRtb2R1bGUuaGVpZ2h0KCk7XG4gICAgICAgICAgaWYod2lkdGggPT09IDAgfHwgaGVpZ2h0ID09PSAwKSB7XG4gICAgICAgICAgICBtb2R1bGUuZXJyb3IoZXJyb3IudW5kZWZpbmVkU2l6ZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgIG1vZHVsZS5kZWJ1ZygnSW5pdGlhbGl6aW5nIHdpdGggY29sb3JzJywgY29sb3JzKTtcbiAgICAgICAgICBpZiggbW9kdWxlLmNoZWNrUHJlY29uZGl0aW9ucygpICkge1xuXG4gICAgICAgICAgICBtb2R1bGUuYXN5bmMoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5nZXRNZXRhZGF0YSgpO1xuICAgICAgICAgICAgICBtb2R1bGUuY2FudmFzLmNyZWF0ZSgpO1xuXG4gICAgICAgICAgICAgIG1vZHVsZS5kcmF3LmltYWdlKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIG1vZHVsZS5kcmF3LmNvbG9ycygpO1xuICAgICAgICAgICAgICAgIG1vZHVsZS5jYW52YXMubWVyZ2UoKTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgICAuZGF0YSgnbW9kdWxlLScgKyBuYW1lc3BhY2UsIG1vZHVsZSlcbiAgICAgICAgICAgICAgO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlZHJhdzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgbW9kdWxlLmRlYnVnKCdSZWRyYXdpbmcgaW1hZ2UnKTtcbiAgICAgICAgICBtb2R1bGUuYXN5bmMoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBtb2R1bGUuY2FudmFzLmNsZWFyKCk7XG4gICAgICAgICAgICBtb2R1bGUuZHJhdy5jb2xvcnMoKTtcbiAgICAgICAgICAgIG1vZHVsZS5jYW52YXMubWVyZ2UoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICBjaGFuZ2U6IHtcbiAgICAgICAgICBjb2xvcjogZnVuY3Rpb24oY29sb3JOYW1lLCBjb2xvcikge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDaGFuZ2luZyBjb2xvcicsIGNvbG9yTmFtZSk7XG4gICAgICAgICAgICBpZihjb2xvcnNbY29sb3JOYW1lXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5lcnJvcihlcnJvci5taXNzaW5nQ29sb3IpO1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb2xvcnNbY29sb3JOYW1lXSA9IGNvbG9yO1xuICAgICAgICAgICAgbW9kdWxlLnJlZHJhdygpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBjYW52YXM6IHtcbiAgICAgICAgICBjcmVhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDcmVhdGluZyBjYW52YXNlcycpO1xuXG4gICAgICAgICAgICBtYWluQ2FudmFzLndpZHRoICAgICA9IHdpZHRoO1xuICAgICAgICAgICAgbWFpbkNhbnZhcy5oZWlnaHQgICAgPSBoZWlnaHQ7XG4gICAgICAgICAgICBpbWFnZUNhbnZhcy53aWR0aCAgICA9IHdpZHRoO1xuICAgICAgICAgICAgaW1hZ2VDYW52YXMuaGVpZ2h0ICAgPSBoZWlnaHQ7XG4gICAgICAgICAgICBvdmVybGF5Q2FudmFzLndpZHRoICA9IHdpZHRoO1xuICAgICAgICAgICAgb3ZlcmxheUNhbnZhcy5oZWlnaHQgPSBoZWlnaHQ7XG5cbiAgICAgICAgICAgIG1haW5Db250ZXh0ICAgID0gbWFpbkNhbnZhcy5nZXRDb250ZXh0KCcyZCcpO1xuICAgICAgICAgICAgaW1hZ2VDb250ZXh0ICAgPSBpbWFnZUNhbnZhcy5nZXRDb250ZXh0KCcyZCcpO1xuICAgICAgICAgICAgb3ZlcmxheUNvbnRleHQgPSBvdmVybGF5Q2FudmFzLmdldENvbnRleHQoJzJkJyk7XG5cbiAgICAgICAgICAgICRtb2R1bGVcbiAgICAgICAgICAgICAgLmFwcGVuZCggbWFpbkNhbnZhcyApXG4gICAgICAgICAgICA7XG4gICAgICAgICAgICBtYWluQ29udGV4dCAgICA9ICRtb2R1bGUuY2hpbGRyZW4oJ2NhbnZhcycpWzBdLmdldENvbnRleHQoJzJkJyk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBjbGVhcjogZnVuY3Rpb24oY29udGV4dCkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdDbGVhcmluZyBjYW52YXMnKTtcbiAgICAgICAgICAgIG92ZXJsYXlDb250ZXh0LmZpbGxTdHlsZSA9ICcjRkZGRkZGJztcbiAgICAgICAgICAgIG92ZXJsYXlDb250ZXh0LmZpbGxSZWN0KDAsIDAsIHdpZHRoLCBoZWlnaHQpO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgbWVyZ2U6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoICEkLmlzRnVuY3Rpb24obWFpbkNvbnRleHQuYmxlbmRPbnRvKSApIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm1pc3NpbmdQbHVnaW4pO1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBtYWluQ29udGV4dC5wdXRJbWFnZURhdGEoIGltYWdlQ29udGV4dC5nZXRJbWFnZURhdGEoMCwgMCwgd2lkdGgsIGhlaWdodCksIDAsIDApO1xuICAgICAgICAgICAgb3ZlcmxheUNvbnRleHQuYmxlbmRPbnRvKG1haW5Db250ZXh0LCAnbXVsdGlwbHknKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgZHJhdzoge1xuXG4gICAgICAgICAgaW1hZ2U6IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG4gICAgICAgICAgICBtb2R1bGUuZGVidWcoJ0RyYXdpbmcgaW1hZ2UnKTtcbiAgICAgICAgICAgIGNhbGxiYWNrID0gY2FsbGJhY2sgfHwgZnVuY3Rpb24oKXt9O1xuICAgICAgICAgICAgaWYoaW1hZ2UpIHtcbiAgICAgICAgICAgICAgYmFja2dyb3VuZEltYWdlLnNyYyAgICA9IGltYWdlO1xuICAgICAgICAgICAgICBiYWNrZ3JvdW5kSW1hZ2Uub25sb2FkID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgaW1hZ2VDb250ZXh0LmRyYXdJbWFnZShiYWNrZ3JvdW5kSW1hZ2UsIDAsIDApO1xuICAgICAgICAgICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbW9kdWxlLmVycm9yKGVycm9yLm5vSW1hZ2UpO1xuICAgICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG5cbiAgICAgICAgICBjb2xvcnM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbW9kdWxlLmRlYnVnKCdEcmF3aW5nIGNvbG9yIG92ZXJsYXlzJywgY29sb3JzKTtcbiAgICAgICAgICAgICQuZWFjaChjb2xvcnMsIGZ1bmN0aW9uKGNvbG9yTmFtZSwgY29sb3IpIHtcbiAgICAgICAgICAgICAgc2V0dGluZ3Mub25EcmF3KG92ZXJsYXlDb250ZXh0LCBpbWFnZU5hbWUsIGNvbG9yTmFtZSwgY29sb3IpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgIH0sXG5cbiAgICAgICAgZGVidWc6IGZ1bmN0aW9uKG1lc3NhZ2UsIHZhcmlhYmxlTmFtZSkge1xuICAgICAgICAgIGlmKHNldHRpbmdzLmRlYnVnKSB7XG4gICAgICAgICAgICBpZih2YXJpYWJsZU5hbWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICBjb25zb2xlLmluZm8oc2V0dGluZ3MubmFtZSArICc6ICcgKyBtZXNzYWdlLCB2YXJpYWJsZU5hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUuaW5mbyhzZXR0aW5ncy5uYW1lICsgJzogJyArIG1lc3NhZ2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZXJyb3I6IGZ1bmN0aW9uKGVycm9yTWVzc2FnZSkge1xuICAgICAgICAgIGNvbnNvbGUud2FybihzZXR0aW5ncy5uYW1lICsgJzogJyArIGVycm9yTWVzc2FnZSk7XG4gICAgICAgIH0sXG4gICAgICAgIGludm9rZTogZnVuY3Rpb24obWV0aG9kTmFtZSwgY29udGV4dCwgbWV0aG9kQXJndW1lbnRzKSB7XG4gICAgICAgICAgdmFyXG4gICAgICAgICAgICBtZXRob2RcbiAgICAgICAgICA7XG4gICAgICAgICAgbWV0aG9kQXJndW1lbnRzID0gbWV0aG9kQXJndW1lbnRzIHx8IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKCBhcmd1bWVudHMsIDIgKTtcblxuICAgICAgICAgIGlmKHR5cGVvZiBtZXRob2ROYW1lID09ICdzdHJpbmcnICYmIGluc3RhbmNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIG1ldGhvZE5hbWUgPSBtZXRob2ROYW1lLnNwbGl0KCcuJyk7XG4gICAgICAgICAgICAkLmVhY2gobWV0aG9kTmFtZSwgZnVuY3Rpb24oaW5kZXgsIG5hbWUpIHtcbiAgICAgICAgICAgICAgaWYoICQuaXNQbGFpbk9iamVjdCggaW5zdGFuY2VbbmFtZV0gKSApIHtcbiAgICAgICAgICAgICAgICBpbnN0YW5jZSA9IGluc3RhbmNlW25hbWVdO1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYoICQuaXNGdW5jdGlvbiggaW5zdGFuY2VbbmFtZV0gKSApIHtcbiAgICAgICAgICAgICAgICBtZXRob2QgPSBpbnN0YW5jZVtuYW1lXTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBtb2R1bGUuZXJyb3Ioc2V0dGluZ3MuZXJyb3IubWV0aG9kKTtcbiAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiAoICQuaXNGdW5jdGlvbiggbWV0aG9kICkgKVxuICAgICAgICAgICAgPyBtZXRob2QuYXBwbHkoY29udGV4dCwgbWV0aG9kQXJndW1lbnRzKVxuICAgICAgICAgICAgOiBmYWxzZVxuICAgICAgICAgIDtcbiAgICAgICAgfVxuXG4gICAgICB9O1xuICAgICAgaWYoaW5zdGFuY2UgIT09IHVuZGVmaW5lZCAmJiBtb2R1bGVBcmd1bWVudHMpIHtcbiAgICAgICAgLy8gc2ltcGxlciB0aGFuIGludm9rZSByZWFsaXppbmcgdG8gaW52b2tlIGl0c2VsZiAoYW5kIGxvc2luZyBzY29wZSBkdWUgcHJvdG90eXBlLmNhbGwoKVxuICAgICAgICBpZihtb2R1bGVBcmd1bWVudHNbMF0gPT0gJ2ludm9rZScpIHtcbiAgICAgICAgICBtb2R1bGVBcmd1bWVudHMgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCggbW9kdWxlQXJndW1lbnRzLCAxICk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG1vZHVsZS5pbnZva2UobW9kdWxlQXJndW1lbnRzWzBdLCB0aGlzLCBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCggbW9kdWxlQXJndW1lbnRzLCAxICkgKTtcbiAgICAgIH1cbiAgICAgIC8vIGluaXRpYWxpemluZ1xuICAgICAgbW9kdWxlLmluaXRpYWxpemUoKTtcbiAgICB9KVxuICA7XG4gIHJldHVybiB0aGlzO1xufTtcblxuJC5mbi5jb2xvcml6ZS5zZXR0aW5ncyA9IHtcbiAgbmFtZSAgICAgIDogJ0ltYWdlIENvbG9yaXplcicsXG4gIGRlYnVnICAgICA6IHRydWUsXG4gIG5hbWVzcGFjZSA6ICdjb2xvcml6ZScsXG5cbiAgb25EcmF3ICAgIDogZnVuY3Rpb24ob3ZlcmxheUNvbnRleHQsIGltYWdlTmFtZSwgY29sb3JOYW1lLCBjb2xvcikge30sXG5cbiAgLy8gd2hldGhlciB0byBibG9jayBleGVjdXRpb24gd2hpbGUgdXBkYXRpbmcgY2FudmFzXG4gIGFzeW5jICAgICA6IHRydWUsXG4gIC8vIG9iamVjdCBjb250YWluaW5nIG5hbWVzIGFuZCBkZWZhdWx0IHZhbHVlcyBvZiBjb2xvciByZWdpb25zXG4gIGNvbG9ycyAgICA6IHt9LFxuXG4gIG1ldGFkYXRhOiB7XG4gICAgaW1hZ2UgOiAnaW1hZ2UnLFxuICAgIG5hbWUgIDogJ25hbWUnXG4gIH0sXG5cbiAgZXJyb3I6IHtcbiAgICBub0ltYWdlICAgICAgICAgOiAnTm8gdHJhY2luZyBpbWFnZSBzcGVjaWZpZWQnLFxuICAgIHVuZGVmaW5lZENvbG9ycyA6ICdObyBkZWZhdWx0IGNvbG9ycyBzcGVjaWZpZWQuJyxcbiAgICBtaXNzaW5nQ29sb3IgICAgOiAnQXR0ZW1wdGVkIHRvIGNoYW5nZSBjb2xvciB0aGF0IGRvZXMgbm90IGV4aXN0JyxcbiAgICBtaXNzaW5nUGx1Z2luICAgOiAnQmxlbmQgb250byBwbHVnLWluIG11c3QgYmUgaW5jbHVkZWQnLFxuICAgIHVuZGVmaW5lZEhlaWdodCA6ICdUaGUgd2lkdGggb3IgaGVpZ2h0IG9mIGltYWdlIGNhbnZhcyBjb3VsZCBub3QgYmUgYXV0b21hdGljYWxseSBkZXRlcm1pbmVkLiBQbGVhc2Ugc3BlY2lmeSBhIGhlaWdodC4nXG4gIH1cblxufTtcblxufSkoIGpRdWVyeSwgd2luZG93LCBkb2N1bWVudCApO1xuIl19