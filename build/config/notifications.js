
'use strict';

const Mailgun = require('mailgun-js');
const configuration = require('./configuration');

const api_key = configuration.mg.api_key;
const domain = configuration.mg.domain;
const from_who = configuration.mg.from;

const mailgun = new Mailgun({ apiKey: api_key, domain: domain });

function notifyInvite(email) {
  const data = {
    from: from_who,
    to: email,
    subject: 'Bem-vindo ao Professor Z',
    html: `<h3>Bem-vindo ao Professor Z!</h3>
            <p>Estamos trabalhando a todo vapor para construir um grande parceiro para o dia-a-dia do professor.</p>
            <p>Você está na lista de convidados para a versão de testes, então assim que tudo estiver pronto, você receberá um e-mail com o link para acessar a plataforma.</p>
            <p>Enquanto isso, você pode acompanhar novidades em nossa <a href="http://fb.me/professorz.co">página no facebook</a>.<p>
            <p>Um grande abraço da equipe Professor Z.</p>`
  };

  return mailgun.messages().send(data, (err, body) => {
    if (err) {
      console.error(err);
    }

    console.log(body);
  });
}

module.exports = {
  notifyInvite: notifyInvite
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb25maWcvbm90aWZpY2F0aW9ucy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7O0FBRUEsTUFBTSxVQUFVLFFBQVEsWUFBUixDQUFoQjtBQUNBLE1BQU0sZ0JBQWdCLFFBQVEsaUJBQVIsQ0FBdEI7O0FBRUEsTUFBTSxVQUFtQixjQUFjLEVBQWQsQ0FBaUIsT0FBMUM7QUFDQSxNQUFNLFNBQWtCLGNBQWMsRUFBZCxDQUFpQixNQUF6QztBQUNBLE1BQU0sV0FBb0IsY0FBYyxFQUFkLENBQWlCLElBQTNDOztBQUVBLE1BQU0sVUFBbUIsSUFBSSxPQUFKLENBQVksRUFBQyxRQUFRLE9BQVQsRUFBa0IsUUFBUSxNQUExQixFQUFaLENBQXpCOztBQUVBLFNBQVMsWUFBVCxDQUF1QixLQUF2QixFQUF1QztBQUNyQyxRQUFNLE9BQU87QUFDWCxVQUFNLFFBREs7QUFFWCxRQUFJLEtBRk87QUFHWCxhQUFTLDBCQUhFO0FBSVgsVUFBTzs7Ozs7QUFKSSxHQUFiOztBQVdBLFNBQU8sUUFBUSxRQUFSLEdBQW1CLElBQW5CLENBQXdCLElBQXhCLEVBQThCLENBQUMsR0FBRCxFQUFNLElBQU4sS0FBZTtBQUNsRCxRQUFJLEdBQUosRUFBUztBQUNQLGNBQVEsS0FBUixDQUFjLEdBQWQ7QUFDRDs7QUFFRCxZQUFRLEdBQVIsQ0FBWSxJQUFaO0FBQ0QsR0FOTSxDQUFQO0FBT0Q7O0FBRUQsT0FBTyxPQUFQLEdBQWlCO0FBQ2YsZ0JBQWM7QUFEQyxDQUFqQiIsImZpbGUiOiJub3RpZmljYXRpb25zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogQGZsb3cgKi9cbid1c2Ugc3RyaWN0J1xuXG5jb25zdCBNYWlsZ3VuID0gcmVxdWlyZSgnbWFpbGd1bi1qcycpXG5jb25zdCBjb25maWd1cmF0aW9uID0gcmVxdWlyZSgnLi9jb25maWd1cmF0aW9uJylcblxuY29uc3QgYXBpX2tleSA6IHN0cmluZyA9IGNvbmZpZ3VyYXRpb24ubWcuYXBpX2tleVxuY29uc3QgZG9tYWluIDogc3RyaW5nID0gY29uZmlndXJhdGlvbi5tZy5kb21haW5cbmNvbnN0IGZyb21fd2hvIDogc3RyaW5nID0gY29uZmlndXJhdGlvbi5tZy5mcm9tXG5cbmNvbnN0IG1haWxndW4gOiBPYmplY3QgPSBuZXcgTWFpbGd1bih7YXBpS2V5OiBhcGlfa2V5LCBkb21haW46IGRvbWFpbn0pXG5cbmZ1bmN0aW9uIG5vdGlmeUludml0ZSAoZW1haWwgOiBzdHJpbmcpIHtcbiAgY29uc3QgZGF0YSA9IHtcbiAgICBmcm9tOiBmcm9tX3dobyxcbiAgICB0bzogZW1haWwsXG4gICAgc3ViamVjdDogJ0JlbS12aW5kbyBhbyBQcm9mZXNzb3IgWicsXG4gICAgaHRtbDogYDxoMz5CZW0tdmluZG8gYW8gUHJvZmVzc29yIFohPC9oMz5cbiAgICAgICAgICAgIDxwPkVzdGFtb3MgdHJhYmFsaGFuZG8gYSB0b2RvIHZhcG9yIHBhcmEgY29uc3RydWlyIHVtIGdyYW5kZSBwYXJjZWlybyBwYXJhIG8gZGlhLWEtZGlhIGRvIHByb2Zlc3Nvci48L3A+XG4gICAgICAgICAgICA8cD5Wb2PDqiBlc3TDoSBuYSBsaXN0YSBkZSBjb252aWRhZG9zIHBhcmEgYSB2ZXJzw6NvIGRlIHRlc3RlcywgZW50w6NvIGFzc2ltIHF1ZSB0dWRvIGVzdGl2ZXIgcHJvbnRvLCB2b2PDqiByZWNlYmVyw6EgdW0gZS1tYWlsIGNvbSBvIGxpbmsgcGFyYSBhY2Vzc2FyIGEgcGxhdGFmb3JtYS48L3A+XG4gICAgICAgICAgICA8cD5FbnF1YW50byBpc3NvLCB2b2PDqiBwb2RlIGFjb21wYW5oYXIgbm92aWRhZGVzIGVtIG5vc3NhIDxhIGhyZWY9XCJodHRwOi8vZmIubWUvcHJvZmVzc29yei5jb1wiPnDDoWdpbmEgbm8gZmFjZWJvb2s8L2E+LjxwPlxuICAgICAgICAgICAgPHA+VW0gZ3JhbmRlIGFicmHDp28gZGEgZXF1aXBlIFByb2Zlc3NvciBaLjwvcD5gXG4gIH1cblxuICByZXR1cm4gbWFpbGd1bi5tZXNzYWdlcygpLnNlbmQoZGF0YSwgKGVyciwgYm9keSkgPT4ge1xuICAgIGlmIChlcnIpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKVxuICAgIH1cblxuICAgIGNvbnNvbGUubG9nKGJvZHkpXG4gIH0pXG59XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICBub3RpZnlJbnZpdGU6IG5vdGlmeUludml0ZVxufVxuIl19