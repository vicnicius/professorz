'use strict'

const request = require('supertest')

function run (app, server, test) {
  let user_id, token
  test('new user', t => {
    request(server)
      .post('/user')
      .field('username', 'tester')
      .field('password', 'password123')
      .field('email', 'test@test.com')
      .expect(200)
      .end((err, res) => {
         if (err) t.fail(err.message)
         user_id = res.body._id
         token = res.body.token
         t.equal(res.body.email, 'test@test.com')
         return t.end()
      })
  })
  test('existing user', t => {
    request(server)
      .post('/user')
      .field('username', 'tester')
      .field('password', 'password123')
      .field('email', 'test@test.com')
      .expect(409)
      .end((err, res) => {
         if (err) t.fail(err.message)
         t.equal(res.body.message, 'User already exists.')
         t.end()
      })
  })
  test('invalid user update', t => {
    request(server)
      .put(`/user/${user_id}`)
      .field('email', 'invalidupdated@test.com')
      .expect(401)
      .end((err, res) => {
        if (err) t.fail(err.message)
        t.equal(res.status, 401)
        t.end()
      })
  }) 
  test('invalid login', t => {
    request(server)
      .post('/login')
      .field('username', 'tester')
      .field('password', 'wrongPassword')
      .expect(401)
      .end((err, res) => {
         if (err) t.fail(err.message)
         t.equal(res.status, 401)
         return t.end()
      })
  })
  test('login', t => {
    request(server)
      .post('/login')
      .field('username', 'tester')
      .field('password', 'password123')
      .expect(200)
      .end((err, res) => {
        if (err) t.fail(err.message)
        t.ok(res.body.token)
        return t.end()
      })
  })
  test('user update', t => {
    request(server)
      .set('Authorization', `Bearer ${token}`)
      .put(`/user/${user_id}`)
      .field('email', 'updated@test.com')
      .expect(200)
      .end((err, res) => {
        if (err) t.fail(err.message)
        t.equal(res.body.email, 'udpated@test.com')
        return t.end()
      })
  })
  test('logout', t => {
    request(server)
      .post('/logout')
      .expect(200)
      .end((err, res) => {
        if (err) t.fail(err.message)
        t.ok(res.body.message)
        return t.end()
      })
  })
}

module.exports = {run: run}
