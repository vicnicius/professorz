'use strict'

process.env.NODE_ENV = 'test'

const http = require('http')
const test = require('tape-catch')
//Application
const app = require('../build/index')
//Start server and wrap it
const server = http.createServer(app.callback())
const onReady = app.events.onDbConnected
//Run specs

function runTests (specs) {
  specs.map(spec => spec.run(app, server, test))
  test.onFinish(() => process.exit(0))
}

onReady(() => runTests([
  require('./api/user.spec'),
  require('./static.route.spec'),
]))

