const request = require('supertest')

function run (app, server, test) {
  test('root', t => {
    request(server)
      .get('/')
      .expect(200)
      .end((err, res) => {
        if (err) t.fail(err.message)
        t.equal(res.header['content-type'], 'text/html; charset=utf-8', '/ 200 ok')
        return t.end()
      })
  })
  test('app', t => {
    request(server)
      .get('/app')
      .expect(200)
      .end((err, res) => {
        if (err) t.fail(err.message)
        t.equal(res.header['content-type'], 'text/html; charset=utf-8', '/app 200 ok')
        return t.end()
      })
  })
}

module.exports = {run: run};
