import Http
import Json.Decode as Json exposing (..)
import Task exposing (..)
import Html exposing (Html, input, p, div, text, button)
import Html.App as App
import Html.Attributes exposing (type', value)
import Html.Events exposing (onInput, onClick)
import Time exposing (Time, millisecond)
import List exposing (map, head, filter, append, reverse)
import String exposing (length, left)

main =
  App.program
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }


-- MODEL

type StatementType = Open | Boolean

type alias Statement = { id: String, final : String, current : String, statementType : Maybe StatementType }

type alias Conversation = List Statement

type alias Answer = ( Maybe Bool, Maybe String )

type alias Model = Conversation

initialStatement : Statement
initialStatement =
  { id = "1"
  , final = "Olá, eu sou o Professor Z, seu assistente pessoal de sala de aula."
  , current = ""
  , statementType = Nothing
  }

loginQuestion : Statement
loginQuestion =
  { id = "2"
  , final = "Você já tem uma conta em nosso aplicativo?"
  , current = ""
  , statementType = Just Boolean
  }

init : ( Model, Cmd Msg )
init =
  ( [ initialStatement, loginQuestion ], Cmd.none )


incompleteConversation : Conversation -> Conversation
incompleteConversation conversation =
  let
    isIncomplete statement =
      length statement.final > length statement.current
  in
    filter isIncomplete conversation

updateConversation : Conversation -> Conversation
updateConversation conversation =
  let
    incompletes = incompleteConversation conversation
  in
    List.map (\statement -> updateStatement statement incompletes) conversation

updateStatement : Statement -> Conversation -> Statement
updateStatement statement incompletes =
  let
    shouldUpdate =
      case head incompletes of
        Just incompleteStatement -> statement == incompleteStatement
        Nothing -> False
    currentSize =
      length statement.current
  in
    if shouldUpdate then
      { statement | current = left (currentSize + 1) statement.final }
    else
      statement

-- UPDATE

type Msg
  = Tick Time
  | FetchYes
  | FetchNo
  | FetchSucceed String
  | FetchFail Http.Error
  | FetchText String

update : Msg -> Conversation -> ( Conversation , Cmd Msg )
update msg model =
  let
    currentStatement = head <| reverse model
  in
    case msg of
      Tick newTime ->
        ( updateConversation model, Cmd.none )
      FetchYes ->
        ( model, getAnswer (Just True, Nothing) currentStatement )
      FetchNo ->
        ( model, getAnswer (Just False, Nothing) currentStatement )
      FetchText txt ->
        ( model, getAnswer (Nothing, Just txt) currentStatement )
      FetchSucceed _ ->
        ( model, Cmd.none)
      FetchFail _ ->
        ( model, Cmd.none )
-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
  Time.every (4/16 * millisecond) Tick


-- VIEW

view : Model -> Html Msg
view model =
  div [] (List.map displayStatement model)

answerComponent : Statement -> Html Msg
answerComponent statement =
  if length statement.final == length statement.current then
    case statement.statementType of
      Just Open -> input [ type' "Open" ] []
      Just Boolean -> div [] [ button [onClick FetchYes] [ text "Sim" ]
                             , button [onClick FetchNo] [ text "Não" ]
                             ]
      Nothing -> text ""
  else
    text ""

displayStatement : Statement -> Html Msg
displayStatement statement =
  div [] [ p [] [ text statement.current ]
         , answerComponent statement
         ]

-- HTTP
getAnswer : Answer -> Maybe Statement -> Cmd Msg
getAnswer answer statement =
  let
    uri =
      case statement of
        Just statement ->
          "http://localhost:3000/api/conversation/" ++ statement.id
        Nothing ->
          "http://localhost:3000/api/conversation/"
    body =
      case answer of
        (Just True, _) ->
          Http.string """{ "answer" : true}"""
        (Just False, _) ->
          Http.string """{ "answer" : false}"""
        (_, Just txt) ->
          Http.string """{ "answer" : "{{txt}}"}"""
        (_, _) ->
          Http.string """{ "answer" : "" }"""
  in
    Task.perform FetchFail FetchSucceed <| Http.post moveConversation uri body

moveConversation : Json.Decoder String
moveConversation =
  Json.at ["data", "conversation"] Json.string
